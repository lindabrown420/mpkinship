trigger CategoryTrg on Category__c (before insert, before update,after update) {
    TriggerActivation__c IsTriggerActivate = TriggerActivation__c.getAll().get('CategoryTrigger'); 
     
     if (IsTriggerActivate != null && IsTriggerActivate.isActive__c){ 
         TriggerFactory.createAndExecuteHandler(CategoryTriggerHandler.class);
     /**CHECK IF ANY PO LINK TO ANY ACTIVE CATEGORY **/
         set<id> categoryIds = new set<id>();
         if(trigger.isupdate && trigger.isafter){
              for(category__C category : trigger.new){
                  if(category.Inactive__c==true && trigger.oldmap.get(category.id).Inactive__c==false){
                     categoryIds.add(category.id);     
                  }
              } 
              id caseActionRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Case_Actions').getRecordTypeId();
              map<id,list<opportunity>> CategoryCaseActionMap = new map<id,list<opportunity>>();
              list<opportunity> opplist=[select id,Category__c from opportunity where (stagename=:'Draft' or stagename=:'Open')
                                      and recordtypeid=:caseActionRecordTypeId and category__C In: categoryIds];
 
              for(opportunity opp : opplist){
                  if(CategoryCaseActionMap.containskey(opp.category__c)){
                      CategoryCaseActionMap.get(opp.category__c).add(opp);
                  }
                  else
                     CategoryCaseActionMap.put(opp.category__c,new list<opportunity>{opp}); 
              } 
              
              map<id,list<id>> POSOCategoryMap = new map<id,list<id>>();  
               Id porecTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Purchase_Order').getRecordTypeId();
               list<opportunitylineitem> opplineitem=[select id,category__C,OpportunityId,opportunity.stagename from opportunitylineitem 
               where (opportunity.stagename=:'Draft' or opportunity.stagename=:'Open') and opportunity.recordtypeid=: porecTypeId and
                                                     category__c in :categoryIds ];
 
               for(opportunitylineitem oppitem : opplineitem){
                   if(POSOCategoryMap.containskey(oppitem.category__C))
                       POSOCategoryMap.get(oppitem.category__c).add(oppitem.opportunityid);
                   else
                       POSOCategoryMap.put(oppitem.category__c,new list<id>{oppitem.opportunityid});    
               } 
 
               map<id,list<id>> ReceiptVendorRefCategoryMap = new map<id,list<id>>();  
               list<Payment__c> paymets=[Select Id, Category_lookup__c From Payment__c 
                                                   Where (Recordtype.Name=:'Receipt' or Recordtype.Name=:'Vendor Refund') and Category_lookup__c in :categoryIds];
             
               for(Payment__c pay : paymets){
 
                   ReceiptVendorRefCategoryMap.put(pay.Category_lookup__c,new list<id>{pay.Id});    
               } 
 
              for(Category__c cat : trigger.new){
                  if(CategoryCaseActionMap.size()>0 && CategoryCaseActionMap.containskey(cat.id)){
                         if(CategoryCaseActionMap.get(cat.id).size()>0)
                             cat.adderror('You cannot inactive this category as it has an active Case Action');
                  }
                  else if (POSOCategoryMap.size()>0 && POSOCategoryMap.containskey(cat.id)){
                      if(POSOCategoryMap.get(cat.id).size()>0)
                          cat.adderror('You cannot inactive this category as it has an active POSO');
                  }
                  else if (ReceiptVendorRefCategoryMap.size()>0 && ReceiptVendorRefCategoryMap.containskey(cat.id)){
                         cat.adderror('You cannot inactive this category as it has an active Receipt/Vendor Refund');
                 }
              }    
         }
     }
 }