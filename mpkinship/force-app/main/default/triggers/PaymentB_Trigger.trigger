trigger PaymentB_Trigger on Payment__c (after insert, before insert, before update, after update, before delete) {
    if(PaymentTriggerHandler.bypassBTrigger) return;
    set<id> statementList = new set<id>();
    set<id> claimsIds = new set<id>();
    List<Special_Welfare_Account__c> spaTempList = new List<Special_Welfare_Account__c>();
    Special_Welfare_Account__c tempSWA = new Special_Welfare_Account__c();
    List<Bank_Statement__c> bsList = new List<Bank_Statement__c>();
    Id recordTypeId = Schema.SObjectType.Payment__c.getRecordTypeInfosByName().get('Staff & Operations').getRecordTypeId();
    Id vendorRefundRTId = Schema.SObjectType.Payment__c.getRecordTypeInfosByName().get('Vendor Refund').getRecordTypeId();
    Id vendorInvoiceRTId = Schema.SObjectType.Payment__c.getRecordTypeInfosByName().get('Vendor Invoice').getRecordTypeId();
    id specialwelfarereimburseRTId=  Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Special_Welfare_Reimbursement').getRecordTypeId();
    if((Trigger.isbefore))
    {
        if((Trigger.isDelete ) ){
            // KIN 63
            List<Payment__c> paymentList= new List<Payment__c>();   
            List<Id> oppIdList= new List<Id>();
            //ends
            Set<Id> paymentIds = Trigger.oldMap.keySet();
            List<Id> vendorInvoiceIds = new List<Id>();
            Map<Id,Decimal> vendorRefundIdsMap = new Map<Id,Decimal>();
            Id ReceiptrecordTypeId =Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Receipt').getRecordTypeId();  
            //set<id> swaids = new set<id>();
            for(Payment__c invoice :Trigger.Old){
                 //ADDED FOR SWA
                 /*if(invoice.Special_Welfare_Account__c!=null && (invoice.recordtypeid==vendorRefundRTId || invoice.recordtypeid==specialwelfarereimburseRTId
                   || invoice.recordtypeid==vendorInvoiceRTId  || invoice.recordtypeid==ReceiptrecordTypeId) && !Paymentrecordhelper.isSWADelete){
                       //oldmap.put(payment.id,trigger.oldmap.get(payment.id));     
                       swaids.add(invoice.Special_Welfare_Account__c);   
                   } */
                // KIN 63
                     if(!PurchaseOrderController.IspoScreenRun &&  !PurchaseOrderControllerEdit.IspoScreenRun &&
                     !CaseActionPOEditController.IsCaseActionEditScreen && !CaseActionPOController.IsCaseActionScreen){
                        paymentList.add(invoice);
                        oppIdList.add(invoice.Opportunity__c);
                    }
                    // ends
                System.debug('invoice.RecordTypeID '+invoice.RecordTypeID );
                System.debug('recordTypeId'+recordTypeId);
                System.debug('vendorInvoiceRTId'+vendorInvoiceRTId);
                if((invoice.RecordTypeID == vendorInvoiceRTId) || (invoice.RecordTypeID == recordTypeId)){
                    vendorInvoiceIds.add(invoice.Id);
                }
                if(invoice.RecordTypeID == vendorRefundRTId && invoice.Payment__c != null){
                    Decimal refundAmount = vendorRefundIdsMap.get(invoice.Payment__c);
                    if(refundAmount == null){
                        refundAmount = 0.0;
                    }
                    refundAmount += invoice.Payment_Amount__c;
                    vendorRefundIdsMap.put(invoice.Payment__c,refundAmount);
                }
            }
            // KIN 63
            Map<Id,Opportunity> mapOpp;
             if(!PurchaseOrderController.IspoScreenRun &&  !PurchaseOrderControllerEdit.IspoScreenRun &&
                !CaseActionPOEditController.IsCaseActionEditScreen && !CaseActionPOController.IsCaseActionScreen){
                mapOpp = new Map<Id,Opportunity>([Select Id,name,recordtype.name from Opportunity where Id=:oppIdList]);
            }
            if(paymentList.size()>0){
                for(Payment__c opppay : paymentList){
                    System.debug('OpportunityId'+opppay.Opportunity__c);
                    if(opppay.Opportunity__c!=Null && mapOpp.get(opppay.Opportunity__c) !=null){
                        sySTEM.DEBUG('rec type'+mapOpp.get(opppay.Opportunity__c).recordtype.name);
                        if(mapOpp.get(opppay.Opportunity__c).recordtype.name == 'Purchase of Services Order' && !paymentrecordhelper.IsPaymentDelete)
                        opppay.addError('Can not delete a Payment related to POSO');
                        if(mapOpp.get(opppay.Opportunity__c).recordtype.name == 'Case Actions')
                        opppay.addError('Can not delete a Payment related to CA');
                    }
                }
            }
            //ends
            System.debug('vendorInvoiceIds'+vendorInvoiceIds);
            // If vendor invoice gets deleted
            List<Payment__c> venderRefundList = [Select Id from Payment__c where Payment__c In :vendorInvoiceIds And (RecordType.Name = 'Vendor Refund' OR RecordType.Name ='Staff & Operations')];
            System.debug('venderRefundList'+venderRefundList);
            if(venderRefundList.size() > 0){
                delete venderRefundList;
            }

            // If vendor refund gets deleted
            List<Payment__c> venderInvoiceList = [Select Id,Prior_Refund1__c from Payment__c where ID In :vendorRefundIdsMap.keySet()];
            List<Payment__c> tempList = new List<Payment__c>();
            Payment__c temp = new Payment__c();
            for(Payment__c payment : venderInvoiceList){
                temp.Id = payment.Id;
                temp.Prior_Refund1__c = payment.Prior_Refund1__c -  - (vendorRefundIdsMap!=null && vendorRefundIdsMap.size() > 0?vendorRefundIdsMap.get(payment.Id):0.0);
                tempList.add(temp);
                temp = new Payment__c();
            }
            if(tempList.size() > 0 ){
                TransactionJournalService.byPassCheckPosted = true;
                update tempList;
                TransactionJournalService.byPassCheckPosted = false;
            }
            
            //FOR SWA
              /*if(swaids.size()>0)
                paymentrecordhelper.updateSWA(swaids); */
        }
        if((Trigger.isInsert || Trigger.isUpdate) ){
            for(Payment__c inv: Trigger.New){
                if(inv.RecordTypeId == recordTypeId && inv.Payment_Method__c== 'Check' && inv.Invoice_Stage__c != 'Paid'
                && inv.Kinship_Check__c!=null){
                    inv.Invoice_Stage__c = 'Ready to Pay';
                }
                // KIN 1729
                /*if((inv.RecordTypeId == recordTypeId || inv.RecordTypeId == vendorInvoiceRTId) && (inv.Payment_Method__c== '' || inv.Payment_Method__c== null)){
                    inv.Payment_Method__c='Check';
                }*/
            }
        }
        if(Trigger.isUpdate){
            
            Set<Id> paymentIds = Trigger.oldMap.keySet();
            List<Id> vendorInvoiceIds = new List<Id>();
            Map<Id,Decimal> vendorRefundIdsMap = new Map<Id,Decimal>();
            Map<Id,Decimal> vendorRefundIdMap = new Map<Id,Decimal>();
            
            for( Payment__c invoice :Trigger.Old){
                if(invoice.RecordTypeID == vendorRefundRTId && invoice.Payment__c != null){
                    Decimal refundAmount = vendorRefundIdsMap.get(invoice.Payment__c);
                    if(refundAmount == null){
                        refundAmount = 0.0;
                    }
                    refundAmount += invoice.Payment_Amount__c;
                    vendorRefundIdsMap.put(invoice.Payment__c,refundAmount);
                }
            }
            for( Payment__c invoice :Trigger.New){
                if(invoice.RecordTypeID == vendorRefundRTId && invoice.Payment__c != null){
                    Decimal refundAmount = vendorRefundIdMap.get(invoice.Payment__c);
                    if(refundAmount == null){
                        refundAmount = 0.0;
                    }
                    refundAmount += invoice.Payment_Amount__c;
                    vendorRefundIdMap.put(invoice.Payment__c,refundAmount);
                }
            }
            List<Payment__c> venderInvoiceList = [Select Id,Prior_Refund1__c from Payment__c where ID In :vendorRefundIdsMap.keySet()];
            List<Payment__c> tempList = new List<Payment__c>();
            Payment__c temp = new Payment__c();
            for(Payment__c payment : venderInvoiceList){
                temp.Id = payment.Id;
                if(payment.Prior_Refund1__c != null){
                    temp.Prior_Refund1__c = payment.Prior_Refund1__c - vendorRefundIdsMap.get(payment.Id) + vendorRefundIdMap.get(payment.Id);
                }
                tempList.add(temp);
                temp = new Payment__c();
            }
            if(tempList.size() > 0 ){
                TransactionJournalService.byPassCheckPosted = true;
                update tempList;
                TransactionJournalService.byPassCheckPosted = false;
            }
        }
    }
    if( (Trigger.isAfter))
    {
        if((Trigger.isInsert || Trigger.isUpdate) ){
            For(Payment__c payment : Trigger.new){
                if(payment.Special_Welfare_Account__c != null && trigger.isInsert){
                    tempSWA.Id = payment.Special_Welfare_Account__c ;
                    spaTempList.add(tempSWA);
                    tempSWA = new Special_Welfare_Account__c();
                }
                
                if(payment.Bank_Statement__c != null) {
                    statementList.add(payment.Bank_Statement__c);
                }
                if(payment.Claims__c != null){
                    claimsIds.add(payment.Claims__c);
                }
            }     
        }
        //FOR UPDATE IF SPECIAL WELFARE GETS REMOVE
      /*  if(trigger.isUpdate){
            Id ReceiptrecordTypeId =Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Receipt').getRecordTypeId();  
           
             set<id> swalist= new set<id>();
            for(Payment__c payment: trigger.new){
                if(payment.Special_Welfare_Account__c!=trigger.oldmap.get(payment.id).Special_Welfare_Account__c && 
                   trigger.oldmap.get(payment.id).Special_Welfare_Account__c!=null && (payment.recordtypeid==vendorRefundRTId || payment.recordtypeid==specialwelfarereimburseRTId
                   || payment.recordtypeid==vendorInvoiceRTId  || payment.recordtypeid==ReceiptrecordTypeId) && !Paymentrecordhelper.isSWAUpdate){
                            
                       swalist.add(trigger.oldmap.get(payment.id).Special_Welfare_Account__c);  
                       swalist.add(payment.Special_Welfare_Account__c); 
                   }
                else if(payment.Special_Welfare_Account__c!=null && payment.Special_Welfare_Account__c==trigger.oldmap.get(payment.id).Special_Welfare_Account__c &&
                  (payment.Payment_Amount__c!=trigger.oldmap.get(payment.id).Payment_Amount__c || payment.Payment_Source__c != trigger.oldmap.get(payment.id).Payment_Source__c )){
                    swalist.add(payment.Special_Welfare_Account__c); 
                }   
            }
            
             if(swalist.size()>0)
                paymentrecordhelper.updateSWA(swalist);       
        } */
        if(!Trigger.isinsert){
            For(Payment__c payment : Trigger.Old){
            if(payment.Bank_Statement__c != null) {
                statementList.add(payment.Bank_Statement__c);
            }
        }                                   
            
        }
        Decimal total = 0;                            
        try{
            Bank_Statement__c temp = new Bank_Statement__c();
            for(Id bsID : statementList){
                temp = new Bank_Statement__c();
                temp.Id = bsID;
                bsList.Add(temp);
            }
            update bsList;               
            if(spaTempList.size() > 0){
                update spaTempList;
                
                
                
                
                
                
                
                
                
                
                
                
                
            }
        }Catch(Exception e){
            System.debug('Exception in after update:'+e.getMessage());
        }    
        try{
            if(claimsIds.size() > 0)            
            {
                List<Claims__c> Claimss = [Select id,Amount_Received__c,Balance_Due__c,Payment_Status__c from Claims__c where id IN: claimsIds ];
                List<Claims__c> UpdateClaimss = new List<Claims__c>();
                List<AggregateResult> AggregateResultList = [select Sum(Payment_Amount__c)amt, Claims__c from Payment__c        
                 where Claims__c IN :claimsIds group by Claims__c ];
                 system.debug(AggregateResultList  + ' AggregateResultList ????');
                 if(AggregateResultList != null && AggregateResultList.size() > 0){ 
                    
                    for(AggregateResult aggr:AggregateResultList){             
                        for(Claims__c c: Claimss){
                        
                            system.debug( c.id + ' c.id  ' + (id)aggr.get('Claims__c')+ ' (id)aggr.get(Claims__c)????');
                            system.debug( (decimal)aggr.get('amt') + ' (decimal)aggr.get(amt)????');
                            
                            if(c.id == (id)aggr.get('Claims__c')){
                              c.Amount_Received__c  = (decimal)aggr.get('amt');   
                              //KIN 142
                                if(c.Balance_Due__c==0){
                                    c.Payment_Status__c='Paid in Full';
                                }
                                else{
                                    c.Payment_Status__c='Partially Paid';
                                }
                              //ends
                                UpdateClaimss.add(c);
                            }        
                        }                        
                    } 
                } 
                if(UpdateClaimss.size() > 0){
                    update UpdateClaimss ;
                }
            }
        }Catch(Exception e){
            System.debug('Exception in after update:'+e.getMessage());
        }                
    }

}