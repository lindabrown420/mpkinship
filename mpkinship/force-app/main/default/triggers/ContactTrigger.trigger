trigger ContactTrigger on Contact (before insert, before update, after update) {
    TriggerActivation__c qliTrigger = TriggerActivation__c.getAll().get('ContactTrigger');
    if (qliTrigger != null && qliTrigger.isActive__c){
        TriggerFactory.createAndExecuteHandler(ContactTriggerHandler.class);
        if(trigger.isInsert && trigger.isBefore){
            for(Contact con: trigger.New ){
                con.MailingCountry = 'US';
                con.OtherCountry = 'US';
            }
        }
    }
}