trigger Payment_Trigger on Payment__c (before insert, after insert, before update, after update, before delete) {
    TriggerActivation__c qliTrigger = TriggerActivation__c.getAll().get('PaymentTrigger');
    if (qliTrigger != null && qliTrigger.isActive__c){
       set<id> opportunityids = new set<id>();
       
        if(trigger.isafter && trigger.isInsert){
             Id VendorInvoicerecordTypeId =Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Vendor_Invoice').getRecordTypeId(); 
            list<Payment__c > payments = new list<Payment__c>();
            set<id> createdPaymentsIds = new set<id>();
            set<id> paymentidsForCaseActionClaimes = new set<id>();
             set<id> paymentids = new set<id>(); 
             
            for(Payment__c paymentrec : trigger.new){
                if(!Paymentrecordhelper.isidupdate ){
                    payments.add(paymentrec); 
                } 
                //CHANGING RECORD NAME
                if(!paymentrecordhelper.isNameChange){
                    paymentids.add(paymentrec.id);
                }
                //CREATE PAYMENT LINE ITEMS
                if(!Paymentrecordhelper.isPaymentItemRun && !checkTrgHelper.IsCancelledRun && !checkTrgHelper.IsResissued)
                    createdPaymentsIds.add(paymentrec.id ); 
                       
                //CHECK FOR IF ITS A CASEACTION PAYMENT FOR CLAIMS
               if(paymentrec.Claims__c!=null && paymentrec.recordtypeid== VendorInvoicerecordTypeId && paymentrec.Invoice_Type__c=='Case Action'
                && !Paymentrecordhelper.IsClaimUpdateInsert){
                   paymentidsForCaseActionClaimes.add(paymentrec.id);    
               }  
                                                             
            }
            if(payments.size()>0)
                Paymentrecordhelper.populaterecordid(payments);
            if(createdPaymentsIds.size()>0)
                Paymentrecordhelper.createPaymentLineItems1(createdPaymentsIds);  
            if(paymentidsForCaseActionClaimes.size()>0)
                Paymentrecordhelper.ClaimUpdate(paymentidsForCaseActionClaimes);  
            if(paymentids.size()>0)
                paymentrecordhelper.PopulateGlobalSearchName(paymentids);        
        }
    
        /**FOR BEFORE UPDATE **/   
        if(trigger.isbefore && trigger.isUpdate){
            Id VendorInvoicerecordTypeId =Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Vendor_Invoice').getRecordTypeId();  
            Map<Id, Payment__c > paymentMap = Trigger.NewMap;
            map<id,Payment__c> opportunitypaymentmap = new map<id,Payment__c>();
            map<id,Payment__c> OpportunityPaymentamountmap = new map<id,Payment__c>();
            set<id> paymentids = new set<id>();
            set<id> paidPaymentIds = new set<id>();
           
            
            for(Payment__c paymentrecord : trigger.new){
                            
                if(paymentrecord.Written_Off__c==true && trigger.oldmap.get(paymentrecord.id).Paid__c==true)
                    paymentrecord.adderror('Paid payments cannot be written off.');
                    
                
                // Update amount text field.
               // paymentrecord.Amount__c = FactoryCls.formatCurr(paymentrecord.Payment__c);
                
                //CHECK IF CHECK IS REALTED TO INVOICE RECORD THAN CANT CHANGE ITS PAYMENT DATE KIN-1128
               if(paymentrecord.recordtypeid==VendorInvoicerecordTypeId && paymentrecord.Kinship_Check__c!=null && paymentrecord.Payment_Date__c!=
                   trigger.oldmap.get(paymentrecord.id).Payment_Date__c && trigger.oldmap.get(paymentrecord.id).Payment_Date__c!=null &&
                   !Paymentrecordhelper.IsPaymentDateUpdate && !Paymentrecordhelper.IsDateUpdateRun){
                       paymentrecordhelper.IsDateUpdateRun=true;
                       paymentrecord.addError('Please use check record to change the payment date of invoice.');
                       
                   } 
                
                //CHANGE STAGE TO WRITEOFF TOO IF PAYMENT GETS WRITEOFF
                if(paymentrecord.Written_Off__c==true && trigger.oldmap.get(paymentrecord.id).Written_Off__c != paymentrecord.Written_Off__c
                 && paymentrecord.Invoice_Stage__c !='Write off'){
                     paymentrecord.Invoice_Stage__c='Write off';
                 }  
                 
                 //CHECK IF WRITEOFF STAGE IS CHANGED TO READY TO PAY
                 if(paymentrecord.Invoice_Stage__c =='Ready To Pay' && trigger.oldmap.get(paymentrecord.id).Invoice_Stage__c=='Write off' 
                     && paymentrecord.recordtypeid==VendorInvoicerecordTypeId ){
                      paymentrecord.addError('You can not change the stage to Ready to Pay once invoice has been written off');   
                 } 
                    
                // Payment Source field to set a value of 1 for Paid Vendor Invoice with CSA & IVE 
                if(paymentrecord.Paid__c==true && paymentrecord.Paid__c!=trigger.oldmap.get(paymentrecord.id).Paid__c
                  && paymentrecord.Category_lookup__c!=null &&  paymentrecord.recordtypeid==VendorInvoicerecordTypeId){
                      paidPaymentIds.add(paymentrecord.id);    
                  }  
                  
                   
            }
           
            
           Map<Id,Payment__c> paymentidmap= new Map<Id,Payment__c>([select id,Category_lookup__c,Category_lookup__r.Category_Type__c from Payment__c where id in :paidPaymentIds]); 
           if(paymentidmap.size() >0){
               for(Payment__c payment : trigger.new){
                   if(paymentidmap.containskey(payment.id)){
                       Payment__c paymentrec = paymentidmap.get(payment.id);   
                       if(paymentrec.Category_lookup__r.Category_Type__c=='CSA' || paymentrec.Category_lookup__r.Category_Type__c=='IVE'){
                           payment.Payment_Source__c='1';
                       }
                   } 
               }
           }
           
        }  
        
        //ADDED AFTER UPDATE 
        if(trigger.isafter && trigger.isupdate){
             map<id,list<Payment__c>> OpportunityPaymentamountmap = new map<id,list<Payment__c>>();   
             map<id,list<Payment__c>> OpportunityPaymentmap = new map<id,list<Payment__c>>(); 
             set<id> opportunitiesids = new set<id>();   
             set<id> lineItemPaymentIds = new set<id>();  
             set<id> writeoffOpportunitiesIds = new set<id>();
             set<id> claimUpdatePayments = new set<id>(); 
             set<id> paymentIdsSet =new set<id>();
             set<id> paymentNameChangeids = new set<id>();
              
             Id recordTypeId =Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Vendor_Invoice').getRecordTypeId();  
             for(Payment__c paymentrecord : trigger.new){
                 
                /**NAME CHANGES FOR GLOBAL SEARCH **/
                 if(!paymentrecordhelper.isNameChangeUpdate) 
                     paymentNameChangeids.add(paymentrecord.id);
                     
                 /*****IF PAYMENTS ARE COMPLETED BASED ON PAYMENT LINEITEM AMOUNTS THEN WRITEOFF REMAINING **/
                if(paymentrecord.Paid__c==true && trigger.oldmap.get(paymentrecord.id).Paid__c!= paymentrecord.Paid__c 
                    && PurchaseOrderTrgHelper.IsPaymentWriteoff==false && !PaymentTriggerLineItemHandler.isItemPayment && paymentrecord.recordtypeid==recordTypeId
                    && paymentrecord.Invoice_Type__c=='POSO'){
                    lineItemPaymentIds.add(paymentrecord.id);    
                }     
                 
                /** FOR CHANGING STAGE TO CLOSED In paid**/
                if((paymentrecord.Paid__c==true && trigger.oldmap.get(paymentrecord.id).Paid__c!= paymentrecord.Paid__c)
                    && PurchaseOrderTrgHelper.IsPaymentWriteoff==false && !Paymentrecordhelper.ischange && paymentrecord.recordtypeid==recordTypeId){
                    system.debug('**Entered in closed stage of payment');
                    opportunitiesids.add(paymentrecord.Opportunity__c);   
                 } 
                
                /**FOR CHANING THE STAGE IN WRITEOFF ***/
                 if((paymentrecord.Written_Off__c==true && trigger.oldmap.get(paymentrecord.id).Written_Off__c != paymentrecord.Written_Off__c)
                    && PurchaseOrderTrgHelper.IsPaymentWriteoff==false && !Paymentrecordhelper.isWriteoffChange && paymentrecord.recordtypeid==recordTypeId ){
                        system.debug('**Entered in closed stage of payment of writeoff');
                    writeoffOpportunitiesIds.add(paymentrecord.Opportunity__c);   
                 } 
                 /**FOR CLAIM PAYMENTS OF CASEACTION TYPE WHICH ARE SET TO PAID **/
                 if(paymentrecord.Paid__c==true && paymentrecord.Invoice_Stage__c=='Paid' && trigger.oldmap.get(paymentrecord.id).Invoice_Stage__c=='Ready to Pay'
                 && paymentrecord.Claims__c!=null && paymentrecord.recordtypeid== recordTypeId && paymentrecord.Invoice_Type__c=='Case Action'
                 && !Paymentrecordhelper.IsClaimUpdadtPaymentPaid){
                     claimUpdatePayments.add(paymentrecord.id);
                 } 
                 
                 /********FOR FICA PAID PAYMENTS NEED TO CREATE TWO CASE ACTIONS**/
                 if(paymentrecord.Paid__c==true && trigger.oldmap.get(paymentrecord.id).Invoice_Stage__c=='Ready to Pay' && paymentrecord.recordtypeid== recordTypeId &&
                 paymentrecord.Employee_Rate__c!=null && paymentrecord.Category_lookup__c!=null && !Paymentrecordhelper.IsFicaPayments ){
                     paymentIdsSet.add(paymentrecord.id);    
                 }          
            }
           
           /* if(OpportunityPaymentamountmap.size()>0)
                Paymentrecordhelper.checkPaymentAmountTotal(OpportunityPaymentamountmap);   */    
            if(paymentNameChangeids.size()>0)
                paymentrecordhelper.PopulateGlobalSearchName(paymentNameChangeids);   
            if(opportunitiesids.size()>0)
                Paymentrecordhelper.changeStage(opportunitiesids,'Paid'); 
            if(writeoffOpportunitiesIds.size()>0)
                 Paymentrecordhelper.changeStage(writeoffOpportunitiesIds,'Writeoff');    
            if(lineItemPaymentIds.size()>0)
                PaymentTriggerLineItemHandler.updatePayments(lineItemPaymentIds); 
           if(claimUpdatePayments.size()>0)
               Paymentrecordhelper.ClaimUpdate(claimUpdatePayments);
           if(paymentIdsSet.size()>0)
               Paymentrecordhelper.CreateCaseActionsForFICAPayments(paymentIdsSet);            
                     
        }
        
        //FOR DELETE
        if(trigger.isbefore && trigger.isDelete){
            set<id> paymentIDs = new set<id>();
             Id VendorInvoicerecordTypeId =Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Vendor_Invoice').getRecordTypeId(); 
            for(Payment__c  payment : trigger.old){
                if(payment.Paid__c==true && payment.Employee_Rate__c!=null && payment.recordtypeid== VendorInvoicerecordTypeId
                    && !paymentrecordhelper.IsPaymentDelete ){
                    paymentIDs.add(payment.id);    
                }
            
            }
            if(paymentIDs.size()>0)
                Paymentrecordhelper.DeleteCaseActionsOfFICA(paymentIDs);
        }
        TriggerFactory.createAndExecuteHandler(PaymentTriggerHandler.class);
    }
}