trigger KinshipCaseTrigger on Kinship_Case__c (before insert, after insert, before update, after update, after delete) {
    TriggerActivation__c qliTrigger = TriggerActivation__c.getAll().get('KinshipCaseTrigger');
    if (qliTrigger != null && qliTrigger.isActive__c){
        TriggerFactory.createAndExecuteHandler(KinshipCaseTriggerHandler.class);
        //FOR CREATING RANDOM NUMBER FOR LOCALITY CHILD FIELD
         list<Kinship_Case__c> casesToBeUpdated = new list<Kinship_Case__c>();
         if(trigger.isAfter && trigger.isInsert){
             for(Kinship_Case__c caserec : trigger.new){
                 if(!KinshipCaseTriggerHelper.isTrgRun && caserec.InActive__c==false)
                     casesToBeUpdated.add(caserec);                         
             }
             if(casesToBeUpdated.size()>0)
                 KinshipCaseTriggerHelper.populateChildIdentifier(casesToBeUpdated);
         }
        //KIN 1431
        if(trigger.isupdate && trigger.isAfter){
            set<id> caseIds = new set<id>();
            for(Kinship_Case__c caserec : trigger.new){
                System.debug('name change:'+caserec.Name+trigger.oldmap.get(caserec.id).Name);
                if(caserec.Name!=trigger.oldmap.get(caserec.id).Name && caserec.Name!=null && trigger.oldmap.get(caserec.id).Name!=null){
                    caseIds.add(caserec.id);
                }
            }
            if(caseIds.size()>0)
                 KinshipCaseTriggerHelper.claimsnSWAUpdate(caseIds);
        }
       /*  if(trigger.isupdate && trigger.isafter){
             set<id> caseIds = new set<id>();
             for(Kinship_Case__c caserec : trigger.new){
                 if(!KinshipCaseTriggerHelper.isIVErun && caserec.Title_IVE_Eligibility__c!=trigger.oldmap.get(caserec.id).Title_IVE_Eligibility__c){
                     caseIds.add(caserec.id);  
                 }                           
             }
             if(caseIds.size()>0)
                 KinshipCaseTriggerHelper.createNotification(caseIds);
         } */
    
    }
}