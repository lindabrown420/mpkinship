trigger trgGenerateAutoNumber on Opportunity (after insert) {

    Id CARecTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Case Action').getRecordTypeId();
     
    if(!trigger.IsUndelete){
        integer maxOrderNum;
        integer maxOrderNumCA;
        Purchase_Order_Last_ID__c sold;
        Case_Action_Last_ID__c soldCA;
           try{
               if(Schema.sObjectType.Purchase_Order_Last_ID__c.fields.Name.isAccessible()) 
                   sold = [select id,name,Last_Id_No__c from Purchase_Order_Last_ID__c where name = 'PO-ID' limit 1];
               maxOrderNum =  (Integer)sold.Last_Id_No__c;
               
           }
           Catch(Exception e) {
               if(sold==null)    sold = new Purchase_Order_Last_ID__c();
               sold.name = 'PO-ID';
               sold.Last_Id_No__c = 0;
               insert sold;
               maxOrderNum =  0;
               
              
           }
           try{
               if(Schema.sObjectType.Case_Action_Last_ID__c.fields.Name.isAccessible()) 
                   soldCA = [select id,name,Last_Id_No__c from Case_Action_Last_ID__c where name = 'CA-ID' limit 1];
               maxOrderNumCA =  (Integer)soldCA.Last_Id_No__c;
           }
           catch(Exception e){
                if(soldCA==null)    soldCA = new Case_Action_Last_ID__c();
               soldCA.name = 'CA-ID';
               soldCA.Last_Id_No__c = 0;
               insert soldCA;
               maxOrderNumCA =  0;
           }
           List<Opportunity> Pos = new List<Opportunity>();
           List<Opportunity> PosCA = new List<Opportunity>();
           
           Integer maxResultNo = 0;
           Integer maxResultNoCA = 0;
           if(Schema.sObjectType.Opportunity.fields.Name.isAccessible()) {
               List<Opportunity> oppList= [select id,Purchase_Order_ID__c,recordTypeId  from Opportunity where id in : trigger.newmap.keyset()];
               for(Opportunity op:oppList){
                  if(op.recordTypeId == CARecTypeId)    PosCA.add(op);
                  else{
                      Pos.add(op);
                  }
               }
               System.debug('PosCA:'+PosCA.size());
               System.debug('Pos:'+Pos.size());
               List<Opportunity> maxRecord = new List<Opportunity>();
               maxRecord = [Select id, name,Purchase_Order_ID__c  from  Opportunity where Purchase_Order_ID__c  LIKE 'PO-%' order by createddate desc limit 2];
               //System.debug('maxRecord'+maxRecord[1]);
               if(maxRecord.size()>1 && maxRecord[1].Purchase_Order_ID__c!= NULL && maxRecord[1].Purchase_Order_ID__c!= '' && maxRecord[1].Purchase_Order_ID__c.contains('PO-')) {
                   maxResultNo = (Integer.valueOf((maxRecord[1].Purchase_Order_ID__c).substring(maxRecord[1].Purchase_Order_ID__c.length()-3)));
               }
               
               List<Opportunity> maxRecordCA = new List<Opportunity>();
               maxRecordCA = [Select id, name,Purchase_Order_ID__c  from  Opportunity where Purchase_Order_ID__c  LIKE 'CA-%' order by createddate desc limit 2];
               System.debug('maxRecordCA'+maxRecordCA);
               if(maxRecordCA.size()>1 && maxRecordCA[1].Purchase_Order_ID__c!= NULL && maxRecordCA[1].Purchase_Order_ID__c!= '' && maxRecordCA[1].Purchase_Order_ID__c.contains('CA-')) {
                   maxResultNoCA = (Integer.valueOf((maxRecordCA[1].Purchase_Order_ID__c).substring(maxRecordCA[1].Purchase_Order_ID__c.length()-3)));
               }
           }
           
           if(maxResultNo >maxOrderNum ) {
               maxOrderNum = maxResultNo ;
           }
           
           System.debug('maxResultNoCA '+maxResultNoCA );
           System.debug('maxOrderNumCA '+maxOrderNumCA );
           if(maxResultNoCA >maxOrderNumCA ) {
           System.debug('maxResultNoCA >maxOrderNumCA');
               maxOrderNumCA = maxResultNoCA ;
           }
           
           for(Opportunity  so : Pos){
                string maxnum='';
                if(maxOrderNum<9){
                    maxnum='0'+(maxOrderNum+1);   
                }
                if(maxOrderNum>=9){                               
                    maxnum=''+(maxOrderNum+1);
                }
                
                so.Purchase_Order_ID__c = 'PO-'+maxnum;
                maxOrderNum = maxOrderNum +1;
           }
           for(Opportunity  so : PosCA){
                string maxnumCA='';
                if(maxOrderNumCA<9){
                    maxnumCA='0'+(maxOrderNumCA+1);   
                }
                if(maxOrderNumCA>=9){                               
                        maxnumCA=''+(maxOrderNumCA+1);
                }
                
                so.Purchase_Order_ID__c = 'CA-'+maxnumCA;
                maxOrderNumCA = maxOrderNumCA +1;
           }
           
           sold.Last_Id_No__c = (decimal)maxOrderNum;
               if(Schema.sObjectType.Purchase_Order_Last_ID__c.fields.name.isUpdateable())
                   update sold;
           
           if(Pos.size()>0){
               if(Schema.sObjectType.Opportunity.fields.Purchase_Order_ID__c.isUpdateable())
                   update Pos;
           }
           
           soldCA.Last_Id_No__c = (decimal)maxOrderNumCA;
               if(Schema.sObjectType.Case_Action_Last_ID__c.fields.name.isUpdateable())
                   update soldCA;
           
           if(PosCA.size()>0){
               if(Schema.sObjectType.Opportunity.fields.Purchase_Order_ID__c.isUpdateable())
                   update PosCA;
           }
       
    }
}