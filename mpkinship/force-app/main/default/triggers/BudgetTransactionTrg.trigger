trigger BudgetTransactionTrg on Budget_Transactions__c (after insert) {
TriggerActivation__c IsTriggerActivate = TriggerActivation__c.getAll().get('Budget Transaction Trigger'); 
    
    if (IsTriggerActivate != null && IsTriggerActivate.isActive__c){ 
        // Formated check name work
        if(trigger.isInsert && trigger.isAfter){
            List<Budget_Transactions__c> checkNewNameLst = new List<Budget_Transactions__c>();
            Budget_Transactions__c chknew  = new Budget_Transactions__c();
            for(Budget_Transactions__c chk : trigger.new){
                chknew.Id = chk.id;
                chknew.Name = chk.Name__c + ' ' + FactoryCls.formatCurr(chk.Amount__c);
                
                checkNewNameLst.add(chknew);
                chknew  = new Budget_Transactions__c();
            } 
            update checkNewNameLst;
        }
    }
}