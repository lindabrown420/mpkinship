trigger AssessmentTrigger on Assessment__c (before insert,before update) {
    Set<Id> sortedAssessmentId = new Set<Id>();
    Set<Id> sortedCaseId = new Set<Id>();
    
    //Taking Assessment and Case Id of Inserted or Updated Record
    for (Assessment__c assessRecord : trigger.new) {
        if (assessRecord.Record_Type_Name__c == 'CSA') {
            sortedAssessmentId.add(assessRecord.Id);
            sortedCaseId.add(assessRecord.Case__c);
        }                
    }
    List<Assessment__c> listsWithActive=[SELECT ID, Case__c FROM Assessment__c 
                                        WHERE Stages__c ='Active' AND Case__c IN:sortedCaseId AND ID NOT IN:sortedAssessmentId];
    List<Assessment__c> listTobeUpdated= new List<Assessment__c>();
        for (Assessment__c assessRecord1 : trigger.new) {
            if (assessRecord1.Stages__c =='Active' && assessRecord1.Record_Type_Name__c == 'CSA') {
                for (Assessment__c recordsWithActive : listsWithActive) {
                    if (recordsWithActive.Case__c == assessRecord1.Case__c) {
                        Assessment__c recordTobeUpdated = new Assessment__c();
                        recordTobeUpdated.Id=recordsWithActive.Id;
                        recordTobeUpdated.Stages__c='Inactive';
                        listTobeUpdated.add(recordTobeUpdated);
                    }                                
                }            
            }                
        }
        update listTobeUpdated;  
}
    
    