trigger UpdateCSACoordinatorSign on Opportunity (before insert, before update) {
    
    Id currentUserId= UserInfo.getUserId();
    List<User> currentUser= [Select id,name,Signature__c from User where Id=:currentUserId];
    
    if(trigger.IsInsert){
        for(opportunity opprecord : trigger.new){
            if((opprecord.Second_Approver_Id__c==null && opprecord.First_Approver_Id__c==null) && opprecord.stagename=='Approved'){
                opprecord.Signature__c=currentUser[0].Signature__c;
            }
        }
    }
    
    if(trigger.isUpdate){
            List<Opportunity> opportunities = new List<Opportunity>();
            List<Opportunity> oppCSA = new List<Opportunity>();
            List<Id> UserIds= new List<Id>();
            for(opportunity opprecord : trigger.new){
                
                if(opprecord.First_Approver_Id__c!=null && opprecord.First_Approver_Id__c!=trigger.oldmap.get(opprecord.id).First_Approver_Id__c){ opportunities.add(opprecord);   UserIds.add(opprecord.First_Approver_Id__c);      }
                if(opprecord.Second_Approver_Id__c!=null && opprecord.Second_Approver_Id__c!=trigger.oldmap.get(opprecord.id).Second_Approver_Id__c){ oppCSA.add(opprecord);   UserIds.add(opprecord.Second_Approver_Id__c);      }

            }
        Map<Id,User> usersmap= new Map<Id,User> ([Select id,name,Signature__c from User where Id=:UserIds]);
        
        for(Opportunity opprecord : oppCSA){
            if(opprecord.Second_Approver_Id__c!=null && usersmap.get(opprecord.Second_Approver_Id__c)!=null){
                opprecord.Signature__c=currentUser[0].Signature__c;
            }
            
            
            
        }
        for(opportunity opprecord : trigger.new){
            System.debug('in update csa:'+opprecord.Second_Approver_Id__c + '::'+opprecord.First_Approver_Id__c +'::'+opprecord.stagename );
            if((opprecord.Second_Approver_Id__c==null && opprecord.First_Approver_Id__c==null) && opprecord.stagename=='Approved'){
                System.debug('currentUser[0].Signature__c'+currentUser[0].Signature__c);
                opprecord.Signature__c=currentUser[0].Signature__c;
            }
        }
        for(Opportunity opprecord : opportunities){
            if(opprecord.First_Approver_Id__c!=null && usersmap.get(opprecord.First_Approver_Id__c)!=null){
                opprecord.Case_Supervisor_Signature__c=usersmap.get(opprecord.First_Approver_Id__c).Signature__c;
            }
            
        }
    }
    //KIN-628
    if((trigger.isUpdate || trigger.isInsert) && !Purchaseordertrghelper.isROCWelfare && (CaseActionPOController.IsCaseActionScreen || CaseActionPOEditController.IsCaseActionEditScreen)){
    //List<Opportunity> POList= [select id,Purchase_Order_ID__c, Category__c, Category__r.Category_Type__c, Kinship_Case__c,Special_Welfare_Account__c, recordTypeId  from Opportunity where id in : trigger.newmap.keyset()];
    List<Opportunity> CAList = new List<Opportunity>();
    
    List<Id> catIds= new List<Id>();
    List<Id> caseIds= new List<Id>();
    Id CARecTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Case Action').getRecordTypeId();
    for(Opportunity opprecord : Trigger.New){
        if(opprecord.recordTypeId == CARecTypeId ){
            catIds.add(opprecord.Category__c);
        }
         
    }
    
     system.debug('**VAl of catids'+ catIds);   
    
    Map<Id,Category__c> catMap= new Map<Id,Category__c>([Select id, Name,Category_Type__c from Category__c where Id=:catIds]);
    for(opportunity opprecord : Trigger.New){
        
        if( (opprecord.recordTypeId == CARecTypeId ))    {
            if(catMap.get(opprecord.Category__c)!=null){
                if(catMap.get(opprecord.Category__c).Category_Type__c == 'Special Welfare'){
                    CAList.add(opprecord);
                    caseIds.add(opprecord.Kinship_Case__c);
                }
            }
            
        }
        

    }
    system.debug('**VAl of caseIds'+ caseIds);
    
    Map<Id,List<Special_Welfare_Account__c>> mapCase= new Map<Id,List<Special_Welfare_Account__c>>();
    if(caseIds.size()>0){
        for(Kinship_Case__c cases : [Select Id, name,(Select Id,Name,Date_Closed__c,Current_Balance__c from Special_Welfare_Associations__r where Account_Closed__c=false order by createddate desc) from  Kinship_Case__c where Id=:caseIds]){
            if(cases.Special_Welfare_Associations__r.size()>0)
            mapCase.put(cases.Id,cases.Special_Welfare_Associations__r);
        }
    }
    system.debug('**VAl of mapcase'+ mapCase);
    
    for(opportunity opprecord : CAList){
        if(opprecord.Kinship_Case__c!=null){
        system.debug('***Val of case'+ opprecord.Kinship_Case__c);
            if(mapCase.size()>0){
                if(mapCase.containskey(opprecord.Kinship_Case__c)!=null){    
               
                    opprecord.Special_Welfare_Account__c=mapCase.get(opprecord.Kinship_Case__c)[0].Id;
                    if(mapCase.get(opprecord.Kinship_Case__c)[0].Id != null && opprecord.Amount!=Null && mapCase.get(opprecord.Kinship_Case__c)[0].Current_Balance__c!=NULL){
                        if( opprecord.Amount > mapCase.get(opprecord.Kinship_Case__c)[0].Current_Balance__c){
                            opprecord.addError('Amount should be less than or Equal Current Balance of Special Welfare Account. Current Balance of Special Welfare Account is :'+mapCase.get(opprecord.Kinship_Case__c)[0].Current_Balance__c);
                        }
                    }
                    
              
                 }
           }
            else if(mapCase.size()==0){
                system.debug('**Enterd in else part');
                opprecord.addError('Please select a case with Active Special Welfare Account');
            }
            
        }
    }
        
    
        
    }
    
}