trigger MergedCheckTrigger on Check_Run__c (after update) {
    TriggerActivation__c IsTriggerActivate = TriggerActivation__c.getAll().get('MergedCheck'); 
    
    if (IsTriggerActivate != null && IsTriggerActivate.isActive__c){ 
        if(trigger.isUpdate && trigger.isAfter){
            set<id> checkRunIds = new set<id>();
            for(Check_Run__c checkRun : trigger.new){
                //UPDATE CHECK ISSUE DATE AND PAYMENT DATE 
                if(!CheckRunHelper.IsRun && checkRun.Check_Date__c!=trigger.oldmap.get(checkRun.id).Check_Date__c  ){
                    checkRunIds.add(checkRun.id);    
                }
            }
            
            if(checkRunIds.size()>0)
                CheckRunHelper.ChangeInvoicePaymentDates(checkRunIds);
        }
        
    }    
}