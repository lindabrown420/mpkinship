trigger AccountTrigger on Account  (before insert, after insert, before update, after update, after delete) {
    TriggerActivation__c qliTrigger = TriggerActivation__c.getAll().get('AccountTrigger');
    if(trigger.isInsert && trigger.isBefore){
        for(Account  Acc: trigger.New ){
            Acc.BillingCountry = 'US';
            Acc.ShippingCountry = 'US';
        }
    }
    if (qliTrigger != null && qliTrigger.isActive__c){
        TriggerFactory.createAndExecuteHandler(AccountTriggerHandler.class);
    }
}