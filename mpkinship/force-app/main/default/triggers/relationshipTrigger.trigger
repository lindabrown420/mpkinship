trigger relationshipTrigger on Relationship__c (after insert, after delete) {
	List<Relationship__c> relationshipList = new List<Relationship__c>();
    //Map<Id,Id> contIdN_RelatedContIdMap = new Map<Id,Id>();
    
    if(trigger.isInsert){
        for(Relationship__c rel: trigger.new){
           Relationship__c r = new Relationship__c();
           if(rel.Is_Reciprocal_Relationship__c == false){           
               r.Contact__c = rel.Related_Contact__c;
               r.Related_Contact__c = rel.Contact__c;
               r.Status__c = rel.Status__c;
               r.Is_Reciprocal_Relationship__c = true;
               r.Reciprocal_Relationship__c = rel.id;           
               if(rel.Type__c == 'Parent' || rel.Type__c == 'Father' || rel.Type__c == 'Mother'){
                   r.Type__c = 'Child';
               }
               else if(rel.Type__c == 'Child' || rel.Type__c == 'Son' || rel.Type__c == 'Daughter'){
                   r.Type__c = 'Parent';
               }
               else if(rel.Type__c == 'Grandparent' || rel.Type__c == 'Grandfather' || rel.Type__c == 'Grandmother'){
                   r.Type__c = 'Grandchild';
               }
               else if(rel.Type__c == 'Grandchild' || rel.Type__c == 'Grandson' || rel.Type__c == 'Granddaughter'){
                   r.Type__c = 'Grandparent';
               }
               else if(rel.Type__c == 'Employer'){
                   r.Type__c = 'Employee';
               }
               else if(rel.Type__c == 'Employee'){
                   r.Type__c = 'Employer';
               }
               else if(rel.Type__c == 'Husband'){
                   r.Type__c = 'Wife';
               }
               else if(rel.Type__c == 'Wife'){
                   r.Type__c = 'Husband';
               }
               else{
                   r.Type__c = rel.Type__c;
               }           
            } 
            else{
                r.Id = rel.Reciprocal_Relationship__c;
                r.Reciprocal_Relationship__c = rel.Id;
            }
            relationshipList.add(r);
        }
        if(relationshipList.size() > 0){
            upsert relationshipList;
        }
    }
    
    if(trigger.isDelete){
        List<Id> relIdToDelete = new List<Id>();
        for(Relationship__c rel: trigger.old){
            if(rel.Reciprocal_Relationship__c != null){
                relIdToDelete.add(rel.Reciprocal_Relationship__c);
            }
        }
        if(relIdToDelete.size() > 0){
            delete [Select id from Relationship__c where Id In: relIdToDelete];
        }
    }
    
}