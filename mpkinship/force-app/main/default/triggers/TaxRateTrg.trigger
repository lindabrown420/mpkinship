trigger TaxRateTrg on Tax_Rates__c (before insert,before update) {
    TriggerActivation__c IsTriggerActivate = TriggerActivation__c.getAll().get('TaxRate'); 
    if (IsTriggerActivate != null && IsTriggerActivate.isActive__c){ 
        list<Tax_Rates__c> taxRatesTobeInserted = new list<Tax_Rates__c>();
        if(trigger.isInsert && trigger.isBefore){
            for(Tax_Rates__c tr : trigger.new){
                if(tr.Active__c==true && !TaxRateTrgHelper.IsRun){
                    taxRatesTobeInserted.add(tr);    
                }   
            }
            if(taxRatesTobeInserted.size()>0)
            TaxRateTrgHelper.CheckActiveRate(taxRatesTobeInserted,null); 
        
        }  
        if(trigger.isUpdate && trigger.isBefore){
            list<Tax_Rates__c> taxRatesToBeUpdated = new list<Tax_Rates__c>();
            set<id> taxrateIds = new set<id>();
            Id FICArecTypeId = Schema.SObjectType.Tax_Rates__c.getRecordTypeInfosByDeveloperName().get('FICA').getRecordTypeId();
            for(Tax_Rates__c tr : trigger.new){
                if(!TaxRateTrgHelper.isRunUpdate && tr.Active__c==true && (tr.Active__c!=trigger.oldmap.get(tr.id).Active__c 
                || tr.recordtypeid!=trigger.oldmap.get(tr.id).recordtypeid || (tr.recordtypeid==FICArecTypeId && 
                tr.Type__c!=trigger.oldmap.get(tr.id).Type__c))){
                    taxRatesToBeUpdated.add(tr);
                    taxrateIds.add(tr.id);
                }                
            }
            if(taxRatesToBeUpdated.size()>0 && taxrateIds.size()>0)
            TaxRateTrgHelper.CheckActiveRate(taxRatesToBeUpdated,taxrateIds);  
        }
        
        
            
    }

}