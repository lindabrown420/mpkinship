trigger InvoiceLineItemTrg on Payment_Line_Item__c (After update) {
    if(Trigger.IsAfter && Trigger.IsUpdate && !PaymentAndLineItemCtrClone.IsReset){
        List<Id> PayIds= new List<Id>();
        List<Payment__c> paytoupdate= new List<Payment__c>();
        for(Payment_Line_Item__c line:Trigger.New){
            if(line.LineItem_Amount_Billed__c==0 || line.Quantity_Billed__c==0){
                PayIds.add(line.Invoice__c);
            }
        }
        System.debug('PayIds:'+PayIds.size());
        Map<id, Payment__c> payMap= new Map<id,Payment__c> ([Select id,name,Invoice_Stage__c,(select id,name,LineItem_Amount_Billed__c,Quantity_Billed__c from Invoice_Line_Items__r) from Payment__c where id=:PayIds]);
        decimal totalAmount =0;
        decimal totalQuantity=0;
        for(Payment__c paym: payMap.values()){
            for(Payment_Line_Item__c payitem: paym.Invoice_Line_Items__r){
                if(payitem.LineItem_Amount_Billed__c!=null)
                totalAmount=totalAmount+ payitem.LineItem_Amount_Billed__c;
                if(payitem.Quantity_Billed__c!=null)
                totalQuantity=totalQuantity+ payitem.Quantity_Billed__c;
            }
            System.debug('totalAmount'+totalAmount);
            System.debug('totalQuantity'+totalQuantity);
            if(totalAmount==0 || totalQuantity==0){
                paym.Invoice_Stage__c='Write off';
                paym.Written_Off__c=true;
                paytoupdate.add(paym);
            }
        }
        if(paytoupdate.size()>0){
            update paytoupdate;
        }
    }
}