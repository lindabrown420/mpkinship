trigger TransactionJournalTrg on Transaction_Journal__c (after insert, before delete) {
 TriggerActivation__c IsTriggerActivate = TriggerActivation__c.getAll().get('Transaction Journal Trigger'); 
    
    if (IsTriggerActivate != null && IsTriggerActivate.isActive__c){ 
        // Formated check name work
        if(trigger.isInsert && trigger.isAfter){
            List<Transaction_Journal__c> checkNewNameLst = new List<Transaction_Journal__c>();
            Transaction_Journal__c chknew  = new Transaction_Journal__c();
            for(Transaction_Journal__c chk : trigger.new){
                chknew.Id = chk.id;

			    chknew.Name = chk.Name__c + ' ' + FactoryCls.formatCurr(chk.Total_Amount__c);                
                checkNewNameLst.add(chknew);
                 chknew  = new Transaction_Journal__c();
            } 
            update checkNewNameLst;
        }
        
        TriggerFactory.createAndExecuteHandler(TransactionJournalTriggerHandler.class);
    }
}