trigger BudgetTrg on Budgets__c (after insert, after update) {
TriggerActivation__c IsTriggerActivate = TriggerActivation__c.getAll().get('Budget Trigger'); 
    
    if (IsTriggerActivate != null && IsTriggerActivate.isActive__c){ 
        // Formated check name work
        if((trigger.isInsert || trigger.isUpdate) && trigger.isAfter){
            BudgetTriggerHelper.updateBudgetName(trigger.new);
        }
    }
}