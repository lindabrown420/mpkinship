trigger ChartOfAccountTrigger on Chart_of_Account__c (after insert, after update, before update, before insert) {
    TriggerActivation__c qliTrigger = TriggerActivation__c.getAll().get('ChartOfAccountTrigger');
    if (qliTrigger != null && qliTrigger.isActive__c){
        TriggerFactory.createAndExecuteHandler(ChartOfAccountTriggerHandler.class);
    }
}