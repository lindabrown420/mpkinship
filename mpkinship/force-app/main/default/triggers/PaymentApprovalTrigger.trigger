trigger PaymentApprovalTrigger on Payment_Approval__c (before insert, after insert, before update, after update, before delete) {
    TriggerActivation__c qliTrigger = TriggerActivation__c.getAll().get('PaymentApprovalTrigger');
    if (qliTrigger != null && qliTrigger.isActive__c){
        TriggerFactory.createAndExecuteHandler(PaymentApprovalTriggerHandler.class);
    }
}