trigger ServicePlanTrigger on IFSP__c (before update,before insert) {
    // query from CSA agenda where start date = Fapt Date
    // if found a record, If there is an existing CSA Agenda with the same date as the FAPT Date, add the IFSP as an Agenda Item to the CSA Agenda.
    //  If there is no CSA Agenda, create a new CSA Agenda with that FAPT Date and include the case and IFSP as an agenda item.
    
	if(Trigger.isBefore && (Trigger.isUpdate || Trigger.isInsert))
    {
    	List<CSA_Meeting_Agenda__c> lstCSA = [Select ID, Name, Meeting_Date__c, Team__c From CSA_Meeting_Agenda__c];
    	CSA_Meeting_Agenda__c csa = new CSA_Meeting_Agenda__c();     
	    List<CSA_Meeting_Agenda_Item__c> lstCSAItem = new List<CSA_Meeting_Agenda_Item__c>();
     	CSA_Meeting_Agenda_Item__c csaItem  = new CSA_Meeting_Agenda_Item__c();        
     	List<IFSP__c> lstIFSP = new List<IFSP__c>();
     
	    Boolean isCSAExists = false;
        Id csaId;
     	Id iFSPrecordTypeId =  Schema.SObjectType.IFSP__c.getRecordTypeInfosByDeveloperName().get('Individual_and_Family_Services_Plans_IFSP').getRecordTypeId(); 
         
         Set<ID> TeamIds = new Set<Id>();
         for(IFSP__c ServicePlan:trigger.New)
         {
           
             if(iFSPrecordTypeId == ServicePlan.RecordTypeId)
             {
                 if((Trigger.isUpdate && Trigger.oldMap.get(ServicePlan.Id).FAPT_Date__c == null 
                         && ServicePlan.FAPT_Date__c != null && ServicePlan.CSA_Team__c != null) || 
                    (Trigger.isInsert && ServicePlan.FAPT_Date__c != null && ServicePlan.CSA_Team__c != null))
                 {
                     ServicePlan.Status__c = 'In Review';
                     for(CSA_Meeting_Agenda__c c:lstCSA )
                     {
                         if(c.Meeting_Date__c == ServicePlan.FAPT_Date__c && c.Team__c == ServicePlan.CSA_Team__c)
                         {
                             csaId = c.Id;
                             isCSAExists = true;
                         }
                     }
                     
                     if(!isCSAExists)
                     {
                         // Work for new csa agenda
                         isCSAExists = false;
                         
                         csa.Name = ServicePlan.Name + ' Agenda';
                         csa.Meeting_Date__c = ServicePlan.FAPT_Date__c ;
                         csa.Meeting_Start3_Time__c = '8:00:00.000';
                         csa.Meeting_End_Time2__c = '11:00:00.000';
                         csa.Meeting_Status__c = 'Pending';
                         csa.Team__c = ServicePlan.CSA_Team__c;
                         TeamIds.add(ServicePlan.CSA_Team__c);    
                         lstCSA.add(csa);
                         
                         lstIFSP.add(ServicePlan);
                     }
                     else
                     {
                         // Work for existing csa agenda
                         
                         csaItem = new CSA_Meeting_Agenda_Item__c();
                         csaItem.CSA_Meeting_Agenda__c = csaId;
                         csaItem.Case_KNP__c = ServicePlan.Case__c ;
                         csaItem.IFSP__c = ServicePlan.Id;
                         csaItem.Review__c = 'Initial';
                         //csaItem.Team__c = ServicePlan.CSA_Team__c;
                         
                         lstCSAItem.add(csaItem);
                     }
                 }
                 
             }
         }
        
        if(lstCSA.size() > 0)
        {
            List<Database.upsertResult> uResults = Database.upsert(lstCSA ,false);
            for(Database.upsertResult result:uResults) 
            {
                if (result.isSuccess() && result.isCreated()) 
                {
                    for(IFSP__c ifsp:lstIFSP) 
                    {
                        csaItem = new CSA_Meeting_Agenda_Item__c();
                        csaItem.CSA_Meeting_Agenda__c = result.getId();
                        csaItem.Case_KNP__c = ifsp.Case__c ;
                        csaItem.IFSP__c = ifsp.Id;
                        csaItem.Review__c = 'Initial';
                        
                        lstCSAItem.add(csaItem);
                    }
                }              
            }
            
            try{
                List<Team_Member__c> TeamMembers = [Select Id,Contact__c,Contact_Name__c, Contact_Email__c, CSA_Meeting_Notification__c  
                                                    from Team_Member__c
                                                    where CSA_Team__c IN : TeamIds];
                List<CSA_Meeting_Members__c> CMlist = new List<CSA_Meeting_Members__c>();
                CSA_Meeting_Members__c CM = new CSA_Meeting_Members__c();
                
                for(CSA_Meeting_Agenda__c CA: lstCSA)
                {
                    for(Team_Member__c TM: TeamMembers ){
                        
                        CM.Contact__c = TM.Contact__c;
                        CM.CSA_Meeting_Agenda__c = CA.Id;
                        CMlist.add(CM);
                        CM = new CSA_Meeting_Members__c();
                    }
                }     
                if(CMlist.size() > 0)
                { 
                    insert CMlist;
                }  
            }catch(Exception e){
                system.debug( e.getmessage() + 'Exception e >>>' );
            }
        }
        
        if(lstCSAItem.size() > 0)
        {
            Database.insert(lstCSAItem);
        }
        
    } 
   
}