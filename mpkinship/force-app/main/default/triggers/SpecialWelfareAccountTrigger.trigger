trigger SpecialWelfareAccountTrigger on Special_Welfare_Account__c (before update, after insert, after update) {  
    
   if(Trigger.isBefore && Trigger.isUpdate && !Paymentrecordhelper.isSWAUpdate && !Paymentrecordhelper.isSWADelete) {
        List<id> SWAIds = new List<id>();
        for(Special_Welfare_Account__c SWA: Trigger.New){          
                SWAIds.add(SWA.id);
        }
        try{
            List<AggregateResult>  rece = [SELECT RecordType.Name rtname, Special_Welfare_Account__c, sum(Payment_Amount__c) amt, Payment_Source__c pstype 
                                           FROM Payment__c WHERE Special_Welfare_Account__c IN : SWAIds and Category_lookup__r.Category_Type__c='Special Welfare'
                                           GROUP BY RecordType.Name, Special_Welfare_Account__c, Payment_Source__c ];  
            system.debug('**VAl of rece'+ rece.size());
            for(Special_Welfare_Account__c SWA: Trigger.New){
                SWA.Total_Receipts__c = 0.0;
                SWA.Total_Payments__c = 0.0;
                SWA.Child_Support__c= 0.0;
                SWA.Child_Support_Payments__c= 0.0;
                SWA.SSA__c= 0.0;
                SWA.SSA_SSI_Payments__c= 0.0;
                
                for(AggregateResult re: rece) {
                    system.debug(re.get('rtname') + ' re.get(RecordType.Name)????');
                    //system.debug(re.get('Name') + ' re.get(RecordType.Name)????');
                    if(SWA.Id == re.get('Special_Welfare_Account__c')){
                        if(re.get('rtname') == 'Vendor Refund' || re.get('rtname') == 'Receipt')
                        {
                            SWA.Total_Receipts__c += (Decimal)re.get('amt');
                            
                            if(re.get('pstype') == '5')
                            {
                                SWA.Child_Support__c+= (Decimal)re.get('amt');
                            }
                            else if(re.get('pstype') == '6')
                            {
                                SWA.SSA__c+= (Decimal)re.get('amt');
                            }
                            
                        }
                        
                        else if(re.get('rtname') == 'Vendor Invoice' || re.get('rtname')=='Special Welfare Reimbursement')
                        {
                            SWA.Total_Payments__c += (Decimal)re.get('amt'); 
                            
                            if(re.get('pstype') == '5')
                            {
                                SWA.Child_Support_Payments__c+= (Decimal)re.get('amt');
                            }
                            else if(re.get('pstype') == '6')
                            {
                                SWA.SSA_SSI_Payments__c+= (Decimal)re.get('amt');
                            }
                        }
                        
                    }                                   
                }    
            }
            
        }Catch(Exception e){}
        
    } 
    // KIN-628
    if(Trigger.isAfter && (Trigger.IsUpdate || Trigger.isInsert)){
        List<Id> caseIds= new List<Id>();
        for(Special_Welfare_Account__c SWA: Trigger.New){
            caseIds.add(SWA.Case_KNP__c);
        }
        Map<Id,Kinship_Case__c> existingCase = new Map<Id, Kinship_Case__c>([Select Id, Name,(Select Id,Name from Special_Welfare_Associations__r WHERE Account_Closed__c = false) From Kinship_Case__c Where Id =: caseIds]); 
        for(Special_Welfare_Account__c SWA: Trigger.New){
            
            if(existingCase.get(SWA.Case_KNP__c) != null && !SWA.Account_Closed__c){
                System.debug('SWA size 1 : '+existingCase.get(SWA.Case_KNP__c).Special_Welfare_Associations__r.size());
                if(existingCase.get(SWA.Case_KNP__c).Special_Welfare_Associations__r.size() > 1){
                    SWA.adderror('There can be only one Special Welfare Account on a case');
                }
            }
        }
    }
}