Trigger CampaignTrigger on Campaign (before insert, after insert, before update, after update, before delete) {
    TriggerActivation__c qliTrigger = TriggerActivation__c.getAll().get('CampaignTrigger');
    
    List<Campaign> eventCampaign = [Select Id,Auto_Number__c from Campaign Where RecordType.Name = 'Events' AND Auto_Number__c != null Order By Auto_Number__c Desc Limit 1];
    List<Campaign> trpCampaign = [Select Id,Auto_Number__c from Campaign Where RecordType.Name = 'Tax Reporting Period' AND Auto_Number__c != null Order By Auto_Number__c Desc Limit 1];
	
    integer eventAutoNumber = 0;
    integer trpAutoNumber = 0;

    if(eventCampaign.size() == 1){
        eventAutoNumber = integer.valueof(eventCampaign[0].Auto_Number__c);
    }
    if(trpCampaign.size() == 1){
        trpAutoNumber = integer.valueof(trpCampaign[0].Auto_Number__c);
    }
    
    if (qliTrigger != null && qliTrigger.isActive__c){
        //ADDED FOR NAME OF LASER REPORTING PERIOD RECORDTYPE
        if(trigger.isbefore && (trigger.isUpdate || trigger.isInsert)){
            Id objectRecordTypeId = Schema.SObjectType.campaign.getRecordTypeInfosByDeveloperName().get('LASER_Reporting_Period').getRecordTypeId();
            for(campaign camprecord : trigger.new){
                if(camprecord.recordtypeid==objectRecordTypeId && ((trigger.isinsert  && camprecord.EndDate!=null) ||
                   (trigger.isupdate && camprecord.enddate!=null && (camprecord.enddate.month()!=trigger.oldmap.get(camprecord.id).enddate.month()
                    || camprecord.enddate.year()!=trigger.oldmap.get(camprecord.id).enddate.year())))){
                    datetime campdatetime= datetime.newInstance(camprecord.EndDate.year(), camprecord.EndDate.month(), camprecord.EndDate.day());
                    string campname= string.valueOf(campdatetime.format('MM-yyyy'));
                    camprecord.name= campname;
                }
                if(trigger.isInsert){
                    if(Schema.SObjectType.Campaign.getRecordTypeInfosById().get(camprecord.RecordTypeId).getName() == 'Events'){
                        eventAutoNumber += 1;
                        camprecord.Auto_Number__c = eventAutoNumber;
                        camprecord.Name = 'Events - ' + eventAutoNumber;
                    }
                    else if(Schema.SObjectType.Campaign.getRecordTypeInfosById().get(camprecord.RecordTypeId).getName() == 'Tax Reporting Period'){
                        trpAutoNumber += 1;
                        camprecord.Auto_Number__c = trpAutoNumber;
                        camprecord.Name = 'Tax Reporting Period - ' + trpAutoNumber;
                    }
                    
                }
            }
        }
        TriggerFactory.createAndExecuteHandler(CampaignTriggerHandler.class);
    }
}