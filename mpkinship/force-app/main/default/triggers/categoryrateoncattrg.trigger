trigger categoryrateoncattrg on Category_Rate__c (before insert,before update) {
    
    Id otherRecTypeId = Schema.SObjectType.Category_Rate__c.getRecordTypeInfosByName().get('Other Rate').getRecordTypeId();
    Id sptRecTypeId = Schema.SObjectType.Category_Rate__c.getRecordTypeInfosByName().get('Service Placement Types').getRecordTypeId();
    Id serviceRecTypeId = Schema.SObjectType.Category_Rate__c.getRecordTypeInfosByName().get('Service Names').getRecordTypeId();
    Id LASERRecTypeId = Schema.SObjectType.Category__c.getRecordTypeInfosByName().get('LASER Category').getRecordTypeId();
    Id LEDRSRecTypeId = Schema.SObjectType.Category__c.getRecordTypeInfosByName().get('LEDRS Category').getRecordTypeId();
    map<id,List<Category_Rate__c>> catncatRatesMap = new map<id,List<Category_Rate__c>>();
    Set<Id> catSetIds= new Set<Id>();
    for(Category_Rate__c cr : trigger.new){
       catSetIds.add(cr.Category__c);
    }
    List<Category__c> catList = [Select Id,name,RecordTypeId,(Select Id,name,Category__c,Category__r.RecordTypeId,RecordTypeId,Status__c,Min_Age__c,Max_Age__c  from Category_Rates__r) from Category__c where Id=:catSetIds];
    for(Category__c cat : catList ){
        catncatRatesMap.put(cat.Id,cat.Category_Rates__r);
    }
    List<CategoryAndSPT__c> sptRelToCat= [Select Id, name, Service_Placement_Type__c from CategoryAndSPT__c where Category__c=:catSetIds];
    List<Id> SPTIds= new List<Id>();
    for(CategoryAndSPT__c catspt: sptRelToCat){
        SPTIds.add(catspt.Service_Placement_Type__c);
        System.debug('catspt--Service_Placement_Type__c'+catspt.Service_Placement_Type__c);
    }
    for(Category_Rate__c cr : trigger.new){
        System.debug(cr.Category__c);
        if(cr.Category__c!=null ){
            
            // for lookup filter
            if(cr.recordTypeId == sptRecTypeId ){
                System.debug('spt recordtype');
                if(!SPTIds.contains(cr.Service_Placement_Type__c)){
                    System.debug('error');
                    cr.addError('Please select Service Placement Types that are related to the above category.');
                    break;
                }
            }
            // ends
            List<Category_Rate__c > oldCatrates= catncatRatesMap.get(cr.Category__c);
            
            for(Category_Rate__c rate : oldCatrates){
                if(Trigger.IsInsert ){
                if(rate.RecordTypeId == otherRecTypeId && rate.Status__c=='Active' && cr.Status__c=='Active') cr.addError('There is already an active category rate of type "Other" for this category.');
                
                if(rate.RecordTypeId == sptRecTypeId && rate.Status__c=='Active' && cr.Status__c=='Active') cr.addError('There is already an active category rate of type "SPT" for this category.');
                
                if(rate.RecordTypeId == serviceRecTypeId && rate.Status__c=='Active' && cr.Status__c=='Active'&& (cr.RecordTypeId == otherRecTypeId || cr.RecordTypeId == sptRecTypeId ))   cr.addError('There is already an active category rate of type "Service" for this category.');
                
                if(cr.RecordTypeId == serviceRecTypeId && rate.Min_Age__c== cr.Min_Age__c && rate.Max_Age__c == cr.Max_Age__c && rate.Min_Age__c!=NULL && rate.Max_Age__c !=NULL)    cr.addError('Age Range exists for this category.');
                
                if(rate.Category__r.RecordTypeId==LASERRecTypeId && cr.RecordTypeId==sptRecTypeId)      cr.addError('Cannot create Category Rate of Type "SPT" as this a category of type "LASER".');
                
                if(rate.Category__r.RecordTypeId==LEDRSRecTypeId && cr.RecordTypeId==serviceRecTypeId)     cr.addError('Cannot create Category Rate of Type "Service Name" as this is a category of type "LEDRS".');
                
                }
                
                if( (Trigger.IsUpdate && Trigger.oldMap.get(cr.id).Status__c != Trigger.newMap.get(cr.id).Status__c)){
                if(rate.RecordTypeId == otherRecTypeId && rate.Status__c=='Active' && cr.Status__c=='Active') cr.addError('There is already an active category rate of type "Other" for this category.');
                
                if(rate.RecordTypeId == sptRecTypeId && rate.Status__c=='Active' && cr.Status__c=='Active') cr.addError('There is already an active category rate of type "SPT" for this category.');
                
                if(rate.RecordTypeId == serviceRecTypeId && rate.Status__c=='Active' && cr.Status__c=='Active'&& (cr.RecordTypeId == otherRecTypeId || cr.RecordTypeId == sptRecTypeId ))          cr.addError('There is already an active category rate of type "Service" for this category.');
                
                }
                if( Trigger.IsUpdate && (Trigger.oldMap.get(cr.id).Min_Age__c != Trigger.newMap.get(cr.id).Min_Age__c || Trigger.oldMap.get(cr.id).Max_Age__c != Trigger.newMap.get(cr.id).Max_Age__c)){
                if(cr.RecordTypeId == serviceRecTypeId && rate.Min_Age__c== cr.Min_Age__c && rate.Max_Age__c == cr.Max_Age__c && rate.Min_Age__c!=NULL && rate.Max_Age__c !=NULL )  cr.addError('Age Range exists for this category.');
                
                }
        
            }
            
        }
    
    }

}