/**PURCHASE ORDER TRIGGER ****/
trigger PurchaseOrderTrg on Opportunity (before insert,after insert,before update,after update,before delete) { 
    TriggerActivation__c IsTriggerActivate = TriggerActivation__c.getAll().get('PurchaseOrder Trigger'); 
    string BeginDate, EndDate ;
    if (IsTriggerActivate != null && IsTriggerActivate.isActive__c){ 
   
        Id porecTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Purchase_Order').getRecordTypeId();
        Id CaseActionrecTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Case_Actions').getRecordTypeId();               
        system.debug('**Recordtypeid'+porecTypeId);
        //FOR INSERT 
        if(trigger.isInsert){
            
            set<id> opportunitiesNameChangeids=new set<id>();
            list<opportunity> opportunitiesupdates = new list<opportunity>();
            list<opportunity> opplist = new list<opportunity>();
            
            set<id> opportunitiesIds = new set<id>();
            list<opportunity> opportunitieslist = new list<opportunity>();
            set<id> accountIds = new set<id>();   
            
            if(trigger.isBefore){
                for(opportunity opprecord : trigger.new){
                    // commented by Ankita while npsp removal
                    /*if(opprecord.recordtypeid== porecTypeId || opprecord.recordtypeid == Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Case_Actions').getRecordTypeId())                  
                        opprecord.npe01__Do_Not_Automatically_Create_Payment__c=true;*/
                    
                    if(opprecord.Payment_Type__c!='Check')
                        opprecord.Payment_Type__c='Check';
                  
                } //FOR LOOP ENDS HERE
                
                
            }
            else{  
                
                for(opportunity opprecord :trigger.new){
                    
                    /***FOR UPDATING THE NAME ***/
                    if(!PurchaseOrderTrgHelper.isNameRun && (opprecord.Kinship_Case__c!=null || opprecord.Category__c!=null 
                      || opprecord.Purchase_Order_Id__c!=null || opprecord.Amount!=null )
                      && (opprecord.recordtypeid==porectypeid || opprecord.recordtypeid== CaseActionrecTypeId)){
                       opportunitiesNameChangeids.add(opprecord.id);    
                   }
                    
                    //FOR ADDING OPP ID TO Locality_Service_Record_Identifier__c field
                    if(!PurchaseOrderTrgHelper.isLocalityRun){
                        opportunitiesupdates.add(opprecord);    
                    }
                    
                }
            }
            
            if(opportunitiesNameChangeids.size()>0)
                PurchaseOrderTrgHelper.ChangeOpportunitiesName(opportunitiesNameChangeids);           
            if(opportunitiesupdates.size()>0)
                PurchaseOrderTrgHelper.UpdateLocalityService(opportunitiesupdates);
            
        }
        List<Deposits__c> depositList = new List<Deposits__c>();
        Deposits__c deposit = new Deposits__c();
        Set<Id> depositIds = new Set<Id>();
        Set<Id> perDepositIds = new Set<Id>();
        Decimal depositAmount = 0.0;
        Decimal perDepositAmount = 0.0;
        //FOR UPDATE
        if(trigger.isUpdate){
            set<id>opportunitiesforPaymentsIds = new set<id>();
            set<id> opportunitiesNameChangeids=new set<id>();
            set<id> opportunitieschangedids = new set<id>();
            map<id,integer> opportunityNumbermap = new map<id,integer>();
            list<opportunity> opportunitiesstagelist = new list<opportunity>();
            list<opportunity> opportunitiesForPayments = new list<opportunity>();
            set<id> opportunitiesForPaymentIds = new set<id>();
            list<opportunity> newOpportunitiesForPayments = new list<opportunity>();
            set<id> newOpportunitiesForPaymentIds = new set<id>();
            list<opportunity> opportunitiestoupdate = new list<opportunity>();
            
            set<id> opportunitiesidstobeclosed = new set<id>();
            set<id> opportunitiesPaymentIds = new set<id>();
            set<id> OppinvoicestoUpdated = new set<id>();
            set<id> opportunitiesforCAPOIds = new set<id>();
            list<opportunity> opportunitiesForCAPO = new list<opportunity>();
            set<id> opportunitiesForCAIds = new set<id>();
            
            set<id> OpportunitiesIdsForFips = new set<id>();
            set<id> OpportunitiesIdsForFiscalYear = new set<id>();
            set<id> ROCIdsSet = new set<id>();

            if(trigger.isafter){
                 id caseactionRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Case_Actions').getRecordTypeId();
                 id POSORecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Purchase_Order').getRecordTypeId();
                 Id ROCRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Report_of_Collection').getRecordTypeId();
                 POSO_Created_Date__c POSOcs = POSO_Created_Date__c.getInstance();
                for(opportunity opprecord : trigger.new){

                    if(Trigger.oldMap.get(opprecord.Id).Deposit__c != Trigger.newMap.get(opprecord.Id).Deposit__c){
                        if(Trigger.newMap.get(opprecord.Id).Deposit__c != null){
                            depositAmount = opprecord.Payment_Amount_Received__c;
                            //perDepositAmount = Trigger.oldMap.get(opprecord.Id).Payment_Amount_Received__c;
                            depositIds.add(opprecord.Deposit__c);
                            perDepositIds.add(Trigger.oldMap.get(opprecord.Id).Deposit__c);
                        }
                        else{
                            depositAmount = depositAmount - opprecord.Payment_Amount_Received__c;
                            depositIds.add(Trigger.oldMap.get(opprecord.Id).Deposit__c );
                        }
                        
                    }
                    else if(Trigger.oldMap.get(opprecord.Id).Payment_Amount_Received__c != Trigger.newMap.get(opprecord.Id).Payment_Amount_Received__c){ 
                        if(opprecord.Deposit__c != null){
                            depositAmount = opprecord.Payment_Amount_Received__c;
                            perDepositAmount = Trigger.oldMap.get(opprecord.Id).Payment_Amount_Received__c;
                            depositIds.add(opprecord.Deposit__c);
                        }
                    } 
                    else if(Trigger.oldMap.get(opprecord.Id).StageName != Trigger.newMap.get(opprecord.Id).StageName){
                        depositIds.add(opprecord.Deposit__c);
                            
                    } 

                    /*** CHANGE ON PAYMENT CHECK METHOD ***/
                    if(opprecord.Payment_Type__c!=null && opprecord.Payment_Type__c!=trigger.oldmap.get(opprecord.id).Payment_Type__c){
                        opportunitiesPaymentIds.add(opprecord.id);    
                    }
                    
                    /***FOR UPDATING THE NAME ***/
                    if(!PurchaseOrderTrgHelper.isNameUpdateRun && (opprecord.recordtypeid == porecTypeId 
                      || opprecord.recordtypeid == CaseActionrecTypeId ) && ((opprecord.Kinship_Case__c!=null && opprecord.Kinship_Case__c!= 
                    trigger.oldmap.get(opprecord.id).Kinship_Case__c) || (opprecord.Category__c!=null && opprecord.Category__c!= 
                    trigger.oldmap.get(opprecord.id).Category__c) || (opprecord.Purchase_Order_Id__c!=null && opprecord.Purchase_Order_Id__c!= 
                    trigger.oldmap.get(opprecord.id).Purchase_Order_Id__c) || (opprecord.Amount!=null && opprecord.Amount!= 
                    trigger.oldmap.get(opprecord.id).Amount))){
                         opportunitiesNameChangeids.add(opprecord.id);    
                    }
                    
                    /**FOR CREATING FIRST TIME PAYMENTS FOR PO **/               
                    if(opprecord.Number_of_Months_Units__c!=null && opprecord.Number_of_Months_Units__c >0 && 
                       opprecord.stagename=='Approved' && opprecord.stagename!=trigger.oldmap.get(opprecord.id).stagename 
                       && opprecord.Number_of_Months_Units__c== trigger.oldmap.get(opprecord.id).Number_of_Months_Units__c
                       && (!PurchaseOrderTrgHelper.isRun) && opprecord.Remaining_Balance__c >0  && 
                       Paymentrecordhelper.isOpportunity==false && opprecord.recordtypeid==porecTypeId ){
                           system.debug('**Enterd in creating payment of po');
                           if(POSOcs!=null && POSOcs.Created_Date__c!=null && POSOcs.Created_Date__c < date.valueof(opprecord.createddate)){
                               system.debug('**Enterd in 144');
                               newOpportunitiesForPayments.add(opprecord);
                               newopportunitiesForPaymentIds.add(opprecord.id);    
                           }
                           else{
                               system.debug('**Entered in 148 line');
                               opportunitiesForPayments.add(opprecord);
                               opportunitiesForPaymentIds.add(opprecord.id);
                           }    
                       }
                    
                    /***FOR AUTO WRITEOFF DECREASED PAYMENTS ***/
                    if(opprecord.Number_of_Months_Units__c!=null  &&  opprecord.Number_of_Months_Units__c!= 
                       trigger.oldmap.get(opprecord.id).Number_of_Months_Units__c && (!PurchaseOrderTrgHelper.IsPaymentWriteoff)
                       && opprecord.Remaining_Balance__c >0 && opprecord.Number_of_Months_Units__c < 
                       trigger.oldmap.get(opprecord.id).Number_of_Months_Units__c && Paymentrecordhelper.isOpportunity==false){                     
                           integer totalunitstobeleft = integer.valueof(opprecord.Number_of_Months_Units__c);
                           opportunityNumbermap.put(opprecord.id,totalunitstobeleft);
                       }  
                    
                    /** FOR POPULATING STAGE TO CLOSED ONCE BALANCE CHANGED TO 0 ***/
                    if((opprecord.Remaining_Balance__c!=null) && opprecord.Remaining_Balance__c>=1 && 
                       !PurchaseOrderTrgHelper.isClosedRun && opprecord.stagename=='Approved' && opprecord.stagename!=
                       trigger.oldmap.get(opprecord.id).stagename &&  opprecord.Number_of_Months_Units__c>0 && 
                       (opprecord.recordtypeid==porecTypeId || opprecord.recordtypeid == Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Case_Actions').getRecordTypeId())){
                           opportunitiesstagelist.add(opprecord);
                       } 
                    
                    if(opprecord.Number_of_Months_Units__c==0 && opprecord.Number_of_Months_Units__c!=
                       trigger.oldmap.get(opprecord.id).Number_of_Months_Units__c && opprecord.stagename!='Draft'
                       && opprecord.stagename!='Awaiting Approval' && !PurchaseOrderTrgHelper.isClosedRun){
                           opportunitiestoupdate.add(opprecord);
                       } 
                    
                    if(opprecord.stagename!=trigger.oldmap.get(opprecord.id).stagename && opprecord.stagename=='Closed'
                       && !PurchaseOrderTrgHelper.isWriteoff && (opprecord.recordtypeid==porecTypeId || 
                       opprecord.recordtypeid== Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Case_Actions').getRecordTypeId())){ 
                        opportunitiesidstobeclosed.add(opprecord.id);        
                   }
                    
                    /**FOR CREATING  PAYMENTS FOR CA **/               
                    if(opprecord.Number_of_Months_Units__c!=null && opprecord.Number_of_Months_Units__c >0 && 
                       opprecord.stagename=='Approved' && opprecord.stagename!=trigger.oldmap.get(opprecord.id).stagename 
                       && opprecord.Number_of_Months_Units__c== trigger.oldmap.get(opprecord.id).Number_of_Months_Units__c
                       && opprecord.Amount>0  && Paymentrecordhelper.isOpportunity==false && opprecord.recordtypeid==
                       Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Case_Actions').getRecordTypeId()){
                           system.debug('entered in po ca condition');
                           if(opprecord.Invoice_Stage__c!='Invoice Created' && !PurchaseOrderTrgHelper.isCAPO){ 
                               system.debug('**not invoice created');  
                               opportunitiesForCAPO.add(opprecord);
                               opportunitiesforCAPOIds.add(opprecord.id);
                           }   
                           else if(!PurchaseOrderTrgHelper.IsAgeCA && opprecord.Invoice_Stage__c=='Invoice Created' ){
                               opportunitiesForCAIds.add(opprecord.id);
                           } 
                    }
                    
                   
                   
                    /**FOR CREATING CASEACTION FROM SPECIAL REIMBURSEMENT IF ROC IS CLOSED **/
                    if(opprecord.stagename=='Completed' && opprecord.stagename!=trigger.oldmap.get(opprecord.id).stagename && 
                    opprecord.recordtypeid==ROCRecordTypeId && !PurchaseorderTrgHelper.isROCWelfare){
                        ROCIdsSet.add(opprecord.id);    
                    }
                } /**FOR LOOP ENDS HERE **/
                
                if(opportunitiesForPayments.size()>0)
                    PurchaseOrderTrgHelper.createPaymentsfromPOOld(opportunitiesForPayments,opportunitiesForPaymentIds);
                
                if(newopportunitiesForPayments.size()>0)
                   PurchaseOrderTrgHelper.createPaymentsfromPO(newopportunitiesForPayments,newopportunitiesForPaymentIds);    
                         
                if(opportunitiestoupdate.size()>0)
                    PurchaseOrderTrgHelper.changePOstagetoClosed1(opportunitiestoupdate);                  
                
                if(opportunitiesidstobeclosed.size()>0)
                    PurchaseOrderTrgHelper.WriteoffPaymentsonClosedopp(opportunitiesidstobeclosed);     
                
                if(opportunitiesforCAPOIds.size()>0)
                    PurchaseOrderTrgHelper.createPaymentsForPOCA(opportunitiesForCAPO,opportunitiesforCAPOIds);
                
                if(opportunitiesForCAIds.size()>0)
                    PurchaseOrderTrgHelper.UpdatePaymentsAfterCAageApproval(opportunitiesForCAIds);
                              
                /*if(OpportunitiesIdsForFiscalYear.size()>0)
                    PurchaseOrderTrgHelper.UpdateFiscalYearOnInvoices(OpportunitiesIdsForFiscalYear); */  
                
                if(ROCIdsSet.size()>0)
                    PurchaseOrderTrgHelper.ChangeSpecialWelfareInvoices(ROCIdsSet);             
            
                List<Deposits__c> depositListComp = new List<Deposits__c>();
                Deposits__c depositComp = new Deposits__c();
                System.debug('depositIds.size()'+depositIds.size());
                if(depositIds.size() > 0){
                    depositList = new List<Deposits__c>();
                    depositList = [Select Id ,Deposit_Amount__c, (Select id,StageName from Authorizations__r) from Deposits__c where Id IN: depositIds];
                    for(Deposits__c d: depositList){
                        Boolean check  = false;
                        for(Opportunity o : d.Authorizations__r){
                            System.debug('o.StageName'+o.StageName);
                            if(o.StageName != 'Completed'){
                                check = true;
                            }
                        }
                        if(!check){
                            depositComp = new Deposits__c();
                            depositComp.Id = d.Id;
                            if(d.Deposit_Amount__c != null && d.Deposit_Amount__c>0){
                                d.Deposit_Amount__c = d.Deposit_Amount__c - perDepositAmount;
                                depositComp.Deposit_Amount__c = d.Deposit_Amount__c + depositAmount;
                            }
                            else{
                                depositComp.Deposit_Amount__c = depositAmount;
                            }
                            depositComp.Status__c = 'Completed'; 
                            depositListComp.add(depositComp);
                        }
                        else{
                            depositComp = new Deposits__c();
                            depositComp.Id = d.Id;
                            if(d.Deposit_Amount__c != null && d.Deposit_Amount__c>0){
                                d.Deposit_Amount__c = d.Deposit_Amount__c - perDepositAmount;
                                depositComp.Deposit_Amount__c = d.Deposit_Amount__c + depositAmount;
                            }
                            else{
                                depositComp.Deposit_Amount__c = depositAmount;
                            }
                            depositComp.Status__c = 'In Progress'; 
                            depositListComp.add(depositComp);
                        }
                    }
                    
                    depositList = new List<Deposits__c>();
                    depositList = [Select Id ,Deposit_Amount__c, (Select id,StageName from Authorizations__r) from Deposits__c where Id IN: perDepositIds];
                    for(Deposits__c d: depositList){
                            depositComp = new Deposits__c();
                            depositComp.Id = d.Id;
                            if(d.Deposit_Amount__c != null && d.Deposit_Amount__c>0){
                                depositComp.Deposit_Amount__c = d.Deposit_Amount__c - depositAmount;
                            }
                            depositListComp.add(depositComp);
                    }
                    if(depositListComp.size() > 0 ){
                        update depositListComp;
                    }
                }    
            } 
            
            //BEFORE
            if(trigger.isBefore){
                set<id> changedopportunities = new set<id>();
                Map<Id, Opportunity> OppMap = Trigger.NewMap;
                set<id> accountids = new set<id>();
                set<id> categoriesids =new set<id>();              
                set<id> PBaccountIds = new set<id>();
                
                Id profileId = UserInfo.getProfileId();
                String profileName =[Select Id, Name from Profile where Id=:profileId].Name;
                
                for(opportunity opprecord:trigger.new){ 
                                       
                    if(opprecord.RecordTypeId == Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Report of Collections').getRecordTypeId())
                        
                    {    
                        BeginDate = opprecord.CloseDate.month() + '-' + opprecord.CloseDate.day() + '-' + opprecord.CloseDate.year();
                        EndDate = opprecord.End_Date__c.month() + '-' + opprecord.End_Date__c.day() + '-' + opprecord.End_Date__c.year();
                        if(!(opprecord.Name).contains('$'))
                        {
                            opprecord.Name = opprecord.ROC_Name__c + ' ' + BeginDate + '--' +EndDate + ' ' + FactoryCls.formatCurr(opprecord.Amount);
                        }
                    }   
                    
                    if(opprecord.RecordTypeId == Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Staff & Operations').getRecordTypeId())
                        
                    {    
                         opprecord.Name = opprecord.Voucher_Number__c;
                    }
                    
                    if(opprecord.RecordTypeId == Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Purchase_Order').getRecordTypeId())
                    {    
                        if(opprecord.Amount != null){ 
                            if(!(opprecord.Name).contains('$'))
                            {
                                opprecord.Name  += ' ' + FactoryCls.formatCurr(opprecord.Amount);
                            }
                        }
                    }
                    
                    if(opprecord.RecordTypeId == Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Case_Actions').getRecordTypeId())
                    {    
                        if(opprecord.Amount != null){ 
                            if(!(opprecord.Name).contains('$'))
                            {
                                opprecord.Name  += ' ' + FactoryCls.formatCurr(opprecord.Amount);
                            }
                        }
                    }
                    
                    /**If units are changed, validate that the number of units can only be decreased if there are unpaid payments **/
                    if(opprecord.Number_of_Months_Units__c!=0 && opprecord.Number_of_Months_Units__c < 
                       trigger.oldmap.get(opprecord.id).Number_of_Months_Units__c){
                           changedopportunities.add(opprecord.id);
                       }
                                        
                    /**IF OPPORUTNITY STAGE CANNOT BE CHANGED ONCE APPROVED **/
                    if(opprecord.recordtypeid==porecTypeId && (opprecord.stagename=='Draft' || opprecord.stagename=='Awaiting Approval' || 
                      opprecord.stagename=='Approved') && trigger.oldmap.get(opprecord.id).stagename !=opprecord.stagename
                       && (trigger.oldmap.get(opprecord.id).stagename=='Open'|| trigger.oldmap.get(opprecord.id).stagename=='Closed') 
                       && profileName!=null && profileName!='Fiscal Officer' && profileName!='CSA Admin' && profileName!='System Administrator')
                        opprecord.addError('Stage cannot be reverted once Purchase Order gets approved.');
                    
                    //FOR THE STAGE CHANGED TO OPEN FROM CLOSED   
                    if (opprecord.stagename=='Open' && trigger.oldmap.get(opprecord.id).stagename !=opprecord.stagename &&
                        trigger.oldmap.get(opprecord.id).stagename=='Closed' && !CheckTrgHelper.IsCancelledRun && !CheckTrgHelper.IsResissued)                       
                        opprecord.addError('Stage cannot be reverted once Purchase Order gets approved.'); 
                    
                    /**MAKE SOME OF THE FIELDS READ-ONLY ON PURCHASE ORDER **/
                    if(opprecord.stagename!='Draft' && opprecord.recordtypeid==porecTypeId && profileName!=null &&
                       profileName!='Fiscal Officer' && profileName!='CSA Admin' && profileName!='System Administrator' &&
                       ((opprecord.accountid!=trigger.oldmap.get(opprecord.id).accountid) ||
                        (opprecord.Category__c != trigger.oldmap.get(opprecord.id).Category__c) ||
                        (opprecord.Kinship_Case__c!= trigger.oldmap.get(opprecord.id).Kinship_Case__c) ||
                        (opprecord.CampaignId!=trigger.oldmap.get(opprecord.id).CampaignId) || 
                        (opprecord.FIPS_Code__c != trigger.oldmap.get(opprecord.id).FIPS_Code__c) ||
                        (opprecord.Parent_Recipient__c!=trigger.oldmap.get(opprecord.id).Parent_Recipient__c) || 
                        (opprecord.closedate!=trigger.oldmap.get(opprecord.id).closedate) || 
                        (opprecord.End_Date__c!=trigger.oldmap.get(opprecord.id).End_Date__c)))
                        opprecord.addError('Changes cannot be made once the Purchase Order has been approved.'); 
                    
                    /** IF OPPORTUNITY STAGE CHANGED DIRECTLY TO APPROVED **/
                    if(opprecord.stagename=='Approved' && trigger.oldmap.get(opprecord.id).stagename=='Draft' && profileName!=null &&
                       profileName!='Fiscal Officer' && profileName!='CSA Admin')
                        opprecord.addError('Please send the Purchase Order for approval by clicking on "Submit for Approval" button.'); 
                    
                    /** OPPORTUNITY STAGE CHANGES FROM APPROVED TO BACK **/
                    if((opprecord.stagename=='Draft' || opprecord.stagename=='Awaiting Approval') && 
                       trigger.oldmap.get(opprecord.id).stagename=='Approved'){
                           opprecord.addError('Stage cannot be reverted.');
                       } 
                    
                    /***CHECK FOR STAGE AND PRICEBOOK ***/
                    if(opprecord.Pricebook2Id!=null && opprecord.Pricebook2Id != trigger.oldmap.get(opprecord.id).Pricebook2Id 
                       && (opprecord.stagename!='Draft' && opprecord.stagename!='Awaiting Approval')) 
                        opprecord.addError('Pricebook cannot be added or changed if the purchase order has been approved.'); 
                 
                }    
                
                
                if(changedopportunities.size()>0)
                    PurchaseOrderTrgHelper.CheckUnPaidPaymentExist(changedopportunities,oppmap);  
                
            } //BEFORE UPDATE ENDS HERE
            
            if(opportunitiesNameChangeids.size()>0)
                PurchaseOrderTrgHelper.ChangeOpportunitiesName(opportunitiesNameChangeids);                    
            
            /***FOR WRITEOFF NEW CHANGE ***/
            if(opportunityNumbermap.size()>0)
                PurchaseOrderTrgHelper.WriteOffScheduledPayments(opportunityNumbermap);   
            if(opportunitiesstagelist.size()>0)
                PurchaseOrderTrgHelper.changePOstagetoClosed(opportunitiesstagelist);            
        }
        
        if(trigger.isbefore && Trigger.isdelete){
            id caseactionrecordtypeid = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Case_Actions').getRecordTypeId();
            set<id> opportunityIds = new set<id>();
            for(opportunity opp : trigger.old){
                if(opp.recordtypeid==caseactionrecordtypeid && opp.ROC_Id__c!=null && !PurchaseOrderTrgHelper.IsclaimUpdate)
                    opportunityIds.add(opp.id);
            }
            
            if(opportunityIds.size() >0)
                PurchaseOrderTrgHelper.DeletePaymentsClaimUpdation(opportunityIds);
        }
        
    }
}