trigger ClaimsTrg on Claims__c (after insert,after update) {
    TriggerActivation__c IsTriggerActivate = TriggerActivation__c.getAll().get('Claims Trigger'); 
    
    if (IsTriggerActivate != null && IsTriggerActivate.isActive__c){ 
        System.debug('@@'+trigger.isUpdate + trigger.isAfter);
    	// KIN 1431
        if(trigger.isUpdate && trigger.isAfter){
            
            List<Claims__c> checkNewNameLst = new List<Claims__c>();
            Claims__c chknew  = new Claims__c();
            //List<Id> VenIds= new List<Id>();
            List<Id> CaseIds= new List<Id>();
            for(Claims__c chk : trigger.new){
                //|| chk.Payable_to__c!=trigger.oldmap.get(chk.id).Payable_to__c
                if(chk.Case__c!=trigger.oldmap.get(chk.id).Case__c ){
                	CaseIds.add(chk.Case__c);
                	//VenIds.add(chk.Payable_to__c);
                }
                
            }
            //System.debug('VenIds'+VenIds);
            System.debug('CaseIds'+CaseIds);
            //Map<Id,Account> VenMap;
            Map<Id,Kinship_Case__c> CaseMap;
            //if(VenIds.size()>0)
            //VenMap= new Map<Id,Account>([Select id,name from Account where id=:VenIds]);
            if(CaseIds.size()>0)
            CaseMap= new Map<Id,Kinship_Case__c>([Select id,name from Kinship_Case__c where id=:CaseIds]);
            //System.debug('VenMap'+VenMap);
            System.debug('CaseMap'+CaseMap);
            for(Claims__c chk : trigger.new){
                //&& VenMap !=null
                if(CaseMap!=null ){
                chknew.Id = chk.id;
                // commented on 1431
                //chknew.Name = chk.Name__c + ' ' + FactoryCls.formatCurr(chk.Claim_Amount__c);
                System.debug('CaseMap'+CaseMap);
                //System.debug('VenMap'+VenMap);
                //&& VenMap.get(chk.Payable_to__c)!=null
                if(chk.Name__c!=null && CaseMap.get(chk.Case__c)!=null  ){
                    //&& VenMap.get(chk.Payable_to__c).name!=null
                    //+' '+VenMap.get(chk.Payable_to__c).name
                    if(CaseMap.get(chk.Case__c).name!=null )
                		chknew.Name = chk.Name__c+ ' '+ CaseMap.get(chk.Case__c).name ;
                } 
                checkNewNameLst.add(chknew);
                chknew  = new Claims__c();
                }
            }
            if(checkNewNameLst.size()>0)
            update checkNewNameLst;
        
        }
        // Formated check name work
        if(trigger.isInsert && trigger.isAfter){
            List<Claims__c> checkNewNameLst = new List<Claims__c>();
            Claims__c chknew  = new Claims__c();
            //List<Id> VenIds= new List<Id>();
            List<Id> CaseIds= new List<Id>();
            for(Claims__c chk : trigger.new){
               // VenIds.add(chk.Payable_to__c);
                CaseIds.add(chk.Case__c);
            }
            //System.debug('VenIds'+VenIds);
            System.debug('CaseIds'+CaseIds);
            //Map<Id,Account> VenMap= new Map<Id,Account>([Select id,name from Account where id=:VenIds]);
            Map<Id,Kinship_Case__c> CaseMap= new Map<Id,Kinship_Case__c>([Select id,name from Kinship_Case__c where id=:CaseIds]);
            //System.debug('VenMap'+VenMap);
            System.debug('CaseMap'+CaseMap);
            for(Claims__c chk : trigger.new){
                chknew.Id = chk.id;
                // commented on 1431
                //chknew.Name = chk.Name__c + ' ' + FactoryCls.formatCurr(chk.Claim_Amount__c);
                System.debug('CaseMap'+CaseMap.get(chk.Case__c));
                //System.debug('VenMap'+VenMap);
                //&& VenMap.get(chk.Payable_to__c)!=null   
                //&& VenMap.get(chk.Payable_to__c).name!=null
                //+' '+VenMap.get(chk.Payable_to__c).name
                if(chk.Name__c!=null && CaseMap.get(chk.Case__c)!=null  ){
                    if(CaseMap.get(chk.Case__c).name!=null )
                		chknew.Name = chk.Name__c+ ' '+ CaseMap.get(chk.Case__c).name ;
                }
                //ends
                // KIN 142
                if(chk.Claim_Amount__c!=null && chk.Balance_Due__c!=null){
                    if(chk.Claim_Amount__c>0 && chk.Balance_Due__c==0){
                        chknew.Payment_Status__c='Paid in Full';
                    }
                    if(chk.Claim_Amount__c>0 && chk.Balance_Due__c>0 && chk.Balance_Due__c<chk.Claim_Amount__c){
                        chknew.Payment_Status__c='Partially Paid';
                    }
                    if(chk.Claim_Amount__c>0 && chk.Balance_Due__c==chk.Claim_Amount__c){
                        chknew.Payment_Status__c='Unpaid';
                    }
                }
                checkNewNameLst.add(chknew);
                chknew  = new Claims__c();
            } 
            update checkNewNameLst;
        }
    }
    
}