trigger BankStatementTrigger on Bank_Statement__c (before update) {
    
    List<id> bankStatmentIds = new List<id>();
    
    if(Trigger.isUpdate) {
        for(Bank_Statement__c bs: Trigger.new) {
            bankStatmentIds.add(bs.id);
        }
        try{
            
             List<AggregateResult>  rece = [SELECT RecordType.Name rtname, Bank_Statement__c , sum(Payment_Amount__c) amt 
                                       FROM Payment__c WHERE Bank_Statement__c  IN : bankStatmentIds
                                       GROUP BY RecordType.Name, Bank_Statement__c ]; 
                                       
            for(Bank_Statement__c bs: Trigger.new) {
                bs.Total_Receipt_Amount__c = 0.0;
                bs.Total_Payment_Amount__c = 0.0;  
            
                 for(AggregateResult re: rece) {
                   system.debug(re.get('rtname') + ' re.get(RecordType.Name)????');
                   
                    if(bs.Id == re.get('Bank_Statement__c')){
                          if(re.get('rtname') == 'Vendor Refund' || re.get('rtname') == 'Receipt'){
                              bs.Total_Receipt_Amount__c += (Decimal)re.get('amt');    
                          }else if(re.get('rtname') == 'Vendor Invoice'){
                              bs.Total_Payment_Amount__c += (Decimal)re.get('amt');    
                          }
                    }                                   
                }       
            }
                  
        }Catch(Exception e){}
                              
    }
    
}