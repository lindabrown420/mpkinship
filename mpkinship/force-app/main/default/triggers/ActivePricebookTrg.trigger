trigger ActivePricebookTrg on Pricebook2 (before insert,before update) {
   TriggerActivation__c IsTriggerActivate = TriggerActivation__c.getAll().get('Pricebook Trigger'); 
   if (IsTriggerActivate != null && IsTriggerActivate.isActive__c){ 
         list<pricebook2> pricebooks = new list<pricebook2>();
         set<id> vendorids = new set<id>();
         set<id> accountingPeriodIds = new set<id>();
        if(trigger.isinsert && Trigger.isBefore){
            for(Pricebook2 pricebookrecord : trigger.new){
                if(pricebookrecord.isactive==true && pricebookrecord.Vendor__c!=null && pricebookrecord.Accounting_Period__c!=null
                    && !ActivePricebookTrgHelper.isRun && pricebookrecord.isActive==true){
                    pricebooks.add(pricebookrecord); 
                    vendorids.add(pricebookrecord.Vendor__c);
                    accountingPeriodIds.add(pricebookrecord.Accounting_Period__c);
                      
                }             
            }
            
            if(pricebooks.size()>0)
                ActivePricebookTrgHelper.CheckDuplicatePricebook(pricebooks,vendorids,accountingPeriodIds);  
        } 
        
        else if(trigger.isupdate && trigger.isBefore){
            for(Pricebook2 pricebookrecord : trigger.new){
                if(pricebookrecord.isactive==true && pricebookrecord.Vendor__c!=null && pricebookrecord.Accounting_Period__c!=null
                    && !ActivePricebookTrgHelper.isRunUpdate && pricebookrecord.isActive==true &&
                     (pricebookrecord.isactive!=trigger.oldmap.get(pricebookrecord.id).isactive || 
                      pricebookrecord.Vendor__c!=trigger.oldmap.get(pricebookrecord.id).vendor__c ||
                      pricebookrecord.Accounting_Period__c!=trigger.oldmap.get(pricebookrecord.id).Accounting_Period__c)){
                    pricebooks.add(pricebookrecord); 
                    vendorids.add(pricebookrecord.Vendor__c);
                    accountingPeriodIds.add(pricebookrecord.Accounting_Period__c);
                      
                }             
            }
            
            if(pricebooks.size()>0)
                ActivePricebookTrgHelper.CheckDuplicatePricebook(pricebooks,vendorids,accountingPeriodIds); 
        }     
   }
   
}