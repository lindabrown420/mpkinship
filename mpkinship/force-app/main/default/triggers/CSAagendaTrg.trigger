trigger CSAagendaTrg on CSA_Meeting_Agenda__c (before insert, before update,after insert,after update) {
    if(trigger.isUpdate && trigger.isAfter){
        set<id> cmagendaids = new set<id>();
        map<id,CSA_Meeting_Agenda__c> csaAgendaMap = new map<id,CSA_Meeting_Agenda__c>();
        map<id,CSA_Meeting_Agenda__c> csaAgendaConfirmMap = new map<id,CSA_Meeting_Agenda__c>();
        
        for(CSA_Meeting_Agenda__c cmagenda : trigger.new){
            if(cmagenda.Meeting_Status__c =='Cancelled' && cmagenda.Meeting_Status__c !=trigger.oldmap.get(cmagenda.id).Meeting_Status__c
            && !CSAagendaTrgHelper.isRun ){
                csaAgendaMap.put(cmagenda.id,cmagenda);    
            }  
            if(cmagenda.Send_Notification__c==true && cmagenda.Send_Notification__c!=trigger.oldmap.get(cmagenda.id).Send_Notification__c
            && !CSAagendaTrgHelper.IsSendRun){
                csaAgendaConfirmMap.put(cmagenda.id,cmagenda);          
            }  
        }
        if(csaAgendaMap.size()>0){
            CSAagendaTrgHelper.SendCancelledEmail(csaAgendaMap);    
        }
        if(csaAgendaConfirmMap.size()>0)
            CSAagendaTrgHelper.SendNotificationEmail(csaAgendaConfirmMap);    
    }
}