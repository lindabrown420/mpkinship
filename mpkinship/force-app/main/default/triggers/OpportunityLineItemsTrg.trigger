trigger OpportunityLineItemsTrg on OpportunityLineItem (before insert,after insert,before update,before delete,after update) {
    TriggerActivation__c IsTriggerActivate = TriggerActivation__c.getAll().get('OpportunityLineItemTrigger'); 
    
    if (IsTriggerActivate != null && IsTriggerActivate.isActive__c){ 
        Id profileId = UserInfo.getProfileId();
        String profileName =[Select Id, Name from Profile where Id=:profileId].Name;
        
        if(trigger.isinsert || trigger.isupdate){
            if(trigger.IsBefore){
                list<opportunitylineitem>opportunitylineitems = new list<opportunitylineitem>();
                set<id>opportunityIds = new set<id>();
                Set<Id> catIds= new Set<Id>();
                for(OpportunityLineItem  opplineItem : trigger.new){
                    opportunityIds.add(opplineitem.OpportunityId);  
                    opportunitylineitems.add(opplineitem);
                    catIds.add(opplineitem.Category__c);
                }
                OppLineItemTrgHelper.ValidatePOSOscreen(opportunitylineitems,opportunityIds,catIds);
                
            }
            
        }
        if(trigger.isinsert || trigger.isupdate){
            set<id> opplineitemIds = new set<id>();
            map<id,opportunitylineitem> opplineItemIDMap = new map<id,opportunitylineitem>();
            list<OpportunityLineItem> opportunitiesLineItems = new list<OpportunityLineItem>();
            set<id> opportunityIds = new set<id>();
            set<id> updatedOpportunityIds = new set<id>();
            list<OpportunityLineItem> opportunitiesUpdatedLineItems = new list<OpportunityLineItem>();
           
            for(OpportunityLineItem  opplineItem : trigger.new){
               
                if(trigger.isInsert && trigger.isBefore && opplineItem.Category__c!=null &&
                 !OpportunityLineItemsTrgHelper.IsValidate){
                    opportunitiesLineItems.add(opplineItem); 
                    opportunityIds.add(opplineItem.OpportunityId);        
                }
                
                if(trigger.isUpdate && trigger.isBefore && ((opplineitem.product2id!=trigger.oldmap.get(opplineitem.id).product2id) ||
                 (opplineitem.Category__c!=null && opplineitem.Category__c!=trigger.oldmap.get(opplineitem.id).Category__c)) &&
                 !OpportunityLineItemsTrgHelper.IsUpdateValidate){
                     updatedOpportunityIds.add(opplineitem.OpportunityId);  
                     opportunitiesUpdatedLineItems.add(opplineitem);      
                 }
                
               /* if(trigger.isinsert && !OpportunityLineItemsTrgHelper.IsInsertRun && trigger.isafter){
                    opplineitemIds.add(opplineItem.id);
                    opplineItemIDMap.put(opplineitem.id,opplineitem);
                } */
                if(trigger.isInsert && trigger.isafter){
                    if(opplineItem.UnitPrice!=null && opplineItem.unitprice !=opplineItem.ListPrice){
                        opplineItem.addError('You cannot change the Sales price assigned to the Vendor Pricebook.');
                        break;
                    }    
                }
                if(trigger.isBefore && trigger.isupdate && opplineitem.unitprice != trigger.oldmap.get(opplineitem.id).unitprice){
                    opplineitem.addError('You cannot change the Sales price assigned to the Vendor Pricebook.');    
                }
                
                if(!OpportunityLineItemsTrgHelper.IsRun && Trigger.isUpdate && trigger.isBefore  && profileName!=null &&
                profileName!='Fiscal Officer' && profileName!='CSA Admin' && profileName!='System Administrator' &&
               /* && (opplineItem.UnitPrice !=trigger.oldmap.get(opplineItem.id).UnitPrice ||*/
               (opplineItem.Unit_Measure__c !=trigger.oldmap.get(opplineItem.id).Unit_Measure__c || 
                opplineItem.Quantity!= trigger.oldmap.get(opplineItem.id).Quantity || 
                opplineItem.ServiceDate!= trigger.oldmap.get(opplineItem.id).ServiceDate ||
                opplineItem.Category__c!=trigger.oldmap.get(opplineItem.id).Category__c ||
                opplineItem.Description!= trigger.oldmap.get(opplineItem.id).Description)){
                    opplineitemIds.add(opplineItem.id);
                    opplineItemIDMap.put(opplineitem.id,opplineitem);
                }
            }
            
           
            if(opportunitiesLineItems.size()>0)
                OpportunityLineItemsTrgHelper.ValidateCategory(opportunitiesLineItems,opportunityIds);
            if(opplineitemIds.size()>0)
                OpportunityLineItemsTrgHelper.validateLineItems(opplineitemIds,opplineItemIDMap );  
            if(opportunitiesUpdatedLineItems.size()>0)
                OpportunityLineItemsTrgHelper.ValidateCategory(opportunitiesUpdatedLineItems,updatedOpportunityIds);      
        
        }
        
        if(Trigger.isAfter && trigger.isInsert){
            set<id> opplineitemsidsforIVE = new set<id>();
            set<id> opportunityiveIds = new set<id>();
             for(OpportunityLineItem  opplineItem : trigger.new){
               /**CHECK ON INSERT OR UPDATE THAT THE RESPECITVE FOR PO'S CASE TITLE IVE FIELD IF CAT IS OF TYPE CSA OR IVE AND 
                IF NO USER CANT USE IV E CATEGORY**/
                if(opplineItem.Category__c!=null && !OpportunityLineItemsTrgHelper.IsIVEvalidate){
                    system.debug('***Entered in first if');
                    opplineitemsidsforIVE.add(opplineItem.id);
                    opportunityiveIds.add(opplineItem.opportunityid);    
                }    
             }
             if(opplineitemsidsforIVE.size()>0)
                 OpportunityLineItemsTrgHelper.ValidateIVEForCategory(opplineitemsidsforIVE,opportunityiveIds);
        }
        
        if(trigger.isAfter && trigger.isUpdate){
            set<id> opplineitemidsForIVEupdate = new set<id>();
             set<id> opportunityiveIdsUpdate = new set<id>();             
             
             for(OpportunityLineItem  opplineItem : trigger.new){
               /**CHECK ON INSERT OR UPDATE THAT THE RESPECITVE FOR PO'S CASE TITLE IVE FIELD IF CAT IS OF TYPE CSA OR IVE AND 
                IF NO USER CANT USE IV E CATEGORY**/
                if(opplineItem.Category__c!=null && opplineItem.Category__c!=trigger.oldmap.get(opplineItem.id).Category__c
                 && !OpportunityLineItemsTrgHelper.IsIVEvalidateUpdate){
                    system.debug('***Entered in first if');
                    opplineitemidsForIVEupdate.add(opplineItem.id);
                    opportunityiveIdsUpdate.add(opplineItem.opportunityid);    
                } 
                              
             }
             if(opplineitemidsForIVEupdate.size()>0)
                 OpportunityLineItemsTrgHelper.ValidateIVEForCategory(opplineitemidsForIVEupdate,opportunityiveIdsUpdate );
                 
                    
        }
        
        if(trigger.isbefore && trigger.isDelete){
            // KIN 63
            List<opportunitylineitem> oppLineItemsList= new List<opportunitylineitem>();
            List<Id> oppIdList= new List<Id>();
            //ends
            set<id> opportunitylineitemIds = new set<id>();
            map<id,opportunitylineitem> opplineItemIDMap = new map<id,opportunitylineitem>();
            for(OpportunityLineItem  opplineitem : trigger.old) {
                // KIN 63
                if(!PurchaseOrderController.IspoScreenRun &&  !PurchaseOrderControllerEdit.IspoScreenRun){
                        oppLineItemsList.add(opplineitem);
                        oppIdList.add(opplineitem.OpportunityId);
                    }
                //ends
                if(!OpportunityLineItemsTrgHelper.IsDeleteRun && profileName!=null &&
                profileName!='Fiscal Officer' && profileName!='CSA Admin' && profileName!='System Administrator'){
                    opportunitylineitemIds.add(opplineitem.id);  
                    opplineItemIDMap.put(opplineitem.id,opplineitem);
                }      
            } 
            // KIN 63
            if(oppLineItemsList.size()>0){
                OpportunityLineItemsTrgHelper.POServicesCheck(oppLineItemsList,oppIdList);
            }
            // ends
            if(opportunitylineitemIds.size()>0)
                 OpportunityLineItemsTrgHelper.validateLineItems(opportunitylineitemIds ,opplineItemIDMap);          
        
        }
    }
    

}