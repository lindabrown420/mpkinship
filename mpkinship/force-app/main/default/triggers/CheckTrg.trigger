trigger CheckTrg on Kinship_Check__c (after insert,after update,before update) {
    TriggerActivation__c IsTriggerActivate = TriggerActivation__c.getAll().get('Check Trigger'); 
    
    if (IsTriggerActivate != null && IsTriggerActivate.isActive__c){ 
        // Formated check name work
        if(trigger.isBefore){
            Set<Id> venIds = new Set<Id>();
            Map<String, String> mapVendorName = new Map<String, String>();
            for(Kinship_Check__c chk : trigger.new){
                venIds.add(chk.Vendor__c);
            }
            for(Account a : [SELECT id, Name FROM Account WHERE Id IN :venIds]){
                mapVendorName.put(a.id, a.Name);
            }
            String tName;
            for(Kinship_Check__c chk : trigger.new){
                tName = mapVendorName.get(chk.Vendor__c);
                if(String.isBlank(tName)) {
                    tName = '';
                } else {
                    tName = ' ' + tName;
                }
                if(String.isNotBlank(chk.Check_No__c)) {
                    chk.Name = KinshipUtil.getMaxLength(chk.Check_No__c + ' ' + FactoryCls.formatCurr(chk.Amount__c) + tName, 80);
                } else {
                	chk.Name = KinshipUtil.getMaxLength(chk.Name__c + ' ' + FactoryCls.formatCurr(chk.Amount__c) + tName, 80);
                }
            }
        }
        
        //CHANGES RELATED TO CHECK STATUS CANCELLED OR REISSUED
        if(trigger.isUpdate && trigger.isAfter){
            set<id> cancelledCheckIds = new set<id>();
            set<id> ReissuedCheckIds = new set<id>();
            for(Kinship_Check__c checkrec : trigger.new){
                if(checkrec.Status__c=='Cancelled' && checkrec.Status__c!=trigger.oldmap.get(checkrec.id).Status__c &&
                !CheckTrgHelper.IsCancelledRun){
                 system.debug('**Entered in cancelled check');
                    cancelledCheckIds.add(checkrec.id);    
                }
                if(checkrec.Status__c=='Cancelled & Reissued' && checkrec.Status__c!=trigger.oldmap.get(checkrec.id).Status__c && 
                !CheckTrgHelper.IsResissued){
                    ReissuedCheckIds.add(checkrec.id);    
                }
            }
            system.debug('**VAl of checkids'+ cancelledCheckIds);
            if(cancelledCheckIds.size()>0)
                CheckTrgHelper.CancelledCheckUpdates(cancelledCheckIds);
            if(ReissuedCheckIds.size()>0)
               CheckTrgHelper.ReissuedCheckUpdates(ReissuedCheckIds);
        }
        
        //BEFORE UPDATE
       /* if(trigger.isUpdate && trigger.isBefore){
            for(Kinship_Check__c checkrec : trigger.new){
                if(checkrec.Status__c!='Cancelled'  && checkrec.Status__c!=trigger.oldmap.get(checkrec.id).Status__c &&
                    trigger.oldmap.get(checkrec.id).Status__c=='Cancelled'){
                        checkrec.addError('You cannot change the status of check again once it is cancelled');
                    }
                else if(checkrec.Status__c!='Cancelled & Reissued'  && checkrec.Status__c!=trigger.oldmap.get(checkrec.id).Status__c &&
                    trigger.oldmap.get(checkrec.id).Status__c=='Cancelled & Reissued'){
                        checkrec.addError('You cannot change the status of check again once it is reissued');
                 }    
            }
        } */
    }
    
}