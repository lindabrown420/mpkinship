trigger FIPSTrigger on FIPS__c (before insert, after insert, before update, after update, before delete) {
    TriggerActivation__c qliTrigger = TriggerActivation__c.getAll().get('FIPSTrigger');
    if (qliTrigger != null && qliTrigger.isActive__c){
        TriggerFactory.createAndExecuteHandler(FIPSTriggerHandler.class);
        // KIN 191
        if(!PurchaseOrderController.IspoScreenRun && !PurchaseOrderControllerEdit.IspoScreenRun){
        	if(Trigger.IsInsert && Trigger.IsBefore){
            List<FIPS__c> lstFIPSDefault= [SELECT id, Name, Is_Default__c FROM FIPS__c WHERE Is_Default__c = true LIMIT 1];
                for(FIPS__c fips: Trigger.New){
                    if(fips.Is_Default__c && lstFIPSDefault.size()>0){
                        fips.adderror('The default FIPS record already exists. You need to uncheck the "Is Default" checkbox on the current default FIPS record and then you will be able to save this record.');
                    }
                }
                
            }
            if(Trigger.IsUpdate && Trigger.IsBefore){
                List<FIPS__c> lstFIPSDefault= [SELECT id, Name, Is_Default__c FROM FIPS__c WHERE Is_Default__c = true LIMIT 1];
                for(FIPS__c fips: Trigger.New){
                    if(Trigger.oldMap.get(fips.id).Is_Default__c != Trigger.newMap.get(fips.id).Is_Default__c && fips.Is_Default__c && lstFIPSDefault.size()>0){
                        fips.adderror('The default FIPS record already exists. You need to uncheck the "Is Default" checkbox on the current default FIPS record and then you will be able to save this record.');
                    }
                }
            }
        }
        //ends
    }
}