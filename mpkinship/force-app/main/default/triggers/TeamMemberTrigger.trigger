trigger TeamMemberTrigger on Team_Member__c (before insert, before update) {
        
    for(Team_Member__c TM: Trigger.New){
     if(TM.Contact__c!=null){
         TM.Contact_ID__c = TM.Contact__c;
     }
    }
}