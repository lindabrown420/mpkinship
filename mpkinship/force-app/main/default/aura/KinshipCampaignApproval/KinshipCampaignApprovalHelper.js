({
    showToast : function(type, title, message) {
        let strTitle = title.split('\n').join('');
        let strMessage = message.split('\n').join('');
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : strTitle,
            message: strMessage,
            duration:' 4000',
            key: 'info_alt',
            type: type, // success , error, info
            mode: 'pester'//'sticky'
        });
        toastEvent.fire();
    },
    getData : function(component) {
        component.set('v.Spinner', true);
        let action = component.get("c.getApprovalCampignData");
        
        //action.setParams({});
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let data = response.getReturnValue();
                if(data) {
                    component.set("v.data", data);
                }else{
                    this.showToast('error', 'Error', 'Please contact your Administrator for more details.');
                }
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                this.showToast('error', 'Error', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    updatePayment : function(component, status) {
        let data = component.find('paymentrecord').getSelectedRows();
        if(data.length <= 0) {
            this.showToast('info', 'Missing Record', 'Please choose a Check Record before clicking Approve/Reject button.');
            return;
        }
        let lstId = [];
        for(let i = 0 ; i < data.length ; i++){
            lstId.push(data[i].id);
        }
        
        component.set('v.Spinner', true);
        let action = component.get("c.updateCampaignStatus");
        let strNote = component.get('v.arnotes');
        
        action.setParams({
            lstId: lstId,
            status: status,
            strNote: strNote
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let data = response.getReturnValue();
                if(data) {
                    let setid = new Set();
                    for(let i = 0 ; i < data.length ; i++){
                        setid.add(data[i].Id);
                    }
                    let olddata = component.get('v.data');
                    let newdata = [];
                    for(let i = 0 ; i < olddata.length ; i++) {
                        if(!setid.has(olddata[i].id)){
                            newdata.push(olddata[i]);
                        }
                    }
                    component.set("v.data", newdata);
                    this.showToast('success', 'Success', 'You ' + status + ' ' + data.length + ' payments successfully.');
                    component.set('v.isApproveModel', false);
                    component.set('v.isRejectModel', false);
                }else{
                    this.showToast('error', 'Error', 'Please contact your Administrator for more details.');
                }
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].messsage){
                        strErr = errors[0].message;
                    }
                }
                this.showToast('error', 'Error', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    laserReport : function(component, action, popup, camId) {
        component.set('v.Spinner', true);
        action.setParams({
            apId: camId
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let value = response.getReturnValue();
                component.set('v.reportTJ', value);
                component.set('v.' + popup, true);
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                this.showToast('error', 'Error', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    }
})