({
    init : function(component, event, helper) {
        component.set('v.columns', [
            {label: 'Accounting Period', fieldName: 'url', type: 'url', typeAttributes: { 
                label: {
                    fieldName: 'name'
                },
                target: '_blank'
            }},
            {label: 'Requested By', fieldName: 'requestedby', type: 'text'},
            {label: 'Requested Type', fieldName: 'type', type: 'text'},
            {label: 'View LASER Details', type: 'button', initialWidth: 250, typeAttributes: { label: 'View Details', name: 'view_detail', title: 'Click to View Details'}}
        ]);
        helper.getData(component);
    },
    approvedPayment : function(component, event, helper) {
        helper.updatePayment(component, 'Approved')
    },
    rejectPayment : function(component, event, helper) {
        helper.updatePayment(component, 'Rejected');
    },
    closeApproveModel : function(component, event, helper) {
        component.set('v.isApproveModel', false);
    },
    openApproveModel : function(component, event, helper) {
        let data = component.find('paymentrecord').getSelectedRows();
        if(data.length <= 0) {
            helper.showToast('info', 'Missing Record', 'Please choose a check record before clicking the "Approve" button.');
            return;
        }
        component.set('v.isApproveModel', true);
    },
    closeRejectModel : function(component, event, helper) {
        component.set('v.isRejectModel', false);
    },
    openRejectModel : function(component, event, helper) {
        let data = component.find('paymentrecord').getSelectedRows();
        if(data.length <= 0) {
            helper.showToast('info', 'Missing Record', 'Please choose a check record before clicking "Reject" button.');
            return;
        }
        component.set('v.isRejectModel', true);
    },
    handleRowAction: function (component, event, helper) {
        let action = event.getParam('action');
        let row = event.getParam('row');
        let camId = row.id.split('_');
        switch (action.name) {
            case 'view_detail':
                if(row.type === 'LASER Certification') {
                    helper.laserReport(component, component.get("c.createLASERCertificationReport"), 'islaserCertification', camId[0]);
                } else {
                    helper.laserReport(component, component.get("c.createLASERReconciliationReport"), 'islaserReconciliation', camId[0]);
                }
                break;
            case 'view_recon':
                //helper.laserReport(component, component.get("c.createLASERReconciliationReport"), 'islaserReconciliation', row.id);
                break;
            default:
                break;
        }
    },
    closelaserCertificationModel:function(component, event, helper) {
        component.set('v.islaserCertification', false);
    },
    closelaserReconciliationModel:function(component, event, helper) {
        component.set('v.islaserReconciliation', false);
    }
})