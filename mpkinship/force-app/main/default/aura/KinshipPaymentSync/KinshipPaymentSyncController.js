({
    init : function(component, event, helper) {
        let pageRef = component.get("v.pageReference");
        if(pageRef && pageRef.state) {
            component.set('v.pid', pageRef.state.c__pid);
            component.set('v.poid', pageRef.state.c__poid);
        }
        // Inline actions for Pre-check list 
        var actions = [
            { label: 'Update Cost Center', name: 'update_cost_center' },
            { label: 'Write off', name: 'write_off' },
            { label: 'On Hold', name: 'on_hold' }
        ];
        // End inline actions for pre-check list
        component.set('v.columns', [
            {label: 'Invoice', fieldName: 'paymentUrl', type: 'url',sortable: true, typeAttributes: { 
                label: {
                    fieldName: 'paymentNo'
                },
                target: '_blank'
            }},
            {label: 'Category', fieldName: 'category', type: 'text',sortable: true},
            {label: 'Vendor', fieldName: 'vendorUrl', type: 'url',sortable: true, typeAttributes: { 
                label: {
                    fieldName: 'vendorName'
                },
                target: '_blank'
            }},
            {label: 'Vendor Number', fieldName: 'vendorNumber', type: 'text',sortable: true, editable: true},
            {label: 'Purchase Order', fieldName: 'oppUrl', type: 'url',sortable: true, typeAttributes: { 
                label: {
                    fieldName: 'oppName'
                },
                target: '_blank'
            }},
            {label: 'Invoice Schedule Date', fieldName: 'paymentDate', type: 'date-local',sortable: true,
                typeAttributes:{
                    month: "2-digit",
                    day: "2-digit",
                    year:"numeric"
                }
            },
            {label: 'Amount', fieldName: 'amount', type: 'currency',sortable: true, typeAttributes: { currencyCode: 'USD' }},
            {label: 'Approval Status', fieldName: 'status', type: 'text',sortable: true},
            {label: 'Description', fieldName: 'memo', type: 'text',sortable: true},
            {label: 'Cost Center', fieldName: 'costCenterUrl', type: 'url',sortable: true, typeAttributes: { 
                label: {
                    fieldName: 'costCenterName'
                },
                target: '_blank'
            }},
            { type: 'action', typeAttributes: { rowActions: actions } }
        ]);
        
        component.set('v.columns2', [
            {label: 'Invoice', fieldName: 'paymentUrl', type: 'url',sortable: true, typeAttributes: { 
                label: {
                    fieldName: 'paymentNo'
                },
                target: '_blank'
            }},
            {label: 'Category', fieldName: 'category', type: 'text',sortable: true},
            {label: 'Vendor', fieldName: 'vendorUrl', type: 'url',sortable: true, typeAttributes: { 
                label: {
                    fieldName: 'vendorName'
                },
                target: '_blank'
            }},
            {label: 'Vendor Number', fieldName: 'vendorNumber', type: 'text',sortable: true, editable: true},
            {label: 'Purchase Order', fieldName: 'oppUrl', type: 'url',sortable: true, typeAttributes: { 
                label: {
                    fieldName: 'oppName'
                },
                target: '_blank'
            }},
            {label: 'Receipt Number', fieldName: 'receiptNumberUrl', type: 'url',sortable: true, typeAttributes: { 
                label: {
                    fieldName: 'receiptNumber'
                },
                target: '_blank'
            }},
            {label: 'Report of Collections', fieldName: 'rocIdUrl', type: 'url',sortable: true, typeAttributes: { 
                label: {
                    fieldName: 'rocId'
                },
                target: '_blank'
            }},
            {label: 'Invoice Schedule Date', fieldName: 'paymentDate', type: 'date-local',sortable: true,
                typeAttributes:{
                    month: "2-digit",
                    day: "2-digit",
                    year:"numeric"
                }
            },
            {label: 'Amount', fieldName: 'amount', type: 'currency',sortable: true, typeAttributes: { currencyCode: 'USD' }},
            {label: 'Approval Status', fieldName: 'status', type: 'text',sortable: true},
            {label: 'Description', fieldName: 'memo', type: 'text',sortable: true},
            {label: 'Cost Center', fieldName: 'costCenterUrl', type: 'url',sortable: true, typeAttributes: { 
                label: {
                    fieldName: 'costCenterName'
                },
                target: '_blank'
            }},
            { type: 'action', typeAttributes: { rowActions: actions } }
        ]);

        component.set('v.ficacolumns', [
            {label: 'Invoice', fieldName: 'paymentUrl', type: 'url',sortable: true, typeAttributes: { 
                label: {
                    fieldName: 'paymentNo'
                },
                target: '_blank'
            }},
            {label: 'Category', fieldName: 'category', type: 'text',sortable: true},
            {label: 'Vendor', fieldName: 'vendorUrl', type: 'url',sortable: true, typeAttributes: { 
                label: {
                    fieldName: 'vendorName'
                },
                target: '_blank'
            }},
            {label: 'Vendor Number', fieldName: 'vendorNumber', type: 'text',sortable: true, editable: true},
            {label: 'Purchase Order', fieldName: 'oppUrl', type: 'url',sortable: true, typeAttributes: { 
                label: {
                    fieldName: 'oppName'
                },
                target: '_blank'
            }},
            {label: 'Invoice Schedule Date', fieldName: 'paymentDate', type: 'date-local',sortable: true,
                typeAttributes:{
                    month: "2-digit",
                    day: "2-digit",
                    year:"numeric"
                }
            },
            {label: 'Amount', fieldName: 'amount', type: 'currency',sortable: true, typeAttributes: { currencyCode: 'USD' }},
            {label: 'Approval Status', fieldName: 'status', type: 'text',sortable: true},
            {label: 'Description', fieldName: 'memo', type: 'text',sortable: true},
            {label: 'Stage', fieldName: 'stage', type: 'text',sortable: true},
            {label: 'Cost Center', fieldName: 'costCenterUrl', type: 'url',sortable: true, typeAttributes: { 
                label: {
                    fieldName: 'costCenterName'
                },
                target: '_blank'
            }}
        ]);
        
        component.set('v.ficacolumns2', [
            {label: 'Invoice', fieldName: 'paymentUrl', type: 'url',sortable: true, typeAttributes: { 
                label: {
                    fieldName: 'paymentNo'
                },
                target: '_blank'
            }},
            {label: 'Category', fieldName: 'category', type: 'text',sortable: true},
            {label: 'Vendor', fieldName: 'vendorUrl', type: 'url',sortable: true, typeAttributes: { 
                label: {
                    fieldName: 'vendorName'
                },
                target: '_blank'
            }},
            {label: 'Vendor Number', fieldName: 'vendorNumber', type: 'text',sortable: true, editable: true},
            {label: 'Purchase Order', fieldName: 'oppUrl', type: 'url',sortable: true, typeAttributes: { 
                label: {
                    fieldName: 'oppName'
                },
                target: '_blank'
            }},
            {label: 'Receipt Number', fieldName: 'receiptNumberUrl', type: 'url',sortable: true, typeAttributes: { 
                label: {
                    fieldName: 'receiptNumber'
                },
                target: '_blank'
            }},
            {label: 'Report of Collections', fieldName: 'rocIdUrl', type: 'url',sortable: true, typeAttributes: { 
                label: {
                    fieldName: 'rocId'
                },
                target: '_blank'
            }},
            {label: 'Invoice Schedule Date', fieldName: 'paymentDate', type: 'date-local',sortable: true,
                typeAttributes:{
                    month: "2-digit",
                    day: "2-digit",
                    year:"numeric"
                }
            },
            {label: 'Amount', fieldName: 'amount', type: 'currency',sortable: true, typeAttributes: { currencyCode: 'USD' }},
            {label: 'Approval Status', fieldName: 'status', type: 'text',sortable: true},
            {label: 'Description', fieldName: 'memo', type: 'text',sortable: true},
            {label: 'Stage', fieldName: 'stage', type: 'text',sortable: true},
            {label: 'Cost Center', fieldName: 'costCenterUrl', type: 'url',sortable: true, typeAttributes: { 
                label: {
                    fieldName: 'costCenterName'
                },
                target: '_blank'
            }}
        ]);

        component.set('v.apcolumns', [
            {label: 'Invoice', fieldName: 'paymentUrl', type: 'url',sortable: true, typeAttributes: { 
                label: {
                    fieldName: 'paymentNo'
                },
                target: '_blank'
            }},
            {label: 'Category', fieldName: 'category', type: 'text',sortable: true},
            {label: 'Vendor', fieldName: 'vendorUrl', type: 'url',sortable: true, typeAttributes: { 
                label: {
                    fieldName: 'vendorName'
                },
                target: '_blank'
            }},
            {label: 'Vendor Number', fieldName: 'vendorNumber', type: 'text',sortable: true, editable: true},
            {label: 'Purchase Order', fieldName: 'oppUrl', type: 'url',sortable: true, typeAttributes: { 
                label: {
                    fieldName: 'oppName'
                },
                target: '_blank'
            }},
            {label: 'Invoice Schedule Date', fieldName: 'paymentDate', type: 'date-local',sortable: true,
                typeAttributes:{
                    month: "2-digit",
                    day: "2-digit",
                    year:"numeric"
                }
            },
            {label: 'Amount', fieldName: 'amount', type: 'currency',sortable: true, typeAttributes: { currencyCode: 'USD' }},
            {label: 'Approved By', fieldName: 'arby', type: 'text',sortable: true},
            {label: 'Description', fieldName: 'memo', type: 'text',sortable: true},
            {label: 'Record Type', fieldName: 'recordType', type: 'text',sortable: true}
        ]);

        // Check Run
        component.set('v.crsteps', [
            //KINMP-322 Ankita reverted
            { label: 'Reconsolidation Invoices', value: 'step1' },
            //{ label: 'Assign Check Date', value: 'step1' },
            //KINMP-322 Ankita reverted
            { label: 'Draft Check', value: 'step2' },
            //{ label: 'Create Checks', value: 'step2' },
            { label: 'Check-Run Completed', value: 'step3' }
        ]);
        component.set('v.accurrentStep', 'step1');
        let today = new Date();
        component.set('v.checkrunDate', today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate());

        helper.fetchData(component,'Payment__c','Status__c','','statusoptions');
        helper.getData(component, true);
        component.set('v.isSeperateCheck', ($A.get("$Label.c.CR_SEPERATE_DISABLED") === "true"?false:true));
    },
    doneRendering : function(component, event, helper) {
        if(window.location && window.location.pathname) {
            let isBack = component.get('v.isBack');
            if(window.location.pathname.indexOf('Pay_Invoices') !== -1) {
                if(!isBack) {
                    component.set('v.ctvalue',[]);
                    component.set('v.isAllCT', true);
                    component.set('v.rtvalue',[]);
                    component.set('v.isAllRT', true);
                    component.set("v.vendorSelection", []);
                    component.set("v.cateSelection", []);
                    component.set("v.oppSelection", []);
                    component.set("v.paymentSelection", []);
                    component.set("v.caseSelection", []);
                    component.set("v.campSelection", []);
                    component.set("v.startDate", null);
                    component.set("v.endDate", null);
                    component.set('v.isBack', true);
                    component.set('v.accurrentStep', 'step1');
                    /*let pageRef = component.get("v.pageReference");
                    if(pageRef && pageRef.state) {
                        component.set('v.pid', pageRef.state.c__pid);
                    }*/
                    let url = window.location.href;
                    if(url) {
                        let urlSplit = url.split('c__pid=');
                        if(urlSplit.length == 2) {
                            component.set('v.pid', urlSplit[1]);
                        }
                        urlSplit = url.split('c__poid=');
                        if(urlSplit.length == 2) {
                            component.set('v.poid', urlSplit[1]);
                        }
                    }
                    helper.getData(component, true);
                }
            } else {
                if(isBack) {
                    component.set('v.isBack', false);
                }
            }
        }
    },
    searchPayment : function(component, event, helper) {
        helper.getData(component);
    },
    updateAll : function(component, event, helper) {
        let data = component.get('v.data');
        for(let i = 0 ; i < data.length ; i++){
            if(data[i].status !== 'Approved'){
                helper.showToast('info', 'Non-Approved Payment', 'Your selection has non-approved payments. You can only pay for an approved payment record. Please select again.');
                return true;
            }
        }
        let lstUpdate = [];
        for(let i = 0 ; i < data.length ; i++) {
            lstUpdate.push(data[i].id);
        }
        helper.syncPayment(component, lstUpdate);
    },
    updateSelected : function(component, event, helper) {
        let data = component.find('paymentrecord').getSelectedRows();
        if(data.length <= 0) {
            helper.showToast('info', 'Missing Record', 'Please choose a check record before clicking the update button.');
            return;
        }
        for(let i = 0 ; i < data.length ; i++){
            if(data[i].status !== 'Approved'){
                helper.showToast('info',  'Non-Approved Payment', 'Your selection has non-approved payments. You can only pay for an approved payment record. Please select again.');
                return true;
            }
        }
        let lstUpdate = [];
        for(let i = 0 ; i < data.length ; i++) {
            lstUpdate.push(data[i].id);
        }
        helper.syncPayment(component, lstUpdate);
    },
    gotoCheckListView : function(component, event, helper) {
        let navEvent = $A.get("e.force:navigateToList");
        navEvent.setParams({
            "listViewId": '00B18000001yMJBEA2',
            "listViewName": null,
            "scope": "Kinship_Check__c"
        });
        navEvent.fire();
    },
    gotoCheckRunScreen : function(component, event, helper) {
        window.open('/lightning/n/Post_Check_List', '_blank');
    },
    vendorLookupSearch : function(component, event, helper) {
        const lookupComponent = event.getSource();
        const serverSearchAction = component.get('c.kinshipSearchLookup');
        serverSearchAction.setParam('anOptionalParam', 'Account');
        lookupComponent.search(serverSearchAction);
    },
    clearVendorErrorsOnChange: function(component, event, helper) {
        const selection = component.get('v.vendorSelection');
        const errors = component.get('v.vendorErrors');

        if (selection.length && errors.length) {
            component.set('v.vendorErrors', []);
        }
    },
    oppLookupSearch : function(component, event, helper) {
        const lookupComponent = event.getSource();
        const serverSearchAction = component.get('c.kinshipSearchLookup');
        serverSearchAction.setParam('anOptionalParam', 'Opportunity');
        lookupComponent.search(serverSearchAction);
    },
    clearOppErrorsOnChange: function(component, event, helper) {
        const selection = component.get('v.oppSelection');
        const errors = component.get('v.oppErrors');

        if (selection.length && errors.length) {
            component.set('v.oppErrors', []);
        }
    },
    caLookupSearch : function(component, event, helper) {
        const lookupComponent = event.getSource();
        const serverSearchAction = component.get('c.kinshipSearchLookup');
        serverSearchAction.setParam('anOptionalParam', 'CA');
        lookupComponent.search(serverSearchAction);
    },
    clearcaErrorsOnChange: function(component, event, helper) {
        const selection = component.get('v.caSelection');
        const errors = component.get('v.caErrors');

        if (selection.length && errors.length) {
            component.set('v.caErrors', []);
        }
    },
    cateLookupSearch : function(component, event, helper) {
        const lookupComponent = event.getSource();
        const serverSearchAction = component.get('c.kinshipSearchLookup');
        serverSearchAction.setParam('anOptionalParam', 'Category__c');
        lookupComponent.search(serverSearchAction);
    },
    clearCateErrorsOnChange: function(component, event, helper) {
        const selection = component.get('v.cateSelection');
        const errors = component.get('v.cateErrors');

        if (selection.length && errors.length) {
            component.set('v.cateErrors', []);
        }
    },
    paymentLookupSearch : function(component, event, helper) {
        const lookupComponent = event.getSource();
        const serverSearchAction = component.get('c.kinshipSearchLookup');
        serverSearchAction.setParam('anOptionalParam', 'Payment');
        lookupComponent.search(serverSearchAction);
    },
    clearPaymentErrorsOnChange: function(component, event, helper) {
        const selection = component.get('v.paymentSelection');
        const errors = component.get('v.paymentErrors');

        if (selection.length && errors.length) {
            component.set('v.paymentErrors', []);
        }
    },
    caseLookupSearch : function(component, event, helper) {
        const lookupComponent = event.getSource();
        const serverSearchAction = component.get('c.kinshipSearchLookup');
        serverSearchAction.setParam('anOptionalParam', 'Employee');
        lookupComponent.search(serverSearchAction);
    },
    clearCaseErrorsOnChange: function(component, event, helper) {
        const selection = component.get('v.caseSelection');
        const errors = component.get('v.caseErrors');

        if (selection.length && errors.length) {
            component.set('v.caseErrors', []);
        }
    },
    userLookupSearch : function(component, event, helper) {
        const lookupComponent = event.getSource();
        const serverSearchAction = component.get('c.kinshipSearchLookup');
        serverSearchAction.setParam('anOptionalParam', 'User');
        lookupComponent.search(serverSearchAction);
    },
    clearUserErrorsOnChange: function(component, event, helper) {
        const selection = component.get('v.userSelection');
        const errors = component.get('v.userErrors');

        if (selection.length && errors.length) {
            component.set('v.userErrors', []);
        }
    },
    campLookupSearch : function(component, event, helper) {
        const lookupComponent = event.getSource();
        const serverSearchAction = component.get('c.kinshipSearchLookup');
        serverSearchAction.setParam('anOptionalParam', 'Campaign');
        lookupComponent.search(serverSearchAction);
    },
    clearCampErrorsOnChange: function(component, event, helper) {
        const selection = component.get('v.campSelection');
        const errors = component.get('v.campErrors');

        if (selection.length && errors.length) {
            component.set('v.campErrors', []);
        }
    },
    submitForApproval: function(component, event, helper) {
        let precheckData = component.get('v.precheckData');
        let data = [];
        for(let i = 0 ; i < precheckData.length ; i++) {
            let temp = precheckData[i].selectedRows;
            if(temp.length > 0) {
                data = data.concat(temp);
            }
        }
        let userId = $A.get("$SObjectType.CurrentUser.Id");
        let userSelection = component.get("v.userSelection");
        
        if(!userSelection || userSelection.length == 0) {
            helper.showToast('info', 'Missing Approver', 'Please choose the Approver before clicking the Request For Approval.');
            return;
        }
        let approvalUser = userSelection[0].id;
        if(approvalUser === userId){
            helper.showToast('info', 'Wrong Approver', 'You cannot approve the Invoice Requested by You.');
            return;
        }
        
        component.set('v.Spinner', true);
        let action = component.get("c.submitPaymentForApproval");//component.get("c.syncPaymentToCheck");
        action.setParams({
            lstData: data,
            userId: approvalUser
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let message = data.length + ' payments have been successfully submitted for approval. An email notification has been sent to the Approver requesting the approval.';
                helper.showToast('success', 'Success!', message);
                //let data = component.find('paymentrecord').getSelectedRows();
                let precheckData = component.get('v.precheckData');
                let waData = component.get('v.waData');
                let setid = new Set();
                for(let i = 0 ; i < precheckData.length ; i++) {
                    let temp = precheckData[i].selectedRows;
                    if(temp.length > 0) {
                        //data = data.concat(temp);
                        for(let i = 0 ; i < temp.length ; i++){
                            setid.add(temp[i].id);
                        }
                    }
                }
                let olddata = component.get('v.data');
                for(let i = 0 ; i < olddata.length ; i++) {
                    if(setid.has(olddata[i].id)){
                        olddata[i].status = 'Submitted For Approval';
                        waData.push(olddata[i]);
                    }
                }
                component.set('v.data', olddata);

                for(let i = 0 ; i < precheckData.length ; i++) {
                    let temp = [];
                    let totalTemp = 0;
                    for(let j = 0 ; j < precheckData[i].data.length ; j++) {
                        if(setid.has(precheckData[i].data[j].id)){
                            precheckData[i].data[j].status = 'Submitted For Approval';
                        } else {
                            temp.push(precheckData[i].data[j]);
                            totalTemp += precheckData[i].data[j].amount;
                        }
                    }
                    precheckData[i].data = temp;
                    precheckData[i].total = totalTemp;
                }
                component.set('v.precheckData', precheckData);
                component.set('v.waData', waData);
                component.set('v.isApprover', false);
                helper.getData(component, true);
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                helper.showToast('error', 'Error', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    closeApproverModel: function(component, event, helper) {
        component.set('v.isApprover', false);
    },
    openApproverModel: function(component, event, helper) {
        let precheckData = component.get('v.precheckData');
        let data = [];
        for(let i = 0 ; i < precheckData.length ; i++) {
            let temp = precheckData[i].selectedRows;
            if(temp.length > 0) {
                data = data.concat(temp);
            }
        }
        if(data.length <= 0) {
            helper.showToast('info', 'Missing Record', 'Please choose a Non-Approved Record before clicking the Request For Approval.');
            return;
        }
        let preCateData = [];
        let strTemp = '';
        let total = 0;
        for(let i = 0 ; i < data.length ; i++){
            if(!data[i].fips){
                component.set('v.isFIPS', true);
                return true;
            }
            if(data[i].localityAccountCode === '61090' && data[i].stage === 'Pending'){
                helper.showToast('info', 'Pending FICA', 'Please choose a Non-Pending FICA Record before clicking the Request For Approval.');
                return true;
            }
            if(data[i].status === 'Approved'){
                helper.showToast('info', 'Non Approved Payment', 'Please choose a Non-Approved Record before clicking the Request For Approval.');
                return true;
            }

            if(data[i].status === 'Submitted For Approval'){
                helper.showToast('info', 'Non-Submitted For Approval Status', 'Please choose a record in Non-Submitted For Approval status before clicking the Request For Approval.');
                return true;
            }

            if(!data[i].vendorNumber){
                helper.showToast('info', 'Missing Vendor Number', 'One of the selected record has Vendor Number is blank, please update the vendor number before start Check Run Process.');
                return true;
            }
            
            /*if(!data[i].costCenterId){
                helper.showToast('info', 'Missing Cost Center', 'One of the selected record has Cost Center is blank, please update the Cost Center before start Check Run Process.');
                return true;
            }*/

            if(strTemp.indexOf(data[i].category) !== -1) {
                for(let k = 0 ; k < preCateData.length ; k++) {
                    if(preCateData[k].cate === data[i].category) {
                        preCateData[k].total += data[i].amount;
                        break;
                    }
                }
            } else {
                preCateData.push({'cate': data[i].category, 'total': data[i].amount});
            }
            total += data[i].amount;
        }
        preCateData.push({'cate': 'TOTAL', 'total': total});
        
        component.set('v.isApprover', true);
        component.set('v.preCateData', preCateData);
    },
    exportPreCheck: function(component, event, helper) {
        let data = component.get("v.preCateData");
        if (data.length > 0) {
            let csvStringResult, counter, keys, columnDivider, lineDivider;
            columnDivider = ',';
            lineDivider =  '\n';

            let headers = ['Category', 'Total'];
            keys = ['cate', 'total'];
            csvStringResult = '';
            csvStringResult += headers.join(columnDivider);
            csvStringResult += lineDivider;
            
            for(let i=0; i < data.length; i++){  
                counter = 0;
            
                for(let sTempkey in keys) {
                    let skey = keys[sTempkey] ;  
                    if(counter > 0){ 
                        csvStringResult += columnDivider; 
                    }   
                    csvStringResult += '"' + (data[i][skey]?data[i][skey]:'0') + '"'; 
                    counter++;
                }
                csvStringResult += lineDivider;
            }
            csvStringResult += lineDivider;

            let hiddenElement = document.createElement('a');
            hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csvStringResult);
            hiddenElement.target = '_self';
            hiddenElement.download = 'Summary Export.csv';//this.getName(component, data[0], true);
            document.body.appendChild(hiddenElement);
            hiddenElement.click();
        } else {
            helper.showToast('info', 'No Data.', 'There is no record for this Accounting Period.');
        }
    },
    handlePreCheckSelection: function(component, event, helper) {
        let source = event.getSource();
        if(source) {
            let index = source.get('v.class');
            let i = parseInt(index);
            let precheckData = component.get('v.precheckData');
            let selectedRows = event.getParam('selectedRows');
            let selectedData = [];
            for(let i = 0 ; i < selectedRows.length ; i++){
                if(selectedRows[i].status !== 'Submitted For Approval'){
                    selectedData.push(selectedRows[i]);
                }
            }
            precheckData[i].selectedRows = selectedData;
            component.set('v.precheckData', precheckData);
        }
    },
    closeCheckRunModel: function(component, event, helper) {
        component.set('v.isCheckRun', false);
        helper.resetCheckRunModel(component);
    },
    openCheckRunModel: function(component, event, helper) {
        let data = [];
        let apdata = component.get('v.apdata');
        for(let i = 0 ; i < apdata.length ; i++){
            data = data.concat(apdata[i].selectedRows);
        }
        if(data.length <= 0) {
            helper.showToast('info', 'Missing Record', 'Please choose a Check Record before clicking the update Button.');
            return;
        }
        let strFIPS = '';
        let strFY = '';
        for(let i = 0 ; i < data.length ; i++){
            if(data[i].status !== 'Approved'){
                helper.showToast('info', 'Non-Approved Payment', 'Your selection has Non-Approved Payments. You can only pay for an Approved Payment Record. Please select again.');
                return true;
            }
            if(!data[i].vendorNumber){
                helper.showToast('info', 'Missing Vendor Number', 'One of the selected record has Vendor Number is blank, please update the vendor number before start Check Run Process.');
                return true;
            }
            if(!strFIPS) {
                strFIPS = data[i].fips;
            }
            if(strFIPS !== data[i].fips) {
                helper.showToast('info', 'Info', 'The same check can not be used for payments with two different FIPS, these checks should generate separately.');
                return true;
            }
            if(!strFY) {
                strFY = data[i].fiscalYear;
            }
            if(strFY !== data[i].fiscalYear) {
                helper.showToast('info', 'Info', 'The same check can not be used for payments with different fiscal years, these checks should generate separately.');
                return true;
            }
        }
        let lstUpdate = [];
        for(let i = 0 ; i < data.length ; i++) {
            lstUpdate.push(data[i].id);
        }
        
        let checkData = helper.convertDatatoCheckData(component, event, data);
        component.set('v.checkData', checkData);
        component.set('v.isCheckRun', true);
    },
    gotoNextStep: function(component, event, helper) {
        helper.nextStep(component);
    },
    gotoPreviousStep: function(component, event, helper) {
        helper.previousStep(component);
    },
    completeCheckRunModel: function(component, event, helper) {
        /*let isPrint = component.get('v.isPrint');
        if(!isPrint) {
            helper.showToast('info', 'Check is not printed yet.', 'You have not printed the Check yet. Please print the check first.');
            return;
        }*/
        component.set('v.isCheckRun', false);
        helper.resetCheckRunModel(component);
    },
    seperateCheck: function(component, event, helper) {
        let accesskey = event.getSource().get('v.accesskey');
        if(accesskey) {
            let ids = accesskey.split('_');
            let vendorId = ids[0];
            let paymentId = ids[1];
            helper.serperateCheckDataWithPayment(component, vendorId, paymentId);
        }
    },
    generateDraftCheck: function(component, event, helper) {
        let checkrunDate = component.get('v.checkrunDate');
        if(!checkrunDate) {
            helper.showToast('error', 'Missing Check Run Date.', 'Please input Check Run Date.');
            return;
        }
        if(helper.getDateFromStr(checkrunDate) > new Date()) {
            helper.showToast('error', 'Wrong Check Run Date.', 'Check Run Date can not in the future.');
            return;
        }
        let checkData = component.get('v.checkData');
        for(let i = 0 ; i < checkData.length ; i++){
            let total = 0;
            for(let j = 0 ; j < checkData[i].lstPayment.length ; j++){
                total += checkData[i].lstPayment[j].amount;
            }
            checkData[i].amount = total;
            checkData[i].status = 'Issued';
            checkData[i].isPrinted = false;
            checkData[i].description = '';
            checkData[i].issueDate = helper.getDateFromStr(checkrunDate);
        }
        component.set('v.checkData', checkData);
        helper.nextStep(component);
    },
    saveCheck : function(component, event, helper) {
        // check lstPayment Size
        let checkData = component.get('v.checkData');
        for(let i = 0 ; i < checkData.length ; i++){
            if(checkData[i].lstPayment && checkData[i].lstPayment.length > 30){
                helper.showToast('info', '30 Invoices per Check is permitted', 'Each check can print upto 30 Invoices. Please split into multiple Checks.');
                return;
            }
        }

        component.set('v.Spinner', true);
        let action = component.get("c.savePaymentToCheck");
        action.setParams({
            lstPayment: checkData,
            checkRunDate: helper.getDateFromStr(component.get('v.checkrunDate'))
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let data = response.getReturnValue();
                let message = 'You have created ' + (data?data.length:'') + ' check records successfully.';
                helper.showToast('success', 'Success', message);
                let checkData = component.get('v.checkData');
                let checkrunid = '';
                let setId = new Set();
                for(let i = 0 ; i < data.length ; i++){
                    checkData[i].id = data[i].Id;
                    checkData[i].checkName = data[i].Name;
                    for(let j = 0 ; j < checkData[i].lstPayment.length ; j++){
                        setId.add(checkData[i].lstPayment[j].id);
                    }
                    checkData[i].checkNo = data[i].Check_No__c;
                    checkData[i].issueDate = data[i].Issue_Date__c;
                    checkrunid = data[i].Check_Run__c;
                }
                component.set('v.checkData', checkData);
                let paymentData = component.get('v.data');
                let newPaymentData = [];
                for(let i = 0 ; i < paymentData.length ; i++){
                    if(!setId.has(paymentData[i].id)){
                        newPaymentData.push(paymentData[i]);
                    }
                }
                component.set('v.data', newPaymentData);
                component.set('v.checkrunid', checkrunid);
                helper.nextStep(component);
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;  
                    }
                }
                helper.showToast('error', 'Error', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    closeCheckNoModel: function(component, event, helper) {
        component.set('v.isCheckNoModel', false);
        //helper.resetCheckRunModel(component);
    },
    openCheckNoModel: function(component, event, helper) {
        let isPrint = component.get('v.isPrint');
        if(isPrint) {
            helper.showToast('info', 'Check has been printed already', 'Check has been printed already. Please start a new Check Run process.');
            return;
        }
        component.set('v.isCheckNoModel', true);
    },
    printCheck : function(component, event, helper) {
        let startNumber = component.get('v.beginNumber');
        if(!startNumber) {
            helper.showToast('info', 'Missing Start Number', 'Please input Start Number.');
            return;
        }
        let istartNumber = 0;
        try{
            istartNumber = parseInt(startNumber);
            if(isNaN(istartNumber)){
                helper.showToast('info', 'Invalid Number', 'Please input a valid Start Number.');
                return;
            }
        }catch(e){
            helper.showToast('info', 'Invalid Number', 'Please input a valid Start Number.');
            return;
        }
        let checkData = component.get('v.checkData');
        let lstCheck = [];
        for(let i = 0 ; i < checkData.length ; i++) {
            lstCheck.push(checkData[i].id);
        }

        component.set('v.Spinner', true);
        let action = component.get("c.updateCheckNo");
        action.setParams({
            lstId: lstCheck,
            startNumber: istartNumber
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let data = response.getReturnValue();
                let checkData = component.get('v.checkData');
                for(let i = 0 ; i < data.length ; i++){
                    checkData[i].checkNo = data[i].Check_No__c;
                    checkData[i].isPrinted = true;
                }
                component.set('v.isPrint', true);
                component.set('v.checkData', checkData);
                component.set('v.isCheckNoModel', false);

                /*let newLocation = window.location.protocol + '//' + window.location.host + '/lightning/o/Loop__Document_Request__c/list?filterName=00B18000001yOPFEA2';
                let redirectUrl = window.location.protocol + '//' + window.location.host + '/apex/loop__masslooplus?';
                redirectUrl += '&recordIds='+lstCheck.join(',');
                redirectUrl += '&sessionId=' + component.get('v.sessionId');
                redirectUrl += '&ddpIds=a2718000006qxUjAAI';
                redirectUrl += '&deploy=a2518000001WV32AAG';
                redirectUrl += '&autorun=true';
                redirectUrl += '&retURL='+newLocation;
                window.open(redirectUrl ,'_blank');*/
                let checkrunid = component.get('v.checkrunid');

                let documentPackageId = $A.get('$Label.c.KINSHIP_CR_DDP');
                let deliveryOptionId = $A.get('$Label.c.KINSHIP_CR_OPTION');
                let recordIds = checkrunid;
                let sObjectType = 'Check_Run__c';
                let url = window.location.protocol + '//' + window.location.host + '/apex/ProcessDdp';
                url = url + '?ddpId=' + documentPackageId;
                url = url + '&deliveryOptionId=' + deliveryOptionId;
                url = url + '&ids=' + recordIds;
                url = url + '&sObjectType=' + sObjectType;
                window.open(url ,'_blank');
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                if(strErr.indexOf('DUPLICATE_VALUE') !== -1){
                    helper.showToast('error', 'Duplicate Check Number', 'Duplicate Check Number found. Please input another Start Check Number.');
                } else {
                    helper.showToast('error', 'Error', strErr);
                }
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    exportToCSV: function(component, event, helper) {
        component.set('v.Spinner', true);
        let data = component.get('v.checkData');
        let lstCheck = [];
        for(let i = 0 ; i < data.length ; i++) {
            lstCheck.push(data[i].id);
        }
        let action = component.get("c.getExportData");
        action.setParams({
            lstCheck: lstCheck
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let data = response.getReturnValue();
                let csv = helper.convertArrayOfObjectsToCSV(component, data);   
                if (csv == null){return;} 
                
                /*var hiddenElement = document.createElement('a');
                hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURIComponent(csv);
                hiddenElement.target = '_self';
                hiddenElement.download = 'CheckTXT.txt';
                document.body.appendChild(hiddenElement);
                hiddenElement.click();*/
                
                let action = component.get("c.insertCheckFile");
                action.setParams({
                    body: csv,
                    fileName: 'CheckTXT.txt',
                    crId: data[0].checkRun
                });
                action.setCallback(this, function(response) {
                    var hiddenElement = document.createElement('a');
                    hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURIComponent(csv);
                    hiddenElement.target = '_self';
                    hiddenElement.download = 'CheckTXT.txt';
                    document.body.appendChild(hiddenElement);
                    hiddenElement.click();
                });
                $A.enqueueAction(action);
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                        console.log('**Val of error'+strErr);
                    }
                }
                helper.showToast('error', 'Error', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    exportToCSVUpdated: function(component, event, helper) {
        component.set("v.gTotal",0);
         let precheckData = component.get('v.precheckData');
         let data = component.get('v.precheckData');
        
        let selected = [];
        for(let i = 0 ; i < precheckData.length ; i++) {
            let temp = precheckData[i].selectedRows;
            if(temp.length > 0) {
                            console.log('selectedRows : ' + precheckData[i].selectedRows.length);
                selected.push(temp);
            }
        }
        if(selected.length <= 0) {
            helper.showToast('info', 'Missing Record', 'Please select records.');
            return;
        }
        var check = true;
        var isChild = false;
        var lastFipName;
        var isFinal = false;
        var STotal = 0;
        if(data.length > 0){
            for(let i=0; i < data.length; i++){
                if(precheckData[i].selectedRows.length > 0){
                    if(i - STotal > 1){
                        STotal = STotal + (i - STotal);
                    }
                    else if(i - STotal == 1){
                        STotal += 1;
                    }
                    
                    
                }
            }
        }
        if(data.length > 0){
            for(let i=0; i < data.length; i++){
                //debugger;
                //STotal = precheckData[i].selectedRows.length;
                if(precheckData[i].selectedRows.length > 0){
                    if(i==STotal){
                        isFinal = true;
                    }
                    if(data.length > 1){
                        if(i > 0){
                            if(data[i].fipsName != data[i-1].fipsName){
                                if (csv == null){return;} 
                                var hiddenElement = document.createElement('a');
                                hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURIComponent(csv);
                                hiddenElement.target = '_self';
                                hiddenElement.download = data[i-1].fipsName+'.csv';
                                document.body.appendChild(hiddenElement);
                                hiddenElement.click();
                                check = true;
                                csv = null;
                                
                            }
                            if(!data[i].isChild){
                                var csv = helper.convertArrayOfObjectsToCSVUpdated(isFinal,data[i].selectedRows,component, data[i],check,csv);
                                check = false;
                            }
                            else{
                                isChild = true
                                if(data[i].data.length > 0){
                                    for(let j=0; j < data[i].data.length; j++){
                                        if(data[i].data.length > 1){
                                            if(j > 0){
                                                if(data[i].data[j].poRecordTypeName != data[i].data[j-1].poRecordTypeName){
                                                    check = true;
                                                }
                                                var csv = helper.convertArrayOfObjectsToCSVUpdated(isFinal,data[i].selectedRows,component, data[i].data[j],check,csv,isChild,data[i]);
                                                check = false;
                                            }
                                            else {
                                                var csv = helper.convertArrayOfObjectsToCSVUpdated(isFinal,data[i].selectedRows,component, data[i].data[j],check,csv,isChild,data[i]);
                                                check = false;
                                            }
                                        }
                                        else{
                                            var csv = helper.convertArrayOfObjectsToCSVUpdated(isFinal,data[i].selectedRows,component, data[i].data[j],check,csv,isChild,data[i]);
                                        }
                                        isChild = false;
                                    }
                                    
                                }
                                
                            }
                            
                        }
                        else {
                            //isFinal = true;
                            var csv = helper.convertArrayOfObjectsToCSVUpdated(isFinal,data[i].selectedRows,component, data[i],check);
                            check = false;
                        }
                    }
                    else{
                        isFinal = true;
                        var csv = helper.convertArrayOfObjectsToCSVUpdated(isFinal,data[i].selectedRows,component, data[i],check);
                    }
                    lastFipName = data[i].fipsName;
                }
            }
            console.log('csv: ' + csv);
            if (csv == null){return;} 
            let grandtotal = component.get("v.gTotal");
            component.set("v.gTotal",0);
            var hiddenElement = document.createElement('a');
            hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURIComponent(csv);
            hiddenElement.target = '_self';
            hiddenElement.download = lastFipName+'.csv';
            document.body.appendChild(hiddenElement);
            hiddenElement.click();
        }
        
    },
    printPdf : function(component, event, helper) {
        let data = component.get("v.vendorSelection");
        let lstVendor = [];
        if(data) {
            data.forEach(element => {
                lstVendor.push(element.id);
            });
        }
        data = component.get("v.cateSelection");
        let lstCategory = [];
        if(data) {
            data.forEach(element => {
                lstCategory.push(element.id);
            });
        }
        data = component.get("v.oppSelection");
        let lstOpp = [];
        if(data) {
            data.forEach(element => {
                lstOpp.push(element.id);
            });
        }
        data = component.get("v.caSelection");
        if(data) {
            data.forEach(element => {
                lstOpp.push(element.id);
            });
        }
        data = component.get("v.paymentSelection");
        let lstPayment = [];
        if(data) {
            data.forEach(element => {
                lstPayment.push(element.id);
            });
        }
        data = component.get("v.caseSelection");
        let caseWorker = [];
        if(data) {
            data.forEach(element => {
                caseWorker.push(element.id);
            });
        }
        data = component.get("v.campSelection");
        let camp = [];
        if(data) {
            data.forEach(element => {
                camp.push(element.id);
            });
        }
        let pid = component.get('v.pid');
        if(pid) {
            lstPayment.push(pid);
        }
        let poid = component.get('v.poid');
        if(poid) {
            lstOpp.push(poid);
        }
        data = component.get('v.precheckData')
        if(data) {
            data.forEach(element => {
                element.selectedRows.forEach( (selectedRow) => {
                    lstPayment.push(selectedRow.id);
                });
            });
        }
        if(lstPayment.length <= 0) {
            helper.showToast('info', 'Missing Record', 'Please select records.');
            return;
        }
        //let strOppRT = component.get("v.strOppRT");
        let startDate = component.get("v.startDate");
        let endDate = component.get("v.endDate");
        let status = component.get('v.strStatus');
        let rtvalue = component.get('v.rtvalue');
        let ctvalue = component.get('v.ctvalue');
        let startAmount = component.get('v.startAmount');
        if(!startAmount && startAmount !== 0){
            startAmount = null;
        }
        let endAmount = component.get('v.endAmount');
        if(!endAmount && endAmount !== 0){
            endAmount = null;
        }
      
        window.open("/apex/Pay_Invoice_PDF?lstVendor="+lstVendor+"&lstCategory="+lstCategory+"&lstOpp="+lstOpp+"&lstPayment="+lstPayment+"&caseWorker="+caseWorker+"&camp="+camp+"&startDate="+startDate+"&endDate="+endDate+"&rtvalue="+rtvalue+"&ctvalue="+ctvalue+"&startAmount="+startAmount+"&endAmount="+endAmount+"&status="+status, '_blank');
        /*var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url":"/apex/Pay_Invoice_PDF?id="+"0063R000001rhaxQAA"
        });
        urlEvent.fire(); */
    },
    handleaschanged: function(component, event, helper){
        
    },
    handlertchanged : function(component, event, helper){
        let isAllRT = component.get('v.isAllRT');
        if(isAllRT) {
            component.set('v.rtvalue', []);
        }
    },
    handlectchanged : function(component, event, helper){
        let isAllRT = component.get('v.isAllCT');
        if(isAllRT) {
            component.set('v.ctvalue', []);
        }
    },
    handleDataSort: function(cmp, event, helper) {
        //helper.handleSort(cmp, event, 'sortDirection', 'data', 'sortedBy');
        let sortedBy = event.getParam('fieldName');
        let sortDirection = event.getParam('sortDirection');
        let index = event.getSource().get('v.class');
        let i = parseInt(index);
        let precheckData = cmp.get('v.precheckData');
        let cloneData = precheckData[i].data.slice(0);
        cloneData.sort((helper.sortBy(sortedBy, sortDirection === 'asc' ? 1 : -1)));
        precheckData[i].data = cloneData;
        precheckData[i].sortDirection = sortDirection;
        precheckData[i].sortedBy = sortedBy;
        cmp.set('v.precheckData', precheckData);
    },
    handleAPDataSort: function(cmp, event, helper) {
        helper.handleSort(cmp, event, 'apsortDirection', 'apdata', 'apsortedBy');
    },
    handleSaveEdition: function (component, event, helper) {
        let draft = event.getParam('draftValues');
        let str = '';
        let draftValues = [];
        let data = component.get('v.data');
        let apdata = component.get('v.apdata');
        
        for(let i = 0 ; i < draft.length ; i++) {
            for(let j = 0 ; j < data.length ; j++) {
                if(draft[i].id === data[j].id) {
                    draft[i].vendorId = data[j].vendorId;
                    break;
                }
            }
            for(let j = 0 ; j < apdata.length ; j++) {
                if(draft[i].id === apdata[j].id) {
                    draft[i].vendorId = apdata[j].vendorId;
                    break;
                }
            }
        }
        for(let i = 0 ; i < draft.length ; i++) {
            if(str.indexOf(draft[i].vendorId) === -1) {
                draftValues.push(draft[i]);
                str += ',' + draft[i].vendorId;
            }
        }
        console.log(draftValues);
        
        for(let i = 0 ; i < data.length ; i++){
            for(let j = 0 ; j < draftValues.length ; j++) {
                if(data[i].id === draftValues[j].id) {
                    data[i].vendorNumber = draftValues[j].vendorNumber;
                    draftValues[j].vendorId = data[i].vendorId;
                    draftValues[j].lvnId = data[i].lvnId;
                    draftValues[j].fips = data[i].fips;
                    draftValues[j].fipsName = data[i].fipsName;
                }
            }
        }
        for(let i = 0 ; i < apdata.length ; i++){
            for(let k = 0 ; k < apdata[i].data.length ; k++){
                for(let j = 0 ; j < draftValues.length ; j++) {
                    if(apdata[i].data[k].id === draftValues[j].id) {
                        apdata[i].data[k].vendorNumber = draftValues[j].vendorNumber;
                        draftValues[j].vendorId = apdata[i].data[k].vendorId;
                        draftValues[j].lvnId = apdata[i].data[k].lvnId;
                        draftValues[j].fips = apdata[i].data[k].fips;
                        draftValues[j].fipsName = apdata[i].data[k].fipsName;
                    }
                }
            }
        }
        for(let i = 0 ; i < data.length ; i++){
            for(let j = 0 ; j < draftValues.length ; j++) {
                if(data[i].vendorId === draftValues[j].vendorId && data[i].fips === draftValues[j].fips) {
                    data[i].vendorNumber = draftValues[j].vendorNumber;
                }
            }
        }
        for(let i = 0 ; i < apdata.length ; i++){
            for(let k = 0 ; k < apdata[i].data.length ; k++){
                for(let j = 0 ; j < draftValues.length ; j++) {
                    if(apdata[i].data[k].vendorId === draftValues[j].vendorId && apdata[i].data[k].fips === draftValues[j].fips) {
                        apdata[i].data[k].vendorNumber = draftValues[j].vendorNumber;
                    }
                }
            }
        }
        let precheckData = component.get('v.precheckData');
        for(let i = 0 ; i < precheckData.length ; i++) {
            for(let j = 0 ; j < precheckData[i].data.length ; j++){
                for(let k = 0 ; k < draftValues.length ; k++) {
                    if(precheckData[i].data[j].vendorId === draftValues[k].vendorId) {
                        precheckData[i].data[j].vendorNumber = draftValues[k].vendorNumber;
                    }
                }
            }
        }

        for(let i = 0 ; i < draftValues.length ; i++) {
            if(!draftValues[i].fips) {
                helper.showToast('info', 'Info', 'Missing FIPS on the Payment.');
                return;
            }
            if(draftValues[i].fipsName && draftValues[i].fipsName.startsWith('Alleghany') && 
                (!draftValues[i].vendorNumber.startsWith('A') && !draftValues[i].vendorNumber.startsWith('H') && !draftValues[i].vendorNumber.startsWith('Y'))) {
                helper.showToast('info', 'Info', 'Alleghany Locality Vendor Number has to start with A or H or Y.');
                return;
            }
            if(draftValues[i].fipsName && draftValues[i].fipsName.startsWith('Covington') && !draftValues[i].vendorNumber.startsWith('Z')) {
                helper.showToast('info', 'Info', 'Covington Locality Vendor Number has to start with Z.');
                return;
            }
        }

        component.set('v.precheckData', precheckData); 
        component.set('v.data', data);
        component.set('v.apdata', apdata);
        let action = component.get("c.updateVendorNumber");
        action.setParams({
            data: draftValues
        });
        component.set('v.Spinner', true);
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                //component.find("paymentrecord").set("v.draftValues", null);
                component.set('v.draftValues', []);
                component.set('v.apdraftValues', []);
                helper.showToast('success', 'Success', 'Vendor Number successfully updated.');
                helper.getData(component, true);
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                        console.log('**Val of error'+strErr);
                    }
                }
                helper.showToast('error', 'Error', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    gotoPreCheckListReport: function(component, event, helper){
        window.open($A.get('$Label.c.KINSHIP_PRECHECK'), '_blank');
    },
   	ccLookupSearch : function(component, event, helper) {
        const lookupComponent = event.getSource();
        const serverSearchAction = component.get('c.kinshipSearchLookup');
        serverSearchAction.setParam('anOptionalParam', 'CostCenter');
        lookupComponent.search(serverSearchAction);
    },
    clearCCErrorsOnChange: function(component, event, helper) {
        const selection = component.get('v.ccSelection');
        const errors = component.get('v.ccErrors');

        if (selection.length && errors.length) {
            component.set('v.ccErrors', []);
        }
    },
    closeCostCenterModel: function(component, event, helper){
        component.set('v.isCostCenter', false);
        component.set("v.ccSelection", []);
    },
    openCostCenterModel: function(component, event, helper){
        let precheckData = component.get('v.precheckData');
        let data = [];
        for(let i = 0 ; i < precheckData.length ; i++) {
            let temp = precheckData[i].selectedRows;
            if(temp.length > 0) {
                data = data.concat(temp);
            }
        }
        if(data.length <= 0) {
            helper.showToast('info', 'Missing Record', 'Please choose a Record before clicking the Update Cost Center.');
            return;
        }
        for(let i = 0 ; i < data.length ; i++){
            if(data[i].costCenterId){
                helper.showToast('info', 'Empty Cost Center Payment', 'Please choose an Empty Cost Center before clicking the Update Cost Center.');
                return;
            }
        }
        component.set('v.isCostCenter', true);
    },
    updateCostCenter: function(component, event, helper){
        let precheckData = component.get('v.precheckData');
        let data = [];
        for(let i = 0 ; i < precheckData.length ; i++) {
            let temp = precheckData[i].selectedRows;
            if(temp.length > 0) {
                data = data.concat(temp);
            }
        }
        let costcenter = component.get("v.ccSelection");
        if(!costcenter || costcenter.length == 0) {
            helper.showToast('info', 'Missing Cost Center', 'Please choose the Cost Center before clicking the Save Button.');
            return;
        }
        let strId = '';
        for(let i = 0 ; i < data.length ; i++) {
            data[i].costCenterId = costcenter[0].id;
            data[i].costCenterUrl = '/' + costcenter[0].id;
            data[i].costCenterName = costcenter[0].title;
            strId += data[i].id;
        }
        
        let action = component.get("c.updateCostCenterOnPayment");
        action.setParams({
            lstPayment: data
        });
        component.set('v.Spinner', true);
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                for(let i = 0 ; i < precheckData.length ; i++) {
                    for(let j = 0 ; j < precheckData[i].data.length ; j++) {
                        if(strId.indexOf(precheckData[i].data[j].id) !== -1){
                            precheckData[i].data[j].costCenterId = costcenter[0].id;
                            precheckData[i].data[j].costCenterUrl = '/' + costcenter[0].id;
                            precheckData[i].data[j].costCenterName = costcenter[0].title;
                        }
                    }
                }
                helper.showToast('success', 'Success', 'Cost Center successfully updated.');
                component.set('v.precheckData', precheckData);
                component.set('v.isCostCenter', false);
        		component.set("v.ccSelection", []);
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                        console.log('**Val of error'+strErr);
                    }
                }
                helper.showToast('error', 'Error', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    handlePreCheckListRowAction: function (cmp, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');

        switch (action.name) {
            case 'update_cost_center':
                //alert('Showing Details: ' + JSON.stringify(row));
                helper.handleInlineUpdateCostCenter(cmp, event, helper);
                break;
            case 'write_off':
                helper.updatePaymentWriteOffAndStatus(cmp, event, helper, false);
                break;
            case 'on_hold':
                helper.updatePaymentWriteOffAndStatus(cmp, event, helper, true);    
                break;
        }
    },
    handleApprovedCheckSelection: function(component, event, helper) {
        let total = 0;
        let selectedRows = event.getParam('selectedRows');
        for(let i = 0 ; i < selectedRows.length ; i++){
            total += selectedRows[i].amount;
        }
        component.set('v.totalSelectedApproval', total);
        
        let source = event.getSource();
        if(source) {
            let index = source.get('v.class');
            let i = parseInt(index);
            let apdata = component.get('v.apdata');
            let selectedData = [];
            for(let i = 0 ; i < selectedRows.length ; i++){
                selectedData.push(selectedRows[i]);
            }
            apdata[i].selectedRows = selectedData;
            component.set('v.apdata', apdata);
        }
    },
    fipsLookupSearch : function(component, event, helper) {
        const lookupComponent = event.getSource();
        const serverSearchAction = component.get('c.kinshipSearchLookup');
        serverSearchAction.setParam('anOptionalParam', 'FIPS');
        lookupComponent.search(serverSearchAction);
    },
    clearFIPSErrorsOnChange: function(component, event, helper) {
        const selection = component.get('v.fipsSelection');
        const errors = component.get('v.fipsErrors');

        if (selection.length && errors.length) {
            component.set('v.fipsErrors', []);
        }
    },
    closeFIPSModel: function(component, event, helper){
        component.set('v.isFIPS', false);
        component.set("v.fipsSelection", []);
    },
    openFIPSModel: function(component, event, helper){
        component.set('v.isFIPS', true);
    },
    updateFIPSOnPayment: function(component, event, helper){
        let precheckData = component.get('v.precheckData');
        let data = [];
        for(let i = 0 ; i < precheckData.length ; i++) {
            let temp = precheckData[i].selectedRows;
            if(temp.length > 0) {
                for(let i = 0 ; i < temp.length ; i++) {
                    if(!temp[i].fips) {
                        data.push(temp[i].id);
                    }
                }
            }
        }
        let fips = component.get("v.fipsSelection");
        if(!fips || fips.length == 0) {
            helper.showToast('info', 'Missing FIPS', 'Please choose the FIPS before clicking the Save Button.');
            return;
        }
        
        let action = component.get("c.updateFIPS");
        action.setParams({
            lstId: data,
            fips: fips[0].id
        });
        component.set('v.Spinner', true);
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                helper.showToast('success', 'Success', 'FIPS successfully updated.');
                /*component.set('v.precheckData', precheckData);
                component.set('v.isCostCenter', false);
        		component.set("v.ccSelection", []);*/
                component.set('v.isFIPS', false);
        		component.set("v.fipsSelection", []);
                helper.getData(component, true);
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                helper.showToast('info', 'Info', strErr);
                component.set('v.Spinner', false);
            }
            
        });
        $A.enqueueAction(action);
    }
})