({
    showToast : function(type, title, message) {
        let strTitle = title.split('\n').join('');
        let strMessage = message.split('\n').join('');
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : strTitle,
            message: strMessage,
            duration:' 4000',
            key: 'info_alt',
            type: type, // success , error, info
            mode: 'pester'//'sticky'
        });
        toastEvent.fire();
    },
    fetchData: function(component, objName, apiName, strNon, strValue){
        let action = component.get("c.getPicklistvalues");
        action.setParams({
            'objectName': objName,
            'field_apiname': apiName,
            'strNone': strNon
        });
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS"){
                let d = a.getReturnValue();
                if (d) {
                    let data = [];
                    for(let i = 0 ; i < d.length ; i++) {
                        data.push({'label' : d[i], 'value':d[i]});
                    }
                    component.set("v." + strValue, data);
                }
            } 
        });
        $A.enqueueAction(action);
    },
    getData : function(component, isInit) {
        component.set('v.Spinner', true);
        let action = component.get("c.getPaymentData");
        let data = component.get("v.vendorSelection");
        let lstVendor = [];
        if(data) {
            data.forEach(element => {
                lstVendor.push(element.id);
            });
        }
        data = component.get("v.cateSelection");
        let lstCategory = [];
        if(data) {
            data.forEach(element => {
                lstCategory.push(element.id);
            });
        }
        data = component.get("v.oppSelection");
        let lstOpp = [];
        if(data) {
            data.forEach(element => {
                lstOpp.push(element.id);
            });
        }
        data = component.get("v.caSelection");
        if(data) {
            data.forEach(element => {
                lstOpp.push(element.id);
            });
        }
        data = component.get("v.paymentSelection");
        let lstPayment = [];
        if(data) {
            data.forEach(element => {
                lstPayment.push(element.id);
            });
        }
        data = component.get("v.caseSelection");
        let caseWorker = [];
        if(data) {
            data.forEach(element => {
                caseWorker.push(element.id);
            });
        }
        data = component.get("v.campSelection");
        let camp = [];
        if(data) {
            data.forEach(element => {
                camp.push(element.id);
            });
        }
        if(isInit){
            let pid = component.get('v.pid');
            if(pid) {
                lstPayment.push(pid);
            }
            let poid = component.get('v.poid');
            if(poid) {
                lstOpp.push(poid);
            }
        }
        //let strOppRT = component.get("v.strOppRT");
        let startDate = component.get("v.startDate");
        let endDate = component.get("v.endDate");
        let status = isInit?'':component.get('v.strStatus');//isInit?'Approved':component.get('v.strStatus');
        let rtvalue = component.get('v.rtvalue');
        let ctvalue = component.get('v.ctvalue');
        let startAmount = component.get('v.startAmount');
        if(!startAmount && startAmount !== 0){
            startAmount = null;
        }
        let endAmount = component.get('v.endAmount');
        if(!endAmount && endAmount !== 0){
            endAmount = null;
        }
        action.setParams({
            lstVendor: lstVendor,
            lstCategory: lstCategory,
            lstOpp: lstOpp,
            lstPayment: lstPayment,
            lstCaseWorker: caseWorker,
            lstAP: camp,
            startDate: startDate,
            endDate: endDate,
            status: status,
            lstRT: rtvalue,
            lstCT: ctvalue,
            startAmount: startAmount,
            endAmount: endAmount
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let data = response.getReturnValue();
                if(data) {
                    let nonAppData = [];
                    let appData = [];
                    let totalPreCheck = 0;
                    let totalApproval = 0;
                    let waData = [];
                    let totalWAData = 0;
                    for(let i = 0 ; i < data.length ; i++) {
                        if(data[i].localityAccountCode === '61090') {
                            data[i].poRecordTypeName = 'Taxes Payable';
                        } else if(data[i].poRecordTypeName === 'Purchase of Services Order') {
                            data[i].poRecordTypeName = 'Purchase of Services Orders';
                        } else if(data[i].poRecordTypeName === 'One time Case Actions') {
                            data[i].poRecordTypeName = 'One-Time Case Actions';
                        } 
                        data[i].amount = this.parseAmount(data[i].amount);
                        if(data[i].status === 'Approved') {
                            appData.push(data[i]);
                            totalApproval += data[i].amount;
                        } else if(data[i].status === 'Submitted For Approval') {
                            waData.push(data[i]);
                            totalWAData += data[i].amount;
                        } else {
                            nonAppData.push(data[i]);
                            totalPreCheck += data[i].amount;
                        }
                    }
                    component.set("v.data", nonAppData);
                    component.set("v.apdata", appData);
                    component.set("v.totalPreCheck", totalPreCheck);
                    component.set("v.totalApproval", totalApproval);
                    component.set("v.waData", waData);
                    component.set("v.totalWAData", totalWAData);

                    let precheckData = [];
                    let strSD = 'Staff & Operations,One-Time Case Actions,Ongoing Case Actions,Purchase of Services Orders,Claims Payable,Special Welfare';
                    let strFIPS = '';
                    for(let i = 0 ; i < nonAppData.length ; i++){
                        if(strFIPS.indexOf(nonAppData[i].fipsName) !== -1) {
                            for(let j = 0 ; j < precheckData.length ; j++) {
                                if(precheckData[j].poRecordTypeName === nonAppData[i].poRecordTypeName && precheckData[j].fipsName === nonAppData[i].fipsName) {
                                    precheckData[j].data.push(nonAppData[i]);
                                    continue;
                                }
                            }
                        } else {
                            precheckData.push({'poRecordTypeName': 'Staff & Operations', 'fipsName': nonAppData[i].fipsName, 'isShow': false, 'data': [], 'selectedRows': [],'defaultSortDirection': 'asc', 'sortDirection': 'asc', 'sortedBy': '', 'draftValues' : [], 'total':0 });
                            precheckData.push({'poRecordTypeName': 'One-Time Case Actions','fipsName': nonAppData[i].fipsName, 'isShow': false, 'data': [], 'selectedRows': [],'defaultSortDirection': 'asc', 'sortDirection': 'asc', 'sortedBy': '', 'draftValues' : [], 'total':0 });
                            precheckData.push({'poRecordTypeName': 'Ongoing Case Actions','fipsName': nonAppData[i].fipsName, 'isShow': false, 'data': [], 'selectedRows': [],'defaultSortDirection': 'asc', 'sortDirection': 'asc', 'sortedBy': '', 'draftValues' : [], 'total':0 });
                            precheckData.push({'poRecordTypeName': 'Purchase of Services Orders','fipsName': nonAppData[i].fipsName, 'isShow': false, 'data': [], 'selectedRows': [],'defaultSortDirection': 'asc', 'sortDirection': 'asc', 'sortedBy': '', 'draftValues' : [], 'total':0 });
                            precheckData.push({'poRecordTypeName': 'Claims Payable','fipsName': nonAppData[i].fipsName, 'isShow': false, 'data': [], 'selectedRows': [],'defaultSortDirection': 'asc', 'sortDirection': 'asc', 'sortedBy': '', 'draftValues' : [], 'total':0, 'isChild': true  });
                            precheckData.push({'poRecordTypeName': 'Special Welfare','fipsName': nonAppData[i].fipsName, 'isShow': false, 'data': [], 'selectedRows': [],'defaultSortDirection': 'asc', 'sortDirection': 'asc', 'sortedBy': '', 'draftValues' : [], 'total':0 });
                            precheckData.push({'poRecordTypeName': 'Taxes Payable','fipsName': nonAppData[i].fipsName, 'isShow': false, 'data': [], 'selectedRows': [],'defaultSortDirection': 'asc', 'sortDirection': 'asc', 'sortedBy': '', 'draftValues' : [], 'total':0, 'isChild': true });
                            for(let j = 0 ; j < precheckData.length ; j++) {
                                if(precheckData[j].poRecordTypeName === nonAppData[i].poRecordTypeName && precheckData[j].fipsName === nonAppData[i].fipsName) {
                                    precheckData[j].data.push(nonAppData[i]);
                                    continue;
                                }
                            }
                            strFIPS += nonAppData[i].fipsName + ',';
                        }
                    }
                    for(let i = 0 ; i < precheckData.length ; i++){
                        let tTemp = 0;
                        for(let j = 0 ; j < precheckData[i].data.length ; j++) {
                            tTemp += precheckData[i].data[j].amount;
                        }
                        precheckData[i].total = tTemp;
                    }
                    let newprecheckData = [];
                    strFIPS = '';
                    for(let i = 0 ; i < precheckData.length ; i++){
                        if(precheckData[i].data.length > 0) {
                            if(strFIPS !== precheckData[i].fipsName){
                                precheckData[i].isShow = true;
                                strFIPS = precheckData[i].fipsName;
                            }
                            if(!strFIPS) {
                                //precheckData[i].fipsName = 'Missing FIPS';
                                newprecheckData.unshift(precheckData[i]);
                            } else {
                            	newprecheckData.push(precheckData[i]);
                            }
                        }
                    }

                    // calculate for child
                    for(let i = 0 ; i < newprecheckData.length ; i++){
                        if(newprecheckData[i].isChild) {
                            if(newprecheckData[i].poRecordTypeName === 'Claims Payable'){
                                let strCate = '';
                                let temp;
                                let childData = [];
                                for(let j = 0 ; j < newprecheckData[i].data.length ; j++) {
                                    temp = newprecheckData[i].data[j].category;
                                    if(strCate.indexOf(',' + temp) !== -1){
                                        for(let k = 0 ; k < childData.length ; k++) {
                                            if(temp === childData[k].poRecordTypeName) {
                                                childData[k].data.push(newprecheckData[i].data[j]);
                                                continue;
                                            }
                                        }
                                    } else {
                                        let tempData = {'poRecordTypeName': temp, 'fipsName': '', 'isShow': false, 'data': [], 'selectedRows': [],'defaultSortDirection': 'asc', 'sortDirection': 'asc', 'sortedBy': '', 'draftValues' : [], 'total':0 , 'isCP': true };
                                        strCate += ',' + temp;
                                        tempData.data.push(newprecheckData[i].data[j]);
                                        childData.push(tempData);
                                    }
                                }
                                for(let i = 0 ; i < childData.length ; i++){
                                    let tTemp = 0;
                                    for(let j = 0 ; j < childData[i].data.length ; j++) {
                                        tTemp += childData[i].data[j].amount;
                                    }
                                    childData[i].total = tTemp;
                                }
                                newprecheckData[i].data = childData;
                            }
                            if(newprecheckData[i].poRecordTypeName === 'Taxes Payable'){
                                let strMonth = '';
                                let temp;
                                let childData = [];
                                for(let j = 0 ; j < newprecheckData[i].data.length ; j++) {
                                    temp = this.getMonthString(newprecheckData[i].data[j].paymentDate);
                                    if(strMonth.indexOf(',' + temp) !== -1){
                                        for(let k = 0 ; k < childData.length ; k++) {
                                            if(temp === childData[k].poRecordTypeName) {
                                                childData[k].data.push(newprecheckData[i].data[j]);
                                                continue;
                                            }
                                        }
                                    } else {
                                        let tempData = {'poRecordTypeName': temp, 'fipsName': '', 'isShow': false, 'data': [], 'selectedRows': [],'defaultSortDirection': 'asc', 'sortDirection': 'asc', 'sortedBy': '', 'draftValues' : [], 'total':0 };
                                        strMonth += ',' + temp;
                                        tempData.data.push(newprecheckData[i].data[j]);
                                        childData.push(tempData);
                                    }
                                }
                                for(let i = 0 ; i < childData.length ; i++){
                                    let tTemp = 0;
                                    for(let j = 0 ; j < childData[i].data.length ; j++) {
                                        tTemp += childData[i].data[j].amount;
                                    }
                                    childData[i].total = tTemp;
                                }
                                newprecheckData[i].data = childData;
                            }
                        }
                    }
                    component.set('v.precheckData', newprecheckData);
                    
                    // approval by fips
                    strFIPS = '';
                    let newAPData = [];
                    for(let i = 0 ; i < appData.length ; i++){
                        if(strFIPS.indexOf(appData[i].fipsName) !== -1) {
                            for(let j = 0 ; j < newAPData.length ; j++) {
                                if(newAPData[j].poRecordTypeName === appData[i].fipsName) {
                                    newAPData[j].data.push(appData[i]);
                                    continue;
                                }
                            }
                        } else {
                            let tData = [];
                            tData.push(appData[i]);
                            newAPData.push({'poRecordTypeName': appData[i].fipsName, 'fipsName': appData[i].fipsName, 'isShow': false, 'data': tData, 'selectedRows': [],'defaultSortDirection': 'asc', 'sortDirection': 'asc', 'sortedBy': '', 'draftValues' : [], 'total':0 });
                            strFIPS += appData[i].fipsName + ',';
                        }
                    }
                    component.set("v.apdata", newAPData);
                }else{
                    this.showToast('info', 'Info', 'Please contact your Administrator for more details.');
                }
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                this.showToast('info', 'Info', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    syncPayment: function(component, lstId) {
        component.set('v.Spinner', true);
        let action = component.get("c.syncPaymentToCheck");
        action.setParams({
            lstId: lstId
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let data = response.getReturnValue();
                let message = 'You have created ' + (data?data.length:'') + ' check records successfully.';
                this.showToast('success', 'Success', message);
                data = component.find('paymentrecord').getSelectedRows();
                let setid = new Set();
                for(let i = 0 ; i < data.length ; i++){
                    setid.add(data[i].id);
                }
                let olddata = component.get('v.data');
                let newdata = [];
                for(let i = 0 ; i < olddata.length ; i++) {
                    if(!setid.has(olddata[i].id)){
                        newdata.push(olddata[i]);
                    }
                }
                component.set('v.data', newdata);
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                        console.log('**Val of error'+strErr);
                        //ADDED BY BHAVNA
                        if(strErr.includes('Please write off or pay the previous payments.')){
                            strErr = 'There are previous payments pending for this Purchase Order. Please clear them first.';
                            console.log('**str'+strErr);
                        }   
                    }
                }
                this.showToast('error', 'Error', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    convertDatatoCheckData: function (component, event, data){
        let mapData = new Map();
        let checkData = [];
        for(let i = 0 ; i < data.length ; i++){
            let temp = mapData.get(data[i].vendorId);
            if(!temp){
                temp = {'paymentId': data[i].vendorId, 'vendorId' : data[i].vendorId, 'vendorName' : data[i].vendorName, 'vendorAddress': data[i].vendorAddress,
                        'amount' : 0, 'description': '', 'status': '','isPrinted': false, 'description' : data[i].memo,
                        'status': '','isPrinted': false, 
                        'lstPayment':[]};
            }
            temp.lstPayment.push(data[i]);
            mapData.set(data[i].vendorId, temp);
        }
        let keys = Array.from(mapData.keys());
        for(let i = 0 ; i < keys.length ; i++){
            checkData.push(mapData.get(keys[i]));
        }
        return checkData;
    },
    serperateCheckDataWithPayment: function(component, vendorId, paymentId) {
        let checkData = component.get('v.checkData');
        let newCheckData = [];
        for(let i = 0 ; i < checkData.length ; i++) {
            if(checkData[i].vendorId === vendorId) {
                let sepCheck1 = {'paymentId': (checkData[i].paymentId + '_' + 1), 'vendorId' : checkData[i].vendorId,'vendorAddress': checkData[i].vendorAddress, 
                                'vendorName' : checkData[i].vendorName, 'amount' : 0, 'description': checkData[i].memo,
                                'status': '','isPrinted': false, 'lstPayment':[]};
                let sepCheck2 = {'paymentId': (checkData[i].paymentId + '_' + 2), 'vendorId' : checkData[i].vendorId,'vendorAddress': checkData[i].vendorAddress,  
                                'vendorName' : checkData[i].vendorName, 'amount' : 0, 'description': checkData[i].memo,
                                'status': '','isPrinted': false, 'lstPayment':[]};
                let flag = false;
                for(let j = 0 ; j < checkData[i].lstPayment.length ; j++){
                    if(checkData[i].lstPayment[j].id === paymentId) {
                        sepCheck2.lstPayment.push(checkData[i].lstPayment[j]);
                        flag = true;
                    } else {
                        sepCheck1.lstPayment.push(checkData[i].lstPayment[j]);
                    }
                }
                component.set('v.activeSection', sepCheck1.paymentId);
                if(flag) {
                    newCheckData.push(sepCheck1);
                    newCheckData.push(sepCheck2);
                } else {
                    newCheckData.push(checkData[i]);
                }
            } else {
                newCheckData.push(checkData[i]);
            }
        }
        component.set('v.checkData', newCheckData);
    },
    nextStep: function(component) {
        let accurrentStep = component.get('v.accurrentStep');
        if(accurrentStep === 'step1') {
            component.set('v.isstep1', false);
            component.set('v.isstep2', true);
            component.set('v.isstep3', false);
            component.set('v.accurrentStep', 'step2');
        } else if(accurrentStep === 'step2') {
            component.set('v.isstep1', false);
            component.set('v.isstep2', false);
            component.set('v.isstep3', true);
            component.set('v.accurrentStep', 'step3');
        }
    },
    previousStep: function(component) {
        let accurrentStep = component.get('v.accurrentStep');
        if(accurrentStep === 'step2') {
            component.set('v.isstep1', true);
            component.set('v.isstep2', false);
            component.set('v.isstep3', false);
            component.set('v.accurrentStep', 'step1');
        } else if(accurrentStep === 'step3') {
            component.set('v.isstep1', false);
            component.set('v.isstep2', true);
            component.set('v.isstep3', false);
            component.set('v.accurrentStep', 'step2');
        }
    },
    resetCheckRunModel: function(component) {
        component.set('v.isstep1', true);
        component.set('v.isstep2', false);
        component.set('v.isstep3', false);
        component.set('v.accurrentStep', 'step1');
        component.set('v.isPrint', false);
        let today = new Date();
        component.set('v.checkrunDate', today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate());
        this.getData(component, true);
    },
    convertArrayOfObjectsToCSV : function(component, data){
        let csvStringResult, counter, keys, columnDivider, lineDivider;
       
        if (data == null || !data.length) {
            return null;
         }

        columnDivider = '|';
        lineDivider =  '\n';
        csvStringResult = '';
 
        for(let i=0; i < data.length; i++){
            if(data[i].recordTypeName === 'Case Action') {
                csvStringResult += '||||' + this.formatData(data[i].costCenterCode) + columnDivider +
                                            this.formatData(data[i].localityAccountNumber) + '|||' +
                                            this.formatData(data[i].localityVendorNumber) + columnDivider +
                                            'N' + columnDivider + '11100' + columnDivider +
                                            this.formatDate(data[i].paymentDate) + columnDivider +
                                            this.formatData2(data[i].paymentNumber) + columnDivider +
                                            this.formatNumber(data[i].amount) + columnDivider +
                                            this.formatData2(data[i].caseName) + this.addSpace(this.formatData2(data[i].servicePeriod)).toUpperCase() + this.addSpace(this.formatData2(data[i].invoiceNumber)) + columnDivider  +
                                            'N' + '|||||||||||||||';
            } else if(data[i].recordTypeName === 'Purchase of Services Order') {
                csvStringResult += '||||' + this.formatData(data[i].costCenterCode) + columnDivider +
                                            this.formatData(data[i].localityAccountNumber) + '|||' +
                                            this.formatData(data[i].localityVendorNumber) + columnDivider +
                                            'M' + columnDivider + '11100' + columnDivider +
                                            this.formatDate(data[i].paymentDate) + columnDivider +
                                            this.formatData2(data[i].poNumber) + this.formatData2(data[i].paymentNumber) + columnDivider +
                                            this.formatNumber(data[i].amount) + columnDivider +
                                            this.formatData2(data[i].poNumber) + this.addSpace(this.formatData2(data[i].caseName)) + this.addSpace(this.formatData2(data[i].servicePeriod)).toUpperCase() + this.addSpace(this.formatData2(data[i].invoiceNumber)) + columnDivider  +
                                            'N' + '|||||||||||||||';
            } else if(data[i].recordTypeName === 'Staff & Operations') {
                csvStringResult += '||||' + this.formatData(data[i].costCenterCode) + columnDivider +
                                            this.formatData(data[i].localityAccountNumber) + '|||' +
                                            this.formatData(data[i].localityVendorNumber) + columnDivider +
                                            'M' + columnDivider + '11100' + columnDivider +
                                            this.formatDate(data[i].paymentDate) + columnDivider +
                                            this.formatData2(data[i].paymentNumber) + columnDivider +
                                            this.formatNumber(data[i].amount) + columnDivider +
                                            this.formatData(data[i].invoiceNumber) + this.addSpace(this.formatData(data[i].description)) + columnDivider  +
                                            'N' + '|||||||||||||||';
            }
            csvStringResult += lineDivider;
        }
       
        return csvStringResult;        
    },
    convertArrayOfObjectsToCSVUpdated : function(isFinal,selectedData,component, dataa,check,csv,isChild,parentData){
        var result, ctr, keys, columnDelimiter, lineDelimiter, data;
        data = selectedData || null;
            //data = dataa.data || null;
        //data = dataa;
            if (data == null || !data.length) {
                return null;
            }
            var total = 0;
            data.forEach(function(item) {
                    total +=item.amount;
            });
            let grandtotal = component.get("v.gTotal");
            console.log("gtotal:" + grandtotal);
            grandtotal = grandtotal + total;
            console.log("grandtotal: " + grandtotal);
            component.set("v.gTotal",grandtotal);
            data.push({ 'amount':total,'category': "",'costCenterName': "",'costCenterUrl': "",'fips': "",'fipsName': "",'fiscalYear': "",'id': "",'invoiceStage': "",'localityAccountCode': "",'lvnId': "",'oppName': "",'oppUrl': "",'paymentDate': "",'paymentNo': "",'paymentUrl': "",'poRecordTypeName': "",'recordType': "",'stage': "",'vendorAddress': "",'vendorId': "",'vendorName': "",'vendorNumber': "",'vendorUrl': "",'total':"" });
            if(isFinal){
            data.push({ 'amount':'Grand Total: ' + grandtotal,'category': "",'costCenterName': "",'costCenterUrl': "",'fips': "",'fipsName': "",'fiscalYear': "",'id': "",'invoiceStage': "",'localityAccountCode': "",'lvnId': "",'oppName': "",'oppUrl': "",'paymentDate': "",'paymentNo': "",'paymentUrl': "",'poRecordTypeName': "",'recordType': "",'stage': "",'vendorAddress': "",'vendorId': "",'vendorName': "",'vendorNumber': "",'vendorUrl': "",'total':"" });
            
        }
        //debugger;
            columnDelimiter = selectedData.columnDelimiter || ',';
            lineDelimiter = selectedData.lineDelimiter || '\n';
            if(data[1]){
                keys = Object.keys(data[1]);
            }
            else{
                keys = Object.keys(data[0]);
            }
            if(check){
                result = '';
            }
            if(csv){
                result = csv;
            }
            if(isChild){
                if(parentData){
                    result += parentData.poRecordTypeName;
                }
            }
            result += lineDelimiter;
            result += dataa.poRecordTypeName;
            result += lineDelimiter;
            result += keys.join(columnDelimiter);
            result += lineDelimiter; 
            data.forEach(function(item) {
                ctr = 0;
                keys.forEach(function(key) {
                    if (ctr > 0) result += columnDelimiter;
        
                    result += item[key];
                    ctr++;
                });
                result += lineDelimiter;
            });
            if(typeof data[data.length-1].amount == 'number'){
                data.pop();
            }
            else{
                if(data[data.length-1].amount.includes('Grand')){
                    data.pop();
                    data.pop();
                }
            }
            
            return result;     
    },
    addSpace: function(str) {
        if(str) {
            return ' ' + str;
        }
        return str;
    },
    formatDate: function(d) {
        if(d){
            if(typeof d !== 'date'){
                d = new Date(d + ' 00:00:00');
            }
            return d.toLocaleDateString('en-US', {year: 'numeric', month: '2-digit', day: '2-digit'}).replaceAll('/','');
        }
        return '';
    },
    formatData: function(d) {
        if(d){
            return d;
        }
        return '';
    },
    formatData2: function(d) {
        if(d){
            return d.replaceAll('-','').replaceAll('#','');
        }
        return '';
    },
    formatDateString: function(d) {
        if(d){
            return d.replaceAll('-','').replaceAll('#','').trim().toUpperCase();
        }
        return '';
    },
    formatNumber: function(d) {
        if(d){
            return d.toLocaleString('en-US', { minimumFractionDigits: 2 });
        }
        return '';
    },
    sortBy: function(field, reverse, primer) {
        var key = primer
            ? function(x) {
                  return primer(x[field]);
              }
            : function(x) {
                  return x[field];
              };

        return function(a, b) {
            a = key(a);
            b = key(b);
            return reverse * ((a > b) - (b > a));
        };
    },
    handleSort: function(cmp, event, sd, d, sb) {
        let sortedBy = event.getParam('fieldName');
        let sortDirection = event.getParam('sortDirection');
        let data = cmp.get('v.' + d);
        let cloneData = data.slice(0);
        cloneData.sort((this.sortBy(sortedBy, sortDirection === 'asc' ? 1 : -1)));
        
        cmp.set('v.' + d, cloneData);
        cmp.set('v.' + sd, sortDirection);
        cmp.set('v.' + sb, sortedBy);
    },
    parseAmount: function(d) {
        if(typeof d !== 'number'){
            try{
                d = parseFloat(d);
            }catch (e){
                d = 0;
            }
        }
        return d;
    },
    getDateFromStr: function(d) {
        if(d){
            if(typeof d !== 'date'){
                return new Date(d + ' 00:00:00');
            }
            return d;
        }
        return null;
    },
    getMonthString: function(d) {
        if(d && typeof d === 'date'){
            return d.toLocaleString('default', { month: 'long' });
        }
        if(d && typeof d === 'string'){
            let pd = new Date(d + ' 00:00:00');
            return pd.toLocaleString('default', { month: 'long' });
        }
        return '';
    },
    openCostCenterModelHelper: function(component, event, helper){
        let precheckData = component.get('v.precheckData');
        let data = [];
        for(let i = 0 ; i < precheckData.length ; i++) {
            let temp = precheckData[i].selectedRows;
            if(temp.length > 0) {
                data = data.concat(temp);
            }
        }
        if(data.length <= 0) {
            helper.showToast('info', 'Missing Record', 'Please choose a Record before clicking the Update Cost Center.');
            return;
        }
        for(let i = 0 ; i < data.length ; i++){
            if(data[i].costCenterId){
                helper.showToast('info', 'Empty Cost Center Payment', 'Please choose an Empty Cost Center before clicking the Update Cost Center.');
                return;
            }
        }
        component.set('v.isCostCenter', true);
    },
    handleInlineUpdateCostCenter: function(component, event, helper) {
        let row = event.getParam('row');
        let index = event.getSource().get('v.class');
        let i = parseInt(index);
        let precheckData = component.get('v.precheckData');
        //let selectedRows = event.getParam('selectedRows');
        precheckData[i].selectedRows = [row];
        component.set('v.precheckData', precheckData);
        helper.openCostCenterModelHelper(component, event, helper);
    },
    updatePaymentWriteOffAndStatus: function(component, event, helper, isStatus) {
        let row = event.getParam('row');
        
        let action = component.get("c.updatePaymentWriteOffAndStatus");
        action.setParams({
            paymentId: row.id,
            isStatus: isStatus
        });
        component.set('v.Spinner', true);
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                helper.showToast('info', 'Payment updated', 'The payment  has been updated');
            
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                        console.log('**Val of error'+strErr);
                    }
                }
                helper.showToast('error', 'Error', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    }
})