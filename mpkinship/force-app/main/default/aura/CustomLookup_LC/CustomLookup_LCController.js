({
    doInit:function(component,event,helper){
       // console.log(component.get("v.Rowindex"));
       console.log('**VAl of selecterecord'+component.get("v.selectedRecord"));
        if(component.get("v.selectedRecord")!='' && component.get("v.selectedRecord")!=undefined){
            helper.InitialselectRecord(component,event,helper);
        }	
    },
    changeLookupValue : function(component,event,helper){
        console.log('**Enreed in changelookupvalue'+component.get("v.selectedRecord"));
        var eventval = component.getEvent("dataChange");
        eventval.setParam("dataChange", component.get("v.selectedRecord"));
  		 eventval.fire();
    },   
   onfocus : function(component,event,helper){
       $A.util.addClass(component.find("mySpinner"), "slds-show");
        var forOpen = component.find("searchRes");
            $A.util.addClass(forOpen, 'slds-is-open');
            $A.util.removeClass(forOpen, 'slds-is-close');
        // Get Default 5 Records order by createdDate DESC  
         var getInputkeyWord = '';
         helper.searchHelper(component,event,getInputkeyWord);
         
    },
    onblur : function(component,event,helper){       
        component.set("v.listOfSearchRecords", null );
        var forclose = component.find("searchRes");
        $A.util.addClass(forclose, 'slds-is-close');
        $A.util.removeClass(forclose, 'slds-is-open');
    },
    keyPressController : function(component, event, helper) {
        console.log('***Enterd in keypresscontroller');
        console.log( event.target.value);
       // get the search Input keyword   
         //var getInputkeyWord = component.get("v.SearchKeyWord"); // commented for now
         var getInputkeyWord=event.target.value;
        console.log(getInputkeyWord);
       // check if getInputKeyWord size id more then 0 then open the lookup result List and 
       // call the helper 
       // else close the lookup result List part.   
        if( getInputkeyWord.length > 0 ){
             var forOpen = component.find("searchRes");
               $A.util.addClass(forOpen, 'slds-is-open');
               $A.util.removeClass(forOpen, 'slds-is-close');
            helper.searchHelper(component,event,getInputkeyWord);
        }
        else{  
             component.set("v.listOfSearchRecords", null ); 
             var forclose = component.find("searchRes");
               $A.util.addClass(forclose, 'slds-is-close');
               $A.util.removeClass(forclose, 'slds-is-open');
          }
	},
    
  // function for clear the Record Selaction 
    clear :function(component,event,heplper){
         var pillTarget = component.find("lookup-pill");
         var lookUpTarget = component.find("lookupField"); 
        
         $A.util.addClass(pillTarget, 'slds-hide');
         $A.util.removeClass(pillTarget, 'slds-show');
        
         $A.util.addClass(lookUpTarget, 'slds-show');
         $A.util.removeClass(lookUpTarget, 'slds-hide');
      
         component.set("v.SearchKeyWord",null);
        component.set("v.SearchKeyWord",'');
         component.set("v.listOfSearchRecords", null );
         //component.set("v.selectedRecord", {} );  
        component.set("v.selectedRecord", ''); 
         console.log(component.get("v.SearchKeyWord"));
        //PASS EVENT VALUE TO PARENT ON CROSSING THE SELECTED PILL
        var eventval = component.getEvent("dataChange");
        eventval.setParams({"dataChange": '',
                            "IndexRow":component.get("v.Rowindex")});
  		 eventval.fire(); 
    },
    
  // This function call when the end User Select any record from the result list.   
    selectRecord : function(component, event, helper) {
    // get the selected Account record from the COMPONETN event 	 
      /* var selectedAccountGetFromEvent = event.getParam("recordByEvent");
	   component.set("v.selectedRecord" , selectedAccountGetFromEvent); */    
        console.log('**Entred in selectrecord');
        var selectedItem = event.currentTarget; // Get the target object
		var index = selectedItem.dataset.record; // Get its value i.e. the index
		var selectedaccount = component.get("v.listOfSearchRecords")[index]; // Use it retrieve the store record
        component.set("v.selectedRecord",selectedaccount);
        
        console.log('***rowindex'+component.get("v.Rowindex"));
         var eventval = component.getEvent("dataChange");
        eventval.setParams({"dataChange": component.get("v.selectedRecord"),
                            "IndexRow":component.get("v.Rowindex")});
  		 eventval.fire(); 
        
        var forclose = component.find("lookup-pill");
           $A.util.addClass(forclose, 'slds-show');
           $A.util.removeClass(forclose, 'slds-hide');
  
        var forclose = component.find("searchRes");
           $A.util.addClass(forclose, 'slds-is-close');
           $A.util.removeClass(forclose, 'slds-is-open');
        
        var lookUpTarget = component.find("lookupField");
            $A.util.addClass(lookUpTarget, 'slds-hide');
            $A.util.removeClass(lookUpTarget, 'slds-show');  
      
	},
})