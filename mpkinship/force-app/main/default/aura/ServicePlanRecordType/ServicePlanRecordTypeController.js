({
	doInit: function(component, event, helper) {
        var recordTypeId = component.get("v.pageReference").state.recordTypeId;
        console.log('recordTypeId: '+recordTypeId);
        
        var action = component.get("c.getRT_Name");
        action.setParams({
            rtId: recordTypeId
        })
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('rt name: '+response.getReturnValue());
                
                component.set("v.recordTypeName",response.getReturnValue());
                
                if(response.getReturnValue() == 'Program Service Plan'){
                    var createRecordEvent = $A.get("e.force:createRecord");
                    createRecordEvent.setParams({
                        "entityApiName": "IFSP__c",
                        "recordTypeId": recordTypeId,
                        "defaultFieldValues": {
                            'Refresh_Page__c' : true                            
                        }
                    });
                    createRecordEvent.fire();
                    //window.open('/lightning/page/home','_self');
                }                
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
	},
    
    handleSuccess : function(component, event, helper) {
        console.log('handleSuccess');
        var record = event.getParam("response");
        console.log(record);
        var apiName = record.apiName;
        component.set("v.newRecordId",record.id); // ID of updated or created record
    },
    onPageReferenceChanged: function(cmp, event, helper) {
        $A.get('e.force:refreshView').fire();
    }
})