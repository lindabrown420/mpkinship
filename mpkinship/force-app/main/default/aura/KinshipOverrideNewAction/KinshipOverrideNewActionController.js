({
    init : function(component, event, helper) {
        let sobjecttype = component.get('v.sobjecttype');
        if('Kinship_Check__c' === sobjecttype) {
            //component.set('v.message', 'Please Use Pay Invoice Feature to Print a Check.');
            window.open('/lightning/n/Pay_Invoices','_self');
        } else if('npe01__OppPayment__c' === sobjecttype) {
            //component.set('v.message', 'Invoices will automatic create from Purchase of Order Process.');
            let navEvent = $A.get("e.force:navigateToList");
            navEvent.setParams({
                "listViewId": null,
                "listViewName": null,
                "scope": "Opportunity"
            });
            navEvent.fire();
        } else if('Receipt__c' === sobjecttype) {
            //component.set('v.message', 'Please Use ROC Feature to create a Receipt.');
            let navEvent = $A.get("e.force:navigateToList");
            navEvent.setParams({
                "listViewId": null,
                "listViewName": null,
                "scope": "Report_of_Collection__c"
            });
            navEvent.fire();
        } else if('Transaction_Journal__c' === sobjecttype) {
            component.set('v.message', 'Transaction Journal record will be auto-created when you create an Invoice or Receipt.');
        }
    }
})