({
    showToast : function(type, title, message) {
        let strTitle = title.split('\n').join('');
        let strMessage = message.split('\n').join('');
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : strTitle,
            message: strMessage,
            duration:' 4000',
            key: 'info_alt',
            type: type, // success , error, info
            mode: 'pester'//'sticky'
        });
        toastEvent.fire();
    },
    gobacktoListView: function(component){
        let navEvent = $A.get("e.force:navigateToList");
        navEvent.setParams({
            "listViewId": null,
            "listViewName": 'Budget_Transactions',
            "scope": "Budget_Transactions__c"
        });
        navEvent.fire();
    },
    parseDecimal: function(d) {
        if(d && typeof d === "string"){
            return parseFloat(d);
        }
        return d;
    },
    gotoBudgetListView: function(component){
        let navEvent = $A.get("e.force:navigateToList");
        navEvent.setParams({
            "listViewId": null,
            "listViewName": "Current_Budget_Balance",
            "scope": "Budgets__c"
        });
        navEvent.fire();
    }
})