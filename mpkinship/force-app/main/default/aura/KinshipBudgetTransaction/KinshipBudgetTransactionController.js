({
    init : function(component, event, helper) {
        component.set('v.Spinner', true);
        let action = component.get("c.initData");
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let data = response.getReturnValue();
                if(data) {
                    component.set("v.btData", data);
                }else{
                    helper.showToast('error', 'Error', 'Please contact your Administrator for more details.');
                }
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                console.log(strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    campLookupSearch : function(component, event, helper) {
        const lookupComponent = event.getSource();
        const serverSearchAction = component.get('c.kinshipSearchLookup');
        serverSearchAction.setParam('anOptionalParam', 'Campaign');
        lookupComponent.search(serverSearchAction);
    },
    clearCampErrorsOnChange: function(component, event, helper) {
        const selection = component.get('v.campSelection');
        const errors = component.get('v.campErrors');

        let formData = event.getParam("formData");
        if(formData) {
            let btData = component.get('v.btData');
            btData.accountPeriod = formData.recordId;
            if(!btData.accountPeriod){
                component.set('v.isShowChild', false);
                btData.lstChild = [];
            } else {
                component.set('v.Spinner', true);
                let action = component.get("c.getBudgetsData");
                action.setParams({
                    'fiscalYear': formData.recordId
                });
                action.setCallback(this, function(response) {
                    let state = response.getState();
                    if (state === "SUCCESS") {
                        let lstBudgets = response.getReturnValue();
                        if(lstBudgets) {
                            if(lstBudgets.length === 0) {
                                helper.showToast('info', 'No Budgets', 'There is no Budget linked to this Fiscal Year. Please create the Budget first.');
                                component.set('v.Spinner', false);
                                return;
                            }
                            let data = component.get('v.btData');
                            data.lstBudgets = lstBudgets;
                            component.set("v.btData", data);
                            component.set('v.isShowChild', true);
                        }else{
                            helper.showToast('error', 'Error', 'Please contact your Administrator for more details.');
                        }
                    } else {
                        const errors = response.getError();
                        let strErr = '';
                        if(errors) {
                            if(errors[0] && errors[0].message){
                                strErr = errors[0].message;
                            }
                        }
                        console.log(strErr);
                    }
                    component.set('v.Spinner', false);
                });
                $A.enqueueAction(action);
            }
            component.set('v.btData', btData);
        }
        if (selection.length && errors.length) {
            component.set('v.campErrors', []);
        }
    },
    handleAmountChange: function(component, event, helper) {
        let target = event.getSource();
        let strClass = target.get("v.class");
        let indexes = strClass.split(' ');
        if (indexes && indexes.length == 2) {
            let row = parseInt(indexes[1]);
            let total = 0;
            let data = component.get('v.btData');
            for(let i = 0 ; i < data[row].lstChild.length ; i++){
                //total += helper.parseDecimal(data.lstChild[i].amount);
                //data.lstChild[i].totalTransferAmount = helper.parseDecimal(data.lstChild[i].amount);
                if(data[row].lstChild[i].btType === 'Budget Reduction') {
                    //data.lstChild[i].amount = -helper.parseDecimal(data.lstChild[i].amount);
                    total -= helper.parseDecimal(data[row].lstChild[i].amount)
                } else if(data[row].lstChild[i].btType === 'Supplemental Allocation'){
                    total += helper.parseDecimal(data[row].lstChild[i].amount);
                }

            }
            //component.set('v.total', total);
            data[row].total = total;

            target = event.getSource();
            strClass = target.get("v.accesskey");
            if(!strClass) {component.set('v.btData', data);return;}
            indexes = strClass.split('index_');
            if (indexes && indexes.length == 2 && indexes[1]) {
                let parent = parseInt(indexes[1]);
                total = helper.parseDecimal(data[row].lstChild[parent].amount);
                for(let i = 0 ; i < data[row].lstChild.length ; i++){
                    if(data[row].lstChild[i].isChild && data[row].lstChild[i].parentIndex === parent) {
                        total += helper.parseDecimal(data[row].lstChild[i].amount);
                    }
                }
                data[row].lstChild[parent].totalTransferAmount = total;
            } else {
                let itemp = 0;
                for(let i = 0 ; i < data[row].lstChild.length ; i++){
                    if(!data[row].lstChild[i].isChild) {
                        itemp = i;
                        data[row].lstChild[itemp].totalTransferAmount = helper.parseDecimal(data[row].lstChild[i].amount);
                    } else {
                        data[row].lstChild[itemp].totalTransferAmount += helper.parseDecimal(data[row].lstChild[i].amount);
                    }
                }
            }
            component.set('v.btData', data);
        }
    },
    handleAdd: function(component, event, helper) {
        let target = event.getSource();
        let strClass = target.get("v.class");
        if(strClass !== undefined) {
            let row = parseInt(strClass);
            let btData = component.get('v.btData');
            let no = 1;
            for(let i = btData[row].lstChild.length - 1 ; i >= 0 ; i--){
                if(!btData[row].lstChild[i].isChild){
                    no = btData[row].lstChild[i].No + 1;
                    break;
                }
            }
            if(no > 1) {
                //component.set('v.isShowDelete', true);
                btData[row].isShowDelete = true;
            } else {
                //component.set('v.isShowDelete', false);
                btData[row].isShowDelete = false;
            }
            if(!btData[row].lstChild) btData[row].lstChild = [];
            let child = {'No':no, 'isChild':false, 'code':'','description':'', 'code2':'','description2':'','btType':'','avaiAmount':0,'amount':0,
                        'codeSelection':[],'codeErrors':[],'codeToSelection':[],'codeToErrors':[], 'numChild': 1, 'totalTransferAmount': 0};
            btData[row].lstChild.push(child);
            component.set('v.btData', btData);
        }
    },
    handleAddChild: function(component, event, helper) {
        let target = event.getSource();
        let strClass = target.get("v.class");
        let indexes = strClass.split('_');
        if (indexes && indexes.length == 2) {
            let row = parseInt(indexes[0]);
            let i = parseInt(indexes[1]);
            let btData = component.get('v.btData');
            let child = {'parentIndex':i, 'isChild':true, 'code': btData[row].lstChild[i].code,'description':btData[row].lstChild[i].description,'btType':btData[row].lstChild[i].btType, 
                        'budgetFrom': btData[row].lstChild[i].budgetFrom, 'avaiAmount' : btData[row].lstChild[i].avaiAmount, 'code2':'','description2':'','amount':0,
                        'codeSelection':[],'codeErrors':[],'codeToSelection':[],'codeToErrors':[], 'totalTransferAmount': 0};
            //btData.lstChild.push(child);
            btData[row].lstChild.splice(i + btData[row].lstChild[i].numChild, 0, child);
            btData[row].lstChild[i].numChild += 1;
            component.set('v.btData', btData);
        }
    },
    handleDelete: function(component, event, helper) {
        let target = event.getSource();
        let strClass = target.get("v.class");
        let indexes = strClass.split('_');
        if (indexes && indexes.length == 2) {
            let row = parseInt(indexes[0]);
            let btData = component.get('v.btData');
            let newbtData = [];
            let index = parseInt(indexes[1]);
            for(let i = 0 ; i < btData[row].lstChild.length ; i++) {
                if (i !== index && index !== btData[row].lstChild[i].parentIndex) {
                    newbtData.push(btData[row].lstChild[i]);
                }
            }
            let total = component.get('v.total');
            if(btData[row].lstChild[index].btType === 'Supplemental Allocation') {
                total -= helper.parseDecimal(btData[row].lstChild[index].amount);
            } else if (btData[row].lstChild[index].btType === 'Budget Reduction') {
                total += helper.parseDecimal(btData[row].lstChild[index].amount);
            }/* else {
                total -= btData.lstChild[index].totalTransferAmount;
            }*/

            let temp;
            for(let i = 0 ; i < newbtData.length ; i++) {
                if(!newbtData[i].isChild){
                    temp = i;
                } else {
                    newbtData[i].parentIndex = temp;
                }
            }
            btData[row].lstChild = newbtData;
            component.set('v.btData', btData);
            component.set('v.total', total);
        }
    },
    handleDeleteChild: function(component, event, helper) {
        let target = event.getSource();
        let strClass = target.get("v.class");
        let indexes = strClass.split('_');
        //let total = component.get('v.total');
        if (indexes && indexes.length == 2) {
            let row = parseInt(indexes[0]);
            let btData = component.get('v.btData');
            let newbtData = [];
            for(let i = 0 ; i < btData[row].lstChild.length ; i++) {
                if (i !== parseInt(indexes[1])) {
                    newbtData.push(btData[row].lstChild[i]);
                }else{
                    //total -= btData.lstChild[i].amount;
                    newbtData[btData[row].lstChild[i].parentIndex].totalTransferAmount -= helper.parseDecimal(btData[row].lstChild[i].amount);
                    newbtData[btData[row].lstChild[i].parentIndex].numChild -= 1;
                }
            }
            let temp;
            for(let i = 0 ; i < newbtData.length ; i++) {
                if(!newbtData[i].isChild){
                    temp = i;
                } else {
                    newbtData[i].parentIndex = temp;
                }
            }
            btData[row].lstChild = newbtData;
            component.set('v.btData', btData);
            //component.set('v.total', total);
        }
    },
    handleTypeChange: function(component, event, helper) {
        let target = event.getSource();
        let strClass = target.get("v.class");
        let indexes = strClass.split('_');
        if (indexes && indexes.length == 2) {
            let row = parseInt(indexes[0]);
            let btData = component.get('v.btData');
            let newbtData = [];
            let index = parseInt(indexes[1]);
            for(let i = 0 ; i < btData[row].lstChild.length ; i++) {
                if (index !== btData[row].lstChild[i].parentIndex) {
                    newbtData.push(btData[row].lstChild[i]);
                }
            }
            btData[row].lstChild = newbtData;
            component.set('v.btData', btData);
        }
    },
    handleSave: function(component, event, helper) {
        let btData = component.get("v.btData");
        let hasData = false;
        let data = [];
        for(let row = 0 ; row < btData.length ; row++) {
            if(btData[row].isShow){
                for(let i = 0 ; i < btData[row].lstChild.length ; i++) {
                    hasData = true;
                    if(!btData[row].lstChild[i].code){
                        helper.showToast('info', 'Missing Data', 'Please Input the Code From.');
                        return;
                    }
    
                    if(btData[row].lstChild[i].btType === 'Budget Transfer') {
                        if(!btData[row].lstChild[i].code2){
                            helper.showToast('info', 'Missing Data', 'Please Input the Code To.');
                            return;
                        }
                        if(btData[row].lstChild[i].code === btData[row].lstChild[i].code2){
                            helper.showToast('info', 'Wrong Data', 'You cannot transfer on the same code.');
                            return;
                        }
                    }
                    if(!btData[row].lstChild[i].amount || btData[row].lstChild[i].amount === 0){
                        helper.showToast('info', 'Missing Data', 'Please Input the Amount.');
                        return;
                    }
                    btData[row].lstChild[i].amount = helper.parseDecimal(btData[row].lstChild[i].amount);
                    if(btData[row].lstChild[i].amount > btData[row].lstChild[i].avaiAmount && btData[row].lstChild[i].btType !== 'Supplemental Allocation'){
                        helper.showToast('info', 'Wrong Data', 'Amount cannot be greater than Available Amount.');
                        return;
                    }
                    if(!btData[row].lstChild[i].budgetFrom || (!btData[row].lstChild[i].budgetTo && btData[row].lstChild[i].btType === 'Budget Transfer')){
                        helper.showToast('info', 'Missing Budget', 'Missing Budget for the Budget Code, Please add the Budget First.');
                        return;
                    }
                }
                data.push(btData[row]);
            }
        }
        if(!hasData) {
            helper.showToast('info', 'Missing Data', 'There is no record to save. Please add budget transaction first.');
            return;
        }

        // call save Data
        component.set('v.Spinner', true);
        let action = component.get("c.saveBudgetTransactions");
        action.setParams({
            'data': data
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let data = component.get('v.btData');
                component.set('v.isShowChild', false);
                data.lstChild = [];
                component.set('v.btData', data);
                helper.gobacktoListView(component);
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                console.log(strErr);
                helper.showToast('info', 'Info', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    handleCancel: function(component, event, helper) {
        helper.gobacktoListView(component)
    },
    codeLookupSearch : function(component, event, helper) {
        const lookupComponent = event.getSource();
        const serverSearchAction = component.get('c.kinshipSearchLookup');
        serverSearchAction.setParam('anOptionalParam', 'Code');
        lookupComponent.search(serverSearchAction);
    },
    clearCodeErrorsOnChange: function(component, event, helper) {
        let formData = event.getParam("formData");
        if(formData) {
            let btData = component.get('v.btData');
            let indexes = formData.param.split('_');
            let row = parseInt(indexes[0]);
            let index = parseInt(indexes[1]);
            btData[row].lstChild[index].code = formData.recordId;
            btData[row].lstChild[index].description = formData.subtitle;
            for(let i = 0 ; i < btData[row].lstBudget.length ; i++) {
                if(btData[row].lstBudget[i].Category__c === formData.recordId || 
                    btData[row].lstBudget[i].Laser_Budget_Code__c === formData.recordId ||
                    btData[row].lstBudget[i].Locality_Account_Code_Name__c === formData.recordId){
                    btData[row].lstChild[index].budgetFrom = btData[row].lstBudget[i].Id;
                    btData[row].lstChild[index].avaiAmount = btData[row].lstBudget[i].Available_Balance__c;
                }
            }
            for(let i = 0 ; i < btData[row].lstChild.length ; i++) {
                if(btData[row].lstChild[i].isChild && btData[row].lstChild[i].parentIndex === index){
                    btData[row].lstChild[i].code = formData.recordId;
                    btData[row].lstChild[i].budgetFrom = btData[row].lstChild[index].budgetFrom;
                }
            }
            component.set('v.btData', btData);
        }
    },
    codeToLookupSearch : function(component, event, helper) {
        const lookupComponent = event.getSource();
        const serverSearchAction = component.get('c.kinshipSearchLookup');
        serverSearchAction.setParam('anOptionalParam', 'Code');
        lookupComponent.search(serverSearchAction);
    },
    clearCodeToErrorsOnChange: function(component, event, helper) {
        let formData = event.getParam("formData");
        if(formData) {
            let btData = component.get('v.btData');
            let indexes = formData.param.split('_');
            let row = parseInt(indexes[0]);
            let index = parseInt(indexes[1]);
            btData[row].lstChild[index].code2 = formData.recordId;
            btData[row].lstChild[index].description2 = formData.subtitle;
            for(let i = 0 ; i < btData[row].lstBudget.length ; i++) {
                if(btData[row].lstBudget[i].Category__c === formData.recordId || 
                    btData[row].lstBudget[i].Laser_Budget_Code__c === formData.recordId ||
                    btData[row].lstBudget[i].Locality_Account_Code_Name__c === formData.recordId){
                    btData[row].lstChild[index].budgetTo = btData[row].lstBudget[i].Id;
                    btData[row].lstChild[index].avaiAmount2 = btData[row].lstBudget[i].Available_Balance__c;
                }
            }
            component.set('v.btData', btData);
        }
    },
    gotoBudget: function(component, event, helper) {
        helper.gotoBudgetListView(component)
    }
})