({
    fetchColumns : function(component,event,helper) {
        console.log('***entered in fetch columns');
        component.set('v.columns',[{label: 'Service Name', fieldName: 'Service_Name__c', type: 'text'},	
                                   {label:'Service Unit Measure', fieldName: 'Unit_Measure_Label__c',type: 'text'},
                                   {label: 'List Price',fieldName: 'UnitPrice',type:'currency', typeAttributes: { currencyCode: 'EUR'}},              
                                   {label: 'Service Code', fieldName: 'ProductCode', type: 'text'},                                  
                                   {label: 'Service Description', fieldName: 'Service_Description__c', type: 'text'},
                                   {label: 'Service Family', fieldName: 'Service_Family__c', type: 'text'}]);         
        console.log(component.get("v.columns"));
    },
    checkRowValidity : function(component,event,helper,indexval){
        console.log('***Enterd in checkrowvalidity');
    	var Entries = component.get("v.pricebookentriesfromParent");
        console.log(JSON.stringify(Entries));
        var SelectedEntry = Entries[indexval];
        var validity=false;
        if(SelectedEntry.quantity==undefined || SelectedEntry.quantity=='' ||
           SelectedEntry.categorySelection==undefined || SelectedEntry.categorySelection==''){
            console.log('**Entered in error check');
            if((SelectedEntry.quantity==undefined || SelectedEntry.quantity=='') && 
               (SelectedEntry.categorySelection==undefined || SelectedEntry.categorySelection=='')) {
                SelectedEntry.error =true;
                SelectedEntry.message ='Quantity and Category cannot be empty.';	
                validity= true;
            }
           else if(SelectedEntry.quantity==undefined || SelectedEntry.quantity==''){
                SelectedEntry.error =true;
                SelectedEntry.message ='Quantity cannot be empty.';	
                validity= true;
            }
           else if(SelectedEntry.categorySelection==undefined || SelectedEntry.categorySelection==''){
               console.log('**Enreed in category seelction error');
            	SelectedEntry.error =true;
                SelectedEntry.message ='Category cannot be empty.Please search and select the desired category.';	
                validity= true;
            }
            component.set("v.pricebookentriesfromParent",Entries);
            console.log('********endline');
        }
        return validity;
    },
    CheckList : function(component,event,helper){
    	var Entries = component.get("v.pricebookentriesfromParent");
        var validity = false;
        for(var i=0 ; i< Entries.length ; i++){
        	Entries[i].serviceCheck=true;
            if(Entries[i].quantity==undefined || Entries[i].quantity=='' ||
             Entries[i].categorySelection==undefined || Entries[i].categorySelection==''){
                if((Entries[i].quantity==undefined || Entries[i].quantity=='') && 
                   (Entries[i].categorySelection==undefined || Entries[i].categorySelection=='')) {
                    Entries[i].error =true;
                    Entries[i].message ='Quantity and Category cannot be empty.';	
                    validity=true;
                }
               else if(Entries[i].quantity==undefined || Entries[i].quantity==''){
                    Entries[i].error =true;
                    Entries[i].message ='Quantity cannot be empty.';		
                   validity=true;
                }
               else if(Entries[i].categorySelection==undefined || Entries[i].categorySelection==''){
                    Entries[i].error =true;
                    Entries[i].message ='Category cannot be empty.Please search and select the desired category.';	
                   validity=true;
                }
            }      
        }
        	console.log('****'+JSON.stringify(Entries));
            component.set("v.pricebookentriesfromParent",Entries);
           this.fireSaveEventOnAllCheck(component,event,helper,validity);
    },
    uncheckList : function(component,event,helper){
    	var Entries = component.get("v.pricebookentriesfromParent");
         for(var i=0 ; i< Entries.length ; i++){
            Entries[i].serviceCheck=false;
             console.log('%%%');           
                 Entries[i].error=false;
                 Entries[i].message='';
         } 
        component.set("v.pricebookentriesfromParent",Entries);
        this.fireSaveEventOnAllUncheck(component,event,helper,false);
        
    },
    fireSaveEventOnCheck : function(component,event,helper,indexval,validity){
        console.log('Firsesaveeevent');
        console.log(indexval);
    	var entries = component.get("v.pricebookentriesfromParent");
        console.log(entries[indexval]);
      
        var alreadyselectedServices = component.get("v.setSelectedservices");
        const selectedindex = alreadyselectedServices.indexOf(entries[indexval]);
        if(selectedindex >-1){
            alreadyselectedServices.splice(selectedindex, 1);  
        }    
        alreadyselectedServices.push(entries[indexval]);
        component.set("v.setSelectedservices",alreadyselectedServices);
        //ERROR CHECK ADDED
        var getErrorlist= component.get("v.ErrorList");
        console.log('**VAl of errorlist on savecheck'+JSON.stringify(getErrorlist));
        const index = getErrorlist.indexOf(entries[indexval]);
        if(index >-1){
            getErrorlist.splice(index, 1);    
        }  
        component.set("v.ErrorList",getErrorlist);
        console.log('**Error list length'+JSON.stringify(getErrorlist));
        if(getErrorlist.length>0)
            validity=true;
         //FIRE SELECTED SERVICES EVENT
         var eventval = component.getEvent("CheckedServices");
         eventval.setParams({"SelectedServices": component.get("v.setSelectedservices"),
                            "IsShow":validity}); 
  		 eventval.fire(); 
    },
    
    fireSaveEventOnUnCheck : function(component,event,helper,indexval,validity,errorPresent){
        console.log('**entered in uncheck');
        console.log(indexval);
    	var entries = component.get("v.pricebookentriesfromParent");
        console.log('^^^^^***%%%%%'+JSON.stringify(entries));
        console.log(entries[indexval]);
        var selectedServicesAlready = component.get("v.setSelectedservices");
        const index = selectedServicesAlready.indexOf(entries[indexval]);
        console.log('-------'+index);
		if (index > -1) {
  			selectedServicesAlready.splice(index, 1);
		}
		component.set("v.setSelectedservices",selectedServicesAlready);
        console.log('%%%%%%%%@@@');
        console.log(component.get("v.setSelectedservices").length);
        console.log('&&&&&');
        console.log(JSON.stringify(component.get("v.setSelectedservices")));
        //ADD IT TO ERROR LIST 
        if(errorPresent==true){
            console.log('**Entered in errror present');
            var getErrorlist=component.get("v.ErrorList");
            console.log(JSON.stringify(getErrorlist));
            const index = getErrorlist.indexOf(entries[indexval]);
            if(index <=-1){
                getErrorlist.push(entries[indexval]);
                
            }
            component.set("v.ErrorList",getErrorlist);
            console.log('list val of error on if'+JSON.stringify(component.get("v.ErrorList")));
        }
        else{
           var getErrorlist=component.get("v.ErrorList");
            const index = getErrorlist.indexOf(entries[indexval]);
            if(index >-1){
            	getErrorlist.splice(index, 1);
                
            }  
            component.set("v.ErrorList",getErrorlist);
            console.log('list val of error on else'+JSON.stringify(component.get("v.ErrorList")));
        }
        console.log('**Val fo error list'+JSON.stringify(getErrorlist));
        //FIRE SELECTED SERVICES EVENT
        if(component.get("v.setSelectedservices").length>0 && validity==false &&
           component.get("v.ErrorList").length==0)
           validity=false;
        else
            validity=true;
        var eventval = component.getEvent("CheckedServices");
        eventval.setParams({"SelectedServices": component.get("v.setSelectedservices"),
                            "IsShow":validity});
  		 eventval.fire(); 
    },
    rowDeletion : function(component,event,helper,indexval){
    	var entries = component.get("v.pricebookentriesfromParent");  
        var selectedServicesAlready = component.get("v.setSelectedservices");
        var errorlist =component.get("v.ErrorList");
        if(selectedServicesAlready!=undefined && selectedServicesAlready.length >0){
            const index = selectedServicesAlready.indexOf(entries[indexval]);
            if(index >-1){
                selectedServicesAlready.splice(index,1);
            }
            component.set("v.setSelectedservices",selectedServicesAlready);
        }
        if(errorlist!=undefined && errorlist.length >0){
            const index = errorlist.indexOf(entries[indexval]);
            if(index > -1){
               errorlist.splice(index,1); 
            }
            component.set("v.ErrorList",errorlist);
        }
        console.log('**VAl of errorlist'+JSON.stringify(component.get("v.ErrorList")));
        console.log('**VAl of selectedservices'+JSON.stringify(component.get("v.setSelectedservices")));
        entries.splice(indexval,1);
        component.set("v.pricebookentriesfromParent",entries);
        if(component.get("v.ErrorList")!=undefined && component.get("v.ErrorList")!='' && component.get("v.ErrorList").length >0){
        	var eventval = component.getEvent("CheckedServices");
        	eventval.setParams({"SelectedServices": component.get("v.setSelectedservices"),
                            "IsShow":true});
  		 	eventval.fire();         
        }
        else if(component.get("v.ErrorList")!=undefined && component.get("v.ErrorList").length==0 &&
               component.get("v.setSelectedservices")!=undefined && component.get("v.setSelectedservices")!='' &&  component.get("v.setSelectedservices").length > 0){
        	console.log('**Entereed here in seleected serv>0 in row deletion'); 
            var eventval = component.getEvent("CheckedServices");
        	eventval.setParams({"SelectedServices": component.get("v.setSelectedservices"),
                            "IsShow":false});
  		 	eventval.fire();     
        }
         else if(component.get("v.ErrorList")!=undefined && component.get("v.ErrorList").length==0 &&
               component.get("v.setSelectedservices")!=undefined &&  component.get("v.setSelectedservices").length== 0){
         	var eventval = component.getEvent("CheckedServices");
        	eventval.setParams({"SelectedServices": component.get("v.setSelectedservices"),
                            "IsShow":true});
  		 	eventval.fire();    
         }
    },
    fireSaveEventOnUnCheck1 : function(component,event,helper,indexval,validity){
        console.log('**entered in uncheck');
        console.log(indexval);
    	var entries = component.get("v.pricebookentriesfromParent");
        //console.log(entries[indexval]);
        var selectedServicesAlready = component.get("v.setSelectedservices");
        const index = selectedServicesAlready.indexOf(entries[indexval]);
        console.log('-------'+index);
		if (index > -1) {
  			selectedServicesAlready.splice(index, 1);
		}
		component.set("v.setSelectedservices",selectedServicesAlready);
        console.log('%%%%%%%%@@@');
        console.log(component.get("v.setSelectedservices").length);
        //FIRE SELECTED SERVICES EVENT
        if(component.get("v.setSelectedservices").length>0 && validity==false)
           validity=false;
        else
            validity=true;
        var eventval = component.getEvent("CheckedServices");
        eventval.setParams({"SelectedServices": component.get("v.setSelectedservices"),
                            "IsShow":validity});
  		 eventval.fire(); 
    },
    fireSaveEventOnAllCheck : function(component,event,helper,validity){
        console.log('**All check fire');
    	var entries = component.get("v.pricebookentriesfromParent");
      
        if(validity==true){
            component.set("v.setSelectedservices",[]);
        }
        else
            component.set("v.setSelectedservices",entries);
        console.log('**Val of setselectedServices'+component.get("v.setSelectedservices"));
        //FIRE SELECTED SERVICES EVENT
        var eventval = component.getEvent("CheckedServices");
        eventval.setParams({"SelectedServices": component.get("v.setSelectedservices"),
                            "IsShow":validity});
  		 eventval.fire(); 
        
    },
    fireSaveEventOnAllUncheck : function(component,event,helper){
    	component.set("v.setSelectedservices",[]);
         //FIRE SELECTED SERVICES EVENT
        var eventval = component.getEvent("CheckedServices");
        eventval.setParams({"SelectedServices": component.get("v.setSelectedservices"),
                            "IsShow":false});
  		 eventval.fire(); 
    },
    checkDuplicateService :function(component,event,helper,indexval){
       console.log('**Enterd in duplicate check');
        console.log('**al of indexval'+indexval);
       var entries = component.get("v.pricebookentriesfromParent");
        console.log('entries ***'+JSON.stringify(entries));
       var indexentry = entries[indexval];
        console.log(JSON.stringify(indexentry));
       var alreadyselected = component.get("v.setSelectedservices");
         console.log('***duplicate service');
         console.log(JSON.stringify(alreadyselected));
         console.log(alreadyselected.length);
      
        var isError =false;
        if(alreadyselected.length >0){
            for(var i =0 ; i<alreadyselected.length ; i++){
                if(indexentry.IsNewService!=undefined && indexentry.IsNewService==true){
                    console.log('**Enterd in first if isnewservice is true');
                    console.log('**indexentry.servicenamval'+ indexentry.Service_Name__c);
                    console.log('**alreadyselceted servicenam'+ alreadyselected[i].Service_Name__c);
                	if(alreadyselected.indexOf(indexentry)<=-1 && indexentry.Service_Name__c ==alreadyselected[i].Service_Name__c && 
                       indexentry.categorySelection.Id == alreadyselected[i].categorySelection.Id ){
                        isError=true;
                        break;
                    }    
                }
               else if(alreadyselected.indexOf(indexentry)<=-1 && indexentry.Product2Id==alreadyselected[i].Product2Id && 
                   indexentry.categorySelection.Id == alreadyselected[i].categorySelection.Id){                  
                       isError=true;
                       break;                      
                }
            }
        }
       
       return isError;
    },
    fetchCSAValues : function(component,event,helper,categoryval){
        console.log('**Entered in fetchcsavalues');
        console.log(typeof categoryval);
        console.log(categoryval);
        var action= component.get("c.getCategoryandSPT");
        action.setParams({
        	categoryval : categoryval
        });	
        action.setCallback(this,function(response){            
            var state = response.getState();   
            console.log('**VAl of state');
            if (state === "SUCCESS") {
                console.log('**Val of success');
                var result=response.getReturnValue();
                console.log('**VAl of mandate and spt'+JSON.stringify(response.getReturnValue())); 
                  //FOR MANDATE OPTIONS
                if((result.MandateNames!=null && result.MandateNames.length==0) || (result.Names!=null && result.Names.length==0)){
                	//component.set("v.openModal",false);
                   //this.showToast('Error','error','Please contact your administrator to provide Mandate types, SPT and service name code for this category if not added or use a different category.');
                }
                else{
                   if(result.MandateNames!=null && result.MandateNames.length>0){
                        var mandatenames = response.getReturnValue().MandateNames;
                        var items=[];
                        var item = {                           
                            "label": "---None---",
                            "value": ''
                        };
                        items.push(item);
                        
                       for (var i = 0; i < mandatenames.length; i++) {
                            var item = {                           
                                "label": mandatenames[i].label,
                                "value": mandatenames[i].value.toString()
                            };
                            items.push(item);
                        } 
                        
                        component.set("v.Mandateoptions",items);                    
                    }  
                    //KIN 237
                    if(result.MandateDesc!=null && result.MandateDesc.length>0){
                        var Mandatevalues=[];
                        var Mandatenames = response.getReturnValue().MandateDesc;
                        //GETTING SCROLLBAR ON ITEMS
                        var items = [];
                        /*var item = {                           
                            "label": "",
                            "value": ''
                        };
                        items.push(item);*/
                        console.log('desc Mandate'+Mandatenames);
                        console.log(Mandatenames[0].label);
                        console.log(Mandatenames[0].value);
                        for (var i = 0; i < Mandatenames.length; i++) {
                            var item = {                           
                                "label": Mandatenames[i].label,
                                "value": Mandatenames[i].value.toString()
                            };
                            items.push(item);
                        }                      
                        component.set("v.Mandatedescmap",items);  
                    } 
                    
                    //ends
                    if(result.Names!=null && result.Names.length>0){
                        var sptvalues=[];
                        var sptnames = response.getReturnValue().Names;
                        //GETTING SCROLLBAR ON ITEMS
                        var items = [];
                        var item = {                           
                            "label": "---None---",
                            "value": ''
                        };
                        items.push(item);
                        for (var i = 0; i < sptnames.length; i++) {
                            var item = {                           
                                "label": sptnames[i].label,
                                "value": sptnames[i].value.toString()
                            };
                            items.push(item);
                        }                      
                        component.set("v.SPToptions",items);  
                    } 
                    // KIN 237
                    if(result.SPTDesc!=null && result.SPTDesc.length>0){
                        var sptvalues=[];
                        var sptnames = response.getReturnValue().SPTDesc;
                        //GETTING SCROLLBAR ON ITEMS
                        var items = [];
                        /*var item = {                           
                            "label": "",
                            "value": ''
                        };
                        items.push(item);*/
                        console.log('desc'+sptnames);
                        console.log(sptnames[0].label);
                        console.log(sptnames[0].value);
                        for (var i = 0; i < sptnames.length; i++) {
                            var item = {                           
                                "label": sptnames[i].label,
                                "value": sptnames[i].value.toString()
                            };
                            items.push(item);
                        }                      
                        component.set("v.SPTdescmap",items);  
                    } 
                    // ends
                }
            }   
            else{
                system.debug('***Error');
            }
        });        
        $A.enqueueAction(action);     
    },
    //KIN 237
    getSNCDesc : function(component,event,helper,selectedOptionValue){
        var action= component.get("c.getSNCDesc");
        	action.setParams({
            	sncValue: selectedOptionValue
        	});	   
            action.setCallback(this, function(response){
            	var state = response.getState();
            	console.log('**VAl of state'+state);
            	console.log('***response'+response.getReturnValue());
            	if (state === "SUCCESS") { 
                	var sncvalues=[];
                    var sncdesc = response.getReturnValue();
                     console.log('***Sncnames'+sncdesc);
                    if(sncdesc==null){
                    	component.set("v.showSNCDesc",false);
                    }
                   else{
                      
                        component.set("v.selSNCDesc",sncdesc);    
                       component.set("v.showSNCDesc",true);
                   }
                }
            
            });     
    	$A.enqueueAction(action);
    },
    getSPTNamesforSPT : function(component,event,helper,selectedOptionValue){
        var action= component.get("c.getSPTNames");
        	action.setParams({
            	sptValue: selectedOptionValue
        	});	   
            action.setCallback(this, function(response){
            	var state = response.getState();
            	console.log('**VAl of state'+state);
            	console.log('***response'+response.getReturnValue());
            	if (state === "SUCCESS") { 
                	var sptvalues=[];
                    var sptnames = response.getReturnValue();
                     console.log('***Sptnames'+sptnames);
                    if(sptnames==null){
                    	component.set("v.openModal",false);
                    	this.showToast('Error','error','Please contact your administrator to provide  service name code for this selected spt if not added or use a different category.');    
                    }
                   else{
                     //GETTING SCROLLBAR ON ITEMS
                        var items = [];
                         var item = {                           
                                "label": "---None---",
                                "value": ''
                        };
                        items.push(item);
                        for (var i = 0; i < sptnames.length; i++) {
                            var item = {                           
                                "label": sptnames[i].label,
                                "value": sptnames[i].value.toString()
                            };
                            items.push(item);
                        } 
                      
                        component.set("v.SPTNames",items);    
                        //CHECK THAT ALREADY SELECTED VALUE PRESENT ON LIST IF NOT THEN CLEAR THE VALUE OF SELECTED
                        var SPTNameoptions = component.get("v.SPTNames");
                        var alreadySelectedSPTName = component.get("v.SPTNameselected");
                        console.log('**Already selected sptname'+alreadySelectedSPTName);
                        console.log('sptnamelabel'+component.get("v.SPTNameselectedlabel"));
                        var isPresent=false;
                        console.log('**Sptname options'+SPTNameoptions);
                        for(var i=0; i< SPTNameoptions.length; i++){
                            if(alreadySelectedSPTName==SPTNameoptions[i].value){
                                isPresent=true;
                                break;
                            }
                        }
                        if(isPresent==false){
                            component.set("v.SPTNameselected",'');
                            component.set("v.SPTNameselectedlabel",'');
                        }     
                    } 
                }      
         });     
    	$A.enqueueAction(action);     
    },
    checkCSASave : function(component,event,helper){
        component.set("v.CSAError",false);
        var sptval = component.get("v.SPTselected");
        var sptname = component.get("v.SPTNameselected");
        var mandateval = component.get("v.MandateSelected");
        var sptlabel= component.get("v.SPTselectedlabel");
        var sptnamelabel= component.get("v.SPTNameselectedlabel");
        var mandatelabel= component.get("v.MandateSelectedlabel");
        var recipientval =component.get("v.recipient");
        
        console.log('**VAl of sptval'+sptval);
        console.log('**val of sptname'+sptname);
        console.log('**Val of sptname selectedlabel'+sptnamelabel);
        console.log('**Desc val'+ component.get("v.Description"));
        if(sptname!='24'){
       		component.set("v.Description",'');
        } 
        if(sptval=='' || sptval==undefined){
        	component.set("v.ShowSpinner",false);
            sptname='';
            component.set("v.SPTNameselected",sptname);
            component.set("v.SPTselectedlabel",sptname);
            component.set("v.CSAError",true);
        	this.showToast('Error', 'Error', 'Please add Service Placement Type. If No SPT present, contact administrator to add SPT for this category or choose different category.');     
        }
        if(sptname=='' || sptname==undefined){
            console.log('**Entered here in spt blank');
           component.set("v.ShowSpinner",false);
           component.set("v.CSAError",true);             
            this.showToast('Error', 'Error', 'Please add Service Name Code. If No service Name code present, contact administrator or choose different SPT.');     
        }
        if(mandateval=='' || mandateval==undefined){
        	 component.set("v.ShowSpinner",false);
            component.set("v.CSAError",true);
            this.showToast('Error', 'Error', 'Please add Mandate Type. If No Mandate Type present, contact administrator to add Mandate Type for this category or choose different category.');
        }
        if (sptname=='24' && (component.get("v.Description")=='' || component.get("v.Description")==undefined)){
            console.log('****@@@**');
            component.set("v.ShowSpinner",false);
            component.set("v.CSAError",true);
        	this.showToast('Error', 'Error', 'Please add Description.'); 
            
        }   
        if(recipientval=='' || recipientval=='0'){
           component.set("v.ShowSpinner",false);
            component.set("v.CSAError",true);
        	this.showToast('Error', 'Error', 'Please provide Parent recipient.'); 
        }
         if(component.get("v.costcenterSelection")==undefined || component.get("v.costcenterSelection")==''){
        	component.set("v.ShowSpinner",false);
            component.set("v.CSAError",true);
        	this.showToast('Error', 'Error', 'Please provide cost center.');     
        }
           
    },
    checkIVESave : function(component,event,helper){
    	 component.set("v.CSAError",false);   
         var recipientval =component.get("v.recipientIVE");
         if(recipientval=='' || recipientval=='0'){
           component.set("v.ShowSpinner",false);
            component.set("v.CSAError",true);
        	this.showToast('Error', 'Error', 'Please provide Parent recipient.'); 
        }
        if(component.get("v.costcenterSelection")==undefined || component.get("v.costcenterSelection")==''){
        	component.set("v.ShowSpinner",false);
            component.set("v.CSAError",true);
        	this.showToast('Error', 'Error', 'Please provide cost center.');     
        }
    },
    checkFundSave : function(component,event,helper){
        component.set("v.CSAError",false); 
    	if(component.get("v.costcenterSelection")==undefined || component.get("v.costcenterSelection")==''){
        	component.set("v.ShowSpinner",false);
            component.set("v.CSAError",true);
        	this.showToast('Error', 'Error', 'Please provide cost center.');     
        }    
    },
    showToast : function(type, title, message) {       
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : title,
            message: message,
            duration:' 4000',
            key: 'info_alt',
            type: type,
            mode: 'pester'
        });
        toastEvent.fire();
    },
    CSAvaluesBlank : function(component,event,helper){
        component.set("v.SPToptions",'[]');
        component.set("v.SPTNames",'[]');
        component.set("v.Mandateoptions",'[]');
        component.set("v.SPTselected",'');
        component.set("v.SPTNameselected",'');
        component.set("v.MandateSelected",'');
        component.set("v.Description",'');
        component.set("v.SPTselectedlabel",'');
        component.set("v.SPTNameselectedlabel",'');
        component.set("v.MandateSelectedlabel",'');
        component.set("v.recipient",'');
        component.set("v.CSAError",false);
        // KIN 228
        component.set("v.AgeError",false);
        component.set("v.AgeError1",false);
        component.set("v.AgeError2",false);
        //ends
        console.log('****');
        console.log(component.get("v.SPTselected"));
        console.log(component.get("v.SPTNameselected"));
         
    },
    PrepopulateCSAvalues:function(component,event,helper,selectedRecord){
        console.log('**VAl of selectedRecord'+JSON.stringify(selectedRecord));
        console.log('&&&&&&&&&&@@@');
    	component.set("v.SPTselected",selectedRecord.SPTselected);
        console.log('**Selectdspt'+component.get("v.SPTselected"));
        component.set("v.SPTselectedlabel",selectedRecord.SPTselectedlabel);
         console.log('**Selectdsptlabel'+component.get("v.SPTselectedlabel"));
        component.set("v.SPToptions",selectedRecord.SPToptions);
        component.set("v.SPTNames",selectedRecord.SPTNames);
          console.log('**selectedsptname'+component.get("v.SPTNameselected"));
        component.set("v.SPTNameselected",selectedRecord.SPTNameselected);
        component.set("v.SPTNameselectedlabel",selectedRecord.SPTNameselectedlabel);
        component.set("v.MandateSelected",selectedRecord.MandateSelected);
         console.log('**selectedmandatename'+component.get("v.MandateSelected"));
        component.set("v.Mandateoptions",selectedRecord.Mandateoptions);
        component.set("v.MandateSelectedlabel",selectedRecord.MandateSelectedlabel);
        if(selectedRecord.Description!='' && selectedRecord.Description!=undefined)
        	component.set("v.Description",selectedRecord.Description);
        component.set("v.recipient",selectedRecord.recipient);
        if(selectedRecord.costcenterSelection!='' && selectedRecord.costcenterSelection!=undefined)
            component.set("v.costcenterSelection",selectedRecord.costcenterSelection);
        if(selectedRecord.costcenterIds!='' && selectedRecord.costcenterIds!=undefined)
        	component.set("v.CostCenterIds",selectedRecord.costcenterIds);
        
        component.set("v.CSAError",false);
        selectedRecord.CSAError=false;
        //KIN 237
        var categoryval= selectedRecord.categorySelection.Id;
        console.log('categoryval'+categoryval);
        var action= component.get("c.getCategoryandSPT");
        action.setParams({
        	categoryval : categoryval
        });	
        action.setCallback(this,function(response){            
            var state = response.getState();   
            console.log('**VAl of state');
            if (state === "SUCCESS") {
                console.log('**Val of success');
                var result=response.getReturnValue();
                console.log('**VAl of mandate and spt'+JSON.stringify(response.getReturnValue())); 
                  //FOR MANDATE OPTIONS
                if((result.MandateNames!=null && result.MandateNames.length==0) || (result.Names!=null && result.Names.length==0)){
                	//component.set("v.openModal",false);
                   //this.showToast('Error','error','Please contact your administrator to provide Mandate types, SPT and service name code for this category if not added or use a different category.');
                }
                else{
                    if(result.MandateDesc!=null && result.MandateDesc.length>0){
                        var Mandatevalues=[];
                        var Mandatenames = response.getReturnValue().MandateDesc;
                        //GETTING SCROLLBAR ON ITEMS
                        var items = [];
                        /*var item = {                           
                            "label": "",
                            "value": ''
                        };
                        items.push(item);*/
                        console.log('desc Mandate'+Mandatenames);
                        console.log(Mandatenames[0].label);
                        console.log(Mandatenames[0].value);
                        for (var i = 0; i < Mandatenames.length; i++) {
                            var item = {                           
                                "label": Mandatenames[i].label,
                                "value": Mandatenames[i].value.toString()
                            };
                            items.push(item);
                        }                      
                        component.set("v.Mandatedescmap",items);  
                    } 
                    if(result.SPTDesc!=null && result.SPTDesc.length>0){
                        var sptvalues=[];
                        var sptnames = response.getReturnValue().SPTDesc;
                        //GETTING SCROLLBAR ON ITEMS
                        var items = [];
                        /*var item = {                           
                            "label": "",
                            "value": ''
                        };
                        items.push(item);*/
                        console.log('desc'+sptnames);
                        console.log(sptnames[0].label);
                        console.log(sptnames[0].value);
                        for (var i = 0; i < sptnames.length; i++) {
                            var item = {                           
                                "label": sptnames[i].label,
                                "value": sptnames[i].value.toString()
                            };
                            items.push(item);
                        }                      
                        component.set("v.SPTdescmap",items);  
                    } 
                }
            }
            else{
                system.debug('***Error');
            }
        });
        $A.enqueueAction(action);  
        //ends
        
    },
    MakeCSAValuesBlank:function(component,event,helper,indexRow){
        console.log('**VAl of index row in csavalueblank'+indexRow);
    	var pricebookentries = component.get("v.pricebookentriesfromParent");
        var changedEntry = pricebookentries[indexRow];  
        console.log('*********');
        console.log(JSON.stringify(changedEntry));
        changedEntry.SPTselected='';
        changedEntry.SPTselectedlabel='';
        changedEntry.SPToptions='';
        changedEntry.SPTNames='';
        changedEntry.SPTNameselected='';
        changedEntry.SPTNameselectedlabel='';
        changedEntry.MandateSelected='';
        changedEntry.Mandateoptions='';
        changedEntry.MandateSelectedlabel='';
        changedEntry.Description='';
        changedEntry.recipient='';
        changedEntry.CSAError=false;
        component.set("v.pricebookentriesfromParent",pricebookentries);
        console.log('**Changed entry row'+JSON.stringify(component.get("v.pricebookentriesfromParent")));
    },
    fetchUnitMeasure : function(component,event,helper){
        var action = component.get("c.getUnitMeasure");
         action.setCallback(this,function(response){           
            var state = response.getState();
            console.log('**State val'+state);
            if(state==="SUCCESS"){
                var result=response.getReturnValue();
                console.log(JSON.stringify(result));
                if(result!=null && result.message!=undefined && result.message=='Unit Measuer found' && 
                  result.UnitMeasures!=undefined && result.UnitMeasures.length >0 ){                	
                        var items=[];
                        var item = {                           
                            "label": "---None---",
                            "value": ''
                        };
                        items.push(item);
                        
                       for (var i = 0; i <  result.UnitMeasures.length; i++) {
                            var item = {                           
                                "label": result.UnitMeasures[i].label,
                                "value": result.UnitMeasures[i].value.toString()
                            };
                            items.push(item);
                        }                         
                       component.set("v.UnitMeasures",items);   
                     console.log('*unitemesaure val'+JSON.stringify(component.get("v.UnitMeasures")));
                }
            }
       });
        //KIN 228
        console.log('caseIdforAge'+component.get("v.caseIdforAge"));
        var action1= component.get("c.getCaseAge");
        	action1.setParams({
            	caseId: component.get("v.caseIdforAge")
        	});	   
            action1.setCallback(this, function(response){
            	var state = response.getState();
            	console.log('**VAl of state'+state);
            	console.log('***response'+response.getReturnValue());
                if (state === "SUCCESS") { 
                    var caseval = response.getReturnValue();
                     console.log('***case'+caseval);
                    console.log('***case'+caseval.Age__c);
                    if(caseval!=null){
                    	component.set("v.selectedCaseAge",caseval.Age__c);
                    }
                }
            });
        
        $A.enqueueAction(action); 
        if(component.get("v.caseIdforAge")){
        $A.enqueueAction(action1);
        }
        //ends
    },
    checkForServiceCreationError: function(component,event,helper,index){
        var validity=false;
        var pricebookentries = component.get("v.pricebookentriesfromParent");
        var changedEntry = pricebookentries[index];
        console.log('**VAl of changedEntry'+JSON.stringify(changedEntry));
      
       //MAKE ERROR BLANK IF EVERYTHING IS BLANK
       if(changedEntry.IsNewService!=undefined && changedEntry.IsNewService==true && (changedEntry.Service_Name__c=='' || changedEntry.Service_Name__c==undefined) &&
              (changedEntry.Service_Unit_Measure__c==undefined || changedEntry.Service_Unit_Measure__c=='') && 
              (changedEntry.UnitPrice==undefined || changedEntry.UnitPrice=='') && (changedEntry.quantity=='' || changedEntry.quantity==undefined) &&
              (changedEntry.categorySelection==undefined || changedEntry.categorySelection=='')){
            console.log('**entered in 8th');
           changedEntry.error =false;
            changedEntry.message=''; 
            validity=false;
            helper.fireSaveEventOnUnCheck(component,event,helper,index,validity,false);   
       } 
      //MAKE ERROR BLANK IF PRICEBOOKENTRY IS THERE AND QUANTITY AND CATEGORY IS BLANK  
      else if ((changedEntry.IsNewService==undefined || (changedEntry.IsNewService!=undefined && changedEntry.IsNewService==false)) && 
              (changedEntry.quantity=='' || changedEntry.quantity==undefined) && (changedEntry.categorySelection==undefined || changedEntry.categorySelection=='')){
       		console.log('**entered in 9th');
           changedEntry.error =false;
            changedEntry.message=''; 
            validity=false;
            helper.fireSaveEventOnUnCheck(component,event,helper,index,validity,false);          
      } 
               
      else if((changedEntry.Service_Name__c==undefined || changedEntry.Service_Name__c=='') && 
               changedEntry.IsNewService!=undefined && changedEntry.IsNewService==true){
            console.log('**Entered in 1st');
        	changedEntry.error=true;
            changedEntry.message='Provide Service Name.'; 
            validity=true;
           this.fireSaveEventOnUnCheck(component,event,helper,index,validity,true);       
        }
        
       else if((changedEntry.Service_Unit_Measure__c==undefined || changedEntry.Service_Unit_Measure__c=='') && 
             changedEntry.IsNewService!=undefined && changedEntry.IsNewService==true){
            console.log('**Entered in 3rd'); 
           changedEntry.error=true;
            changedEntry.message='Select a Unit Measure.'; 
            validity=true;
            this.fireSaveEventOnUnCheck(component,event,helper,index,validity,true);   
        }
        else if((changedEntry.UnitPrice==undefined || changedEntry.UnitPrice=='') && 
               changedEntry.IsNewService==true){
             console.log('**Entered in 4th');
            changedEntry.error=true;
            changedEntry.message='Provide Unit Price.'; 
            validity=true;
            this.fireSaveEventOnUnCheck(component,event,helper,index,validity,true);   
        }
      
        else if(changedEntry.quantity==undefined || changedEntry.quantity=='' || changedEntry.quantity==0){
            console.log('***enteredd in provide qunaity error 5th');
            changedEntry.error=true;
            changedEntry.message='Provide quantity.'; 
            validity=true;
            this.fireSaveEventOnUnCheck(component,event,helper,index,validity,true);   
        }
        else if(changedEntry.quantity!=undefined && changedEntry.quantity!='' && changedEntry.quantity>0 &&
           changedEntry.quantity.toString().indexOf(".")==1 && changedEntry.quantity.toString().split('.')[1].length > 2){
           console.log('**Entered in quantity decimal more than 2');
           console.log('**Val of quantity'+ changedEntry.quantity.toString().split('.')[1].length );
           changedEntry.error=true;
           changedEntry.message='You can add quantity upto two decimal places'; 
           validity=true;
           this.fireSaveEventOnUnCheck(component,event,helper,index,validity,true);       
            
        } 
        else if(changedEntry.categorySelection==undefined || changedEntry.categorySelection==''){
             console.log('**Entered in 6th');
            changedEntry.error=true;
            changedEntry.message='Provide a Category.'; 
            validity=true;
            this.fireSaveEventOnUnCheck(component,event,helper,index,validity,true);   
        }
        //NOT ALLOWING CATEGORY OF TYPE SPECIAL WELFARE AS PER KIN-1347
        else if(changedEntry.categorySelection!='' && changedEntry.categorySelection!=undefined && changedEntry.categorySelection!=null &&  changedEntry.categorySelection.Category_Type__c!=undefined &&
              changedEntry.categorySelection.Category_Type__c=='Special Welfare'  ){
         	changedEntry.error=true;
            changedEntry.message='Special Welfare Payments may only be processed using a Case Action.'; 
            validity=true;
            this.fireSaveEventOnUnCheck(component,event,helper,index,validity,true);        
         } 
        else if(changedEntry.categorySelection!='' && changedEntry.categorySelection!=undefined && changedEntry.categorySelection!=null &&  changedEntry.categorySelection.Category_Type__c!=undefined &&
          changedEntry.categorySelection.Category_Type__c=='CSA' && (changedEntry.SPTNameselected=='' || changedEntry.SPTNameselected==undefined || 
           changedEntry.MandateSelected=='' || changedEntry.MandateSelected==undefined || changedEntry.SPTselected=='' || changedEntry.SPTselected==undefined
           || changedEntry.recipient=='' || changedEntry.recipient=='0' || changedEntry.recipient==undefined  ||
            changedEntry.costcenterSelection==undefined || changedEntry.costcenterSelection==undefined)){
          	console.log('**entered in 7th');
            changedEntry.error=true;
            changedEntry.message='For CSA categories, CSA information should be filled.'; 
            validity=true;
            this.fireSaveEventOnUnCheck(component,event,helper,index,validity,true);    
      }
      else if(changedEntry.categorySelection!='' && changedEntry.categorySelection!=undefined && changedEntry.categorySelection!=null &&  changedEntry.categorySelection.Category_Type__c!=undefined &&
          changedEntry.categorySelection.Category_Type__c=='IVE' && (changedEntry.recipientIVE=='' || changedEntry.recipientIVE=='0' || changedEntry.recipientIVE==undefined 
             || changedEntry.costcenterSelection==undefined || changedEntry.costcenterSelection=='' )){
          	console.log('**entered in 7th');
            changedEntry.error=true;
            changedEntry.message='For IVE categories, Parent recipient information should be filled.'; 
            validity=true;
            this.fireSaveEventOnUnCheck(component,event,helper,index,validity,true);    
      }    
      else if(changedEntry.categorySelection!='' && changedEntry.categorySelection!=undefined && changedEntry.categorySelection!=null &&  changedEntry.categorySelection.Category_Type__c!=undefined &&
          changedEntry.categorySelection.Category_Type__c!='IVE' && changedEntry.categorySelection.RecordType!=undefined && changedEntry.categorySelection.RecordType.DeveloperName!=undefined && changedEntry.categorySelection.RecordType.DeveloperName=='LASER_Category'   
           && (changedEntry.costcenterSelection==undefined || changedEntry.costcenterSelection=='' )){
          	console.log('**entered in 8th');
            changedEntry.error=true;
            changedEntry.message='Please provide Cost Center.'; 
            validity=true;
            this.fireSaveEventOnUnCheck(component,event,helper,index,validity,true);    
      }  
      //ADDED FOR UNITMEASURE DAY IF SERVICE NAME CODE IS 26
      else if(changedEntry.categorySelection!=undefined && changedEntry.categorySelection!='' && changedEntry.categorySelection!=null &&  changedEntry.categorySelection.Category_Type__c!=undefined &&
          changedEntry.categorySelection.Category_Type__c=='CSA' && changedEntry.SPTNameselected!='' && changedEntry.SPTNameselected!=undefined 
              && changedEntry.SPTNameselected=='26' && changedEntry.Service_Unit_Measure__c!=undefined && changedEntry.Service_Unit_Measure__c!='' 
             && changedEntry.Service_Unit_Measure__c!='2') {
           changedEntry.error=true;
            changedEntry.message='For SPT name selected as 26, Unit mesaure should be Day.'; 
            validity=true;
            this.fireSaveEventOnUnCheck(component,event,helper,index,validity,true);  
      }
     //ADDED FOR    
      else {
            console.log('**entered in 10th');
            var count=helper.checkDuplicateService(component,event,helper,index);
            console.log('**VAl of count'+count);
            if(count==true){
            	changedEntry.error=true;
                changedEntry.message='You cannot select the same category with same service again.'; 
                validity=true;
             	 helper.fireSaveEventOnUnCheck(component,event,helper,index,validity,true);	    
            }
            else{
                changedEntry.error =false;
                changedEntry.message='';
                validity=false;
                helper.fireSaveEventOnCheck(component,event,helper,index,validity);
            }     
        } 
     	component.set("v.pricebookentriesfromParent",pricebookentries);
        console.log('**VAl of parententries'+JSON.stringify(component.get("v.pricebookentriesfromParent")));
    },
    
    DuplicateNameCheck :function(component,event,helper,index){  
        var validity=false;
        var pricebookentries=component.get("v.pricebookentriesfromParent");
        var isError=false;
        var changedEntry = pricebookentries[index];
    	if((changedEntry.Service_Name__c!=undefined || changedEntry.Service_Name__c!='') && 
          changedEntry.IsNewService!=undefined && changedEntry.IsNewService==true){
             console.log('**Entered in duplicatename method');
        	 var entries = component.get("v.pricebookentriesfromParent");
            
            for(var i=0 ; i <entries.length ; i++){
                if(i!=index && entries[i]!=undefined && entries[i].Service_Name__c==changedEntry.Service_Name__c){
                	changedEntry.error=true;
                    changedEntry.message='This service Name already exist.'; 
                    validity=true;
                    isError=true;
                   helper.fireSaveEventOnUnCheck(component,event,helper,index,validity,true); 
                    break;
                }    
            }
           
        } 
        component.set("v.pricebookentriesfromParent",pricebookentries); 
        return isError;
    },
  /*  fetchCostCenters: function(component,event,helper,categoryvalId,fipsid){
    	var action=component.get("c.getCostCenters");
        action.setParams({
            categoryId : categoryvalId,
            FipsId : fipsid
            
        });
         action.setCallback(this,function(response){           
            var state = response.getState(); 
            if(state==="SUCCESS"){
                 var result=response.getReturnValue();
                console.log('**Costcenter ids'+result);
                if(result!=null && result.length >0){
                   component.set("v.CostCenterIds",result);
                    component.set("v.showCostCenterLookup",true);
                    if(result.length==1)
                        this.fetchOneCostCenter(component,event,helper,result[0]);
                }
            } 
             else if(state==="ERROR"){
                 var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                }    
             }
        });        
        $A.enqueueAction(action);       
    },
    fetchOneCostCenter:function(component,event,helper,costcenterid){
        console.log('***Enterd here');
        console.log('**VAl of costcenterid'+costcenterid);
    	var action=component.get("c.getOneCostCenter");
        action.setParams({
            costcenterId : costcenterid.toString()
            
        });
         action.setCallback(this,function(response){           
            var state = response.getState(); 
            if(state==="SUCCESS"){
                 var result=response.getReturnValue();
                console.log('**Costcenter id one'+JSON.stringify(result));
                if(result!=null){
                    component.set("v.costcenterSelection",result);
                  
                }
            }      
        });        
        $A.enqueueAction(action);       
    } */
    fetchCostCenters: function(component,event,helper,categoryvalId,fipsid,indexrow){
    	var action=component.get("c.getCostCenters");
        action.setParams({
            categoryId : categoryvalId,
            FipsId : fipsid
            
        });
         action.setCallback(this,function(response){           
            var state = response.getState(); 
            if(state==="SUCCESS"){
                 var result=response.getReturnValue();
                console.log('**Costcenter ids'+result);
                if(result!=null && result.length >0){
                    var pricebookentries = component.get("v.pricebookentriesfromParent");
                   //component.set("v.CostCenterIds",result);
                   // component.set("v.showCostCenterLookup",true);
                    var pricebookentries = component.get("v.pricebookentriesfromParent");
        			var changedEntry = pricebookentries[indexrow];
                    changedEntry.CostCenterIds = result;
                    changedEntry.showCostCenterLookup=true;
                    component.set("v.pricebookentriesfromParent",pricebookentries);
                    console.log('**VAl of changedEntry*****'+JSON.stringify(component.get("v.pricebookentriesfromParent")));    
                    if(result.length==1)
                        this.fetchOneCostCenter(component,event,helper,result[0],indexrow);
                    else{
                         //component.set("v.costcenterSelection",'');
                        changedEntry.costcenterSelection='';
                        //VALIDATION CHECKS
                        var count=false;
                        
                        count=this.DuplicateNameCheck(component,event,helper,indexrow);
                        if(count==false)
                            this.checkForServiceCreationError(component,event,helper,indexrow); 
                    }
                        
                }
            } 
             else if(state==="ERROR"){
                 //component.set("v.CostCodeCheck",true);
                 var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } 
                 var count=false;
                 count=this.DuplicateNameCheck(component,event,helper,indexrow);
                 if(count==false)
                     this.checkForServiceCreationError(component,event,helper,indexrow);
             }
        });        
        $A.enqueueAction(action);       
    },
    fetchOneCostCenter:function(component,event,helper,costcenterid,indexrow){
        console.log('***Enterd here');
        console.log('**VAl of costcenterid'+costcenterid);
    	var action=component.get("c.getOneCostCenter");
        action.setParams({
            costcenterId : costcenterid.toString()
            
        });
         action.setCallback(this,function(response){           
            var state = response.getState(); 
            if(state==="SUCCESS"){
                 var result=response.getReturnValue();
                console.log('**Costcenter id one'+JSON.stringify(result));
                if(result!=null){
                   // component.set("v.costcenterSelection",result);
                    var pricebookentries = component.get("v.pricebookentriesfromParent");
        			var changedEntry = pricebookentries[indexrow];
                    changedEntry.costcenterSelection=result;
                  	component.set("v.pricebookentriesfromParent",pricebookentries);
                     console.log('**VAl of changedEntryon one cost center1'+JSON.stringify(component.get("v.pricebookentriesfromParent")));    
                    var count=false;
                    count=this.DuplicateNameCheck(component,event,helper,indexrow);
                    if(count==false)
                        this.checkForServiceCreationError(component,event,helper,indexrow);
                }
            }  
            else{
                 //component.set("v.CostCodeCheck",true);
                 changedEntry.costcenterSelection='';
                 var count=false;
                count=this.DuplicateNameCheck(component,event,helper,indexrow);
                if(count==false)
                    this.checkForServiceCreationError(component,event,helper,indexrow);
             }
        });        
        $A.enqueueAction(action);       
    }
    
  
})