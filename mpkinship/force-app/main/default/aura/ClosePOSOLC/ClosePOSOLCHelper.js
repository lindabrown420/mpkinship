({
	FetchOnLoad : function(component,event,helper) {
		var action=component.get("c.FetchInitialValues");
        action.setParams({
        	recordid : component.get("v.recordId")
        });	
         action.setCallback(this,function(response){           
            var state = response.getState();
            console.log('**State val'+state);
            if(state==="SUCCESS"){
                 var result=response.getReturnValue(); 
                 console.log('*VAl of result'+result);
                if(result!=null){
                    if(result.opp!=null){
                    	component.set("v.Initialopportunity",result.opp);
                        console.log('**Action selected val'+ result.opp.Action_Selected__c);
                        if(result.opp.Action_Selected__c!=null){
                            component.set("v.actionSelected",result.opp.Action_Selected__c);
                            console.log('**VAl of actionselected'+component.get("v.actionSelected"));
                            //component.find("ActionId").set("v.value",result.Action_Selected__c);
                            if(result.opp.Action_Selected__c=='Await Final Invoice'){
                                component.set("v.showInvoices",true);
                                if(result.invoices!=null && result.invoices.length > 0){
                                	component.set("v.data",result.invoices);    
                                    this.setColumns(component,event,helper);
                                }
                            }
                            else if(result.opp.Action_Selected__c=='Change End Date'){
                                component.set("v.showEnddate",true);
                                if(result.opp.End_Date__c !=null)
                                    component.set("v.EndDate",result.opp.End_Date__c);
                                else{
                                    //show today date
                                     let todayDate = new Date();
                                    console.log('todaydate'+todayDate);
                                    var dd = todayDate.getDate();
                                    var mm = todayDate.getMonth()+1; //As January is 0.
                                    var yyyy = todayDate.getFullYear();
                                    var finaldate = yyyy+'-'+mm+'-'+dd;
                                    console.log('finaldate val'+finaldate);
                                    component.set("v.EndDate",finaldate);
                                }
                                   
                            }
                        }
                    }    
                }
            }
         });
        $A.enqueueAction(action);        
	},
    setColumns:function (component, event, helper){
        // KIN - 1064 change in line no 5(Invoice Line Item Name)
        component.set('v.columns', [
            {label: 'Payment Name', fieldName: 'Name', type: 'text',editable: false},
            {label: 'Encumbered Amount', fieldName: 'Amount_Paid__c', type: 'currency', editable: false},
            {label: 'Payment Amount', fieldName: 'npe01__Payment_Amount__c', type: 'currency', editable: false },
            {label: 'Scheduled Date', fieldName: 'npe01__Scheduled_Date__c', type: 'text', editable: false },
            {label: 'Category',fieldName: 'Category_Name__c',type:'text',editable: false}
            
        ]); 
       
    },
    getInvoices : function(component,event,helper){
    	var action=component.get("c.FetchInvoices");
        action.setParams({
        	recordid : component.get("v.recordId")
        });	
         action.setCallback(this,function(response){           
            var state = response.getState();
            console.log('**State val'+state);
            if(state==="SUCCESS"){
                 var result=response.getReturnValue(); 
                if(result!=null){
                    console.log('**VAl of result'+ JSON.stringify(result));
                	component.set("v.data",result); 
                    this.setColumns(component,event,helper);
                    component.set("v.noInvoiceFound",false);
                    component.set("v.showInvoices",true);
                }
                else{
                    component.set("v.noInvoiceFound",true);
                }
            } 
          });
        $A.enqueueAction(action);     
    },
     showToast : function(type, title, message) {
       
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : title,
            message: message,
            duration:' 5000',
            key: 'info_alt',
            type: type,
            mode: 'pester'
        });
        toastEvent.fire();
    },
     navigatetourl : function(component,event,helper){
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": component.get("v.recordId"),
            "slideDevName": "related"
        });
        navEvt.fire();
    }
})