({
	doInit : function(component, event, helper) {
		helper.FetchOnLoad(component,event,helper);	
	},
    handleActionChange: function(component,event,helper){
        var actionvalue= component.find("ActionId").get("v.value"); 
        component.set("v.actionSelected",actionvalue);
        if(actionvalue!=undefined && actionvalue=='Close Immediately'){
            //DO NOTHING FOR NOW
            component.set("v.showInvoices",false);
            component.set("v.finalawaitIds",'[]');
            component.set("v.showEnddate",false);
        }
        else if(actionvalue!=undefined && actionvalue=='Await Final Invoice'){
            component.set("v.showEnddate",false);
			component.set("v.finalawaitIds",'[]');
            helper.getInvoices(component,event,helper);
        }
        else if(actionvalue!=undefined && actionvalue=='Change End Date'){
            component.set("v.showInvoices",false);
            component.set("v.finalawaitIds",'[]');
            let todayDate = new Date();
            console.log('todaydate'+todayDate);
            var dd = todayDate.getDate();
			var mm = todayDate.getMonth()+1; //As January is 0.
			var yyyy = todayDate.getFullYear();
            var finaldate = yyyy+'-'+mm+'-'+dd;
            console.log('finaldate val'+finaldate);
            component.set("v.EndDate",finaldate);
            component.set("v.showEnddate",true);
                
        }
    },
    getDate : function(component,event,helper){
        console.log('**Val of date'+component.get("v.EndDate"));
    },
    updateRows :function(component,event,helper){
     	var selectedRows = event.getParam('selectedRows');
        console.log('**val of select4edrows');
        console.log(JSON.stringify(selectedRows));
        let paymentids=[];
        for(var i=0; i< selectedRows.length ; i++){
            if(paymentids.indexOf(selectedRows[i].Id)==-1)
        		paymentids.push(selectedRows[i].Id);    
        }
    	
    	component.set("v.finalawaitIds",paymentids);
        
    },
    cancel :function(component,event,helper){
         $A.get("e.force:closeQuickAction").fire();
    },
    save : function(component,event,helper){
        component.set("v.ShowSpinner",true);
        var picklistval = component.get("v.actionSelected");
        console.log('**picklistval'+ picklistval);
        if(picklistval==undefined || picklistval==''){
            helper.showToast('Error','Error','Please choose a desired action.');
        }
        else if(picklistval!=undefined && picklistval!=''){
            console.log('**Al of ifnalawaits'+component.get("v.finalawaitIds"));
            console.log('**Al of ifnalawaits***'+component.get("v.finalawaitIds").length);
            if(picklistval=='Await Final Invoice' && component.get("v.showInvoices")==true && 
               component.get("v.finalawaitIds")!=undefined && component.get("v.finalawaitIds")=='[]'){
                console.log('**Enterd here in line 62');
                helper.showToast('Error','Error','Please select invoice(s) for final await');
            }
            else if(picklistval=='Change End Date' && component.get("v.showEnddate")==true && (
                component.get("v.EndDate")=='' || component.get("v.EndDate")==undefined)){
                helper.showToast('Error','Error','Please provide an end date');
            }
            else{
                //NO ERRORS FINAL SAVE
                console.log('fainal await ids'+JSON.stringify(component.get("v.finalawaitIds")));
            	var action =component.get("c.SaveRecords");
                action.setParams({
                    recordid : component.get("v.recordId"),
                    picklistval : component.get("v.actionSelected"),
                    EndDate : component.get("v.EndDate"),
                    invoiceIds : JSON.stringify(component.get("v.finalawaitIds"))
                });	
                action.setCallback(this,function(response){           
                	var state = response.getState();
                    component.set("v.ShowSpinner",false);
                	console.log('**State val'+state);
                    if(state==="SUCCESS"){
                    	 var result=response.getReturnValue(); 
                        console.log('result val');
                        console.log(result);
                        if(result!=null){
                            if(result.picklistval=='Change End Date')
                            	helper.showToast('Success','Success','Record will be successfully closed on the provided end date and End date has been updated.');
                        	else if(result.picklistval=='Await Final Invoice')
                                helper.showToast('Success','Success','Record has been updated successfully.');
                            else if(result.picklistval=='Close Immediately')
                                helper.showToast('Success','Success','Record has been updated Successfully.');
                            //helper.navigatetourl(component,event,helper);
                            location.reload();
                        }
                    }
                    else if(state==="ERROR"){
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " + 
                                         errors[0].message);
                                helper.showToast('Error','error',errors[0].message);  
                                $A.get("e.force:closeQuickAction").fire();
                            }
                        } else {
                            console.log("Unknown error");
                        }
                  }    
                 
                 });
                $A.enqueueAction(action);   
                
            }
        }    
    }
})