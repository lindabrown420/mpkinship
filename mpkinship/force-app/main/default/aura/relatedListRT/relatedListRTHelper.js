({
	createTableColumns : function(component, event, helper) {
		var colums = [];
        var f1 = component.get( "v.fieldAPI1" );
        var f1Label = component.get( "v.fieldLabel1" );
        if(f1 && f1 != ''){
            colums.push({ label: f1Label, fieldName: 'recordURL', type: 'url', 
             	typeAttributes: { label: { fieldName: f1 }, tooltip:{ fieldName: f1 }, target: '_blank' }  });
            
        }
        var f2 = component.get( "v.fieldAPI2" );
        var f2Label = component.get( "v.fieldLabel2" );
        if(f2 && f2 != ''){
            colums.push({ label: f2Label, fieldName: f2, type: 'text' });
            
        }
        var f3 = component.get( "v.fieldAPI3" );
        var f3Label = component.get( "v.fieldLabel3" );
        if(f3 && f3 != ''){
            colums.push({ label: f3Label, fieldName: f3, type: 'text' });
            
        }
        var f4 = component.get( "v.fieldAPI4" );
        var f4Label = component.get( "v.fieldLabel4" );
        if(f4 && f4 != ''){
            colums.push({ label: f4Label, fieldName: f4, type: 'text' });
            
        }
        var f5 = component.get( "v.fieldAPI5" );
        var f5Label = component.get( "v.fieldLabel5" );
        if(f5 && f5 != ''){
            colums.push({ label: f5Label, fieldName: f5, type: 'text' });
            
        }
        component.set('v.columns', colums);
        
	},
    createSelectStr: function(component, event, helper) {
        var selectStr = '';//component.get('v.selectStr');
        var f1 = component.get( "v.fieldAPI1" );
        var isLookup1 = component.get( "v.isLookup1" );
        if(f1 && f1 != ''){
            selectStr += f1;
        }
        var f2 = component.get( "v.fieldAPI2" );
        if(f2 && f2 != ''){
            if(selectStr != ''){
                selectStr += ', ';
            }
            selectStr += f2;
        }
        var f3 = component.get( "v.fieldAPI3" );
        if(f3 && f3 != ''){
            if(selectStr != ''){
                selectStr += ', ';
            }
            selectStr += f3;
        }
        var f4 = component.get( "v.fieldAPI4" );
        if(f4 && f4 != ''){
            if(selectStr != ''){
                selectStr += ', ';
            }
            selectStr += f4;
        }
        var f5 = component.get( "v.fieldAPI5" );
        if(f5 && f5 != ''){
            if(selectStr != ''){
                selectStr += ', ';
            }
            selectStr += f5;
        }
        component.set('v.selectStr',selectStr);
    }
})