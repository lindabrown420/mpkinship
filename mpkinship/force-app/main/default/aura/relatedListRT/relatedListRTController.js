({
	init: function (component, event, helper) {
		var actions = [
            { label: 'View', name: 'view' },
            { label: 'Delete', name: 'delete' }
        ];
        helper.createTableColumns(component, event, helper);
        helper.createSelectStr(component, event, helper);
        var temObjName = component.get( "v.sobjectName" );  
        var action = component.get( "c.fetchRecs" );  
        action.setParams({  
            recId: component.get( "v.recordId" ),  
            sObjName: temObjName,  
            parentFldNam: component.get( "v.parentFieldName" ),  
            strCriteria: component.get( "v.criteria" ),
            selectStr: component.get( "v.selectStr" )
        });  
        action.setCallback(this, function(response) {  
            var state = response.getState();  
            if ( state === "SUCCESS" ) {  
                  
                var tempTitle = component.get( "v.title" );  
                var res = response.getReturnValue();
                for(var i = 0; i < res.length; i++){
                    res[i].recordURL = '/'+res[i].Id;
                }
                component.set( "v.data", response.getReturnValue() );  
                //component.set( "v.title", tempTitle + response.getReturnValue().strTitle );  
                  
            }  
        });  
        $A.enqueueAction(action); 

	},
    handleRowAction: function (cmp, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');

        switch (action.name) {
            case 'view':
                alert('Showing Details: ' + JSON.stringify(row));
                break;
            case 'delete':
                helper.removeBook(cmp, row);
                break;
        }
    },
    viewRelatedList: function (component, event, helper) {  
          
        var navEvt = $A.get("e.force:navigateToRelatedList");  
        navEvt.setParams({  
            "entityApiName": component.get("v.childRelName"),
            "relatedListId": component.get( "v.sobjectName" ),  
            "parentRecordId": component.get( "v.recordId" )  
        });
        window.open('/lightning/r/'+component.get("v.childRelName")+'/'+component.get( "v.recordId" )+'/related/'+ component.get( "v.sobjectNames" )+'/view');
        //navEvt.fire();  
    }
})