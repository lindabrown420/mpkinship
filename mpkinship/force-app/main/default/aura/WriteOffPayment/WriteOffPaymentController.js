({
    doInit : function(component,event,helper){
    	var action=component.get("c.getInvoiceType");
        action.setParams({                
                recordid : component.get("v.recordId") 
        	});	
        action.setCallback(this,function(response){
             var state = response.getState();
             if(state==="SUCCESS"){
             	 var result=response.getReturnValue();
                 if(result!=null && result.Invoice_Type__c!=undefined)
                     component.set("v.InvoiceType",result.Invoice_Type__c);
             }    
             
        });
        $A.enqueueAction(action);                       
    },
	cancel : function(component, event, helper) {
		 $A.get("e.force:closeQuickAction").fire();	
	},
    SaveChange:function(component,event,helper){
        component.set("v.ShowSpinner",true);
         var action=component.get("c.WriteOffInvoice");
             action.setParams({                
                recordid : component.get("v.recordId") 
        	});	
            action.setCallback(this,function(response){
           
                var state = response.getState();
                console.log('**State val'+state);            
                console.log('**REsponse return val'+JSON.stringify(response.getReturnValue()));
                if(state==="SUCCESS"){
                    component.set("v.ShowSpinner",false);
                    var result=response.getReturnValue();  
                    if(result.msg=='Record updated'){
                      
                       helper.showToast('Success','Success','Invoice has been write off successfully.');
                       $A.get("e.force:closeQuickAction").fire();	
                       $A.get('e.force:refreshView').fire(); 
                    	//helper.navigatetourl(component,event,helper);
                        
                    }
                }
                else if(state==="ERROR"){
                    component.set("v.ShowSpinner",false);
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                     errors[0].message);
                            helper.showToast('Error','error',errors[0].message);  
                            $A.get("e.force:closeQuickAction").fire();
                        }
                    } else {
                        console.log("Unknown error");
                	}
                }    
             });
        	$A.enqueueAction(action);      
        }
    
})