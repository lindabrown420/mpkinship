({
    getResponse: function(component) {
        var action = component.get("c.getAllAgenda");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                console.log("Data: \n" + result);
                var eventArr = [];
                result.forEach(function(key) {
                    eventArr.push({
                        'id':key.Id,
                        'start': key.Meeting_Date__c + ' ' + key.Meeting_Start3_Time__c.slice(0, -1).concat('Z'),
                        'end': key.Meeting_Date__c + ' ' + key.Meeting_End_Time2__c.slice(0, -1).concat('Z'),
                        'title':key.Name,
                        'url': '/'+key.Id
                    });
                });
                this.loadCalendar(eventArr, component);
                
            } else if (state === "INCOMPLETE") {
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    loadCalendar :function(data, component){
        var selectInfo;
        //var isCreateable = component.get('v.isCreateable');
        var m = moment();
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay,listWeek'
            },
            defaultDate: m.format(),
            editable: false,
            selectable: true,
            navLinks: true,
            weekNumbers: true,
            weekNumbersWithinDays: true,
            weekNumberCalculation: 'ISO',
            eventLimit: true,
            events:data,
            eventClick: function(info) {
            },
            dateClick: function(info) {
            },
            select: function(info) {
                let isCreateable = component.get('v.isCreateable');
                if(!isCreateable) {
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : 'Info',
                        message: $A.get('$Label.c.KINSHIP_FAPT_AGENDA_MSG'),
                        duration:' 4000',
                        key: 'info_alt',
                        type: 'info', // success , error, info
                        mode: 'pester'//'sticky'
                    });
                    toastEvent.fire();
                    return;
                }
                let startDate = info._d;
                if(startDate) {
                    let month = '' + (startDate.getMonth() + 1);
                    let day = '' + startDate.getDate();
                    let year = startDate.getFullYear();
                    if (month.length < 2) month = '0' + month;
                    if (day.length < 2) day = '0' + day;
                    component.set('v.eventDate', [year, month, day].join('-'));
                    let h = info.hour();
                    if(h < 8) {
                        h = 8;
                    }
                    if(h > 20) {
                        h = 20;
                    }
                    let startTime = h + ':' + (info.minute() === 0?'00':info.minute()) + ':' + '00.000';//  selectInfo.end._d;
                    component.set('v.startTime', startTime);
                    //let strEndDate = selectInfo.end._d.toTimeString();
                    //component.set('v.endTime', strEndDate.substr(0, 8) + '.000');
                }
                component.set('v.isCSAAgenda', true);
                /*let createRecordEvent = $A.get("e.force:createRecord");
                createRecordEvent.setParams({
                    "entityApiName": "CSA_Meeting_Agenda__c",
                    "defaultFieldValues": {
                        "Meeting_Date__c": selectInfo.start._d,
                        "Meeting_Start_Time__c": null,
                        "Meeting_End_Time__c" : null
                    },
                    "navigationLocation":'LOOKUP',
                    "panelOnDestroyCallback": function(event) {
                        var action = component.get("c.getAllAgenda");
                        action.setCallback(this, function(response) {
                            var state = response.getState();
                            if (state === "SUCCESS") {
                                var result = response.getReturnValue();
                                console.log("Data: \n" + result);
                                var eventArr = [];
                                result.forEach(function(key) {
                                    eventArr.push({
                                        'id':key.Id,
                                        'start':key.Start_DateTime__c,
                                        'end':key.End_DateTime__c,
                                        'title':key.Name,
                                        'url': '/'+key.Id
                                    });
                                });
                                $('#calendar').fullCalendar('removeEvents');
                                $('#calendar').fullCalendar('addEventSource', eventArr);
                                $('#calendar').fullCalendar('refetchEvents');
                                
                            } else if (state === "INCOMPLETE") {
                            } else if (state === "ERROR") {
                                var errors = response.getError();
                                if (errors) {
                                    if (errors[0] && errors[0].message) {
                                        console.log("Error message: " + errors[0].message);
                                    }
                                } else {
                                    console.log("Unknown error");
                                }
                            }
                        });
                        $A.enqueueAction(action);
                    }
                });
                createRecordEvent.fire();*/
            },
            selectAllow: function(info) {
                selectInfo = info;
            }
        });
    }
})