({
    scriptsLoaded : function(component, event, helper) {
        helper.getResponse(component);
    },
    init : function(component, event, helper) {
        let action = component.get("c.checkProfile");
        //action.setParams({});
        component.set("v.isCreateable", false);
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS"){
                let d = a.getReturnValue();
                if (d) {
                    component.set("v.isCreateable", true);
                }
            } 
        });
        $A.enqueueAction(action);
    },
    closeModel: function(component, event, helper) {
        let action = component.get("c.getAllAgenda");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                console.log("Data: \n" + result);
                var eventArr = [];
                result.forEach(function(key) {
                    eventArr.push({
                        'id':key.Id,
                        'start':key.Meeting_Date__c + ' ' + key.Meeting_Start3_Time__c.slice(0, -1).concat('Z'),
                        'end':key.Meeting_Date__c + ' ' + key.Meeting_End_Time2__c.slice(0, -1).concat('Z'),
                        'title':key.Name,
                        'url': '/'+key.Id
                    });
                });
                $('#calendar').fullCalendar('removeEvents');
                $('#calendar').fullCalendar('addEventSource', eventArr);
                $('#calendar').fullCalendar('refetchEvents');
                
            } else if (state === "INCOMPLETE") {
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
        component.set('v.isCSAAgenda', false);
    },
    openModel: function(component, event, helper) {
        component.set('v.isCSAAgenda', true);
    }
})