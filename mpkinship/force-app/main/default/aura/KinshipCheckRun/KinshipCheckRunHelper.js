({
    showToast : function(type, title, message) {
        let strTitle = title.split('\n').join('');
        let strMessage = message.split('\n').join('');
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : strTitle,
            message: strMessage,
            duration:' 4000',
            key: 'info_alt',
            type: type, // success , error, info
            mode: 'pester'//'sticky'
        });
        toastEvent.fire();
    },
    fetchData: function(component, objName, apiName, strNon, strValue){
        let action = component.get("c.getPicklistvalues");
        action.setParams({
            'objectName': objName,
            'field_apiname': apiName,
            'strNone': strNon
        });
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS"){
                let d = a.getReturnValue();
                if (d) {
                    let data = [];
                    for(let i = 0 ; i < d.length ; i++) {
                        //if(d[i] !== 'Created') {
                            data.push({'label' : d[i], 'value':d[i]});
                        //}
                    }
                    component.set("v." + strValue, data);
                }
            } 
        });
        $A.enqueueAction(action);
    },
    getData : function(component) {
        component.set('v.Spinner', true);
        let action = component.get("c.searchCheckData");
        let data = component.get("v.vendorSelection");
        let lstVendor = [];
        if(data) {
            data.forEach(element => {
                lstVendor.push(element.id);
            });
        }
        let startDate = component.get("v.startDate");
        let endDate = component.get("v.endDate");
        let searchStatus = component.get("v.searchStatus");
        action.setParams({
            lstVendor: lstVendor,
            searchStatus: searchStatus,
            startDate: startDate,
            endDate: endDate
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let data = response.getReturnValue();
                if(data) {
                    component.set("v.data", data);
                }else{
                    this.showToast('error', 'Error', 'Please contact your Administrator for more details.');
                }
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                this.showToast('error', 'Error', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    updateData : function(component, data, status) {
        component.set('v.Spinner', true);
        let action = component.get("c.updateChecks");
        action.setParams({
            lstCheck: data,
            status: status
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                this.showToast('success', 'Successful', 'Successful');
                let oldData = component.get('v.data');
                let setData = new Set();
                for(let i = 0 ; i < data.length ; i++) {
                    setData.add(data[i]);
                }
                for(let i = 0 ; i < oldData.length ; i++) {
                    if(setData.has(oldData[i].id)) {
                        oldData[i].status = status;
                    }
                }
                component.set('v.data', oldData);
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                this.showToast('error', 'Error', strErr);
            }
            component.set('v.Spinner', false);
            //this.getData(component);
        });
        $A.enqueueAction(action);
    },
    initData : function(component) {
        component.set('v.Spinner', true);
        let action = component.get("c.getLastestSyncDate");
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let data = response.getReturnValue();
                if(data) {
                    component.set("v.lastSyncDT", data);
                }
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    convertArrayOfObjectsToCSV : function(component, objectRecords){
        let csvStringResult, counter, keys, columnDivider, lineDivider;
       
        if (objectRecords == null || !objectRecords.length) {
            return null;
         }

        columnDivider = ',';
        lineDivider =  '\n';
 
        keys = ['checkNo','cashAcc','issueDate', 'vendorName','acct', 'description', 'amount'];
        let headers = ['Check Number', 'Cash Acc', 'Date Issue', 'Vendor', 'ACCT', 'Description', 'Amount'];
        
        csvStringResult = '';
        csvStringResult += headers.join(columnDivider);
        csvStringResult += lineDivider;
 
        for(let i=0; i < objectRecords.length; i++){   
            counter = 0;
           
             for(let sTempkey in keys) {
                let skey = keys[sTempkey] ;  
 
                if(counter > 0){ 
                    csvStringResult += columnDivider; 
                }   
               
               csvStringResult += '"'+ objectRecords[i][skey]+'"'; 
               
               counter++;
 
            }
            csvStringResult += lineDivider;
        }
       
        return csvStringResult;        
    },
    getUserSessionId: function(component){
        let action = component.get("c.getSessionId");
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS"){
                let d = a.getReturnValue();
                component.set('v.sessionId', d);
            } 
        });
        $A.enqueueAction(action);
    }
})