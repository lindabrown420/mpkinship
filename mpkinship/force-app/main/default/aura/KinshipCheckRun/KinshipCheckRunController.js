({
    init : function(component, event, helper) {
        component.set('v.columns', [
            {label: 'Id', fieldName: 'checkUrl', type: 'url', typeAttributes: { 
                label: {
                    fieldName: 'Name'
                },
                target: '_blank'
            }},
            {label: 'Check No', fieldName: 'checkNo', type: 'text'},
            {label: 'Status', fieldName: 'status', type: 'text'},
            {label: 'Vendor', fieldName: 'vendorUrl', type: 'url', typeAttributes: { 
                label: {
                    fieldName: 'vendorName'
                },
                target: '_blank'
            }},
            {label: 'Issue Date', fieldName: 'issueDate', type: 'date',
                typeAttributes:{
                    month: "2-digit",
                    day: "2-digit",
                    year: "numeric"
                }
            },
            {label: 'Amount', fieldName: 'amount', type: 'currency', typeAttributes: { currencyCode: 'USD' }},
            {label: 'Is Printed', fieldName: 'isPrinted', type: 'boolean'},
            {label: 'Organization', fieldName: 'organization', type: 'text'},
            {label: 'Description', fieldName: 'description', type: 'text'}
        ]);
        //helper.initData(component)
        helper.fetchData(component,'Kinship_Check__c','Status__c','','statusoption');
        helper.getUserSessionId(component);
    },
    searchCheck : function(component, event, helper) {
        helper.getData(component);
    },
    printCheck : function(component, event, helper) {
        let startNumber = component.get('v.beginNumber');
        if(!startNumber) {
            helper.showToast('info', 'Missing Start Number', 'Please input Start Number.');
            return;
        }
        let istartNumber = 0;
        try{
            istartNumber = parseInt(startNumber);
        }catch(e){
            helper.showToast('info', 'Invalid Number', 'Please input a valid Start Number.');
            return;
        }

        let data = component.find('paymentrecord').getSelectedRows();
        let lstCheck = [];
        let isFalse = false;
        for(let i = 0 ; i < data.length ; i++) {
            if(data[i].checkNo) {
                helper.showToast('info', 'Check Number already assigned to the check', 'Please select a record with a blank Check Number.');
                isFalse = true;
                return;
            }
            lstCheck.push(data[i].id);
        }
        if(isFalse) return;

        component.set('v.Spinner', true);
        let action = component.get("c.updateCheckNo");
        action.setParams({
            lstId: lstCheck,
            startNumber: istartNumber
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let lstUpdate = response.getReturnValue();
                let olddata = component.get('v.data');
                for(let i = 0 ; i < olddata.length ; i++) {
                    for(let j = 0 ; j < lstUpdate.length ; j++){
                        if(lstUpdate[j].Id === olddata[i].id){
                            olddata[i].checkNo = lstUpdate[j].Check_No__c;
                        }
                    }
                }
                component.set('v.data', olddata);
                component.set('v.isModalOpen', false);

                let newLocation = window.location.protocol + '//' + window.location.host + '/lightning/o/Loop__Document_Request__c/list?filterName=00B18000001yOPFEA2';
                let redirectUrl = window.location.protocol + '//' + window.location.host + '/apex/loop__masslooplus?';
                
                redirectUrl += '&recordIds='+lstCheck.join(',');
                redirectUrl += '&sessionId=' + component.get('v.sessionId');
                redirectUrl += '&ddpIds=a2718000006qxUjAAI';
                redirectUrl += '&deploy=a2518000001WV32AAG';
                redirectUrl += '&autorun=true';
                redirectUrl += '&retURL='+newLocation;
                window.open(redirectUrl ,'_blank');
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                if(strErr.indexOf('DUPLICATE_VALUE') !== -1){
                    helper.showToast('error', 'Duplicate Check Number', 'Duplicate Check Number found. Please input another Start Check Number.');
                } else {
                    helper.showToast('error', 'Error', strErr);
                }
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    printCheckWithCheckNo : function(component, event, helper) {
        let data = component.find('paymentrecord').getSelectedRows();
        if(data.length <= 0) {
            helper.showToast('info', 'Missing Record', 'Please choose a Check Record before clicking the "Print" Button.');
            return;
        }
        let lstCheck = [];
        let isFalse = false;
        for(let i = 0 ; i < data.length ; i++) {
            if(!data[i].checkNo) {
                helper.showToast('info', 'Check Without Check Number', 'Please select a record without a blank Check Number.');
                isFalse = true;
                return;
            }
            if(data[i].isPrinted) {
                helper.showToast('info', 'Check Already Printed', 'Please Select a Check that is not printed yet.');
                isFalse = true;
                return;
            }
            lstCheck.push(data[i].id);
        }
        if(isFalse) return;

        component.set('v.Spinner', true);
        let action = component.get("c.updateChecksPrinted");
        action.setParams({
            lstCheck: lstCheck
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.isModalOpen', false);

                let newLocation = window.location.protocol + '//' + window.location.host + '/lightning/o/Loop__Document_Request__c/list?filterName=00B18000001yOPFEA2';
                let redirectUrl = window.location.protocol + '//' + window.location.host + '/apex/loop__masslooplus?';
                
                redirectUrl += '&recordIds='+lstCheck.join(',');
                redirectUrl += '&sessionId=' + component.get('v.sessionId');
                redirectUrl += '&ddpIds=a2718000006qxUjAAI';
                redirectUrl += '&deploy=a2518000001WV32AAG';
                redirectUrl += '&autorun=true';
                redirectUrl += '&retURL='+newLocation;
                window.open(redirectUrl ,'_blank');
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                if(strErr.indexOf('DUPLICATE_VALUE') !== -1){
                    helper.showToast('error', 'Duplicate Check Number', 'Duplicate Check Number found. Please input another Start Check Number.');
                } else {
                    helper.showToast('error', 'Error', strErr);
                }
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    exportToCSV: function(component, event, helper) {
        let data = component.find('paymentrecord').getSelectedRows();
        if(data.length <= 0) {
            helper.showToast('info', 'Missing Record', 'Please choose a Check Record before clicking the "Export CSV" button.');
            return;
        }
        
        let csv = helper.convertArrayOfObjectsToCSV(component, data);   
        if (csv == null){return;} 
          
	    var hiddenElement = document.createElement('a');
        hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
        hiddenElement.target = '_self';
        hiddenElement.download = 'CheckCSV.csv';
        document.body.appendChild(hiddenElement);
    	hiddenElement.click();
    },
    vendorLookupSearch : function(component, event, helper) {
        const lookupComponent = event.getSource();
        const serverSearchAction = component.get('c.kinshipSearchLookup');
        serverSearchAction.setParam('anOptionalParam', 'Account');
        lookupComponent.search(serverSearchAction);
    },
    clearVendorErrorsOnChange: function(component, event, helper) {
        const selection = component.get('v.vendorSelection');
        const errors = component.get('v.vendorErrors');

        if (selection.length && errors.length) {
            component.set('v.vendorErrors', []);
        }
    }, 
    closeModel: function(component, event, helper) {
        component.set('v.isModalOpen', false);
    },
    openModel: function(component, event, helper) {
        let data = component.find('paymentrecord').getSelectedRows();
        if(data.length <= 0) {
            helper.showToast('info', 'Missing Record', 'Please choose a Check Record before clicking the "Print" Button.');
            return;
        }
        let isFalse = false;
        for(let i = 0 ; i < data.length ; i++) {
            if(data[i].checkNo) {
                helper.showToast('info', 'Check Number already assigned to the check', 'Please select a record with a blank Check Number.');
                isFalse = true;
                return;
            }
        }
        if(isFalse) return;
        component.set('v.isModalOpen', true);
    }
})