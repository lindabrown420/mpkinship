({
    showToast : function(type, title, message) {
        if(type.toUpperCase() == 'ERROR'){
            type = 'info';
        }
        
        let strTitle = title.split('\n').join('');
        let strMessage = message.split('\n').join('');
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : strTitle,
            message: strMessage,
            duration:' 4000',
            key: 'info_alt',
            type: type, // success , error, info
            mode: 'pester'//'sticky'
        });
        toastEvent.fire();
    },
    getData : function(component) {
        component.set('v.Spinner', true);
        let stage = component.get('v.strStage');
        if(stage === 'Open') {
            component.set('v.isShowComplete', true);
            component.set('v.isShowPost', true);
            component.set('v.isShowReOpen', false);
        } else if(stage === 'Completed') {
            component.set('v.isShowComplete', false);
            component.set('v.isShowPost', true);
            component.set('v.isShowReOpen', true);
        } else {
            component.set('v.isShowComplete', false);
            component.set('v.isShowPost', false);
            component.set('v.isShowReOpen', false)
        }
        let action = component.get("c.getUnpostROC");
        action.setParams({
            stage: stage
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let data = response.getReturnValue();
                if(data) {
                    component.set("v.data", data);
                }else{
                    this.showToast('error', 'Error', 'Please contact your Administrator for more details.');
                }
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                this.showToast('error', 'Error', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    openModel: function(component, label) {
        let data = component.find('rocrecord').getSelectedRows();
        if(data.length <= 0) {
            this.showToast('info', 'Missing Record', 'Please choose a record before clicking the button.');
            return;
        }
        let month1 = ['June', 'July', 'August', 'September', 'October', 'November', 'December'];
        let month2 = ['January', 'February', 'March', 'April', 'May'];
        let today = new Date();
        let year1, year2;
        if(today.getMonth() <= 4) {
            year1 = ' ' + (today.getFullYear() - 1);
            year2 = ' ' + (today.getFullYear());
        } else {
            year1 = ' ' + (today.getFullYear());
            year2 = ' ' + (today.getFullYear() + 1);
        }
        let ypOptions = [];
        for(let i = 0 ; i < month1.length ; i++) {
            ypOptions.push({'label': month1[i] + year1, 'value': month1[i] + year1});
        }
        for(let i = 0 ; i < month2.length ; i++) {
            ypOptions.push({'label': month2[i] + year2, 'value': month2[i] + year2});
        }
        component.set('v.ypOptions', ypOptions);
        component.set('v.isYPModel', true);
        component.set('v.labelYP', label);
    }
})