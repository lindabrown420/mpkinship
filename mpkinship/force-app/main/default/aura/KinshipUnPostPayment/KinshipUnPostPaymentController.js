({
    init : function(component, event, helper) {
        component.set('v.columns', [
            {label: 'Name', fieldName: 'rocurl', type: 'url', typeAttributes: { 
                label: {
                    fieldName: 'name'
                },
                target: '_blank'
            }},
            {label: 'Amount', fieldName: 'amount', type: 'currency', typeAttributes: { currencyCode: 'USD' }},
            {label: 'Fiscal Year', fieldName: 'fyUrl', type: 'url',sortable: true, typeAttributes: { 
                label: {
                    fieldName: 'fiscalYear'
                },
                target: '_blank'
            }},
            {label: 'Stage', fieldName: 'stage', type: 'text'},
            {label: 'Posting Period', fieldName: 'postingPeriod', type: 'text'},
            {label: 'Created Date', fieldName: 'createdDate', type: 'date-local',sortable: true,
                typeAttributes:{
                    month: "2-digit",
                    day: "2-digit",
                    year:"numeric"
                }
            },
            {label: 'Created By', fieldName: 'createdBy', type: 'text'}
        ]);
        helper.getData(component);
    },
    createdROC : function(component, event, helper) {
        window.open('/lightning/n/Create_Report_of_Collections', '_blank');
    },
    gotoLaser : function(component, event, helper) {
        window.open('/lightning/n/LASER_Month_End_Close', '_blank');
    },
    gotoLedrs : function(component, event, helper) {
        window.open('/lightning/n/LEDRS_Month_End_Close', '_blank');
    },
    completeandpost : function(component, event, helper) {
        let data = component.find('rocrecord').getSelectedRows();
        if(data.length <= 0) {
            helper.showToast('info', 'Missing Record', 'Please choose a record before clicking the button.');
            return;
        }
        let datePosted = component.get("v.datePosted");
        if(!datePosted) {
            helper.showToast('info', 'Missing Date Posted', 'Please choose the Date Posted before clicking the button.');
            return;
        }
        let lstROC = [];
        for(let i = 0 ; i < data.length ; i++) {
            lstROC.push(data[i].id);
        }
        let label = component.get('v.labelYP');
        let stage = 'Posted';
        /*if(label === 'Complete And Post') {
            stage = 'Completed';
        }*/

        component.set('v.Spinner', true);
        let action = component.get("c.CompleteAndPostROC");
        action.setParams({
            lstId: lstROC,
            datePosted: datePosted,
            stage: stage
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                helper.showToast('success', 'Successed', 'Successful.');
                component.set('v.isYPModel', false);
                component.set('v.datePosted', '');
                helper.getData(component);
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                helper.showToast('info', 'Info', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    completeROC : function(component, event, helper) {
        let data = component.find('rocrecord').getSelectedRows();
        if(data.length <= 0) {
            helper.showToast('info', 'Missing Record', 'Please choose a record before clicking the button.');
            return;
        }
        let lstROC = [];
        for(let i = 0 ; i < data.length ; i++) {
            lstROC.push(data[i].id);
        }

        component.set('v.Spinner', true);
        let action = component.get("c.markCompleteStage");
        action.setParams({
            lstId: lstROC
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                helper.showToast('success', 'Successed', 'Successful.');
                helper.getData(component);
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                helper.showToast('info', 'Info', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    movetonextAP : function(component, event, helper) {
        let data = component.find('rocrecord').getSelectedRows();
        if(data.length <= 0) {
            helper.showToast('info', 'Missing Record', 'Please choose a record before clicking the button.');
            return;
        }
        let lstROC = [];
        for(let i = 0 ; i < data.length ; i++) {
            lstROC.push(data[i].id);
        }

        component.set('v.Spinner', true);
        let action = component.get("c.MoveROCToNextAP");
        action.setParams({
            lstId: lstROC
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                helper.showToast('success', 'Successed', 'Successful.');
                helper.getData(component);
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                helper.showToast('error', 'Error', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    closeYPModel: function(component, event, helper) {
        component.set('v.isYPModel', false);
    },
    openPost: function(component, event, helper) {
       	helper.openModel(component, 'Post');
    },
    handleReOpen: function(component, event, helper) {
       	let data = component.find('rocrecord').getSelectedRows();
        if(data.length <= 0) {
            helper.showToast('info', 'Missing Record', 'Please choose a record before clicking the button.');
            return;
        }
        let lstROC = [];
        for(let i = 0 ; i < data.length ; i++) {
            lstROC.push(data[i].id);
        }

        component.set('v.Spinner', true);
        let action = component.get("c.reOpenROC");
        action.setParams({
            lstROC: lstROC
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                if(response.getReturnValue()) {
                    helper.showToast('info', 'Info', 'Cant reopen it as it has paid case action.');
                } else {
                    helper.showToast('success', 'Successed', 'Successful.');
                    helper.getData(component);
                }
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                helper.showToast('info', 'Info', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    filterROC: function(component, event, helper) {
        helper.getData(component);
    }
})