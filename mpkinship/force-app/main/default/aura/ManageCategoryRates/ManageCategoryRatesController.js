({
	doInit : function(component, event, helper) {
		//GET PICKLIST VALUES FOR STATUS ON LOAD
		var action =component.get("c.getCategoryRate");
        action.setParams({
            	recordId: component.get("v.recordId")            	
        });	
        action.setCallback(this, function(response){
        	console.log('***action');
            var state = response.getState();
            if (state === "SUCCESS") {
                var result=response.getReturnValue();
                if(result!=null){
                    component.set("v.SelectedStatus",result.StatusValue);
                    component.set("v.InitialStatus",result.StatusValue);
                    component.set("v.PriceValue",result.PriceValue);
                    component.set("v.InitialPrice",result.PriceValue);
                    
                    //CREATING STATUS OPTIONS
                    if(result.StatusNames !=undefined && result.StatusNames.length>0){
                        var items = [];
                       /* var item = {                           
                                "label": "---None---",
                                "value": ''
                        };
                        items.push(item); */
                        for (var i = 0; i < result.StatusNames.length; i++) {
                            var item = {                           
                                "label": result.StatusNames[i].label,
                                "value": result.StatusNames[i].value.toString()
                            };
                            items.push(item);
                        } 
                        component.set("v.StatusOptions",items);   
                    }    
                }
            }    
         });        
        $A.enqueueAction(action);   
                           
	},
    handleStatusChange : function(component,event,helper){
        var statusValue = component.get("v.SelectedStatus");
        var initialStatusValue = component.get("v.InitialStatus");
        var PriceValue = component.get("v.PriceValue");
        var initialPriceValue = component.get("v.InitialPrice");
        console.log('****'+statusValue);
        /***CHECK IF RATE HAS BEEN ACTIVE ***/
        if((statusValue=='Active'|| statusValue=='Inactive') && statusValue != initialStatusValue ){
            
            component.set("v.ShowCaseActions",true);
            helper.getCaseActions(component,event,helper);
            component.set("v.IsChanged",true);
        }
        /**DONT ALLOW STATUS CHANGE TO PENDING IF STATUS IS ACTIVE OR INACTIVE BEFORE **/
        if(statusValue=='Pending' && statusValue !=initialStatusValue){            
        	var statuscomp = component.find("Statusid");
            statuscomp.setCustomValidity("Status cannot be reverted back to pending.");
            statuscomp.reportValidity();
            component.set("v.ShowCaseActions",false);
            component.set("v.IsChanged",false);
            
        }
        /**TO REMOVE THE ERROR OF PENDING **/
        if(statusValue=='Active' || statusValue=='Inactive'){
        	/**CHECK IF CUSTOM VALIDITY EXISTS **/
            var statuscomp = component.find("Statusid");            
            	statuscomp.setCustomValidity('');
                statuscomp.reportValidity();                 
        }
        /** IF PENDING TO BACK TO ACTIVE AND PRICE IS DIFFERENT **/
        if(statusValue=='Active' && statusValue == initialStatusValue && PriceValue!=initialPriceValue){
        	component.set("v.ShowCaseActions",true);
            helper.getCaseActions(component,event,helper);
            component.set("v.IsChanged",true);    
        }
       
    },
    handlePriceChange : function(component,event,helper){
        var PriceValue = component.get("v.PriceValue");
        var initialPriceValue = component.get("v.InitialPrice");
        var statusValue = component.get("v.SelectedStatus");
        var initialStatusValue = component.get("v.InitialStatus");
        /***CHECK IF PRICE GETS CHANGED ***/
        if(PriceValue != initialPriceValue && statusValue!='Pending'){
        	component.set("v.ShowCaseActions",true);
            helper.getCaseActions(component,event,helper);    
            //DISPLAY SAVE BUTTON
            component.set("v.IsChanged",true);
        }
        /**FOR PENDING ONE,THEY CAN CHANGE THE PRICE DIRECTLY **/
        else if(PriceValue != initialPriceValue && statusValue=='Pending' && 
                statusValue == initialStatusValue){
            component.set("v.ShowCaseActions",false);
            component.set("v.IsChanged",true);
        }
    },
    handleSave : function(component,event,helper){
        var statusValue = component.get("v.SelectedStatus");
        var initialStatusValue = component.get("v.InitialStatus");
        var caseactionlist = component.get("v.CaseActionsList");
        var pricevalue = component.get("v.PriceValue");
        var initialPriceValue = component.get("v.InitialPrice");
        console.log('statusValue***%%'+statusValue);
        /**OPEN MODAL CODE **/
        if(statusValue=='Inactive' && statusValue!=initialStatusValue &&
          caseactionlist.length >0){
        	component.set("v.IsOpenModal",true);    
        } 
        else if(statusValue=='Inactive' && statusValue!=initialStatusValue && 
                caseactionlist.length == 0){
            /**CHANGES DONE TO CATEGORY RATE ONLY ***/
            helper.updateCategoryRate(component,event,helper);
        }
        /*** MAKE RATE ACTIVE OR CHANGE IN PRICE THEN UPDATE RELATED CASEACTIONS ***/
        else if((statusValue=='Active' && statusValue!=initialStatusValue) || 
                (statusValue=='Active') && pricevalue >0 && pricevalue!=initialPriceValue){
         	/**NOW CHECK IF CASE ACTIONS EXIST THEN CHANGES APPLIES TO CASE ACTION ***/
             if(caseactionlist.length >0)
             	helper.UpdateCaseActionsForActiveStatus(component,event,helper);    
            /***IF NOT THEN MAKE THE RATE AS ACTIVE ONLY ***/
            else     
             	helper.updateCategoryRate(component,event,helper);
        }
        /**UPDATE RATES FOR PENDING ***/
            else if(statusValue=='Pending' && pricevalue!=initialPriceValue){
            	helper.updateCategoryRate(component,event,helper);    
            }
    },
    submitDetails : function(component,event,helper){
        var getEnddate = component.get("v.EndDate");
        console.log('**VAl of enddate'+getEnddate);
        if(getEnddate !='' && getEnddate!=null){
            component.set("v.IsOpenModal",false);
            helper.showSpinner(component,event,helper);
            //CALL HELPER TO MAKE THE CHANGES TO CASE ACTIONS
            helper.InactiveStatusChanges(component,event,helper,getEnddate);
        }    
    },
    closeModal : function(component,event,helper){
        /**IF END DATE HAS ANY VALUE THEN ALSO MAKE IT BLANK ***/
        component.set("v.EndDate",'');
        component.set("v.IsOpenModal",false);
    }
})