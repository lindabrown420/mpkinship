({
	getCaseActions : function(component,event,helper) {
		var action= component.get("c.fetchCaseActions");
         action.setParams({
            	recordid: component.get("v.recordId")
        });	
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var result=response.getReturnValue();
                if(result!=null){
                	//CHECK IF ANY CASE ACTION EXIST 
                    if(result.caseactions.length>0){
                        component.set("v.CaseActionsList",result.caseactions);
                        console.log('**val fo fcaseactionslist'+ JSON.stringify(component.get("v.CaseActionsList")));
                    }  
                    else if(result.caseactions.length==0 && result.message=='No case Action Found'){
                        component.set("v.NoCaseActionFound",true);
                    }
                }    
            }    
        });
        $A.enqueueAction(action);   
	},
    
    InactiveStatusChanges : function(component,event,helper,getEnddate){
		var action= component.get("c.CaseActionChangesForInactiveStatus");
         action.setParams({
         	caseactions: JSON.stringify(component.get("v.CaseActionsList")),
            Enddate : component.get("v.EndDate"),
            recordid : component.get("v.recordId") 
        });	
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('***VAl of state'+state);
            var result=response.getReturnValue();
            console.log('**Val of result'+result);
            if (state === "SUCCESS") {
            	
                if(result!=null && result=='Updation Completed'){
                    helper.hideSpinner(component,event,helper);
                    //CLOSE THE QUICK ACTION
					$A.get("e.force:closeQuickAction").fire();
                    //FIRE A TOAST 
                    helper.showToast(component,event,helper,'Case action(s) has been updated successfully',
                                    'Success!','success');
                    $A.get('e.force:refreshView').fire();
                }
                else{
					 helper.hideSpinner(component,event,helper);                   
                    //CLOSE THE QUICK ACTION
                     helper.showToast(component,event,helper,'Case action(s) has not been updated successfully',
                                    'Error','Error');      
					$A.get("e.force:closeQuickAction").fire();
                    //FIRE A TOAST 
                                
                }
            }
            else{
                /**CHECK THIS LATER TOAST IS NOT WORKING **/
                console.log('***Failed');
                const errors = response.getError();
            	helper.hideSpinner(component,event,helper);
                //FIRE A TOAST 
                helper.showToast(component,event,helper,errors[0].message,
                                    'Error!','Error');  
                //CLOSE THE QUICK ACTION
			    $A.get("e.force:closeQuickAction").fire();
                            
            }
         });
        $A.enqueueAction(action);
                           
    },
    UpdateCaseActionsForActiveStatus : function(component,event,helper){
    	var action= component.get("c.CaseActionChangesForActiveStatus");
         action.setParams({
         	caseactions: JSON.stringify(component.get("v.CaseActionsList")),
             Price : component.get("v.PriceValue"),
            recordid : component.get("v.recordId") 
        });	
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('***VAl of state'+state);
            var result=response.getReturnValue();
            console.log('**Val of result'+result);
            if (state === "SUCCESS") {
            	
                if(result!=null && result=='Updation Completed'){
                    helper.hideSpinner(component,event,helper);
                    //CLOSE THE QUICK ACTION
					$A.get("e.force:closeQuickAction").fire();
                    //FIRE A TOAST 
                    helper.showToast(component,event,helper,'Case action(s) has been updated successfully',
                                    'Success!','success');
                    $A.get('e.force:refreshView').fire();
                }
                else{
					 helper.hideSpinner(component,event,helper);                   
                    //CLOSE THE QUICK ACTION
                     helper.showToast(component,event,helper,'Case action(s) has not been updated successfully',
                                    'Error','Error');      
					$A.get("e.force:closeQuickAction").fire();
                    //FIRE A TOAST 
                                
                }
            }
            else{
                /**CHECK THIS LATER TOAST IS NOT WORKING **/               
                const errors = response.getError();
            	helper.hideSpinner(component,event,helper);
                //FIRE A TOAST 
                helper.showToast(component,event,helper,errors[0].message,
                                    'Error!','Error');  
                //CLOSE THE QUICK ACTION
			    $A.get("e.force:closeQuickAction").fire();
                            
            }
         });
        $A.enqueueAction(action);    
    },
    
    updateCategoryRate: function(component,event,helper){
    	var action= component.get("c.UpdateCategoryRate");
        action.setParams({
            status : component.get("v.SelectedStatus"),            
            price : component.get("v.PriceValue"),
            recordid : component.get("v.recordId") 
        });	
        action.setCallback(this, function(response){
        	var state = response.getState();
            console.log('***VAl of state'+state);
            var result=response.getReturnValue();
            console.log('**Val of result'+result);
            if (state === "SUCCESS") {
            	
                if(result!=null && result=='Updation Completed'){
                    helper.hideSpinner(component,event,helper);
                    //CLOSE THE QUICK ACTION
					$A.get("e.force:closeQuickAction").fire();
                    //FIRE A TOAST 
                    helper.showToast(component,event,helper,'Category rate has been updated successfully',
                                    'Success!','success');
                    $A.get('e.force:refreshView').fire();
                }
                else{
					 helper.hideSpinner(component,event,helper);                   
                    //CLOSE THE QUICK ACTION
                     helper.showToast(component,event,helper,'Case action(s) has not been updated successfully',
                                    'Error','Error');      
					$A.get("e.force:closeQuickAction").fire();
                    //FIRE A TOAST 
                                
                }
            }
            else{
                /**CHECK THIS LATER TOAST IS NOT WORKING **/               
                const errors = response.getError();
                console.log('**Val of errors'+JSON.stringify(errors));
                console.log(errors[0].message);
            	helper.hideSpinner(component,event,helper);
                //FIRE A TOAST 
                helper.showToast(component,event,helper,errors[0].message,
                                    'Error!','Error');  
                //CLOSE THE QUICK ACTION
			    $A.get("e.force:closeQuickAction").fire();
                            
            }    
        });
        $A.enqueueAction(action);       
    },
    showToast : function(component, event, helper,msg,title,type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: title,
            message: msg,
            duration:' 5000',
            type: type,
            mode: 'pester'
        });
        toastEvent.fire();
	},
     showSpinner: function (component, event, helper) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },
     
    hideSpinner: function (component, event, helper) {
        var spinner = component.find("mySpinner");
        $A.util.addClass(spinner, "slds-hide");
    }
})