({
    showToast : function(type, title, message) {
        let strTitle = title.split('\n').join('');
        let strMessage = message.split('\n').join('');
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : strTitle,
            message: strMessage,
            duration:' 4000',
            key: 'info_alt',
            type: type, // success , error, info
            mode: 'pester'//'pester sticky'
        });
        toastEvent.fire();
    },
    getData : function(component) {
        component.set('v.Spinner', true);
        let data = component.get("v.poSelection");
        let lstPO = [];
        if(data) {
            data.forEach(element => {
                lstPO.push(element.id);
            });
        }
        data = component.get("v.paymentSelection");
        let lstPayment = [];
        if(data) {
            data.forEach(element => {
                lstPayment.push(element.id);
            });
        }
        let action = component.get("c.getOpportunitis");
        action.setParams({
            lstPO: lstPO,
            lstPayment: lstPayment
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let data = response.getReturnValue();
                if(data) {
                    component.set("v.data", data);
                }else{
                    this.showToast('info', 'Info', 'Please contact your Administrator for more details.');
                }
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                this.showToast('error', 'Error', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    }
})