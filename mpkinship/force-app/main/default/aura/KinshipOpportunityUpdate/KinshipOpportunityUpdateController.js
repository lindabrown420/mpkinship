({
    init : function(component, event, helper) {
        component.set('v.columns', [
            {label: 'Name', fieldName: 'rocurl', type: 'url', typeAttributes: { 
                label: {
                    fieldName: 'name'
                },
                target: '_blank'
            }},
            {label: 'Begin Date', fieldName: 'beginDate', type: 'date-local',sortable: true,
                typeAttributes:{
                    month: "2-digit",
                    day: "2-digit",
                    year:"numeric"
                }
            },
            {label: 'End Date', fieldName: 'endDate', type: 'date-local',sortable: true,
                typeAttributes:{
                    month: "2-digit",
                    day: "2-digit",
                    year:"numeric"
                }
            },
            {label: 'Amount', fieldName: 'amount', type: 'currency', typeAttributes: { currencyCode: 'USD' }},
            {label: 'Fiscal Year', fieldName: 'fyUrl', type: 'url',sortable: true, typeAttributes: { 
                label: {
                    fieldName: 'fiscalYear'
                },
                target: '_blank'
            }},
            {label: 'FIPS', fieldName: 'fipsurl', type: 'url', typeAttributes: { 
                label: {
                    fieldName: 'fipsname'
                },
                target: '_blank'
            }},
            {label: 'Stage', fieldName: 'stage', type: 'text'},
            {label: 'Created By', fieldName: 'createdBy', type: 'text'},
            {label: '',type: 'button', 
                typeAttributes: {
                    label: "Update FIPS",
                    name: "updateposo",
                    title: "Update FIPS",
                    value: { fieldName: "id" }
                }
            }
        ]);
    },
    searchData : function(component, event, helper) {
        helper.getData(component);
    },
    closeUpdateModel : function(component, event, helper) {
        component.set('v.isUpdate', false);
    },
    openUpdateModel : function(component, event, helper) {
        let data = component.find('opprecord').getSelectedRows();
        if(data.length <= 0) {
            helper.showToast('info', 'Info', 'Please select 1 record.');
            return;
        }
        component.set('v.selectedData', data);
        component.set('v.isUpdate', true);
    },
    updatePOSOInformation : function(component, event, helper) {
        let data = component.get("v.fipsSelection");
        let lstFIPS = [];
        if(data) {
            data.forEach(element => {
                lstFIPS.push(element.id);
            });
        }
        if(lstFIPS.length <= 0) {
            helper.showToast('info', 'Info', 'Missing FIPS.');
            return;
        }

        data = component.get("v.ccSelection");
        let lstCC = [];
        if(data) {
            data.forEach(element => {
                lstCC.push(element.id);
            });
        }
        if(lstCC.length <= 0) {
            helper.showToast('info', 'Info', 'Missing Cost Center.');
            return;
        }

        let lstPO = component.get('v.selectedData');
        for(let i = 0 ; i < lstPO.length ; i++) {
            if(lstPO[i].fipsid === lstFIPS[0]) {
                helper.showToast('info', 'Info', 'FIPS Code is same with Authorization FIPS.');
                return;
            }
        }

        component.set('v.Spinner', true);
        let action = component.get("c.updateFIPSonPOSO");
        action.setParams({
            fipsId: lstFIPS[0],
            ccId: lstCC[0],
            lstPO: component.get('v.selectedData')
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                helper.showToast('success', 'Info', 'Successfull.');
                component.set('v.isUpdate', false);
                helper.getData(component);
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                helper.showToast('info', 'Info', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    fipsLookupSearch : function(component, event, helper) {
        const lookupComponent = event.getSource();
        const serverSearchAction = component.get('c.kinshipSearchLookup');
        serverSearchAction.setParam('anOptionalParam', 'FIPS');
        lookupComponent.search(serverSearchAction);
    },
    clearFIPSErrorsOnChange: function(component, event, helper) {
        const selection = component.get('v.fipsSelection');
        const errors = component.get('v.fipsErrors');

        if (selection.length && errors.length) {
            component.set('v.fipsErrors', []);
        }
    },
    ccLookupSearch : function(component, event, helper) {
        const lookupComponent = event.getSource();
        const serverSearchAction = component.get('c.kinshipSearchLookup');
        serverSearchAction.setParam('anOptionalParam', 'CC');
        lookupComponent.search(serverSearchAction);
    },
    clearCCErrorsOnChange: function(component, event, helper) {
        const selection = component.get('v.ccSelection');
        const errors = component.get('v.ccErrors');

        if (selection.length && errors.length) {
            component.set('v.ccErrors', []);
        }
    },
    handleRowAction: function(component, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');

        switch (action.name) {
            case 'updateposo':
                let data = [];
                data.push(row);
                component.set('v.selectedData', data);
                component.set('v.isUpdate', true);
                break;
        }
    },
    poLookupSearch : function(component, event, helper) {
        const lookupComponent = event.getSource();
        const serverSearchAction = component.get('c.kinshipSearchLookup');
        serverSearchAction.setParam('anOptionalParam', 'PO');
        lookupComponent.search(serverSearchAction);
    },
    clearPOErrorsOnChange: function(component, event, helper) {
        const selection = component.get('v.poSelection');
        const errors = component.get('v.poErrors');

        if (selection.length && errors.length) {
            component.set('v.poErrors', []);
        }
    },
    paymentLookupSearch : function(component, event, helper) {
        const lookupComponent = event.getSource();
        const serverSearchAction = component.get('c.kinshipSearchLookup');
        serverSearchAction.setParam('anOptionalParam', 'Payment');
        lookupComponent.search(serverSearchAction);
    },
    clearPaymentErrorsOnChange: function(component, event, helper) {
        const selection = component.get('v.paymentSelection');
        const errors = component.get('v.paymentErrors');

        if (selection.length && errors.length) {
            component.set('v.paymentErrors', []);
        }
    }
})