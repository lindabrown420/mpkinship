({
    init : function(cmp, event, helper) {
        this.handleClick(cmp, event, helper)
    },
    handleClick: function(cmp, event, helper) {
        let navService = cmp.find("navService");
        let pageReference = {
            "type": "standard__component",
            "attributes": {
                "componentName": "c__KinshipPaymentSync"    
            },    
            "state": {
                "c__pid": cmp.get('v.recordId')  
            }
        };
        navService.navigate(pageReference);
        /*let defaultUrl = "#";
        navService.generateUrl(pageReference)
            .then($A.getCallback(function(url) {
                //cmp.set("v.url", url ? url : defaultUrl);
                navService.navigate(pageReference);
                window.open(url);

            }), $A.getCallback(function(error) {
                cmp.set("v.url", defaultUrl);
            }));*/
        //navService.navigate(pageReference);
    }
})