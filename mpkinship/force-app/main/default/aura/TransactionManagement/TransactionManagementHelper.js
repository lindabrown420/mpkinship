({
    showToast : function(type, title, message) {
        if(type.toUpperCase() == 'ERROR'){
            type = 'info';
        }

        let strTitle = title.split('\n').join('');
        let strMessage = message.split('\n').join('');
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : strTitle,
            message: strMessage,
            duration:' 4000',
            key: 'info_alt',
            type: type, // success , error, info
            mode: 'pester'//'sticky'
        });
        toastEvent.fire();
    },
    getUser : function(component) {
        component.set('v.Spinner', true);
        let action = component.get("c.initData");
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let data = response.getReturnValue();
                component.set("v.currentUser", data);
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                this.showToast('error', 'Error', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    getData : function(component) {
        component.set('v.Spinner', true);
        let action = component.get("c.getTransactionByAP");
        let data = component.get("v.campaignSelection");
        let lstCampaign = [];
        if(data) {
            data.forEach(element => {
                lstCampaign.push(element.id);
            });
        }
        action.setParams({
            lstAccPeriod: lstCampaign
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let data = response.getReturnValue();
                if(data) {
                    component.set("v.data", data);
                    if(data && data.length > 0 && data[0].approvalStatus === 'Approved'){
                        component.set("v.isApproved", true);
                    } else {
                        component.set("v.isApproved", false);
                    }
                }else{
                    this.showToast('error', 'Error', 'Please contact your Administrator for more details.');
                }
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                this.showToast('error', 'Error', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    updateData : function(component, data, status) {
        /*for(let i = 0 ; i < data.length ; i++) {
            if(data[i].status === 'Posted' || data[i].status === 'Closed' ) {
                helper.showToast('info', 'Can not update Posted or Closed Tran', 'Please Select The Record Without Blank Check Number.');
                isFalse = true;
                return;
            }
        }*/
        component.set('v.Spinner', true);
        let action = component.get("c.updateTransactionStatus");
        action.setParams({
            lstTJ: data,
            status: status
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                this.showToast('success', 'Successful!', 'Successful!');
                /*let oldData = component.get('v.data');
                let setData = new Set();
                for(let i = 0 ; i < data.length ; i++) {
                    setData.add(data[i]);
                }
                let newData = [];
                for(let i = 0 ; i < oldData.length ; i++) {
                    if(!setData.has(oldData[i].id)) {
                        newData.push(oldData[i]);
                    }
                }
                component.set('v.data', newData);*/
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                this.showToast('error', 'Error', strErr);
            }
            component.set('v.Spinner', false);
            //this.getData(component);
        });
        $A.enqueueAction(action);
    },
    convertArrayOfObjectsToCSV : function(component, data){
        let csvStringResult, counter, keys, columnDivider, lineDivider;
       
        if (data == null || !data.length) {
            return null;
        }
        columnDivider = ',';
        lineDivider =  '\n';
        csvStringResult = 'CostCode,FIPS,Fund,Account,ExpType,Program,Grant,Project,BudgetLine,Debit,Credit,Dff' + lineDivider;
 
        for(let i=0; i < data.length; i++){   
            csvStringResult += this.formatData(data[i].costCode) + columnDivider +
                                this.formatData(data[i].fipsCode) + columnDivider +
                                this.formatData(data[i].fund) + columnDivider +
                                this.formatData(data[i].accountCode) + columnDivider +
                                this.formatData(data[i].expType) + columnDivider +
                                this.formatData(data[i].programe) + columnDivider +
                                this.formatData(data[i].grant) + columnDivider +
                                this.formatData(data[i].project) + columnDivider +
                                this.formatData(data[i].budgetLine) + columnDivider +
                                this.formatData(data[i].debit) + columnDivider +
                                this.formatData(data[i].credit) + columnDivider +
                                this.getDFF(data[i]);
            csvStringResult += lineDivider;
        }
       
        return csvStringResult;        
    },
    getDFF: function(data){
        let dot = '.';
        if(data.dff === 'Fraud Fee') {
            return 'Fraud Fee' + dot + 
                    this.formatDate(data.warrantDate) + dot + 
                    'Non-ChargeBack' + dot + 
                    this.formatData(data.caseNumber) + dot + 
                    this.formatData(data.receiptNumber) + dot + 
                    this.formatData(data.clientID) + dot + 
                    this.formatData(data.clientName);
        } else if(data.dff === 'Current Month Expenditure') {
            return 'Current Month Expenditure' + dot + 
                    this.formatData(data.caseCount) + dot + 
                    this.formatData(data.childCount) + dot + 
                    this.formatData(data.adultCount) + dot + 
                    this.formatData(data.childinRes) + dot + 
                    this.formatData(data.recepientCount);
        } else if(data.dff === 'ChargeBacks') {
            return 'ChargeBacks' + dot + 
                    this.formatDate(data.warrantDate) + dot + 
                    'Chargeback - Agency Error' + dot + 
                    this.formatData(data.caseNumber) + dot + 
                    this.formatData(data.clientID) + dot + 
                    this.formatData(data.clientName);
        } else if(data.dff === 'Cash Refunds') {
            return 'Cash Refunds' + dot + 
                    this.formatDate(data.warrantDate) + dot + 
                    'Current Month Correction' + dot + 
                    this.formatData(data.caseNumber) + dot + 
                    this.formatData(data.receiptNumber) + dot + 
                    this.formatData(data.clientID) + dot + 
                    this.formatData(data.clientName);
        } else {
            return 'Adjustments' + dot + 
                    this.formatData(data.caseCount) + dot + 
                    this.formatData(data.childCount) + dot + 
                    this.formatData(data.adultCount) + dot + 
                    this.formatData(data.childinRes) + dot + 
                    this.formatData(data.recepientCount) + dot + 
                    this.formatDate(data.serviceDate) + dot +
                    'Current Month Adjustment' + dot +//this.formatData(data.adjustmentReason) + dot +
                    this.formatData(data.caseNumber) + dot +
                    this.formatData(data.clientID) + dot + 
                    this.formatData(data.clientName);
        }
    },
    formatDate: function(d) {
        if(d){
            if(typeof d !== 'date'){
                d = new Date(d);
            }
            let strDate = d.toLocaleDateString('en-US', {year: 'numeric', month: '2-digit', day: '2-digit'});
            let split = strDate.split('/');
            if(split.length === 3) {
            	return split[1] + '-' + this.getMonth(split[0]) + '-' + split[2];
            } else {
                return '';
            }
        }
        return '';
    },
    getMonth: function(str) {
        switch(str){
            case '01':return 'JAN';
            case '02':return 'FEB';
            case '03':return 'MAR';
            case '04':return 'APR';
            case '05':return 'MAY';
            case '06':return 'JUN';
            case '07':return 'JUL';
            case '08':return 'AUG';
            case '09':return 'SEP';
            case '10':return 'OCT';
            case '11':return 'NOV';
            case '12':return 'DEC';
        }
    },
    formatData: function(d) {
        if(d !== null && d != undefined){
            return d;
        }
        return '';
    },
    laserReport : function(component, action, popup) {
        /*let data = component.find('tjrecord').getSelectedRows();
        if(data.length <= 0) {
            this.showToast('info', 'Missing Record', 'Please select a Record before click The Button.');
            return;
        }*/
        let data = component.get("v.campaignSelection");
        let camId = '';
        if(!data || data.length == 0) {
            this.showToast('info', 'Missing Accounting Period', 'Please select an Accounting Period before clicking the button.');
            return;
        }
        camId = data[0].id;

        component.set('v.Spinner', true);
        action.setParams({
            apId: camId
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let value = response.getReturnValue();
                component.set('v.reportTJ', value);
                component.set('v.' + popup, true);
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                this.showToast('error', 'Error', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    laserExportReport : function(component, action, docId, deliveryId) {
        component.set('v.Spinner', true);
        action.setParams({
            data: component.get('v.reportTJ')
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let value = response.getReturnValue();
                if(value.Id) {
                    //let documentPackageId = 'a2718000006qwZNAAY';
                    //let deliveryOptionId = 'a2518000001WQGNAA4';
                    let recordIds = value.Id;
                    let sObjectType = 'LASER_Report_TJ__c';
                    let url = window.location.protocol + '//' + window.location.host + '/apex/ProcessDdp';
                    url = url + '?ddpId=' + docId;
                    url = url + '&deliveryOptionId=' + deliveryId;
                    url = url + '&ids=' + recordIds;
                    url = url + '&sObjectType=' + sObjectType;
                    window.open(url ,'_blank');
                } else {
                    this.showToast('error', 'Error', 'Cannot create the report. Please contact Admin for more details.');
                }
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                this.showToast('error', 'Error', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    sendEmailToLASER:function(component) {
        component.set('v.Spinner', true);
        let data = component.get('v.reportTJ');
        data.To_Address__c = component.get('v.strEmail');
        data.CC_Address__c = component.get('v.strCC');
        data.Subject__c = component.get('v.strSubject');
        data.Body__c = component.get('v.strBody');
        if(component.get('v.isCertification')) {
            data.Is_Certification__c = true;
        } else {
            data.Is_Reconciliation__c = true;
        }
        let action = component.get('c.insertTR');
        action.setParams({
            data: data
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                this.showToast('success', 'Success', 'Sent Email Successfully.');
                component.set('v.isEmail', false);
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                this.showToast('error', 'Error', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    fotmatString: function(data){
        if(!data) {
            return '';
        }
        return data;
    },
    parseDecimal: function(d){
        if(d) {
            return parseFloat(d);
        }
        return 0;
    },
    isPostAP : function(component) {
        let data = component.get("v.campaignSelection");
        if(!data || data.length == 0) {
            return;
        }
        let lstId = [];
        lstId.push(data[0].id);
        if(lstId.length > 0){
            component.set('v.Spinner', true);
            let action = component.get("c.checkPostAP");
            action.setParams({
                lstId: lstId
            });
            action.setCallback(this, function(response) {
                let state = response.getState();
                if (state === "SUCCESS") {
                    let data = response.getReturnValue();;
                    if(data) {
                        component.set('v.isShowPAP', true);
                    } else {
                        component.set('v.isShowPAP', false);
                    }
                } else {
                    const errors = response.getError();
                    let strErr = '';
                    if(errors) {
                        if(errors[0] && errors[0].message){
                            strErr = errors[0].message;
                        }
                    }
                    helper.showToast('error', 'Error', strErr);
                }
                component.set('v.Spinner', false);
            });
            $A.enqueueAction(action);
        }
    },
    getHeaderText : function(component, ap) {
        let headerText = {'fips': '', 'startDate':'', 'endDate':'', 'yearEnd':''};
        let title = ap;
        let split = title.split('-');
        headerText.fips = split[4].trim().toUpperCase();
        headerText.startDate = split[2].trim() + '/01/' + split[1].trim();
        headerText.endDate = split[2].trim() + '/' + split[3].trim() + '/' + split[1].trim();
        let q = this.getQuaterOnly(title);
        headerText.yearEnd = '05/31/' + (parseInt(split[1].trim()) + ((q==='1' || q === '2')?1:0));
        return headerText;
    },
    formatNumber: function(d) {
        if(d){
            if(typeof d !== 'number'){
                d = parseFloat(d);
            }
            if(d < 0) d = -d;
            return d;
        }
        return 0;
    },
    getQuaterOnly: function(title) {
        let lst = title.split('-');
        if(lst.length == 5) {
            let d = parseInt(lst[2]);
            switch(d){
                case 1:return "3";
                case 2:return "3";
                case 3:return "3";
                case 4:return "4";
                case 5:return "4";
                case 6:return "4";
                case 7:return "1";
                case 8:return "1";
                case 9:return "1";
                case 10:return "2";
                case 11:return "2";
                case 12:return "2";
            }
        }
        return null;
    },
    getNumber: function(d) {
        if(d) {
            return d;
        }
        return 0;
    },
     getTotalData:function(str) {
        if(str) {
            return '"' + str + '"'
        }
        return '""';
    }
})