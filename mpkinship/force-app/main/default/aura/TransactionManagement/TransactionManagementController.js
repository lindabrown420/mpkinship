({
    init : function(component, event, helper) {
        let pageRef = component.get("v.pageReference");
        if(pageRef && pageRef.state && pageRef.state.c__apid) {
            component.set('v.apid', pageRef.state.c__apid);
            // get default apid
            let action = component.get("c.getDefaultAP");
            action.setParams({
                strId: pageRef.state.c__apid
            });
            action.setCallback(this, function(response) {
                let state = response.getState();
                if (state === "SUCCESS") {
                    let data = response.getReturnValue();
                    if(data) {
                        let campaignSelection = component.get("v.campaignSelection");
                        campaignSelection = [];
                        campaignSelection.push(data);
                        component.set('v.campaignSelection', campaignSelection);
                        helper.getData(component);
                    }
                }
            });
            $A.enqueueAction(action);
        }
        component.set('v.columns', [
            {label: 'Name', fieldName: 'tjUrl', type: 'url', typeAttributes: { 
                label: {
                    fieldName: 'name'
                },
                target: '_blank'
            }},
            {label: 'Type', fieldName: 'type', type: 'text'},
            /*{label: 'Parent Campaign', fieldName: 'parentCamUrl', type: 'url', typeAttributes: { 
                label: {
                    fieldName: 'parentCam'
                },
                target: '_blank'
            }},
            {label: 'Child Campaign', fieldName: 'childCamUrl', type: 'url', typeAttributes: { 
                label: {
                    fieldName: 'childCam'
                },
                target: '_blank'
            }},*/
            {label: 'Receipt', fieldName: 'receiptUrl', type: 'url', typeAttributes: { 
                label: {
                    fieldName: 'receiptName'
                },
                target: '_blank'
            }},
            {label: 'Payment', fieldName: 'paymentUrl', type: 'url', typeAttributes: { 
                label: {
                    fieldName: 'paymentName'
                },
                target: '_blank'
            }},
            {label: 'Status', fieldName: 'status', type: 'text'},
            {label: 'LASER Account Code', fieldName: 'laserAccountCode', type: 'text'},
            {label: 'LASER Cost Code', fieldName: 'laserCodeCode', type: 'text'},
            {label: 'FIPS Code', fieldName: 'fipsCode', type: 'text'},
            {label: 'Debit', fieldName: 'debit', type: 'currency', typeAttributes: { currencyCode: 'USD' }},
            {label: 'Credit', fieldName: 'credit', type: 'currency', typeAttributes: { currencyCode: 'USD' }},
            {label: 'Amount', fieldName: 'amount', type: 'currency', typeAttributes: { currencyCode: 'USD' }},
            {label: 'Description', fieldName: 'description', type: 'text'}
        ]);
        helper.getUser(component);
    },
    seachTransaction : function(component, event, helper) {
        let data = component.get("v.campaignSelection");
        if(!data || data.length == 0) {
            helper.showToast('info', 'Missing Accounting Period', 'Please select an Accounting Period before clicking the button.');
            return;
        }
        helper.getData(component);
    },
    updateAll : function(component, event, helper) {
        let status = component.get('v.strStatus');
        if(!status) {
            helper.showToast('info', 'Missing Status', 'Please choose Status before clicking the "Update" button.');
            return;
        }
        let data = component.get('v.data');
        let lstUpdate = [];
        for(let i = 0 ; i < data.length ; i++) {
            if(data[i].status !== 'Open'){
                helper.showToast('info', 'Cannot update Posted/Closed status', 'Please choose a Transaction Record with "Open" Status.');
                return;
            }
            lstUpdate.push(data[i].id);
        }
        helper.updateData(component, lstUpdate, status);
    },
    updateSelected : function(component, event, helper) {
        let status = component.get('v.strStatus');
        if(!status) {
            helper.showToast('info', 'Missing Status', 'Please choose Status before clicking the "Update" button.');
            return;
        }
        let data = component.find('tjrecord').getSelectedRows();
        if(data.length <= 0) {
            helper.showToast('info', 'Missing Record', 'Please choose a Transaction Record before clicking the "Update" button.');
            return;
        }
        let lstUpdate = [];
        for(let i = 0 ; i < data.length ; i++) {
            if(data[i].status !== 'Open'){
                helper.showToast('info', 'Cannot update Posted/Closed Status', 'Please choose a Transaction Record with "Open" Status.');
                return;
            }
            lstUpdate.push(data[i].id);
        }
        helper.updateData(component, lstUpdate, status);
    },
    campaignLookupSearch : function(component, event, helper) {
        const lookupComponent = event.getSource();
        const serverSearchAction = component.get('c.kinshipSearchLookup');
        serverSearchAction.setParam('anOptionalParam', 'Campaign');
        lookupComponent.search(serverSearchAction);
    },
    clearCampaignErrorsOnChange: function(component, event, helper) {
        const selection = component.get('v.campaignSelection');
        const errors = component.get('v.campaignErrors');
        let formData = event.getParam("formData");
        if(formData && formData.subtitle) {
            let status = formData.subtitle.split('-');
            if(status.length == 2) {
                let cert = status[0].split(':');
                if(cert[1] === 'Approved'){
                    component.set('v.isCertApproved', true);
                } else {
                    component.set('v.isCertApproved', false);
                }
                if(cert[0] === 'Submitted For Approval'){
                    component.set('v.isShowCert', false);
                } else {
                    component.set('v.isShowCert', true);
                }
                let recon = status[1].split(':');
                if(recon[1] === 'Approved'){
                    component.set('v.isReconApproved', true);
                } else {
                    component.set('v.isReconApproved', false);
                }
                if(recon[1] === 'Submitted For Approval'){
                    component.set('v.isShowRecon', false);
                } else {
                    component.set('v.isShowRecon', true);
                }
            }
        }
        if(formData && formData.recordId) {
            helper.isPostAP(component);
        }
        if (selection.length && errors.length) {
            component.set('v.campaignErrors', []);
        }
    },
    laserExport: function(component, event, helper) {
        let data = component.get("v.campaignSelection");
        let camId = '';
        if(!data || data.length == 0) {
            helper.showToast('info', 'Missing Accounting Period', 'Please select an Accounting Period before clicking the button.');
            return;
        }
        camId = data[0].id;

        component.set('v.Spinner', true);
        let action = component.get('c.getExportData');
        action.setParams({
            apId: camId
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let data = response.getReturnValue();
                let csv = helper.convertArrayOfObjectsToCSV(component, data);   
                if (csv !== null){
                    var hiddenElement = document.createElement('a');
                    hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
                    hiddenElement.target = '_self';
                    hiddenElement.download = 'LASER Export.csv';
                    document.body.appendChild(hiddenElement);
                    hiddenElement.click();
                }
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                helper.showToast('error', 'Error', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    laserCertification: function(component, event, helper) {
        helper.laserExportReport(component, component.get("c.insertTR"), $A.get('$Label.c.KINSHIP_CERT_DDP'), $A.get('$Label.c.KINSHIP_CERT_OPTION'));
    },
    openlaserCertificationModel:function(component, event, helper) {
        helper.laserReport(component, component.get("c.createLASERCertificationReport"), 'islaserCertification');
    },
    closelaserCertificationModel:function(component, event, helper) {
        component.set('v.islaserCertification', false);
    },
    laserReconciliation: function(component, event, helper) {
        helper.laserExportReport(component, component.get("c.insertTR"), $A.get('$Label.c.KINSHIP_RECON_DDP'), $A.get('$Label.c.KINSHIP_RECON_OPTION'));
    },
    openlaserReconciliationModel:function(component, event, helper) {
        helper.laserReport(component, component.get("c.createLASERReconciliationReport"), 'islaserReconciliation');
    },
    closelaserReconciliationModel:function(component, event, helper) {
        component.set('v.islaserReconciliation', false);
    },
    openEmailPopupCertification:function(component, event, helper) {
        component.set('v.isEmail', true);
        component.set('v.isCertification', true);
        component.set('v.strSubject', 'FIPS 995 Nov-21Certification');
        let currentUser = component.get('v.currentUser');
        let strUser = currentUser?(helper.fotmatString(currentUser.Name) + ',' + helper.fotmatString(currentUser.Title)) : '';
        component.set('v.strBody', 'The LASER report is attached. ' + strUser);
    },
    openEmailPopupReconciliation:function(component, event, helper) {
        component.set('v.isEmail', true);
        component.set('v.isCertification', false);
        component.set('v.strSubject', 'FIPS 995 Nov-21 Reconciliation');
        let currentUser = component.get('v.currentUser');
        let strUser = currentUser?(helper.fotmatString(currentUser.Name) + ',' + helper.fotmatString(currentUser.Title)) : '';
        component.set('v.strBody', 'The LASER report is attached. ' + strUser);
    },
    closeEmailPopup:function(component, event, helper) {
        component.set('v.isEmail', false);
    },
    sendEmailToLASER:function(component, event, helper) {
        helper.sendEmailToLASER(component);
    },
    userLookupSearch : function(component, event, helper) {
        const lookupComponent = event.getSource();
        const serverSearchAction = component.get('c.kinshipSearchLookup');
        serverSearchAction.setParam('anOptionalParam', 'User');
        lookupComponent.search(serverSearchAction);
    },
    clearUserErrorsOnChange: function(component, event, helper) {
        const selection = component.get('v.userSelection');
        const errors = component.get('v.userErrors');

        if (selection.length && errors.length) {
            component.set('v.userErrors', []);
        }
    },
    submitForApproval: function(component, event, helper) {
        let userId = $A.get("$SObjectType.CurrentUser.Id");
        let userSelection = component.get("v.userSelection");
        
        if(!userSelection || userSelection.length == 0) {
            helper.showToast('info', 'Missing Approver', 'Please choose the Approver before clicking the "Request For Approval" button.');
            return;
        }
        let approvalUser = userSelection[0].id;
        if(approvalUser === userId){
            //helper.showToast('info', 'Wrong Approver', 'You cannot approve the Invoice requested by you.');
            //return;
        }
        
        let data = component.get("v.campaignSelection");
        let camId = '';
        if(!data || data.length == 0) {
            helper.showToast('info', 'Missing Accounting Period', 'Please select an Accounting Period before clicking the button.');
            return;
        }
        camId = data[0].id;

        let type = 'LASER Certification';
        if(component.get('v.islaserReconciliation')){
            type = 'LASER Reconciliation';
        }
        let reportTJ = component.get('v.reportTJ');
        
        component.set('v.Spinner', true);
        let action = component.get("c.submitCampaignForApproval");
        action.setParams({
            camp: camId,
            userId: approvalUser,
            type: type,
            reportTJ: reportTJ
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let message = 'You have submitted for Approval successfully. The Approver will get a notification shortly';
                helper.showToast('success', 'Success!', message);
                component.set('v.isApprovePopup', false);
                if(type === 'LASER Certification') {
                    component.set('v.isShowCert', false);
                } else {
                    component.set('v.isShowRecon', false);
                }
                component.set('v.userSelection', []);
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                helper.showToast('error', 'Error', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    closeApproverModel: function(component, event, helper) {
        component.set('v.isApprovePopup', false);
    },
    openApproverModel: function(component, event, helper) {
        component.set('v.isApprovePopup', true);
    },
    handleTotalChanged: function(component, event, helper) {
        let reportTJ = component.get('v.reportTJ');
        let total = helper.parseDecimal(reportTJ.AT_Total_Staff_Exp__c) +
                    helper.parseDecimal(reportTJ.AT_Total_OE__c) +
                    helper.parseDecimal(reportTJ.AT_Total_MAP__c) +
                    helper.parseDecimal(reportTJ.AT_Total_POSE__c) +
                    helper.parseDecimal(reportTJ.AT_Total_Other__c)
        component.set('v.sumTotal', total);
    },
    gotoReportFolder: function(component, event, helper) {
        window.open($A.get('$Label.c.KINSHIP_LASER_REPORT_FOLDER'), '_blank');
    },
    autoPostAP: function(component, event, helper) {
        let data = component.get("v.campaignSelection");
        if(!data || data.length == 0) {
            return;
        }
        let lstId = [];
        lstId.push(data[0].id);
        component.set('v.Spinner', true);
        let action = component.get("c.postedAP");
        action.setParams({
            lstId: lstId
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                helper.showToast('success', 'Success!', 'The LASER Accounting Period has been successfully posted.');
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                helper.showToast('error', 'Error', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    openSummary:function(component, event, helper) {
        let data = component.get("v.campaignSelection");
        if(!data || data.length == 0) {
            helper.showToast('info', 'Info', 'Please select an Accounting Period.');
            return;
        }
        let lstId = [];
        lstId.push(data[0].id);
        component.set('v.Spinner', true);
        let action = component.get("c.getLASERReportData");
        action.setParams({
            lstAccPeriod: lstId
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let data = response.getReturnValue();
                if (data.length > 0) {
                    let sumTotalCodeGross = [];
                    let sumTotalCodeRA = [];
                    // sort
                    let lstSummary = [];
                    let strYear = '';
                    let strCreditRT = 'Receipt,Special Welfare Reimbursement,Vendor Refund';
                    for(let i = 0 ; i < data.length ; i++) {
                        if(strYear.indexOf(data[i].payment.Category_lookup__r.Locality_Account__r.Name) !== -1) {
                            let isNew;
                            for(let k = 0 ; k < lstSummary.length ; k++) {
                                if(data[i].payment.Category_lookup__r.Locality_Account__r.Name === lstSummary[k].code) {
                                    if(strCreditRT.indexOf(data[i].payment.RecordType.Name) !== -1) {
                                        data[i].payment.Payment_Amount__c = -data[i].payment.Payment_Amount__c;
                                    }
                                    lstSummary[k].lstPayment.push(data[i]);
                                }
                            }
                        } else {
                            let sumData = [];
                            if(strCreditRT.indexOf(data[i].payment.RecordType.Name) !== -1) {
                                data[i].payment.Payment_Amount__c = -data[i].payment.Payment_Amount__c;
                            }
                            sumData.push(data[i]);
                            lstSummary.push({'code': data[i].payment.Category_lookup__r.Locality_Account__r.Name, 'lac': data[i].payment.Category_lookup__r.Locality_Account__r.Name + ' ' + data[i].payment.Category_lookup__r.Locality_Account__r.Account_Title__c, 
                                             'lstPayment': sumData, 'total':0});
                            strYear += ',' + data[i].payment.Category_lookup__r.Locality_Account__r.Name;
                        }
                    }
                    let sum = 0;
                    for(let k = 0 ; k < lstSummary.length ; k++) {
                        sum = 0;
                        for(let i = 0 ; i < lstSummary[k].lstPayment.length ; i++) {
                            sum += helper.getNumber(lstSummary[k].lstPayment[i].payment.Payment_Amount__c);
                        }
                        lstSummary[k].total = sum;
                    }
                    component.set('v.lstSummary', lstSummary);
                    component.set('v.isShowTotal', true);
                    component.set('v.headerText', helper.getHeaderText(component, data[0].payment.Accounting_Period__r.Name));
                } else {
                    helper.showToast('info', 'No Data.', 'There is no record for this Accounting Period.');
                }
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                helper.showToast('info', 'Info', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    closeSumPopup: function(component, event, helper) {
        component.set('v.isShowTotal', false);
    },
    summaryExportToCSV: function(component, event, helper) {
        let data = component.get("v.lstSummary");
        if (data.length > 0) {
            let csvStringResult, counter, keys, columnDivider, lineDivider;
            columnDivider = ',';
            lineDivider =  '\n';

            let headerText = component.get('v.headerText');
            csvStringResult = '';
            csvStringResult += ',,"' + headerText.fips + 'COUNTY"' + lineDivider;
            csvStringResult += ',,"FROM' + headerText.startDate + ' TO ' + headerText.endDate + '"' + lineDivider;
            csvStringResult += ',,"FISCAL YEAR ENDING ' + headerText.yearEnd + '"' + lineDivider + lineDivider;
            
            for(let k = 0 ; k < data.length ; k++) {
                csvStringResult += data[k].lac + lineDivider;
                let headers = ['Payment Number', 'Payment Date', 'Payment Amount', 'Vendor', 'Case', 'Description', 'Stage', 'Posted Status', 'Transaction Journal', 'Laser Cost Code', 'Laser Account Code'];
                csvStringResult += headers.join(columnDivider);
                csvStringResult += lineDivider;
                
                for(let i=0; i < data[k].lstPayment.length; i++){  
                    counter = 0;
                    
                    csvStringResult += (data[k].lstPayment[i].payment?helper.getTotalData(data[k].lstPayment[i].payment.Name):'') + columnDivider;
                    csvStringResult += (data[k].lstPayment[i].payment?helper.getTotalData(data[k].lstPayment[i].payment.Payment_Date__c):'') + columnDivider;
                    csvStringResult += (data[k].lstPayment[i].payment?helper.getTotalData(data[k].lstPayment[i].payment.Payment_Amount__c):'') + columnDivider;
                    csvStringResult += ((data[k].lstPayment[i].payment && data[k].lstPayment[i].payment.Vendor__r)?helper.getTotalData(data[k].lstPayment[i].payment.Vendor__r.Name):'') + columnDivider;
                    csvStringResult += ((data[k].lstPayment[i].payment && data[k].lstPayment[i].payment.Kinship_Case__r)?helper.getTotalData(data[k].lstPayment[i].payment.Kinship_Case__r.Name):'') + columnDivider;
                    csvStringResult += (data[k].lstPayment[i].payment?helper.getTotalData(data[k].lstPayment[i].payment.Description__c):'') + columnDivider;
                    csvStringResult += (data[k].lstPayment[i].payment?helper.getTotalData(data[k].lstPayment[i].payment.Invoice_Stage__c):'') + columnDivider;
                    csvStringResult += (data[k].lstPayment[i].payment?helper.getTotalData(data[k].lstPayment[i].payment.Invoices_Status__c):'') + columnDivider;
                    csvStringResult += (data[k].lstPayment[i].tj?helper.getTotalData(data[k].lstPayment[i].tj.Name):'') + columnDivider;
                    csvStringResult += ((data[k].lstPayment[i].payment && data[k].lstPayment[i].payment.Category_lookup__r && data[k].lstPayment[i].payment.Category_lookup__r.LASER_Cost_Code__r)?helper.getTotalData(data[k].lstPayment[i].payment.Category_lookup__r.LASER_Cost_Code__r.Name):'') + columnDivider;
                    csvStringResult += ((data[k].lstPayment[i].payment && data[k].lstPayment[i].payment.Category_lookup__r && data[k].lstPayment[i].payment.Category_lookup__r.LASER_Account_Code__r)?helper.getTotalData(data[k].lstPayment[i].payment.Category_lookup__r.LASER_Account_Code__r.Name):'') + columnDivider;
                    csvStringResult += lineDivider;
                }
                csvStringResult += lineDivider;
            }

            let hiddenElement = document.createElement('a');
            hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURIComponent(csvStringResult);
            hiddenElement.target = '_self';
            hiddenElement.download = 'LocalityAccountCodeReport.csv';//this.getName(component, data[0], true);
            document.body.appendChild(hiddenElement);
            hiddenElement.click();
        } else {
            helper.showToast('info', 'No Data.', 'There is no record for this Accounting Period.');
        }
    }
})