({

	searchHelper : function(component,event,getInputkeyWord) {
        console.log(getInputkeyWord);
        console.log('***entrerdin searchHelper');
	  // call the apex class method           
        var action = component.get("c.fetchLookUpValuesForCaseAction");
        // set param to method  
        action.setParams({
            'searchKeyWord': getInputkeyWord,
            'ObjectName' : component.get("v.objectAPIName"),
            'recordtype' : component.get("v.recordtype"),
            'AdditionalCriteria' : component.get("v.AdditionalCriteria"),
            'AdditionalListCriteria' : component.get("v.AdditionalListCriteria")
        });
        // set a callBack    
        action.setCallback(this, function(response) {
            $A.util.removeClass(component.find("mySpinner"), "slds-show");
            var state = response.getState();
            console.log('******'+state);
            console.log(response.getReturnValue());
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                //console.log('**VAl of storeResponse'+storeResponse);
                // if storeResponse size is equal 0 ,display No Result Found... message on screen.                }
                if (storeResponse.length == 0) {
                    component.set("v.Message", 'No results found...');
                } else {
                    component.set("v.Message", '');
                }
                // set searchResult list with return value from server.
                component.set("v.listOfSearchRecords", storeResponse);
            }
            else{
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                console.log('error value'+strErr);
            }
            
        });
        // enqueue the Action  
        $A.enqueueAction(action);
      
	},
	 InitialselectRecord : function(component, event, helper) {
    // get the selected Account record from the COMPONETN event 	 
      /* var selectedAccountGetFromEvent = event.getParam("recordByEvent");
	   component.set("v.selectedRecord" , selectedAccountGetFromEvent); */    
        console.log('**Entred in selectrecord1');
        var forclose = component.find("lookup-pill");
           $A.util.addClass(forclose, 'slds-show');
           $A.util.removeClass(forclose, 'slds-hide');
  
        var forclose = component.find("searchRes");
           $A.util.addClass(forclose, 'slds-is-close');
           $A.util.removeClass(forclose, 'slds-is-open');
        
        var lookUpTarget = component.find("lookupField");
            $A.util.addClass(lookUpTarget, 'slds-hide');
            $A.util.removeClass(lookUpTarget, 'slds-show');  
      
	}
})