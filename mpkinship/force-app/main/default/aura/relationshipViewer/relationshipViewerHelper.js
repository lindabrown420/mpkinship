({
	showRelationshipsMethod : function(cmp, event, helper){
        console.log('showRelationshipsMethod');
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:relationshipViewer",
            componentAttributes: {
                recordId: cmp.get("v.recordId"),
                showRelationships: true
            }
        });
        evt.fire();
    }
})