({
	doInit : function(cmp, event, helper) {
		console.log('do Init');
        console.log('record Id: '+cmp.get("v.recordId"));
        console.log(cmp.get("v.showRelationships"));
        var action = cmp.get("c.getContactWithRelationship");
        action.setParams({
            contId: cmp.get("v.recordId")
        })
        action.setCallback(this,function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                cmp.set("v.contList",response.getReturnValue());
                //helper.showRelationshipsMethod(cmp, event, helper);
            }
        });
        $A.enqueueAction(action);
	},
    
    relatedContClicked : function(cmp, event, helper) {
        console.log('relatedContClicked');
        console.log(event.target.dataset.id);
        console.log(event.target.dataset.index);
        
        /*if(cmp.get("v.showRelatedRelationship"))
            cmp.set("v.showRelatedRelationship",false);
        else*/
        	cmp.set("v.showRelatedRelationship",true);
        
        cmp.set("v.currentIndex",parseInt(event.target.dataset.index));
        
        console.log(cmp.get("v.showRelatedRelationship"));
        console.log(cmp.get("v.currentIndex"));
        
        var action = cmp.get("c.getContactWithRelationship");
        action.setParams({
            contId: event.target.dataset.id
        })
        action.setCallback(this,function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());                
                cmp.set("v.relatedContList",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    
    onContactClick : function(cmp, event, helper){
        console.log('onContactClick');
        window.open('/'+event.target.dataset.id);
    
	},
    
    createRecord : function (cmp, event, helper) {
    var createRecordEvent = $A.get("e.force:createRecord");
    createRecordEvent.setParams({
        "entityApiName": "Relationship__c",
        "defaultFieldValues": {
            'Contact__c' : cmp.get("v.recordId")                            
        }
    });
    createRecordEvent.fire();
}

    
})