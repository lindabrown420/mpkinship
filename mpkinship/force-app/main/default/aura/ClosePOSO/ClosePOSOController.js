({	
    handleLoad : function(component,event,helper){
    	var endDate = component.find("endDateId").get("v.value");
        console.log('**Val of endDate'+endDate);
        if(endDate!=undefined && endDate!='' && endDate!=null){
            component.set("v.OldEndDate",endDate);
        }
    },
    EndDatechange :function(component,event,helper){
    	var newEndDate=component.find("endDateId").get("v.value");  
        console.log('**VAl of newenddate'+newEndDate);
        var oldEndDate=component.get("v.OldEndDate");
        console.log('***Val of oldendate'+ oldEndDate);
        var beginDate=component.find("beginDate").get("v.value");
        if(newEndDate > oldEndDate){
            let button = component.find('ButtonId');
    		button.set('v.disabled',true);		
            helper.showToast('Warning','End Date Wrong','To terminate this POSO, enter a date earlier than the current end date');
        }
        if(newEndDate<=oldEndDate){
            let button = component.find('ButtonId');
   			button.set('v.disabled',false);
            
        }
        if(newEndDate < beginDate){
        	let button = component.find('ButtonId');
   			button.set('v.disabled',true);    
            helper.showToast('Warning','End Date Wrong','To terminate this POSO, enter a date greater than or equal to Begin date ');
        }
    },
	save : function(component, event, helper) {
        component.set("v.ShowSpinner",true);
       // var await = component.find("awaitId").get("v.value");
		var reason = component.find("reasonId").get("v.value");
        var endDate =component.find("endDateId").get("v.value");
        console.log('**VAl of reason'+reason);
        console.log('**VAl of enddate'+endDate);
        if(reason=='' || reason==undefined || reason==null){
            component.set("v.ShowSpinner",false);
            helper.showToast('Error','error','Please provide reason.');  
        }
        else{
            var action=component.get("c.updatePO");
             action.setParams({ 
                oppId : component.get("v.recordId"),
             	newEndDate:endDate,
             	statusRsn:reason,
                typeOfPO :'POSO' 
        	});	
            action.setCallback(this,function(response){           
                var state = response.getState();
                console.log('**State val'+state);            
                console.log('**REsponse return val'+JSON.stringify(response.getReturnValue()));
                if(state==="SUCCESS"){
                    component.set("v.ShowSpinner",false);
                    var result=response.getReturnValue();  
                    if(result.msg=='Record updated'){
                        helper.showToast('Success','dismissible','Case Action will be successfully closed on the provided end date.');
                        //var today = new Date().toISOString().slice(0, 10);
                       /* if(endDate <=today && await==false)
                       		helper.showToast('Success','Success','POSO has been successfully closed.');
                       
                        else if(await==true)
                            helper.showToast('Success','Success','POSO will be closed after final payment is done or writeoff.');*/
                       
                    	helper.navigatetourl(component,event,helper);
                    }
                }
                else if(state==="ERROR"){
                    component.set("v.ShowSpinner",false);
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                     errors[0].message);
                            helper.showToast('Error','error',errors[0].message);  
                            $A.get("e.force:closeQuickAction").fire();
                        }
                    } else {
                        console.log("Unknown error");
                	}
                }    
             });
        	$A.enqueueAction(action);      
        }
	},
    cancel : function(component,event,helper){
         $A.get("e.force:closeQuickAction").fire();
    }
})