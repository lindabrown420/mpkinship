({
    init: function (component, event, helper) {
        console.log('init');
        var action = component.get("c.showStartButtonMethod");
        action.setParams({
            recId: component.get("v.recordId")
        })
        action.setCallback(this, function (response){
            var state = response.getState();
            console.log('showStartButtonMethod: '+state);
            if (state === "SUCCESS") {
                var resData = response.getReturnValue();
                console.log('init: '+resData);
                if(resData == true){
                    component.set("v.showStartButton",true);
                    helper.initCont(component, event, helper);
                }
                else{
                    helper.totalPointsHelper(component,event,helper);
                }
            }
        });
        $A.enqueueAction(action);
        
    },
    
    OpenModal: function (cmp)
    {
        cmp.set("v.isModalOpen", true);
    },
    CloseModel: function (cmp,event,helper)
    {
        cmp.set("v.isModalOpen", false);
        cmp.set("v.isTotalPointsModalOpen", false);
    },
    Close_SaveModel: function (cmp,event,helper)
    {
        helper.handleSave(cmp, event,helper,'true');        
    },
    handleChange: function (cmp, event) {
        // This will contain the string of the "value" attribute of the selected option
        console.log('handleChange');
        var selectedOptionValue = event.getParam("value");
        var fieldName = event.getSource().get("v.name");
        fieldName = fieldName.split("-");
        console.log(fieldName);
        var assessmentData = cmp.get("v.assessmentDataToIterate");
		console.log(JSON.stringify(assessmentData));
        if (selectedOptionValue == 'yes') {
            if (fieldName[1] == 'm') {
                assessmentData[fieldName[0]].data.Minimal[fieldName[2]].score = assessmentData[fieldName[0]].data.Minimal[fieldName[2]].Points__c;
                assessmentData[fieldName[0]].data.Minimal[fieldName[2]].isScore = true;
                
                if(assessmentData[fieldName[0]].data.Moderate != undefined){
                    assessmentData[fieldName[0]].data.Moderate[fieldName[2]].score = 0;
                    assessmentData[fieldName[0]].data.Moderate[fieldName[2]].isScore = false;
                    assessmentData[fieldName[0]].data.Moderate[fieldName[2]].applyScore = 'no';
                    assessmentData[fieldName[0]].data.Moderate[fieldName[2]].applyScoreDisabled = true;
                    assessmentData[fieldName[0]].data.Moderate[fieldName[2]].scoreDisabled = true;
                }
                
                if(assessmentData[fieldName[0]].data.Severe != undefined){
                    assessmentData[fieldName[0]].data.Severe[fieldName[2]].score = 0;
                    assessmentData[fieldName[0]].data.Severe[fieldName[2]].isScore = false;
                    assessmentData[fieldName[0]].data.Severe[fieldName[2]].applyScore = 'no';
                    assessmentData[fieldName[0]].data.Severe[fieldName[2]].applyScoreDisabled = true;
                    assessmentData[fieldName[0]].data.Severe[fieldName[2]].scoreDisabled = true;
                }
                
            }
            else if (fieldName[1] == 'mo') {
                assessmentData[fieldName[0]].data.Moderate[fieldName[2]].score = assessmentData[fieldName[0]].data.Moderate[fieldName[2]].Points__c;
                assessmentData[fieldName[0]].data.Moderate[fieldName[2]].isScore = true;
                
                if(assessmentData[fieldName[0]].data.Minimal != undefined){
                    assessmentData[fieldName[0]].data.Minimal[fieldName[2]].score = 0;
                    assessmentData[fieldName[0]].data.Minimal[fieldName[2]].isScore = false;
                    assessmentData[fieldName[0]].data.Minimal[fieldName[2]].applyScore = 'no';
                    assessmentData[fieldName[0]].data.Minimal[fieldName[2]].applyScoreDisabled = true;
                    assessmentData[fieldName[0]].data.Minimal[fieldName[2]].scoreDisabled = true;
                }                
                
                if(assessmentData[fieldName[0]].data.Severe != undefined){
                    assessmentData[fieldName[0]].data.Severe[fieldName[2]].score = 0;
                    assessmentData[fieldName[0]].data.Severe[fieldName[2]].isScore = false;
                    assessmentData[fieldName[0]].data.Severe[fieldName[2]].applyScore = 'no';
                    assessmentData[fieldName[0]].data.Severe[fieldName[2]].applyScoreDisabled = true;
                    assessmentData[fieldName[0]].data.Severe[fieldName[2]].scoreDisabled = true;
                }
            }
            else {
                assessmentData[fieldName[0]].data.Severe[fieldName[2]].score = assessmentData[fieldName[0]].data.Severe[fieldName[2]].Points__c;
                assessmentData[fieldName[0]].data.Severe[fieldName[2]].isScore = true;
                
                if(assessmentData[fieldName[0]].data.Minimal != undefined){
                    assessmentData[fieldName[0]].data.Minimal[fieldName[2]].score = 0;
                    assessmentData[fieldName[0]].data.Minimal[fieldName[2]].isScore = false;
                    assessmentData[fieldName[0]].data.Minimal[fieldName[2]].applyScore = 'no';
                    assessmentData[fieldName[0]].data.Minimal[fieldName[2]].applyScoreDisabled = true;
                    assessmentData[fieldName[0]].data.Minimal[fieldName[2]].scoreDisabled = true;
                }
                
                if(assessmentData[fieldName[0]].data.Moderate != undefined){
                    assessmentData[fieldName[0]].data.Moderate[fieldName[2]].score = 0;
                    assessmentData[fieldName[0]].data.Moderate[fieldName[2]].isScore = false;
                    assessmentData[fieldName[0]].data.Moderate[fieldName[2]].applyScore = 'no';
                    assessmentData[fieldName[0]].data.Moderate[fieldName[2]].applyScoreDisabled = true;
                    assessmentData[fieldName[0]].data.Moderate[fieldName[2]].scoreDisabled = true;
                }
            }

        }
        else {
            if (fieldName[1] == 'm') {
                assessmentData[fieldName[0]].data.Minimal[fieldName[2]].score = 0;
                assessmentData[fieldName[0]].data.Minimal[fieldName[2]].isScore = false;
                
                if(assessmentData[fieldName[0]].data.Moderate != undefined){
                    assessmentData[fieldName[0]].data.Moderate[fieldName[2]].applyScoreDisabled = false;
                    assessmentData[fieldName[0]].data.Moderate[fieldName[2]].scoreDisabled = false;
                }
                
                if(assessmentData[fieldName[0]].data.Severe != undefined){
                    assessmentData[fieldName[0]].data.Severe[fieldName[2]].applyScoreDisabled = false;
                    assessmentData[fieldName[0]].data.Severe[fieldName[2]].scoreDisabled = false;
                }
            }
            else if (fieldName[1] == 'mo') {
                assessmentData[fieldName[0]].data.Moderate[fieldName[2]].score = 0;
                assessmentData[fieldName[0]].data.Moderate[fieldName[2]].isScore = false;
                
                if(assessmentData[fieldName[0]].data.Minimal != undefined){
                    assessmentData[fieldName[0]].data.Minimal[fieldName[2]].applyScoreDisabled = false;
                    assessmentData[fieldName[0]].data.Minimal[fieldName[2]].scoreDisabled = false;
                }
                
                if(assessmentData[fieldName[0]].data.Severe != undefined){
                    assessmentData[fieldName[0]].data.Severe[fieldName[2]].applyScoreDisabled = false;
                    assessmentData[fieldName[0]].data.Severe[fieldName[2]].scoreDisabled = false;
                }
            }
            else {
                assessmentData[fieldName[0]].data.Severe[fieldName[2]].score = 0;
                assessmentData[fieldName[0]].data.Severe[fieldName[2]].isScore = false;
                
                if(assessmentData[fieldName[0]].data.Moderate != undefined){
                    assessmentData[fieldName[0]].data.Moderate[fieldName[2]].applyScoreDisabled = false;
                    assessmentData[fieldName[0]].data.Moderate[fieldName[2]].scoreDisabled = false;
                }
                
                if(assessmentData[fieldName[0]].data.Minimal != undefined){
                    assessmentData[fieldName[0]].data.Minimal[fieldName[2]].applyScoreDisabled = false;
                    assessmentData[fieldName[0]].data.Minimal[fieldName[2]].scoreDisabled = false;
                }
            }
        }
        cmp.set("v.assessmentDataToIterate", assessmentData);

    },
    onCheck: function (cmp, event) {
        var chkName = event.getSource().get("v.name");
        var chkValue = event.getSource().get("v.value");
        var assessmentData = cmp.get("v.assessmentDataToIterate");
		var notApplicable = cmp.get("v.notApplicable");
        if (chkValue == true) {
            if(assessmentData[chkName].data.Minimal != undefined){
                assessmentData[chkName].data.Minimal.forEach(element => {
                    element.applyScore = 'NA';
                    element.score = 0;
                    element.applyScoreDisabled = true;
                    element.scoreDisabled = true;
                });
            }
            if(assessmentData[chkName].data.Moderate != undefined){
                assessmentData[chkName].data.Moderate.forEach(element => {
                    element.applyScore = 'NA';
                    element.score = 0;
                    element.applyScoreDisabled = true;
                    element.scoreDisabled = true;
                });
            }
            if(assessmentData[chkName].data.Severe != undefined){
                assessmentData[chkName].data.Severe.forEach(element => {
                    element.applyScore = 'NA';
                    element.score = 0;
                    element.applyScoreDisabled = true;
                    element.scoreDisabled = true;
                });
            }
            notApplicable[cmp.get("v.headerName")] = true;
            
        } else {
            if(assessmentData[chkName].data.Minimal != undefined){
                assessmentData[chkName].data.Minimal.forEach(element => {
                    element.applyScore = '';
                    element.score = 0;
                    element.applyScoreDisabled = false;
                    element.scoreDisabled = false;
                });
            }
            if(assessmentData[chkName].data.Moderate != undefined){        
                assessmentData[chkName].data.Moderate.forEach(element => {
                    element.applyScore = '';
                    element.score = 0;
                    element.applyScoreDisabled = false;
                    element.scoreDisabled = false;
                });
            }
            if(assessmentData[chkName].data.Severe != undefined){
                assessmentData[chkName].data.Severe.forEach(element => {
                    element.applyScore = '';
                    element.score = 0;
                    element.applyScoreDisabled = false;
                    element.scoreDisabled = false;
                });
            }
            notApplicable[cmp.get("v.headerName")] = false;
           
        }
        console.log('notApplicable');
        console.log(JSON.stringify(notApplicable));
        cmp.set('v.notApplicable',notApplicable);
        cmp.set("v.assessmentDataToIterate", assessmentData);

    },
    handleCancel: function (cmp, event) {

        $A.get("e.force:closeQuickAction").fire();

    },
    
    handlePrevious: function (cmp, event,helper) {
        console.log('handlePrevious');
        console.log(JSON.stringify(cmp.get('v.notApplicable')));
        cmp.set("v.showTotalScore",false);
        cmp.set("v.showSaveButton",false);
        if(parseInt(cmp.get("v.currentScreen")) > 0){
            cmp.set("v.currentScreen",(parseInt(cmp.get("v.currentScreen")) - 1).toString());
            cmp.set("v.currentScreenInt",(parseInt(cmp.get("v.currentScreen"))));
        }
        console.log(cmp.get("v.assessmentData"));
        cmp.set("v.headerName",cmp.get("v.assessmentData")[cmp.get("v.currentScreenInt")][0].key);
		cmp.set("v.assessmentDataToIterate",cmp.get("v.assessmentData")[cmp.get("v.currentScreenInt")]);
		
        var notApplicable = cmp.get("v.notApplicable");
		if(notApplicable[cmp.get("v.headerName")] == true){
            cmp.set("v.checkBoxValue",true);
        }
        else{
            cmp.set("v.checkBoxValue",false);
        }        
    },
    
    handleNext: function (cmp, event,helper) {
        console.log('handleNext');        
        
        cmp.set("v.showSaveButton",false);
        var isScored = false;
        var assessmentData = cmp.get("v.assessmentDataToIterate");
        var isApplicableEmpty = false;
        cmp.find('apply-score').forEach(function (ele) {
            var value = ele.get("v.value");
            console.log('value: '+value);
            if (value == undefined) {
                console.log('In if');
                ele.setCustomValidity("Apply score is required");
            } else {
                ele.setCustomValidity(""); // if there was a custom error before, reset it                
            }
        });
        
        for (let index = 0; index < assessmentData.length; index++) {
			
            if(assessmentData[index].data.Minimal != undefined){
                assessmentData[index].data.Minimal.forEach(element => {
                    
                    var assessLine = Object.assign({}, cmp.get("v.AssessmentLineItem"));
                    assessLine.Apply_Score__c = element.applyScore;
                    if(assessLine.Apply_Score__c ==null || assessLine.Apply_Score__c ==''){
                        isApplicableEmpty = true;
                        helper.showToast('error', 'Apply Score is  Required', 'Please select apply score.');
    
                    }
                    if (element.score) {
                        assessLine.Score__c = element.score;
                    }
                    if(assessLine.Apply_Score__c == 'yes' && element.score == 0){
                        helper.showToast('error', 'Score Required', 'Please enter score.');
                        isScored = true;
                    }
                    
                });
        	}
            
        	if(assessmentData[index].data.Moderate != undefined){
                assessmentData[index].data.Moderate.forEach(element => {
                    var assessLine = Object.assign({}, cmp.get("v.AssessmentLineItem"));
                    assessLine.Apply_Score__c = element.applyScore;
                    if(assessLine.Apply_Score__c ==null || assessLine.Apply_Score__c ==''){
                        isApplicableEmpty = true;
                        helper.showToast('error', 'Apply Score is  Required', 'Please select apply score.');
    
                    }
                    if (element.score) {
                        assessLine.Score__c = element.score;
                    }
                    if(assessLine.Apply_Score__c == 'yes' && element.score == 0){
                        helper.showToast('error', 'Score Required', 'Please enter score.');
                        isScored = true;
                    }                
                    
                });
    		}
                    
            if(assessmentData[index].data.Severe != undefined){
                assessmentData[index].data.Severe.forEach(element => {
                    var assessLine = Object.assign({}, cmp.get("v.AssessmentLineItem"));
                    assessLine.Apply_Score__c = element.applyScore;
                    if(assessLine.Apply_Score__c ==null || assessLine.Apply_Score__c ==''){
                        isApplicableEmpty = true;
                        helper.showToast('error', 'Apply Score is  Required', 'Please select apply score.');
    
                    }
                    if (element.score) {
                        assessLine.Score__c = element.score;
                    }
                    if(assessLine.Apply_Score__c == 'yes' && element.score == 0){
                        helper.showToast('error', 'Score Required', 'Please enter score.');
                        isScored = true;
                    }                
                });
        	}
            
        }

        if(!isScored && !isApplicableEmpty){
            if(parseInt(cmp.get("v.currentScreen")) < cmp.get("v.totalDomain")){
            cmp.set("v.currentScreen",(parseInt(cmp.get("v.currentScreen")) + 1).toString());
            cmp.set("v.currentScreenInt",(parseInt(cmp.get("v.currentScreen"))));
            }
            if(parseInt(cmp.get("v.currentScreen")) == cmp.get("v.totalDomain") - 1){
                cmp.set("v.showSaveButton",true);
            }       
                        
            let assessmentData = cmp.get("v.assessmentData");
            assessmentData[cmp.get("v.currentScreenInt") - 1] = cmp.get("v.assessmentDataToIterate");
            cmp.set("v.assessmentData",assessmentData);                        
            
        }
        if(cmp.get("v.totalDomain") == cmp.get("v.currentScreenInt")){
            cmp.set("v.headerName",'Review');
            cmp.set("v.showTotalScore",true);
            helper.handleSave(cmp, event,helper,'false');
        }
        else{
            console.log('in else');
            console.log(cmp.get("v.assessmentData"));
            console.log(cmp.get("v.currentScreenInt"));
            if(cmp.get("v.assessmentData")[cmp.get("v.currentScreenInt")][0] == undefined){
                cmp.set("v.headerName",cmp.get("v.assessmentData")[cmp.get("v.currentScreenInt")].key);
            }
            else{
                cmp.set("v.headerName",cmp.get("v.assessmentData")[cmp.get("v.currentScreenInt")][0].key);
            }
            var notApplicable = cmp.get("v.notApplicable");
            if(notApplicable[cmp.get("v.headerName")] == true){
                cmp.set("v.checkBoxValue",true);
            }
            else{
                cmp.set("v.checkBoxValue",false);
            }
            cmp.set("v.assessmentDataToIterate",cmp.get("v.assessmentData")[cmp.get("v.currentScreenInt")]);
        }
        
    },
        
    openTotalPointsModal: function(cmp,event,helper){
    	console.log('openTotalPointsModal');
        cmp.set("v.isTotalPointsModalOpen", true);
    },
        
    totalPointsSave: function(cmp,event,helper){
    	console.log('totalPointsSave');
        console.log(cmp.get("v.totalScore"));
        
        helper.updateMethod(cmp,event,helper,cmp.get("v.totalScore"));
    }
})