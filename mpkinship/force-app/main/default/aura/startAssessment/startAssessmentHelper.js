({
    initCont: function(component,event,helper){
        console.log('initCont');
        var action = component.get("c.getAssessmentSettings");
        action.setCallback(this, function (response) {
			var state = response.getState();
            console.log('showStartButtonMethod Helper: '+state);
            
            if (state === "SUCCESS") {
                var resData = response.getReturnValue();
                var tempData = [];
                component.set("v.assessmentData", response.getReturnValue());
                var key1;
                for (key1 in resData) {
                    if (resData.hasOwnProperty(key1)) {
                        tempData.push({ key: key1, data: resData[key1] });
                        //console.log("%c " + key + " = " + resData[key], "color:cyan");
                    }
                }
                component.set("v.assessmentData", tempData);
                if(component.get("v.assessmentData").length > 0){
                    component.set("v.assessmentDataToIterate",component.get("v.assessmentData")[0]);
                    component.set("v.headerName",component.get("v.assessmentData")[0].key);
                	component.set("v.totalDomain",component.get("v.assessmentData").length);
                }                
                
            } else if (state === "ERROR") {
                console.log("Error == ", response.getError());
            }
        });
        $A.enqueueAction(action);
    },

    showToast : function(type, title, message) {
       
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : title,
            message: message,
            duration:' 4000',
            key: 'info_alt',
            type: type,
            mode: 'pester'
        });
        toastEvent.fire();
    },
    
    handleSave: function (cmp, event,helper,saveRecord) {
		console.log('handleSave');
        //var isScored = false;
        var assessId = cmp.get("v.recordId");
        var assessLineList = cmp.get("v.AssessmentLineItemList");
        var assessmentData = cmp.get("v.assessmentData");
        console.log(JSON.stringify(assessmentData));
        if(saveRecord == 'false'){
            var totalScore = 0;
            for (let index = 0; index < assessmentData.length; index++) {
                console.log('for');
                //console.log(assessmentData[index][0].data);
                if(assessmentData[index][0].data.Minimal != undefined){
                    assessmentData[index][0].data.Minimal.forEach(element => {                
                        console.log('Minimal');
                        var assessLine = Object.assign({}, cmp.get("v.AssessmentLineItem"));
                        assessLine.Apply_Score__c = element.applyScore;
                        
                        if (element.score) {
                            assessLine.Score__c = element.score;
                            totalScore = parseInt(totalScore) + parseInt(element.score);
                        }
                        
                        assessLine.VEMAT_Assessment__c = element.Id;
                        assessLine.Assessment__c = assessId;
                        assessLineList.push(assessLine);
                        
                    });
                }
                if(assessmentData[index][0].data.Moderate != undefined){
                    assessmentData[index][0].data.Moderate.forEach(element => {
                        var assessLine = Object.assign({}, cmp.get("v.AssessmentLineItem"));
                        assessLine.Apply_Score__c = element.applyScore;
                        
                        if (element.score) {
                            assessLine.Score__c = element.score;
                            totalScore = parseInt(totalScore) + parseInt(element.score);
                        }
                        
                        assessLine.VEMAT_Assessment__c = element.Id;
                        assessLine.Assessment__c = assessId;
                        assessLineList.push(assessLine);
                        
                        
                    });
                }
                if(assessmentData[index][0].data.Severe != undefined){
                    assessmentData[index][0].data.Severe.forEach(element => {
                        var assessLine = Object.assign({}, cmp.get("v.AssessmentLineItem"));
                        assessLine.Apply_Score__c = element.applyScore;
                        
                        if (element.score) {
                            assessLine.Score__c = element.score;
                            totalScore = parseInt(totalScore) + parseInt(element.score);
                        }
                        
                        assessLine.VEMAT_Assessment__c = element.Id;
                        assessLine.Assessment__c = assessId;
                        assessLineList.push(assessLine);
                        
                    });
                }
                cmp.set("v.totalScore",totalScore)                
                
            }
        }

        if(saveRecord == 'true'){
            var action = cmp.get("c.saveAssessment");
        	action.setParams({
            assessLine: assessLineList,
            recId: cmp.get("v.recordId")
            })
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    //helper.showToast('success', 'Records created', 'Records have been created.');
                    //$A.get("e.force:closeQuickAction").fire();
                    //cmp.set("v.isModalOpen",false);
                    console.log("Success: " + response.getReturnValue());
                    cmp.set("v.headerName",'Total Score');
                    helper.updateMethod(cmp,event,helper,cmp.get("v.totalScore"));
                }
                else {
                    console.log('ERROR:' + state + response.getError());
                }
            });
            $A.enqueueAction(action)  
        }
    },
        
    updateMethod: function(cmp,event,helper,totalScore){
    	var action = cmp.get("c.updateAssessment");
        action.setParams({
            recId: cmp.get("v.recordId"),
            totalPoints: totalScore
        })
        action.setCallback(this, function (response) {
            console.log()
            var state = response.getState();
            if (state === "SUCCESS") {
                cmp.set("v.isTotalPointsModalOpen", false);
                cmp.set("v.showTotalPointsButton",false);
                cmp.set("v.isModalOpen", false);
                cmp.set("v.showStartButton",false);
                helper.showToast('success', 'Records updated', 'Records have been updated.');
            }
        });
        $A.enqueueAction(action)
    },
        
    totalPointsHelper: function(cmp,event,helper){
    	var action = cmp.get("c.showTotalPointsButtonMethod");
        action.setParams({
            recId: cmp.get("v.recordId")
        })
        action.setCallback(this, function (response){
            var state = response.getState();
            console.log('showTotalPointsButtonMethod: '+state);
            if (state === "SUCCESS") {
                var resData = response.getReturnValue();
                console.log('init: '+resData);
                if(resData == true){
                    cmp.set("v.showTotalPointsButton",true);                    
                }
            }
        });
        
        $A.enqueueAction(action);
    }
})