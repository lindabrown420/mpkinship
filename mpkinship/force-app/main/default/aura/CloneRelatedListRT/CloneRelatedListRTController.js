({
	init: function (component, event, helper) {
		var actions = [
            { label: 'View', name: 'view' },
            { label: 'Delete', name: 'delete' }
        ];
        helper.createTableColumns(component, event, helper);
        helper.createSelectStr(component, event, helper);
        var temObjName = component.get( "v.sobjectName" );  
        var action = component.get( "c.fetchRecs" );  
        action.setParams({  
            recId: component.get( "v.recordId" ),  
            sObjName: temObjName,  
            parentFldNam: component.get( "v.parentFieldName" ),  
            strCriteria: component.get( "v.criteria" ),
            selectStr: component.get( "v.selectStr" )
        });  
        action.setCallback(this, function(response) {  
            var state = response.getState();  
            if ( state === "SUCCESS" ) {  
                  
                var tempTitle = component.get( "v.title" );  
                var res = response.getReturnValue();
                console.log('**Val of res'+JSON.stringify(res));
                if(res!=null && res!=undefined && res.length > 10)
                    component.set("v.IsShowAll",true);
                else
                    component.set("v.IsShowAll",false);
                for(var i = 0; i < res.length; i++){
                    	res[i].recordURL = '/'+res[i].Id; 
                    console.log('**lookup2val'+ component.get("v.isLookup2"));
                    console.log('***parentfieldapi2'+component.get("v.ParentfieldAPI2"));
                    /*if(component.get("v.isLookup2")!=undefined && component.get("v.isLookup2")==true &&
                       component.get("v.ParentfieldAPI2")!=undefined ){
                   		console.log('**Enterd in if');
                        console.log('**VAl of parentfieldapi2'+ component.get("v.ParentfieldAPI2"));
                        var parentid =component.get("v.ParentfieldAPI2");
                        if(component.get("v.ParentfieldAPI2")=='npe01__Opportunity__c')
                        	res[i].f2 ='/'+res[i].npe01__Opportunity__c;
                        else{
                            component.set("v.isLookup2",false);
                        }    
                    }*/
                    
                    if(component.get("v.isLookup1")!=undefined && component.get("v.isLookup1")==true &&
                       component.get("v.ParentfieldAPI1")!=undefined ){
                        var parentid =component.get("v.ParentfieldAPI1");
                        var parentName =component.get( "v.fieldAPI1");
                        parentName = parentName.split(".");
                        if(res[i][parentid]){
                            res[i].f2url ='/'+res[i].parentid;
                        }
                        else{
                            res[i].f2url = '';
                        }
                        if(parentName.length == 2 && res[i][parentName[0]]){
                            if(res[i][parentName[0]][parentName[1]]){
                                res[i].f2Name =res[i][parentName[0]][parentName[1]];
                            }
                            else{
                                res[i].f2Name = '';
                            }
                        }
                           
                    }
                    if(component.get("v.isLookup2")!=undefined && component.get("v.isLookup2")==true &&
                       component.get("v.ParentfieldAPI2")!=undefined ){
                        var parentid =component.get("v.ParentfieldAPI2");
                        var parentName =component.get( "v.fieldAPI2");
                        parentName = parentName.split(".");
                        if(res[i][parentid]){
                            res[i].f2url ='/'+res[i].parentid;
                        }
                        else{
                            res[i].f2url = '';
                        }
                        if(parentName.length == 2 && res[i][parentName[0]]){
                            if(res[i][parentName[0]][parentName[1]]){
                                res[i].f2Name =res[i][parentName[0]][parentName[1]];
                            }
                            else{
                                res[i].f2Name = '';
                            }
                        }
                           
                    }
                    if(component.get("v.isLookup3")!=undefined && component.get("v.isLookup3")==true &&
                       component.get("v.ParentfieldAPI3")!=undefined ){
                        var parentid =component.get("v.ParentfieldAPI3");
                        var parentName =component.get( "v.fieldAPI3");
                        parentName = parentName.split(".");
                        if(res[i][parentid]){
                            res[i].f3url ='/'+res[i][parentid];
                        }
                        else{
                            res[i].f3url = '';
                        }
                        if(parentName.length == 2 && res[i][parentName[0]]){
                            if(res[i][parentName[0]][parentName[1]]){
                                res[i].f3Name =res[i][parentName[0]][parentName[1]];
                            }
                            else{
                                res[i].f3Name = '';
                            }
                        }
                           
                    }
                    if(component.get("v.isLookup4")!=undefined && component.get("v.isLookup4")==true &&
                       component.get("v.ParentfieldAPI4")!=undefined ){
                        var parentid =component.get("v.ParentfieldAPI4");
                        var parentName =component.get( "v.fieldAPI4");
                        parentName = parentName.split(".");
                        if(res[i][parentid]){
                            res[i].f4url ='/'+res[i][parentid];
                        }
                        else{
                            res[i].f4url = '';
                        }
                        if(parentName.length == 2 && res[i][parentName[0]]){
                            if(res[i][parentName[0]][parentName[1]]){
                                res[i].f4Name =res[i][parentName[0]][parentName[1]];
                            }
                            else{
                                res[i].f4Name = '';
                            }
                        }
                            
                    }
                    if(component.get("v.isLookup5")!=undefined && component.get("v.isLookup5")==true &&
                       component.get("v.ParentfieldAPI5")!=undefined ){
                        var parentid =component.get("v.ParentfieldAPI5");
                        var parentName =component.get( "v.fieldAPI5");
                        parentName = parentName.split(".");
                        if(res[i][parentid]){
                            res[i].f5url ='/'+res[i][parentid];
                        }
                        else{
                            res[i].f5url = '';
                        }
                        if(parentName.length == 2 && res[i][parentName[0]]){
                            if(res[i][parentName[0]][parentName[1]]){
                                res[i].f5Name =res[i][parentName[0]][parentName[1]];
                            }
                            else{
                                res[i].f5Name = '';
                            }
                        }
                           
                    }
                    if(component.get("v.isLookup6")!=undefined && component.get("v.isLookup6")==true &&
                       component.get("v.ParentfieldAPI6")!=undefined ){
                        var parentid =component.get("v.ParentfieldAPI6");
                        var parentName =component.get( "v.fieldAPI6");
                        parentName = parentName.split(".");
                        if(res[i][parentid]){
                            res[i].f6url ='/'+res[i][parentid];
                        }
                        else{
                            res[i].f6url = '';
                        }
                        if(parentName.length == 2 && res[i][parentName[0]]){
                            if(res[i][parentName[0]][parentName[1]]){
                                res[i].f6Name =res[i][parentName[0]][parentName[1]];
                            }
                            else{
                                res[i].f6Name = '';
                            }
                        }
                       
                    }

                }
                
                var responselist=[];
                if(res.length > 10){
                    for(var i=0 ; i < res.length ;i++){
                        if(i<10)
                        	responselist.push(res[i]);
                        else
                            break;
                    }
                }
                if(res.length <=10)    
                	component.set( "v.data", response.getReturnValue() ); 
                else if( res.length >10 && responselist.length >0)
                    component.set("v.data",responselist);
            }
            else if(state==="ERROR"){
         
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                }
            }     
        });  
        $A.enqueueAction(action); 

	},
    createNew : function (cmp, event, helper) {
        var Rtype= cmp.get( "v.recType" );
        var obj= cmp.get( "v.sobjectName" );
        console.log('Rtype'+Rtype);
        if(Rtype=="Case Actions" && obj=='Opportunity'){
            /*var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": "/lightning/n/Create_Case_Action"
            });
            urlEvent.fire();*/
            window.open('/lightning/n/Create_Case_Action');
        }
        else if(Rtype=="Report of Collection" && obj=='Opportunity'){
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": "/lightning/n/Create_Report_of_Collections"
            });
            urlEvent.fire();
        }
        else if(Rtype=="Staff & Operations" && obj=='Opportunity'){
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": "/lightning/n/New_Invoice"
            });
            urlEvent.fire();
        }
        else if(Rtype=="Purchase of Services Order" && obj=='Opportunity'){         
            window.open('/lightning/n/Create_POSO');            
        }
        else{
                var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": "/lightning/o/"+obj+"/new"
            });
            urlEvent.fire();
            }
    },
    handleRowAction: function (cmp, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');

        switch (action.name) {
            case 'view':
                alert('Showing Details: ' + JSON.stringify(row));
                break;
            case 'delete':
                helper.removeBook(cmp, row);
                break;
        }
    },
    viewRelatedList: function (component, event, helper) {  
          
        var navEvt = $A.get("e.force:navigateToRelatedList");  
        navEvt.setParams({  
            "entityApiName": component.get("v.childRelName"),
            "relatedListId": component.get( "v.sobjectName" ),  
            "parentRecordId": component.get( "v.recordId" )  
        });
        window.open('/lightning/r/'+component.get("v.childRelName")+'/'+component.get( "v.recordId" )+'/related/'+ component.get( "v.sobjectNames" )+'/view');
        //navEvt.fire();  
    },
    viewRelatedFullList : function(component,event,helper){
    	 var pageReference = {
            type: "standard__component",
            attributes: {
                componentName: "c__RelatedListFullView"    
            },    
            state: {
                c__rid:component.get("v.recordId"),
                c__title : component.get("v.title"),
                c__columns : JSON.stringify(component.get("v.columns")),
                c__selectStr : component.get("v.selectStr"),
                c__sObjName : component.get("v.sobjectName"),
                c__criteria : component.get( "v.criteria" ),
                c__parentFldNam :component.get("v.parentFieldName"),  
                c__isLookup1 :component.get("v.isLookup1"),
                c__isLookup2 :component.get("v.isLookup2"),
                c__isLookup3 :component.get("v.isLookup3"),
                c__isLookup4 :component.get("v.isLookup4"),
                c__isLookup5 :component.get("v.isLookup5"),
                c__isLookup6 :component.get("v.isLookup6"),
                c__ParentfieldAPI1 :component.get("v.ParentfieldAPI1"),
                c__ParentfieldAPI2 :component.get("v.ParentfieldAPI2"),
                c__ParentfieldAPI3 :component.get("v.ParentfieldAPI3"),
                c__ParentfieldAPI4 :component.get("v.ParentfieldAPI4"),
                c__ParentfieldAPI5 :component.get("v.ParentfieldAPI5"),
                c__ParentfieldAPI6 :component.get("v.ParentfieldAPI6"),
                c__fieldAPI1 :component.get("v.fieldAPI1"),
                c__fieldAPI2 :component.get("v.fieldAPI2"),
                c__fieldAPI3 :component.get("v.fieldAPI3"),
                c__fieldAPI4 :component.get("v.fieldAPI4"),
                c__fieldAPI5 :component.get("v.fieldAPI5"),
                c__fieldAPI6 :component.get("v.fieldAPI6"),
            }
        };
        var navService = component.find("navService");
          const handleUrl = (url) => {
            window.open(url);
        };
        const handleError = (error) => {
            console.log(error);
        };
        navService.generateUrl(pageReference).then(handleUrl, handleError);
        //navService.navigate(pageReference);
	}    
    
})