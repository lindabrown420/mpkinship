({
	createTableColumns : function(component, event, helper) {
		var colums = [];
        var f1 = component.get( "v.fieldAPI1" );
        var f1Label = component.get( "v.fieldLabel1" );
        
        if(f1 && f1 != ''){
            if(component.get("v.isLookup1")!=undefined && component.get("v.isLookup1")==true && 
              component.get("v.ParentfieldAPI1")!=undefined && component.get("v.ParentfieldAPI1")!='')
           colums.push({ label: f1Label, fieldName: 'f1url', type: 'url', 
                typeAttributes: { label: { fieldName: 'f1Name' }, tooltip:{ fieldName: f1 }, target: '_blank' } });    
            else
               colums.push({ label: f1Label, fieldName: 'recordURL', type: 'url', 
                        typeAttributes: { label: { fieldName: f1 }, tooltip:{ fieldName: f1 }, target: '_blank' } });
       }
        var f2 = component.get( "v.fieldAPI2" );
        var f2Label = component.get( "v.fieldLabel2" );
        var type2 = component.get('v.fieldAPI2Type');
        if(f2 && f2 != ''){
            if(component.get("v.isLookup2")!=undefined && component.get("v.isLookup2")==true && 
               component.get("v.ParentfieldAPI2")!=undefined && component.get("v.ParentfieldAPI2")!=''){
           		colums.push({ label: f2Label, fieldName: 'f2url', type: 'url', 
             	typeAttributes: { label: { fieldName: 'f2Name' }, tooltip:{ fieldName: f2 }, target: '_blank' } });     
            }
            else
            	colums.push({ label: f2Label, fieldName: f2, type: type2});
            
        }
        var f3 = component.get( "v.fieldAPI3" );
        var f3Label = component.get( "v.fieldLabel3" );
        var type3 = component.get('v.fieldAPI3Type');
        if(f3 && f3 != ''){
            if(component.get("v.isLookup3")!=undefined && component.get("v.isLookup3")==true && 
               component.get("v.ParentfieldAPI3")!=undefined && component.get("v.ParentfieldAPI3")!=''){
           		colums.push({ label: f3Label, fieldName: 'f3url', type: 'url', 
             	typeAttributes: { label: { fieldName: 'f3Name' }, tooltip:{ fieldName: f3 }, target: '_blank'  }  });     
            }
            else
            	colums.push({ label: f3Label, fieldName: f3, type: type3 });
            
        }
        var f4 = component.get( "v.fieldAPI4" );
        var f4Label = component.get( "v.fieldLabel4" );
        var type4 = component.get('v.fieldAPI4Type');
        if(f4 && f4 != ''){
            if(component.get("v.isLookup4")!=undefined && component.get("v.isLookup4")==true && 
               component.get("v.ParentfieldAPI4")!=undefined && component.get("v.ParentfieldAPI4")!=''){
           		colums.push({ label: f4Label, fieldName: 'f4url', type: 'url', 
             	typeAttributes: { label: { fieldName: 'f4Name' }, tooltip:{ fieldName: f4 }, target: '_blank' }  });     
            }
            else
            	colums.push({ label: f4Label, fieldName: f4, type: type4 });
            
        }
        var f5 = component.get( "v.fieldAPI5" );
        var f5Label = component.get( "v.fieldLabel5" );
        var type5 = component.get('v.fieldAPI5Type');
        if(f5 && f5 != ''){
            if(component.get("v.isLookup5")!=undefined && component.get("v.isLookup5")==true && 
               component.get("v.ParentfieldAPI5")!=undefined && component.get("v.ParentfieldAPI5")!=''){
           		colums.push({ label: f5Label, fieldName: 'f5url', type: 'url', 
             	typeAttributes: { label: { fieldName: 'f5Name' }, tooltip:{ fieldName: f5 }, target: '_blank' }  });     
            }
            else
            	colums.push({ label: f5Label, fieldName: f5, type: type5 });
            
        }

        var f6 = component.get( "v.fieldAPI6" );
        var f6Label = component.get( "v.fieldLabel6" );
        var type6 = component.get('v.fieldAPI6Type');
        if(f6 && f6 != ''){
            if(component.get("v.isLookup6")!=undefined && component.get("v.isLookup6")==true && 
            component.get("v.ParentfieldAPI6")!=undefined && component.get("v.ParentfieldAPI6")!=''){
           		colums.push({ label: f6Label, fieldName: 'f6url', type: 'url', 
             	typeAttributes: { label: { fieldName:'f6Name' }, tooltip:{ fieldName: f6 }, target: '_blank' }  });     
            }
            else
            	colums.push({ label: f6Label, fieldName: f6, type: type6 });
            
        }
        component.set('v.columns', colums);
        console.log('**Col type'+ typeof colums);
        console.log('**col'+JSON.stringify(component.get("v.columns")));
        
	},
    createSelectStr: function(component, event, helper) {
        var selectStr = '';//component.get('v.selectStr');
        var f1 = component.get( "v.fieldAPI1" );
      
        if(f1 && f1 != ''){
            selectStr += f1;
        }
        var f2 = component.get( "v.fieldAPI2" );
        var isLookup2 = component.get( "v.isLookup2" );
        var ParentfieldAPI2 = component.get("v.ParentfieldAPI2");
        if(f2 && f2 != ''){
            if(selectStr != ''){
                selectStr += ', ';
                
            }
            selectStr += f2;
            if(isLookup2!=undefined && isLookup2!='' && ParentfieldAPI2!=undefined && ParentfieldAPI2!=''){
            	if(selectStr != ''){
                    selectStr += ', ';
                    selectStr +=ParentfieldAPI2;
            	}    
            }
        }
        var f3 = component.get( "v.fieldAPI3" );
        var isLookup3 = component.get( "v.isLookup3" );
        var ParentfieldAPI3 = component.get("v.ParentfieldAPI3");
        if(f3 && f3 != ''){
            if(selectStr != ''){
                selectStr += ', ';
            }
            selectStr += f3;
            if(isLookup3!=undefined && isLookup3!='' && ParentfieldAPI3!=undefined && ParentfieldAPI3!=''){
            	if(selectStr != ''){
                    selectStr += ', ';
                    selectStr +=ParentfieldAPI3;
            	}    
            }
        }
        var f4 = component.get( "v.fieldAPI4" );
        var isLookup4 = component.get( "v.isLookup4" );
        var ParentfieldAPI4 = component.get("v.ParentfieldAPI4");
        if(f4 && f4 != ''){
            if(selectStr != ''){
                selectStr += ', ';
            }
            selectStr += f4;
            if(isLookup4!=undefined && isLookup4!='' && ParentfieldAPI4!=undefined && ParentfieldAPI4!=''){
            	if(selectStr != ''){
                    selectStr += ', ';
                    selectStr +=ParentfieldAPI4;
            	}    
            }
        }
        var f5 = component.get( "v.fieldAPI5" );
        var isLookup5 = component.get( "v.isLookup5" );
        var ParentfieldAPI5 = component.get("v.ParentfieldAPI5");
        if(f5 && f5 != ''){
            if(selectStr != ''){
                selectStr += ', ';
            }
            selectStr += f5;
            if(isLookup5!=undefined && isLookup5!='' && ParentfieldAPI5!=undefined && ParentfieldAPI5!=''){
            	if(selectStr != ''){
                    selectStr += ', ';
                    selectStr +=ParentfieldAPI5;
            	}    
            }
        }

        var f6 = component.get( "v.fieldAPI6" );
        var isLookup6 = component.get( "v.isLookup6" );
        var ParentfieldAPI6 = component.get("v.ParentfieldAPI6");
        if(f6 && f6 != ''){
            if(selectStr != ''){
                selectStr += ', ';
            }
            selectStr += f6;
            if(isLookup6!=undefined && isLookup6!='' && ParentfieldAPI6!=undefined && ParentfieldAPI6!=''){
            	if(selectStr != ''){
                    selectStr += ', ';
                    selectStr +=ParentfieldAPI6;
            	}    
            }
        }
        component.set('v.selectStr',selectStr);
    }
})