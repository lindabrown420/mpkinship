({
    init : function(component, event, helper) {
        component.set('v.columns', [
            {label: 'Printed Check Name', fieldName: 'name', type: 'text'},
            {label: 'Check Run', type: 'button', initialWidth: 135, typeAttributes: { label: 'Check Run', name: 'download_checkrun', title: 'Click to download the Check Run'}},
            {label: 'Created By', fieldName: 'createdBy', type: 'text'},
            {label: 'Created Date', fieldName: 'createdDate', type: 'date-local',typeAttributes: {  
                                                                                    day: 'numeric',  
                                                                                    month: 'short',  
                                                                                    year: '2-digit',  
                                                                                    hour: '2-digit',  
                                                                                    minute: '2-digit',  
                                                                                    second: '2-digit',  
                                                                                    hour12: true}}
        ]);

        helper.getData(component);
    },
    handleRowAction: function (cmp, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');
        switch (action.name) {
            case 'download_checkrun':
                helper.getCheck(cmp, row);
                break;
        }
    }
})