({
    getData : function(component) {
        component.set('v.Spinner', true);
        let action = component.get("c.getCheckList");
        let recordId = component.get("v.recordId");
        let strType = component.get("v.strType");
        action.setParams({
            strId: recordId,
            strType: strType
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let data = response.getReturnValue();
                if(data) {
                    component.set("v.data", data);
                }
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    getCheck: function(component, data) {
        let checkrunid = data.id;
        let documentPackageId = $A.get('$Label.c.KINSHIP_CR_DDP');
        let deliveryOptionId = $A.get('$Label.c.KINSHIP_CR_OPTION');
        let recordIds = checkrunid;
        let sObjectType = 'Check_Run__c';
        let url = window.location.protocol + '//' + window.location.host + '/apex/ProcessDdp';
        url = url + '?ddpId=' + documentPackageId;
        url = url + '&deliveryOptionId=' + deliveryOptionId;
        url = url + '&ids=' + recordIds;
        url = url + '&sObjectType=' + sObjectType;
        window.open(url ,'_blank');
    }
})