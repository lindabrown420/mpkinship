({
    init : function(component, event, helper) {
        component.set('v.Spinner', true);
        let action = component.get("c.RefreshBudget");
        action.setParams({});
        action.setCallback(this, function(a) {
            //var state = a.getState();
            //if (state === "SUCCESS"){
            let navEvent = $A.get("e.force:navigateToList");
            navEvent.setParams({
                "listViewId": '00B3d000000UAmBEAW',
                "listViewName": null,
                "scope": "Budgets__c"
            });
            navEvent.fire(); 
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    }
})