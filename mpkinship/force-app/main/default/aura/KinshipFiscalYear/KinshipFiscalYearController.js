({
    init : function(component, event, helper) {
        component.set('v.Spinner', true);
        let action = component.get("c.initData");
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let data = response.getReturnValue();
                if(data) {
                    data.lstChild = [];
                    //let child = {'name':'','parentId':'','reporter':'','reportDueDate':null,'startDate':null,'endDate':null,'status':'','fyType':'Local','isSendDueDate':false,'userSelection':[],'userErrors':[]};
                    //data.lstChild.push(child);
                    data.parentCamp.startDate = helper.getDate(6, 1, 0);
                    data.parentCamp.endDate = helper.getDate(5, 30, 1);
                    data.parentCamp.status = '';
                    data.parentCamp.fyType = 'Local';
                    component.set("v.campData", data);
                    helper.getPicklistOptions(component, data.statusoptions, 'statusoption');
                    helper.getPicklistOptions(component, data.typeoptions, 'fytypeoption');
                    helper.getFIPSOptions(component, data.lstFIPS);
                }else{
                    helper.showToast('error', 'Error', 'Please contact your Administrator for more details.');
                }
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                console.log(strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    campLookupSearch : function(component, event, helper) {
        const lookupComponent = event.getSource();
        const serverSearchAction = component.get('c.kinshipSearchLookup');
        serverSearchAction.setParam('anOptionalParam', 'Campaign');
        lookupComponent.search(serverSearchAction);
    },
    clearCampErrorsOnChange: function(component, event, helper) {
        const selection = component.get('v.campSelection');
        const errors = component.get('v.campErrors');

        let formData = event.getParam("formData");
        if(formData) {
            let campData = component.get('v.campData');
            campData.parentCamp.parentId = formData.recordId;
            component.set('v.campData', campData);
        }
        if (selection.length && errors.length) {
            component.set('v.campErrors', []);
        }
    },
    userLookupSearch : function(component, event, helper) {
        const lookupComponent = event.getSource();
        const serverSearchAction = component.get('c.kinshipSearchLookup');
        serverSearchAction.setParam('anOptionalParam', 'User');
        lookupComponent.search(serverSearchAction);
    },
    clearUserErrorsOnChange: function(component, event, helper) {
        const selection = component.get('v.userSelection');
        const errors = component.get('v.userErrors');
        let formData = event.getParam("formData");
        if(formData) {
            let campData = component.get('v.campData');
            let index = parseInt(formData.param);
            campData.lstChild[index].reporter = formData.recordId;
            component.set('v.campData', campData);
        }

        if (selection.length && errors.length) {
            component.set('v.userErrors', []);
        }
    },
    fipsLookupSearch : function(component, event, helper) {
        const lookupComponent = event.getSource();
        const serverSearchAction = component.get('c.kinshipSearchLookup');
        serverSearchAction.setParam('anOptionalParam', 'FIPS');
        lookupComponent.search(serverSearchAction);
    },
    clearFipsErrorsOnChange: function(component, event, helper) {
        const selection = component.get('v.fipsSelection');
        const errors = component.get('v.fipsErrors');

        let formData = event.getParam("formData");
        if(formData) {
            let campData = component.get('v.campData');
            campData.parentCamp.fips = formData.recordId;
            component.set('v.campData', campData);
        }
        if (selection.length && errors.length) {
            component.set('v.fipErrors', []);
        }
    },
    handleRemove: function(component, event, helper) {
        let target = event.getSource();
        let strClass = target.get("v.class");
        let indexes = strClass.split('index_');
        if (indexes && indexes.length == 2) {
            let campData = component.get('v.campData');
            let newcampData = [];
            for(let i = 0 ; i < campData.lstChild.length ; i++) {
                if (i !== parseInt(indexes[1])) {
                    newcampData.push(campData.lstChild[i]);
                }
            }
            campData.lstChild = newcampData;
            component.set('v.campData', campData);
        }
    },
    handleAdd: function(component, event, helper) {
        let campData = component.get('v.campData');
        let child = {'name':'','parentId':'','reporter':'','reportDueDate':null,'startDate':null,'endDate':null,'status':'','fyType':'Local','isSendDueDate':false,'userSelection':[],'userErrors':[]};
        campData.lstChild.push(child);
        component.set('v.campData', campData);
    },
    handleCancel: function(component, event, helper) {
        helper.gobacktoListView(component);
    },
    handleSave: function(component, event, helper) {
        // check data
        let campData = component.get("v.campData");
        campData.parentCamp.startDate = helper.parseDate(campData.parentCamp.startDate);
        campData.parentCamp.endDate = helper.parseDate(campData.parentCamp.endDate);

        /*if(!campData.parentCamp.status){
            helper.showToast('info', 'Missing Data', 'Please input Status.');
            return;
        }*/
        if(!campData.parentCamp.fyType){
            helper.showToast('info', 'Missing Data', 'Please choose Fiscal Year Type.');
            return;
        }
        if(!campData.parentCamp.startDate){
            helper.showToast('info', 'Missing Data', 'Please choose Begin Date.');
            return;
        }
        if(!campData.fips || campData.fips.length === 0){
            helper.showToast('info', 'Missing Data', 'Please choose FIPS.');
            return;
        }
        if(!campData.parentCamp.endDate){
            helper.showToast('info', 'Missing Data', 'Please choose End Date.');
            return;
        }
        if(campData.parentCamp.startDate > campData.parentCamp.endDate){
            helper.showToast('info', 'Missing Data', 'Begin Date must be less than End Date.');
            return;
        }
        if(!helper.checkDate(campData.parentCamp.startDate, true)){
            helper.showToast('info', 'Wrong Data', 'Begin Date must start on 1st of the Month.');
            return;
        }
        if(!helper.checkDate(campData.parentCamp.endDate, false)){
            helper.showToast('info', 'Wrong Data', 'End Date must start on the end of the Month.');
            return;
        }

        if(campData.parentCamp.fyType !== 'Local') {
            /*if(!campData.parentCamp.parentId){
                helper.showToast('info', 'Missing Data', 'Missing Parent Fiscal Year.');
                return;
            }*/
            if(campData.lstChild.length === 0){
                helper.showToast('info', 'Missing Data', 'Please choose Reporting Period to add LASER or LEDRS information.');
                return;
            }
            for(let i = 0 ; i < campData.lstChild.length ; i++) {
                campData.lstChild[i].startDate = helper.parseDate(campData.lstChild[i].startDate);
                campData.lstChild[i].endDate = helper.parseDate(campData.lstChild[i].endDate);
                campData.lstChild[i].reportDueDate = helper.parseDate(campData.lstChild[i].reportDueDate);
                if(!campData.lstChild[i].startDate){
                    helper.showToast('info', 'Missing Data', 'Please choose Child Start Date.');
                    return;
                }
                if(!campData.lstChild[i].endDate){
                    helper.showToast('info', 'Missing Data', 'Please choose Child End Date.');
                    return;
                }
                /*if(!campData.lstChild[i].reporter){
                    helper.showToast('info', 'Missing Data', 'Please choose child reporter.');
                    return;
                }
                if(!campData.lstChild[i].reportDueDate){
                    helper.showToast('info', 'Missing Data', 'Please choose child Report Due Date.');
                    return;
                }*/
                if(campData.lstChild[i].startDate > campData.lstChild[i].endDate){
                    helper.showToast('info', 'Wrong Data', 'Child Begin Date must be less than End Date.');
                    return;
                }
                if(campData.lstChild[i].startDate < campData.parentCamp.startDate || campData.lstChild[i].startDate > campData.parentCamp.endDate){
                    helper.showToast('info', 'Wrong Data', 'Child Begin Date must be between Parent Begin Date and End Date.');
                    return;
                }
                if(campData.lstChild[i].endDate < campData.parentCamp.startDate || campData.lstChild[i].endDate > campData.parentCamp.endDate){
                    helper.showToast('info', 'Wrong Data', 'Child End Date must be between Parent Begin Date and End Date.');
                    return;
                }
                if(!helper.checkDate(campData.lstChild[i].startDate, true)){
                    helper.showToast('info', 'Wrong Data', 'Child Begin Date must start on 1st of the Month.');
                    return;
                }
                if(!helper.checkDate(campData.lstChild[i].endDate, false)){
                    helper.showToast('info', 'Wrong Data', 'Child End Date must start on the end of the Month.');
                    return;
                }
                if(campData.parentCamp.fyType === 'DSS') {
                    let d = helper.checkInt(campData.lstChild[i].autoPostDays);
                    if(d === '' || isNaN(d)){
                        helper.showToast('info', 'Wrong Data', 'Child Auto Post Days must be a number.');
                        return;
                    } else {
                        campData.lstChild[i].autoPostDays = d;
                        if(!helper.checkAutoPostDay(campData.lstChild[i])){
                            helper.showToast('info', 'Wrong Data', 'The Auto Post Date must fall between the 1st of the month and LASER Due Date.');
                            return;
                        }
                    }
                }
            }
        }
        // call save Data
        component.set('v.Spinner', true);
        let action = component.get("c.saveFiscalYear");
        action.setParams({
            'data': campData
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                helper.resetData(component, event);
                helper.gobacktoListView(component);
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                console.log(strErr);
                helper.showToast('error', 'Error', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    handleTypeChange: function(component, event, helper) {
        let selectedValue = event.getParam("value");
        if('DSS' === selectedValue) {
            component.set('v.sectionName', 'LASER Reporting Periods');
            component.set('v.is12Months', false);
            helper.resetData(component, event);
            let campData = component.get('v.campData');
            campData.parentCamp.startDate = helper.getDate(5, 1, 0);
            campData.parentCamp.endDate = helper.getDate(4, 31, 1);
            component.set('v.campData', campData);
        } else if('CSA' === selectedValue) {
            component.set('v.sectionName', 'LEDRS Reporting Periods');
            component.set('v.is12Months', false);
            helper.resetData(component, event);
            let campData = component.get('v.campData');
            campData.parentCamp.startDate = helper.getDate(6, 1, 0);
            campData.parentCamp.endDate = helper.getDate(5, 30, 1);
            component.set('v.campData', campData);
        } else if('Local' === selectedValue) {
            component.set('v.sectionName', 'LOCAL Fiscal Year');
            component.set('v.is12Months', true);
            helper.resetData(component, event);
            let campData = component.get('v.campData');
            campData.parentCamp.startDate = helper.getDate(6, 1, 0);
            campData.parentCamp.endDate = helper.getDate(5, 30, 1);
            component.set('v.campData', campData);
        }
    },
    handlePeriodChanged : function(component, event, helper){
        helper.generateChild(component, event);
    },
    handleDateChanged: function(component, event, helper){
        component.set('v.period', '');
        helper.resetData(component, event, true);
    }
})