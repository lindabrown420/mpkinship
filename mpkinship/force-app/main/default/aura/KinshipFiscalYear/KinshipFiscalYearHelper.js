({
    showToast : function(type, title, message) {
        let strTitle = title.split('\n').join('');
        let strMessage = message.split('\n').join('');
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : strTitle,
            message: strMessage,
            duration:' 4000',
            key: 'info_alt',
            type: type, // success , error, info
            mode: 'pester'//'sticky'
        });
        toastEvent.fire();
    },
    getPicklistOptions: function(component, d, strValue) {
        if(d) {
            let data = [];
            for(let i = 0 ; i < d.length ; i++) {
                data.push({'label' : d[i], 'value':d[i]});
            }
            component.set("v." + strValue, data);
        }
    },
    getFIPSOptions: function(component, d) {
        if(d) {
            let data = [];
            for(let i = 0 ; i < d.length ; i++) {
                data.push({'label' : d[i].Name, 'value':d[i].Id});
            }
            component.set("v.fipsoptions", data);
        }
    },
    gobacktoListView: function(component){
        let navEvent = $A.get("e.force:navigateToList");
        navEvent.setParams({
            "listViewId": null,
            "listViewName": null,
            "scope": "Campaign"
        });
        navEvent.fire();
    },
    resetData: function(component, event, isDate) {
        let campData = component.get('v.campData');
        //let child = {'name':'','parentId':'','reporter':'','reportDueDate':null,'startDate':null,'endDate':null,'status':'','fyType':'Local','isSendDueDate':false,'userSelection':[],'userErrors':[]};
        campData.lstChild = [];
        campData.fips = [];
        campData.parentCamp.status = '';
        if(!isDate) {
            campData.parentCamp.startDate = this.getDate(6, 1, 0);
            campData.parentCamp.endDate = this.getDate(5, 30, 1);
        }
        //campData.lstChild.push(child);
        component.set('v.campData', campData);
        component.set('v.period', '');
    },
    generateChild: function(component, event){
        let period = component.get('v.period');
        if(!period){
            return;
        }
        let count = 1;
        if('quaterly' === period){
            count = 3;
        }
        let campData = component.get('v.campData');
        campData.parentCamp.startDate = this.parseDate(campData.parentCamp.startDate);
        campData.parentCamp.endDate = this.parseDate(campData.parentCamp.endDate);
        if(!campData.parentCamp.startDate){
            this.showToast('info', 'Missing Data', 'Please Choose Begin Date.');
            return;
        }
        if(!campData.parentCamp.endDate){
            this.showToast('info', 'Missing Data', 'Please Choose End Date.');
            return;
        }
        if(campData.parentCamp.startDate > campData.parentCamp.endDate){
            this.showToast('info', 'Missing Data', 'Begin Date must be less than End Date.');
            return;
        }
        if(!this.checkDate(campData.parentCamp.startDate, true)){
            this.showToast('info', 'Wrong Data', 'Begin Date must start at 1st of the Month.');
            return;
        }
        if(!this.checkDate(campData.parentCamp.endDate, false)){
            this.showToast('info', 'Wrong Data', 'End Date must start at the end of the Month.');
            return;
        }
        
        let d = new Date(campData.parentCamp.startDate);
        campData.lstChild = [];
        while(d <= campData.parentCamp.endDate){
            let firstDate = new Date(d.getFullYear(), d.getMonth(), 1);;
            let lastDate = new Date(d.getFullYear(), d.getMonth() + count, 0);;
            let child = {'name':'','parentId':'','reporter':'','reportDueDate':null,'startDate':this.formatDate(firstDate),'endDate':this.formatDate(lastDate),'status':'','fyType':'Local','isSendDueDate':false,'userSelection':[],'userErrors':[]};
            campData.lstChild.push(child);
            d = new Date(d.setMonth(d.getMonth() + count));
        }
        campData.parentCamp.startDate = this.formatDate(campData.parentCamp.startDate);
        campData.parentCamp.endDate = this.formatDate(campData.parentCamp.endDate);
        this.getDueDate(campData);
        component.set('v.campData', campData);
    },
    getDueDate: function(campData){
        if(campData.parentCamp.fyType !== 'DSS'){
            return;
        }
        let lstSchedule = campData.lstSchedule;
        for(let i = 0 ; i < campData.lstChild.length ; i++){
            let endDate= this.parseDate(campData.lstChild[i].endDate);
            for(let j = 0 ; j < lstSchedule.length ; j++) {
                if((endDate.getMonth() + 1) === this.getInt(lstSchedule[j].Month__c) && endDate.getFullYear() === this.getInt(lstSchedule[j].Year__c)){
                    campData.lstChild[i].reportDueDate = lstSchedule[j].LASER_Close__c;
                    campData.lstChild[i].isSendDueDate = true;
                    campData.lstChild[i].reporterName = lstSchedule[j].Reporter__r.Name;
                    campData.lstChild[i].reporter = lstSchedule[j].Reporter__c;
                    campData.lstChild[i].autoPostDays = 0;
                    continue;
                }
            }
        }
    },
    getDate: function(m, d, y){
        let today = new Date();
        return (today.getFullYear() + y) + '-' + (m + 1) + '-' + d;
    },
    formatDate: function(d){
        if(!d) return '';
        return d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate();
    },
    checkDate: function(d, isStart) {
        if(isStart) {
            if(d && d.getDate() !== 1){
                return false;
            }
        } else {
            if(d){
                let month = d.getMonth();
                let nextMonth = new Date(d.getTime());
                nextMonth.setDate(nextMonth.getDate() + 1);
                return nextMonth.getMonth() !== month;
            }
        }
        return true;
    },
    parseDate: function(d) {
        if(d && typeof d === "string"){
            return new Date(d + ' 00:00:00');
        }
        return d;
    },
    getInt: function(d) {
        if(d && typeof d === "string"){
            return parseInt(d);
        }
        return d;
    },
    checkInt: function(d) {
        try{
            return parseInt(d);
        }catch(e){
            return '';
        }
    },
    checkAutoPostDay: function(data){
        if(data.reportDueDate && data.reportDueDate.getDate() - data.autoPostDays <= 0) {
            return false;
        }
        return true;
    }
})