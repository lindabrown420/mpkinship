({
    showToast : function(type, title, message) {
       
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : title,
            message: message,
            duration:' 4000',
            key: 'info_alt',
            type: type,
            mode: 'pester'
        });
        toastEvent.fire();
    },
    showSpinner: function (component, event, helper) {
        component.set('v.loaded', true);
    },
     
    hideSpinner: function (component, event, helper) {
        component.set('v.loaded', false);
    }
})