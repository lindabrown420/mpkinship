({
    init : function(component, event, helper) {
        let today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
        component.set('v.endDate', today);
    },
    onChangeBegin: function(component, event, helper) {
        component.set('v.startDate', component.find('begin').get('v.value'));
        let startDate = component.get('v.startDate');
        if(startDate) {
        	component.set('v.startDateD', component.get('v.startDate').split('-')[1] +'/'+ component.get('v.startDate').split('-')[2]+'/'+component.get('v.startDate').split('-')[0]);
        }
    },
    onChangeEnd: function(component, event, helper) {
        component.set('v.endDate', component.find('end').get('v.value'));
        let endDateD = component.get('v.endDateD');
        if(endDateD) {
        	component.set('v.endDateD', component.get('v.endDate').split('-')[1] +'/'+ component.get('v.endDate').split('-')[2]+'/'+component.get('v.endDate').split('-')[0]);
        }
    },
    generateSPR: function(component, event, helper) {
        helper.showSpinner(component);
        var listofId = [];
        listofId.push(component.get('v.recordId'));
        var action = component.get('c.generate');

        action.setParams({
            caseId:listofId,
            startDate: component.get('v.startDateD'),
            endDate: component.get('v.endDateD')
        })

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                helper.showToast('success', 'SPR Report', 'The Requested SPR has been generated. To view the SPR, go to Files in the Quick Links below.');
                helper.hideSpinner(component);
                $A.get("e.force:closeQuickAction").fire();
                console.log("Success: " + response.getReturnValue());
            }
            else {
                console.log('ERROR:' + state + response.getError());
            }
        })
        $A.enqueueAction(action);
    },
})