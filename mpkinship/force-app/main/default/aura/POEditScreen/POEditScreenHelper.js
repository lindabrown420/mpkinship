({
    getInitialValues : function(component,event,helper){
        console.log('**Entered in getinitialvalues');
    	var action=component.get("c.FetchInitialValues");
        action.setParams({          	                
            recordid : component.get("v.recordId")
        });	
         action.setCallback(this,function(response){           
            var state = response.getState();
            console.log('**State val'+state);
            if(state==="SUCCESS"){
                 var result=response.getReturnValue(); 
                console.log('******&&&&&&&&&&&&&&&&&&');
                 console.log(JSON.stringify(result));
                //GET PROFILE VALUE
                if(result.profileName!=null){
                    component.set("v.profileName",result.profileName);
                    if(result.profileName=='System Administrator' || result.profileName=='CSA Admin' ||
                       result.profileName=='Fiscal Officer'){                 
                        component.set("v.IsDesiredProfiles",true);
                        console.log('**Entered in is desired profile'+component.get("v.IsDesiredProfiles"));
                    }
                }
                
                if(result.IsPaidPayments!=null){
                    console.log('**Val of ispaidpayments'+result.IsPaidPayments);
                    component.set("v.IsPaidPayments",result.IsPaidPayments);
                }    
                //GET OPPORTUNITY RECORD VALUE
                if(result.opportunity!=null){   
                    console.log('**Val of opporutnity'+JSON.stringify(result.opportunity));
                    console.log('**Val of stagename'+result.opportunity.StageName);
                    component.set("v.opportunityStage",result.opportunity.StageName);
                    if(result.opportunity.AccountId!=null){
                        component.set("v.vendorId",result.opportunity.AccountId);
                        component.set("v.InitialVendorId",result.opportunity.AccountId);
                    }
                    //KIN 989
                    if(result.opportunity.Caseworker_Name__c!=null){
                        component.set("v.employeeId",result.opportunity.Caseworker_Name__c);
                    } 
                    //ends
                    if(result.opportunity.Campaign!=null){
                        component.set("v.selectedLookupRecord",result.opportunity.Campaign);
                        component.set("v.campaignId",result.opportunity.Campaign.Id);
                        component.set("v.InitialCampaignId",component.get("v.campaignId"));
                        if(result.opportunity.Campaign.StartDate!=null)
                            component.set("v.APbeginDate",result.opportunity.Campaign.StartDate);
                        console.log('**Ap begin date'+component.get("v.APbeginDate"));
                        if(result.opportunity.Campaign.EndDate!=null)
                            component.set("v.APEndDate",result.opportunity.Campaign.EndDate);
                    }    
                    console.log(result.opportunity.Purchase_Order_Id__c);
                    console.log(component.get("v.POSONum"));
                    //KIN 1339
                    if(result.opportunity.Purchase_Order_Id__c!=null){
                        component.set("v.POSONum",result.opportunity.Purchase_Order_Id__c);
                    }
                    //ends
                    if(result.opportunity.FIPS_Code__c!=null)
                        component.set("v.FIPSId",result.opportunity.FIPS_Code__c);
                    if(result.opportunity.CloseDate!=null)  
                         component.set("v.closeDate",result.opportunity.CloseDate);
                    if(result.opportunity.End_Date__c!=null)
                        component.set("v.EndDate",result.opportunity.End_Date__c);
                    if(result.opportunity.Kinship_Case__c!=null)
                        component.set("v.caseId",result.opportunity.Kinship_Case__c);
                    if(result.opportunity.Send_Email__c!=null){
                        component.set("v.SendEmail",result.opportunity.Send_Email__c);
                        if(result.opportunity.Send_Email__c==true && result.opportunity.StageName=='Draft')
                            component.find("sendEmailId").set("v.value",true);
                        if(result.opportunity.Amount!=null && result.opportunity.Amount!=undefined)
                         component.set("v.TotalAmount",result.opportunity.Amount);
                        if(result.opportunity.Description!=null)
                        	component.set("v.POSONote",result.opportunity.Description);
                    }
                        
                    if(result.opportunity.OpportunityLineItems!=null && result.opportunity.OpportunityLineItems!=undefined && result.opportunity.OpportunityLineItems.length>0){
                        console.log('&&&&&&');                        
                        component.set("v.OpportunitiesLineItemsList",result.opportunity.OpportunityLineItems);
                        console.log(JSON.stringify(component.get("v.OpportunitiesLineItemsList")));
                        component.set("v.opportunitylineitems",result.opportunity.OpportunityLineItems); 
                        console.log('*****'+JSON.stringify(component.get("v.opportunitylineitems")));
                        //WRAP OF CATEGORY, MADNATE TYEPS AND ALL.
                        if(result.oppitemswrap!=null && result.oppitemswrap!=undefined)
                        	component.set("v.opportunitywraplineitems",result.oppitemswrap);
                        console.log('**Stringify item wrap'+JSON.stringify(component.get("v.opportunitywraplineitems")));
                    }
                     if(result.pricebookrec!=null && result.pricebookrec!=undefined){
                        component.set("v.pricebookrecordId",result.pricebookrec.Id);  
                
                        console.log('before setpricebookentries');
                         if(result.pricebookrec.PricebookEntries!=null && result.pricebookrec.PricebookEntries!=undefined){
                        	console.log('**entered in pricebookentry found on doinit');
                            component.set("v.Pricebookfound",true);
            				component.set("v.ShowAddServices",true);
                             //ADDED PRICEBOOKPRESENT TO STORE THE VALUE OF PRICEBOOKID
                        	component.set("v.pricebookrecordId",result.pricebookrec.Id);
                            component.set("v.PricebookPresent",true);
                            component.set("v.EntriesValues",result.pricebookrec.PricebookEntries);
                             //ADDED FOR KIN-934
                            if(component.get("v.profileName")!=undefined && (component.get("v.profileName")=='System Administrator' || 
                               component.get("v.profileName")=='Fiscal Officer' || component.get("v.profileName")=='CSA Admin' )){
                                component.set("v.Pricebookfound",false);
                            }     
                            this.setPricebookEntries(component,event,helper,result.pricebookrec.PricebookEntries);
                         }
                         else if(result.pricebookrec.PricebookEntries==null || result.pricebookrec.PricebookEntries==undefined){
                         	component.set("v.pricebookandentries",'');
                            component.set("v.IsPricebookWithoutEntry",true);
                            component.set("v.PricebookIdWithoutEntry",result.pricebookrec.Id);
                            component.set("v.Ispricebookmessage",true); 
                            component.set("v.messageText",'There are no active services for the pricebook associated with the selected vendor for the selected Parent Fiscal year. Please reach out to the Finance department to create the services for the pricebook. You will be able to continue by selecting a different vendor.');
                            console.log('**msgtext'+component.get("v.messageText"));
                            //ADDED
                            if(component.get("v.profileName")!=undefined && (component.get("v.profileName")=='System Administrator' || 
                               component.get("v.profileName")=='Fiscal Officer' || component.get("v.profileName")=='CSA Admin' )){
                            //if(component.get("v.profileName")!=undefined && (component.get("v.profileName")=='Fiscal Officer' || component.get("v.profileName")=='CSA Admin' )){ 
                                component.set("v.ShowAddServices",true);
                                component.set("v.messageText",'There are no active pricebooks associated with the selected vendor for the selected Parent Fiscal year. Please click on add button to add a new service.');
                            }    
                             component.set("v.Pricebookfound",false);
                             component.set("v.pricebookrecordId",'');
                             component.set("v.PricebookPresent",false);
                         }
                    	
                     }
                   
                    if(result.pricebookrec==null){
                    	component.set("v.Ispricebookmessage",true); 
                		component.set("v.pricebookandentries",'');
                	    component.set("v.messageText",'There are no active pricebooks associated with the selected vendor for the selected Parent Fiscal year. Please reach out to the Finance department to create the pricebook. You will be able to continue by selecting a different vendor.');    
                    	//ADDED
                        if(component.get("v.profileName")!=undefined && (component.get("v.profileName")=='System Administrator' || 
                          component.get("v.profileName")=='Fiscal Officer' || component.get("v.profileName")=='CSA Admin' )){
                       /* if(component.get("v.profileName")!=undefined && (component.get("v.profileName")=='Fiscal Officer' || component.get("v.profileName")=='CSA Admin' )){ */
                            component.set("v.ShowAddServices",true);
                            component.set("v.messageText",'There are no active pricebooks associated with the selected vendor for the selected Parent Fiscal year. Please click on add button to add a new service.');
                        }    
                        component.set("v.Pricebookfound",false);
                        component.set("v.pricebookrecordId",'');
                        component.set("v.PricebookPresent",false);
                    }
                    
                } 
                if(component.get("v.opportunityStage")=='Open' && component.get("v.IsPaidPayments")==false && component.get("v.IsDesiredProfiles")==true)
                    component.set("v.IsOpenAllConditions",true);
            }   
        });
        $A.enqueueAction(action); 
    },
    setPricebookEntries:function(component,event,helper,pricebookEntries){
      //var opplineitems = component.get("v.opportunitylineitems");   
      var opplineitems = component.get("v.opportunitywraplineitems");  
      console.log('**oppitems'+JSON.stringify(opplineitems));
       var finalpricebookentrieslist=[];
       var entriesNotinLineitems=[];
        if(opplineitems!=undefined && opplineitems.length > 0){
            for(var p=0 ; p < pricebookEntries.length ; p++){
                var isfound=false;
                for(var i =0 ; i< opplineitems.length ; i++){  
                    console.log('^^^^'+(JSON.stringify(opplineitems[i])));
                    if(pricebookEntries[p].Product2Id== opplineitems[i].opplineitem.Product2Id && 
                       pricebookEntries[p].Id == opplineitems[i].opplineitem.PricebookEntryId) {
                    	isfound=true;
                        console.log('**Enterd in if'+JSON.stringify(pricebookEntries[p]));
                        var pricebookentry={};
                        pricebookentry.opplineitemId =opplineitems[i].opplineitem.Id;
                        pricebookentry.Service_Name__c=pricebookEntries[p].Service_Name__c;
                        pricebookentry.Pricebook2Id = pricebookEntries[p].Pricebook2Id;
                        pricebookentry.Service_Unit_Measure__c= pricebookEntries[p].Service_Unit_Measure__c;
                        //pricebookentry.Service_Description__c=pricebookEntries[p].Service_Description__c;
                        pricebookentry.Unit_Measure_Label__c=pricebookEntries[p].Unit_Measure_Label__c;
                        pricebookentry.UnitPrice=pricebookEntries[p].UnitPrice;
                        pricebookentry.Product2Id=pricebookEntries[p].Product2Id;
                        pricebookentry.Id = pricebookEntries[p].Id;
                        pricebookentry.quantity=opplineitems[i].opplineitem.Quantity;
                        console.log('**pricebook cat'+JSON.stringify(opplineitems[i].opplineitem.Category__r));
                        opplineitems[i].categorySelection={"Id":opplineitems[i].opplineitem.Category__r.Id,"Name":opplineitems[i].opplineitem.Category__r.Name,"Category_Type__c":opplineitems[i].opplineitem.Category__r.Category_Type__c,"Inactive__c":opplineitems[i].opplineitem.Category__r.Inactive__c,"RecordType":{"DeveloperName":opplineitems[i].opplineitem.Category__r.RecordType.DeveloperName,"Id":opplineitems[i].opplineitem.Category__r.RecordType.Id}};
                     
                        //opplineitems[i].categorySelection={"Id":opplineitems[i].opplineitem.Category__r.Id,"Name":opplineitems[i].opplineitem.Category__r.Name,"Category_Type__c":opplineitems[i].opplineitem.Category__r.Category_Type__c,"Inactive__c":opplineitems[i].opplineitem.Category__r.Inactive__c};
                        pricebookentry.categorySelection= opplineitems[i].categorySelection;
                       // pricebookentry.recipient=opplineitems[i].opplineitem.Parent_Recipient__c;
                        console.log('succeed till here');
                        if(opplineitems[i].opplineitem.Cost_Center__c!=null)
                            pricebookentry.costcenterSelection={"Id":opplineitems[i].opplineitem.Cost_Center__c,"Name":opplineitems[i].opplineitem.Cost_Center__r.Name};
                        if(opplineitems[i].costcenterids!=null)
                            pricebookentry.CostCenterIds = opplineitems[i].costcenterids;
                        if(opplineitems[i].showcostcenter!=null)
                            pricebookentry.showCostCenterLookup = opplineitems[i].showcostcenter;
                        //FOR IVE PART
                        if(opplineitems[i].opplineitem.Category__r.Category_Type__c=='IVE' && opplineitems[i].opplineitem.Parent_Recipient__c!=undefined ){
                        	console.log('**enterd in ive part'); 
                            pricebookentry.showCSA=false;
                            pricebookentry.showIVE=true;
                            pricebookentry.showFund=false;
                            component.set("v.IsCSA",false);
                            pricebookentry.recipientIVE=opplineitems[i].opplineitem.Parent_Recipient__c;
                        	pricebookentry.FundIVE= opplineitems[i].opplineitem.Fund__c;
                        }
                        //FOR OTHER LASER CATEGORIES
                        else if(opplineitems[i].opplineitem.Category__r.Category_Type__c!='IVE' && opplineitems[i].opplineitem.Category__r.Category_Type__c!='CSA'
                           && opplineitems[i].opplineitem.Category__r.RecordType!=undefined && opplineitems[i].opplineitem.Category__r.RecordType.DeveloperName!=undefined && opplineitems[i].opplineitem.Category__r.RecordType.DeveloperName=='LASER_Category'){
                        	 pricebookentry.showCSA=false;
                            pricebookentry.showIVE=false;
                            pricebookentry.showFund=true;	
                            pricebookentry.Fund= opplineitems[i].opplineitem.Fund__c;
                            pricebookentry.IsFund=true;
                        }
                        //FOR CSA PART
                       else if(opplineitems[i].opplineitem.Category__r.Category_Type__c=='CSA' && opplineitems[i].opplineitem.Service_Placement_Type__c!=null && opplineitems[i].opplineitem.Service_Name_Code_Value__c!=null
                         && opplineitems[i].opplineitem.Service_Name_Code__c!=null && opplineitems[i].opplineitem.Service_Placement_Type_Code__c!=null
                           && opplineitems[i].opplineitem.Mandate_Type__c!=null && opplineitems[i].opplineitem.Mandate_Type_Code__c !=null){
                            pricebookentry.SPTNameselectedlabel=opplineitems[i].opplineitem.Service_Name_Code__c;
                            pricebookentry.SPTNameselected=opplineitems[i].opplineitem.Service_Name_Code_Value__c;
                            pricebookentry.MandateSelected=opplineitems[i].opplineitem.Mandate_Type_Code__c ;
                            pricebookentry.MandateSelectedlabel=opplineitems[i].opplineitem.Mandate_Type__c;
                            pricebookentry.SPTselected= opplineitems[i].opplineitem.Service_Placement_Type_Code__c;
                            pricebookentry.SPTselectedlabel =opplineitems[i].opplineitem.Service_Placement_Type__c;
                            pricebookentry.showCSA=true;
                            pricebookentry.showIVE=false;
                            pricebookentry.showFund=false;
                             component.set("v.IsCSA",true);
                            if(opplineitems[i].opplineitem.Parent_Recipient__c!=undefined){
                                console.log('**Val of parent reciepinet'+ opplineitems[i].opplineitem.Parent_Recipient__c);
                            	pricebookentry.recipient = opplineitems[i].opplineitem.Parent_Recipient__c;
                            }
                         }
                        if(opplineitems[i].opplineitem.Service_Description_Other__c!=null)
                            pricebookentry.Description=opplineitems[i].opplineitem.Service_Description_Other__c;
                        console.log('succeed 2');
                        console.log('**@@@VAl of mandatenames'+ JSON.stringify(opplineitems[i]));
                       if(opplineitems[i].MandateNames!=null && opplineitems[i].MandateNames.length >0){
                            var mandatenames = opplineitems[i].MandateNames;
                            console.log('**VAl of mandatenames'+JSON.stringify(mandatenames));
                            var items=[];
                            var item = {                           
                                "label": "---None---",
                                "value": ''
                            };
                            items.push(item);
                            
                           for(var m=0; m < mandatenames.length; m++) {
                                var item = {                           
                                    "label": mandatenames[m].label,
                                    "value": mandatenames[m].value.toString()
                                };
                                items.push(item);
                            } 
                           pricebookentry.Mandateoptions=items; 
                                             
                        }  
                        console.log('succeed 3');
                        console.log('**VAl of names'+ JSON.stringify(opplineitems[i]));
                        if(opplineitems[i].Names!=null && opplineitems[i].Names.length >0){
                            var sptvalues=[];
                            var sptnames = opplineitems[i].Names;
                            console.log('**sptnames'+sptnames);
                            //GETTING SCROLLBAR ON ITEMS
                            var items = [];
                            var item = {                           
                                "label": "---None---",
                                "value": ''
                            };
                            items.push(item);
                            for (var s = 0; s < sptnames.length; s++) {
                                var item = {                           
                                    "label": sptnames[s].label,
                                    "value": sptnames[s].value.toString()
                                };
                                items.push(item);
                            } 
                            console.log('**items'+items);
                            pricebookentry.SPToptions=items;            	
                      }
                       console.log('succeed 4'); 
                       if(opplineitems[i].SPTNamesCodes!=null && opplineitems[i].SPTNamesCodes.length>0){
                            var sptvalues=[];
                            var sptcodenames = opplineitems[i].SPTNamesCodes;
                            //GETTING SCROLLBAR ON ITEMS
                            var items = [];
                            var item = {                           
                                "label": "---None---",
                                "value": ''
                            };
                            items.push(item);
                            for (var sc = 0; sc < sptcodenames.length; sc++) {
                                var item = {                           
                                    "label": sptcodenames[sc].label,
                                    "value": sptcodenames[sc].value.toString()
                                };
                                items.push(item);
                            }                      
                            pricebookentry.SPTNames=items;            	
                      	}
                      	console.log('**Succeed 5');                    
                        finalpricebookentrieslist.push(pricebookentry);
                    }              
                }
                if(isfound==false && pricebookEntries[p].IsActive==true)
                    entriesNotinLineitems.push(pricebookEntries[p]);    
            } 
        }     
        
        console.log('**Finalpricebookentries'+JSON.stringify(finalpricebookentrieslist));
        console.log('**Entries not lineiems'+JSON.stringify(entriesNotinLineitems));
        //TO SHOW SAVE BUTTON
        if(finalpricebookentrieslist.length>0){
            component.set("v.alreadyselectedServices",finalpricebookentrieslist);
            //ADDED
            component.set("v.InitialOpportunityLineitems",finalpricebookentrieslist);
            component.set("v.POselectedServices",finalpricebookentrieslist);
            component.set("v.IsSave",true);
        }   
        if(entriesNotinLineitems.length>0){
            for(var i=0 ; i < entriesNotinLineitems.length ; i++){
                finalpricebookentrieslist.push(entriesNotinLineitems[i]);
            }			 
        }    
        console.log('FINAL LIST'+JSON.stringify(finalpricebookentrieslist));
        component.set("v.pricebookentries",finalpricebookentrieslist);
        console.log('**VAl of opportunityStage'+component.get("v.opportunityStage"));
        console.log('**Val of desiredproduct'+component.get("v.IsDesiredProfiles"));
        console.log('**Val of profileName'+component.get("v.profileName"));
        
    },
    
	fetchPricebook : function(component,event,helper) {
		console.log('**fetch pricebook entered'); 		
        var action = component.get("c.getPricebook");
         action.setParams({          	                
                campaignId : String(component.get("v.campaignId")),
                vendorId : String(component.get("v.vendorId"))
        });	
        action.setCallback(this,function(response){
           
            var state = response.getState();
            console.log('**State val'+state);            
            console.log('**REsponse return val'+JSON.stringify(response.getReturnValue()));
            if(state==="SUCCESS"){
            	var result=response.getReturnValue();  
                console.log('&&&&&&&&&&&&'+result.message);
                if(result!=null && result.message!=null && result.message=='Got pricebook'){                   
                    console.log('**resutl.pricebookrec'+JSON.stringify(result.Pricebookrec));
                    component.set("v.pricebookandentries",result.Pricebookrec);
                    if(result.Pricebookrec!=null && result.Pricebookrec.PricebookEntries!=null 
                       && result.Pricebookrec.PricebookEntries!=undefined){
                    	component.set("v.pricebookentries",result.Pricebookrec.PricebookEntries);
                        component.set("v.Pricebookfound",true);
                         //ADDED PRICEBOOKPRESENT TO STORE THE VALUE OF PRICEBOOKID
                        component.set("v.pricebookrecordId",result.Pricebookrec.Id);
                        component.set("v.PricebookPresent",true);
                        component.set("v.EntriesValues",result.Pricebookrec.PricebookEntries);
                    }    
                    
                    if(result.Pricebookrec!=null && result.Pricebookrec.Accounting_Period__c!=null &&
                       result.Pricebookrec.Accounting_Period__c!=undefined && result.Pricebookrec.Accounting_Period__r.StartDate!=null){
                   		component.set("v.APbeginDate",result.Pricebookrec.Accounting_Period__r.StartDate);
                        console.log('**What is apbegindate val'+component.get("v.APbeginDate"));
                    }
                    if(result.Pricebookrec!=null && result.Pricebookrec.Accounting_Period__c!=null && result.Pricebookrec.Accounting_Period__c!=undefined
                        && result.Pricebookrec.Accounting_Period__r.EndDate!=null)
                    	component.set("v.APEndDate",result.Pricebookrec.Accounting_Period__r.EndDate);
                    	component.set("v.Ispricebookmessage",false);
                    //ADDED
            		component.set("v.ShowAddServices",true);
                    //ADDED FOR KIN-934
                    if(component.get("v.profileName")!=undefined && (component.get("v.profileName")=='System Administrator' || 
                       component.get("v.profileName")=='Fiscal Officer' || component.get("v.profileName")=='CSA Admin' )){
                    	component.set("v.Pricebookfound",false);
                    }    
                }
                else if(result!=null && result.message!=null && result.message=='Got PB without entry'){
                    console.log('**Enterd in 2nd');
                    component.set("v.pricebookandentries",'');
                    component.set("v.Ispricebookmessage",true); 
                    component.set("v.IsPricebookWithoutEntry",true);
                    component.set("v.PricebookIdWithoutEntry",result.Pricebookrec.Id);
                	component.set("v.messageText",'There are no active services for the pricebook associated with the selected vendor for the selected Parent Fiscal year. Please reach out to the Finance department to create the services for the pricebook. You will be able to continue by selecting a different vendor.');
                	console.log('**msgtext'+component.get("v.messageText"));
                    //ADDED
                    if(component.get("v.profileName")!=undefined && (component.get("v.profileName")=='System Administrator' || 
                       component.get("v.profileName")=='Fiscal Officer' || component.get("v.profileName")=='CSA Admin' )){
                    //if(component.get("v.profileName")!=undefined && (component.get("v.profileName")=='Fiscal Officer' || component.get("v.profileName")=='CSA Admin' )){ 
                    	component.set("v.ShowAddServices",true);
                        component.set("v.messageText",'There are no active pricebooks associated with the selected vendor for the selected Parent Fiscal year. Please click on add button to add a new service.');
                    }    
                    component.set("v.Pricebookfound",false);
                    
                     component.set("v.pricebookrecordId",'');
                     component.set("v.PricebookPresent",false);
                }
                else if(result!=null && result.message!=null && result.message=='No pricebook found'){
                    component.set("v.Ispricebookmessage",true); 
                    component.set("v.pricebookandentries",'');
                    component.set("v.messageText",'There are no active services for the pricebook associated with the selected vendor for the selected Parent Fiscal year. Please reach out to the Finance department to create the services for the pricebook. You will be able to continue by selecting a different vendor.');
            		//ADDED
            		if(component.get("v.profileName")!=undefined && (component.get("v.profileName")=='System Administrator' || 
                      component.get("v.profileName")=='Fiscal Officer' || component.get("v.profileName")=='CSA Admin' )){
                   /* if(component.get("v.profileName")!=undefined && (component.get("v.profileName")=='Fiscal Officer' || component.get("v.profileName")=='CSA Admin' )){ */
                    	component.set("v.ShowAddServices",true);
                        component.set("v.messageText",'There are no active pricebooks associated with the selected vendor for the selected Parent Fiscal year. Please click on add button to add a new service.');
                    }    
                    component.set("v.Pricebookfound",false);
                    component.set("v.pricebookrecordId",'');
                    component.set("v.PricebookPresent",false);
                }
            }
            else if(state==="ERROR"){
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown Error");
                }
                component.set("v.Ispricebookmessage",true);
                component.set("v.messageText",'Failed to retrieve the pricebook.');
                component.set("v.pricebookandentries",'');
            }
         });
        $A.enqueueAction(action);      
	},
     getCaseValue : function(component,event,helper,caseid){
    	var action = component.get("c.getCase"); 
        action.setParams({ caseId : caseid.toString(),accountId : component.get("v.vendorId")});
         action.setCallback(this,function(response){           
            var state = response.getState();
            console.log('**State val'+state);
            if(state==="SUCCESS"){
                 var result=response.getReturnValue(); 
                 console.log(JSON.stringify(result));
                if(result!=null)
                    component.set("v.CaseValue",result);
                else
                    component.set("v.CaseValue",'');
            }  
              else if(state==="ERROR"){
              	component.set("v.CaseValue",'');
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown Error");
                }
               
            }
         });
        $A.enqueueAction(action);     
    },
    cancelForm : function(component,event,helper){
        this.redirecttoRecord(component,event,helper,component.get("v.recordId"));
      
    },  
    validateForm : function(component,event,helper){
        var isnotCSABlock =false;
        var caseid = component.get("v.caseId");
        console.log('***validateform main'+component.get("v.POselectedServices").length);
       // if(component.get("v.POselectedServices").length<=0 && component.get("v.OpportunitiesLineItemsList").length<=0){
        if(component.get("v.POselectedServices")!=undefined && component.get("v.POselectedServices").length<=0){	
       		 component.set("v.ShowSpinner",false);
            component.set("v.POError",true);
        	this.showToast('Error','Error','Services cannot be empty.');         
        }
         //ADDED FOR INACTIVE CATEGORIES
        var IsCategoryInactive = false;
        if(component.get("v.POselectedServices")!=undefined && component.get("v.POselectedServices").length>0){
         	console.log('**Enterd in inactive categories check');  
            var services = component.get("v.POselectedServices");
            console.log('inactive services val'+JSON.stringify(services));
             for(var i=0 ; i< services.length ; i++){
                 if(services[i].categorySelection!=undefined && services[i].categorySelection.Inactive__c!=undefined && services[i].categorySelection.Inactive__c==true){
                  	component.set("v.ShowSpinner",false);
                    component.set("v.POError",true);
                    this.showToast('Error','Error','Services Category can not be Inactive'); 
                    IsCategoryInactive=true; 
                     break;
                 }
             }       
        }
        
        if(IsCategoryInactive==false){
            if(caseid=='' || caseid==undefined || caseid==null){
                component.set("v.ShowSpinner",false);
                component.set("v.POError",true);
                this.showToast('Error','Error','Please provide a Case');               
            }
            else if(component.get("v.campaignId")=='' || component.get("v.campaignId")==undefined 
             || component.get("v.campaignId")==null){
                component.set("v.ShowSpinner",false);
                component.set("v.POError",true);
                this.showToast('Error','Error','Please provide a Parent Fiscal Year');    
            }
            else if(component.get("v.vendorId")=='' || component.get("v.vendorId")==undefined 
                    || component.get("v.vendorId")==null){
                component.set("v.ShowSpinner",false);
                component.set("v.POError",true);
                this.showToast('Error','Error','Please provide a Vendor');     
            }
            else if(component.get("v.closeDate")=='' || component.get("v.closeDate")==undefined 
                    || component.get("v.closeDate")==null){
                component.set("v.ShowSpinner",false);
                component.set("v.POError",true);
                this.showToast('Error','Error','Please provide a Begin Date');     
            }
            else if(component.get("v.EndDate")=='' || component.get("v.EndDate")==undefined 
                    || component.get("v.EndDate")==null){
                component.set("v.POError",true);
                this.showToast('Error','Error','Please provide a End Date');     
            }
            else if(component.get("v.closeDate")!='' && component.get("v.closeDate")!=undefined && 
                   component.get("v.EndDate")!='' && component.get("v.EndDate")!=undefined && 
                    component.get("v.closeDate") > component.get("v.EndDate")){
                component.set("v.ShowSpinner",false);
                component.set("v.POError",true);
                this.showToast('Error','Error','POSO End date should be greater than POSO Begin date');
            }
            else if(component.get("v.EndDate")!='' && component.get("v.EndDate")!=undefined && component.get("v.APEndDate")!=undefined
                 && component.get("v.APEndDate")!='' && component.get("v.EndDate") > component.get("v.APEndDate")) {
                component.set("v.ShowSpinner",false);
                 component.set("v.POError",true);
                 this.showToast('Error','Error','POSO End date should lies between POSO Begin Date and its Fiscal Year End date');
            }
           
           /* else if(component.get("v.closeDate")!='' && component.get("v.closeDate")!=undefined &&
                 component.get("v.APbeginDate")!='' && component.get("v.APEndDate")!='' && 
                  component.get("v.APbeginDate")!=undefined && component.get("v.APEndDate")!=undefined &&   
                  (component.get("v.closeDate") < component.get("v.APbeginDate") || 
                   component.get("v.closeDate") > component.get("v.APEndDate"))){
                component.set("v.ShowSpinner",false);
                 component.set("v.POError",true);
                 this.showToast('Error','Error','PO Begin Date should lie between its Parent Fiscal Year dates');
            } */
            //KIN -952 STARTS HERE
            else if(component.get("v.closeDate")!='' && component.get("v.closeDate")!=undefined 
                 && component.get("v.APEndDate")!='' && component.get("v.APEndDate")!=undefined &&             
                 component.get("v.closeDate") > component.get("v.APEndDate")){
                 component.set("v.ShowSpinner",false);
                 component.set("v.POError",true);
                 this.showToast('Error','Error','POSO Begin Date should not be greater than its Fiscal Year End date');
            }
             else if(component.get("v.closeDate")!='' && component.get("v.closeDate")!=undefined && component.get("v.POselectedServices")!=undefined && component.get("v.POselectedServices").length>0 ){
                 console.log('**entered in close enddate'+component.get("v.APbeginDate"));
                 var services = component.get("v.POselectedServices");
                 var isCSA= false;
                 for(var i=0 ; i< services.length ; i++){
                    if(services[i].categorySelection!=undefined && services[i].categorySelection.Category_Type__c!=undefined && 
                       services[i].categorySelection.Category_Type__c=='CSA'){
                        console.log('**Enterd in iscsa');
                        isCSA=true; 
                        break;
                    }                  
                 }
                 if(isCSA==true && component.get("v.APbeginDate")!='' && component.get("v.APbeginDate")!=undefined
                    && component.get("v.closeDate") < component.get("v.APbeginDate")){
                    component.set("v.ShowSpinner",false);
                    component.set("v.POError",true);
                    this.showToast('Error','Error','POSO Begin Date should lie between its Fiscal Year Begin and End dates');
                 }
                 else{
                     //component.set("v.ShowSpinner",false);
                    component.set("v.POError",false);
                     isnotCSABlock=true;
                 }
               console.log('reached here');                  
             }
            //KIN -952 ENDS HERE
            
            if(isnotCSABlock==true && caseid!='' && caseid!=undefined && caseid!=null && component.get("v.POselectedServices")!=undefined && component.get("v.POselectedServices").length>0){
                /*component.set("v.OasisShow",false);
                component.set("v.IsStudentIdRequired",false);
                component.set("v.Referal",'');
                component.set("v.OasisClinetId",''); */ 
                this.updateCaseRollback(component,event,helper);
                var action = component.get("c.getCase"); 
                action.setParams({ caseId : caseid.toString(),accountId : component.get("v.vendorId")});
                action.setCallback(this,function(response){           
                var state = response.getState();
                console.log('**State val'+state);
                if(state==="SUCCESS"){
                     var result=response.getReturnValue(); 
                     console.log(JSON.stringify(result));
                    if(result!=null){
                        if(result.caseval!=null && result.caseval!=undefined)
                    		component.set("v.CaseValue",result.caseval);
                         if(result.accountVal!= null && result.accountVal!=undefined && result.accountVal.Tax_ID__c!=null);
                    	component.set("v.TaxId",result.accountVal.Tax_ID__c);
                        var caseval=component.get("v.CaseValue");
                        console.log('**VAl of caeval'+JSON.stringify(caseval));
                        if(caseval!=null && caseval!=undefined){
                            if(caseval.Primary_Contact__r!=undefined && caseval.Primary_Contact__r.FirstName !=undefined)
                                component.set("v.ConFirstName",caseval.Primary_Contact__r.FirstName);
                            if(caseval.Primary_Contact__r!=undefined && caseval.Primary_Contact__r.LastName !=undefined)
                                component.set("v.ConLastName",caseval.Primary_Contact__r.LastName);
                            if(caseval.DOB__c!=undefined && caseval.DOB__c!=null)
                                component.set("v.DOB",caseval.DOB__c);
                            if(caseval.Race__c!=undefined)
                                component.set("v.Race",caseval.Race__c);
                            if(caseval.Hispanic_Ethnicity__c !=undefined)
                                component.set("v.HispanicEthnicity",caseval.Hispanic_Ethnicity__c);
                            if(caseval.Gender_Preference__c !=undefined)
                                component.set("v.Gender",caseval.Gender_Preference__c);
                            if(caseval.OASIS_Client_ID__c!=undefined)
                                component.set("v.OasisClinetId",caseval.OASIS_Client_ID__c);
                            if(caseval.Title_IVE_Eligibility__c!=undefined)
                                component.set("v.TitleIVE", caseval.Title_IVE_Eligibility__c);
                            if(caseval.DSM_V_Indicator_ICD_10_1__c!=undefined)
                                component.set("v.DSM", caseval.DSM_V_Indicator_ICD_10_1__c);
                             if(caseval.Clinical_Medication_Indicator_1__c!=undefined)
                                component.set("v.Clinical", caseval.Clinical_Medication_Indicator_1__c);
                             if(caseval.Medicaid_Indicator_1__c!=undefined)
                                component.set("v.Medical", caseval.Medicaid_Indicator_1__c);
                             if(caseval.Referral_Source__c!=undefined)
                                component.set("v.Referal", caseval.Referral_Source__c);
                             if(caseval.Autism_Flag_1__c!=undefined)
                                component.set("v.Autism", caseval.Autism_Flag_1__c);
                              if(caseval.ssn__c!=undefined)
                            component.set("v.SSN",caseval.ssn__c);
                            if(caseval.Student_Identifier__c !=undefined)
                                component.set("v.studentidentifier",caseval.Student_Identifier__c);
                            if(result.optionValues!=undefined && result.optionValues.length >0){
                                
                                var values=[];
                                var options = result.optionValues;
                                //GETTING SCROLLBAR ON ITEMS
                                var items = [];
                                var item = {                           
                                    "label": "---None---",
                                    "value": ''
                                };
                                items.push(item);
                                for (var i = 0; i < options.length; i++) {
                                    var item = {                           
                                        "label": options[i].label,
                                        "value": options[i].value.toString()
                                    };
                                    items.push(item);
                                }
                              
                                component.set("v.referaloptions",result.optionValues); 
                            }     
                        }
                        
                        var services = component.get("v.POselectedServices");
                        for(var i=0 ; i< services.length ; i++){
                           
                            if(services[i].categorySelection!=undefined && services[i].categorySelection.Category_Type__c!=undefined && 
                               services[i].categorySelection.Category_Type__c=='CSA' && ((component.get("v.TaxId")==undefined || component.get("v.TaxId")=='') || caseval.Primary_Contact__c==undefined || caseval.Primary_Contact__r.LastName==null || caseval.Primary_Contact__r.FirstName==null
                             || caseval.DOB__c==null || caseval.Gender_Preference__c==null || caseval.Race__c==null || caseval.Autism_Flag_1__c==null ||
                              caseval.DSM_V_Indicator_ICD_10_1__c ==null || caseval.Clinical_Medication_Indicator_1__c==null || caseval.Medicaid_Indicator_1__c==null || 
                            caseval.Referral_Source__c==null ||  caseval.Hispanic_Ethnicity__c ==null || caseval.Primary_Contact__r.LastName==undefined || caseval.Primary_Contact__r.FirstName==undefined
                            || caseval.DOB__c==undefined || caseval.Gender_Preference__c==undefined || caseval.Race__c==undefined || caseval.Autism_Flag_1__c==undefined ||
                             caseval.DSM_V_Indicator_ICD_10_1__c ==undefined || caseval.Clinical_Medication_Indicator_1__c==undefined || caseval.Medicaid_Indicator_1__c==undefined || 
                              caseval.Referral_Source__c==undefined ||  caseval.Hispanic_Ethnicity__c ==undefined || caseval.Title_IVE_Eligibility__c==undefined || caseval.Title_IVE_Eligibility__c=='' ||
                            (services[i].SPTselected!=undefined && (services[i].SPTselected=='6' || services[i].SPTselected=='7' || services[i].SPTselected=='18') &&
                         (caseval.Student_Identifier__c==undefined || caseval.Student_Identifier__c==null || caseval.Student_Identifier__c=='')) || ((caseval.Student_Identifier__c==undefined || caseval.Student_Identifier__c==null || caseval.Student_Identifier__c=='' ) &&
                         (caseval.ssn__c==undefined || caseval.ssn__c==null || caseval.ssn__c=='')) || (services[i].MandateSelected!=undefined && (services[i].MandateSelected=='2' || services[i].MandateSelected=='3' 
                           || services[i].MandateSelected=='6' ||  services[i].MandateSelected=='7' || services[i].MandateSelected=='8') && (caseval.OASIS_Client_ID__c==undefined || caseval.OASIS_Client_ID__c==null || caseval.OASIS_Client_ID__c==''))  
                          )){
                            if(services[i].MandateSelected!=undefined && (services[i].MandateSelected=='2' || services[i].MandateSelected=='3' || services[i].MandateSelected=='6' ||  services[i].MandateSelected=='7'
                            || services[i].MandateSelected=='8') && (caseval.OASIS_Client_ID__c==undefined || caseval.OASIS_Client_ID__c==null || caseval.OASIS_Client_ID__c==''))
                                component.set("v.OasisShow",true);
                            
                            if (services[i].SPTselected!=undefined && (services[i].SPTselected=='6' || services[i].SPTselected=='7' || services[i].SPTselected=='18') &&
                         	(caseval.Student_Identifier__c==undefined || caseval.Student_Identifier__c==null || caseval.Student_Identifier__c==''))
                                component.set("v.IsStudentIdRequired",true);
                                
                                console.log('***eNTERED HERE');
                                component.set("v.ShowSpinner",false);
                                component.set("v.POError",true);
                                component.set("v.IsCSACase",true);
                                //component.set("v.showCSACase",true);
                               // this.showIncreaseToast('Error','Error','For CSA category,Please check if any of these fields -Firstname, Lastname, DOB, Gender,Race, Hispanic Ethnicity, Autism, DSM V Indicator/ICD-10, Clinical Medication Indicator, Medicaid Indicator, Referral Source are not filled on associated case.');
                                //break;
                            } 
                           /* if(services[i].categorySelection!=undefined && services[i].categorySelection.Category_Type__c!=undefined && services[i].categorySelection.Category_Type__c=='IVE'){
                                 //TO NOT ALLOW IVE CATEGORY IF TITLE IVE OF CASE IS NO
                                if(caseval!=undefined && caseval.Title_IVE_Eligibility__c!=undefined && caseval.Title_IVE_Eligibility__c=='1'){
                                    component.set("v.ShowSpinner",false);
                                    component.set("v.POError",true);
                                    this.showIncreaseToast('Error','Error','You cannot use category of type IV-E if its associated case title IV-E eligibility field has no value.');
                                    break;    
                                } */
                                if (services[i].categorySelection!=undefined && services[i].categorySelection.Category_Type__c!=undefined && services[i].categorySelection.Category_Type__c=='IVE' &&
                                    (caseval.Primary_Contact__c==undefined || caseval.Primary_Contact__r.LastName==null || caseval.Primary_Contact__r.FirstName==null
                                  || caseval.DOB__c==null || caseval.Gender_Preference__c==null || caseval.Race__c==null || caseval.OASIS_Client_ID__c==null ||
                                  caseval.Hispanic_Ethnicity__c ==null || caseval.Primary_Contact__r.LastName==undefined || caseval.Primary_Contact__r.FirstName==undefined
                                   || caseval.DOB__c==undefined || caseval.Gender_Preference__c==undefined || caseval.Race__c==undefined || caseval.OASIS_Client_ID__c==undefined ||
                                    caseval.Hispanic_Ethnicity__c ==undefined || caseval.Title_IVE_Eligibility__c==undefined || caseval.Title_IVE_Eligibility__c=='' ||
                                    (component.get("v.TaxId")==undefined || component.get("v.TaxId")=='') || caseval.Medicaid_Indicator_1__c==undefined || caseval.Medicaid_Indicator_1__c=='' ||
                                caseval.Medicaid_Indicator_1__c==null ||  ((caseval.Student_Identifier__c==undefined || caseval.Student_Identifier__c==null || caseval.Student_Identifier__c=='' ) &&
                         		(caseval.ssn__c==undefined || caseval.ssn__c==null || caseval.ssn__c=='')))){
                                    component.set("v.ShowSpinner",false);
                                    component.set("v.POError",true);
                                    component.set("v.isIVECase",true);
                                    //this.showIncreaseToast('Error','Error','FOR IVE category,Please check if any of these fields -Firstname, Lastname, DOB, Gender,Race, Hispanic Ethnicity or Oasis Client ID are not filled on associated case.');
                                    //break;
                                } 
                             /**CHECK IF BOTH ARE TRUE ***/
                            if(component.get("v.isIVECase")==true && component.get("v.IsCSACase")==true){
                                 this.showToast('Error','Error','To proceed, please populate the required fields on the Case Record.');
                                component.set("v.showCSACase",true);
                                 component.set("v.ShowSpinner",false);
                                 component.set("v.POError",true);
                                break;
                            }	
                               
                        } //FOR LOOP ENDS HERE
                        //FOR CSA CHECK 
                        if(component.get("v.POError")==true && component.get("v.IsCSACase")==true){
                             this.showToast('Error','Error','To proceed, please populate the required fields on the Case Record.');
                            component.set("v.showCSACase",true);
                                 component.set("v.ShowSpinner",false);
                                 component.set("v.POError",true);    
                        }
                        else if(component.get("v.POError")==true && component.get("v.isIVECase")==true){
    
                            component.set("v.showCSACase",true);
                                 component.set("v.ShowSpinner",false);
                                 component.set("v.POError",true);        
                        }
                        //SENDING FOR SAVE IF NO ERROR FOUND
                        if(component.get("v.POError")==false){
                            console.log('**VAl of save'+component.get("v.Save"));
                            if(component.get("v.Save")=='Save'){
                                this.savePO(component,event,helper,component.get("v.profileName"),false);
                            }
                            else
                               this.savePO(component,event,helper,component.get("v.profileName"),true); 
                        }
                    }    
                    else{
                         component.set("v.ShowSpinner",false);
                        component.set("v.POError",true);
                        this.showToast('Error','Error','Unable to find case record');
                    }   
                }  
                  else if(state==="ERROR"){
                        component.set("v.CaseValue",'');
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " + 
                                         errors[0].message);
                                 component.set("v.ShowSpinner",false);
                                 component.set("v.POError",true);
                                  this.showToast('Error','Error',errors[0].message);
                            }
                        } else {
                            console.log("Unknown Error");
                            component.set("v.ShowSpinner",false);
                            component.set("v.POError",true);
                            this.showToast('Error','Error','Unknown Error');
                        }               
                    }
                });
                $A.enqueueAction(action);  
            }
        }    
    },
    showToast : function(type, title, message) {
       
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : title,
            message: message,
            duration:'5000',
            key: 'info_alt',
            type: type,
            mode: 'pester'
        });
        toastEvent.fire();
    } ,
    showIncreaseToast : function(type, title, message) {
       
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : title,
            message: message,
            duration:' 10000',
            key: 'info_alt',
            type: type,
            mode: 'pester'
        });
        toastEvent.fire();
    } ,
     CalculatePOSOTotal : function(component,event,helper,selectedServices){
    	console.log('**Enterd in poso total');
        console.log(JSON.stringify(selectedServices));
        if(selectedServices!=undefined && selectedServices.length >0){
            var total=0;
            for(var i=0 ; i < selectedServices.length ; i++){
                if(selectedServices[i].UnitPrice!=undefined && selectedServices[i].quantity!=undefined){
                    total = total + (selectedServices[i].UnitPrice * selectedServices[i].quantity);
                }
            }
            component.set("v.TotalAmount",total);
        }
    },
    savePO : function(component,event,helper,profilename,submitforApproval){
        console.log('**Entred in savepo'+submitforApproval);
        component.set("v.IsSubmitForApproval",false);
         //component.set("v.ShowSpinner",false);
        var recordid = component.get("v.recordId");
        var POobject ={};
        var opportunity ={};
        var caseid = component.get("v.caseId");
        var campaignId=component.get("v.campaignId");
        var vendorId = component.get("v.vendorId");
        var beginDate = component.get("v.closeDate");
        var endDate = component.get("v.EndDate");
        var FipsId = component.get("v.FIPSId");
        var contractId = component.get("v.ContractId");
        console.log('***here');
        // KIN 989
        var empId = component.get("v.employeeId");
        //ends
        opportunity.Kinship_Case__c = caseid;
        opportunity.CampaignId=campaignId;
        opportunity.accountid = vendorId;
        opportunity.closedate = beginDate;
        opportunity.End_Date__c=endDate;  
        opportunity.Send_Email__c=component.get("v.SendEmail");
        // KIN 989
        opportunity.Caseworker_Name__c = empId;
        // ends
        if(component.get("v.POSONote")!='' && component.get("v.POSONote")!=undefined)
            opportunity.Description = component.get("v.POSONote");
        POobject.Opp = opportunity;
        console.log(POobject);
        console.log(JSON.stringify(POobject));
        var selectedoppitems = component.get("v.POselectedServices");
        console.log('$$$$$$$');
        console.log(JSON.stringify(selectedoppitems));
        var SelectedProducts =[];
       
        for(var i =0 ; i < selectedoppitems.length ; i++){
            var services = {};
            if(selectedoppitems[i].Pricebook2Id !=undefined)
            	POobject.pricebookid=selectedoppitems[i].Pricebook2Id;   
            if(selectedoppitems[i].Product2Id!=undefined)
            	services.Product2Id= selectedoppitems[i].Product2Id;
            if(selectedoppitems[i].Unit_Measure_Label__c !=undefined)
            	services.unitLabel=selectedoppitems[i].Unit_Measure_Label__c;
            if(selectedoppitems[i].Service_Unit_Measure__c!=undefined)
            	services.unitMeasure = selectedoppitems[i].Service_Unit_Measure__c;
            // KIN 1311
            if(selectedoppitems[i].noteTitle!=undefined)
            	services.noteTitle = selectedoppitems[i].noteTitle;
            if(selectedoppitems[i].noteContent!=undefined)
            	services.noteContent = selectedoppitems[i].noteContent;
            // 1311 ends
            services.UnitPrice=selectedoppitems[i].UnitPrice;
            services.quantity= selectedoppitems[i].quantity;
            if(selectedoppitems[i].Id!=undefined)
            	services.pricebookentryid=selectedoppitems[i].Id;
            services.categoryid=selectedoppitems[i].categorySelection.Id;
            if(selectedoppitems[i].recipient!=undefined && selectedoppitems[i].recipient!='')  
            	services.recipient=selectedoppitems[i].recipient;
            if(selectedoppitems[i].SPTNameselected!='' && selectedoppitems[i].SPTNameselected!=undefined)
                services.SPTNameselected=selectedoppitems[i].SPTNameselected;
            if(selectedoppitems[i].SPTselected!='' && selectedoppitems[i].SPTselected!=undefined)
                services.SPTselected=selectedoppitems[i].SPTselected;
            if(selectedoppitems[i].MandateSelected!='' && selectedoppitems[i].MandateSelected!=undefined)
                services.MandateSelected=selectedoppitems[i].MandateSelected;
            if(selectedoppitems[i].SPTselectedlabel!='' && selectedoppitems[i].SPTselectedlabel!=undefined)
                services.SPTselectedlabel=selectedoppitems[i].SPTselectedlabel;
            if(selectedoppitems[i].SPTNameselectedlabel!='' && selectedoppitems[i].SPTNameselectedlabel!=undefined)
                services.SPTNameselectedlabel=selectedoppitems[i].SPTNameselectedlabel;
            if(selectedoppitems[i].MandateSelectedlabel!='' && selectedoppitems[i].MandateSelectedlabel!=undefined)
                services.MandateSelectedlabel=selectedoppitems[i].MandateSelectedlabel;
            if(selectedoppitems[i].Description!='' && selectedoppitems[i].Description!=undefined)
                services.Description=selectedoppitems[i].Description;
            if(selectedoppitems[i].recipientIVE !=undefined && selectedoppitems[i].recipientIVE !='')
               services.recipientIvE = selectedoppitems[i].recipientIVE;
            if( selectedoppitems[i].Fund!=undefined && selectedoppitems[i].Fund!=''
               && selectedoppitems[i].categorySelection!=undefined && selectedoppitems[i].categorySelection.RecordType!=undefined &&
               selectedoppitems[i].categorySelection.RecordType.DeveloperName!=undefined && selectedoppitems[i].categorySelection.RecordType.DeveloperName=='LASER_Category'){
                console.log('**Entered in 572');
                services.FundVal = selectedoppitems[i].Fund;
            } 
            if( selectedoppitems[i].FundIVE!=undefined && selectedoppitems[i].FundIVE!='' && selectedoppitems[i].categorySelection.Category_Type__c!=undefined && selectedoppitems[i].categorySelection.Category_Type__c=='IVE'
               && selectedoppitems[i].categorySelection!=undefined){
              //selectedoppitems[i].categorySelection.RecordType!=undefined && selectedoppitems[i].categorySelection.RecordType.DeveloperName!=undefined && selectedoppitems[i].categorySelection.RecordType.DeveloperName=='LASER_Category'){
                console.log('**Entered in 572');
                services.FundVal = selectedoppitems[i].FundIVE;
            } 
            if(selectedoppitems[i].costcenterSelection!=undefined && selectedoppitems[i].costcenterSelection!=''){
                services.costcenter=selectedoppitems[i].costcenterSelection.Id;
            }
            if(selectedoppitems[i].opplineitemId!=undefined && selectedoppitems[i].opplineitemId!=null && 
              selectedoppitems[i].opplineitemId!='')                
            	services.oppitemid =selectedoppitems[i].opplineitemId;
             //ADDED FOR NEW SERVICES 
            if(selectedoppitems[i].IsNewService !=undefined)
                services.IsNewService =selectedoppitems[i].IsNewService;
             if(selectedoppitems[i].serviceProduct!= undefined ){
               if(selectedoppitems[i].serviceProduct.product!=undefined)
                 services.ProductSelectedId = selectedoppitems[i].serviceProduct.product.Id;
             	if(selectedoppitems[i].serviceProduct.UnitPrice!=undefined)
                 services.ProductSelectedUnitPrice = selectedoppitems[i].serviceProduct.UnitPrice;
             }
             services.ServiceName = selectedoppitems[i].Service_Name__c;
            // services.ServiceDescription = selectedoppitems[i].Service_Description__c;
        	SelectedProducts.push(services);
        }
        POobject.oppproduct =SelectedProducts; 
         if(component.get("v.PricebookPresent")!=undefined && 
          component.get("v.PricebookPresent")==true && component.get("v.pricebookrecordId")!=undefined &&
           component.get("v.pricebookrecordId")!=''){
            if(POobject.pricebookid=='' || POobject.pricebookid==null || POobject.pricebookid==undefined)
            	POobject.pricebookid = component.get("v.pricebookrecordId");
            //CHECK IF IT IS DELETED ENTRY AND ADDED AGAIN THAT MEANS PRICEBOOK ENTRY IS THERE 
            var pricebookentries = component.get("v.EntriesValues");
            console.log('****entries'+JSON.stringify(pricebookentries));
            console.log('***selectedproducts'+JSON.stringify(SelectedProducts));
            if(SelectedProducts!=undefined && SelectedProducts.length >0 && pricebookentries!=undefined && pricebookentries.length>0 ){
                console.log('**Entered in first if **');
                for(var i =0 ; i< pricebookentries.length ; i++){
                    for(var j=0; j< SelectedProducts.length ; j++){
                        if(SelectedProducts[j].IsNewService==true && SelectedProducts[j].ProductSelectedId == pricebookentries[i].Product2Id && 
                          SelectedProducts[j].ProductSelectedUnitPrice!=undefined && SelectedProducts[j].ProductSelectedUnitPrice!=null){
                            console.log('**matched');
                            SelectedProducts[j].EntryExist = true;
                            SelectedProducts[j].Product2Id = pricebookentries[i].Product2Id;
                            SelectedProducts[j].pricebookentryid=pricebookentries[i].Id;
                            break;
                        }
                    }
                }
                 POobject.oppproduct =SelectedProducts;
            }
        }
         console.log('**FINAL LIST'+JSON.stringify(POobject));
       var action = component.get("c.EditPO");
        action.setParams({          	                
            POwrapper : JSON.stringify(POobject),
            Profile : profilename,
            SubmitforApproval : submitforApproval,
            recordid : recordid,
            isNoEntryPricebook : component.get("v.IsPricebookWithoutEntry"),
            PricbeookIDForNoEntry : component.get("v.PricebookIdWithoutEntry"),
            IsCSAAdminProfile : component.get("v.IsCSAAdminProfile"),
            stagename : component.get("v.opportunityStage")
        });	
         action.setCallback(this,function(response){           
            var state = response.getState();
            console.log('**State val'+state);
            if(state==="SUCCESS"){
                component.set("v.ShowSpinner",false);
                var result=response.getReturnValue(); 
                 console.log(JSON.stringify(result));
                if(result.recordid!=null){
                    if(result.message =='record updated'){
                        console.log('@#@#@#@#');
                    	 this.showToast('Success','Success','Changes saved successfully!');                         
                    	//this.redirecttoRecord(component,event,helper,result.recordid );
                         //$A.get('e.force:refreshView').fire();
                         if(component.get("v.IsCSAAdminProfile")==true){
                             component.set("v.IsSubmitForApproval",true);
                             console.log('**Val of recordid'+result.recordid);
                             component.set("v.recordId",result.recordid);    
                         }
                    }
                    else {
                        this.showToast('Success','Success','PO has been updated and submitted for approval successfully!');    
                    	/***SUBMITTED OF APPROVAL PART COMMENTED TO SHOW ANOTHER SCREEN **/
                        component.set("v.IsSubmitForApproval",true);
                        console.log('**Val of recordid'+result.recordid);
                        component.set("v.recordId",result.recordid);
                    	/*this.redirecttoRecord(component,event,helper,result.recordid );
                    	$A.get('e.force:refreshView').fire(); */
                    }
                }
                
            }   
             else if(state=="ERROR"){
                 component.set("v.ShowSpinner",false);
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                 // KIN 280
                if(strErr.indexOf('Please select a case with Active Special Welfare Account')  !=-1){
                    strErr= strErr + 'OR Click on New SWA button.';
                }
                this.showToast('Error', 'Error', strErr);
                console.log('strErr '+strErr.indexOf('Please select a case with Active Special Welfare'));
                if(strErr.indexOf('Please select a case with Active Special Welfare Account')  !=-1){
                    component.set("v.showNewSWA",true);
                }
                console.log(component.get("v.showNewSWA"));
             }
        });
        $A.enqueueAction(action);
    },  
    redirecttoRecord : function(component,event,helper,recordid){
         var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": recordid,
            "slideDevName": "related"
        });
        navEvt.fire();
        
    },
    getRecordType : function(component,event,helper){
        console.log('recordId'+component.get("v.recordId"));
       var action=component.get("c.FetchRecordtypeName");
        action.setParams({          	                
            recordid : component.get("v.recordId")
        });	
         action.setCallback(this,function(response){           
            var state = response.getState();
            console.log('**State val'+state);
            if(state==="SUCCESS"){
                 var recordid = component.get("v.recordId");
                 var result=response.getReturnValue(); 
                console.log('*****'+JSON.stringify(result));
                 if(result!=null && result.DeveloperName!=null)
                     component.set("v.recordtypename",result.DeveloperName);
                if(result!=null && result.DeveloperName=='NPSP_Default'){
                    console.log('hiiiii');
                    console.log('***'+component.get("v.recordId"));
                    component.set("v.ispo",false);
                    component.set("v.isCA",true);
                    //this.callEditAction(component,event,helper);
                    
                }else if(result.DeveloperName=='Staff_Operations' || result.DeveloperName =='Report_of_Collection'){
                    window.location = '/lightning/r/Opportunity/' + recordid + '/view' ;
                    
                }
                else{
                    component.set("v.ispo",true); 
                    component.set("v.isCA",false);
            	//CALL CONTROLLER
           		  this.getInitialValues(component,event,helper);  
                }
            } 
             else{
                 component.set("v.ispo",true);  
            	//CALL CONTROLLER
           		  this.getInitialValues(component,event,helper);  
             }
         });
        $A.enqueueAction(action);       
    },
    fetchBeginDate : function(component,event,helper){
        console.log('**Entered in fetch begin date');
    	var action=component.get("c.FetchBeginDate");
         action.setParams({          	                
                campaignId : String(component.get("v.campaignId"))
        });	
         action.setCallback(this,function(response){           
            var state = response.getState();
            console.log('**State val'+state);
            if(state==="SUCCESS"){
                 var result=response.getReturnValue(); 
                 console.log(JSON.stringify(result));              
                if(result!=null && result.accountingPeriodId!=null)
                    component.set("v.campaignId",result.accountingPeriodId);
                if(result!=null && result.APstartDate!=null){
                    component.set("v.APbeginDate",result.APstartDate);
                    component.set("v.closeDate",result.APstartDate);
                }   
                if(result!=null && result.APendDate!=null)
                    component.set("v.APEndDate",result.APendDate);
            } 
             else{
                 component.set("v.APbeginDate",'');
                 component.set("v.closeDate",'');
                 component.set("v.APEndDate",'');
             }
        });
        $A.enqueueAction(action);     
    },
    callEditAction : function(component,event,helper){
        console.log('**enered in calleditaction');
        console.log(component.get("v.recordId"));
        
        var editRecordEvent = $A.get("e.force:editRecord");
        editRecordEvent.setParams({
            "recordId": component.get("v.recordId")
        });
        editRecordEvent.fire(); 
        
    },
     checkCaseRequiredFields : function(component,event,helper){
        var isCSA = component.get("v.IsCSACase");
        var isIVE = component.get("v.isIVECase");
        console.log('**VAl of hispanic'+component.get("v.HispanicEthnicity"));
        if(isCSA==true || isIVE==true){
            console.log('**inside iscsa nad isive');
            if(component.get("v.TaxId")==undefined || component.get("v.TaxId")==''){
            	component.set("v.ShowSpinner",false);
                component.set("v.CaseError",true);
        		this.showToast('Error','Error','Please provide vendor Tax Id.');       
            }
            if(component.get("v.ConFirstName")==undefined || component.get("v.ConFirstName")==''){
          		component.set("v.ShowSpinner",false);
                component.set("v.CaseError",true);
        		this.showToast('Error','Error','Please provide a Contact First Name.');     
            }
            else if(component.get("v.ConLastName")==undefined || component.get("v.ConLastName")==''){
          		component.set("v.ShowSpinner",false);
                component.set("v.CaseError",true);
        		this.showToast('Error','Error','Please provide a Contact Last Name.');     
            }
          else if(component.get("v.DOB")==undefined || component.get("v.DOB")==''){
          		component.set("v.ShowSpinner",false);
              component.set("v.CaseError",true);
        		this.showToast('Error','Error','Please provide a DOB.');     
            }
          else if(component.get("v.Gender")==undefined || component.get("v.Gender")==''){
          		component.set("v.ShowSpinner",false);
               component.set("v.CaseError",true);
        		this.showToast('Error','Error','Please provide a Gender.');     
            }
           else if(component.get("v.Race")==undefined || component.get("v.Race")==''){
          		component.set("v.ShowSpinner",false);
                component.set("v.CaseError",true);
        		this.showToast('Error','Error','Please provide a Race.');     
            }
           else if(component.get("v.HispanicEthnicity")==undefined || component.get("v.HispanicEthnicity")==''){
          		console.log('**entered in hispanic');
               component.set("v.ShowSpinner",false);
               component.set("v.CaseError",true);
        		this.showToast('Error','Error','Please provide a Hispanic Ethnicity.');     
            }
          else if(component.get("v.TitleIVE")==undefined || component.get("v.TitleIVE")==''){
               component.set("v.ShowSpinner",false);
              component.set("v.CaseError",true);
        		this.showToast('Error','Error','Please provide a Title IVE eligibility.');     
            }
         else if(component.get("v.Medical")==undefined || component.get("v.Medical")==''){
                    console.log('***entered in medical');
                   component.set("v.ShowSpinner",false);
                    component.set("v.CaseError",true);
                    this.showToast('Error','Error','Please provide Medical Indicator.');     
           }
           else if((component.get("v.studentidentifier")==undefined || component.get("v.studentidentifier")==null || component.get("v.studentidentifier")=='' ) &&
                         (component.get("v.SSN")==undefined || component.get("v.SSN")==null || component.get("v.SSN")=='')){
                	component.set("v.ShowSpinner",false);
                   component.set("v.CaseError",true);
                    this.showToast('Error','Error','Please provide either Student Identifier or SSN.');           
           } 
            else if(isIVE==true){
               if(component.get("v.TitleIVE")!=undefined && component.get("v.TitleIVE")!='' && 
                    component.get("v.TitleIVE")=='1'){ 
                        component.set("v.ShowSpinner",false);
                       component.set("v.CaseError",true);
                        this.showToast('Error','Error','You cannot use category of type IV-E if its associated case title IV-E eligibility field has no value.');
                             
                }
                else if(component.get("v.OasisClinetId")==undefined || component.get("v.OasisClinetId")==''){
                   component.set("v.ShowSpinner",false);
                    component.set("v.CaseError",true);
                    this.showToast('Error','Error','Please provide Oasis Client Id.');     
                }
                    
            }
           
            else if(isCSA==true){
                console.log('**Enterd in iscsa');
                if(component.get("v.DSM")==undefined || component.get("v.DSM")==''){
                   component.set("v.ShowSpinner",false);
                    component.set("v.CaseError",true);
                    this.showToast('Error','Error','Please provide DSM V Indicator/ ICD-10.');     
                }
               /* else if(component.get("v.Medical")==undefined || component.get("v.Medical")==''){
                    console.log('***entered in medical');
                   component.set("v.ShowSpinner",false);
                    component.set("v.CaseError",true);
                    this.showToast('Error','Error','Please provide Medical.');     
                } */
                else if(component.get("v.Referal")==undefined || component.get("v.Referal")==''){
                   component.set("v.ShowSpinner",false);
                    component.set("v.CaseError",true);
                    this.showToast('Error','Error','Please provide Referral Source.');     
                }
               else if(component.get("v.Clinical")==undefined || component.get("v.Clinical")==''){
                   component.set("v.ShowSpinner",false);
                   component.set("v.CaseError",true);
                    this.showToast('Error','Error','Please provide Clinical Medication Indicator.');     
                }
               else if(component.get("v.Autism")==undefined || component.get("v.Autism")==''){
                   component.set("v.ShowSpinner",false);
                   component.set("v.CaseError",true);
                    this.showToast('Error','Error','Please provide Autism Flag.');     
                }
                else if(component.get("v.IsStudentIdRequired")!=undefined && component.get("v.IsStudentIdRequired")==true &&
                 (component.get("v.studentidentifier")==undefined || component.get("v.studentidentifier")==null || component.get("v.studentidentifier")=='')){
                 	component.set("v.ShowSpinner",false);
                   component.set("v.CaseError",true);
                    this.showToast('Error','Error','Please provide Student Identifier.');    
                }
               /* else if((component.get("v.studentidentifier")==undefined || component.get("v.studentidentifier")==null || component.get("v.studentidentifier")=='' ) &&
                         (component.get("v.SSN")==undefined || component.get("v.SSN")==null || component.get("v.SSN")=='')){
                	component.set("v.ShowSpinner",false);
                   component.set("v.CaseError",true);
                    this.showToast('Error','Error','Please provide either Student Identifier or SSN.');           
                } */
                else if(component.get("v.OasisShow")!=undefined && component.get("v.OasisShow")==true && (component.get("v.OasisClinetId")==undefined || component.get("v.OasisClinetId")==null || component.get("v.OasisClinetId")=='')){
                   component.set("v.ShowSpinner",false);
                   component.set("v.CaseError",true);
                    this.showToast('Error','Error','Please provide Oasis client Id.');             
                }
              console.log('**Enterd in the end of else csa');      
               
            }
           
        	console.log('end of if');
        } 
    },
    updateCase : function(component,event,helper){
        var caseid = component.get("v.caseId");
        var casewrap ={};
        casewrap.contactFirstName = component.get("v.ConFirstName");
        casewrap.contactLastName = component.get("v.ConLastName");
        casewrap.DOB=component.get("v.DOB");
        casewrap.HispanicEthnicity=component.get("v.HispanicEthnicity");
        casewrap.Gender = component.get("v.Gender");
        casewrap.Race = component.get("v.Race");
        casewrap.OasisClinetId=component.get("v.OasisClinetId");
        casewrap.DSM=component.get("v.DSM");
        casewrap.Medical = component.get("v.Medical");
        casewrap.Referal = component.get("v.Referal");
        casewrap.Clinical=component.get("v.Clinical");
        casewrap.Autism=component.get("v.Autism");
        casewrap.titleIVE=component.get("v.TitleIVE");
        casewrap.studentIdentifierId = component.get("v.studentidentifier");
        casewrap.SSN= component.get("v.SSN");
        casewrap.taxId=component.get("v.TaxId"); 
     	 var action = component.get("c.UpdateCase"); 
        	action.setParams({ caseId : caseid.toString(),
                              caseWrapper : JSON.stringify(casewrap),
                             vendorId : component.get("v.vendorId")});
         	action.setCallback(this,function(response){           
            var state = response.getState();
            console.log('**State val of case update'+state);
            if(state==="SUCCESS"){
                 var result=response.getReturnValue(); 
                 console.log(JSON.stringify(result));
                if(result!=null && result.caserec!=null){
                    component.set("v.CaseValue",result.caserec);
                    component.set("v.ShowSpinner",false);                  
                    this.showToast('Success','Success','Case record has been updated.');  
                    component.set("v.showCSACase",false);
                    component.set("v.isIVECase",false);
                    component.set("v.IsCSACase",false);
                } 
                else if(result!=null && result.message=='Case contact doesnt exist'){
                	component.set("v.CaseValue",'');
                    component.set("v.ShowSpinner",false);                  
                    this.showToast('Error','Error','First associate a contact with the case');  
                    component.set("v.showCSACase",false);
                    component.set("v.isIVECase",false);
                    component.set("v.IsCSACase",false);
                }
                else {
                    component.set("v.CaseValue",'');
                    component.set("v.ShowSpinner",false);                  
                    this.showToast('Error','Error','Failed to update the case record.');  
                   
                }    
            }  
              else if(state==="ERROR"){
              	component.set("v.CaseValue",'');
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                        component.set("v.CaseValue",'');
                    	component.set("v.ShowSpinner",false);                  
                    	this.showToast('Error','Error',errors[0].message); 
                       
                    }
                } else {
                    console.log("Unknown Error");
                    component.set("v.CaseValue",'');
                    component.set("v.ShowSpinner",false);                  
                    this.showToast('Error','Error','Failed to update Case.');
                    
                    
                }
               
            }
         });
        $A.enqueueAction(action);             
    },
    updateCaseRollback : function(component,event,helper){
         component.set("v.showCSACase",false);
        component.set("v.ConFirstName",'');
        component.set("v.HispanicEthnicity",'');
        component.set("v.ConLastName",'');
        component.set("v.DOB",'');
        component.set("v.Gender",'');
        component.set("v.Race",'');
        component.set("v.OasisClinetId",'');
        component.set("v.DSM",'');
        component.set("v.Medical",'');
        component.set("v.Referal",'');
        component.set("v.Clinical",'');
        component.set("v.Autism",'');
        component.set("v.TitleIVE",'');
        component.set("v.OasisShow",false);
        component.set("v.SSN",'');
        component.set("v.studentidentifier",'');
        component.set("v.IsStudentIdRequired",false);
        component.set("v.isIVECase",false);
        component.set("v.IsCSACase",false);
        component.set("v.TaxId",'');
    },
    fetchServices : function(component,event,helper,PricebookEntries){
    	 var action = component.get("c.getOpportunityProducts");
         action.setParams({          	                
                campaignId : String(component.get("v.InitialCampaignId")),
                vendorId : String(component.get("v.InitialVendorId")),
             	recordid : component.get("v.recordId")
        });	
        action.setCallback(this,function(response){
            var state = response.getState();
            console.log('**State val'+state);
            if(state==="SUCCESS"){
                 var result=response.getReturnValue(); 
                 if(result.msg!=null && result.msg=='Records found' && result.oppproducts!=undefined
                    && result.oppproducts.length >0){
                     component.set("v.InitialServices",result.oppproducts);
                     //FOR SERVICES WHICH ARE NOT IN INITIAL LIST BASICALLY DELETED 
                     if(result.oppproducts!=undefined && component.get("v.InitialOpportunityLineitems")!=undefined
                       && result.oppproducts.length >0 && component.get("v.InitialOpportunityLineitems").length >0){
                         var initialopplist = component.get("v.InitialOpportunityLineitems");
                         var finallist =[];
                         for(var i =0 ; i < result.oppproducts.length ; i++){
                             for(var j =0 ; j< initialopplist.length; j++){
                                 if(initialopplist[j].opplineitemId == result.oppproducts[i].Id){
                                 	finallist.push(initialopplist[j]);    
                                 }   
                             }
                            
                         }
                        component.set("v.InitialOpportunityLineitems",finallist);
                         console.log('&&&&&&&&&&$$$'+JSON.stringify(finallist));
                         var finalpricebookentries =[];
                         if(finallist.length >0){
                             for(var i = 0 ; i<finallist.length ; i++){
                                 finalpricebookentries.push(finallist[i]);
                             }   
                         }
                         if(PricebookEntries!=undefined && PricebookEntries.length >0){
                             console.log('**Val of pricebookentries'+JSON.stringify(PricebookEntries));
                             for(var i =0 ; i< PricebookEntries.length ; i++){
                                 var isFound=false;
                                 for(var j=0 ; j< finallist.length ; j++){
                                     console.log('**finallist'+JSON.stringify(finallist));
                                      if(PricebookEntries[i].Product2Id== finallist[j].Product2Id && 
                       					PricebookEntries[i].Id == finallist[j].Id) {
                                          console.log('**enterd in if');
                                          isFound=true;
                                       		break;   
                                      }   
                                 }
                                if(isFound==false)
                                	finalpricebookentries.push(PricebookEntries[i]); 
                             }
                         }
                         //FOR ALREADY CREATED OPPORTUNITY ITEMS LIST
                         if(finallist.length >0){
                           component.set("v.alreadyselectedServices",finallist);            
                           component.set("v.POselectedServices",finallist);
                           component.set("v.IsSave",true);
                         }    
                         component.set("v.pricebookentries",finalpricebookentries);    
                         
                     }
                     
                 }
                
            }     
            else {
                component.set("v.pricebookentries",PricebookEntries);    
            }
         });
        $A.enqueueAction(action);      
        
    },
    fetchPricebookInitial : function(component,event,helper){
    	console.log('**fetch pricebook entered'); 		
        var action = component.get("c.getPricebook");
         action.setParams({          	                
                campaignId : String(component.get("v.campaignId")),
                vendorId : String(component.get("v.vendorId"))
        });	
        action.setCallback(this,function(response){
           
            var state = response.getState();
            console.log('**State val'+state);            
            console.log('**REsponse return val'+JSON.stringify(response.getReturnValue()));
            if(state==="SUCCESS"){
            	var result=response.getReturnValue();  
                console.log('&&&&&&&&&&&&'+result.message);
                if(result!=null && result.message!=null && result.message=='Got pricebook'){                   
                    console.log('**resutl.pricebookrec'+JSON.stringify(result.Pricebookrec));
                    component.set("v.pricebookandentries",result.Pricebookrec);
                    if(result.Pricebookrec!=null && result.Pricebookrec.PricebookEntries!=null 
                       && result.Pricebookrec.PricebookEntries!=undefined){
                         this.fetchServices(component,event,helper,result.Pricebookrec.PricebookEntries);
                    	//component.set("v.pricebookentries",result.Pricebookrec.PricebookEntries);
                        component.set("v.Pricebookfound",true);
                          //ADDED PRICEBOOKPRESENT TO STORE THE VALUE OF PRICEBOOKID
                        component.set("v.pricebookrecordId",result.Pricebookrec.Id);
                        component.set("v.PricebookPresent",true);
                        component.set("v.EntriesValues",result.Pricebookrec.PricebookEntries);
                    }    
                    
                    if(result.Pricebookrec!=null && result.Pricebookrec.Accounting_Period__c!=null &&
                       result.Pricebookrec.Accounting_Period__c!=undefined && result.Pricebookrec.Accounting_Period__r.StartDate!=null){
                   		component.set("v.APbeginDate",result.Pricebookrec.Accounting_Period__r.StartDate);
                        console.log('**What is apbegindate val'+component.get("v.APbeginDate"));
                    }
                    if(result.Pricebookrec!=null && result.Pricebookrec.Accounting_Period__c!=null && result.Pricebookrec.Accounting_Period__c!=undefined
                        && result.Pricebookrec.Accounting_Period__r.EndDate!=null)
                    	component.set("v.APEndDate",result.Pricebookrec.Accounting_Period__r.EndDate);
                    	component.set("v.Ispricebookmessage",false);
                    //ADDED
            		component.set("v.ShowAddServices",true);
                    //ADDED FOR KIN-934
                    if(component.get("v.profileName")!=undefined && (component.get("v.profileName")=='System Administrator' || 
                       component.get("v.profileName")=='Fiscal Officer' || component.get("v.profileName")=='CSA Admin' )){
                    	component.set("v.Pricebookfound",false);
                    }    
                }
                else if(result!=null && result.message!=null && result.message=='Got PB without entry'){
                    console.log('**Enterd in 2nd');
                    component.set("v.pricebookandentries",'');
                    component.set("v.Ispricebookmessage",true); 
                    component.set("v.IsPricebookWithoutEntry",true);
                    component.set("v.PricebookIdWithoutEntry",result.Pricebookrec.Id);
                	component.set("v.messageText",'There are no active services for the pricebook associated with the selected vendor for the selected Parent Fiscal year. Please reach out to the Finance department to create the services for the pricebook. You will be able to continue by selecting a different vendor.');
                	console.log('**msgtext'+component.get("v.messageText"));
                    //ADDED
                    if(component.get("v.profileName")!=undefined && (component.get("v.profileName")=='System Administrator' || 
                       component.get("v.profileName")=='Fiscal Officer' || component.get("v.profileName")=='CSA Admin' )){
                    //if(component.get("v.profileName")!=undefined && (component.get("v.profileName")=='Fiscal Officer' || component.get("v.profileName")=='CSA Admin' )){ 
                    	component.set("v.ShowAddServices",true);
                        component.set("v.messageText",'There are no active pricebooks associated with the selected vendor for the selected Parent Fiscal year. Please click on add button to add a new service.');
                    }    
                    component.set("v.Pricebookfound",false);
                     component.set("v.pricebookrecordId",'');
                     component.set("v.PricebookPresent",false);
                }
                else if(result!=null && result.message!=null && result.message=='No pricebook found'){
                    component.set("v.Ispricebookmessage",true); 
                    component.set("v.pricebookandentries",'');
                    component.set("v.messageText",'There are no active services for the pricebook associated with the selected vendor for the selected Parent Fiscal year. Please reach out to the Finance department to create the services for the pricebook. You will be able to continue by selecting a different vendor.');
            		//ADDED
            		if(component.get("v.profileName")!=undefined && (component.get("v.profileName")=='System Administrator' || 
                      component.get("v.profileName")=='Fiscal Officer' || component.get("v.profileName")=='CSA Admin' )){
                   /* if(component.get("v.profileName")!=undefined && (component.get("v.profileName")=='Fiscal Officer' || component.get("v.profileName")=='CSA Admin' )){ */
                    	component.set("v.ShowAddServices",true);
                        component.set("v.messageText",'There are no active pricebooks associated with the selected vendor for the selected Parent Fiscal year. Please click on add button to add a new service.');
                    }    
                    component.set("v.Pricebookfound",false);
                     component.set("v.pricebookrecordId",'');
                     component.set("v.PricebookPresent",false);
                }
            }
            else if(state==="ERROR"){
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown Error");
                }
                component.set("v.Ispricebookmessage",true);
                component.set("v.messageText",'Failed to retrieve the pricebook.');
                component.set("v.pricebookandentries",'');
            }
         });
        $A.enqueueAction(action);       
    }
})