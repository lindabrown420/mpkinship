({
    doInit : function(component, event, helper) {       
        console.log('**Entered in doinit'); 
        	helper.getRecordType(component,event,helper);        	           
    },
    //KIN 1367
    handleUploadFinished : function(component,event,helper){
        var uploadedFiles = event.getParam("files");
        console.log("Files uploaded : " + uploadedFiles.length);

        // Get the file name
        uploadedFiles.forEach(file =>{
            console.log(file.name);
        });
    },
    //KIN 228
    sendCasetoChild : function(component,event,helper){
        var caseid = component.find("Caseauraid").get("v.value");
        console.log(caseid);
        var objCompB = component.find('compchild');
        if(component.find('compchild'))
        objCompB.sampleMethod(JSON.parse(JSON.stringify(caseid))[0]);
    	//component.set("v.caseId",JSON.parse(JSON.stringify(caseid))[0]);
	},
   CampaingdataChanged: function(component,event,helper){
         var vendorid = component.get("v.vendorId");
        component.set("v.campaignId",'');
        component.set("v.POselectedServices",[]);
        component.set("v.IsSave",false);
        component.set("v.APbeginDate",'');
        component.set("v.APEndDate",'');
        component.set("v.pricebookentries",[]);
        component.set("v.InitialServices",[]);
        component.set("v.alreadyselectedServices",[]);
        component.set("v.Pricebookfound",false);
        component.set("v.ShowAddServices",false);
        component.set("v.pricebookandentries",[]);
        component.set("v.Ispricebookmessage",false);
        component.set("v.IsPricebookWithoutEntry",false);
        component.set("v.PricebookIdWithoutEntry",'');
        component.set("v.PricebookPresent",false);
        component.set("v.EntriesValues",'[]');
        component.set("v.pricebookrecordId",'');
        var changedcampval = event.getParam("dataChange");       
        console.log('9999'+JSON.stringify(changedcampval));
         if(changedcampval!='' && changedcampval!=undefined && changedcampval.Id!=undefined){
             console.log('**VAl of campaignid'+changedcampval);
         	component.set("v.campaignId",changedcampval.Id); 
             if(changedcampval!=undefined && changedcampval.FIPS__c!=undefined)
                 component.set("v.FIPSId",changedcampval.FIPS__c);
               console.log('**VAl of fips'+component.get("v.FIPSId"));
              helper.fetchBeginDate(component,event,helper);
         }
        var campaignId = component.get("v.campaignId");
        //CHECK FOR PRICEBOOK IF CAMPAIGNID IS NOT NULL AND VENDORID IS ALSO NOT NULL
        if(campaignId!='' && campaignId!=undefined && vendorid!='' && vendorid!=undefined){
            console.log('**Entered in bring pricebook here in campaign'+campaignId);  
            //CHECK IF VENDOR ID AND CAMPAIGN ID IS SAME AS ON LOAD
            if(campaignId==component.get("v.InitialCampaignId") && vendorid==component.get("v.InitialVendorId")){
                helper.fetchServices(component,event,helper);
                helper.fetchPricebookInitial(component,event,helper);
            }
            else
            	helper.fetchPricebook(component,event,helper);
               
        }
     },     
    CampaignChange : function(component,event,helper){        
        console.log('**Entered here');
        var campaignId = component.find("campid").get("v.value");
        component.set("v.campaignId",campaignId);        
        var vendorid = component.get("v.vendorId");
        component.set("v.POselectedServices",[]);
        component.set("v.APbeginDate",'');
        component.set("v.APEndDate",'');
        component.set("v.pricebookentries",[]);
        component.set("v.alreadyselectedServices",[]);
        component.set("v.pricebookandentries",[]);
        component.set("v.ShowAddServices",false);
        component.set("v.IsSave",false);
        component.set("v.Ispricebookmessage",false);
        component.set("v.IsPricebookWithoutEntry",false);
        component.set("v.PricebookIdWithoutEntry",'');
        //FETCH APBEGIN AND END DATE
        helper.fetchBeginDate(component,event,helper);
        //CHECK FOR PRICEBOOK IF CAMPAIGNID IS NOT NULL AND VENDORID IS ALSO NOT NULL
        if(campaignId!='' && campaignId!=undefined && vendorid!='' && vendorid!=undefined){
            console.log('**Entered in bring pricebook here in campaign');             
            helper.fetchPricebook(component,event,helper);
        }
        
    },
    VendorChange : function(component,event,helper){
        console.log('**Entered in vendor change');
        var vendorid = event.getSource().get('v.value');       
        component.set("v.vendorId",vendorid);        
        var campaignId=component.get("v.campaignId");
        component.set("v.POselectedServices",[]);
        component.set("v.pricebookentries",[]);
        component.set("v.pricebookandentries",[]);
        component.set("v.InitialServices",[]);
        component.set("v.alreadyselectedServices",[]);
        component.set("v.Pricebookfound",false);
        component.set("v.ShowAddServices",false);
        component.set("v.IsSave",false);
        component.set("v.Ispricebookmessage",false);
        component.set("v.IsPricebookWithoutEntry",false);
        component.set("v.PricebookIdWithoutEntry",'');
        component.set("v.InitialServices",[]);
        component.set("v.PricebookPresent",false);
        component.set("v.EntriesValues",'[]');
        component.set("v.pricebookrecordId",'');
        //CHECK FOR PRICEBOOK IF CAMPAIGN AND VENDOR ID BOTH ARE NOT NULL
        if(vendorid!='' && vendorid!=undefined && campaignId!='' && campaignId!=undefined){
            console.log('**Entered in bring pricebook here in vendor');
            //CHECK IF VENDOR ID AND CAMPAIGN ID IS SAME AS ON LOAD
            if(campaignId==component.get("v.InitialCampaignId") && vendorid==component.get("v.InitialVendorId")){
               
                helper.fetchPricebookInitial(component,event,helper);
            }
            else 
            	helper.fetchPricebook(component,event,helper);   
               
        } 
    },
    cancelpo :function(component,event,helper){
        helper.cancelForm(component,event,helper);
    },
    handleCancel:function(component,event,helper){
          helper.cancelForm(component,event,helper);
    },
    SelectedServices : function(component,event,helper){
     	var selectedServices = event.getParam("SelectedServices");
        console.log(JSON.stringify(selectedServices));
        component.set("v.POselectedServices",selectedServices);
        var ShowSave = event.getParam("IsShow");
        console.log(ShowSave);  
        console.log('**length of selectedservices'+ selectedServices.length);
        if(ShowSave==false && selectedServices!=undefined && selectedServices.length>0){
            component.set("v.IsSave",true);
            //CALCULATE TOTAL AMOUNT
            helper.CalculatePOSOTotal(component,event,helper,selectedServices);
        }    
        else
            component.set("v.IsSave",false);
    },
    CallSave :function(component,event,helper){
        component.set("v.Save",'Save');
        console.log('**Enterd in save method');      
        component.set("v.IsCSAAdminProfile",false);
        console.log(JSON.stringify(component.get("v.POselectedServices")));
        component.set("v.POError",false);
        component.set("v.ShowSpinner",true);
    	helper.validateForm(component,event,helper);  
       /* if(component.get("v.POError")==false){
            helper.savePO(component,event,helper,component.get("v.profileName"),false);
        } */
    },
    CallSaveAndSubmitforAproval : function(component,event,helper){
        component.set("v.Save",'Not Save');
        component.set("v.IsCSAAdminProfile",false);
        component.set("v.POError",false);
        component.set("v.ShowSpinner",true);
    	helper.validateForm(component,event,helper);  
        /* if(component.get("v.POError")==false){
            helper.savePO(component,event,helper,component.get("v.profileName"),true);
        } */
    },
    CallSaveApproved : function(component,event,helper){
    	component.set("v.Save",'Save');
        //ADDED FOR FISCAL OFFICER TOO
        component.set("v.IsCSAAdminProfile",true);
        component.set("v.POError",false);
        component.set("v.ShowSpinner",true);
    	helper.validateForm(component,event,helper);      
    },
    cancelpoForNotDraft:function(component,event,helper){
        var recordid =component.get("v.recordId");
    	helper.redirecttoRecord(component,event,helper,recordid);
	},
    update :function(component,event,helper){
        var recordtypename =component.get("v.recordtypename");
        //if(recordtypename!='' && recordtypename!=undefined && recordtypename!='NPSP_Default')
        if(recordtypename!='' && recordtypename!=undefined)
        	$A.get('e.force:refreshView').fire();
    },
    handleSendEmail : function(component,event,helper){
        var sendEmailvalue=component.find("sendEmailId").get("v.value");
        console.log('*******send email val'+sendEmailvalue);
        component.set("v.SendEmail",sendEmailvalue);
    },
    closeModel : function(component,event,helper){
        component.set("v.openModal",false);
    },
   openCSA :function(component,event,helper){
        var indexval = event.getSource().get("v.name"); 
        component.set("v.openModal",true);
       component.set("v.IsCSA",true);
        console.log('**opencsa'+indexval);
        var pricebookentries =component.get("v.opportunitywraplineitems");
        if(pricebookentries!=undefined && pricebookentries.length >0 && indexval!=undefined){
            var selectedRecord = pricebookentries[indexval];
            console.log('**Selectedrecord'+JSON.stringify(selectedRecord));
            if(selectedRecord!=undefined && selectedRecord!=''){
                if(selectedRecord.opplineitem !=undefined){
                     if(selectedRecord.costcenterids!='' && selectedRecord.costcenterids!=undefined){
                     component.set("v.CostCenterIds",selectedRecord.costcenterids);
                     
                 }
                 if(selectedRecord.opplineitem.Cost_Center__c!='' && selectedRecord.opplineitem.Cost_Center__c!=undefined){
                     	 var costcenterSelection={"Id":selectedRecord.opplineitem.Cost_Center__c,"Name":selectedRecord.opplineitem.Cost_Center__r.Name};
            		component.set("v.costcenterSelection",costcenterSelection);
                    
                 } 
                 else{
                    component.set("v.costcenterSelection",''); 
                 }
                if(selectedRecord.costcenterids!='' && selectedRecord.costcenterids!=undefined){
                     
                 	component.set("v.showCostCenterLookup",true);
                 }
                
                    component.set("v.MandateSelected",selectedRecord.opplineitem.Mandate_Type_Code__c);
                    console.log('**VAl of selected spt'+ selectedRecord.opplineitem.Service_Placement_Type_Code__c);
                    component.set("v.SPTselected",selectedRecord.opplineitem.Service_Placement_Type_Code__c);
                    console.log('&&&&'+component.get("v.SPTselected"));
                    component.set("v.SPTselectedlabel",selectedRecord.opplineitem.Service_Placement_Type__c);
                    component.set("v.SPTNameselected",selectedRecord.opplineitem.Service_Name_Code_Value__c);
                    if(selectedRecord.opplineitem.Service_Description_Other__c!=null && selectedRecord.opplineitem.Service_Description_Other__c!=undefined)
                        component.set("v.Description",selectedRecord.opplineitem.Service_Description_Other__c);
              		if(selectedRecord.opplineitem.Parent_Recipient__c!=undefined)
                        component.set("v.recipient",selectedRecord.opplineitem.Parent_Recipient__c);
                }    
                if(selectedRecord.MandateNames!=undefined && selectedRecord.MandateNames.length>0){
                    var mandatenames = selectedRecord.MandateNames;
                    var items=[];
                    var item = {                           
                        "label": "---None---",
                        "value": ''
                    };
                    items.push(item);
                    
                    for (var i = 0; i < mandatenames.length; i++) {
                        var item = {                           
                            "label": mandatenames[i].label,
                            "value": mandatenames[i].value.toString()
                        };
                        items.push(item);
                    } 
                    
                    component.set("v.Mandateoptions",items);                    
                }  
                if(selectedRecord.Names!=undefined && selectedRecord.Names.length>0){
                    var sptvalues=[];
                    var sptnames = selectedRecord.Names;
                    //GETTING SCROLLBAR ON ITEMS
                    var items = [];
                    var item = {                           
                        "label": "---None---",
                        "value": ''
                    };
                    items.push(item);
                    for (var i = 0; i < sptnames.length; i++) {
                        var item = {                           
                            "label": sptnames[i].label,
                            "value": sptnames[i].value.toString()
                        };
                        items.push(item);
                    }                      
                    component.set("v.SPToptions",items);  
                } 
                if(selectedRecord.SPTNamesCodes!=undefined && selectedRecord.SPTNamesCodes.length>0){
                    var sptvalues=[];
                    var sptnamecodes = selectedRecord.SPTNamesCodes;
                    //GETTING SCROLLBAR ON ITEMS
                    var items = [];
                    var item = {                           
                        "label": "---None---",
                        "value": ''
                    };
                    items.push(item);
                    for (var i = 0; i < sptnamecodes.length; i++) {
                        var item = {                           
                            "label": sptnamecodes[i].label,
                            "value": sptnamecodes[i].value.toString()
                        };
                        items.push(item);
                    }                      
                    component.set("v.SPTNames",items);  
                }
            }
        }    
    },
    
    openIVE : function(component,event,helper){
    	var indexval = event.getSource().get("v.name"); 
        component.set("v.openModal",true);
        console.log('**opencsa'+indexval);
        component.set("v.IsCSA",false);
        component.set("v.IsFund",false);
        var pricebookentries =component.get("v.opportunitywraplineitems");
        if(pricebookentries!=undefined && pricebookentries.length >0 && indexval!=undefined){
            var selectedRecord = pricebookentries[indexval];
            console.log('**Selectedrecord'+JSON.stringify(selectedRecord));
            if(selectedRecord!=undefined && selectedRecord!=''){
                if(selectedRecord.opplineitem !=undefined){
                    component.set("v.recipientIVE",selectedRecord.opplineitem.Parent_Recipient__c);
                    component.set("v.FundIVE",selectedRecord.opplineitem.Fund__c);
                    if(selectedRecord.costcenterids!='' && selectedRecord.costcenterids!=undefined){
                     component.set("v.CostCenterIds",selectedRecord.costcenterids);
                     
                 }
                 if(selectedRecord.opplineitem.Cost_Center__c!='' && selectedRecord.opplineitem.Cost_Center__c!=undefined){
                     	 var costcenterSelection={"Id":selectedRecord.opplineitem.Cost_Center__c,"Name":selectedRecord.opplineitem.Cost_Center__r.Name};
            		component.set("v.costcenterSelection",costcenterSelection);
                    
                 } 
                 else{
                    component.set("v.costcenterSelection",''); 
                 }
                if(selectedRecord.costcenterids!='' && selectedRecord.costcenterids!=undefined){
                     
                 	component.set("v.showCostCenterLookup",true);
                 } 
                } 
               
               
            }
        }        
    },
    openFund : function(component,event,helper){
    	var indexval = event.getSource().get("v.name"); 
        component.set("v.openModal",true);
        console.log('**opencsa'+indexval);
        component.set("v.IsCSA",false);
        component.set("v.IsFund",true);
        var pricebookentries =component.get("v.opportunitywraplineitems");
        if(pricebookentries!=undefined && pricebookentries.length >0 && indexval!=undefined){
            var selectedRecord = pricebookentries[indexval];
            console.log('**Selectedrecord'+JSON.stringify(selectedRecord));
            if(selectedRecord!=undefined && selectedRecord!=''){
                if(selectedRecord.opplineitem !=undefined){
                    component.set("v.Fund",selectedRecord.opplineitem.Fund__c);
                    if(selectedRecord.costcenterids!='' && selectedRecord.costcenterids!=undefined){
                     component.set("v.CostCenterIds",selectedRecord.costcenterids);
                     
                 }
                 if(selectedRecord.opplineitem.Cost_Center__c!='' && selectedRecord.opplineitem.Cost_Center__c!=undefined){
                     	 var costcenterSelection={"Id":selectedRecord.opplineitem.Cost_Center__c,"Name":selectedRecord.opplineitem.Cost_Center__r.Name};
            		component.set("v.costcenterSelection",costcenterSelection);
                    
                 } 
                 else{
                    component.set("v.costcenterSelection",''); 
                 }
                if(selectedRecord.costcenterids!='' && selectedRecord.costcenterids!=undefined){
                     
                 	component.set("v.showCostCenterLookup",true);
                 }
                    
                }    
               
            }
        }            
    },
     closeCaseModel : function(component,event,helper){
        component.set("v.showCSACase",false);
        component.set("v.ConFirstName",'');
        component.set("v.HispanicEthnicity",'');
        component.set("v.ConLastName",'');
        component.set("v.DOB",'');
        component.set("v.Gender",'');
        component.set("v.Race",'');
        component.set("v.OasisClinetId",'');
        component.set("v.DSM",'');
        component.set("v.Medical",'');
        component.set("v.Referal",'');
        component.set("v.Clinical",'');
        component.set("v.Autism",'');
        component.set("v.TitleIVE",'');
        component.set("v.isIVECase",false);
        component.set("v.IsCSACase",false);
     	
    },
    SaveUpdatedCase :function(component,event,helper){
        component.set("v.ShowSpinner",true);
        component.set("v.CaseError",false);
        helper.checkCaseRequiredFields(component,event,helper);
        console.log('***enteed in afterr helper line');
        console.log('**CAse error val'+ component.get("v.CaseError"));
        if(component.get("v.CaseError") == false){
            console.log('**enterd here in caseerror no');
            helper.updateCase(component,event,helper);
        } 
    },
    //KIN 1311
    opennewNote : function (component, event, helper) {
        var cmpTarget = component.find('NoteModalbox');
		var cmpBack = component.find('NoteModalbackdrop');
		$A.util.addClass(cmpTarget, 'slds-fade-in-open');
		$A.util.addClass(cmpBack, 'slds-backdrop--open');
    },
    closeNoteModal : function(component,event,helper){
        component.set("v.note",{'sobjectType': 'ContentNote','Title': '','Content': ''})
        var cmpTarget = component.find('NoteModalbox');
		var cmpBack = component.find('NoteModalbackdrop');
		$A.util.removeClass(cmpBack,'slds-backdrop--open');
		$A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
    },
    createNote : function(component,event,helper){
        var newNote = component.get("v.note");
        console.log('newNote'+JSON.stringify(newNote));
        if(component.get("v.note").Title == "" || component.get("v.note").Title ==null) {
            helper.showToast('Error', 'Error', 'Please Fill Title of Note');
                
            }
        else{
        var action = component.get("c.createNoteRecord");
        action.setParams({
            nt : newNote,
            prntId : component.get("v.recordId")
        });
        action.setCallback(this,function(a){
            //get the response state
            var state = a.getState();
            //check if result is successfull
            if(state == "SUCCESS"){
                //Reset Form
                var not = {'sobjectType': 'ContentNote',
                                    'Title': '',
                                    'Content': ''
                                   };
                //resetting the Values in the form
                component.set("v.note",not);
                //KIN 1683
                component.find('noteButton').set("v.iconName","utility:check");
                var cmpTarget = component.find('NoteModalbox');
                var cmpBack = component.find('NoteModalbackdrop');
                $A.util.removeClass(cmpBack,'slds-backdrop--open');
                $A.util.removeClass(cmpTarget, 'slds-fade-in-open');
            } else if(state == "ERROR"){
                console.log('Error in calling server side action');
            }
        });
        //adds the server-side action to the queue        
        $A.enqueueAction(action);
        }
        
    },
    // ends KIN 1311
    // KIN 280
    newSWA : function (component, event, helper) {
        console.log('@@'+component.get("v.case"));
        var nameFieldValue = component.find("caseonSWA").set("v.value", component.get("v.caseId"));
        /*var createRecordEvent = $A.get("e.force:createRecord");
        /*var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "Special_Welfare_Account__c",
            "defaultFieldValues": {
            'Case_KNP__c' : component.get("v.caseId")
        	}
        });
        createRecordEvent.fire();*/
        var cmpTarget = component.find('SWAModalbox');
		var cmpBack = component.find('Modalbackdrop');
		$A.util.addClass(cmpTarget, 'slds-fade-in-open');
		$A.util.addClass(cmpBack, 'slds-backdrop--open');
	},
    handleload :function(component,event,helper){
        var fValue1 = component.find("initialbal").set("v.value", 0);
    },
    handleSuccess :function(component,event,helper){
        var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "type" :"Success",
                "title": "Success!",
                "message": "The SWA record has been created successfully."
            });
            toastEvent.fire();
        component.set("v.showNewSWA",false);
        var cmpTarget = component.find('SWAModalbox');
		var cmpBack = component.find('Modalbackdrop');
		$A.util.removeClass(cmpBack,'slds-backdrop--open');
		$A.util.removeClass(cmpTarget, 'slds-fade-in-open');
    },
    handleSubmit :function(component,event,helper){
       
    },
    closeModal : function(component,event,helper){
        var cmpTarget = component.find('SWAModalbox');
		var cmpBack = component.find('Modalbackdrop');
		$A.util.removeClass(cmpBack,'slds-backdrop--open');
		$A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
    },
    // ends KIN 280
     POSODetails :function(component,event,helper){
    	helper.redirecttoRecord(component,event,helper,component.get("v.recordId") );
        $A.get('e.force:refreshView').fire();            
    },
    printPOSO :function(component,event,helper){
        var recordid=component.get("v.recordId");
        console.log('**VAl of recordid'+recordid);
        
        var ddpid=$A.get("$Label.c.POSO_print_ddpid");
        var deliveryid=$A.get("$Label.c.Poso_print_delivery_id");
        var urlval='/apex/ProcessDdp?ddpId='+ddpid+'&deliveryOptionId='+deliveryid+'&ids='+recordid+'&sObjectType=Opportunity';
        console.log('**VAl of urlval'+urlval);
        var urlEvent = $A.get("e.force:navigateToURL");
        
        urlEvent.setParams({
          "url": urlval
        });
        urlEvent.fire();
                
     },
    newPOSO :function(component,event,helper){
   		var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:CreatePurchaseOrder"
            
        });
        evt.fire();             
    },
    printVI :function(component,event,helper){
    	var recordid=component.get("v.recordId");
        console.log('**VAl of recordid'+recordid);
        
        var ddpid=$A.get("$Label.c.Print_VI_ddpID");
        var deliveryid=$A.get("$Label.c.Print_VI_delivery_Option");
        var urlval='/apex/ProcessDdp?ddpId='+ddpid+'&deliveryOptionId='+deliveryid+'&ids='+recordid+'&sObjectType=Opportunity';
        console.log('**VAl of urlval'+urlval);
        var urlEvent = $A.get("e.force:navigateToURL");
        
        urlEvent.setParams({
          "url": urlval
        });
        urlEvent.fire();            
    },
    POSOandVI :function(component,event,helper){
    	var recordid=component.get("v.recordId");
        console.log('**VAl of recordid'+recordid);
        
        var ddpid=$A.get("$Label.c.POSO_AND_VI_Print_ddpId");
        var deliveryid=$A.get("$Label.c.POSO_and_VI_Print_deliveryId");
        var urlval='/apex/ProcessDdp?ddpId='+ddpid+'&deliveryOptionId='+deliveryid+'&ids='+recordid+'&sObjectType=Opportunity';
        console.log('**VAl of urlval'+urlval);
        var urlEvent = $A.get("e.force:navigateToURL");
        
        urlEvent.setParams({
          "url": urlval
        });
        urlEvent.fire();            
    }         

})