({
	doInit: function (component, event, helper) {
        
         let recordId = component.get('v.recordId');
        var action = component.get("c.UpdateState");
        action.setParams({ 'recordId' : recordId });
        action.setCallback(this, function(response) {
             var state = response.getState();
             $A.get('e.force:closeQuickAction').fire();  
             if(state==="SUCCESS"){
                 
             	 var result=response.getReturnValue(); 
                 console.log('**VAl ofresult'+result);
                 if(result!=null && result.message=='Record Updated'){
                     helper.showToast('Success','Success','Record has been approved successfully!');   
                      $A.get('e.force:refreshView').fire();   
                 }
             }
             else if(state=="ERROR"){
                 var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                     errors[0].message);
                        }
                    }    
                 helper.showToast('Error','Error','Record has not been approved successfully!');
                  $A.get('e.force:refreshView').fire();   
             }    
                
        });
        $A.enqueueAction(action);
    }
    
         
       
})