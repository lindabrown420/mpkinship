({
	showToast : function(type, title, message) {
    
        if(type.toUpperCase() == 'ERROR'){
            type = 'info';
        }
        
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : title,
            message: message,
            duration:' 5000',
            key: 'info_alt',
            type: type,
            mode: 'pester'
        });
        toastEvent.fire();
    } 
})