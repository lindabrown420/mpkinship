({
    doInit: function(component, event, helper) {
        console.log('doInit');

        var recordTypeId = component.get("v.pageReference").state.recordTypeId;
        console.log('recordTypeId: '+recordTypeId);
        var action = component.get("c.getRT_Name");
        action.setParams({
            rtId: recordTypeId
        })
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('rt name: '+response.getReturnValue());
                
                //component.set("v.recordTypeName",response.getReturnValue());
                
                if(response.getReturnValue() == 'Events' || response.getReturnValue() == 'Tax Reporting Period'){
                    component.set('v.showFlow',false);
                    var createRecordEvent = $A.get("e.force:createRecord");
                    createRecordEvent.setParams({
                        "entityApiName": "Campaign",
                        "recordTypeId": recordTypeId                            
                    });
                    createRecordEvent.fire(); 
                }
                else{
                    component.set("v.showCustomScreen",true);
                }
            }
        });
        $A.enqueueAction(action);

    }
})