({
    showToast : function(type, title, message) {
        let strTitle = title.split('\n').join('');
        let strMessage = message.split('\n').join('');
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : strTitle,
            message: strMessage,
            duration:' 4000',
            key: 'info_alt',
            type: type, // success , error, info
            mode: 'sticky'//'pester'
        });
        toastEvent.fire();
    },
    getData : function(component) {
        let action = component.get("c.getListFile");
        let recordId = component.get("v.recordId");
        action.setParams({
            strId: recordId
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let data = response.getReturnValue();
                if(data) {
                    component.set("v.data", data);
                    component.set('v.downloadurl', data[0].url)
                }
            }
        });
        $A.enqueueAction(action);
    },
    updateCSVFile : function(component, file) {
        if(file) {
            component.set('v.Spinner', true);
            let action = component.get("c.updateData");
            action.setParams({
                strVD: file.contentVersionId,
                pulicDate: component.get("v.publicDate")
            });
            action.setCallback(this, function(response) {
                let state = response.getState();
                if (state === "SUCCESS") {
                    let data = response.getReturnValue();
                    if(data) {
                        this.getData(component);
                    } else {
                        this.showToast('error', 'The file format is incorrect.', 'Please upload the correct .csv file.');
                    }
                }
                component.set('v.Spinner', false);
            });
            $A.enqueueAction(action);
            /*let reader = new FileReader();
            reader.readAsText(file, "UTF-8");
            reader.onload = function(evt) {
                let data = evt.target.result;
                if(data.split('\n').length <= 1) {
                    alert('Please select a right csv format.');
                } else {
                    component.set('v.Spinner', true);
                    let action = component.get("c.updateData");
                    action.setParams({
                        strData: data
                    });
                    action.setCallback(this, function(response) {
                        let state = response.getState();
                        if (state === "SUCCESS") {
                            let data = response.getReturnValue();
                            if(data) {
                                this.getData(component);
                            }
                        }
                        component.set('v.Spinner', false);
                    });
                    $A.enqueueAction(action);
                }
            }*/
        }
        
    }
})