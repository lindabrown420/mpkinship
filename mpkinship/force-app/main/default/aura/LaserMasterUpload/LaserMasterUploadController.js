({
    init : function(component, event, helper) {
        component.set('v.columns', [
            {label: 'File Name', fieldName: 'url', type: 'url', typeAttributes: { 
                label: {
                    fieldName: 'fileName'
                },
                target: '_blank'
            }},
            {label: 'Created By', fieldName: 'createdBy', type: 'text'},
            {label: 'Created Date', fieldName: 'createdDate', type: 'date',typeAttributes: {  
                                                                                    day: 'numeric',  
                                                                                    month: 'short',  
                                                                                    year: '2-digit',  
                                                                                    hour: '2-digit',  
                                                                                    minute: '2-digit',  
                                                                                    second: '2-digit',  
                                                                                    hour12: true}},
            {label: 'Effective Date', fieldName: 'publicDate', type: 'date',typeAttributes: {  
                                                                                        day: 'numeric',  
                                                                                        month: 'short',  
                                                                                        year: 'numeric'}}
        ]);
        helper.getData(component);
    },
    handleUploadFinished: function (component, event, helper) {
        // Get the list of uploaded files
        let uploadedFiles = event.getParam("files");
        //alert("Files uploaded : " + uploadedFiles.length);

        // Get the file name
        uploadedFiles.forEach(file => console.log(file.name));

        //helper.getData(component);
        helper.updateCSVFile(component, uploadedFiles[0]);
    },
    downloadfile : function (component, event, helper) {
        window.open(component.get('v.downloadurl'), '_blank');
    },
    downloadTemplate : function (component, event, helper) {
        window.open('https://alleghanydss.my.salesforce.com/sfc/servlet.shepherd/document/download/0693d0000007NwAAAU?operationContext=S1', '_blank');
    },
    datechanged: function (component, event, helper) {
        let publicDate = component.get('v.publicDate');
        if(publicDate) {
            component.set('v.disabled', false);
        } else {
            component.set('v.disabled', true);
        }
    }
})