({
    init : function(component, event, helper) {
        component.set('v.columns', [
            {label: 'Field', fieldName: 'fieldName', type: 'text'},
            {label: 'Old Value', fieldName: 'oldValue', type: 'text'},
            {label: 'New Value', fieldName: 'newValue', type: 'text'},
            {label: 'Action', fieldName: 'action', type: 'text'},
            {label: 'Created By', fieldName: 'createdBy', type: 'text'},
            {label: 'Created Date', fieldName: 'createdDate', type: 'date',typeAttributes: {  
                                                                                    day: 'numeric',  
                                                                                    month: 'short',  
                                                                                    year: '2-digit',  
                                                                                    hour: '2-digit',  
                                                                                    minute: '2-digit',  
                                                                                    second: '2-digit',  
                                                                                    hour12: true}}
        ]);

        helper.getData(component);
    },
    refreshHistory: function(component, event, helper) {
        helper.getData(component);
    },
    closeModel: function(component, event, helper) {
        component.set('v.isOpen', false);
    },
    openModel: function(component, event, helper) {
        component.set('v.isOpen', true);
    }
})