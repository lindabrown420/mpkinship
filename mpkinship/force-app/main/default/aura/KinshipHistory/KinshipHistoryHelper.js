({
    getData : function(component) {
        component.set('v.Spinner', true);
        let action = component.get("c.getListHistory");
        let recordId = component.get("v.recordId");
        action.setParams({
            strId: recordId
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let data = response.getReturnValue();
                if(data) {
                    component.set("v.data", data);
                }
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    }
})