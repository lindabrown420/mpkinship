({
    init : function(component, event, helper) {
        component.set('v.Spinner', true);
        let action = component.get("c.initData");
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let data = response.getReturnValue();
                if(data) {
                    data = helper.refactorData(component, data, '');
                    component.set("v.budgetData", data);
                }else{
                    helper.showToast('error', 'Error', 'Please contact your Administrator for more details.');
                }
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                console.log(strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    campLookupSearch : function(component, event, helper) {
        const lookupComponent = event.getSource();
        const serverSearchAction = component.get('c.kinshipSearchLookup');
        /*let recordTypeId = component.get('v.recordTypeId');
        let budgetData = component.get('v.budgetData');
        let type = '';
        if(recordTypeId === budgetData.rtDSS) {
            type = 'DSS';
        } else if(recordTypeId === budgetData.rtCSA) {
            type = 'CSA';
        } else if(recordTypeId === budgetData.rtLocal) {
            type = 'Local';
        }*/
        serverSearchAction.setParam('anOptionalParam', 'Campaign');
        lookupComponent.search(serverSearchAction);
    },
    clearCampErrorsOnChange: function(component, event, helper) {
        const selection = component.get('v.campSelection');
        const errors = component.get('v.campErrors');

        let formData = event.getParam("formData");
        if(formData) {
            component.set('v.Spinner', true);
            let action = component.get("c.getCurrentBudgetData");
            action.setParams({
                'strId': formData.recordId
            });
            action.setCallback(this, function(response) {
                let state = response.getState();
                if (state === "SUCCESS") {
                    let currentBudget = response.getReturnValue();
                    let budgetData = component.get('v.budgetData');
                    budgetData.lstCurrentBudget = currentBudget.lstCurrentBudget;
                    budgetData.accountPeriod = formData.recordId;
                    component.set('v.recordType', formData.subtitle);
                    let data = helper.refactorData(component, budgetData, formData.subtitle);
                    if(formData.subtitle === 'CSA'){
                        component.set('v.isShowBaseBudget', true);
                    } else {
                        component.set('v.isShowBaseBudget', false);
                    }
                    component.set('v.budgetData', data);
                } else {
                    const errors = response.getError();
                    let strErr = '';
                    if(errors) {
                        if(errors[0] && errors[0].message){
                            strErr = errors[0].message;
                        }
                    }
                    console.log(strErr);
                }
                component.set('v.Spinner', false);
            });
            $A.enqueueAction(action);
        } else {
            component.set('v.isShowCostCenter', false);
        }
        if (selection.length && errors.length) {
            component.set('v.campErrors', []);
        }
    },
    ccLookupSearch : function(component, event, helper) {
        const lookupComponent = event.getSource();
        const serverSearchAction = component.get('c.kinshipSearchLookup');
        serverSearchAction.setParam('anOptionalParam', 'Center');
        lookupComponent.search(serverSearchAction);
    },
    clearCCErrorsOnChange: function(component, event, helper) {
        const selection = component.get('v.ccSelection');
        const errors = component.get('v.ccErrors');
        if (selection.length && errors.length) {
            component.set('v.ccErrors', []);
        }
        let budgetData = component.get('v.budgetData');
        let data = helper.refactorData(component, budgetData, 'Local');
        component.set('v.budgetData', data);
    },
    handleCancel: function(component, event, helper) {
        helper.gobacktoListView(component);
    },
    handleSave: function(component, event, helper) {
        let budgetData = component.get("v.budgetData");
        let isBaseBudget = component.get('v.isBaseBudget');
        if(isBaseBudget) {
            budgetData = helper.refactorData(component, budgetData, 'CSA', true);
        }

        if(!budgetData.accountPeriod){
            helper.showToast('info', 'Missing Data', 'Please choose the Accounting Period.');
            return;
        }

        for(let i = 0 ; i < budgetData.lstChild.length ; i++) {
            if(!budgetData.lstChild[i].initialBudget && budgetData.lstChild[i].initialBudget !== 0){
                budgetData.lstChild[i].initialBudget = 0;
            }
        }

        /*for(let i = 0 ; i < budgetData.lstChild.length ; i++) {
            if(!budgetData.lstChild[i].initialBudget || budgetData.lstChild[i].initialBudget === 0){
                helper.showToast('info', 'Missing Data', 'Please input Initial Budget.');
                return;
            }
        }*/

        // call save Data
        component.set('v.Spinner', true);
        let action = component.get("c.createBudgets");
        action.setParams({
            'data': budgetData
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let strmessage = response.getReturnValue();
                if(strmessage){
                    helper.showToast('error', 'Error', strmessage);
                }else{
                    helper.gobacktoListView(component);
                }
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                console.log(strErr);
                helper.showToast('error', 'Error', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    handleTypeChange: function(component, event, helper) {
        let rtBudget = component.get('v.rtBudget');
        let data = helper.refactorData(component, component.get('v.budgetData'), rtBudget);
        component.set('v.budgetData', data);
    },
    handleAmountChange: function(component, event, helper) {
        let total = 0;
        let data = component.get('v.budgetData');
        for(let i = 0 ; i < data.lstChild.length ; i++){
            total += helper.parseDecimal(data.lstChild[i].initialBudget);
        }
        component.set('v.total', total);
    }
})