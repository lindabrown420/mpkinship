({
    showToast : function(type, title, message) {
        let strTitle = title.split('\n').join('');
        let strMessage = message.split('\n').join('');
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : strTitle,
            message: strMessage,
            duration:' 4000',
            key: 'info_alt',
            type: type, // success , error, info
            mode: 'pester'//'sticky'
        });
        toastEvent.fire();
    },
    gobacktoListView: function(component){
        let navEvent = $A.get("e.force:navigateToList");
        navEvent.setParams({
            "listViewId": null,
            "listViewName": "Current_Budget_Balance",
            "scope": "Budgets__c"
        });
        navEvent.fire();
    },
    refactorData: function(component, data, recordType, isSave){
        let total = 0;
        data.lstChild = [];
        if(recordType === 'DSS') {
            for(let i = 0 ; i < data.lstBudgetLine.length ; i++){
                let strId = '';
                let amount = 0;
                for(let j = 0 ; j < data.lstCurrentBudget.length ; j++) {
                    if(data.lstBudgetLine[i].Id === data.lstCurrentBudget[j].Laser_Budget_Code__c) {
                        strId = data.lstCurrentBudget[j].Id;
                        amount = data.lstCurrentBudget[j].Initial_Budget__c;
                        total += amount;
                    }
                }
                data.lstChild.push({'id': strId, 'name':data.lstBudgetLine[i].Name, 'accountPeriod':'','initialBudget': amount, 'description': data.lstBudgetLine[i].Budget_Line_Description__c, 'recordTypeId': data.rtDSS,'budgetLine': data.lstBudgetLine[i].Id,'localityCode':'', 'csaCate': ''});
            }
            component.set('v.isShowCostCenter', false);
        } else if(recordType === 'CSA') {
            let isBaseBudget = component.get('v.isBaseBudget');
            let baseAmount = 0;
            if(isBaseBudget) {
                let baseBudgetNumber = component.get('v.baseBudgetNumber');
                if(isSave && !baseBudgetNumber) {
                    this.showToast('info', 'Missing Data', 'Please input Base Budget.');
                    return;
                }
                baseAmount = baseBudgetNumber / data.lstCSACate.length;
            }
            for(let i = 0 ; i < data.lstCSACate.length ; i++){
                let strId = '';
                let amount = baseAmount;
                if(!isBaseBudget){
                    for(let j = 0 ; j < data.lstCurrentBudget.length ; j++) {
                        if(data.lstCSACate[i].id === data.lstCurrentBudget[j].Category__c) {
                            strId = data.lstCurrentBudget[j].Id;
                            amount = data.lstCurrentBudget[j].Initial_Budget__c;
                            total += amount;
                        }
                    }
                }
                data.lstChild.push({'id': strId, 'name':data.lstCSACate[i].Name, 'accountPeriod':'','initialBudget': amount, 'description': data.lstCSACate[i].Category_Account_Description__c, 'recordTypeId': data.rtCSA,'budgetLine': '','localityCode':'', 'csaCate': data.lstCSACate[i].Id});
            }
            component.set('v.isShowCostCenter', false);
        } else if(recordType === 'Local') {
            component.set('v.isShowCostCenter', true);
            let costCenter = component.get("v.ccSelection");
            for(let i = 0 ; i < data.lstLocality.length ; i++){
                let strId = '';
                let amount = 0;
                for(let j = 0 ; j < data.lstCurrentBudget.length ; j++) {
                    if(data.lstLocality[i].Id === data.lstCurrentBudget[j].Locality_Account_Code_Name__c) {
                        strId = data.lstCurrentBudget[j].Id;
                        amount = data.lstCurrentBudget[j].Initial_Budget__c;
                        total += amount;
                    }
                }
                if(costCenter.length === 0 || costCenter[0].id === data.lstLocality[i].costCenter){
                    data.lstChild.push({'id': strId, 'name':data.lstLocality[i].localityAccountNumber, 'accountPeriod':'','initialBudget': amount, 'description': data.lstLocality[i].accountTitle, 'recordTypeId': data.rtLocal,'budgetLine': '','localityCode': data.lstLocality[i].id, 'csaCate': ''});
                }
            }
        } else {
            component.set('v.isShowCostCenter', false);
        }
        component.set('v.total', total);
        return data;
    },
    parseDecimal: function(d) {
        if(!d){
            return 0;
        }
        if(d && typeof d === "string"){
            return parseFloat(d);
        }
        return d;
    }
})