({
    init : function(component, event, helper) {
        component.set('v.Spinner', true);
        let action = component.get("c.initData");
        action.setParams({
            'strbudget': component.get('v.recordId')
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let data = response.getReturnValue();
                if(data) {
                    component.set("v.budgetData", data);
                }else{
                    helper.showToast('error', 'Error', 'Please contact your Administrator for more details.');
                }
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                console.log(strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    saveBudgets: function(component, event, helper) {
        component.set('v.Spinner', true);
        let action = component.get("c.saveInitialBudgets");
        action.setParams({
            data: component.get('v.budgetData')
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                helper.showToast('success', 'Success!', 'The Budgets has been successfully updated.');
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                helper.showToast('error', 'Error', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    handleAmountChange: function(component, event, helper) {
        let data = component.get('v.budgetData');
        let total = 0;
        let totalAvaiB = 0;
        for(let i = 0 ; i < data.lstBudget.length ; i++) {
            total += helper.getDecimal(data.lstBudget[i].Initial_Budget__c);
            data.lstBudget[i].Available_Balance__c = helper.getDecimal(data.lstBudget[i].Initial_Budget__c) + helper.getDecimal(data.lstBudget[i].Budget_Amount__c) - helper.getDecimal(data.lstBudget[i].Total_Expenditures__c) + helper.getDecimal(data.lstBudget[i].Total_Receipts__c);
            totalAvaiB += data.lstBudget[i].Available_Balance__c;
        }
        data.totalIB = total;
        data.totalAvaiB = totalAvaiB;
        component.set('v.budgetData', data);
    }
})