({
    showToast : function(type, title, message) {
        let strTitle = title.split('\n').join('');
        let strMessage = message.split('\n').join('');
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : strTitle,
            message: strMessage,
            duration:' 4000',
            key: 'info_alt',
            type: type, // success , error, info
            mode: 'pester'//'sticky'
        });
        toastEvent.fire();
    },
    getDecimal: function(d) {
        if(!d) return 0;
        if(typeof d !== 'number'){
            try{
                d = parseFloat(d);
            }catch (e){
                d = 0;
            }
        }
        return d;
    }
})