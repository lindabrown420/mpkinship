({
    doInit: function(component, event, helper) {  
        let recordTypeId = component.get("v.pageReference").state.recordTypeId;  
        component.set('v.recordTypeId', recordTypeId);
    },
    doneRendering: function(component, event, helper) {
        let recordTypeId = component.get("v.pageReference").state.recordTypeId;  
        component.set('v.recordTypeId', recordTypeId);
    }
})