({
	doInit : function(component, event, helper) {
		var action= component.get("c.getLaserAccountCodes");
        action.setParams({
            	recordid: component.get("v.recordId")
        });	
        action.setCallback(this, function(response){	
        	var state = response.getState();
            console.log('**VAl of state'+state);
            console.log('***response'+JSON.stringify(response.getReturnValue()));
            if (state === "SUCCESS") {
                 var result=response.getReturnValue();
                if(result.message=='Laser Code is null'){
                    component.set("v.Ismessage",true); 
                }
                else{
                    
                    component.set("v.Ismessage",false);
                    component.set("v.LaserAccountCodeselected",result.laseraccountcodeid);
                    component.set("v.LaserAccountCodeSelectedlabel",result.laseraccountcodeName);
                    if(result.LaserOptions!=undefined && result.LaserOptions.length>0)
                        helper.getLaserOptions(component,event,helper,result.LaserOptions);
                }
            }    
        });
         
        $A.enqueueAction(action);
	},
     handleCancel :function(component,event,helper){
        $A.get("e.force:closeQuickAction").fire();
    },
    handleServiceNameChange: function(component,event,helper){
        var selectedOptionValue = event.getParam("value");
        component.set("v.ServiceNameselected",selectedOptionValue);
        if(selectedOptionValue=='')
             component.set("v.ServiceNameselectedlabel",'');
        else{
            var options = component.get("v.ServiceNames");
            var selectedlabel='';
            for(var i=0; i< options.length ; i++){
                if(options[i].value==selectedOptionValue){
                    selectedlabel=options[i].label;
                    break;
                }
            }
            if(selectedlabel!='')
                component.set("v.ServiceNameselectedlabel",selectedlabel);
        }    
    	component.set("v.ServiceNameselected",selectedOptionValue);	
    },
    handleLaserAccountCodeChange : function(component,event,helper){
    	 var selectedOptionValue = event.getParam("value");  
         component.set("v.LaserAccountCodeselected",selectedOptionValue);
        if(selectedOptionValue=='')
             component.set("v.LaserAccountCodeSelectedlabel",'');
        else{
            var options = component.get("v.AccountCodes");
            var selectedlabel='';
            for(var i=0; i< options.length ; i++){
                if(options[i].value==selectedOptionValue){
                    selectedlabel=options[i].label;
                    break;
                }
            }
            if(selectedlabel!='')
                component.set("v.LaserAccountCodeSelectedlabel",selectedlabel);
        }  
    },
    handleSave : function(component,event,helper){
         helper.showSpinner(component);
         var laseraccountCodename = component.get("v.LaserAccountCodeselected");
         var laseraccountCodelabel = component.get("v.LaserAccountCodeSelectedlabel");
         console.log('**VAl of selected'+laseraccountCodename);
         console.log('**VAl of label'+laseraccountCodelabel);
         var action= component.get("c.UpdateLaserAccountCode");
         action.setParams({
                    recordid : component.get("v.recordId"),
                	laseraccountcode: laseraccountCodename,
                	laseraccountCodeLabel : laseraccountCodelabel
         });	 
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('**VAl of state'+state);
            console.log('***response'+response.getReturnValue());
            if (state === "SUCCESS") {
                helper.hideSpinner(component);
                if(response.getReturnValue()==true){
                    helper.showToast('success', 'Record updated', 'Record has been updated.');
                    $A.get("e.force:closeQuickAction").fire();
                    $A.get('e.force:refreshView').fire();
                }
                
            }
            else if(state==="ERROR"){                
                var error = response.getError();
                console.log(error[0].message);               
                 helper.hideSpinner(component);
                 helper.showToast('Error', 'Record Error', error[0].message);
                 $A.get("e.force:closeQuickAction").fire();
            	
            }
           
        });     
        $A.enqueueAction(action);  
    }
})