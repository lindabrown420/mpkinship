({
	getServiceNameOptions : function(component,event,helper,ServiceNamesList) {
		 var items = [];
         var item = {                           
                     "label": "---None---",
                       "value": ''
                     };
         items.push(item);
         for (var i = 0; i < ServiceNamesList.length; i++) {
         	var item = {                           
                        "label": ServiceNamesList[i].label,
                        "value": ServiceNamesList[i].value.toString()
                       };
         	items.push(item);
         } 
         component.set("v.ServiceNames",items);    	
	},
    getLaserOptions :function(component,event,helper,LaserAccountCodes){
    	var items = [];
         var item = {                           
                     "label": "---None---",
                       "value": ''
                     };
         items.push(item);
         for (var i = 0; i < LaserAccountCodes.length; i++) {
         	var item = {                           
                        "label": LaserAccountCodes[i].label,
                        "value": LaserAccountCodes[i].value.toString()
                       };
         	items.push(item);
         } 
         component.set("v.AccountCodes",items);    	    
    },

	showToast : function(type, title, message) {
        if(type.toUpperCase() == 'ERROR'){
            type = 'info';
        }
               
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : title,
            message: message,
            duration:' 4000',
            key: 'info_alt',
            type: type,
            mode: 'pester'
        });
        toastEvent.fire();
    },
    
    showSpinner: function (component, event, helper) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },
     
    hideSpinner: function (component, event, helper) {
        var spinner = component.find("mySpinner");
        $A.util.addClass(spinner, "slds-hide");
    }

})