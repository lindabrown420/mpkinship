({
	doInit : function(component, event, helper) {
		var recordTypeId = component.get("v.pageReference").state.recordTypeId;
        component.set("v.recordtypeId",recordTypeId);
        console.log('*****'+recordTypeId);
        helper.createPOForm(component,event,helper);
	},
    changeRecordType : function(component, event, helper) {
		var recordTypeId = component.get("v.pageReference").state.recordTypeId;
        component.set("v.recordtypeId",recordTypeId);
        if(component.find("poid")!=undefined){
        	component.find("poid").destroy();    
        }
            
        helper.createPOForm(component,event,helper);
	}
})