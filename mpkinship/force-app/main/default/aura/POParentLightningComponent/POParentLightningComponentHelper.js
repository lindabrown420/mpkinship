({
    createPOForm : function(component,event,helper) {
		console.log('***create po form');
        $A.createComponent(
            "c:PurchaseOrderLightningComponent",
            {
                "aura:id":"poid",
                "recordtypeId":component.getReference('v.recordtypeId')                
            },
            function(msgBox){
                if (component.isValid()) {
                    var targetCmp = component.find('theForm');
                    var body = targetCmp.get("v.body");
                    body.push(msgBox);
                    targetCmp.set("v.body", body);
                }
            }
        );		
    }
})