({
	printPOSO : function(component, event, helper) {
		var selectedOpp = component.get("v.selectedOpps");
        console.log('selectedOpps'+selectedOpp);
        var selOppIdsstr='';
        for ( var i = 0; i < selectedOpp.length; i++ ) {
            
            console.log(selectedOpp[i].Id);
            selOppIdsstr=selOppIdsstr+selectedOpp[i].Id+',';

        }
        console.log('selectedOpps length'+selectedOpp.length);
        if(selectedOpp.length > 7){
            let toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : 'Error',
                message: 'Please select 7 records at a time.',
                duration:'5000',
                key: 'info_alt',
                type: 'Error',
                mode: 'pester'
            });
            toastEvent.fire();
        }
        else if(selOppIdsstr=='undefined' || selOppIdsstr==''){
            let toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : 'Error',
                message: 'Please select at least 1 record.',
                duration:'5000',
                key: 'info_alt',
                type: 'Error',
                mode: 'pester'
            });
            toastEvent.fire();
        }
        else{
            var url = '/apex/ProcessDdp?ddpId=a153d0000004Ei1AAE&deliveryOptionId=a133d0000004HORAA2&ids='+selOppIdsstr+'&sObjectType=Opportunity';
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": url
            });
            urlEvent.fire();
        }
	},
    fetchOpp : function(component, event, helper){
        helper.fetchOppHelper(component, event,helper);
    },
     handleNext : function(component, event, helper) { 
        var pageNumber = component.get("v.pageNumber");
        component.set("v.pageNumber", pageNumber+1);
        helper.fetchOppHelper(component, event, helper);
    },
     
    handlePrev : function(component, event, helper) {        
        var pageNumber = component.get("v.pageNumber");
        component.set("v.pageNumber", pageNumber-1);
        helper.fetchOppHelper(component, event, helper);
    },
    
    handleSelect : function(component, event, helper) {
        
        var selectedRows = event.getParam('selectedRows');
        console.log('selectedRows'+JSON.stringify(selectedRows));
        var setRows = [];
        console.log(selectedRows.length);
        if(selectedRows.length>7){
            let toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : 'Error',
                message: 'Please select 7 records at a time.',
                duration:'5000',
                key: 'info_alt',
                type: 'Error',
                mode: 'pester'
            });
            toastEvent.fire();
        }
        for ( var i = 0; i < selectedRows.length; i++ ) {
            
            setRows.push(selectedRows[i]);

        }
        component.set("v.selectedOpps", setRows);
        
    },
})