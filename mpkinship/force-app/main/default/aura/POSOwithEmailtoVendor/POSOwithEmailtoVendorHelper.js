({
	fetchOppHelper : function(component, event, helper) {
        console.log('inside fetchOppHelper');
        component.set('v.mycolumns', [
                {label: 'Name', fieldName: 'linkName', type: 'url', 
            		typeAttributes: {label: { fieldName: 'Name' }, target: '_blank'}},
                {label: 'Stage', fieldName: 'StageName', type: 'text'},
                {label: 'Begin Date', fieldName: 'CloseDate', type: 'date'},
            	{label: 'Amount', fieldName: 'Amount', type: 'currency', cellAttributes: { alignment: 'left' }},
            	{label: 'Created By', fieldName: 'CreatedById', type: 'url',
                      typeAttributes: {label: { fieldName: 'CreatedBy' }, target: '_blank'}},
            	{label: 'Created Date', fieldName: 'CreatedDate', type: 'date'},
            	{label: 'Case Name', fieldName: 'Kinship_Case__c', type: 'url',
                      typeAttributes: {label: { fieldName: 'KinshipCase' }, target: '_blank'}},
            ]);
        
        
        var pageSize = component.get("v.pageSize").toString();
        var pageNumber = component.get("v.pageNumber").toString();
         
        var action = component.get("c.fetchOpportunity");
        action.setParams({
             'pageSize' : pageSize,
            'pageNumber' : pageNumber
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log(response.getError());
            if (state === "SUCCESS") {
                 var records =response.getReturnValue();
            console.log('records'+JSON.stringify(records));
                records.forEach(function(record){
                    record.linkName = '/'+record.Id;
                    record.CreatedBy = record.CreatedBy.FirstName + ' '+ record.CreatedBy.LastName; 
    				record.WhatId = '/' + record.CreatedById ;
           			if(record.Kinship_Case__r){
            		record.KinshipCase = record.Kinship_Case__r.Name ; 
            		}
            		if(record.Kinship_Case__c){
    				record.Kinship_Case__c = '/' + record.Kinship_Case__c ;
            		}
                });
                if(records.length < component.get("v.pageSize")){
                    component.set("v.isLastPage", true);
                } else{
                    component.set("v.isLastPage", false);
                }
                component.set("v.dataSize", records.length);
                component.set("v.oppyList", records);
            }
        });
        $A.enqueueAction(action);
    }
})