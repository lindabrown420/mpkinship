({
	init : function(component, event, helper) {
		 var pageReference = component.get("v.pageReference");
        console.log(pageReference);
        console.log(pageReference.state.c__rid);
      
        console.log('***columns'+JSON.stringify(JSON.parse(pageReference.state.c__columns)));
        component.set("v.title",pageReference.state.c__title);
        component.set("v.recordid",pageReference.state.c__rid);
        component.set("v.columns",JSON.parse(pageReference.state.c__columns));
         var action = component.get( "c.fetchRecs" );  
        action.setParams({  
            recId: component.get( "v.recordid" ),  
            sObjName: pageReference.state.c__sObjName,  
            parentFldNam: pageReference.state.c__parentFldNam,  
            strCriteria: pageReference.state.c__criteria,
            selectStr: pageReference.state.c__selectStr
        });  
        action.setCallback(this, function(response) {  
            var state = response.getState();  
            if ( state === "SUCCESS" ) {  
                  
                var tempTitle = component.get( "v.title" );  
                var res = response.getReturnValue();
                console.log('**Val of res'+res.length);
                console.log('response val'+ JSON.stringify(res));
                for(var i = 0; i < res.length; i++){
                    	res[i].recordURL = '/'+res[i].Id; 
                        if(pageReference.state.c__isLookup1 !=undefined && pageReference.state.c__isLookup1 =="true" &&
                        pageReference.state.c__ParentfieldAPI1!=undefined ){
                            var parentid =pageReference.state.c__ParentfieldAPI1;
                            var parentName =pageReference.state.c__fieldAPI1;
                            parentName = parentName.split(".");
                            if(res[i][parentid]){
                                res[i].f2url ='/'+res[i].parentid;
                            }
                            else{
                                res[i].f2url = '';
                            }
                            if(parentName.length == 2){
                                if(res[i][parentName[0]]){
                                    if(res[i][parentName[0]][parentName[1]]){
                                        res[i].f2Name =res[i][parentName[0]][parentName[1]];
                                    }
                                    else{
                                        res[i].f2Name = '';
                                    }
                                }
                                else{
                                    res[i].f2Name = '';
                                }
                            }
                            
                        }
                        if(pageReference.state.c__isLookup2 !=undefined && pageReference.state.c__isLookup2 =="true" &&
                            pageReference.state.c__ParentfieldAPI2 !=undefined ){
                            var parentid =pageReference.state.c__ParentfieldAPI2;
                            var parentName =pageReference.state.c__fieldAPI2;
                            parentName = parentName.split(".");
                            if(res[i][parentid]){
                                res[i].f2url ='/'+res[i].parentid;
                            }
                            else{
                                res[i].f2url = '';
                            }
                            if(parentName.length == 2){
                                if(res[i][parentName[0]]){
                                    if(res[i][parentName[0]][parentName[1]]){
                                        res[i].f2Name =res[i][parentName[0]][parentName[1]];
                                    }
                                    else{
                                        res[i].f2Name = '';
                                    }
                                }
                                else{
                                    res[i].f2Name = '';
                                }
                            }
                            
                        }
                        if(pageReference.state.c__isLookup3 !=undefined && pageReference.state.c__isLookup3 =="true" &&
                            pageReference.state.c__ParentfieldAPI3 !=undefined ){
                            var parentid =pageReference.state.c__ParentfieldAPI3;
                            var parentName =pageReference.state.c__fieldAPI3;
                            parentName = parentName.split(".");
                            if(res[i][parentid]){
                                res[i].f3url ='/'+res[i][parentid];
                            }
                            else{
                                res[i].f3url = '';
                            }
                            if(parentName.length == 2){
                                if(res[i][parentName[0]]){
                                    if(res[i][parentName[0]][parentName[1]]){
                                        res[i].f3Name =res[i][parentName[0]][parentName[1]];
                                    }
                                    else{
                                        res[i].f3Name = '';
                                    }
                                }
                                else{
                                    res[i].f3Name = '';
                                }
                            }
                            
                        }
                        if(pageReference.state.c__isLookup4 !=undefined && pageReference.state.c__isLookup4 =="true" &&
                            pageReference.state.c__ParentfieldAPI4!=undefined ){
                            var parentid =pageReference.state.c__ParentfieldAPI4;
                            var parentName =pageReference.state.c__fieldAPI4;
                            parentName = parentName.split(".");
                            if(res[i][parentid]){
                                res[i].f4url ='/'+res[i][parentid];
                            }
                            else{
                                res[i].f4url = '';
                            }
                            if(parentName.length == 2){
                                if(res[i][parentName[0]]){
                                    if(res[i][parentName[0]][parentName[1]]){
                                        res[i].f4Name =res[i][parentName[0]][parentName[1]];
                                    }
                                    else{
                                        res[i].f4Name = '';
                                    }
                                }
                                else{
                                    res[i].f4Name = '';
                                }
                            }
                                
                        }
                        if(pageReference.state.c__isLookup5!=undefined && pageReference.state.c__isLookup5 =="true" &&
                            pageReference.state.c__ParentfieldAPI5 !=undefined ){
                            var parentid =pageReference.state.c__ParentfieldAPI5;
                            var parentName =pageReference.state.c__fieldAPI5;
                            parentName = parentName.split(".");
                            if(res[i][parentid]){
                                res[i].f5url ='/'+res[i][parentid];
                            }
                            else{
                                res[i].f5url = '';
                            }
                            if(parentName.length == 2){
                                if(res[i][parentName[0]]){
                                    if(res[i][parentName[0]][parentName[1]]){
                                        res[i].f5Name =res[i][parentName[0]][parentName[1]];
                                    }
                                    else{
                                        res[i].f5Name = '';
                                    }
                                }
                                else{
                                    res[i].f5Name = '';
                                }
                            }
                            
                        }
                        if(pageReference.state.c__isLookup6!=undefined && pageReference.state.c__isLookup6 =="true" &&
                            pageReference.state.c__ParentfieldAPI6!=undefined ){
                            var parentid =pageReference.state.c__ParentfieldAPI6;
                            var parentName =pageReference.state.c__fieldAPI6;
                            parentName = parentName.split(".");
                            if(res[i][parentid]){
                                res[i].f6url ='/'+res[i][parentid];
                            }
                            else{
                                res[i].f6url = '';
                            }
                            if(parentName.length == 2){
                                if(res[i][parentName[0]]){
                                    if(res[i][parentName[0]][parentName[1]]){
                                        res[i].f6Name =res[i][parentName[0]][parentName[1]];
                                    }
                                    else{
                                        res[i].f6Name = '';
                                    }
                                }
                                else{
                                    res[i].f6Name = '';
                                }
                            }
                        
                        }                   
                }
                var responselist=[];
                console.log('resr???????',res)
                component.set( "v.data", res );
                
            }  
        });  
        $A.enqueueAction(action); 
	}
})