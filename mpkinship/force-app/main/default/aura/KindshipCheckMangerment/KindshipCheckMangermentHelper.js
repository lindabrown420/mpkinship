({
    showToast : function(type, title, message) {
        let strTitle = title.split('\n').join('');
        let strMessage = message.split('\n').join('');
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : strTitle,
            message: strMessage,
            duration:' 4000',
            key: 'info_alt',
            type: type, // success , error, info
            mode: 'pester'//'sticky'
        });
        toastEvent.fire();
    },
    fetchData: function(component, objName, apiName, strNon, strValue){
        let action = component.get("c.getPicklistvalues");
        action.setParams({
            'objectName': objName,
            'field_apiname': apiName,
            'strNone': strNon
        });
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS"){
                let d = a.getReturnValue();
                if (d) {
                    let data = [];
                    for(let i = 0 ; i < d.length ; i++) {
                        //if(d[i] !== 'Created') {
                            data.push({'label' : d[i], 'value':d[i]});
                        //}
                    }
                    component.set("v." + strValue, data);
                }
            } 
        });
        $A.enqueueAction(action);
    },
    getData : function(component) {
        let action = component.get("c.getCheckData");
        let startNo = component.get("v.beginNumber");
        let endNo = component.get("v.endNumber");
        let searchStatus = component.get("v.searchStatus");
        let checkDate = component.get("v.checkDate");
        let amountFrom = component.get("v.amountFrom");
        if(!amountFrom && amountFrom !== 0) {
            amountFrom = null;
        }
        let amountTo = component.get("v.amountTo");
        if(!amountTo && amountTo !== 0) {
            amountTo = null;
        }
        if(!startNo && !endNo && !searchStatus && !checkDate && amountTo === null && amountFrom === null) {
            this.showToast('info', 'Missing Search Filter', 'Please add at least 1 search condition.');
            return;
        }
        component.set('v.Spinner', true);
        action.setParams({
            startNo: startNo,
            endNo: endNo,
            searchStatus: searchStatus,
            checkDate: checkDate,
            amountFrom: amountFrom,
            amountTo : amountTo
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let data = response.getReturnValue();
                if(data) {
                    let i = 1;
                    data.forEach(ele => {
                        if(ele.status){
                            if(ele.status == 'Issued' || ele.status == 'Uncancelled'){
                                ele.cancel = false;
                                ele.uncancel = true;
                            }
                            else if(ele.status == 'Cancelled & Reissued' || ele.status == 'Cancelled'){
                                ele.cancel = true;
                                ele.uncancel = false;
                            }
                        }
                        else{
                            ele.cancel = true;
                            ele.uncancel = true;
                        }
                        ele.no = i;
                        i++;
                    });
                    component.set("v.data", data);
            
            		// pagination
                    let paginationWrapper = component.get('v.paginationWrapper');
                    // hold all the records into an attribute productData
                    paginationWrapper.productData = data;
                    if (paginationWrapper.productData.length === 0) {
                        paginationWrapper.totalPage = 1;
                    } else if (paginationWrapper.productData.length % paginationWrapper.pageSize === 0){
                        paginationWrapper.totalPage = Math.floor(paginationWrapper.productData.length / paginationWrapper.pageSize);
                    } else {
                        paginationWrapper.totalPage = Math.floor(paginationWrapper.productData.length / paginationWrapper.pageSize) + 1;
                    }
                    paginationWrapper.startPage = 0;
                    paginationWrapper.endPage = paginationWrapper.pageSize;
                    
                    component.set('v.searchResults', paginationWrapper.productData.slice(paginationWrapper.startPage, paginationWrapper.endPage));
                    component.set('v.paginationWrapper', paginationWrapper);
                    component.set('v.paginationWrapper.isViewAll', true);
                }else{
                    this.showToast('error', 'Error', 'Please contact your Administrator for more details.');
                }
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                this.showToast('error', 'Error', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    updateData : function(component, data, status) {
        component.set('v.Spinner', true);
        let action = component.get("c.updateChecks");
        action.setParams({
            lstCheck: data,
            status: status
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                this.showToast('success', 'Successed', 'Successed');
                let oldData = component.get('v.data');
                let setData = new Set();
                for(let i = 0 ; i < data.length ; i++) {
                    setData.add(data[i]);
                }
                for(let i = 0 ; i < oldData.length ; i++) {
                    if(setData.has(oldData[i].id)) {
                        oldData[i].status = status;
                    }
                }
                component.set('v.data', oldData);
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                this.showToast('error', 'Error', strErr);
            }
            component.set('v.Spinner', false);
            //this.getData(component);
        });
        $A.enqueueAction(action);
    },
    initData : function(component) {
        component.set('v.Spinner', true);
        let action = component.get("c.initData");
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.checkDate", response.getReturnValue());
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    convertArrayOfObjectsToCSV : function(component, data){
        let csvStringResult, counter, keys, columnDivider, lineDivider;
       
        if (data == null || !data.length) {
            return null;
         }

        columnDivider = '|';
        lineDivider =  '\n';
        csvStringResult = '';
 
        for(let i=0; i < data.length; i++){
            if(data[i].recordTypeName === 'Case Actions') {
                csvStringResult += '||||' + this.formatData(data[i].costCenterCode) + columnDivider +
                                            this.formatData(data[i].localityAccountNumber) + '|||' +
                                            this.formatData(data[i].localityVendorNumber) + columnDivider +
                                            'N' + columnDivider + '11100' + columnDivider +
                                            this.formatDate(data[i].paymentDate) + columnDivider +
                                            this.formatData2(data[i].paymentNumber) + columnDivider +
                                            this.formatNumber(data[i].amount) + columnDivider +
                                            this.formatData2(data[i].caseName) + this.addSpace(this.formatData2(data[i].servicePeriod)).toUpperCase() + this.addSpace(this.formatData2(data[i].invoiceNumber)) + columnDivider  +
                                            'N' + '|||||||||||||||';
            } else if(data[i].recordTypeName === 'Purchase of Services Order') {
                csvStringResult += '||||' + this.formatData(data[i].costCenterCode) + columnDivider +
                                            this.formatData(data[i].localityAccountNumber) + '|||' +
                                            this.formatData(data[i].localityVendorNumber) + columnDivider +
                                            'M' + columnDivider + '11100' + columnDivider +
                                            this.formatDate(data[i].paymentDate) + columnDivider +
                                            this.formatData2(data[i].poNumber) + this.formatData2(data[i].paymentNumber) + columnDivider +
                                            this.formatNumber(data[i].amount) + columnDivider +
                                            this.formatData2(data[i].poNumber) + this.addSpace(this.formatData2(data[i].caseName)) + this.addSpace(this.formatData2(data[i].servicePeriod)).toUpperCase() + this.addSpace(this.formatData2(data[i].invoiceNumber)) + columnDivider  +
                                            'N' + '|||||||||||||||';
            } else if(data[i].recordTypeName === 'Staff & Operations') {
                csvStringResult += '||||' + this.formatData(data[i].costCenterCode) + columnDivider +
                                            this.formatData(data[i].localityAccountNumber) + '|||' +
                                            this.formatData(data[i].localityVendorNumber) + columnDivider +
                                            'M' + columnDivider + '11100' + columnDivider +
                                            this.formatDate(data[i].paymentDate) + columnDivider +
                                            this.formatData2(data[i].paymentNumber) + columnDivider +
                                            this.formatNumber(data[i].amount) + columnDivider +
                                            this.formatData(data[i].invoiceNumber) + this.addSpace(this.formatData(data[i].description)) + columnDivider  +
                                            'N' + '|||||||||||||||';
            }
            csvStringResult += lineDivider;
        }
       
        return csvStringResult;        
    },
    addSpace: function(str) {
        if(str) {
            return ' ' + str;
        }
        return str;
    },
    formatDate: function(d) {
        if(d){
            if(typeof d !== 'date'){
                d = new Date(d + ' 00:00:00');
            }
            return d.toLocaleDateString('en-US', {year: 'numeric', month: '2-digit', day: '2-digit'}).replaceAll('/','');
        }
        return '';
    },
    formatData: function(d) {
        if(d){
            return d;
        }
        return '';
    },
    formatData2: function(d) {
        if(d){
            return d.replaceAll('-','').replaceAll('#','');
        }
        return '';
    },
    formatNumber: function(d) {
        if(d){
            return d.toLocaleString('en-US', { minimumFractionDigits: 2 });
        }
        return '';
    },
    cancelCheck: function(component, row,helper){
        // check totall
        let total = 0;
        for(let i = 0 ; i < row.lstPayment.length ; i++) {
            total += row.lstPayment[i].npe01__Payment_Amount__c;
        }
        if(row.amount !== total) {//This check has been migrated from the previous system and requires additional validation before it can be cancelled. Email us at support@lnbsolutions.com with the check number.
            this.showToast('info', 'INFO', 'This check has been migrated from the previous system and requires additional validation before it can be cancelled. The request has been sent to LNBSolutions Team.');
            this.updateRequestSupport(component, row.id, 'Cancelled');
            return;
        }
        if(confirm('Are you sure you would like to Cancel this check?') === false) {
            return;
        }
        if(this.isMigratedCheck(component, row)) {
            for(let i = 0 ; i < row.lstPayment.length ; i++) {
                row.lstPayment[i].cateSelection = [];
                row.lstPayment[i].cateErrors = [];
            }
            component.set('v.migrationCheck', row);
            component.set('v.isMigrationCheck', true);
            component.set('v.isMigrationType', '1');
        } else {
            this.doCancelCheck(component, row.id, helper);
        }
    },
    isMigratedCheck: function(component, record){
        if(record.lstPayment.length === 0) return true;
        for(let i = 0 ; i < record.lstPayment.length ; i++) {
            if(!record.lstPayment[i].Category_lookup__c || !record.lstPayment[i].npe01__Opportunity__r.Category__c) {
                return true;
            }
        }
        return false;
    },
    doCancelCheck: function(component, strId, helper){
    	let action = component.get("c.changeCheckStatus");
        action.setParams({
            recordid: strId,
            status: 'Cancelled',
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let data = response.getReturnValue();
                if(data == 'Done'){
                    helper.getData(component);
                }
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                this.showToast('info', 'INFO', strErr);
            }
        });
        $A.enqueueAction(action);
    },
    uncancelCheck: function(component, row,helper){
        if(confirm('Are you sure you would like to UnCancel this check?') === false) {
            return;
        }
        let action = component.get("c.changeCheckStatus");
        action.setParams({
            recordid: row.id,
            status: 'Uncancelled',
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let data = response.getReturnValue();
                if(data == 'Done'){
                    helper.getData(component);
                }
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                this.showToast('info', 'INFO', strErr);
            }
        });
        $A.enqueueAction(action);
    },
    cancel_Reissue: function(component, row,helper){
        // check totall
        let total = 0;
        let isError = false;
        for(let i = 0 ; i < row.lstPayment.length ; i++) {
            total += row.lstPayment[i].npe01__Payment_Amount__c;
            if(!row.lstPayment[i].npe01__Paid__c || row.lstPayment[i].Invoice_Stage__c !== 'Paid'
                || row.lstPayment[i].npe01__Payment_Method__c !== 'Check' || !row.lstPayment[i].npe01__Scheduled_Date__c ||
                !row.lstPayment[i].Cost_Center__c) {
                isError = true;
            }
        }
        if(row.amount !== total || isError) {
            this.showToast('info', 'INFO', 'This check has been migrated from the previous system and requires additional validation before it can be cancelled. The request has been sent to LNBSolutions Team.');
            this.updateRequestSupport(component, row.id, 'Cancelled & Reissued');
            return;
        }
        if(confirm('Are you sure you would like to Cancel and Reissue this check?') === false) {
            return;
        }
        if(this.isMigratedCheck(component, row)) {
            for(let i = 0 ; i < row.lstPayment.length ; i++) {
                row.lstPayment[i].cateSelection = [];
                row.lstPayment[i].cateErrors = [];
            }
            component.set('v.migrationCheck', row);
            component.set('v.isMigrationCheck', true);
            component.set('v.isMigrationType', '2');
        } else {
            this.doCancelAndReissuedCheck(component, row.id, helper);
        }
    },
    doCancelAndReissuedCheck: function(component, strId, helper){
    	component.set('v.Spinner', true);
        let action = component.get("c.changeCheckStatus");
        action.setParams({
            recordid: strId,
            status: 'Cancelled & Reissued',
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let data = response.getReturnValue();
                if(data == 'Done'){
                    helper.getData(component);
                    this.getPaymentDateData(component, strId);
                }
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                this.showToast('info', 'INFO', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    next : function(component, event){
        let paginationWrapper = component.get('v.paginationWrapper');
        if (paginationWrapper.endPage < paginationWrapper.productData.length) {
            paginationWrapper.startPage = paginationWrapper.endPage;
            paginationWrapper.endPage = paginationWrapper.endPage + paginationWrapper.pageSize;
            component.set('v.paginationWrapper', paginationWrapper);
            component.set('v.searchResults', paginationWrapper.productData.slice(paginationWrapper.startPage, paginationWrapper.endPage));
        }
    },
    previous : function(component, event){
        let paginationWrapper = component.get('v.paginationWrapper');
        if (paginationWrapper.startPage > 0) {
            paginationWrapper.endPage = paginationWrapper.startPage;
            paginationWrapper.startPage = paginationWrapper.startPage -  paginationWrapper.pageSize;
            component.set('v.paginationWrapper', paginationWrapper);
            component.set('v.searchResults', paginationWrapper.productData.slice(paginationWrapper.startPage, paginationWrapper.endPage));
        }
    },
    mergeChecks: function(component, row, helper){
        component.set('v.mainCheck', row.id);
        let action = component.get("c.getMergeChecks");
        action.setParams({
            check: row
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let data = response.getReturnValue();
                component.set('v.lstMergeCheck', data);
                let oriMC = [];
                oriMC.push(row);
                component.set('v.lstOriMergeCheck', oriMC);
                component.set('v.isMergeCheck', true);
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                this.showToast('info', 'INFO', strErr);
            }
        });
        $A.enqueueAction(action);
    },
    getValue: function(data){
        if(data) return data;
        return '';
    },
    getPaymentDateData: function(component, checkId) {
        component.set('v.Spinner', true);
        let action = component.get("c.getPaymentCreated");
        action.setParams({
            strId: checkId
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let data = response.getReturnValue();
                if(data && data.length > 0) {
                    component.set('v.lstPaymentCreated', data);
                    component.set('v.isShowPaymentCreated', true);
                }
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                this.showToast('info', 'INFO', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    updateRequestSupport: function(component, checkId, status) {
        component.set('v.Spinner', true);
        let action = component.get("c.requestSupport");
        action.setParams({
            recordid: checkId,
            status: status
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                this.getData(component);
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    }
})