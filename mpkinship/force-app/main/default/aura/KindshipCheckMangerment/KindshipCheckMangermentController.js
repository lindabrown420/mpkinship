({
    init : function(component, event, helper) {
        component.set('v.columns', [
            {label: '', fieldName: 'checkUrl', type: 'url',initialWidth: 30, typeAttributes: { 
                label: {
                    fieldName: 'no'
                },
                target: '_blank'
            }},
            {label: 'Check No', fieldName: 'checkNo', type: 'text', editable: true},
            {label: 'Status', fieldName: 'status', type: 'text'},
            {label: 'Vendor', fieldName: 'vendorUrl', type: 'url', typeAttributes: { 
                label: {
                    fieldName: 'vendorName'
                },
                target: '_blank'
            }},
            {label: 'Issue Date', fieldName: 'issueDate', type: 'date-local',
                typeAttributes:{
                    month: "2-digit",
                    day: "2-digit",
                    year: "numeric"
                }
            },
            {label: 'Amount', fieldName: 'amount', type: 'currency', typeAttributes: { currencyCode: 'USD' }},
            {label: 'Organization', fieldName: 'organization', type: 'text'},
            {label: 'Description', fieldName: 'description', type: 'text'},
            {label: '',type: 'button-icon', 
                typeAttributes: { iconName: "utility:merge",
                    name: "mergechecks",
                    title: "Merge Checks",
                    value: { fieldName: "Id" },
                    disabled: { fieldName: "cancel" }
                },
                fixedWidth: 50
            },
            {label: '',type: 'button-icon', 
                typeAttributes: { iconName: "utility:cancel_file_request",
                    name: "cancel",
                    title: "Cancel",
                    disabled: { fieldName: "cancel" },
                    value: { fieldName: "Id" }
                },
                fixedWidth: 50
            },
            {label: '',type: 'button-icon', 
                typeAttributes: { iconName: "utility:cancel_transfer",
                    name: "cancel_Reissue",
                    title: "Cancel & Reissue",
                    disabled: { fieldName: "cancel" },
                    value: { fieldName: "Id" }
                },
                fixedWidth: 50
            },
            {label: '',type: 'button-icon', 
                typeAttributes: { iconName: "utility:error",
                    name: "uncancelled",
                    title: "Uncancel",
                    disabled: true,
                    value: { fieldName: "Id" }
                },
                fixedWidth: 50
            },
        ]);

        component.set('v.mccolumns', [
            {label: '', fieldName: 'no', type: 'text',initialWidth: 30},
            {label: 'Check No', fieldName: 'checkUrl', type: 'url', typeAttributes: { 
                label: {
                    fieldName: 'checkNo'
                },
                target: '_blank'
            }},
            {label: 'Status', fieldName: 'status', type: 'text'},
            {label: 'Vendor', fieldName: 'vendorUrl', type: 'url', typeAttributes: { 
                label: {
                    fieldName: 'vendorName'
                },
                target: '_blank'
            }},
            {label: 'Issue Date', fieldName: 'issueDate', type: 'date-local',
                typeAttributes:{
                    month: "2-digit",
                    day: "2-digit",
                    year: "numeric"
                }
            },
            {label: 'Amount', fieldName: 'amount', type: 'currency', typeAttributes: { currencyCode: 'USD' }}
        ]);

        component.set('v.mccolumns2', [
            {label: 'Check No', fieldName: 'checkUrl', type: 'url', typeAttributes: { 
                label: {
                    fieldName: 'checkNo'
                },
                target: '_blank'
            }},
            {label: 'Status', fieldName: 'status', type: 'text'},
            {label: 'Vendor', fieldName: 'vendorUrl', type: 'url', typeAttributes: { 
                label: {
                    fieldName: 'vendorName'
                },
                target: '_blank'
            }},
            {label: 'Issue Date', fieldName: 'issueDate', type: 'date-local',
                typeAttributes:{
                    month: "2-digit",
                    day: "2-digit",
                    year: "numeric"
                }
            },
            {label: 'Amount', fieldName: 'amount', type: 'currency', typeAttributes: { currencyCode: 'USD' }}
        ]);

        component.set('v.crcolumns', [
            {label: 'Name', fieldName: 'Name', type: 'text'},
            {label: 'Check Date', fieldName: 'Check_Date__c', type: 'date-local'},
            {label: 'Number of Check', fieldName: 'Number_Of_Check__c', type: 'number'},
            {label: 'Check Run Total', fieldName: 'Check_Run_Total__c', type: 'currency',sortable: true, typeAttributes: { currencyCode: 'USD' }},
            {label: 'FIPS', fieldName: 'FIPS_Name__c', type: 'text'}
        ]);

        component.set('v.pccolumns', [
            {label: 'Payment', fieldName: 'paymentUrl', type: 'url',sortable: true, typeAttributes: { 
                label: {
                    fieldName: 'paymentNo'
                },
                target: '_blank'
            }},
            {label: 'Schedule Date', fieldName: 'paymentDate', type: 'date-local',sortable: true,
                typeAttributes:{
                    month: "2-digit",
                    day: "2-digit",
                    year:"numeric"
                }
            },
            {label: 'Amount', fieldName: 'amount', type: 'currency',sortable: true, typeAttributes: { currencyCode: 'USD' }},
            {label: 'Authorization', fieldName: 'oppUrl', type: 'url',sortable: true, typeAttributes: { 
                label: {
                    fieldName: 'oppName'
                },
                target: '_blank'
            }}
        ]);
        
        // Pagination
        let paginationWrapper = {'productData':[], 'isViewAll': false, 'startPage': 0, 'endPage': 0, 'pageSize': 10, 'totalPage': 0};
        component.set('v.paginationWrapper', paginationWrapper);
        
        helper.initData(component);
        helper.fetchData(component,'Kinship_Check__c','Status__c','','statusoption');
    },
    searchCheck : function(component, event, helper) {
        helper.getData(component);
    },
    updateAll : function(component, event, helper) {
        let status = component.get('v.strStatus');
        if(!status) {
            helper.showToast('info', 'Missing Status', 'Please choose Status before clicking the "Update" button.');
            return;
        }
        let data = component.get('v.data');
        let lstUpdate = [];
        for(let i = 0 ; i < data.length ; i++) {
            lstUpdate.push(data[i].id);
        }
        helper.updateData(component, lstUpdate, status);
    },
    updateSelected : function(component, event, helper) {
        let status = component.get('v.strStatus');
        if(!status) {
            helper.showToast('info', 'Missing Status', 'Please choose Status before clicking the "Update" button.');
            return;
        }
        let data = component.find('paymentrecord').getSelectedRows();
        if(data.length <= 0) {
            helper.showToast('info', 'Missing Record', 'Please choose a Check Record before clicking the "Update" button.');
            return;
        }
        let lstUpdate = [];
        for(let i = 0 ; i < data.length ; i++) {
            lstUpdate.push(data[i].id);
        }
        helper.updateData(component, lstUpdate, status);
    },
    gotoCheckListView : function(component, event, helper) {
        let navEvent = $A.get("e.force:navigateToList");
        navEvent.setParams({
            "listViewId": '00B3d000000UAmWEAW',
            "listViewName": null,
            "scope": "Kinship_Check__c"
        });
        navEvent.fire();
    },
    exportToCSV: function(component, event, helper) {
        component.set('v.Spinner', true);
        let data = component.get('v.data');
        let lstCheck = [];
        for(let i = 0 ; i < data.length ; i++) {
            lstCheck.push(data[i].id);
        }
        let action = component.get("c.getExportData");
        action.setParams({
            lstCheck: lstCheck
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let data = response.getReturnValue();
                let csv = helper.convertArrayOfObjectsToCSV(component, data);   
                if (csv == null){
                    csv = '';
                } 
                
                var hiddenElement = document.createElement('a');
                hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURIComponent(csv);//encodeURI(csv);
                hiddenElement.target = '_self';
                hiddenElement.download = 'CheckTXT.txt';
                document.body.appendChild(hiddenElement);
                hiddenElement.click();
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                        console.log('**Val of error'+strErr);
                    }
                }
                this.showToast('error', 'Error', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    warrantCheckReport: function(component, event, helper) {
        component.set('v.Spinner', true);
        let action = component.get("c.getCheckRun");
        action.setParams({
            checkDate: Date.today()
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let mergeCheckId = response.getReturnValue();

                let documentPackageId = $A.get('$Label.c.KINSHIP_WC_DDP');
                let deliveryOptionId = $A.get('$Label.c.KINSHIP_WC_OPTION');
                let recordIds = mergeCheckId;
                let sObjectType = 'Check_Run__c';
                let url = window.location.protocol + '//' + window.location.host + '/apex/ProcessDdp';
                url = url + '?ddpId=' + documentPackageId;
                url = url + '&deliveryOptionId=' + deliveryOptionId;
                url = url + '&ids=' + recordIds;
                url = url + '&sObjectType=' + sObjectType;
                window.open(url ,'_blank');
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                helper.showToast('error', 'Error', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    warrantCheckReport: function(component, event, helper) {
        component.set('v.Spinner', true);
        let checkDate = component.get("v.checkDate");
        if(!checkDate){
            helper.showToast('info', 'Missing Check Date', 'Please input Check Date.');
            component.set('v.Spinner', false);
            return;
        }
        let action = component.get("c.getCheckRun");
        action.setParams({
            checkDate: checkDate
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let lstCheckRun = response.getReturnValue();
                if(lstCheckRun && lstCheckRun.length > 0) {
                    component.set("v.isShowCheckRun", true);
                    component.set("v.lstCheckRun", lstCheckRun);
                    let crselectedRows = [];
                    crselectedRows.push(lstCheckRun[0].Id);
                    let crdata = component.find("crdata");
                    crdata.set("v.selectedRows", crselectedRows);
                } else {
                    helper.showToast('info', 'No Check Run Found', 'Please input another Check Date.');
                }
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                helper.showToast('error', 'Error', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    closeCheckRunModel: function(component, event, helper) {
        component.set("v.isShowCheckRun", false);
        component.set("v.lstCheckRun", []);
    },
    printWarrantCheck: function(component, event, helper) {
        let crselectedRows = component.find('crdata').getSelectedRows();
        if(crselectedRows.length > 0) {
            let documentPackageId = $A.get('$Label.c.KINSHIP_WC_DDP');
            let deliveryOptionId = $A.get('$Label.c.KINSHIP_WC_OPTION');
            let sObjectType = 'Check_Run__c';
            let url = window.location.protocol + '//' + window.location.host + '/apex/ProcessDdp';
            url = url + '?ddpId=' + documentPackageId;
            url = url + '&deliveryOptionId=' + deliveryOptionId;
            url = url + '&ids=' + crselectedRows[0].Id;
            url = url + '&sObjectType=' + sObjectType;
            window.open(url ,'_blank');
        } else {
            helper.showToast('info', 'Missing Data.', 'Please select a Check Run record.');
        }
    },
    handleRowAction: function (cmp, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');

        switch (action.name) {
            case 'cancel':
                helper.cancelCheck(cmp, row,helper);
                break;
            case 'cancel_Reissue':
                helper.cancel_Reissue(cmp, row,helper);
                break;
            case 'uncancelled':
                helper.uncancelCheck(cmp, row,helper);
                break;
            case 'mergechecks': 
                helper.mergeChecks(cmp, row, helper);
                break;
        }
    },
    previous : function(component, event, helper) {
        helper.previous(component, event);
    },
    next : function(component, event, helper) {
        helper.next(component, event);
    },
    closeMergeCheckModel: function(component, event, helper) {
        component.set('v.isMergeCheck', false);
    },
    mergeCheck: function(component, event, helper) {
        let data = component.find('mcdata').getSelectedRows();
        let lstUpdate = [];
        let checkNo = '';
        for(let i = 0 ; i < data.length ; i++) {
            lstUpdate.push(data[i].id);
            checkNo += data[i].checkNo + ' ';
        }
        if(lstUpdate.length === 0) {
            helper.showToast('info', 'Info!', 'Please select at least 1 record.');
            return;
        }
        let lstOriMergeCheck = component.get('v.lstOriMergeCheck');
        if(confirm('Are you sure you want to merge check ' + lstOriMergeCheck[0].checkNo + ' with check(s) ' + checkNo + '?.') === false) {
            return;
        }
        let action = component.get("c.saveMergeCheck");
        action.setParams({
            lstId: lstUpdate,
            mainCheck: component.get('v.mainCheck')
        });
        component.set('v.Spinner', true);
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                helper.showToast('success', 'Success!', 'The Checks has been successfully merged.');
                component.set('v.isMergeCheck', false);
                helper.getData(component);
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                helper.showToast('error', 'Error', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    closeMigrationCheck: function(component, event, helper) {
        component.set('v.isMigrationCheck', false);
    },
    saveMigrationPayment: function(component, event, helper) {
        let migrationCheck = component.get('v.migrationCheck');
        let data,cate;
        for(let i = 0 ; i < migrationCheck.lstPayment.length ; i++) {
            if(!migrationCheck.lstPayment[i].npe01__Opportunity__r.Category__c) {
                data = migrationCheck.lstPayment[i].cateSelection;
                if(data && data.length > 0) {
                    migrationCheck.lstPayment[i].npe01__Opportunity__r.Category__c = data[0].id;
                } else {
                    helper.showToast('info', 'Missing Data', 'New Category is Empty.');
                    return;
                }
            }
            migrationCheck.lstPayment[i].Vendor__c = helper.getValue(migrationCheck.lstPayment[i].npe01__Opportunity__r.AccountId);
            migrationCheck.lstPayment[i].FIPS_Code__c = helper.getValue(migrationCheck.lstPayment[i].npe01__Opportunity__r.FIPS_Code__c);
            migrationCheck.lstPayment[i].Category_lookup__c = migrationCheck.lstPayment[i].npe01__Opportunity__r.Category__c;
            migrationCheck.lstPayment[i].Kinship_Case__c = helper.getValue(migrationCheck.lstPayment[i].npe01__Opportunity__r.Kinship_Case__c);
            migrationCheck.lstPayment[i].npe01__Payment_Date__c = migrationCheck.issueDate;
            if(migrationCheck.lstPayment[i].npe01__Paid__c != true) {
            	migrationCheck.lstPayment[i].npe01__Paid__c = true;
            }
        }
        component.set('v.Spinner', true);
        let action = component.get("c.saveMigrationCheck");
        action.setParams({
            migrationCheck: migrationCheck
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                //helper.showToast('success', 'Success', 'success');
                //helper.doCancelCheck(component, migrationCheck.id);
                component.set('v.migrationCheck', {});
                component.set('v.isMigrationCheck', false);
                component.set("v.cateSelection", []);
                component.set("v.accSelection", []);
                component.set("v.caseSelection", []);
                component.set("v.fipsSelection", []);
                let isMigrationType = component.get('v.isMigrationType');
                if(isMigrationType === '1') {
                    helper.doCancelCheck(component, migrationCheck.id, helper);
                } else {
                    helper.doCancelAndReissuedCheck(component, migrationCheck.id, helper);
                }
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                helper.showToast('info', 'INFO', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    cateLookupSearch : function(component, event, helper) {
        const lookupComponent = event.getSource();
        const serverSearchAction = component.get('c.kinshipSearchLookup');
        serverSearchAction.setParam('anOptionalParam', 'Cate');
        lookupComponent.search(serverSearchAction);
    },
    clearCateErrorsOnChange: function(component, event, helper) {
        let formData = event.getParam("formData");
        if(formData) {
            let btData = component.get('v.btData');
            let index = formData.param;
        }
    },
    accLookupSearch : function(component, event, helper) {
        const lookupComponent = event.getSource();
        const serverSearchAction = component.get('c.kinshipSearchLookup');
        serverSearchAction.setParam('anOptionalParam', 'Account');
        lookupComponent.search(serverSearchAction);
    },
    clearAccErrorsOnChange: function(component, event, helper) {
        const selection = component.get('v.accSelection');
        const errors = component.get('v.accErrors');

        if (selection.length && errors.length) {
            component.set('v.accErrors', []);
        }
    },
    fipsLookupSearch : function(component, event, helper) {
        const lookupComponent = event.getSource();
        const serverSearchAction = component.get('c.kinshipSearchLookup');
        serverSearchAction.setParam('anOptionalParam', 'FIPS');
        lookupComponent.search(serverSearchAction);
    },
    clearFIPSErrorsOnChange: function(component, event, helper) {
        const selection = component.get('v.fipsSelection');
        const errors = component.get('v.fipsErrors');

        if (selection.length && errors.length) {
            component.set('v.fipsErrors', []);
        }
    },
    caseLookupSearch : function(component, event, helper) {
        const lookupComponent = event.getSource();
        const serverSearchAction = component.get('c.kinshipSearchLookup');
        serverSearchAction.setParam('anOptionalParam', 'Case');
        lookupComponent.search(serverSearchAction);
    },
    clearCaseErrorsOnChange: function(component, event, helper) {
        const selection = component.get('v.caseSelection');
        const errors = component.get('v.caseErrors');

        if (selection.length && errors.length) {
            component.set('v.caseErrors', []);
        }
    },
    handleSaveEdition: function (component, event, helper) {
        let draft = event.getParam('draftValues');

        let action = component.get("c.updateCheckNo");
        action.setParams({
            lstData: draft
        });
        component.set('v.Spinner', true);
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.draftValues', []);
                helper.showToast('success', 'Success', 'Check No successfully updated.');
                helper.getData(component);
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors.length >= 1) {
                    if(errors[0]){
                        if(errors[0].pageErrors[0]) {
                        	strErr = errors[0].pageErrors[0].message;
                        }
                        if(errors[0].fieldErrors && errors[0].fieldErrors.Check_No__c && errors[0].fieldErrors.Check_No__c[0]) {
                        	strErr = errors[0].fieldErrors.Check_No__c[0].message;
                        }
                        console.log('**Val of error'+strErr);
                    }
                }
                helper.showToast('info', 'INFO', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    closePaymentCreateModel: function(component, event, helper) {
        component.set('v.isShowPaymentCreated', false);
    },
    getCRSelectedName: function(component, event, helper) {
        let selectedRows = event.getParam('selectedRows');
        if(selectedRows.length > 1) {
            let sRows = component.get('v.crselectedRows');
            let strRows = sRows.toString();
            let crselectedRows = [];
            for(let i = 0 ; i < selectedRows.length ; i++) {
                if(strRows.indexOf(selectedRows[i].Id) === -1) {
                    crselectedRows.push(selectedRows[i].Id);
                    break;
                }
            }
            let crdata = component.find("crdata");
            crdata.set("v.selectedRows", crselectedRows);
        } else if(selectedRows.length === 1){
            let crselectedRows = [];
            crselectedRows.push(selectedRows[0].Id);
            let crdata = component.find("crdata");
            crdata.set("v.selectedRows", crselectedRows);
        }
    }
})