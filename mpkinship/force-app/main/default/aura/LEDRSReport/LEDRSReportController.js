({
    init : function(component, event, helper) {
        component.set('v.columns', [
            {label: 'Name', fieldName: 'tjurl', type: 'url', typeAttributes: { 
                label: {
                    fieldName: 'name'
                },
                target: '_blank'
            },initialWidth: 120/*, sortable: true*/},
            {label: 'Locality Identifier', fieldName: 'localityIdentifier', type: 'text', initialWidth: 150, sortable: true},
            {label: 'Locality Child Identifier', fieldName: 'localityChildIdentifier', type: 'text', initialWidth: 200, sortable: true},
            {label: 'SSN', fieldName: 'ssn', type: 'text', initialWidth: 150, sortable: true},
            {label: 'Student Identifier', fieldName: 'studentIdentifier', type: 'text', initialWidth: 150, sortable: true},
            {label: 'Last Name', fieldName: 'lastName', type: 'text', initialWidth: 150, sortable: true},
            {label: 'First Name', fieldName: 'firstName', type: 'text', initialWidth: 150, sortable: true},
            {label: 'Middle Name', fieldName: 'middleName', type: 'text', initialWidth: 150, sortable: true},
            {label: 'Suffix', fieldName: 'suffix', type: 'text', initialWidth: 50, sortable: true},
            {label: 'OASIS Client Id', fieldName: 'oasisClientId', type: 'text', initialWidth: 150, sortable: true},
            {label: 'Date Of Birth', fieldName: 'dob', type: 'date-local',typeAttributes: {day: '2-digit',month: '2-digit',year: 'numeric'}, initialWidth: 150, sortable: true},
            {label: 'Gender', fieldName: 'gender', type: 'text', initialWidth: 100, sortable: true},
            {label: 'Race', fieldName: 'race', type: 'text', initialWidth: 100, sortable: true},
            {label: 'Hispanic Ethnicity', fieldName: 'hispanicEthnicity', type: 'text', initialWidth: 150, sortable: true},
            {label: 'Discharge Reason Code', fieldName: 'dischargeReasonCode', type: 'text', initialWidth: 200, sortable: true},
            {label: 'Referral Source ', fieldName: 'referralSource', type: 'text', initialWidth: 150, sortable: true},
            {label: 'Autism Flag', fieldName: 'autsmFlag', type: 'text', initialWidth: 100, sortable: true},
            {label: 'DSM V Indicator/ ICD-10', fieldName: 'dsmVIndicator', type: 'text', initialWidth: 100, sortable: true},
            {label: 'Clinical Medication Indicator', fieldName: 'clinicalMedication', type: 'text', initialWidth: 100, sortable: true},
            {label: 'Medicaid Indicator', fieldName: 'medicaidIndicator', type: 'text', initialWidth: 100, sortable: true},
            {label: 'Locality Provider Identifier', fieldName: 'localityProviderIdentifier', type: 'text', initialWidth: 100, sortable: true},
            {label: 'Provider Name', fieldName: 'providerName', type: 'text', initialWidth: 150, sortable: true},
            {label: 'Tax ID', fieldName: 'taxID', type: 'text', initialWidth: 150, sortable: true},
            {label: 'Billing Address1', fieldName: 'billingAddress1', type: 'text', initialWidth: 150, sortable: true},
            {label: 'Billing Address2', fieldName: 'billingAddress2', type: 'text', initialWidth: 150, sortable: true},
            {label: 'Billing City', fieldName: 'billingCity', type: 'text', initialWidth: 150, sortable: true},
            {label: 'Billing State', fieldName: 'billingState', type: 'text', initialWidth: 150, sortable: true},
            {label: 'Billing Zip', fieldName: 'billingZip', type: 'text', initialWidth: 150, sortable: true},
            {label: 'Billing US', fieldName: 'billingUS', type: 'text', initialWidth: 150, sortable: true},
            {label: 'Billing Phone', fieldName: 'billingPhone', type: 'text', initialWidth: 150, sortable: true},
            {label: 'Service Address1', fieldName: 'serviceAddress1', type: 'text', initialWidth: 150, sortable: true},
            {label: 'Service Address2', fieldName: 'serviceAddress2', type: 'text', initialWidth: 150, sortable: true},
            {label: 'Service City', fieldName: 'serviceCity', type: 'text', initialWidth: 150, sortable: true},
            {label: 'Service State', fieldName: 'serviceState', type: 'text', initialWidth: 150, sortable: true},
            {label: 'Service Zip', fieldName: 'serviceZip', type: 'text', initialWidth: 150, sortable: true},
            {label: 'Service Phone', fieldName: 'servicePhone', type: 'text', initialWidth: 150, sortable: true},
            {label: 'Type of Provider', fieldName: 'typeOfProvider', type: 'text', initialWidth: 150, sortable: true},
            {label: 'Purchase Order', fieldName: 'purchaseOrder', type: 'text', initialWidth: 150, sortable: true},
            {label: 'PO Begin Date', fieldName: 'poBeginDate', type: 'date-local',typeAttributes: {day: '2-digit',month: '2-digit',year: 'numeric'}, initialWidth: 150, sortable: true},
            {label: 'PO End Date', fieldName: 'poEndDate', type: 'date-local',typeAttributes: {day: '2-digit',month: '2-digit',year: 'numeric'}, initialWidth: 200, sortable: true},
            {label: 'PO Maximum Quantity', fieldName: 'poMaxQuantity', type: 'number', maximumFractionDigits:"2", initialWidth: 200, sortable: true},
            {label: 'PO Unit Price', fieldName: 'poUnitPrice', type: 'currency', typeAttributes: { currencyCode: 'USD' }, initialWidth: 200, sortable: true},
            {label: 'PO Unit Measure', fieldName: 'poUnitMeasure', type: 'text', initialWidth: 150, sortable: true},
            {label: 'Locality Service Record Identifier', fieldName: 'localityServiceRecord', type: 'text', initialWidth: 250, sortable: true},
            {label: 'Parent Recipient', fieldName: 'parentRecipient', type: 'text', initialWidth: 150, sortable: true},
            {label: 'Date of Service - Beginning', fieldName: 'dateOfServiceBeginning', type: 'date-local',typeAttributes: {day: '2-digit',month: '2-digit',year: 'numeric'}, initialWidth: 200, sortable: true},
            {label: 'Date of Service - End', fieldName: 'dateOfServiceEnd', type: 'date-local',typeAttributes: {day: '2-digit',month: '2-digit',year: 'numeric'}, initialWidth: 200, sortable: true},
            {label: 'Service Placement Type', fieldName: 'spt', type: 'text', initialWidth: 200, sortable: true},
            {label: 'Mandate Type Code for the Service', fieldName: 'mandateType', type: 'text', initialWidth: 200, sortable: true},
            {label: 'Service Names Code', fieldName: 'serviceNameCode', type: 'text', initialWidth: 150, sortable: true},
            {label: 'Service Description - Other', fieldName: 'serviceDesOther', type: 'text', initialWidth: 150, sortable: true},
            {label: 'Locality Payment Identifier', fieldName: 'localityPaymentIdentifier', type: 'text', initialWidth: 200, sortable: true},
            {label: 'Payment/Adjustment Date', fieldName: 'adjustmentDate', type: 'date-local',typeAttributes: {day: '2-digit',month: '2-digit',year: 'numeric'}, initialWidth: 200, sortable: true},
            {label: 'Warrant# / Check#/ Receipt#', fieldName: 'warrant', type: 'text', initialWidth: 200, sortable: true},
            {label: 'Quantity Billed', fieldName: 'quantityBilled', type: 'number', maximumFractionDigits: "2", initialWidth: 150, sortable: true},
            {label: 'Payment Amount', fieldName: 'adjustmentAmount', type: 'number', maximumFractionDigits: "2", initialWidth: 150, sortable: true},
            {label: 'Transaction Code', fieldName: 'transactionCode', type: 'text', initialWidth: 150, sortable: true},
            {label: 'Transaction Description', fieldName: 'transactionDescription', type: 'text', initialWidth: 200, sortable: true},
            {label: 'Expenditure Category', fieldName: 'expenditureCategory', type: 'text', initialWidth: 150, sortable: true},
            {label: 'Program Year', fieldName: 'programYear', type: 'text', initialWidth: 150, sortable: true},
            {label: 'Funding Source', fieldName: 'fundingSource', type: 'text', initialWidth: 150, sortable: true},
            {label: 'Title IV–EEligibilityFlag', fieldName: 'eligibilityFlag', type: 'text', initialWidth: 100, sortable: true}
        ]);

        // Pagination
        let paginationWrapper = {'productData':[], 'isViewAll': false, 'startPage': 0, 'endPage': 0, 'pageSize': 15, 'totalPage': 0};
        component.set('v.paginationWrapper', paginationWrapper);
        helper.fetchPicklist(component, 'npe01__OppPayment__c', 'Payment_Source__c', 'invoiceSource');
    },
    seachTransaction : function(component, event, helper) {
        helper.getData(component);
    },
    ledrsReport: function(component, event, helper) {
        let cateType = component.get("v.cateType");
        if(cateType === 'CSA') {
            helper.ledrscsaReport(component, event);
        } else {
            helper.ledrsiveReport(component, event);
        }
    },
    ledrsExportToCSV:function(component, event, helper) {
        helper.ledrsCSVExport(component, event);
    },
    handleSort: function(component, event, helper) {
        component.set('v.Spinner', true);
        setTimeout(function(){ 
            helper.handleSort(component, event);
            component.set('v.Spinner', false);
        }, 0);
        //helper.handleSort(component, event);
    },
    previous : function(component, event, helper) {
        helper.previous(component, event);
    },
    next : function(component, event, helper) {
        helper.next(component, event);
    },
    campaignLookupSearch : function(component, event, helper) {
        const lookupComponent = event.getSource();
        const serverSearchAction = component.get('c.kinshipSearchLookup');
        serverSearchAction.setParam('anOptionalParam', 'Campaign');
        lookupComponent.search(serverSearchAction);
    },
    clearCampaignErrorsOnChange: function(component, event, helper) {
        const selection = component.get('v.campaignSelection');
        let formData = event.getParam("formData");
        if(formData && formData.recordId) {
            component.set('v.isShowButton', true);
            helper.isPostAP(component);
        } else {
            component.set('v.isShowButton', false);
        }
        const errors = component.get('v.campaignErrors');
        if (selection.length && errors.length) {
            component.set('v.campaignErrors', []);
        }
    },
    closeUploadModel: function(component, event, helper) {
        component.set('v.isUploadModal', false);
        component.set("v.uploadData" , []);
    },
    openUploadModel: function(component, event, helper) {
        component.set('v.isUploadModal', true);
    },
    onSelectChange: function(component, event, helper) {
        let uploadData = component.get("v.uploadData");
        let ssn = '';
        for(let i = 0 ; i < uploadData.length ; i++) {
            if(uploadData[i].eligibilityFlag && ssn.indexOf(uploadData[i].ssn) === -1) {
                for(let j = i + 1 ; j < uploadData.length ; j++) {
                    if(uploadData[j].ssn === uploadData[i].ssn) {
                        uploadData[j].eligibilityFlag = uploadData[i].eligibilityFlag;
                    }
                }
                ssn += ',' + uploadData[i].ssn;
            }
        }
        component.set("v.uploadData" , uploadData);
    },
    exportUploadData: function(component, event, helper) {
        let uploadData = component.get("v.uploadData");
        let csv = '';
        if(uploadData.length === 0) {
            helper.showToast('info', 'No Data To Export', 'Please upload a new File.');
            return;
        }
        for(let i = 0 ; i < uploadData.length ; i++) {
            if(!uploadData[i].eligibilityFlag) {
                helper.showToast('info', 'Missing Title IV–EEligibilityFlag', 'Please choose the Title IV–EEligibilityFlag.');
                return;
            }
            csv += uploadData[i].olddata.replaceAll('\r', '') + '|' + helper.getEligible(uploadData[i].eligibilityFlag) + '\n';
        }  
        if (csv){
            var hiddenElement = document.createElement('a');
            hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURIComponent(csv);
            hiddenElement.target = '_self';
            hiddenElement.download = component.get('v.filename');
            document.body.appendChild(hiddenElement);
            hiddenElement.click();
        }
    },
    handleFilesChange : function (component, event, helper) {
        let uploadFile = event.getSource().get("v.files");
        if(uploadFile && uploadFile.length > 0) {
            let file = uploadFile[0];
            component.set('v.filename', file.name);
            let reader = new FileReader();
            reader.onload =  $A.getCallback(function() {
                let datas = reader.result.split('\n');
                if(!datas.length || datas[0].split('|').length !== 60) {
                    helper.showToast('info', 'Wrong File Format', 'Please upload a new File.');
                    return;
                }
                let uploadData = [];
                for(let i = 0 ; i < datas.length ; i++) {
                    if(datas[i]) {
                        let fields = datas[i].split('|');
                        uploadData.push({"casename": (helper.formatStr(fields[5]) + ' ' + helper.formatStr(fields[4])), "olddata": datas[i], "eligibilityFlag": "No", "ssn":fields[2]});
                    }
                }
                component.set("v.uploadData" , uploadData);
            });
            reader.readAsText(file);
        }
    },
    handleTypeChange: function (component, event, helper) {
        component.set("v.data", []);
        component.set("v.searchResults", []);
    },
    campaign2LookupSearch : function(component, event, helper) {
        const lookupComponent = event.getSource();
        const serverSearchAction = component.get('c.kinshipSearchLookup');
        serverSearchAction.setParam('anOptionalParam', 'Campaign2');
        lookupComponent.search(serverSearchAction);
    },
    clearCampaign2ErrorsOnChange: function(component, event, helper) {
        const selection = component.get('v.campaign2Selection');
        let formData = event.getParam("formData");
        if(formData) {
            if(formData.recordId) {
                let action = component.get("c.getAllAP");
                action.setParams({
                    parentId: formData.recordId
                });
                action.setCallback(this, function(response) {
                    let state = response.getState();
                    if (state === "SUCCESS") {
                        let data = response.getReturnValue();
                        for(let i = 0 ; i < data.length ; i++) {
                            data[i].quater = helper.getQuaterOnly(data[i].Name);
                        }
                        component.set('v.lstCampData', data);
                        helper.isPostAP(component);
                    } else {
                        const errors = response.getError();
                        let strErr = '';
                        if(errors) {
                            if(errors[0] && errors[0].message){
                                strErr = errors[0].message;
                            }
                        }
                        helper.showToast('error', 'Error', strErr);
                    }
                    component.set('v.Spinner', false);
                });
                $A.enqueueAction(action);
                component.set('v.isShowQuater', true);
            } else {
                component.set('v.isShowQuater', false);
            }
        } else {
            component.set('v.isShowQuater', false);
        }
        const errors = component.get('v.campaign2Errors');
        if (selection.length && errors.length) {
            component.set('v.campaign2Errors', []);
        }
    },
    openSummary: function(component, event, helper) {
        let data = component.get("v.data");
        if (data.length > 0) {
            let invoiceSource = component.get('v.invoiceSource');
            let sumTotalCodeGross = [];
            let sumTotalCodeRA = [];
            let keys = Object.keys(invoiceSource);
            
            for(let i = 0 ; i < keys.length ; i++){
                let expCode = helper.getExpCode(keys[i]);
                if(expCode !== '') {
                    if(helper.isGross(keys[i])){
                        sumTotalCodeGross.push({'code':keys[i],'expCode': expCode, 'codeDes': (expCode + ' ' + invoiceSource[keys[i]]), 'total': 0});
                    } else {
                        sumTotalCodeRA.push({'code':keys[i],'expCode': expCode, 'codeDes': (expCode + ' ' + invoiceSource[keys[i]]), 'total': 0});
                    }
                }
            }
            // sort
            let lstSummary = [];
            let strYear = '';
            let grossCode = ',1,8,12,13,16,15,10,';
            for(let i = 0 ; i < data.length ; i++) {
                if(strYear.indexOf(data[i].programYear) !== -1) {
                    let isNew;
                    for(let k = 0 ; k < lstSummary.length ; k++) {
                        if(data[i].programYear === lstSummary[k].programYear) {
                            isNew = true;
                            for(let j = 0 ; j < lstSummary[k].lstTotal.length ; j++) {
                                if(data[i].expenditureCategory === lstSummary[k].lstTotal[j].cate) {
                                    isNew = false;
                                    if(grossCode.indexOf(',' + data[i].transCode + ',') === -1) {
                                        lstSummary[k].lstTotal[j].refundAndAdj += helper.formatNumber(data[i].adjustmentAmount);
                                    } else {
                                        lstSummary[k].lstTotal[j].grossExp += helper.formatNumber(data[i].adjustmentAmount);
                                    }
                                }
                            }
                            if(isNew) {
                                let sumTotal = {'cate': data[i].expenditureCategory, 'cateDes': data[i].categoryDes, 'grossExp': 0, 'refundAndAdj': 0, 'netExp': 0};
                                if(grossCode.indexOf(',' + data[i].transCode + ',') === -1) {
                                    sumTotal.refundAndAdj += helper.formatNumber(data[i].adjustmentAmount);
                                } else {
                                    sumTotal.grossExp += helper.formatNumber(data[i].adjustmentAmount);
                                }
                                lstSummary[k].lstTotal.push(sumTotal);
                            }

                            for(let j = 0 ; j < lstSummary[k].lstTotalCodeGross.length ; j++) {
                                if(data[i].transCode === lstSummary[k].lstTotalCodeGross[j].code) {
                                    lstSummary[k].lstTotalCodeGross[j].total += helper.formatNumber(data[i].adjustmentAmount);
                                }
                            }

                            for(let j = 0 ; j < lstSummary[k].lstTotalCodeRA.length ; j++) {
                                if(data[i].transCode === lstSummary[k].lstTotalCodeRA[j].code) {
                                    lstSummary[k].lstTotalCodeRA[j].total += helper.formatNumber(data[i].adjustmentAmount);
                                }
                            }
                        }
                    }
                } else {
                    let sumData = [];
                    let sumTotal = {'cate': data[i].expenditureCategory, 'cateDes': data[i].categoryDes, 'grossExp': 0, 'refundAndAdj': 0, 'netExp': 0};
                    if(grossCode.indexOf(',' + data[i].transCode + ',') === -1) {
                        sumTotal.refundAndAdj += helper.formatNumber(data[i].adjustmentAmount);
                    } else {
                        sumTotal.grossExp += helper.formatNumber(data[i].adjustmentAmount);
                    }
                    sumData.push(sumTotal);
                    let totalDataGross = Object.assign([],
                        sumTotalCodeGross.sort(helper.sortBy('expCode', 1))
                    );
                    sumTotalCodeGross = JSON.parse(JSON.stringify(sumTotalCodeGross));
                    for(let j = 0 ; j < totalDataGross.length ; j++) {
                        if(data[i].transCode === totalDataGross[j].code) {
                            totalDataGross[j].total += helper.formatNumber(data[i].adjustmentAmount);
                        }
                    }
                    let totalDataRA = Object.assign([],
                        sumTotalCodeRA.sort(helper.sortBy('expCode', 1))
                    );
                    totalDataRA = JSON.parse(JSON.stringify(totalDataRA));
                    for(let j = 0 ; j < totalDataRA.length ; j++) {
                        if(data[i].transCode === totalDataRA[j].code) {
                            totalDataRA[j].total += helper.formatNumber(data[i].adjustmentAmount);
                        }
                    }
                    lstSummary.push({'programYear': data[i].programYear, 'lstTotal': sumData, 'lstTotalCodeGross': totalDataGross, 'lstTotalCodeRA': totalDataRA});
                    strYear += ',' + data[i].programYear;
                }
            }
            for(let k = 0 ; k < lstSummary.length ; k++) {
                let total1 = 0, total2 = 0, total3 = 0;
                for(let j = 0 ; j < lstSummary[k].lstTotal.length ; j++) {
                    lstSummary[k].lstTotal[j].netExp = lstSummary[k].lstTotal[j].grossExp - lstSummary[k].lstTotal[j].refundAndAdj;
                    total1 += lstSummary[k].lstTotal[j].grossExp;
                    total2 += lstSummary[k].lstTotal[j].refundAndAdj;
                    total3 += lstSummary[k].lstTotal[j].netExp;
                }
                lstSummary[k].lstTotal.push({'cate': '', 'cateDes': 'TOTAL EXPENDITURES', 'grossExp': total1, 'refundAndAdj': total2, 'netExp': total3});
                let totalCode = 0;
                for(let j = 0 ; j < lstSummary[k].lstTotalCodeGross.length ; j++) {
                    totalCode += lstSummary[k].lstTotalCodeGross[j].total;
                }
                lstSummary[k].lstTotalCodeGross.push({'code':'','expCode': '', 'codeDes': 'TOTAL GROSS EXPENDITURES', 'total': totalCode});
				totalCode = 0;
                for(let j = 0 ; j < lstSummary[k].lstTotalCodeRA.length ; j++) {
                    totalCode += lstSummary[k].lstTotalCodeRA[j].total;
                }
                lstSummary[k].lstTotalCodeRA.push({'code':'','expCode': '', 'codeDes': 'TOTAL REFUND/ADJUSTMENTS', 'total': totalCode});
            }
            //component.set('v.lstTotal', sumData);
            //component.set('v.lstTotalCode', sumTotalCode);
            component.set('v.lstSummary', lstSummary);
            component.set('v.isShowTotal', true);
            component.set('v.headerText', helper.getHeaderText(component));
        } else {
            helper.showToast('info', 'No Data.', 'There is no record for this Accounting Period.');
        }
    },
    closeSumPopup: function(component, event, helper) {
        component.set('v.isShowTotal', false);
    },
    summaryExportToCSV: function(component, event, helper) {
        let data = component.get("v.lstSummary");
        if (data.length > 0) {
            let csvStringResult, counter, keys, columnDivider, lineDivider;
            columnDivider = ',';
            lineDivider =  '\n';

            let headerText = component.get('v.headerText');
            let cateType = component.get('v.cateType');
            csvStringResult = '';
            csvStringResult += ',,"' + headerText.fips + 'COUNTY"' + lineDivider;
            csvStringResult += ',,"' + cateType + 'EXPENDITUREES LEDRS UPLOAD FILE"' + lineDivider;
            csvStringResult += ',,"FROM' + headerText.startDate + ' TO ' + headerText.endDate + '"' + lineDivider;
            csvStringResult += ',,"FISCAL YEAR ENDING ' + headerText.yearEnd + '"' + lineDivider + lineDivider;
            
            for(let k = 0 ; k < data.length ; k++) {
                csvStringResult += data[k].programYear + lineDivider;
                let headers = ['CODE', 'SUBCATEGORY', 'GROSS EXPENDITURES', 'REFUND ADJUSTMENTS', 'NET EXPENDITURES'];
                keys = ['cate', 'cateDes', 'grossExp', 'refundAndAdj', 'netExp'];
                csvStringResult += headers.join(columnDivider);
                csvStringResult += lineDivider;
                
                for(let i=0; i < data[k].lstTotal.length; i++){  
                    counter = 0;
                
                    for(let sTempkey in keys) {
                        let skey = keys[sTempkey] ;  
                        if(counter > 0){ 
                            csvStringResult += columnDivider; 
                        }   
                        csvStringResult += '"' + (data[k].lstTotal[i][skey]?data[k].lstTotal[i][skey]:'0') + '"'; 
                        counter++;
                    }
                    csvStringResult += lineDivider;
                }
                csvStringResult += lineDivider + lineDivider + lineDivider;
                csvStringResult += 'TOTAL GROSS EXPENDITURES' + lineDivider;
                for(let i = 0 ; i < data[k].lstTotalCodeGross.length ; i++) {
                    csvStringResult += ('"' + data[k].lstTotalCodeGross[i].code + '",' + '"' + data[k].lstTotalCodeGross[i].codeDes + '",,' + '"' + data[k].lstTotalCodeGross[i].total + '",' + lineDivider);
                }
                csvStringResult += lineDivider;
                csvStringResult += 'TOTAL REFUND/ADJUSTMENTS' + lineDivider;
                for(let i = 0 ; i < data[k].lstTotalCodeRA.length ; i++) {
                    csvStringResult += ('"' + data[k].lstTotalCodeRA[i].code + '",' + '"' + data[k].lstTotalCodeRA[i].codeDes + '",,' + '"' + data[k].lstTotalCodeRA[i].total + '",' + lineDivider);
                }
                csvStringResult += lineDivider;
            }

            let hiddenElement = document.createElement('a');
            hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csvStringResult);
            hiddenElement.target = '_self';
            hiddenElement.download = 'Summary Export.csv';//this.getName(component, data[0], true);
            document.body.appendChild(hiddenElement);
            hiddenElement.click();
        } else {
            helper.showToast('info', 'No Data.', 'There is no record for this Accounting Period.');
        }
    },
    autoPostAP: function(component, event, helper) {
        let lstCampaign = helper.getCampaign(component);
        component.set('v.Spinner', true);
        let action = component.get("c.postedAP");
        action.setParams({
            lstId: lstCampaign
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                helper.showToast('success', 'Success!', 'The LEDRS Accounting Period has been successfully posted.');
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                helper.showToast('error', 'Error', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    handleQuaterChange: function(component, event, helper){
        helper.isPostAP(component);
    }
})