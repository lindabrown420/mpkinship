({
    showToast : function(type, title, message) {
        let strTitle = title.split('\n').join('');
        let strMessage = message.split('\n').join('');
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : strTitle,
            message: strMessage,
            duration:' 4000',
            key: 'info_alt',
            type: type, // success , error, info
            mode: 'pester'//'sticky'
        });
        toastEvent.fire();
    },
    fetchPicklist: function(component, objName, apiName, strValue){
        let action = component.get("c.getPicklistvalues");
        action.setParams({
            'objectName': objName,
            'field_apiname': apiName
        });
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS"){
                let d = a.getReturnValue();
                if (d) {
                    component.set("v." + strValue, d);
                }
            } 
        });
        $A.enqueueAction(action);
    },
    getData : function(component) {
        let lstCampaign = this.getCampaign(component);
        component.set('v.Spinner', true);
        let action = component.get("c.getLEDRSData");
        action.setParams({
            lstCamp: lstCampaign
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let data = response.getReturnValue();
                if(data) {
                    component.set("v.data", data);

                    // pagination
                    let paginationWrapper = component.get('v.paginationWrapper');
                    // hold all the records into an attribute productData
                    paginationWrapper.productData = data;
                    if (paginationWrapper.productData.length === 0) {
                        paginationWrapper.totalPage = 1;
                    } else if (paginationWrapper.productData.length % paginationWrapper.pageSize === 0){
                        paginationWrapper.totalPage = Math.floor(paginationWrapper.productData.length / paginationWrapper.pageSize);
                    } else {
                        paginationWrapper.totalPage = Math.floor(paginationWrapper.productData.length / paginationWrapper.pageSize) + 1;
                    }
                    paginationWrapper.startPage = 0;
                    paginationWrapper.endPage = paginationWrapper.pageSize;
                    
                    component.set('v.searchResults', paginationWrapper.productData.slice(paginationWrapper.startPage, paginationWrapper.endPage));
                    component.set('v.paginationWrapper', paginationWrapper);
                    component.set('v.paginationWrapper.isViewAll', true);
                    component.set('v.isShowButton', true);
                }else{
                    this.showToast('error', 'Error', 'Please contact your Administrator for more details.');
                }
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                this.showToast('error', 'Error', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    getCampaign : function(component) {
        let cateType = component.get('v.cateType')
        let lstCampaign = [];

        if(cateType === 'CSA') {
            let data = component.get("v.campaignSelection");
            if(data) {
                data.forEach(element => {
                    lstCampaign.push(element.id);
                });
            }
            if(!lstCampaign.length) {
                this.showToast('info', 'Missing Accounting Period', 'Please choose an Accounting Period.');
                return;
            }
        }
        if(cateType === 'IV-E') {
            let data = component.get("v.campaign2Selection");
            if(data.length !== 1) {
                this.showToast('info', 'Missing Accounting Period', 'Please select CSA Accounting Period.');
                return;
            }
            let quater = component.get('v.quater');
            if(quater === "") {
                this.showToast('info', 'Missing Quater', 'Please select Quater.');
                return;
            }
            let lstCamp = component.get('v.lstCampData');
            for(let i = 0 ; i < lstCamp.length ; i++) {
                if(lstCamp[i].quater === quater) {
                    lstCampaign.push(lstCamp[i].Id);
                }
            }
        }
        return lstCampaign;
    },
    isPostAP : function(component) {
        let lstId = this.getCampaign(component);
        if(lstId.length > 0){
            component.set('v.Spinner', true);
            let action = component.get("c.checkPostAP");
            action.setParams({
                lstId: lstId
            });
            action.setCallback(this, function(response) {
                let state = response.getState();
                if (state === "SUCCESS") {
                    let data = response.getReturnValue();;
                    if(data) {
                        component.set('v.isShowPAP', true);
                    } else {
                        component.set('v.isShowPAP', false);
                    }
                } else {
                    const errors = response.getError();
                    let strErr = '';
                    if(errors) {
                        if(errors[0] && errors[0].message){
                            strErr = errors[0].message;
                        }
                    }
                    helper.showToast('error', 'Error', strErr);
                }
                component.set('v.Spinner', false);
            });
            $A.enqueueAction(action);
        }
    },
    convertArrayOfObjectsToCSV : function(component, objectRecords, isCSA){
        let csvStringResult, counter, keys, columnDivider, lineDivider;

        columnDivider = '|';
        lineDivider =  '\n';
 
        let headers = ['Locality Identifier','Locality Child Identifier','SSN','Student Identifier','Last Name','First Name','Middle Name','Suffix','OASIS Client Id','Date Of Birth','Gender','Race','Hispanic Ethnicity','Discharge Reason Code','Referral Source','Autism Flag','DSM V Indicator/ ICD-10','Clinical Medication Indicator','Medicaid Indicator','Locality Provider Identifier','Provider Name','Tax ID','Billing Address1','Billing Address2','Billing City','Billing State','Billing Zip','Billing US','Billing Phone','Service Address1','Service Address2','Service City','Service State','Service Zip','Service Phone','Type of Provider','Purchase Order','PO Begin Date','PO End Date','PO Maximum Quantity','PO Unit Price','PO Unit Measure','Locality Service Record Identifier','Parent Recipient','Date of Service - Beginning','Date of Service - End','Service Placement Type','Mandate Type Code for the Service','Service Names Code','Service Description - Other','Locality Payment Identifier','Payment/Adjustment Date','Warrant/Check/Receipt','Quantity Billed','Payment Amount','Transaction Code','Transaction Description','Expenditure Category','Program Year','Funding Source','Title IV–EEligibilityFlag'];
        keys = ['localityIdentifier','localityChildIdentifier','ssn','studentIdentifier','lastName','firstName','middleName','suffix','oasisClientId','dob','gender','race','hispanicEthnicity','dischargeReasonCode','referralSource','autsmFlag','dsmVIndicator','clinicalMedication', 'medicaidIndicator','localityProviderIdentifier','providerName','taxID','billingAddress1','billingAddress2','billingCity','billingState','billingZip','billingUS','billingPhone','serviceAddress1','serviceAddress2','serviceCity','serviceState','serviceZip','servicePhone','typeOfProvider','purchaseOrder','poBeginDate','poEndDate','poMaxQuantity','poUnitPrice','poUnitMeasure','localityServiceRecord','parentRecipient','dateOfServiceBeginning','dateOfServiceEnd','spt','mandateType','serviceNameCode','serviceDesOther','localityPaymentIdentifier','adjustmentDate','warrant','quantityBilled','adjustmentAmount','transactionCode','transactionDescription','expenditureCategory','programYear','fundingSource','eligibilityFlag'];
        let datedata = 'dob,poBeginDate,poEndDate,dateOfServiceBeginning,dateOfServiceEnd,adjustmentDate';
        let removeData = 'lastName,firstName,middleName,transactionDescription,serviceDesOther,providerName';
        let amountField = 'quantityBilled,adjustmentAmount,poMaxQuantity,poUnitPrice';
        let taxID = 'taxID';
        let phone = 'billingPhone,servicePhone';
        let unit = 'poUnitMeasure';
        csvStringResult = '';
        //csvStringResult += headers.join(columnDivider);
        //csvStringResult += lineDivider;

        if (!objectRecords) {
            return csvStringResult;
        }
 
        for(let i=0; i < objectRecords.length; i++){  
            if((isCSA && objectRecords[i].fundingSource != 'CSA') || (!isCSA && objectRecords[i].fundingSource != 'IVE')) {
                continue;
            }
            counter = 0;
           
             for(let sTempkey in keys) {
                let skey = keys[sTempkey] ;  
 
                if(counter > 0){ 
                    csvStringResult += columnDivider; 
                }  
                if(unit.indexOf(skey) !== -1){
                    csvStringResult += this.getUnitCode(objectRecords[i][skey]);
                } else if(taxID.indexOf(skey) !== -1){
                    csvStringResult += this.formatSSN(objectRecords[i][skey]);
                } else if(phone.indexOf(skey) !== -1){
                    csvStringResult += this.formatPhone(objectRecords[i][skey]);
                } else if(amountField.indexOf(skey) !== -1){
                    csvStringResult += this.formatNumber2(objectRecords[i][skey]);
                } else if(removeData.indexOf(skey) !== -1){
                    csvStringResult += this.formatAndRemove(objectRecords[i][skey]);
                } else if(datedata.indexOf(skey) === -1){
                csvStringResult += ((objectRecords[i][skey] || objectRecords[i][skey] === 0 || objectRecords[i][skey] === '0')?objectRecords[i][skey]:''); 
               } else {
                csvStringResult += (objectRecords[i][skey]?this.formatDate(objectRecords[i][skey]):''); 
               }
               
               counter++;
 
            }
            if(i < (objectRecords.length - 1)){
            	csvStringResult += lineDivider;
            }
        }
       
        return csvStringResult;        
    },
    convertArrayOfObjectsToCSV2 : function(component, objectRecords, isCSA){
        let csvStringResult, counter, keys, columnDivider, lineDivider;

        columnDivider = ',';
        lineDivider =  '\n';
 
        let headers = ['Locality Identifier','Locality Child Identifier','SSN','Student Identifier','Last Name','First Name','Middle Name','Suffix','OASIS Client Id','Date Of Birth','Gender','Race','Hispanic Ethnicity','Discharge Reason Code','Referral Source','Autism Flag','DSM V Indicator/ ICD-10','Clinical Medication Indicator','Medicaid Indicator','Locality Provider Identifier','Provider Name','Tax ID','Billing Address1','Billing Address2','Billing City','Billing State','Billing Zip','Billing US','Billing Phone','Service Address1','Service Address2','Service City','Service State','Service Zip','Service Phone','Type of Provider','Purchase Order','PO Begin Date','PO End Date','PO Maximum Quantity','PO Unit Price','PO Unit Measure','Locality Service Record Identifier','Parent Recipient','Date of Service - Beginning','Date of Service - End','Service Placement Type','Mandate Type Code for the Service','Service Names Code','Service Description - Other','Locality Payment Identifier','Payment/Adjustment Date','Warrant/Check/Receipt','Quantity Billed','Payment Amount','Transaction Code','Transaction Description','Expenditure Category','Program Year','Funding Source','Title IV–EEligibilityFlag'];
        keys = ['localityIdentifier','localityChildIdentifier','ssn','studentIdentifier','lastName','firstName','middleName','suffix','oasisClientId','dob','gender','race','hispanicEthnicity','dischargeReasonCode','referralSource','autsmFlag','dsmVIndicator','clinicalMedication', 'medicaidIndicator','localityProviderIdentifier','providerName','taxID','billingAddress1','billingAddress2','billingCity','billingState','billingZip','billingUS','billingPhone','serviceAddress1','serviceAddress2','serviceCity','serviceState','serviceZip','servicePhone','typeOfProvider','purchaseOrder','poBeginDate','poEndDate','poMaxQuantity','poUnitPrice','poUnitMeasure','localityServiceRecord','parentRecipient','dateOfServiceBeginning','dateOfServiceEnd','spt','mandateType','serviceNameCode','serviceDesOther','localityPaymentIdentifier','adjustmentDate','warrant','quantityBilled','adjustmentAmount','transactionCode','transactionDescription','expenditureCategory','programYear','fundingSource','eligibilityFlag'];
        let datedata = 'dob,poBeginDate,poEndDate,dateOfServiceBeginning,dateOfServiceEnd,adjustmentDate';
        let removeData = 'lastName,firstName,middleName,transactionDescription,serviceDesOther,providerName';
        let amountField = 'quantityBilled,adjustmentAmount,poMaxQuantity,poUnitPrice';
        let taxID = 'taxID';
        let phone = 'billingPhone,servicePhone';
        let unit = 'poUnitMeasure';
        csvStringResult = '';
        csvStringResult += headers.join(columnDivider);
        csvStringResult += lineDivider;

        if (!objectRecords) {
            return csvStringResult;
        }
 
        for(let i=0; i < objectRecords.length; i++){  
            /*if((isCSA && objectRecords[i].fundingSource != 'CSA') || (!isCSA && objectRecords[i].fundingSource != 'IVE')) {
                continue;
            }*/
            counter = 0;
           
             for(let sTempkey in keys) {
                let skey = keys[sTempkey] ;  
 
                if(counter > 0){ 
                    csvStringResult += columnDivider; 
                }
                if(unit.indexOf(skey) !== -1){
                    csvStringResult += '"' + this.getUnitCode(objectRecords[i][skey]) + '"';
                } else if(taxID.indexOf(skey) !== -1){
                    csvStringResult += '"' + this.formatSSN(objectRecords[i][skey]) + '"';
                } else if(phone.indexOf(skey) !== -1){
                    csvStringResult += '"' + this.formatPhone(objectRecords[i][skey]) + '"';
                } else if(amountField.indexOf(skey) !== -1){
                    csvStringResult += '"' + this.formatNumber2(objectRecords[i][skey]) + '"';
                } else if(removeData.indexOf(skey) !== -1){
                    csvStringResult += '"' + this.formatAndRemove(objectRecords[i][skey]) + '"';
                } else if(datedata.indexOf(skey) === -1){
                csvStringResult += '"' + ((objectRecords[i][skey] || objectRecords[i][skey] === 0 || objectRecords[i][skey] === '0')?objectRecords[i][skey]:'') + '"'; 
               } else {
                csvStringResult += '"' + (objectRecords[i][skey]?this.formatDate(objectRecords[i][skey]):'') + '"'; 
               }
               
               counter++;
 
            }
            if(i < (objectRecords.length - 1)){
            	csvStringResult += lineDivider;
            }
        }
       
        return csvStringResult;        
    },
    getUnitCode: function(str) {
        switch(str){
            case 'Bulk':return "1";
            case 'Day':return "2";
            case 'Each':return "3";
            case 'Fee':return "4";
            case 'Hour':return "5";
            case 'Mileage':return "6";
            case 'Monthly':return "7";
            case 'Miscellaneous':return "8";
            case 'Unit':return "9";
            case 'Weekly':return "10";
            case 'At Cost':return "11";
            case 'Session':return "12";
        }
        return '';
    },
    formatDate: function(d) {
        if(d){
            if(typeof d !== 'date'){
                d = new Date(d + ' 00:00:00');
            }
            return d.toLocaleDateString('en-US', {year: 'numeric', month: '2-digit', day: '2-digit'});
        }
        return '';
    },
    formatAndRemove: function(str) {
        if(str){
            return str.replaceAll('"', '').replaceAll('/', '').replaceAll('\\', '').replaceAll('|', '');
        }
        return '';
    },
    formatNumber: function(d) {
        if(d){
            if(typeof d !== 'number'){
                d = parseFloat(d);
            }
            return d;
        }
        return 0;
    },
    formatNumber2: function(d) {
        if(d){
            if(typeof d !== 'number'){
                d = parseFloat(d);
            }
            return d.toFixed(2);
        }
        return '0.00';
    },
    formatSSN: function(str) {
        if(str){
            return str.substring(0,2) + '-' + str.substring(2,str.length);
        }
        return '';
    },
    formatPhone: function(str) {
        if(str){
            return str.substring(0,3) + '-' + str.substring(3,6) + '-' + str.substring(6,str.length);
        }
        return '';
    },
    sortBy: function(field, reverse, primer) {
        var key = primer
            ? function(x) {
                  return primer(x[field]);
              }
            : function(x) {
                  return x[field];
              };

        return function(a, b) {
            a = key(a);
            a = a?a:'';
            b = key(b);
            b = b?b:'';
            return reverse * ((a > b) - (b > a));
        };
    },
    handleSort: function(cmp, event) {
        var sortedBy = event.getParam('fieldName');
        var sort = sortedBy;
        if(sortedBy === 'tjurl'){
            sort = 'name';
        }
        var sortDirection = event.getParam('sortDirection');
        /*var data = cmp.get('v.data');
        data = Object.assign([],
            data.sort(this.sortBy(sort, sortDirection === 'asc' ? 1 : -1))
        );
        
        cmp.set('v.data', data);*/
        var data = cmp.get('v.searchResults');
        data = Object.assign([],
            data.sort(this.sortBy(sort, sortDirection === 'asc' ? 1 : -1))
        );
        
        cmp.set('v.searchResults', data);
        cmp.set('v.sortDirection', sortDirection);
        cmp.set('v.sortedBy', sortedBy);
    },
    next : function(component, event){
        let paginationWrapper = component.get('v.paginationWrapper');
        if (paginationWrapper.endPage < paginationWrapper.productData.length) {
            paginationWrapper.startPage = paginationWrapper.endPage;
            paginationWrapper.endPage = paginationWrapper.endPage + paginationWrapper.pageSize;
            component.set('v.paginationWrapper', paginationWrapper);
            component.set('v.searchResults', paginationWrapper.productData.slice(paginationWrapper.startPage, paginationWrapper.endPage));
        }
    },
    previous : function(component, event){
        let paginationWrapper = component.get('v.paginationWrapper');
        if (paginationWrapper.startPage > 0) {
            paginationWrapper.endPage = paginationWrapper.startPage;
            paginationWrapper.startPage = paginationWrapper.startPage -  paginationWrapper.pageSize;
            component.set('v.paginationWrapper', paginationWrapper);
            component.set('v.searchResults', paginationWrapper.productData.slice(paginationWrapper.startPage, paginationWrapper.endPage));
        }
    },
    ledrscsaReport: function(component, event) {
        let data = component.get("v.data");
        let csv = this.convertArrayOfObjectsToCSV(component, data, true);   
        if (csv){
            var hiddenElement = document.createElement('a');
            hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURIComponent(csv);
            hiddenElement.target = '_self';
            hiddenElement.download = this.getName(component, data[0], true);
            document.body.appendChild(hiddenElement);
            hiddenElement.click();
        } else {
            this.showToast('info', 'No CSA Data.', 'There is no CSA record for this Accounting Period.');
        }
    },
    ledrsiveReport: function(component, event) {
        let data = component.get("v.data");
        let csv = this.convertArrayOfObjectsToCSV(component, data, false);   
        if (csv){
            var hiddenElement = document.createElement('a');
            hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURIComponent(csv);
            hiddenElement.target = '_self';
            hiddenElement.download = this.getName(component, data[0], false);
            document.body.appendChild(hiddenElement);
            hiddenElement.click();
        } else {
            this.showToast('info', 'No IV-E Data.', 'There is no IV-E record for this Accounting Period.');
        }
    },
    ledrsCSVExport: function(component, event) {
        let data = component.get("v.data");
        if (data.length > 0) {
            let csv = this.convertArrayOfObjectsToCSV2(component, data, true);   
            if (csv){
                var hiddenElement = document.createElement('a');
                hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURIComponent(csv);
                hiddenElement.target = '_self';
                hiddenElement.download = 'LEDRS Export.csv';//this.getName(component, data[0], true);
                document.body.appendChild(hiddenElement);
                hiddenElement.click();
            } else {
                this.showToast('info', 'No Data.', 'There is no record for this Accounting Period.');
            }
        } else {
            this.showToast('info', 'No Data.', 'There is no record for this Accounting Period.');
        }
    },
    getEligible : function(d){
        if(d == 'Yes') return 1
        return 2;
    },
    getName: function(component, data, isCSA) {
        if(isCSA) {
            let ap = component.get('v.campaignSelection');
            let apSplit = ap[0].title.split('-');
            return 'CSA_' + data.localityIdentifier + '_M_' + apSplit[1].trim() + '_' + apSplit[2].trim() + '_1.txt'; 
        } else {
            let ap = component.get('v.campaign2Selection');
            let d = new Date();
            let apSplit = d.getFullYear();//ap[0].title.split('-');
            let quater = component.get('v.quater');
            if(quater === '1' || quater === '2') {
                apSplit -= 1;
            }
            return 'T4E_' + data.localityIdentifier + '_Q_' + apSplit + '_' + quater + '_1.txt';
        }
    },
    getQuater: function(title, year) {
        let lst = title.split('-');
        if(lst.length == 5) {
            if(year !== lst[1]){
                return null;
            }
            let d = parseInt(lst[2]);
            switch(d){
                case 1:return 3;
                case 2:return 3;
                case 3:return 3;
                case 4:return 4;
                case 5:return 4;
                case 6:return 4;
                case 7:return 1;
                case 8:return 1;
                case 9:return 1;
                case 10:return 2;
                case 11:return 2;
                case 12:return 2;
            }
        }
        return null;
    },
    getQuaterYear: function(title) {
        let lst = title.split('-');
        if(lst.length == 5) {
            return lst[1];
        }
        return '';
    },
    getQuaterOnly: function(title) {
        let lst = title.split('-');
        if(lst.length == 5) {
            let d = parseInt(lst[2]);
            switch(d){
                case 1:return "3";
                case 2:return "3";
                case 3:return "3";
                case 4:return "4";
                case 5:return "4";
                case 6:return "4";
                case 7:return "1";
                case 8:return "1";
                case 9:return "1";
                case 10:return "2";
                case 11:return "2";
                case 12:return "2";
            }
        }
        return null;
    },
    getExpCode: function(code) {
        switch(code){
            case '1':return " ";
            case '2':return "010";
            case '3':return "010";
            case '4':return "020";
            case '5':return "040";
            case '6':return "030";
            case '7':return "050";
            case '8':return " ";
            case '10':return "090";
            case '11':return " ";
            case '12':return "010";
            case '13':return "010";
            case '14':return "020";
            case '15':return "040";
            case '16':return "030";
        }
        return '';
    },
    isGross: function(code) {
        switch(code){
            case '1':return true;
            case '2':return false;
            case '3':return false;
            case '4':return false;
            case '5':return false;
            case '6':return false;
            case '7':return false;
            case '8':return true;
            case '10':return true;
            case '11':return false;
            case '12':return true;
            case '13':return true;
            case '14':return false;
            case '15':return true;
            case '16':return true;
        }
        return '';
    },
    getHeaderText : function(component) {
        let cateType = component.get('v.cateType')
        let headerText = {'fips': '', 'startDate':'', 'endDate':'', 'yearEnd':''};

        if(cateType === 'CSA') {
            let data = component.get("v.campaignSelection");
            if(data) {
                let title = data[0].title;
                let split = title.split('-');
                headerText.fips = split[4].trim().toUpperCase();
                headerText.startDate = split[2].trim() + '/01/' + split[1].trim();
                headerText.endDate = split[2].trim() + '/' + split[3].trim() + '/' + split[1].trim();
                let q = this.getQuaterOnly(title);
                headerText.yearEnd = '06/30/' + (parseInt(split[1].trim()) + ((q==='1' || q === '2')?1:0));
            }
        } else if(cateType === 'IV-E') {
            let quater = component.get('v.quater');
            let lstCamp = component.get('v.lstCampData');
            let startAPName = '';
            let endAPName = '';
            for(let i = 0 ; i < lstCamp.length ; i++) {
                if(lstCamp[i].quater === quater) {
                    if(!startAPName) startAPName = lstCamp[i].Name;
                    endAPName = lstCamp[i].Name;
                }
            }
            let startSplit = startAPName.split('-');
            let endSplit = endAPName.split('-');
            headerText.fips = startSplit[4].trim().toUpperCase();
            headerText.startDate = startSplit[2].trim() + '/01/' + startSplit[1].trim();
            headerText.endDate = endSplit[2].trim() + '/' + endSplit[3].trim() + '/' + endSplit[1].trim();
            let q = this.getQuaterOnly(startAPName);
            headerText.yearEnd = '06/30/' + (parseInt(endSplit[1].trim()) + ((q==='1' || q === '2')?1:0));
        }
        return headerText;
    },
    formatStr: function(str) {
        if(str){
            return str;
        }
        return '';
    }
})