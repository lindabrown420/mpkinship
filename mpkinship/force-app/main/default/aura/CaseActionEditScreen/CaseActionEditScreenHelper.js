({
	FetchInitialValues : function(component,event,helper) {
        console.log(component.get("v.recordId"));
        var action=component.get("c.getEditCaseAction");
        action.setParams({          	                
            recordid : component.get("v.recordId")
        });	
         action.setCallback(this,function(response){           
            var state = response.getState();
            console.log('**State val'+state);
            if(state==="SUCCESS"){
                 var result=response.getReturnValue(); 
                console.log('******$$$');
                 console.log(JSON.stringify(result));
                if(result.profileName !=null){
                    component.set("v.profileName",result.profileName);
                    if(result.profileName=='System Administrator' || result.profileName=='CSA Admin' ||
                       result.profileName=='Fiscal Officer'){                 
                        component.set("v.IsDesiredProfiles",true);                       
                    }
                }   
                if(result.IsPaidPayments!=null){
                    console.log('**Val of ispaidpayments'+result.IsPaidPayments);
                    component.set("v.IsPaidPayments",result.IsPaidPayments);
                }  
                if(result.caseactionrec!=null){
                    console.log('**Costcenterids'+result.costcenterids);
                    if(result.caseactionrec.Cost_Center__c!=null && result.caseactionrec.Cost_Center__c!=undefined){
                        var costcenterselected ={"Id":result.caseactionrec.Cost_Center__c,"Name":result.caseactionrec.Cost_Center__r.Name};                        
                   		component.set("v.costcenterSelection",costcenterselected);
                         component.set("v.showCostCenterLookup",true);
                    }
                    if(result.costcenterids!=undefined && result.costcenterids.length >0){
                        component.set("v.CostCenterIds",result.costcenterids);
                        component.set("v.showCostCenterLookup",true);
                    }
                     if(result.caseactionrec.Category__c!=null && result.caseactionrec.Category__r.Id!=null){
                        component.set("v.category",result.caseactionrec.Category__r.Id);
                         
                       // var selectedrecord= {"Id":result.caseactionrec.Category__r.Id,"Name": result.caseactionrec.Category__r.Name};
                     
                         /*  if(result.caseactionrec.Category_Account_Description__c!=undefined)
                            var catdata ={"Id":result.caseactionrec.Category__r.Id,"Name":result.caseactionrec.Category__r.Name,"Category_Account_Description__c":result.caseactionrec.Category_Account_Description__c}; 
                        else
                            var catdata ={"Id":result.caseactionrec.Category__r.Id,"Name":result.caseactionrec.Category__r.Name}; */
                        component.set("v.categorySelection",result.caseactionrec.Category__r); 
                        console.log("val of categoryselection");
                        console.log(JSON.stringify(component.get("v.categorySelection")));
                    }    
                    if(result.caseactionrec.StageName!=null)
                    	component.set("v.opportunityStage",result.caseactionrec.StageName);
                 	if(result.caseactionrec.AccountId!=null)
                        component.set("v.vendor",result.caseactionrec.AccountId);
                    if(result.caseactionrec.FIPS_Code__c!=null)
                        component.set("v.FIPSID",result.caseactionrec.FIPS_Code__c);
                    if(result.caseactionrec.Kinship_Case__c!=null)
                        component.set("v.case",result.caseactionrec.Kinship_Case__c);  
                    
                    console.log(result.caseactionrec.Purchase_Order_Id__c);
                    console.log(component.get("v.CANumber"));
                    
                    //KIN 1339
                    if(result.caseactionrec.Purchase_Order_Id__c!=null){
                        component.set("v.CANumber",result.caseactionrec.Purchase_Order_Id__c);
                    }
                    //ends
                    
                    if(result.caseactionrec.Campaign!=null){
                        console.log('**VAl of campaign'+JSON.stringify(result.caseactionrec.Campaign));
                        component.set("v.campaignId",result.caseactionrec.Campaign.Id);
                         component.set("v.selectedLookupRecord",result.caseactionrec.Campaign);
                    }    
                    if(result.caseactionrec.Campaign!=null && result.caseactionrec.Campaign.StartDate!=null)
                         component.set("v.APbeginDate",result.caseactionrec.Campaign.StartDate);
                    if(result.caseactionrec.Campaign!=null && result.caseactionrec.Campaign.EndDate!=null)
                         component.set("v.APEndDate",result.caseactionrec.Campaign.EndDate);
                    if(result.caseactionrec.CloseDate!=null)  
                         component.set("v.closeDate",result.caseactionrec.CloseDate);
                    if(result.caseactionrec.End_Date__c!=null)
                        component.set("v.EndDate",result.caseactionrec.End_Date__c);
                    if(result.caseactionrec.Amount!=null)
                        component.set("v.amount",result.caseactionrec.Amount);
                    if(result.caseactionrec.Type_of_Case_Action__c!=null)
                        component.set("v.type",result.caseactionrec.Type_of_Case_Action__c);
                    if(result.caseactionrec.Day_of_Month__c!=null){
                        console.log('**Val of dayofmonth'+ result.caseactionrec.Day_of_Month__c);
                        component.set("v.MonthDay",result.caseactionrec.Day_of_Month__c);
                    }    
                    if(component.get("v.type")!=undefined && component.get("v.type")=='Ongoing' ){
                        component.set("v.IsOngoing",true);
                    }
                   /* if(result.caseactionrec.Parent_Recipient__c!=null) 
                        component.set("v.recipient",result.caseactionrec.Parent_Recipient__c); */
                    if(result.caseactionrec.Description!=null)
                        component.set("v.CaseNote",result.caseactionrec.Description);
                    if(result.caseactionrec.Category__c!=null && result.caseactionrec.Category__r.Category_Type__c!=null)  
                        component.set("v.categoryType",result.caseactionrec.Category__r.Category_Type__c);
                    //FOR MAKING END DATE MANDATORY OR NOT 
                   /* if((result.caseactionrec.Category__c!=null && result.caseactionrec.Category__r.Category_Type__c!=null &&
                      (result.caseactionrec.Category__r.Category_Type__c=='CSA' || result.caseactionrec.Category__r.Category_Type__c=='IVE'))
                      || (result.caseactionrec.Type_of_Case_Action__c!=undefined && result.caseactionrec.Type_of_Case_Action__c=='One time'))
                        component.set("v.IsEndDateRequired",true); */
                    if(result.caseactionrec.Category__c!=null && result.caseactionrec.Category__r.Category_Type__c!=null &&
                      (result.caseactionrec.Category__r.Category_Type__c=='CSA' || result.caseactionrec.Category__r.Category_Type__c=='IVE'))                      
                        component.set("v.IsEndDateRequired",true);
                 	
                    if(result.caseactionrec.Transaction_Code__c!=null && result.caseactionrec.Transaction_Code__c!=undefined &&
                       result.caseactionrec.Category__c!=null && result.caseactionrec.Category__r.Category_Type__c!=null && 
                       result.caseactionrec.Category__r.Category_Type__c=='Special Welfare'){
                        console.log('**enteredi n line 108');
                    	component.set("v.ShowSpecialWelfareButton",true); 
                        component.set("v.TransCodeSelected",result.caseactionrec.Transaction_Code__c);
                        if(result.specialWelfare!=null && result.specialWelfare!=undefined)
                            console.log('**Val of specialwelfare'+result.specialWelfare);
                            component.set("v.specialwelfare",result.speciaWelfare);
                        	
                    }                 
                    if(result.caseactionrec.Category__c!=null && result.caseactionrec.Category__r.Category_Type__c!=null && result.caseactionrec.Category__r.Category_Type__c=='IVE'){
                        if(result.caseactionrec.Parent_Recipient__c!=null){ 
                        	component.set("v.recipientIVE",result.caseactionrec.Parent_Recipient__c);
                            
                        }
                        if(result.caseactionrec.Fund__c!=null && result.caseactionrec.Fund__c!=undefined)
                        	component.set("v.FundIVE",result.caseactionrec.Fund__c);
                        
                        component.set("v.ShowCSAButton",false);
                        component.set("v.ShowIVEButton",true);
                        component.set("v.ShowFundButton",false);
                    }
                   
                    if(result.caseactionrec.Category__c!=null && result.caseactionrec.Category__r.RecordType!=undefined &&
                       result.caseactionrec.Category__r.RecordType.DeveloperName=='LASER_Category' && result.caseactionrec.Fund__c!=undefined
                      && result.caseactionrec.Category__r.Category_Type__c!='IVE'){
                       component.set("v.ShowFundButton",true); 
                        component.set("v.ShowCSAButton",false);
                        component.set("v.ShowIVEButton",false);
                        component.set("v.Fund",result.caseactionrec.Fund__c);
                    }
                    
                    if(result.caseactionrec.Category__c!=null && result.caseactionrec.Category__r.Category_Type__c!=null && result.caseactionrec.Category__r.Category_Type__c=='CSA'){
                        console.log('****Enterd in csa');
                        if(result.caseactionrec.Parent_Recipient__c!=null) {
                        	component.set("v.recipient",result.caseactionrec.Parent_Recipient__c);
                            //component.find("RecipientId").set("v.value",result.caseactionrec.Parent_Recipient__c);
                        }       
                        if(result.caseactionrec.Service_Placement_Type_Code__c!=null){ 
                       		component.set("v.SPTselected",result.caseactionrec.Service_Placement_Type_Code__c);
                           // component.find("sPTid").set("v.value",result.caseactionrec.Service_Placement_Type_Code__c);
                        }    
                        if(result.caseactionrec.Service_Placement_Type1__c!=null)  
                       		component.set("v.SPTselectedlabel",result.caseactionrec.Service_Placement_Type1__c);
                        
                        if(result.caseactionrec.Service_Name_Code_Value__c!=null){
                       		component.set("v.SPTNameselected",result.caseactionrec.Service_Name_Code_Value__c);
                           //component.find("SPTNameId").set("v.value",result.caseactionrec.Service_Name_Code_Value__c);
                        }     
                     if(result.caseactionrec.Service_Name_Code1__c!=null) 
                        component.set("v.SPTNameselectedlabel",result.caseactionrec.Service_Name_Code1__c);
                     if(result.caseactionrec.MandateType1__c!=null)   
                       component.set("v.MandateSelectedlabel",result.caseactionrec.MandateType1__c);
                        if(result.caseactionrec.Mandate_Type_Code__c!=null){  
                       		component.set("v.MandateSelected",result.caseactionrec.Mandate_Type_Code__c);
                            
                        }     
                        if(result.caseactionrec.Service_Description_Other__c!=null){
                        	component.set("v.Description",result.caseactionrec.Service_Description_Other__c);
                            //component.find("ServiceDescriptionId").set("v.value",result.caseactionrec.Service_Description_Other__c);
                        }    
                        console.log('00000000000');
                       
                        if(result.Names!=null && result.Names.length >0){
                            console.log('enterd here');
                        	 var sptvalues=[];
                            var sptnames = result.Names;
                            //GETTING SCROLLBAR ON ITEMS
                            var items = [];
                            var item = {                           
                                "label": "---None---",
                                "value": ''
                            };
                            items.push(item);
                            for (var i = 0; i < sptnames.length; i++) {
                                var item = {                           
                                    "label": sptnames[i].label,
                                    "value": sptnames[i].value.toString()
                                };
                                items.push(item);
                            }
                          
                            component.set("v.SPToptions",items);     
                        }
                        console.log('11111');
                        if(result.MandateNames!=null && result.MandateNames.length>0){
                            var mandatenames =  result.MandateNames;
                            var items=[];
                            var item = {                           
                                "label": "---None---",
                                "value": ''
                            };
                            items.push(item);
                            
                           for (var i = 0; i < mandatenames.length; i++) {
                                var item = {                           
                                    "label": mandatenames[i].label,
                                    "value": mandatenames[i].value.toString()
                                };
                                items.push(item);
                            } 
                            
                            component.set("v.Mandateoptions",items);                    
                        }
                        console.log('222222');
                        if(result.SPTNamesCodes!=null && result.SPTNamesCodes.length>0){
                            var sptvalues=[];
                            var sptnames = result.SPTNamesCodes;
                            //GETTING SCROLLBAR ON ITEMS
                            var items = [];
                            var item = {                           
                                "label": "---None---",
                                "value": ''
                            };
                            items.push(item);
                            for (var i = 0; i < sptnames.length; i++) {
                                var item = {                           
                                    "label": sptnames[i].label,
                                    "value": sptnames[i].value.toString()
                                };
                                items.push(item);
                            }
                          
                            component.set("v.SPTNames",items);  
                        } 
                        console.log('33333');                      
                        component.set("v.ShowCSAButton",true);
                        component.set("v.ShowIVEButton",false);
                        component.set("v.ShowFundButton",false); 
                    } //CSA IF ENDS HERE
                    //FOR RATE INFO                
                    if(result.caseactionrec.Prorated_Last_Payment_Amount__c!=null && 
                       result.caseactionrec.Prorated_First_Payment_Amount__c!=null &&
                      result.caseactionrec.Prorated_Last_Payment_Amount__c>0 &&   result.caseactionrec.Prorated_First_Payment_Amount__c>0){
                        component.set("v.IsShowRateInfo",true);
                        var categoryRateInfo ={};
                        console.log('^^^^^^ categoryratewrap');
                        console.log(JSON.stringify(result.categoryRateWrap));
                        console.log('***Sptcatreate');
                        console.log(JSON.stringify(result.SPTCatRate));
                        console.log('**CAseaction rec');
                        console.log(JSON.stringify(result.caseactionrec));
                        //console.log(result.categoryRateWrap.catRate.Price__c);
                        
                        if(result.categoryRateWrap!=null && result.categoryRateWrap.catRate!=null && result.caseactionrec.Amount==result.categoryRateWrap.catRate.Price__c){
                            console.log('**Entered in 126');
                            categoryRateInfo.Id= result.categoryRateWrap.catRate.Id;
                            categoryRateInfo.Name=result.categoryRateWrap.catRate.Name;
                        }
                        else if(result.SPTCatRate!=null && result.SPTCatRate.catRate!=null && result.SPTCatRate.catRate.Id!=null && result.SPTCatRate.catRate.Name!=null){
                        	categoryRateInfo.Id= result.SPTCatRate.catRate.Id;
                            categoryRateInfo.Name=result.SPTCatRate.catRate.Name;    
                        }
                        categoryRateInfo.Price=result.caseactionrec.Amount;                       
                        categoryRateInfo.Prorate_First_Payment__c=result.caseactionrec.Prorate_First_Payment__c;
                        categoryRateInfo.FirstProrateAmount=result.caseactionrec.Prorated_First_Payment_Amount__c;
                        categoryRateInfo.ProrateLastPayment=result.caseactionrec.Prorate_Last_Payment__c;
                        categoryRateInfo.LastProrateAmount=result.caseactionrec.Prorated_Last_Payment_Amount__c;
                        component.set("v.CatRateInfo",categoryRateInfo);
                        console.log('&&&&&'+JSON.stringify(categoryRateInfo));
                        component.set("v.IsAmountDisabled",true);
                    }
                    //CATEGORY RATE
                    if(result.categoryRateWrap!=null && result.categoryRateWrap.catRate!=null){
                    	component.set("v.categoryrate",result.categoryRateWrap.catRate); 
                        if(result.caseactionrec!=null && result.caseactionrec!=undefined && 
                           result.caseactionrec.Amount!=undefined && result.categoryRateWrap.catRate.Price__c!=undefined 
                          && result.caseactionrec.Amount == result.categoryRateWrap.catRate.Price__c)
                            component.set("v.IsAmountDisabled",true);
                    }
                    if(result.SPTCatRate!=null && result.SPTCatRate.catRate!=undefined && result.SPTCatRate.catRate!=null){
                        console.log('**Enterd in sptcatrate part');
                         component.set("v.categoryrate",result.SPTCatRate.catRate);
                         if(result.caseactionrec!=null && result.caseactionrec!=undefined && 
                           result.caseactionrec.Amount!=undefined && result.SPTCatRate.catRate.Price__c!=undefined 
                          && result.caseactionrec.Amount == result.SPTCatRate.catRate.Price__c)
                            component.set("v.IsAmountDisabled",true);
                       
                    } 
            	}	
                if(component.get("v.opportunityStage")=='Open' && component.get("v.IsPaidPayments")==false && component.get("v.IsDesiredProfiles")==true)
                    component.set("v.IsOpenAllConditions",true);   
            }
             else if(state==="ERROR"){
             	const errors = response.getError();
                 console.log('**vAL OF ERROR'+errors[0].message);    
             }
        });
        $A.enqueueAction(action); 
		
	},
    FetchRates : function(component,event,helper) {
        console.log('ftech rates');
        console.log(component.get("v.category"));
		var action=component.get("c.fetchCategoryRates");
        action.setParams({
            categoryId : String(component.get("v.category")),
            caseId : String(component.get("v.case"))
            
        });
         action.setCallback(this,function(response){           
            var state = response.getState();
            console.log('**State val'+state);
            if(state==="SUCCESS"){
                 var result=response.getReturnValue();
                console.log('**Cat create result'+JSON.stringify(result));
                if(result.Catrec!=null && result.Catrec.Category_Type__c!=null){
                    console.log('**Entered in first');
                    component.set("v.categoryType",result.Catrec.Category_Type__c);
                    if(result.Catrec.Category_Type__c=='CSA' || result.Catrec.Category_Type__c=='IVE'){
                    	component.set("v.IsEndDateRequired",true);    
                    }
                    else{
                        component.set("v.IsEndDateRequired",false);  
                        if((component.get("v.EndDate")=='' || component.get("v.EndDate")==undefined)
                          && component.get("v.APEndDate")!='' && component.get("v.APEndDate")!=undefined )
                            component.set("v.EndDate",component.get("v.APEndDate"));
                    }
                    //ADDED TO MAKE END DATE REQUIRED FOR THESE TYPE OF CATEGORY
                   /* if(result.Catrec.Category_Type__c=='CSA' || result.Catrec.Category_Type__c=='IVE'){
                    	component.set("v.IsEndDateRequired",true);    
                    }
                    else{
                        if((component.get("v.type")!=undefined && component.get("v.type")!='' &&
                        component.get("v.type")!='One time') || (component.get("v.type")==undefined ||
                        component.get("v.type")=='' ))
                        	component.set("v.IsEndDateRequired",false);
                    } */
                    
                    if((result.Catrec.Category_Type__c)=='CSA'){
                        console.log('**Entered here');
                        component.set("v.ShowCSAButton",true);
                        component.set("v.ShowIVEButton",false);
                        component.set("v.ShowFundButton",false);
                        component.set("v.ShowSpecialWelfareButton",false);
                        //ADDED 
                        if(component.get("v.SPTselected")!='' && component.get("v.SPTselected")!=undefined)
                            this.FetchSPTRate(component,event,helper);
                    }
                    else if(result.Catrec.Category_Type__c=='IVE'){
                        component.set("v.ShowCSAButton",false);
                        component.set("v.ShowIVEButton",true);
                         component.set("v.ShowFundButton",false);
                         component.set("v.ShowSpecialWelfareButton",false);
                        component.set("v.Fund",'Reimbursable');
                    }  
                    else if(result.Catrec.RecordType!=undefined && result.Catrec.RecordType.DeveloperName!=undefined && result.Catrec.RecordType.DeveloperName=='LASER_Category'
                         && result.Catrec.Category_Type__c!='IVE' ){
                    	component.set("v.ShowCSAButton",false);
                        component.set("v.ShowFundButton",true);
                        component.set("v.ShowIVEButton",false); 
                        component.set("v.ShowSpecialWelfareButton",false);
                        component.set("v.Fund",'Reimbursable');
                    }
                    else if(result.Catrec.RecordType!=undefined && result.Catrec.Category_Type__c=='Special Welfare'){
                    	component.set("v.ShowCSAButton",false);
                        component.set("v.ShowFundButton",false);
                        component.set("v.ShowIVEButton",false); 
                        this.CheckForSpecialWelfareAccount(component,event,helper);
                       // component.set("v.ShowSpecialWelfareButton",true);
                            
                    }
                    else{
                    	component.set("v.ShowCSAButton",false);
                        component.set("v.ShowIVEButton",false); 
                        component.set("v.ShowFundButton",false);
                        component.set("v.ShowSpecialWelfareButton",false);
                   }                   
                }
                if(result.Catrec==null){
                    component.set("v.ShowCSAButton",false);
                }
                if(result.catRate!=null){
                    console.log('**Enterd in catrate too');
                    component.set("v.categoryrate",result.catRate);
                    component.set("v.amount",result.catRate.Price__c);
                    component.set("v.IsAmountDisabled",true);
                    //CALLING HELPER FOR SHOWING RATE INFO
                    this.checkCategoryRate(component,event,helper,component.get("v.categoryrate"));
                }
                else{
                	console.log('cat rate null else');
                    component.set("v.IsAmountDisabled",false);
                    component.set("v.categoryrate",'');
                    this.checkCategoryRate(component,event,helper,component.get("v.categoryrate"));
                }
            }
             else{
                 const errors = response.getError();
                 console.log(errors[0].message);
             }
         });
        $A.enqueueAction(action);     
	},
    getCategoryandSPTType : function(component,event,helper){
        var action= component.get("c.getCategoryandSPTType");
        action.setParams({
        	recordid : component.get("v.category")
        });	
        action.setCallback(this,function(response){            
            var state = response.getState();            
            if (state === "SUCCESS") {
                console.log('**Val of success');
                var result=response.getReturnValue();
                console.log(JSON.stringify(response.getReturnValue())); 
                  //FOR MANDATE OPTIONS
               if((result.MandateNames!=undefined && result.MandateNames.length==0) || (result.Names!=undefined && result.Names.length==0)){
                	component.set("v.openModal",false);
                    this.showToast('Error','Error','Please contact your administrator to provide Mandate Types, Service Placement Types and Service Name Code for this category if not added or use a different category.');
                }
               else{ 
                   if(result.MandateNames!=null && result.MandateNames.length>0){
                        var mandatenames = response.getReturnValue().MandateNames;
                        var items=[];
                        var item = {                           
                            "label": "---None---",
                            "value": ''
                        };
                        items.push(item);
                        
                       for (var i = 0; i < mandatenames.length; i++) {
                            var item = {                           
                                "label": mandatenames[i].label,
                                "value": mandatenames[i].value.toString()
                            };
                            items.push(item);
                        } 
                        
                        component.set("v.Mandateoptions",items);                    
                    }  
                    //KIN 237
                    if(result.MandateDesc!=null && result.MandateDesc.length>0){
                        var Mandatevalues=[];
                        var Mandatenames = response.getReturnValue().MandateDesc;
                        //GETTING SCROLLBAR ON ITEMS
                        var items = [];
                        /*var item = {                           
                            "label": "",
                            "value": ''
                        };
                        items.push(item);*/
                        console.log('desc Mandate'+Mandatenames);
                        console.log(Mandatenames[0].label);
                        console.log(Mandatenames[0].value);
                        for (var i = 0; i < Mandatenames.length; i++) {
                            var item = {                           
                                "label": Mandatenames[i].label,
                                "value": Mandatenames[i].value.toString()
                            };
                            items.push(item);
                        }                      
                        component.set("v.Mandatedescmap",items);  
                    } 
                    
                    //ends
                    if(result.Names!=null && result.Names.length>0){
                        var sptvalues=[];
                        var sptnames = response.getReturnValue().Names;
                        //GETTING SCROLLBAR ON ITEMS
                        var items = [];
                        var item = {                           
                            "label": "---None---",
                            "value": ''
                        };
                        items.push(item);
                        for (var i = 0; i < sptnames.length; i++) {
                            var item = {                           
                                "label": sptnames[i].label,
                                "value": sptnames[i].value.toString()
                            };
                            items.push(item);
                        }
                      
                        component.set("v.SPToptions",items);  
                    }
                   // KIN 237
                    if(result.SPTDesc!=null && result.SPTDesc.length>0){
                        var sptvalues=[];
                        var sptnames = response.getReturnValue().SPTDesc;
                        //GETTING SCROLLBAR ON ITEMS
                        var items = [];
                        /*var item = {                           
                            "label": "",
                            "value": ''
                        };
                        items.push(item);*/
                        console.log('desc'+sptnames);
                        console.log(sptnames[0].label);
                        console.log(sptnames[0].value);
                        for (var i = 0; i < sptnames.length; i++) {
                            var item = {                           
                                "label": sptnames[i].label,
                                "value": sptnames[i].value.toString()
                            };
                            items.push(item);
                        }                      
                        component.set("v.SPTdescmap",items);  
                    } 
                    // ends
               }
            }   
        });        
        $A.enqueueAction(action);     
    },
    //KIN 237
    getSNCDesc : function(component,event,helper,selectedOptionValue){
        console.log('inside getSNCDesc');
        var action= component.get("c.getSNCDesc");
        	action.setParams({
            	sncValue: selectedOptionValue
        	});	   
            action.setCallback(this, function(response){
            	var state = response.getState();
            	console.log('**VAl of state'+state);
            	console.log('***response'+response.getReturnValue());
            	if (state === "SUCCESS") { 
                	var sncvalues=[];
                    var sncdesc = response.getReturnValue();
                     console.log('***Sncnames'+sncdesc);
                    if(sncdesc==null){
                    	component.set("v.showSNCDesc",false);
                    }
                   else{
                      
                        component.set("v.selSNCDesc",sncdesc);    
                       component.set("v.showSNCDesc",true);
                   }
                }
            
            });     
    	$A.enqueueAction(action);
    },
    getSPTNamesforSPT : function(component,event,helper,selectedOptionValue){
        console.log('***Entered here');
        var action= component.get("c.getSPTNames");
        	action.setParams({
            	sptValue: selectedOptionValue
        	});	   
            action.setCallback(this, function(response){
            	var state = response.getState();
            	console.log('**VAl of state'+state);
            	console.log('***response'+response.getReturnValue());
            	if (state === "SUCCESS") { 
                	var sptvalues=[];
                    var sptnames = response.getReturnValue();
                    if(sptnames==null){
                    	component.set("v.openModal",false);
                    	this.showToast('Error','Error','Please contact your administrator to provide Service Name Code for this selected Service Placement Type if not added or use a different Category.');    
                    }
                    else{ 
                         //GETTING SCROLLBAR ON ITEMS
                        var items = [];
                         var item = {                           
                                "label": "---None---",
                                "value": ''
                        };
                        items.push(item);
                        for (var i = 0; i < sptnames.length; i++) {
                            var item = {                           
                                "label": sptnames[i].label,
                                "value": sptnames[i].value.toString()
                            };
                            items.push(item);
                        } 
                      
                        component.set("v.SPTNames",items);    
                        //CHECK THAT ALREADY SELECTED VALUE PRESENT ON LIST IF NOT THEN CLEAR THE VALUE OF SELECTED
                        var SPTNameoptions = component.get("v.SPTNames");
                        var alreadySelectedSPTName = component.get("v.SPTNameselected");
                        console.log('**Already selected sptname'+alreadySelectedSPTName);
                        console.log('sptnamelabel'+component.get("v.SPTNameselectedlabel"));
                        var isPresent=false;
                        console.log('**Sptname options'+SPTNameoptions);
                        for(var i=0; i< SPTNameoptions.length; i++){
                            if(alreadySelectedSPTName==SPTNameoptions[i].value){
                                isPresent=true;
                                break;
                            }
                        }
                        if(isPresent==false){
                            component.set("v.SPTNameselected",'');
                            component.set("v.SPTNameselectedlabel",'');
                        }     
                    } 
                }     
         });     
    	$A.enqueueAction(action);     
    },
    checkCSASave : function(component,event,helper){
        component.set("v.CSAError",false);
        console.log('sptnames'+JSON.stringify(component.get("v.SPTNames")));
        var sptval = component.get("v.SPTselected");
        var sptname = component.get("v.SPTNameselected");
        var mandateval = component.get("v.MandateSelected");
        var sptlabel= component.get("v.SPTselectedlabel");
        var sptnamelabel= component.get("v.SPTNameselectedlabel");
        var mandatelabel= component.get("v.MandateSelectedlabel");
         var recipientval = component.get("v.recipient");
        console.log('**VAl of sptval'+sptval);
        console.log('**val of sptname'+sptname);
        console.log('**Val of sptname selectedlabel'+sptnamelabel);
        console.log('**Desc val'+ component.get("v.Description"));
        if(sptname!='24'){
       		component.set("v.Description",'');
        } 
        if(sptval=='' || sptval==undefined){
        	component.set("v.ShowSpinner",false);
            sptname='';
            component.set("v.SPTNameselected",sptname);
            component.set("v.SPTselectedlabel",sptname);
            component.set("v.CSAError",true);
        	this.showToast('Error', 'Error', 'Please add Service Placement Type(SPT). If SPT is unavailable, please contact your administrator to add SPT for this category or choose a different category instead.');     
        }
        if(sptname=='' || sptname==undefined){
           component.set("v.ShowSpinner",false);
           component.set("v.CSAError",true);             
            this.showToast('Error', 'Error', 'Please add Service Name Code. If Service Name Code is unavailable, please contact your administrator or choose a different Service Placement Type(SPT).');     
        }
        if(mandateval=='' || mandateval==undefined){
        	 component.set("v.ShowSpinner",false);
            component.set("v.CSAError",true);
            this.showToast('Error', 'Error', 'Please add Mandate Type. If Mandate Type is unavailable, please contact your administrator to add the Mandate Type for this category or choose a different category.');
        }
        if (sptname=='24' && (component.get("v.Description")=='' || component.get("v.Description")==undefined)){
            console.log('****@@@**');
            component.set("v.ShowSpinner",false);
            component.set("v.CSAError",true);
        	this.showToast('Error', 'Error', 'Please add Description.'); 
            
        } 
   		if(recipientval=='' || recipientval==undefined || recipientval=='0'){
        	component.set("v.ShowSpinner",false);
            component.set("v.CSAError",true);
        	this.showToast('Error', 'Error', 'Please add recipient.'); 
              
        }
        else if(component.get("v.costcenterSelection")==undefined || component.get("v.costcenterSelection")==''){
         	component.set("v.ShowSpinner",false);
            component.set("v.CSAError",true);
        	this.showToast('Error', 'Error', 'Please add Cost Center.');   
        }
           
    },
    checkIVESave : function(component,event,helper){
    	 component.set("v.CSAError",false);
         var iveRecipient = component.get("v.recipientIVE");
         if(iveRecipient=='' || iveRecipient==undefined || iveRecipient=='0'){
        	component.set("v.ShowSpinner",false);
            component.set("v.CSAError",true);
        	this.showToast('Error', 'Error', 'Please add recipient.');               
        }
         else if(component.get("v.costcenterSelection")==undefined || component.get("v.costcenterSelection")==''){
         	component.set("v.ShowSpinner",false);
            component.set("v.CSAError",true);
        	this.showToast('Error', 'Error', 'Please add Cost Center.');   
        }
    },
    showToast : function(type, title, message) { 
        if(type.toUpperCase() == 'ERROR'){
            type = 'info';
        }
     
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : title,
            message: message,
            duration:'10000',
            key: 'info_alt',
            type: type,
            mode: 'pester'
        });
        toastEvent.fire();
    },
    showIncreaseToast : function(type, title, message) {
        if(type.toUpperCase() == 'ERROR'){
            type = 'info';
        }
               
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : title,
            message: message,
            duration:' 10000',
            key: 'info_alt',
            type: type,
            mode: 'pester'
        });
        toastEvent.fire();
    } ,
    CSABlank : function(component,event,helper){
        component.set("v.SPTselected",'');
        component.set("v.SPTNameselected",'');
        component.set("v.MandateSelected",'');
        component.set("v.Description",'');
        component.set("v.SPTselectedlabel",'');
        component.set("v.SPTNameselectedlabel",'');
        component.set("v.MandateSelectedlabel",'');
        component.set("v.recipient",'');
        component.set("v.CSAError",false);
        // KIN 228
        component.set("v.AgeError",false);
        component.set("v.AgeError1",false);
        component.set("v.AgeError2",false);
        //ends
        component.set("v.recipientIVE",false);
    },
     cancelForm : function(component,event,helper){
         this.redirecttoRecord(component,event,helper,component.get("v.recordId"));
      
    },
    validateForm : function(component,event,helper){
        console.log('**VAlidate form');
        var caseid = component.get("v.case");
        var categoryType = component.get("v.categoryType");   
        console.log('********'+categoryType);
        console.log(component.get("v.SPTNameselected"));
        if(component.get("v.category")=='' || component.get("v.category")==undefined){
        	component.set("v.POError",true);
            component.set("v.POSpinner",false);
            this.showToast('Error', 'Add Category', 'Please provide a category.');           
            
        }
       /*else if(categoryType!=undefined && (categoryType=='CSA' || categoryType=='IVE') && (component.get("v.recipient")=='' || 
        component.get("v.recipient")==undefined)){
        	 component.set("v.POError",true);
             component.set("v.POSpinner",false);
             this.showToast('Error', 'Add Parent Recipient', 'Please provide a value for the Parent Recipient.');       
        } */
       else if(categoryType!=undefined && categoryType=='CSA' && (component.get("v.SPTselected")=='' || component.get("v.SPTselected")==undefined || component.get("v.SPTNameselected")=='' ||
              component.get("v.SPTNameselected")==undefined || component.get("v.MandateSelected")=='' || 
              component.get("v.MandateSelected")==undefined || component.get("v.recipient")==undefined || component.get("v.recipient")=='0' 
               || component.get("v.recipient")=='')){
                console.log('*enterd in first if');
                component.set("v.POError",true);
                component.set("v.POSpinner",false);
            	this.showToast('Error', 'Add CSA Types', 'Please click the CSA button to add CSA values.');   
        }
        
       else if(categoryType!=undefined && categoryType=='IVE' &&  (component.get("v.recipientIVE")==undefined || component.get("v.recipientIVE")=='' 
            || component.get("v.recipientIVE")=='0')){
       	    console.log('*enterd in ive if');
           component.set("v.POError",true);
           component.set("v.POSpinner",false);
           this.showToast('Error', 'Add Parent Recipient', 'Please click IVE CA button to add parent recipient values.');       
       }         
       else if (component.get("v.case")=='' || component.get("v.case")==undefined){
            component.set("v.POSpinner",false);
        	this.showToast('Error','Error','Please provide a Case.');            
            component.set("v.POError",true);
        }
       else if(component.get("v.vendor")=='' || component.get("v.vendor")==undefined){
             component.set("v.POSpinner",false);
        	this.showToast('Error','Error','Please provide a Vendor.');             
            component.set("v.POError",true);
        }
           else if(component.get("v.campaignId")=='' || component.get("v.campaignId")==undefined){
           	 component.set("v.POSpinner",false);
        	this.showToast('Error','Error','Please provide a Fiscal Year.');             
            component.set("v.POError",true);    
           } 
       else if(component.get("v.amount")=='' || component.get("v.amount")==undefined){
            console.log('**Entered in amount error');
             component.set("v.POSpinner",false);
        	this.showToast('Error','Error','Please provide an Amount.');            
            component.set("v.POError",true);
            console.log('***')
        }
       else if(component.get("v.closeDate")=='' || component.get("v.closeDate")==undefined){
            component.set("v.POSpinner",false);
        	this.showToast('Error','Error','Please provide a Begin Date.');              
            component.set("v.POError",true);
        }
       else if((component.get("v.EndDate")=='' || component.get("v.EndDate")==undefined) && 
               component.get("v.categoryType")!='' && component.get("v.categoryType")!=undefined &&
              (component.get("v.categoryType")=='CSA' || component.get("v.categoryType")=='IVE')){
        	 component.set("v.POSpinner",false);
        	this.showToast('Error','Error','Please provide an End Date');              
            component.set("v.POError",true);       
        }
         //ADDED FOR COST CENTER
        else if(component.get("v.category")!=undefined && component.get("v.category")!='' && 
        	component.get("v.FIPSID")!=undefined && component.get("v.FIPSID")!='' && 
            (component.get("v.costcenterSelection")==undefined ||  component.get("v.costcenterSelection")=='')){
        	 component.set("v.POSpinner",false);
        	this.showToast('Error','Error','Please provide Cost Center');              
            component.set("v.POError",true);     
        }
      /* else if((component.get("v.EndDate")=='' || component.get("v.EndDate")==undefined) && 
              ((component.get("v.type")!='' && component.get("v.type")!=undefined && component.get("v.type")=='One time') || 
              (component.get("v.categoryType")!='' && component.get("v.categoryType")!=undefined &&
              (component.get("v.categoryType")=='CSA' || component.get("v.categoryType")=='IVE')))){
            component.set("v.POSpinner",false);
        	this.showToast('Error','Error','Please provide an End Date');               
            component.set("v.POError",true);
        } */
        else if(component.get("v.type")=='' || component.get("v.type")==undefined){
              component.set("v.POSpinner",false);
        	this.showToast('Error','Error','Please provide a Type.');
            component.set("v.POError",true);
        }
        else if(component.get("v.type")!=undefined && component.get("v.type")=='Ongoing' && component.get("v.MonthDay")==''){
                component.set("v.POSpinner",false);
                this.showToast('Error','Error','Please provide Day of the Month.');
                component.set("v.POError",true);    
        }
       else if(component.get("v.closeDate")!='' && component.get("v.closeDate")!=undefined && 
               component.get("v.EndDate")!='' && component.get("v.EndDate")!=undefined && 
                component.get("v.closeDate") > component.get("v.EndDate")){
            component.set("v.POSpinner",false);
            component.set("v.POError",true);
            this.showToast('Error','Error','Case Action End Date should be greater than Case Action Begin Date.');
         } 
        //FOR CATEGORY EQUAL TO CSA KIN -952 MODIFICATION
         else if(component.get("v.closeDate")!='' && component.get("v.closeDate")!=undefined &&
             component.get("v.APbeginDate")!='' && component.get("v.APEndDate")!='' && 
             component.get("v.categoryType")!='' && component.get("v.categoryType")!=undefined &&
            component.get("v.categoryType")=='CSA' && 
              (component.get("v.closeDate") < component.get("v.APbeginDate") || 
               component.get("v.closeDate") > component.get("v.APEndDate"))){
            component.set("v.POSpinner",false);
             component.set("v.POError",true);
             this.showToast('Error','Error','Case Action Begin Date should lie between its Fiscal Year Begin and End dates');
        }
        else if(component.get("v.closeDate")!='' && component.get("v.closeDate")!=undefined &&
             component.get("v.APEndDate")!='' &&
             component.get("v.categoryType")!='' && component.get("v.categoryType")!=undefined &&
            component.get("v.categoryType")!='CSA' &&                
             component.get("v.closeDate") > component.get("v.APEndDate")){
             component.set("v.POSpinner",false);
             component.set("v.POError",true);
             this.showToast('Error','Error','Case Action Begin Date should not be greater than its Fiscal Year End date');
        }
        //KIN 952 MODIFICATION ENDS HERE
        else if(component.get("v.EndDate")!='' && component.get("v.EndDate")!=undefined &&
              component.get("v.APEndDate")!='' && component.get("v.EndDate") > component.get("v.APEndDate")) {
            component.set("v.POSpinner",false);
             component.set("v.POError",true);
             this.showToast('Error','Error','Case Action End date should lies between Case Action Begin Date and its Fiscal Year End date');
        } 
         else if(component.get("v.categoryType")!=undefined && component.get("v.categoryType")=='Special Welfare' &&
               component.get("v.specialwelfare")!=undefined && component.get("v.specialwelfare")!=null && 
               component.get("v.ShowSpecialWelfareButton")!=undefined && component.get("v.ShowSpecialWelfareButton")==true &&
               (component.get("v.TransCodeSelected")=='None' || component.get("v.TransCodeSelected")==undefined || component.get("v.TransCodeSelected")=='')){
               component.set("v.POSpinner",false);
            component.set("v.POError",true);
            this.showToast('Warning','Warning','Select the SWF button next to the Category field to fill in required information for this Case Action.');
        }
        //CHECK FOR AMOUNT WITH CURRENT BALANCE OF SPECIAL WELFARE
        else if(component.get("v.categoryType")!=undefined && component.get("v.categoryType")=='Special Welfare' &&
               component.get("v.specialwelfare")!=undefined && component.get("v.specialwelfare")!=null && 
               component.get("v.ShowSpecialWelfareButton")!=undefined && component.get("v.ShowSpecialWelfareButton")==true &&
               component.get("v.amount")!=undefined && component.get("v.Currentbalance")!=undefined && 
               component.get("V.amount") > component.get("v.Currentbalance")){
               component.set("v.POSpinner",false);
            component.set("v.POError",true);
            this.showToast('Error','Error','Amount should be less than or Equal Current Balance of Special Welfare Account. Current Balance of Special Welfare Account is :'+ component.get("v.Currentbalance"));
        }
        else if(caseid!='' && caseid!=undefined && caseid!=null && component.get("v.category")!=undefined 
               && component.get("v.category")!=''){
            console.log('**enterd her in case');
             component.set("v.OasisShow",false);
            component.set("v.IsStudentIdRequired",false);
            component.set("v.Referal",'');
            var action = component.get("c.getCase"); 
        	action.setParams({ caseId : caseid.toString(),accountId :component.get("v.vendor")});
            
         	action.setCallback(this,function(response){           
            var state = response.getState();
            console.log('**State val'+state);
            if(state==="SUCCESS"){
                 var result=response.getReturnValue(); 
                 console.log(JSON.stringify(result));
                if(result!=null){
                     if(result.caseval!=null && result.caseval!=undefined)
                    	component.set("v.CaseValue",result.caseval);
                     if(result.accountVal!= null && result.accountVal!=undefined && result.accountVal.Tax_ID__c!=null);
                    	component.set("v.TaxId",result.accountVal.Tax_ID__c);
                    var caseval=component.get("v.CaseValue");
                    console.log('**VAl of caeval'+JSON.stringify(caseval));
                    if(caseval!=null && caseval!=undefined){
                        if(caseval.Primary_Contact__r!=undefined && caseval.Primary_Contact__r.FirstName !=undefined)
                            component.set("v.ConFirstName",caseval.Primary_Contact__r.FirstName);
                        if(caseval.Primary_Contact__r!=undefined && caseval.Primary_Contact__r.LastName !=undefined)
                            component.set("v.ConLastName",caseval.Primary_Contact__r.LastName);
                        if(caseval.DOB__c!=undefined && caseval.DOB__c!=null)
                            component.set("v.DOB",caseval.DOB__c);
                        if(caseval.Race__c!=undefined)
                            component.set("v.Race",caseval.Race__c);
                        if(caseval.Hispanic_Ethnicity__c !=undefined)
                            component.set("v.HispanicEthnicity",caseval.Hispanic_Ethnicity__c);
                        if(caseval.Gender_Preference__c !=undefined)
                            component.set("v.Gender",caseval.Gender_Preference__c);
                        if(caseval.OASIS_Client_ID__c!=undefined){
                            component.set("v.OasisClinetId",caseval.OASIS_Client_ID__c);
                             component.set("v.CSAoasisclientId",caseval.OASIS_Client_ID__c);
                        }    
                        if(caseval.Title_IVE_Eligibility__c!=undefined)
                            component.set("v.TitleIVE", caseval.Title_IVE_Eligibility__c);
                        if(caseval.DSM_V_Indicator_ICD_10_1__c!=undefined)
                            component.set("v.DSM", caseval.DSM_V_Indicator_ICD_10_1__c);
                         if(caseval.Clinical_Medication_Indicator_1__c!=undefined)
                            component.set("v.Clinical", caseval.Clinical_Medication_Indicator_1__c);
                         if(caseval.Medicaid_Indicator_1__c!=undefined)
                            component.set("v.Medical", caseval.Medicaid_Indicator_1__c);
                         if(caseval.Referral_Source__c!=undefined)
                            component.set("v.Referal", caseval.Referral_Source__c);
                         if(caseval.Autism_Flag_1__c!=undefined)
                            component.set("v.Autism", caseval.Autism_Flag_1__c);
                        if(caseval.ssn__c!=undefined)
                            component.set("v.SSN",caseval.ssn__c);
                        if(caseval.Student_Identifier__c !=undefined)
                            component.set("v.studentidentifier",caseval.Student_Identifier__c);
                        if(result.optionValues!=undefined && result.optionValues.length >0){
                        	
                        	var values=[];
                        	var options = result.optionValues;
                            //GETTING SCROLLBAR ON ITEMS
                            var items = [];
                            var item = {                           
                                "label": "---None---",
                                "value": ''
                            };
                            items.push(item);
                            for (var i = 0; i < options.length; i++) {
                                var item = {                           
                                    "label": options[i].label,
                                    "value": options[i].value.toString()
                                };
                                items.push(item);
                            }
                          
                            component.set("v.referaloptions",result.optionValues); 
                    	}     
                    }
                    
                    //CHECK FOR CATEGORY AND CASE VALUE
                    if(component.get("v.category")!='' && component.get("v.category")!=undefined && 
                       component.get("v.categoryType")!='' && component.get("v.categoryType")!=undefined){
                        if(component.get("v.categoryType")=='CSA' && ((component.get("v.TaxId")==undefined || component.get("v.TaxId")=='') || caseval.Primary_Contact__c==undefined || caseval.Primary_Contact__r.LastName==null || caseval.Primary_Contact__r.FirstName==null
                         || caseval.DOB__c==null || caseval.Gender_Preference__c==null || caseval.Race__c==null || caseval.Autism_Flag_1__c==null ||
                          caseval.DSM_V_Indicator_ICD_10_1__c ==null || caseval.Clinical_Medication_Indicator_1__c==null || caseval.Medicaid_Indicator_1__c==null || 
                        caseval.Referral_Source__c==null ||  caseval.Hispanic_Ethnicity__c ==null || caseval.Primary_Contact__r.LastName==undefined || caseval.Primary_Contact__r.FirstName==undefined
                        || caseval.DOB__c==undefined || caseval.Gender_Preference__c==undefined || caseval.Race__c==undefined || caseval.Autism_Flag_1__c==undefined ||
                         caseval.DSM_V_Indicator_ICD_10_1__c ==undefined || caseval.Clinical_Medication_Indicator_1__c==undefined || caseval.Medicaid_Indicator_1__c==undefined || 
                           caseval.Referral_Source__c==undefined ||  caseval.Hispanic_Ethnicity__c ==undefined || caseval.Title_IVE_Eligibility__c==undefined || caseval.Title_IVE_Eligibility__c=='' ||
                          (component.get("v.SPTselected")!=undefined && (component.get("v.SPTselected")=='6' || component.get("v.SPTselected")=='7' || component.get("v.SPTselected")=='18') &&
                         (caseval.Student_Identifier__c==undefined || caseval.Student_Identifier__c==null || caseval.Student_Identifier__c=='')) || ((caseval.Student_Identifier__c==undefined || caseval.Student_Identifier__c==null || caseval.Student_Identifier__c=='' ) &&
                         (caseval.ssn__c==undefined || caseval.ssn__c==null || caseval.ssn__c=='')) || (component.get("v.MandateSelected")!=undefined && (component.get("v.MandateSelected")=='2' || component.get("v.MandateSelected")=='3' 
                           || component.get("v.MandateSelected")=='6' ||  component.get("v.MandateSelected")=='7' || component.get("v.MandateSelected")=='8') && (caseval.OASIS_Client_ID__c==undefined || caseval.OASIS_Client_ID__c==null || caseval.OASIS_Client_ID__c==''))
                        )){
                            if(component.get("v.MandateSelected")!=undefined && (component.get("v.MandateSelected")=='2' || component.get("v.MandateSelected")=='3' || component.get("v.MandateSelected")=='6' ||  component.get("v.MandateSelected")=='7'
                            || component.get("v.MandateSelected")=='8') && (caseval.OASIS_Client_ID__c==undefined || caseval.OASIS_Client_ID__c==null || caseval.OASIS_Client_ID__c==''))
                                component.set("v.OasisShow",true);
                            
                            if (component.get("v.SPTselected")!=undefined && (component.get("v.SPTselected")=='6' || component.get("v.SPTselected")=='7' || component.get("v.SPTselected")=='18') &&
                         	(caseval.Student_Identifier__c==undefined || caseval.Student_Identifier__c==null || caseval.Student_Identifier__c==''))
                                component.set("v.IsStudentIdRequired",true);
                         	component.set("v.POSpinner",false);
        					this.showToast('Error','Error','To proceed, please populate the required fields on the Case Record.');
            				component.set("v.POError",true); 
                            component.set("v.IsCSACase",true);
                             component.set("v.showCSACase",true);
                        }
                        
                         if (component.get("v.categoryType")=='IVE' && (caseval.Primary_Contact__c==undefined || caseval.Primary_Contact__r.LastName==null || caseval.Primary_Contact__r.FirstName==null
                              || caseval.DOB__c==null || caseval.Gender_Preference__c==null || caseval.Race__c==null || caseval.OASIS_Client_ID__c==null ||
                              caseval.Hispanic_Ethnicity__c ==null || caseval.Primary_Contact__r.LastName==undefined || caseval.Primary_Contact__r.FirstName==undefined
                               || caseval.DOB__c==undefined || caseval.Gender_Preference__c==undefined || caseval.Race__c==undefined || caseval.OASIS_Client_ID__c==undefined ||
                                caseval.Hispanic_Ethnicity__c ==undefined || caseval.Title_IVE_Eligibility__c==undefined || caseval.Title_IVE_Eligibility__c=='' ||
                                     (component.get("v.TaxId")==undefined || component.get("v.TaxId")=='') || caseval.Medicaid_Indicator_1__c==undefined || caseval.Medicaid_Indicator_1__c=='' ||
                                caseval.Medicaid_Indicator_1__c==null ||  ((caseval.Student_Identifier__c==undefined || caseval.Student_Identifier__c==null || caseval.Student_Identifier__c=='' ) &&
                         		(caseval.ssn__c==undefined || caseval.ssn__c==null || caseval.ssn__c=='')))){
                         	component.set("v.POSpinner",false);
        					this.showToast('Error','Error','To proceed, please populate the required fields on the Case Record.');
            				component.set("v.POError",true); 
                             component.set("v.isIVECase",true);
                              component.set("v.showCSACase",true);
                         }
                        
                    }
                                      
                    //SENDING FOR SAVE IF NO ERROR FOUND
                    if(component.get("v.POError")==false){
                        console.log('**VAl of save'+component.get("v.Save"));
                        if(component.get("v.Save")=='Save'){
                       		this.callSaveApex(component,event,helper,false);
                        }
                        else
                           this.callSaveApex(component,event,helper,true); 
                    }
                }    
                else{
                     component.set("v.ShowSpinner",false);
             		component.set("v.POError",true);
                    this.showToast('Error','Error','Unable to find case record');
                }   
            }  
              else if(state==="ERROR"){
                    component.set("v.CaseValue",'');
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                     errors[0].message);
                             component.set("v.ShowSpinner",false);
                             component.set("v.POError",true);
                              this.showToast('Error','Error',errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                        component.set("v.ShowSpinner",false);
                        component.set("v.POError",true);
                        this.showToast('Error','Error','Unknown error');
                	}               
         		}
         	});
        	$A.enqueueAction(action);  
        }

        
    },
     redirecttoRecord : function(component,event,helper,recordid){
         var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": recordid,
            "slideDevName": "related"
        });
        navEvt.fire();
        
    },
    checkCategoryRate : function(component,event,helper,categoryRate){
        console.log('entered in check category rate');
        var amount= component.get("v.amount");
        var beginDate = component.get("v.closeDate");
        var endDate = component.get("v.EndDate");
        console.log('**VAl of enddate'+endDate);
        var catRateInfo ={};
        console.log('**Val of category rate'+categoryRate);
        if(categoryRate!='' && categoryRate!=undefined){
            if(amount!='' && amount!=null && beginDate!='' && endDate!='' && endDate!=null &&
               amount!=undefined && beginDate!=undefined && endDate!=undefined 
               && beginDate!=null && component.get("v.type")=='Ongoing'){
                console.log('**Entered in first if');
                //WHEN THERE IS NO PRORATING 
                if(categoryRate.Prorate_First_Payment__c!=undefined && categoryRate.Prorate_First_Payment__c==false && 
                   categoryRate.Prorate_final_payment__c!=undefined && categoryRate.Prorate_final_payment__c==false){
                    console.log('**Enterd in second if');
                    catRateInfo.Id=categoryRate.Id;
                    catRateInfo.Name=categoryRate.Name;
                    catRateInfo.Price= categoryRate.Price__c;
                    catRateInfo.Prorate_First_Payment__c=false;
                    catRateInfo.FirstProrateAmount=categoryRate.Price__c;
                    catRateInfo.ProrateLastPayment=false;
                    catRateInfo.LastProrateAmount=categoryRate.Price__c;
                    console.log(JSON.stringify(catRateInfo));
                    component.set("v.CatRateInfo",catRateInfo);
                    component.set("v.IsShowRateInfo",true);
                    console.log('print');
                }
                //WHEN THERE IS PRORATING 
                else{                   
                     this.GetProratedAmounts(component,event,helper);
                }
            }
        }  
        else{
             component.set("v.IsShowRateInfo",false);
             component.set("v.CatRateInfo",'');
        }
        
    },
    GetProratedAmounts : function(component,event,helper){
        var action=component.get("c.FetchProratedAmount");
        action.setParams({
            categoryrateId : component.get("v.categoryrate").Id,
            amount : component.get("v.amount"),
            beginDate : component.get("v.closeDate"),
            endDate : component.get("v.EndDate")            
        });
         action.setCallback(this,function(response){           
            var state = response.getState();
            console.log('**State val'+state);
            if(state==="SUCCESS"){
                 var result=response.getReturnValue();
                console.log('**VAl of result'+JSON.stringify(result));
                if(result!=null){
                    console.log('hii');
                    var categoryRate=component.get("v.categoryrate");
                    var catRateInfo ={};
                    catRateInfo.Id=categoryRate.Id;
                    catRateInfo.Name=categoryRate.Name;
                    catRateInfo.Price= categoryRate.Price__c;
                    catRateInfo.Prorate_First_Payment__c=categoryRate.Prorate_First_Payment__c;
                    catRateInfo.FirstProrateAmount=result.FirstProratedAmount;
                    catRateInfo.ProrateLastPayment=categoryRate.Prorate_final_payment__c;
                    catRateInfo.LastProrateAmount=result.lastProratedAmount;
                    component.set("v.CatRateInfo",catRateInfo);
                    component.set("v.IsShowRateInfo",true);
                }
                else{
                    console.log('succes else');
                      component.set("v.CatRateInfo",{});
            		  component.set("v.IsShowRateInfo",false);	
                }
            }
        });        
        $A.enqueueAction(action);       
    },
    callSaveApex : function(component,event,helper,IsSave){
        component.set("v.IsSubmitForApproval",false);
        var recordid = component.get("v.recordId");
        var opportunity ={};
        var caseid = component.get("v.case");  
        var campaignId=component.get("v.campaignId");
        var vendorId = component.get("v.vendor");
        var beginDate = component.get("v.closeDate");
        console.log(beginDate);
        var endDate = component.get("v.EndDate");
        var amount= component.get("v.amount");
        var type= component.get("v.type");
        var categoryId = component.get("v.category");
        var MonthDay=component.get("v.MonthDay");
        opportunity.Kinship_Case__c = caseid;
        opportunity.AccountId = vendorId;
        opportunity.CampaignId=campaignId;
        opportunity.CloseDate = beginDate;
        console.log(opportunity.CloseDate);
        opportunity.End_Date__c = endDate;
        opportunity.Amount=amount;
        opportunity.Type_of_Case_Action__c=type;
        opportunity.Category__c = categoryId;
        opportunity.Day_of_Month__c= MonthDay;
         if(component.get("v.costcenterSelection")!=undefined)
            opportunity.Cost_Center__c=component.get("v.costcenterSelection").Id;
        console.log('**Val of transcodeselected'+component.get("v.TransCodeSelected"));
        if(component.get("v.TransCodeSelected")!=null && component.get("v.TransCodeSelected")!='')
            opportunity.Transaction_Code__c = component.get("v.TransCodeSelected");
       // opportunity.Parent_Recipient__c=component.get("v.recipient");
        console.log('**Till here');
         if(component.get("v.CaseNote")!='' && component.get("v.CaseNote")!=undefined)
            opportunity.Description = component.get("v.CaseNote");
        var prorateInfo = component.get("v.CatRateInfo");
        var IsShowRateInfo =component.get("v.IsShowRateInfo");
        var ProrateObj ={};
        if(prorateInfo!='' && IsShowRateInfo==true){
            ProrateObj.ProrateFirstPayment=prorateInfo.Prorate_First_Payment__c;
            ProrateObj.ProrateLastPayment=prorateInfo.ProrateLastPayment;
            ProrateObj.ProratedFirstAmount=prorateInfo.FirstProrateAmount;
            ProrateObj.ProratedLastAmount=prorateInfo.LastProrateAmount;
        }
        if(component.get("v.categoryType")=='CSA'){
            if(component.get("v.SPTselected")!='' && component.get("v.SPTselectedlabel")!=''){
                opportunity.Service_Placement_Type1__c = component.get("v.SPTselectedlabel");
                opportunity.Service_Placement_Type_Code__c = component.get("v.SPTselected");
            }
            if(component.get("v.SPTNameselected")!='' && component.get("v.SPTNameselectedlabel")!=''){
                opportunity.Service_Name_Code1__c = component.get("v.SPTNameselectedlabel");
                opportunity.Service_Name_Code_Value__c = component.get("v.SPTNameselected");
            }
            if(component.get("v.MandateSelected")!='' && component.get("v.MandateSelectedlabel")!=''){
                opportunity.MandateType1__c = component.get("v.MandateSelectedlabel");
                opportunity.Mandate_Type_Code__c = component.get("v.MandateSelected");
            }
            if(component.get("v.Description")!='' && component.get("v.Description")!=undefined)
                opportunity.Service_Description_Other__c=component.get("v.Description");
        	 if(component.get("v.recipient")!='')
                opportunity.Parent_Recipient__c=component.get("v.recipient");
        }
        else if(component.get("v.categoryType")=='IVE'){
            if(component.get("v.recipientIVE")!='')
                opportunity.Parent_Recipient__c = component.get("v.recipientIVE");
             if(component.get("v.Fund")!='')
                opportunity.Fund__c = component.get("v.Fund");
        }
        else if(component.get("v.categoryType")!='CSA' && component.get("v.categoryType")!='IVE' && 
                component.get("v.categorySelection")!=undefined && component.get("v.categorySelection")!=''
               && component.get("v.categorySelection").RecordType!=undefined && component.get("v.categorySelection").RecordType.DeveloperName!=undefined 
               && component.get("v.categorySelection").RecordType.DeveloperName=='LASER_Category'){
             opportunity.Fund__c = component.get("v.Fund");
        }
        var POobject ={};
        POobject.Opp = opportunity;
        POobject.ProratedSaveWrap =ProrateObj;
        console.log('**Val of POobject'+JSON.stringify(POobject));
        var action = component.get("c.EditCaseAction");
        action.setParams({          	                
            POwrapper : JSON.stringify(POobject),  
            recordid : recordid,
            SubmitforApproval : IsSave,
            profileName : component.get("v.profileName"),
            IsCSAAdminProfile : component.get("v.IsCSAAdminProfile"),
            stagename : component.get("v.opportunityStage")
        });	
         action.setCallback(this,function(response){           
            var state = response.getState();
            console.log('**State val'+state);
            if(state==="SUCCESS"){
                component.set("v.POSpinner",false);
                var result=response.getReturnValue(); 
                 console.log(JSON.stringify(result));
                if(result.recordid!=null){
                    if(result.message =='record updated'){
                        //KIN 98
                    	 this.showToast('Success','Success','Changes saved successfully!');                         
                    	//this.redirecttoRecord(component,event,helper,result.recordid );
                         //$A.get('e.force:refreshView').fire();
                        if(component.get("v.IsSavenNew")==true){
                            var evt = $A.get("e.force:navigateToComponent");
                            evt.setParams({
                                componentDef : "c:CaseActionPOLightningComponent"
                                
                            });
                            evt.fire();
                        }  
                        else{
                         // ends KIN 98
                             if(component.get("v.IsCSAAdminProfile")==true){
                                 component.set("v.IsSubmitForApproval",true);
                                 console.log('**Val of recordid'+result.recordid);
                                 component.set("v.recordId",result.recordid);    
                             }
                        }    
                    } 
                    else{
                        this.showToast('Success','Success','Case Action has been created and submitted for approval successfully!');                        	
                    	component.set("v.IsSubmitForApproval",true);
                        console.log('**Val of recordid'+result.recordid);
                        component.set("v.recordId",result.recordid);
                        /*this.redirecttoRecord(component,event,helper,result.recordid );
                    	$A.get('e.force:refreshView').fire(); */
                    }
                }
                
            }   
             else if(state=="ERROR"){
                 component.set("v.POSpinner",false);
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                        console.log('**Error'+ strErr);
                    }
                }
                // KIN 280
                if(strErr.indexOf('Please select a case with Active Special Welfare Account')  !=-1){
                    strErr= strErr + 'OR Click on New SWA button.';
                }
                //ends
                this.showIncreaseToast('Error', 'Error', strErr);
                if(strErr.indexOf('Please select a case with Active Special Welfare Account')  !=-1){
                    component.set("v.showNewSWA",true);
                }
                console.log(component.get("v.showNewSWA"));
             }
        });
        $A.enqueueAction(action);
    },
     FetchSPTRate : function(component,event,helper){
        console.log('entered in fetchsptrate');
    	var action=component.get("c.getSPTCategoryRate");
        action.setParams({
            categoryId : String(component.get("v.category")),
            selectedSPT : component.get("v.SPTselected"),
            selectedSPTName : component.get("v.SPTselectedlabel")           
        });
         action.setCallback(this,function(response){           
            var state = response.getState();
            console.log('**State val'+state);
            if(state==="SUCCESS"){
                var result=response.getReturnValue();
                console.log('**Cat create result'+JSON.stringify(result));
                if(result.catRate!=null){
                    component.set("v.categoryrate",result.catRate);
                    if(result.catRate.Price__c!=null)
                    	component.set("v.amount",result.catRate.Price__c);
                    component.set("v.IsAmountDisabled",true);
                    component.set("v.ShowSpinner",false);
            		component.set("v.openModal",false);   
                    this.checkCategoryRate(component,event,helper,component.get("v.categoryrate"));
                }
                else{
                    component.set("v.IsAmountDisabled",false);
                    component.set("v.categoryrate",'');
                    component.set("v.ShowSpinner",false);
            		component.set("v.openModal",false);   
                    this.checkCategoryRate(component,event,helper,component.get("v.categoryrate"));
                }
            }
             else{
                 const errors = response.getError();
                 console.log(errors[0].message);
             }
         });
        $A.enqueueAction(action);      
    },
    fetchBeginDate : function(component,event,helper){
        console.log('**Entered in fetch begin date');
    	var action=component.get("c.FetchBeginDate");
         action.setParams({          	                
                campaignId : String(component.get("v.campaignId"))
        });	
         action.setCallback(this,function(response){           
            var state = response.getState();
            console.log('**State val'+state);
            if(state==="SUCCESS"){
                 var result=response.getReturnValue(); 
                 console.log(JSON.stringify(result));              
                if(result.accountingPeriodId!=null)
                    component.set("v.campaignId",result.accountingPeriodId);
                if(result.APstartDate!=null){
                    component.set("v.APbeginDate",result.APstartDate);
                    component.set("v.closeDate",result.APstartDate);
                }   
                if(result.APendDate!=null)
                    component.set("v.APEndDate",result.APendDate);
            } 
             else{
                 component.set("v.APbeginDate",'');
                 component.set("v.closeDate",'');
                 component.set("v.APEndDate",'');
             }
        });
        $A.enqueueAction(action);     
    },
    checkCaseRequiredFields : function(component,event,helper){
        var isCSA = component.get("v.IsCSACase");
        var isIVE = component.get("v.isIVECase");
        console.log('**VAl of hispanic'+component.get("v.HispanicEthnicity"));
        if(isCSA==true || isIVE==true){
            console.log('**inside iscsa nad isive');
            if(component.get("v.TaxId")==undefined || component.get("v.TaxId")==''){
            	component.set("v.ShowSpinner",false);
                component.set("v.CaseError",true);
        		this.showToast('Error','Error','Please provide vendor Tax Id.');       
            }
            else if(component.get("v.ConFirstName")==undefined || component.get("v.ConFirstName")==''){
          		component.set("v.ShowSpinner",false);
                component.set("v.CaseError",true);
        		this.showToast('Error','Error','Please provide a Contact First Name.');     
            }
            else if(component.get("v.ConLastName")==undefined || component.get("v.ConLastName")==''){
          		component.set("v.ShowSpinner",false);
                component.set("v.CaseError",true);
        		this.showToast('Error','Error','Please provide a Contact Last Name.');     
            }
          else if(component.get("v.DOB")==undefined || component.get("v.DOB")==''){
          		component.set("v.ShowSpinner",false);
              component.set("v.CaseError",true);
        		this.showToast('Error','Error','Please provide a DOB.');     
            }
          else if(component.get("v.Gender")==undefined || component.get("v.Gender")==''){
          		component.set("v.ShowSpinner",false);
               component.set("v.CaseError",true);
        		this.showToast('Error','Error','Please provide a Gender.');     
            }
           else if(component.get("v.Race")==undefined || component.get("v.Race")==''){
          		component.set("v.ShowSpinner",false);
                component.set("v.CaseError",true);
        		this.showToast('Error','Error','Please provide a Race.');     
            }
           else if(component.get("v.HispanicEthnicity")==undefined || component.get("v.HispanicEthnicity")==''){
          		console.log('**entered in hispanic');
               component.set("v.ShowSpinner",false);
               component.set("v.CaseError",true);
        		this.showToast('Error','Error','Please provide a Hispanic Ethnicity.');     
            }
          else if(component.get("v.TitleIVE")==undefined || component.get("v.TitleIVE")==''){
               component.set("v.ShowSpinner",false);
              component.set("v.CaseError",true);
        		this.showToast('Error','Error','Please provide a Title IVE eligibility.');     
            }
           else if(component.get("v.Medical")==undefined || component.get("v.Medical")==''){
                    console.log('***entered in medical');
                   component.set("v.ShowSpinner",false);
                    component.set("v.CaseError",true);
                    this.showToast('Error','Error','Please provide Medical Indicator.');     
           } 
           else if((component.get("v.studentidentifier")==undefined || component.get("v.studentidentifier")==null || component.get("v.studentidentifier")=='' ) &&
                         (component.get("v.SSN")==undefined || component.get("v.SSN")==null || component.get("v.SSN")=='')){
                	component.set("v.ShowSpinner",false);
                   component.set("v.CaseError",true);
                    this.showToast('Error','Error','Please provide either Student Identifier or SSN.');           
           } 
        
            else if(isIVE==true){
                console.log('**VAl of oasisclienid'+component.get("v.OasisClinetId"));
               if(component.get("v.TitleIVE")!=undefined && component.get("v.TitleIVE")!='' && 
                    component.get("v.TitleIVE")=='1'){ 
                        component.set("v.ShowSpinner",false);
                       component.set("v.CaseError",true);
                        this.showToast('Error','Error','You cannot use category of type IV-E if its associated case title IV-E eligibility field has no value.');
                             
                }
                else if(component.get("v.OasisClinetId")==undefined || component.get("v.OasisClinetId")==''){
                    console.log('*enterd in of oasisclienitd');
                    component.set("v.ShowSpinner",false);
                    component.set("v.CaseError",true);
                    this.showToast('Error','Error','Please provide Oasis Client Id.');     
                }
                    
            }
           
            else if(isCSA==true){
                console.log('**Enterd in iscsa');
                if(component.get("v.DSM")==undefined || component.get("v.DSM")==''){
                   component.set("v.ShowSpinner",false);
                    component.set("v.CaseError",true);
                    this.showToast('Error','Error','Please provide DSM V Indicator/ ICD-10.');     
                }
              /*  else if(component.get("v.Medical")==undefined || component.get("v.Medical")==''){
                    console.log('***entered in medical');
                   component.set("v.ShowSpinner",false);
                    component.set("v.CaseError",true);
                    this.showToast('Error','error','Please provide Medical.');     
                } */
                else if(component.get("v.Referal")==undefined || component.get("v.Referal")==''){
                   component.set("v.ShowSpinner",false);
                    component.set("v.CaseError",true);
                    this.showToast('Error','Error','Please provide Referral Source.');     
                }
               else if(component.get("v.Clinical")==undefined || component.get("v.Clinical")==''){
                   component.set("v.ShowSpinner",false);
                   component.set("v.CaseError",true);
                    this.showToast('Error','Error','Please provide Clinical Medication Indicator.');     
                }
               else if(component.get("v.Autism")==undefined || component.get("v.Autism")==''){
                   component.set("v.ShowSpinner",false);
                   component.set("v.CaseError",true);
                    this.showToast('Error','Error','Please provide Autism Flag.');     
                }
                 else if(component.get("v.SPTselected")!=undefined && (component.get("v.SPTselected")=='6' || component.get("v.SPTselected")=='7' || component.get("v.SPTselected")=='18') &&
                 (component.get("v.studentidentifier")==undefined || component.get("v.studentidentifier")==null || component.get("v.studentidentifier")=='')){
                 	component.set("v.ShowSpinner",false);
                   component.set("v.CaseError",true);
                    this.showToast('Error','Error','Please provide Student Identifier.');    
                }
              /*  else if((component.get("v.studentidentifier")==undefined || component.get("v.studentidentifier")==null || component.get("v.studentidentifier")=='' ) &&
                         (component.get("v.SSN")==undefined || component.get("v.SSN")==null || component.get("v.SSN")=='')){
                	component.set("v.ShowSpinner",false);
                   component.set("v.CaseError",true);
                    this.showToast('Error','error','Please provide either Student Identifier or SSN.');           
                } */
                else if(component.get("v.MandateSelected")!=undefined && (component.get("v.MandateSelected")=='2' || component.get("v.MandateSelected")=='3' || component.get("v.MandateSelected")=='6' ||  component.get("v.MandateSelected")=='7'
                 || component.get("v.MandateSelected")=='8') && (component.get("v.CSAoasisclientId")==undefined || component.get("v.CSAoasisclientId")==null || component.get("v.CSAoasisclientId")=='')){
                   component.set("v.ShowSpinner",false);
                   component.set("v.CaseError",true);
                    this.showToast('Error','Error','Please provide Oasis client Id.');             
                }
              console.log('**Enterd in the end of else csa');      
            }
           
        	console.log('end of if');
        } 
    },
    updateCase : function(component,event,helper){
        var caseid = component.get("v.case");
        var casewrap ={};
        casewrap.contactFirstName = component.get("v.ConFirstName");
        casewrap.contactLastName = component.get("v.ConLastName");
        casewrap.DOB=component.get("v.DOB");
        casewrap.HispanicEthnicity=component.get("v.HispanicEthnicity");
        casewrap.Gender = component.get("v.Gender");
        casewrap.Race = component.get("v.Race");
         if(component.get("v.IsCSACase")==true)
            casewrap.OasisClinetId=component.get("v.CSAoasisclientId");
        else   
        	casewrap.OasisClinetId=component.get("v.OasisClinetId");
        casewrap.DSM=component.get("v.DSM");
        casewrap.Medical = component.get("v.Medical");
        casewrap.Referal = component.get("v.Referal");
        casewrap.Clinical=component.get("v.Clinical");
        casewrap.Autism=component.get("v.Autism");
        casewrap.titleIVE=component.get("v.TitleIVE");
        casewrap.studentIdentifierId = component.get("v.studentidentifier");
        casewrap.SSN= component.get("v.SSN");
        casewrap.taxId=component.get("v.TaxId"); 
     	 var action = component.get("c.UpdateCase"); 
        	action.setParams({ caseId : caseid.toString(),
                              caseWrapper : JSON.stringify(casewrap),
                              vendorId : component.get("v.vendor")});
         	action.setCallback(this,function(response){           
            var state = response.getState();
            console.log('**State val of case update'+state);
            if(state==="SUCCESS"){
                 var result=response.getReturnValue(); 
                 console.log(JSON.stringify(result));
                if(result!=null && result.caserec!=null){
                    component.set("v.CaseValue",result.caserec);
                    component.set("v.ShowSpinner",false);                  
                    this.showToast('Success','Success','Case record has been updated.');  
                    component.set("v.showCSACase",false);
                    component.set("v.isIVECase",false);
                    component.set("v.IsCSACase",false);
                } 
                else if(result!=null && result.message=='Case contact doesnt exist'){
                	component.set("v.CaseValue",'');
                    component.set("v.ShowSpinner",false);                  
                    this.showToast('Error','Error','First associate a contact with the case');  
                    component.set("v.showCSACase",false);
                    component.set("v.isIVECase",false);
                    component.set("v.IsCSACase",false);
                }
                else {
                    component.set("v.CaseValue",'');
                    component.set("v.ShowSpinner",false);                  
                    this.showToast('Error','Error','Failed to update the case record.');  
                   
                }    
            }  
              else if(state==="ERROR"){
              	component.set("v.CaseValue",'');
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                        component.set("v.CaseValue",'');
                    	component.set("v.ShowSpinner",false);                  
                    	this.showToast('Error','Error',errors[0].message); 
                       
                    }
                } else {
                    console.log("Unknown error");
                    component.set("v.CaseValue",'');
                    component.set("v.ShowSpinner",false);                  
                    this.showToast('Error','Error','Failed to update Case.');
                    
                    
                }
               
            }
         });
        $A.enqueueAction(action);             
    },
    setOldValues : function(component,event,helper){
        
        component.set("v.MandateSelectedOld",component.get("v.MandateSelected"));
        component.set("v.SPTselectedOld",component.get("v.SPTselected"));
        component.set("v.SPTNameselectedOld",component.get("v.SPTNameselected"));
        component.set("v.MandateSelectedlabelOld",component.get("v.MandateSelectedlabel"));
        component.set("v.SPTNameselectedlabelOld",component.get("v.SPTNameselectedlabel"));
        component.set("v.SPTselectedlabelOld",component.get("v.SPTselectedlabel"));
        component.set("v.DescriptionOld",component.get("v.Description"));
        component.set("v.recipientOld",component.get("v.recipient"));
        component.set("v.recipientIVEOld",component.get("v.recipientIVE"));
        component.set("v.SPToptionsOld",component.get("v.SPToptions"));
        component.set("v.MandateoptionsOld",component.get("v.Mandateoptions"));
        component.set("v.SPTNamesOld",component.get("v.SPTNames"));
    },
    OncancelSetOld : function(component,event,helper){
        var categorytype = component.get("v.categoryType");
        if(categorytype=="CSA"){
        	component.set("v.MandateSelected",component.get("v.MandateSelectedOld"));
            component.set("v.SPTselected",component.get("v.SPTselectedOld"));
            component.set("v.SPTNameselected",component.get("v.SPTNameselectedOld"));
            component.set("v.MandateSelectedlabel",component.get("v.MandateSelectedlabelOld"));
            component.set("v.SPTNameselectedlabel",component.get("v.SPTNameselectedlabelOld"));
            component.set("v.SPTselectedlabel",component.get("v.SPTselectedlabelOld"));
            component.set("v.Description",component.get("v.DescriptionOld"));
            component.set("v.recipient",component.get("v.recipientOld"));

            component.set("v.SPToptions",component.get("v.SPToptionsOld"));
            component.set("v.Mandateoptions",component.get("v.MandateoptionsOld"));
            component.set("v.SPTNames",component.get("v.SPTNamesOld"));   
        }
        else if(categorytype=='IVE'){
        	component.set("v.recipientIVE",component.get("v.recipientIVEOld"));    
        }
    },
    fetchCostCenters: function(component,event,helper){
    	var action=component.get("c.getCostCenters");
        action.setParams({
            categoryId : String(component.get("v.category")),
            FipsId : String(component.get("v.FIPSID"))
            
        });
         action.setCallback(this,function(response){           
            var state = response.getState(); 
            if(state==="SUCCESS"){
                 var result=response.getReturnValue();
                console.log('**Costcenter ids'+result);
                if(result!=null && result.length >0){
                    component.set("v.CostCenterIds",result);
                    if(result.length==1)
                        this.fetchOneCostCenter(component,event,helper,result[0]);
                }
            }      
        });        
        $A.enqueueAction(action);       
    },
    fetchOneCostCenter:function(component,event,helper,costcenterid){
        console.log('***Enterd here');
        console.log('**VAl of costcenterid'+costcenterid);
    	var action=component.get("c.getOneCostCenter");
        action.setParams({
            costcenterId : costcenterid.toString()
            
        });
         action.setCallback(this,function(response){           
            var state = response.getState(); 
            if(state==="SUCCESS"){
                 var result=response.getReturnValue();
                console.log('**Costcenter id one'+JSON.stringify(result));
                if(result!=null){
                    component.set("v.costcenterSelection",result);
                }
            }      
        });        
        $A.enqueueAction(action);       
    },
    CheckForSpecialWelfareAccount: function(component,event,helper){
        console.log('**Entred in checkfrospecilwelfareaccount');
        console.log('case val'+component.get("v.case"));
    	var action=component.get("c.getSWA");
        action.setParams({
            caseID : String(component.get("v.case"))
            
        });  
        action.setCallback(this,function(response){           
            var state = response.getState(); 
            if(state==="SUCCESS"){
                 var result=response.getReturnValue();
                console.log('**swa id one'+JSON.stringify(result));
                if(result!=null){
                    component.set("v.specialwelfare",result);
                    component.set("v.ShowSpecialWelfareButton",true);
                    if(result!=null && result!=undefined){
                        console.log('&&cc'+ result.Child_Support_Funds_Remaining__c);
                        console.log('***ssa'+result.SSA_SSI_Funds_Remaining__c);
                        console.log('**cb'+result.Current_Balance__c);
                        if(result.Child_Support_Funds_Remaining__c!=null && result.Child_Support_Funds_Remaining__c!=undefined)
                        	component.set("v.ChildSupportFundsRemaining",result.Child_Support_Funds_Remaining__c);
                      /*  else
                            component.set("v.ChildSupportFundsRemaining",0.0); */
                        if(result.SSA_SSI_Funds_Remaining__c !=undefined && result.SSA_SSI_Funds_Remaining__c!=null)
                            component.set("v.SSASSIRemaining",result.SSA_SSI_Funds_Remaining__c);
                       
                        if(result.Current_Balance__c!=undefined && result.Current_Balance__c!=null) 
                            component.set("v.Currentbalance",result.Current_Balance__c);
                      
                    }
                }
                else{
                    component.set("v.specialwelfare",'');
                    component.set("v.ShowSpecialWelfareButton",false);
                    component.set("v.ChildSupportFundsRemaining",'');
                    component.set("v.SSASSIRemaining",'');
                    component.set("v.Currentbalance",'');
                    component.set("v.TransCodeSelected",'');
                }
            }      
        });        
        $A.enqueueAction(action); 
    }
    
})