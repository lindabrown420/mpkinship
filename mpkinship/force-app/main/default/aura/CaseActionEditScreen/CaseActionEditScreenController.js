({
	doInit : function(component, event, helper) {
        console.log('****recordid'+component.get("v.recordId"));
		helper.FetchInitialValues(component,event,helper);	
	},
    //KIN 1367
    handleUploadFinished : function(component,event,helper){
        var uploadedFiles = event.getParam("files");
        console.log("Files uploaded : " + uploadedFiles.length);

        // Get the file name
        uploadedFiles.forEach(file =>{
            console.log(file.name);
        });
    },
    CampaingdataChanged :function(component,event,helper){
   		component.set("v.campaignId",'');
        component.set("v.APbeginDate",'');
        component.set("v.APEndDate",'');
        component.set("v.FIPSID",'');
        component.set("v.costcenterSelection",'');        
        component.set("v.CostCenterIds",[]); 
        
        var changedcampval = event.getParam("dataChange");       
        console.log('9999'+JSON.stringify(changedcampval));
        console.log('**VAl of category cahnge'+JSON.stringify(component.get("v.categorySelection")));
         if(changedcampval!='' && changedcampval!=undefined && changedcampval.Id!=undefined){
         	component.set("v.campaignId",changedcampval.Id); 
             if(changedcampval.FIPS__c!=undefined)
                 component.set("v.FIPSID",changedcampval.FIPS__c);
              helper.fetchBeginDate(component,event,helper);
         }   
        //GET COST CENTER 
        var categoryId = component.get("v.category");
        if(component.get("v.FIPSID")!='' && component.get("v.FIPSID")!=undefined && categoryId!='' && categoryId!=undefined){
            helper.fetchCostCenters(component,event,helper);
        }
    },
     
    CategorydataChange : function(component,event,helper){
     	component.set("v.CSAError",false);
        component.set("v.category",'');
        component.set("v.ShowCSAButton",false);
        component.set("v.ShowIVEButton",false);
        component.set("v.ShowFundButton",false);
        component.set("v.ShowSpecialWelfareButton",false);
        component.set("v.specialwelfare",'');
        component.set("v.TransCodeSelected",'');
        component.set("v.IsShowRateInfo",false);
        component.set("v.CatRateInfo",'');
        component.set("v.IsAmountDisabled",false);
        component.set("v.Fund",'');
        component.set("v.costcenterSelection",'');
        component.set("v.showCostCenterLookup",false);
        component.set("v.CostCenterIds",[]); 
        console.log('**VAl of type'+component.get("v.type"));
        component.set("v.IsEndDateRequired",false);
        
         if(component.get("v.categoryrate")!=null && component.get("v.categoryrate")!=undefined
          && component.get("v.categoryrate").Price__c!=undefined && component.get("v.amount")!=undefined
          && component.get("v.categoryrate").Price__c == component.get("v.amount")){
        	component.set("v.amount",'');	    
        }
        component.set("v.category",'');        
        var changedcatval = event.getParam("dataChange");       
        console.log('9999'+JSON.stringify(changedcatval));
        console.log('**VAl of category cahnge'+JSON.stringify(component.get("v.categorySelection")));
         if(changedcatval!='' && changedcatval!=undefined && changedcatval.Id!=undefined){
         	component.set("v.category",changedcatval.Id); 
              helper.fetchBeginDate(component,event,helper);
         }
        
        //CHECK IF CASE IS ADDED
        var caseid=component.get("v.case");
        console.log('**Caseid val'+caseid);
        var categoryId = component.get("v.category");
        //FETCH COST CENTER FIRST
        if(component.get("v.FIPSID")!='' && component.get("v.FIPSID")!=undefined && categoryId!='' && categoryId!=undefined){
            helper.fetchCostCenters(component,event,helper);
        }
        //FETCH RATE RELATED TO THE CATEGORY 
         if(caseid!='' && caseid!=undefined && categoryId!='' && categoryId!=undefined){
            console.log('**Enterd in both are not null');
            helper.CSABlank(component,event,helper);  
        	helper.FetchRates(component,event,helper);
        }  
    },     
    caseChange : function(component,event,helper){
         //MAKE RATE INFO FALSE
        component.set("v.CatRateInfo",{});
        component.set("v.IsShowRateInfo",false);
                
        if(component.get("v.categoryrate")!=null && component.get("v.categoryrate")!=undefined && component.get("v.categoryrate").RecordType!=undefined
          && component.get("v.categoryrate").RecordType.DeveloperName!=undefined && component.get("v.categoryrate").RecordType.DeveloperName=='Service_Names'
          && component.get("v.categoryrate").Price__c!=undefined && component.get("v.amount")!=undefined
          && component.get("v.categoryrate").Price__c == component.get("v.amount")){
        	component.set("v.amount",'');
            component.set("v.IsAmountDisabled",false);
        }
    	var caseId = component.find("caseid").get("v.value");
        console.log(caseId);  
        component.set("v.case",caseId);
        var categoryId = component.get("v.category");
        if(categoryId!='' && caseId!='' && categoryId!=undefined && caseId!=undefined)
            helper.FetchRates(component,event,helper);
         //MAKE LABEL AND NAME OF CSA SPT NULL
       // helper.CSABlank(component,event,helper);
    },
    amountChange : function(component,event,helper){
    	var amountId= component.find("amountid").get("v.value"); 
        component.set("v.amount",amountId);
        console.log('^^^^^^^^^^'+amountId);
       
    },
    changeTypeCA : function(component,event,helper){
    	var type = component.find("typeid").get("v.value");
        component.set("v.type",type);
        console.log(type);
        if(type=='Ongoing'){
        	component.set("v.IsOngoing",true);
            //CHECK FOR CATEGORY TYPE
           /* if(component.get("v.categoryType")!=undefined && component.get("v.categoryType")!='' &&
               (component.get("v.categoryType")=='CSA' || component.get("v.categoryType")=='IVE')){
                component.set("v.IsEndDateRequired",true);
            }
            else
                component.set("v.IsEndDateRequired",false); */
           //CHECK IF CATEGORY RATE IS NOT NULL
           console.log('**'+JSON.stringify(component.get("v.categoryrate")));
        	var categoryRate = component.get("v.categoryrate");
            if(categoryRate!='' && categoryRate!=undefined)
                helper.checkCategoryRate(component,event,helper,categoryRate);           
        }    
        else{
            component.set("v.IsOngoing",false);
            component.set("v.MonthDay",'');
            component.set("v.CatRateInfo",{});
            component.set("v.IsShowRateInfo",false);
        }    
    },
    beginDateChange : function(component,event,helper){
        //MAKE RATE INFO FALSE
        component.set("v.CatRateInfo",{});
        component.set("v.IsShowRateInfo",false);
    	var begDate = component.find("beginid").get("v.value");
        component.set("v.closeDate",begDate);
        var categoryRate = component.get("v.categoryrate");
            if(categoryRate!='' && categoryRate!=undefined)
                helper.checkCategoryRate(component,event,helper,categoryRate);  
    },
    endDateChange : function(component,event,helper){
         //MAKE RATE INFO FALSE
         component.set("v.CatRateInfo",{});
         component.set("v.IsShowRateInfo",false);
    	var endDate = component.find("endId").get("v.value");
        component.set("v.EndDate",endDate);
         var categoryRate = component.get("v.categoryrate");
            if(categoryRate!='' && categoryRate!=undefined)
                helper.checkCategoryRate(component,event,helper,categoryRate);
    },
    handleCSACA : function(component,event,helper){
        //KIN 237
        
        component.set("v.showMandateDesc",false);
        component.set("v.showSPTDesc",false);
        component.set("v.showSNCDesc",false);
        //ends
        //KIN 228
        var action1= component.get("c.getCaseAge");
        	action1.setParams({
            	caseId: component.get("v.case")
        	});	   
            action1.setCallback(this, function(response){
            	var state = response.getState();
            	console.log('**VAl of state'+state);
            	console.log('***response'+response.getReturnValue());
                if (state === "SUCCESS") { 
                    var caseval = response.getReturnValue();
                     console.log('***case'+caseval);
                    console.log('***case age'+caseval.Age__c);
                    if(caseval!=null){
                    	component.set("v.selectedCaseAge",caseval.Age__c);
                    }
                }
            });
        
        $A.enqueueAction(action1);
        
        //ends
        helper.getCategoryandSPTType(component,event,helper);
        helper.setOldValues(component,event,helper);
        //NOW COST CENTER IS MANDATORY
        component.set("v.showCostCenterLookup",true);
       /* if(component.get("v.CostCenterIds")!='' && component.get("v.CostCenterIds").length >0)
            component.set("v.showCostCenterLookup",true);
        else
            component.set("v.showCostCenterLookup",false); */
        component.set("v.openModal",true);
    },
    RecipientIVEChange : function(component,event,helper){
    	var selectedRecipient = event.getParam("value");
       console.log('**REcipient value'+selectedRecipient);
       component.set("v.recipientIVE",selectedRecipient);     
    },
    FundChange : function(component,event,helper){
    	var selectedFund = event.getParam("value");
       component.set("v.Fund",selectedFund);      
    },
    handleIVECA : function(component,event,helper){
    	component.set("v.openModal",true); 
         if(component.get("v.FundIVE")==undefined || component.get("v.FundIVE")==''){
        	component.set("v.FundIVE","Reimbursable"); 
        }  
        //NOW COST CENTER IS MANDATORY
        component.set("v.showCostCenterLookup",true);
       /* if(component.get("v.CostCenterIds")!='' && component.get("v.CostCenterIds").length >0)
            component.set("v.showCostCenterLookup",true);
        else
            component.set("v.showCostCenterLookup",false); */
    },
    handleFundCA : function(component,event,helper){
        component.set("v.openModal",true); 
        if(component.get("v.Fund")==undefined || component.get("v.Fund")==''){
        	component.set("v.Fund","Reimbursable"); 
        }  
        //NOW COST CENTER IS MANDATORY
        component.set("v.showCostCenterLookup",true);
       /* if(component.get("v.CostCenterIds")!='' && component.get("v.CostCenterIds").length >0)
            component.set("v.showCostCenterLookup",true);
        else
            component.set("v.showCostCenterLookup",false); */
    },
    closeModel : function(component,event,helper){
    	component.set("v.openModal",false);   
        helper.OncancelSetOld(component,event,helper);
    },
    handleTransCode : function(component,event,helper){
    	var transcode = event.getParam("value");
        component.set("v.TransCodeSelected",transcode);
    },
     handleMandateChange : function(component,event,helper){
         console.log('**Val of old mandate'+component.get("v.MandateSelected"));
        var selectedMandate = event.getParam("value");
        console.log('**Val of mandate'+selectedMandate);
         if(selectedMandate=='' || selectedMandate==undefined){
             component.set("v.MandateSelectedlabel",'');
             //KIN 237
        	 component.set("v.showMandateDesc",false);
         }
        else{
            var options = component.get("v.Mandateoptions");
            var selectedlabel='';
            for(var i=0; i< options.length ; i++){
                if(options[i].value==selectedMandate){
                    selectedlabel=options[i].label;
                    break;
                }
            }
            if(selectedlabel!=''){
                component.set("v.MandateSelectedlabel",selectedlabel);
                // KIN 228
                var ageofCase=component.get("v.selectedCaseAge");
                console.log('ageofCase:'+ageofCase+'selectedlabel:'+selectedlabel);
                if((selectedlabel=='Foster Care Abuse/Neglect Prevention'||selectedlabel=='Foster Care Child in Need of Services (CHINS) Prevention'||
                  selectedlabel=='Foster Care CHINS – CSA Parental Agreement'|| selectedlabel=='Non-Mandated')&& ageofCase>18){
                    component.set("v.ShowSpinner",false);
                    component.set("v.AgeError",true);
                    component.set("v.AgeError1",false);
                    component.set("v.AgeError2",false);
                    helper.showToast('Error', 'Error', 'The age of the child must be 18 years or less to use this mandate type. Please update your selection.');
                }
                else if((selectedlabel=='Foster Care Abuse/Neglect – DSS Non-Custodial Agreement'||selectedlabel=='Foster Care Abuse/Neglect – Local DSS Entrustment/Custody'||
                  selectedlabel=='Foster Care CHINS – Entrustment/Custody'|| selectedlabel=='Foster Care – Court Ordered for Truancy' || selectedlabel=='Foster Care – Court Ordered for Delinquent Behaviors')&& ageofCase>21){
                    component.set("v.ShowSpinner",false);
                    component.set("v.AgeError1",true);
                    component.set("v.AgeError",false);
                    component.set("v.AgeError2",false);
                    helper.showToast('Error', 'Error', 'The person’s age on this case must be 21 years or less to use this mandate type. Please update your selection.');
                }
                else if((selectedlabel=='Kinship Guardianship'||selectedlabel=='Wrap-Around Services for Students with Disabilities'||
                  selectedlabel=='Special Education Services in an Approved Educational Placement')&& ageofCase>=22){
                    component.set("v.ShowSpinner",false);
                    component.set("v.AgeError2",true);
                    component.set("v.AgeError1",false);
                    component.set("v.AgeError",false);
                    helper.showToast('Error', 'Error', 'The person’s age on this case must be less than 22 years to use this mandate type. Please update your selection.');
                }
                //ends
            }
            //KIN 237
             var MandatedesMap= component.get("v.Mandatedescmap");
             console.log('MandatedesMap'+MandatedesMap);
             var Mandatearr=JSON.parse(JSON.stringify(MandatedesMap));
             var selMandateDesc;
             for(var i=0;i<Mandatearr.length;i++){
                 if(Mandatearr[i]['label']==selectedMandate){
                     console.log('selectedMandate= ',Mandatearr[i]['value']);
                     selMandateDesc = Mandatearr[i]['value'];
                     break;
                 }
             }
             console.log('selMandateDesc'+selMandateDesc);
             component.set("v.Mandatedescselected",selMandateDesc);
            component.set("v.showMandateDesc",true);
            // ends
        }    
        component.set("v.MandateSelected",selectedMandate);	
    },
    handleSPTChange : function(component,event,helper){
        //KIN 237
         component.set("v.showSNCDesc",false);
        var selectedOptionValue = event.getParam("value");       
        component.set("v.SPTselected",selectedOptionValue);
        //CHECK IF CATEGORY RATE EXISTS FOR SPT
        var categoryRate = component.get("v.categoryrate");
        if(categoryRate!=null && categoryRate!='' && categoryRate!=undefined &&
           categoryRate.Service_Placement_Type__c!=null && categoryRate.Service_Placement_Type__c!=undefined){            
        	component.set("v.categoryrate",'');
        }
        //SET LABEL TOO 
        var options = component.get("v.SPToptions");
        var selectedlabel='';
        for(var i=0; i< options.length ; i++){
            if(options[i].value==selectedOptionValue){
                selectedlabel=options[i].label;
                break;
            }
        }
        if(selectedlabel!='')
            component.set("v.SPTselectedlabel",selectedlabel);
         if(selectedOptionValue=='' || selectedOptionValue==undefined){
             //KIN 237
             component.set("v.showSPTDesc",false);
           component.set("v.SPTselectedlabel",'');            
                var items = [];
                var item = {                           
                        "label": "---None---",
                        "value": ''
                };
                items.push(item);
                component.set("v.SPTNames",items);  
                component.set("v.SPTNameselected",'');
                component.set("v.SPTNameselectedlabel",'');
           
        }
         else if(selectedOptionValue!='' && selectedOptionValue!=undefined){
              component.set("v.SPTNameselected",'');
              component.set("v.SPTNameselectedlabel",'');
              var items = [];
              var item = {                           
                        "label": "---None---",
                        "value": ''
                };
               items.push(item);
              component.set("v.SPTNames",items);
             //KIN 237
             var SPTdesMap= component.get("v.SPTdescmap");
             console.log('SPTdesMap'+SPTdesMap);
             var arr=JSON.parse(JSON.stringify(SPTdesMap));
             var selDesc;
             for(var i=0;i<arr.length;i++){
                 if(arr[i]['label']==selectedOptionValue){
                     console.log('selectedOptionValue= ',arr[i]['value']);
                     selDesc = arr[i]['value'];
                     break;
                 }
             }
             console.log('selDesc'+selDesc);
             component.set("v.SPTdescselected",selDesc);
             //ends
        	 helper.getSPTNamesforSPT(component,event,helper,selectedOptionValue);
             //KIN 237
             component.set("v.showSPTDesc",true);
        } // if of selectedoption ends here
    },
     handleSeviceNameChange : function(component,event,helper){
        var selectedOptionValue = event.getParam("value");
        console.log('**Selectedoptionval'+selectedOptionValue);
        if(selectedOptionValue=='')
             component.set("v.SPTNameselectedlabel",'');
        else{
            var options = component.get("v.SPTNames");
            var selectedlabel='';
            for(var i=0; i< options.length ; i++){
                if(options[i].value==selectedOptionValue){
                    selectedlabel=options[i].label;
                    break;
                }
            }
            if(selectedlabel!='')
                component.set("v.SPTNameselectedlabel",selectedlabel);
        }    
    	component.set("v.SPTNameselected",selectedOptionValue);
         //KIN 237
         helper.getSNCDesc(component,event,helper,selectedOptionValue);
    },
    SaveCSA : function(component,event,helper){
       component.set("v.ShowSpinner",true);
        if(component.get("v.ShowCSAButton")==true){ 
            helper.checkCSASave(component,event,helper);
            //KIN 228
            if(component.get("v.AgeError")==true){
                component.set("v.ShowSpinner",false);
                helper.showToast('Error', 'Error', 'The age of the child must be 18 years or less to use this mandate type. Please update your selection.');
            }
            if(component.get("v.AgeError1")==true){
                component.set("v.ShowSpinner",false);
                helper.showToast('Error', 'Error', 'The person’s age on this case must be 21 years or less to use this mandate type. Please update your selection.');
            }
            if(component.get("v.AgeError2")==true){
                component.set("v.ShowSpinner",false);
                helper.showToast('Error', 'Error', 'The person’s age on this case must be less than 22 years to use this mandate type. Please update your selection.');
            }
            //ends
            //KIN 228 added age condition in if
            if(component.get("v.CSAError")==false && component.get("v.AgeError")==false && component.get("v.AgeError1")==false && component.get("v.AgeError2")==false){
                 if((component.get("v.categoryrate")==null || component.get("v.categoryrate")=='')
                  && component.get("v.SPTselected")!=''){
                    console.log('**Entered in spt case');
                    helper.FetchSPTRate(component,event,helper);    
                }
                else{
                    component.set("v.ShowSpinner",false);
                    component.set("v.openModal",false);   
                }
               /* component.set("v.ShowSpinner",false);
                component.set("v.openModal",false);  */ 
            }
        }  
        else if(component.get("v.ShowIVEButton")==true){
        	helper.checkIVESave(component,event,helper);
            if(component.get("v.CSAError")==false){
            	component.set("v.ShowSpinner",false);
                    component.set("v.openModal",false);       
            }    
            
        }
        else if(component.get("v.ShowFundButton")==true){
            if(component.get("v.costcenterSelection")==undefined || component.get("v.costcenterSelection")==''){
         		component.set("v.ShowSpinner",false);
            	component.set("v.CSAError",true);
        		helper.showToast('Error', 'Error', 'Please add Cost Center.'); 
                
            }
            else{
                component.set("v.ShowSpinner",false);
                component.set("v.openModal",false);
            }    
        }
        else if(component.get("v.ShowSpecialWelfareButton")==true){
            if(component.get("v.TransCodeSelected")==undefined || component.get("v.TransCodeSelected")=='None' || component.get("v.TransCodeSelected")==''){
            	component.set("v.ShowSpinner",false);
            	component.set("v.CSAError",true);
        		helper.showToast('Error', 'Error', 'Please select transaction Code.');     
            }
            else if(component.get("v.costcenterSelection")==undefined || component.get("v.costcenterSelection")==''){
         		component.set("v.ShowSpinner",false);
            	component.set("v.CSAError",true);
        		helper.showToast('Error', 'Error', 'Please add Cost Center.'); 
                
         	}
            else{
                component.set("v.ShowSpinner",false);
                component.set("v.openModal",false); 
            }
        }
    },
    finalCancel : function(component,event,helper){
        helper.cancelForm(component,event,helper);
    },
    finalSave : function(component,event,helper){
        component.set("v.POError",false);
        component.set("v.ShowSpinner",false);
         component.set("v.Save",'Save');
        component.set("v.POSpinner",true);
        console.log('***cateogry type'+component.get("v.categoryType"));
        component.set("v.IsCSAAdminProfile",false);
        helper.validateForm(component,event,helper);
        /*if(component.get("v.POError")==false){
            helper.callSaveApex(component,event,helper,false);
        } */
            
    },
    CallSaveAndSubmitforAproval :function(component,event,helper){
    	component.set("v.POError",false);
         component.set("v.Save",'Not Save');
        component.set("v.ShowSpinner",false);
        component.set("v.POSpinner",true);
        component.set("v.IsCSAAdminProfile",false);
        console.log('***cateogry type'+component.get("v.categoryType"));
        helper.validateForm(component,event,helper);
       /* if(component.get("v.POError")==false){
            helper.callSaveApex(component,event,helper,true);
        }  */   
    },
    CallSaveApproved : function(component,event,helper){
    	component.set("v.POError",false);
         component.set("v.Save",'Save');
        component.set("v.ShowSpinner",false);
        component.set("v.POSpinner",true);
        //ADDED FOR FISCAL OFFICER TOO
        component.set("v.IsCSAAdminProfile",true);
        console.log('***cateogry type'+component.get("v.categoryType"));
        helper.validateForm(component,event,helper);    
    },
    closeCaseModel : function(component,event,helper){
        component.set("v.showCSACase",false);
        component.set("v.ConFirstName",'');
        component.set("v.HispanicEthnicity",'');
        component.set("v.ConLastName",'');
        component.set("v.DOB",'');
        component.set("v.Gender",'');
        component.set("v.Race",'');
        component.set("v.OasisClinetId",'');
        component.set("v.DSM",'');
        component.set("v.Medical",'');
        component.set("v.Referal",'');
        component.set("v.Clinical",'');
        component.set("v.Autism",'');
        component.set("v.TitleIVE",'');
        component.set("v.isIVECase",false);
        component.set("v.IsCSACase",false);
     	
    },
    SaveUpdatedCase :function(component,event,helper){
        component.set("v.ShowSpinner",true);
        component.set("v.CaseError",false);
        helper.checkCaseRequiredFields(component,event,helper);
        console.log('***enteed in afterr helper line');
        console.log('**CAse error val'+ component.get("v.CaseError"));
        if(component.get("v.CaseError") == false){
            console.log('**enterd here in caseerror no');
            helper.updateCase(component,event,helper);
        } 
    },
    //KIN 1311
    opennewNote : function (component, event, helper) {
        var cmpTarget = component.find('NoteModalbox');
		var cmpBack = component.find('NoteModalbackdrop');
		$A.util.addClass(cmpTarget, 'slds-fade-in-open');
		$A.util.addClass(cmpBack, 'slds-backdrop--open');
    },
    closeNoteModal : function(component,event,helper){
        component.set("v.note",{'sobjectType': 'ContentNote','Title': '','Content': ''})
        var cmpTarget = component.find('NoteModalbox');
		var cmpBack = component.find('NoteModalbackdrop');
		$A.util.removeClass(cmpBack,'slds-backdrop--open');
		$A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
    },
    createNote : function(component,event,helper){
        var newNote = component.get("v.note");
        console.log('newNote'+JSON.stringify(newNote));
        if(component.get("v.note").Title == "" || component.get("v.note").Title ==null) {
            helper.showToast('Error', 'Error', 'Please Fill Title of Note');
                
            }
        else{
        var action = component.get("c.createNoteRecord");
        action.setParams({
            nt : newNote,
            prntId : component.get("v.recordId")
        });
        action.setCallback(this,function(a){
            //get the response state
            var state = a.getState();
            //check if result is successfull
            if(state == "SUCCESS"){
                //Reset Form
                var not = {'sobjectType': 'ContentNote',
                                    'Title': '',
                                    'Content': ''
                                   };
                //resetting the Values in the form
                component.set("v.note",not);
                // KIN 1683
                component.find('noteButton').set("v.iconName","utility:check");
                var cmpTarget = component.find('NoteModalbox');
                var cmpBack = component.find('NoteModalbackdrop');
                $A.util.removeClass(cmpBack,'slds-backdrop--open');
                $A.util.removeClass(cmpTarget, 'slds-fade-in-open');
            } else if(state == "ERROR"){
                console.log('Error in calling server side action');
            }
        });
        //adds the server-side action to the queue        
        $A.enqueueAction(action);
        }
        
    },
    // ends KIN 1311
    // KIN 280
    newSWA : function (component, event, helper) {
        console.log('@@'+component.get("v.case"));
        var nameFieldValue = component.find("caseonSWA").set("v.value", component.get("v.case"));
        /*var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "Special_Welfare_Account__c",
            "defaultFieldValues": {
            'Case_KNP__c' : component.get("v.case")
        	}
        });
        createRecordEvent.fire();*/
        var cmpTarget = component.find('SWAModalbox');
		var cmpBack = component.find('Modalbackdrop');
		$A.util.addClass(cmpTarget, 'slds-fade-in-open');
		$A.util.addClass(cmpBack, 'slds-backdrop--open');
	},
    handleload :function(component,event,helper){
        var fValue1 = component.find("SSASSIonSWA").set("v.value", 0);
        var fvalue2 = component.find("childInitialBalance").set("v.value", 0);
    },
    handleSuccess :function(component,event,helper){
        var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "type":"Success",
                "title": "Success!",
                "message": "The SWA record has been created successfully."
            });
            toastEvent.fire();
         component.set("v.showNewSWA",false);
        var cmpTarget = component.find('SWAModalbox');
		var cmpBack = component.find('Modalbackdrop');
		$A.util.removeClass(cmpBack,'slds-backdrop--open');
		$A.util.removeClass(cmpTarget, 'slds-fade-in-open');
         //IF SWA IS CREATED 
         helper.CheckForSpecialWelfareAccount(component,event,helper);
    },
    handleSubmit :function(component,event,helper){
       
    },
    closeModal : function(component,event,helper){
        var cmpTarget = component.find('SWAModalbox');
		var cmpBack = component.find('Modalbackdrop');
		$A.util.removeClass(cmpBack,'slds-backdrop--open');
		$A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
    },
    // ends KIN 280
    CostCenterdataChange : function(component,event,helper){
     	var changedcostcenterval = event.getParam("CostCenter");       
        console.log('9999'+JSON.stringify(changedcostcenterval)); 
        component.set("v.costcenterSelection",changedcostcenterval);
        
    },
    handleSpecialWelfare :function(component,event,helper){
    	//component.set("v.OpenSWAModal",true); 
    	component.set("v.openModal",true); 
         //NOW COST CENTER IS MANDATORY
        component.set("v.showCostCenterLookup",true);
    },
    caseactionDetails: function(component,event,helper){
        helper.redirecttoRecord(component,event,helper,component.get("v.recordId") );
         $A.get('e.force:refreshView').fire();
    },
    newca : function(component,event,helper){
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:CaseActionPOLightningComponent"
            
        });
        evt.fire();
    },
    printca :function(component,event,helper){
       var recordid=component.get("v.recordId");
        console.log('**VAl of recordid'+recordid);
        
        var ddpid=$A.get("$Label.c.ca_print_ddpid");
        var deliveryid=$A.get("$Label.c.ca_print_delivery_option_id");
        var urlval='/apex/ProcessDdp?ddpId='+ddpid+'&deliveryOptionId='+deliveryid+'&ids='+recordid+'&sObjectType=Opportunity';
        console.log('**VAl of urlval'+urlval);
        var urlEvent = $A.get("e.force:navigateToURL");
        
        urlEvent.setParams({
          "url": urlval
        });
        urlEvent.fire();
    },
    caseactionListView : function(component,event,helper){
    	 window.location = '/lightning/n/My_Case_Actions';            
    },
    CallSaveandNew :function(component,event,helper){
        component.set("v.IsSavenNew",true);
        component.set("v.POError",false);
        component.set("v.ShowSpinner",false);
        component.set("v.POSpinner",true);
        component.set("v.Save",'Save');
         component.set("v.IsCSAAdminProfile",false);
        console.log('***cateogry type'+component.get("v.categoryType"));
        helper.validateForm(component,event,helper);
       
    }           
    

})