({
	doInit : function(cmp, event, helper) {
		var action = cmp.get("c.getRefreshPage");
        action.setParams({
            recId: cmp.get("v.recordId")
        })
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                if(response.getReturnValue() == true){
                    window.open('/'+cmp.get("v.recordId"),'_self');
                }
            }
		});
        $A.enqueueAction(action);
    }
                           
})