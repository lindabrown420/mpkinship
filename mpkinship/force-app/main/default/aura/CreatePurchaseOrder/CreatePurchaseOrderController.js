({
    doInit : function(component, event, helper) {
   		 helper.getInitialValues(component,event,helper);              
    },
    caseChange : function(component,event,helper){
    	var caseid = component.find("Caseauraid").get("v.value");
        console.log(caseid);
        component.set("v.caseId",caseid);
        console.log(caseid);
        if(caseid!=null && caseid!='' && caseid!=undefined){
            helper.getCaseValue(component,event,helper,caseid);
        }
        else
            component.set("v.CaseValue",'');
    },
    //KIN 1367
    handleUploadFinished : function(component,event,helper){
         // Get the list of uploaded files
        var uploadedFiles = event.getParam("files");
        console.log('uploadedFiles:'+uploadedFiles);
        console.log("Files uploaded : " + uploadedFiles.length);

        // Get the file name
        uploadedFiles.forEach(file =>{
            console.log(file.name);
            component.set("v.contentDocId",file.documentId);
        });
    },
    //KIN 228
    sendCasetoChild : function(component,event,helper){
        var caseid = component.find("Caseauraid").get("v.value");
        console.log(caseid);
        var objCompB = component.find('compchild');
        if(component.find('compchild'))
        objCompB.sampleMethod(JSON.parse(JSON.stringify(caseid))[0]);
    	//component.set("v.caseId",JSON.parse(JSON.stringify(caseid))[0]);
	},
    CampaingdataChanged: function(component,event,helper){
         var vendorid = component.get("v.vendorId");
        component.set("v.campaignId",'');
        console.log('**VAl of campaign id'+ component.get("v.campaignId"));
        component.set("v.POselectedServices",[]);
        component.set("v.IsSave",false);
        component.set("v.APbeginDate",'');
        component.set("v.APEndDate",'');
        component.set("v.pricebookentries",[]);
        component.set("v.Pricebookfound",false);
        component.set("v.ShowAddServices",false);
        component.set("v.pricebookandentries",[]);
        component.set("v.Ispricebookmessage",false);
        component.set("v.IsPricebookWithoutEntry",false);
        component.set("v.PricebookIdWithoutEntry",'');
        component.set("v.PricebookPresent",false);
        component.set("v.EntriesValues",'[]');
         component.set("v.PricebookId",'');
        var changedcampval = event.getParam("dataChange");       
        console.log('9999'+JSON.stringify(changedcampval));
         if(changedcampval!='' && changedcampval!=undefined && changedcampval.Id!=undefined){
         	component.set("v.campaignId",changedcampval.Id); 
             if(changedcampval!=undefined && changedcampval.FIPS__c!=undefined)
                 component.set("v.FIPSId",changedcampval.FIPS__c);
              helper.fetchBeginDate(component,event,helper);
         }
        var campaignId = component.get("v.campaignId");
        //CHECK FOR PRICEBOOK IF CAMPAIGNID IS NOT NULL AND VENDORID IS ALSO NOT NULL
        if(campaignId!='' && campaignId!=undefined && vendorid!='' && vendorid!=undefined){
            console.log('**Entered in bring pricebook here in campaign');  
            helper.fetchPricebook(component,event,helper);
        }
     },     
    CampaignChange : function(component,event,helper){        
        console.log('**Entered here');
        // var campaignId = event.getSource().get('v.value');
        var campaignId = component.find("campid").get("v.value");
        component.set("v.campaignId",campaignId);        
        var vendorid = component.get("v.vendorId");
        component.set("v.POselectedServices",[]);
        component.set("v.IsSave",false);
        component.set("v.APbeginDate",'');
        component.set("v.APEndDate",'');
        component.set("v.pricebookentries",[]);
        component.set("v.Ispricebookmessage",false);
        component.set("v.Pricebookfound",false);
        component.set("v.ShowAddServices",false);
        component.set("v.pricebookandentries",[]);
        
        component.set("v.IsPricebookWithoutEntry",false);
        component.set("v.PricebookIdWithoutEntry",'');
        //CHANGE THE BEGIN DATE FIRST
        helper.fetchBeginDate(component,event,helper);
        //CHECK FOR PRICEBOOK IF CAMPAIGNID IS NOT NULL AND VENDORID IS ALSO NOT NULL
        if(campaignId!='' && campaignId!=undefined && vendorid!='' && vendorid!=undefined){
            console.log('**Entered in bring pricebook here in campaign');  
            helper.fetchPricebook(component,event,helper);
        }
        
    },
    VendorChange : function(component,event,helper){
        var vendorid = event.getSource().get('v.value');       
        component.set("v.vendorId",vendorid);        
        var campaignId=component.get("v.campaignId");
        component.set("v.POselectedServices",[]);
        component.set("v.pricebookentries",[]);
         component.set("v.IsSave",false);
        component.set("v.ShowAddServices",false);
        component.set("v.pricebookandentries",[]);
        component.set("v.Pricebookfound",false);
        component.set("v.Ispricebookmessage",false);
        component.set("v.IsPricebookWithoutEntry",false);
        component.set("v.PricebookIdWithoutEntry",'');
        component.set("v.PricebookPresent",false);
        component.set("v.EntriesValues",'[]');
        component.set("v.PricebookId",'');
        //CHECK FOR PRICEBOOK IF CAMPAIGN AND VENDOR ID BOTH ARE NOT NULL
        if(vendorid!='' && vendorid!=undefined && campaignId!='' && campaignId!=undefined){
            console.log('**Entered in bring pricebook here in vendor');
            helper.fetchPricebook(component,event,helper);            
        } 
    },
    cancelpo :function(component,event,helper){
        helper.cancelForm(component,event,helper);
    },
    handleCancel:function(component,event,helper){
          helper.cancelForm(component,event,helper);
    },
    SelectedServices : function(component,event,helper){
     	var selectedServices = event.getParam("SelectedServices");
        console.log('**Entered in parent');
        console.log(JSON.stringify(selectedServices));
        component.set("v.POselectedServices",selectedServices);
        var ShowSave = event.getParam("IsShow");
        console.log(ShowSave);  
        console.log('**length of selectedservices'+ selectedServices.length);
        if(ShowSave==false && selectedServices!=undefined && selectedServices.length>0){
            component.set("v.IsSave",true);
            //CALCULATE TOTAL AMOUNT
            helper.CalculatePOSOTotal(component,event,helper,selectedServices);
        }    
        else
            component.set("v.IsSave",false);
    },
    CallSave :function(component,event,helper){
        component.set("v.Save",'Save');
        component.set("v.POError",false);
        component.set("v.ShowSpinner",true);
        console.log('**profile name'+component.get("v.profileName"));       
        component.set("v.IsCSAAdminProfile",false);
    	helper.validateForm(component,event,helper);  
        console.log('**VAl of poerror'+component.get("v.POError"));
       /* if(component.get("v.POError")==false){
            helper.savePO(component,event,helper,component.get("v.profileName"),false);
        } */
    },
    CallSaveAndSubmitforAproval : function(component,event,helper){
        component.set("v.Save",'Not Save');
        component.set("v.IsCSAAdminProfile",false);
        component.set("v.POError",false);
        component.set("v.ShowSpinner",true);
    	helper.validateForm(component,event,helper);  
       /*  if(component.get("v.POError")==false){
            helper.savePO(component,event,helper,component.get("v.profileName"),true);
        } */
    },
    CallSaveApproved : function(component,event,helper){
    	 component.set("v.Save",'Save');
        //USING THIS FOR FISCAL NOW TOO
        component.set("v.IsCSAAdminProfile",true);
        component.set("v.POError",false);
        component.set("v.ShowSpinner",true);
    	helper.validateForm(component,event,helper);      
    },
    update : function(component,event,helper){
        $A.get('e.force:refreshView').fire();
    },
    handleSendEmail : function(component,event,helper){
        var sendEmailvalue=component.find("sendEmailId").get("v.value");
        console.log('*******send email val'+sendEmailvalue);
        component.set("v.SendEmail",sendEmailvalue);
    },
    closeCaseModel : function(component,event,helper){
        component.set("v.showCSACase",false);
        component.set("v.ConFirstName",'');
        component.set("v.HispanicEthnicity",'');
        component.set("v.ConLastName",'');
        component.set("v.DOB",'');
        component.set("v.Gender",'');
        component.set("v.Race",'');
        component.set("v.OasisClinetId",'');
        component.set("v.DSM",'');
        component.set("v.Medical",'');
        component.set("v.Referal",'');
        component.set("v.Clinical",'');
        component.set("v.Autism",'');
        component.set("v.TitleIVE",'');
        component.set("v.isIVECase",false);
        component.set("v.IsCSACase",false);
     	
    },
    SaveUpdatedCase :function(component,event,helper){
        component.set("v.ShowSpinner",true);
        component.set("v.CaseError",false);
        helper.checkCaseRequiredFields(component,event,helper);
        console.log('***enteed in afterr helper line');
        if(component.get("v.CaseError")==false){
            console.log('**enterd here in caseerror no');
            helper.updateCase(component,event,helper);
        } 
    },
    //KIN 1311
    opennewNote : function (component, event, helper) {
        var cmpTarget = component.find('NoteModalbox');
		var cmpBack = component.find('NoteModalbackdrop');
		$A.util.addClass(cmpTarget, 'slds-fade-in-open');
		$A.util.addClass(cmpBack, 'slds-backdrop--open');
    },
    closeNoteModal : function(component,event,helper){
        component.set("v.note",{'sobjectType': 'ContentNote','Title': '','Content': ''})
        var cmpTarget = component.find('NoteModalbox');
		var cmpBack = component.find('NoteModalbackdrop');
		$A.util.removeClass(cmpBack,'slds-backdrop--open');
		$A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
    },
    createNote : function(component,event,helper){
        var newNote = component.get("v.note");
        console.log('newNote'+JSON.stringify(newNote));
        if(component.get("v.note").Title == "" || component.get("v.note").Title ==null) {
            helper.showToast('Error', 'Error', 'Please Fill Title of Note');
            
        }
        else{
            component.find('noteButton').set("v.iconName","utility:check");
        }
        var cmpTarget = component.find('NoteModalbox');
		var cmpBack = component.find('NoteModalbackdrop');
		$A.util.removeClass(cmpBack,'slds-backdrop--open');
		$A.util.removeClass(cmpTarget, 'slds-fade-in-open');
    },
    // ends KIN 1311
    //KIN 280
    newSWA : function (component, event, helper) {
        console.log('@@'+component.get("v.case"));
        var nameFieldValue = component.find("caseonSWA").set("v.value", component.get("v.caseId"));
        /*var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "Special_Welfare_Account__c",
            "defaultFieldValues": {
            'Case_KNP__c' : component.get("v.caseId"),
                'Initial_Balance__c' :0
        	}
        });
        createRecordEvent.fire();*/
        var cmpTarget = component.find('SWAModalbox');
		var cmpBack = component.find('Modalbackdrop');
		$A.util.addClass(cmpTarget, 'slds-fade-in-open');
		$A.util.addClass(cmpBack, 'slds-backdrop--open');
	},
    
    handleload :function(component,event,helper){
        var fValue1 = component.find("initialbal").set("v.value", 0);
    },
    handleSuccess :function(component,event,helper){
        var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "type" :"Success",
                "title": "Success!",
                "message": "The SWA record has been created successfully."
            });
            toastEvent.fire();
        component.set("v.showNewSWA",false);
        var cmpTarget = component.find('SWAModalbox');
		var cmpBack = component.find('Modalbackdrop');
		$A.util.removeClass(cmpBack,'slds-backdrop--open');
		$A.util.removeClass(cmpTarget, 'slds-fade-in-open');
    },
    handleSubmit :function(component,event,helper){
       
    },
    closeModal : function(component,event,helper){
        var cmpTarget = component.find('SWAModalbox');
		var cmpBack = component.find('Modalbackdrop');
		$A.util.removeClass(cmpBack,'slds-backdrop--open');
		$A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
    },
    // ends KIN 280
    // KIN 890
    CallSaveandNew :function(component,event,helper){
        component.set("v.Save",'Save');
        component.set("v.IsSavenNew",true);
        component.set("v.POError",false);
        component.set("v.ShowSpinner",true);
        console.log('**profile name'+component.get("v.profileName"));       
        component.set("v.IsCSAAdminProfile",false);
    	helper.validateForm(component,event,helper);  
        console.log('**VAl of poerror'+component.get("v.POError"));
       
        /*var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:CreatePurchaseOrder"
            
        });
        evt.fire();*/
    },
            
    POSODetails :function(component,event,helper){
    	helper.redirecttoRecord(component,event,helper,component.get("v.recordId") );
        $A.get('e.force:refreshView').fire();            
    },
    printPOSO :function(component,event,helper){
        var recordid=component.get("v.recordId");
        console.log('**VAl of recordid'+recordid);
        
        var ddpid=$A.get("$Label.c.POSO_print_ddpid");
        var deliveryid=$A.get("$Label.c.Poso_print_delivery_id");
        var urlval='/apex/ProcessDdp?ddpId='+ddpid+'&deliveryOptionId='+deliveryid+'&ids='+recordid+'&sObjectType=Opportunity';
        console.log('**VAl of urlval'+urlval);
        var urlEvent = $A.get("e.force:navigateToURL");
        
        urlEvent.setParams({
          "url": urlval
        });
        urlEvent.fire();
                
     },
    newPOSO :function(component,event,helper){
   		var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:CreatePurchaseOrder"
            
        });
        evt.fire();             
    },
    printVI :function(component,event,helper){
    	var recordid=component.get("v.recordId");
        console.log('**VAl of recordid'+recordid);
        
        var ddpid=$A.get("$Label.c.Print_VI_ddpID");
        var deliveryid=$A.get("$Label.c.Print_VI_delivery_Option");
        var urlval='/apex/ProcessDdp?ddpId='+ddpid+'&deliveryOptionId='+deliveryid+'&ids='+recordid+'&sObjectType=Opportunity';
        console.log('**VAl of urlval'+urlval);
        var urlEvent = $A.get("e.force:navigateToURL");
        
        urlEvent.setParams({
          "url": urlval
        });
        urlEvent.fire();            
    },
    POSOandVI :function(component,event,helper){
    	var recordid=component.get("v.recordId");
        console.log('**VAl of recordid'+recordid);
        
        var ddpid=$A.get("$Label.c.POSO_AND_VI_Print_ddpId");
        var deliveryid=$A.get("$Label.c.POSO_and_VI_Print_deliveryId");
        var urlval='/apex/ProcessDdp?ddpId='+ddpid+'&deliveryOptionId='+deliveryid+'&ids='+recordid+'&sObjectType=Opportunity';
        console.log('**VAl of urlval'+urlval);
        var urlEvent = $A.get("e.force:navigateToURL");
        
        urlEvent.setParams({
          "url": urlval
        });
        urlEvent.fire();            
    }            	
})