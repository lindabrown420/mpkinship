({
    getInitialValues : function(component,event,helper){
    	var action=component.get("c.FetchInitialValues");
         action.setCallback(this,function(response){           
            var state = response.getState();
            console.log('**State val'+state);
            if(state==="SUCCESS"){
                 var result=response.getReturnValue(); 
                 console.log(JSON.stringify(result));
                if(result.profileName!=null){
                    component.set("v.profileName",result.profileName);
                }
                if(result.accountingPeriodId!=null)
                    component.set("v.campaignId",result.accountingPeriodId);
                if(result.accountingPeriod!=null){
                    component.set("v.selectedLookupRecord",result.accountingPeriod);
                }  
                if(result.APstartDate!=null){
                    component.set("v.APbeginDate",result.APstartDate);
                    component.set("v.closeDate",result.APstartDate);
                }   
                if(result.APendDate!=null)
                    component.set("v.APEndDate",result.APendDate);
            }   
        });
        $A.enqueueAction(action); 
    },
	fetchPricebook : function(component,event,helper) {
		console.log('**fetch pricebook entered'); 		
        var action = component.get("c.getPricebook");
         action.setParams({          	                
                campaignId : String(component.get("v.campaignId")),
                vendorId : String(component.get("v.vendorId"))
        });	
        action.setCallback(this,function(response){
           
            var state = response.getState();
            console.log('**State val'+state);            
            console.log('**REsponse return val'+JSON.stringify(response.getReturnValue()));
            if(state==="SUCCESS"){
            	var result=response.getReturnValue();  
                console.log('&&&&&&&&&&&&'+result.message);
                if(result!=null && result.message!=null && result.message=='Got pricebook'){                   
                    console.log('**resutl.pricebookrec'+JSON.stringify(result.Pricebookrec));
                    component.set("v.pricebookandentries",result.Pricebookrec);
                    if(result.Pricebookrec!=null && result.Pricebookrec.PricebookEntries!=null 
                       && result.Pricebookrec.PricebookEntries!=undefined){
                    	component.set("v.pricebookentries",result.Pricebookrec.PricebookEntries);
                        component.set("v.Pricebookfound",true);
                    }    
                    
                    if(result.Pricebookrec!=null && result.Pricebookrec.Accounting_Period__c!=null &&
                       result.Pricebookrec.Accounting_Period__c!=undefined && result.Pricebookrec.Accounting_Period__r.StartDate!=null)
                   		component.set("v.APbeginDate",result.Pricebookrec.Accounting_Period__r.StartDate);
                    
                    if(result.Pricebookrec!=null && result.Pricebookrec.Accounting_Period__c!=null && result.Pricebookrec.Accounting_Period__c!=undefined
                        && result.Pricebookrec.Accounting_Period__r.EndDate!=null)
                    	component.set("v.APEndDate",result.Pricebookrec.Accounting_Period__r.EndDate);
                    	component.set("v.Ispricebookmessage",false);
                    	//ADDED
            			component.set("v.ShowAddServices",true);
                }
                else if(result!=null && result.message!=null && result.message=='Got PB without entry'){
                    console.log('**Enterd in 2nd');
                    component.set("v.pricebookandentries",'');
                    component.set("v.IsPricebookWithoutEntry",true);
                    component.set("v.PricebookIdWithoutEntry",result.Pricebookrec.Id);
                    component.set("v.Ispricebookmessage",true); 
                	component.set("v.messageText",'There are no active services for the pricebook associated with the selected vendor for the selected Parent Fiscal year. Please reach out to the Finance department to create the services for the pricebook. You will be able to continue by selecting a different vendor.');
                	console.log('**msgtext'+component.get("v.messageText"));
                    //ADDED
                    if(component.get("v.profileName")!=undefined && (component.get("v.profileName")=='System Administrator' || 
                       component.get("v.profileName")=='Fiscal Officer' || component.get("v.profileName")=='CSA Admin' )){
                    //if(component.get("v.profileName")!=undefined && (component.get("v.profileName")=='Fiscal Officer' || component.get("v.profileName")=='CSA Admin' )){ 
                    	component.set("v.ShowAddServices",true);
                        component.set("v.messageText",'There are no active pricebooks associated with the selected vendor for the selected Parent Fiscal year. Please click on add button to add a new service.');
                    }    
                    component.set("v.Pricebookfound",false);
                }
                else if(result!=null && result.message!=null && result.message=='No pricebook found'){
                    component.set("v.Ispricebookmessage",true); 
                    component.set("v.pricebookandentries",'');
                    component.set("v.messageText",'There are no active services for the pricebook associated with the selected vendor for the selected Parent Fiscal year. Please reach out to the Finance department to create the services for the pricebook. You will be able to continue by selecting a different vendor.');
            		//ADDED
            		if(component.get("v.profileName")!=undefined && (component.get("v.profileName")=='System Administrator' || 
                      component.get("v.profileName")=='Fiscal Officer' || component.get("v.profileName")=='CSA Admin' )){
                   /* if(component.get("v.profileName")!=undefined && (component.get("v.profileName")=='Fiscal Officer' || component.get("v.profileName")=='CSA Admin' )){ */
                    	component.set("v.ShowAddServices",true);
                        component.set("v.messageText",'There are no active pricebooks associated with the selected vendor for the selected Parent Fiscal year. Please click on add button to add a new service.');
                    }    
                    component.set("v.Pricebookfound",false);
                }
            }
            else if(state==="ERROR"){
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
                component.set("v.Ispricebookmessage",true);
                component.set("v.messageText",'Failed to retrieve the pricebook.');
                component.set("v.pricebookandentries",'');
            }
         });
        $A.enqueueAction(action);      
	},
    cancelForm : function(component,event,helper){
         window.location = '/lightning/n/Purchase_Of_Service_Order';
       
    },
    getCaseValue: function(component,event,helper,caseid){       
    	var action = component.get("c.getCase"); 
        action.setParams({ caseId : caseid.toString()});
        action.setCallback(this,function(response){           
            var state = response.getState();
            console.log('**State val'+state);
            if(state==="SUCCESS"){
                 var result=response.getReturnValue(); 
                 console.log(JSON.stringify(result));
                if(result!=null)
                    component.set("v.CaseValue",result);
                else
                    component.set("v.CaseValue",'');
            }  
              else if(state==="ERROR"){
              	component.set("v.CaseValue",'');
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
               
            }
         });
        $A.enqueueAction(action); 
        
    },
    validateForm : function(component,event,helper){
        if(component.get("v.POselectedServices")!=undefined && component.get("v.POselectedServices").length<=0){
        	component.set("v.ShowSpinner",false);
            component.set("v.POError",true);
        	this.showToast('Error','error','Services cannot be empty.');        
        }            
        var caseid = component.get("v.caseId");
        if(caseid=='' || caseid==undefined || caseid==null){
            component.set("v.ShowSpinner",false);
            component.set("v.POError",true);
        	this.showToast('Error','error','Please provide a Case.');               
        }
        else if(component.get("v.campaignId")=='' || component.get("v.campaignId")==undefined 
                || component.get("v.campaignId")==null){
            component.set("v.ShowSpinner",false);
            component.set("v.POError",true);
        	this.showToast('Error','error','Please provide a Parent Fiscal Year.');    
        }
        else if(component.get("v.vendorId")=='' || component.get("v.vendorId")==undefined 
                || component.get("v.vendorId")==null){
            component.set("v.ShowSpinner",false);
        	component.set("v.POError",true);
        	this.showToast('Error','error','Please provide a Vendor.');     
        }
        else if(component.get("v.closeDate")=='' || component.get("v.closeDate")==undefined 
                || component.get("v.closeDate")==null){
            component.set("v.ShowSpinner",false);
        	component.set("v.POError",true);
        	this.showToast('Error','error','Please provide a Begin Date.');     
        }
        else if(component.get("v.EndDate")=='' || component.get("v.EndDate")==undefined 
                || component.get("v.EndDate")==null){
             component.set("v.ShowSpinner",false);
        	component.set("v.POError",true);
        	this.showToast('Error','error','Please provide an End Date.');     
        }
        else if(component.get("v.closeDate")!='' && component.get("v.closeDate")!=undefined && 
               component.get("v.EndDate")!='' && component.get("v.EndDate")!=undefined && 
                component.get("v.closeDate") > component.get("v.EndDate")){
            component.set("v.ShowSpinner",false);
            component.set("v.POError",true);
            this.showToast('Error','error','Purchase Order End Date should be greater than Purchase Order Begin Date.');
        }
        else if(component.get("v.closeDate")!='' && component.get("v.closeDate")!=undefined &&
             component.get("v.APbeginDate")!='' && component.get("v.APbeginDate")!=undefined 
             && component.get("v.APEndDate")!='' && component.get("v.APEndDate")!=undefined &&
             (component.get("v.closeDate") < component.get("v.APbeginDate") || 
             component.get("v.closeDate") > component.get("v.APEndDate"))){
             component.set("v.ShowSpinner",false);
             component.set("v.POError",true);
             this.showToast('Error','error','Purchase Order Begin Date should lie between its Parent Fiscal Year dates.');
        }
        else if(component.get("v.EndDate")!='' && component.get("v.EndDate")!=undefined && 
              component.get("v.APEndDate")!='' &&  component.get("v.APEndDate")!=undefined && 
              component.get("v.EndDate") > component.get("v.APEndDate")){
             component.set("v.ShowSpinner",false);
             component.set("v.POError",true);
             this.showToast('Error','error','Purchase Order End Date should lie between the Purchase Order Begin Date and its Parent Fiscal Year End date.');
        }
         else if(caseid!='' && caseid!=undefined && caseid!=null && component.get("v.POselectedServices")!=undefined && component.get("v.POselectedServices").length>0){
            var action = component.get("c.getCase"); 
        	action.setParams({ caseId : caseid.toString()});
         	action.setCallback(this,function(response){           
            var state = response.getState();
            console.log('**State val'+state);
            if(state==="SUCCESS"){
                 var result=response.getReturnValue(); 
                 console.log(JSON.stringify(result));
                if(result!=null){
                    component.set("v.CaseValue",result);
                    var caseval=component.get("v.CaseValue");
                    console.log('**VAl of caeval'+JSON.stringify(caseval));
                    if(caseval!=null && caseval!=undefined){
                        if(caseval.Primary_Contact__r!=undefined && caseval.Primary_Contact__r.FirstName !=undefined)
                            component.set("v.ConFirstName",caseval.Primary_Contact__r.FirstName);
                        if(caseval.Primary_Contact__r!=undefined && caseval.Primary_Contact__r.LastName !=undefined)
                            component.set("v.ConLastName",caseval.Primary_Contact__r.LastName);
                        if(caseval.DOB__c!=undefined && caseval.DOB__c!=null)
                            component.set("v.DOB",caseval.DOB__c);
                        if(caseval.Race__c!=undefined)
                            component.set("v.Race",caseval.Race__c);
                        if(caseval.Hispanic_Ethnicity__c !=undefined)
                            component.set("v.HispanicEthnicity",caseval.Hispanic_Ethnicity__c);
                        if(caseval.Gender_Preference__c !=undefined)
                            component.set("v.Gender",caseval.Gender_Preference__c);
                        if(caseval.OASIS_Client_ID__c!=undefined)
                            component.set("v.OasisClinetId",caseval.OASIS_Client_ID__c);
                        if(caseval.Title_IVE_Eligibility__c!=undefined)
                            component.set("v.TitleIVE", caseval.Title_IVE_Eligibility__c);
                        if(caseval.DSM_V_Indicator_ICD_10_1__c!=undefined)
                            component.set("v.DSM", caseval.DSM_V_Indicator_ICD_10_1__c);
                         if(caseval.Clinical_Medication_Indicator_1__c!=undefined)
                            component.set("v.Clinical", caseval.Clinical_Medication_Indicator_1__c);
                         if(caseval.Medicaid_Indicator_1__c!=undefined)
                            component.set("v.Medical", caseval.Medicaid_Indicator_1__c);
                         if(caseval.Referral_Source__c!=undefined)
                            component.set("v.Referal", caseval.Referral_Source__c);
                         if(caseval.Autism_Flag_1__c!=undefined)
                            component.set("v.Autism", caseval.Autism_Flag_1__c);
                    }
                    
                    var services = component.get("v.POselectedServices");
                    for(var i=0 ; i< services.length ; i++){
                       
                        if(services[i].categorySelection!=undefined && services[i].categorySelection.Category_Type__c!=undefined && 
                           services[i].categorySelection.Category_Type__c=='CSA' && (caseval.Primary_Contact__c==undefined || caseval.Primary_Contact__r.LastName==null || caseval.Primary_Contact__r.FirstName==null
                         || caseval.DOB__c==null || caseval.Gender_Preference__c==null || caseval.Race__c==null || caseval.Autism_Flag_1__c==null ||
                          caseval.DSM_V_Indicator_ICD_10_1__c ==null || caseval.Clinical_Medication_Indicator_1__c==null || caseval.Medicaid_Indicator_1__c==null || 
                        caseval.Referral_Source__c==null ||  caseval.Hispanic_Ethnicity__c ==null || caseval.Primary_Contact__r.LastName==undefined || caseval.Primary_Contact__r.FirstName==undefined
                        || caseval.DOB__c==undefined || caseval.Gender_Preference__c==undefined || caseval.Race__c==undefined || caseval.Autism_Flag_1__c==undefined ||
                         caseval.DSM_V_Indicator_ICD_10_1__c ==undefined || caseval.Clinical_Medication_Indicator_1__c==undefined || caseval.Medicaid_Indicator_1__c==undefined || 
                          caseval.Referral_Source__c==undefined ||  caseval.Hispanic_Ethnicity__c ==undefined || caseval.Title_IVE_Eligibility__c==undefined || caseval.Title_IVE_Eligibility__c=='')){
                            console.log('***eNTERED HERE');
                            component.set("v.ShowSpinner",false);
                            component.set("v.POError",true);
                            component.set("v.IsCSACase",true);
                            //component.set("v.showCSACase",true);
                           // this.showIncreaseToast('Error','error','For CSA category,Please check if any of these fields -Firstname, Lastname, DOB, Gender,Race, Hispanic Ethnicity, Autism, DSM V Indicator/ICD-10, Clinical Medication Indicator, Medicaid Indicator, Referral Source are not filled on associated case.');
                            //break;
                        } 
                       /* if(services[i].categorySelection!=undefined && services[i].categorySelection.Category_Type__c!=undefined && services[i].categorySelection.Category_Type__c=='IVE'){
                             //TO NOT ALLOW IVE CATEGORY IF TITLE IVE OF CASE IS NO
                            if(caseval!=undefined && caseval.Title_IVE_Eligibility__c!=undefined && caseval.Title_IVE_Eligibility__c=='1'){
                            	component.set("v.ShowSpinner",false);
                                component.set("v.POError",true);
                                this.showIncreaseToast('Error','error','You cannot use category of type IV-E if its associated case title IV-E eligibility field has no value.');
                                break;    
                            } */
                            if (services[i].categorySelection!=undefined && services[i].categorySelection.Category_Type__c!=undefined && services[i].categorySelection.Category_Type__c=='IVE' &&
                                (caseval.Primary_Contact__c==undefined || caseval.Primary_Contact__r.LastName==null || caseval.Primary_Contact__r.FirstName==null
                              || caseval.DOB__c==null || caseval.Gender_Preference__c==null || caseval.Race__c==null || caseval.OASIS_Client_ID__c==null ||
                              caseval.Hispanic_Ethnicity__c ==null || caseval.Primary_Contact__r.LastName==undefined || caseval.Primary_Contact__r.FirstName==undefined
                               || caseval.DOB__c==undefined || caseval.Gender_Preference__c==undefined || caseval.Race__c==undefined || caseval.OASIS_Client_ID__c==undefined ||
                                caseval.Hispanic_Ethnicity__c ==undefined || caseval.Title_IVE_Eligibility__c==undefined || caseval.Title_IVE_Eligibility__c=='')){
                                component.set("v.ShowSpinner",false);
                                component.set("v.POError",true);
                                component.set("v.isIVECase",true);
                                //this.showIncreaseToast('Error','error','FOR IVE category,Please check if any of these fields -Firstname, Lastname, DOB, Gender,Race, Hispanic Ethnicity or Oasis Client ID are not filled on associated case.');
                                //break;
                            } 
                         /**CHECK IF BOTH ARE TRUE ***/
                        if(component.get("v.isIVECase")==true && component.get("v.IsCSACase")==true){
                        	component.set("v.showCSACase",true);
                            this.showToast('Error','error','Please fill these required case fields first');
                             component.set("v.ShowSpinner",false);
                             component.set("v.POError",true);
                            break;
                        }	
                           
                    } //FOR LOOP ENDS HERE
                    //FOR CSA CHECK 
                    if(component.get("v.POError")==true && component.get("v.IsCSACase")==true){
                    	component.set("v.showCSACase",true);
                        this.showToast('Error','error','Please fill these required case fields first');
                             component.set("v.ShowSpinner",false);
                             component.set("v.POError",true);    
                    }
                    else if(component.get("v.POError")==true && component.get("v.isIVECase")==true){
                    	component.set("v.showCSACase",true);
                        this.showToast('Error','error','Please fill these required case fields first');
                             component.set("v.ShowSpinner",false);
                             component.set("v.POError",true);        
                    }
                    //SENDING FOR SAVE IF NO ERROR FOUND
                    if(component.get("v.POError")==false){
                        console.log('**VAl of save'+component.get("v.Save"));
                        if(component.get("v.Save")=='Save'){
                       		this.savePO(component,event,helper,component.get("v.profileName"),false);
                        }
                        else
                           this.savePO(component,event,helper,component.get("v.profileName"),true); 
                    }
                }    
                else{
                     component.set("v.ShowSpinner",false);
             		component.set("v.POError",true);
                    this.showToast('Error','error','Unable to find case record');
                }   
            }  
              else if(state==="ERROR"){
                    component.set("v.CaseValue",'');
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                     errors[0].message);
                             component.set("v.ShowSpinner",false);
                             component.set("v.POError",true);
                              this.showToast('Error','error',errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                        component.set("v.ShowSpinner",false);
                        component.set("v.POError",true);
                        this.showToast('Error','error','Unknown error');
                	}               
         		}
         	});
        	$A.enqueueAction(action);  
        }
    },
    showToast : function(type, title, message) {
       
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : title,
            message: message,
            duration:' 5000',
            key: 'info_alt',
            type: type,
            mode: 'pester'
        });
        toastEvent.fire();
    } ,
    showIncreaseToast : function(type, title, message) {
       
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : title,
            message: message,
            duration:' 10000',
            key: 'info_alt',
            type: type,
            mode: 'pester'
        });
        toastEvent.fire();
    } ,
    CalculatePOSOTotal : function(component,event,helper,selectedServices){
    	console.log('**Enterd in poso total');
        console.log(JSON.stringify(selectedServices));
        if(selectedServices!=undefined && selectedServices.length >0){
            var total=0;
            for(var i=0 ; i < selectedServices.length ; i++){
                if(selectedServices[i].UnitPrice!=undefined && selectedServices[i].quantity!=undefined){
                    total = total + (selectedServices[i].UnitPrice * selectedServices[i].quantity);
                }
            }
            component.set("v.TotalAmount",total);
        }
    },
    savePO : function(component,event,helper,profilename,submitforApproval){
        console.log('**Enterd in savepo');
    	 var POobject ={};
        var opportunity ={};
        var caseid = component.get("v.caseId");
        var campaignId=component.get("v.campaignId");
        var vendorId = component.get("v.vendorId");
        var beginDate = component.get("v.closeDate");
        var endDate = component.get("v.EndDate");
        var FipsId = component.get("v.FIPSId");
        var contractId = component.get("v.ContractId");
        var sendEmail = component.get("v.SendEmail");
        console.log('**VAl of sendeamil'+sendEmail);
        console.log('***here');
        opportunity.Kinship_Case__c = caseid;
        opportunity.CampaignId=campaignId;
        opportunity.accountid = vendorId;
        opportunity.closedate = beginDate;
        opportunity.End_Date__c=endDate;
        opportunity.Send_Email__c =sendEmail;
        if(FipsId==null || FipsId=='')
             opportunity.FIPS_Code__c =null;
        else
        	opportunity.FIPS_Code__c =FipsId;
        if(contractId==null || contractId=='')
            opportunity.ContractId =null;
        else
        	opportunity.ContractId = contractId;
      
        POobject.Opp = opportunity;
        console.log(POobject);
        console.log(JSON.stringify(POobject));
        var selectedoppitems = component.get("v.POselectedServices");
        console.log('**selected oppitems list'+ selectedoppitems.length);
         console.log('**selected oppitems list'+ JSON.stringify(selectedoppitems));
          var SelectedProducts =[];  
        for(var i =0 ; i < selectedoppitems.length ; i++){
            console.log('**AVl of i'+ i);
            console.log('**selectedoppitem'+JSON.stringify(selectedoppitems[i]));
            var services={}; 
            if(selectedoppitems[i].Pricebook2Id !=undefined)
            	POobject.pricebookid=selectedoppitems[i].Pricebook2Id;    
            if(selectedoppitems[i].Product2Id!=undefined)
            	services.Product2Id= selectedoppitems[i].Product2Id;
            if(selectedoppitems[i].Unit_Measure_Label__c !=undefined)
            	services.unitLabel=selectedoppitems[i].Unit_Measure_Label__c;
            if(selectedoppitems[i].Service_Unit_Measure__c!=undefined)
            	services.unitMeasure = selectedoppitems[i].Service_Unit_Measure__c;
            services.UnitPrice=selectedoppitems[i].UnitPrice;
            services.quantity= selectedoppitems[i].quantity;
            if(selectedoppitems[i].Id!=undefined)
            	services.pricebookentryid=selectedoppitems[i].Id;                  
            services.categoryid=selectedoppitems[i].categorySelection.Id;
            if(selectedoppitems[i].recipient!=undefined && selectedoppitems[i].recipient!='')    
            	services.recipient=selectedoppitems[i].recipient;
            if(selectedoppitems[i].SPTNameselected!='' && selectedoppitems[i].SPTNameselected!=undefined)
                services.SPTNameselected=selectedoppitems[i].SPTNameselected;
            if(selectedoppitems[i].SPTselected!='' && selectedoppitems[i].SPTselected!=undefined)
                services.SPTselected=selectedoppitems[i].SPTselected;
            if(selectedoppitems[i].MandateSelected!='' && selectedoppitems[i].MandateSelected!=undefined)
                services.MandateSelected=selectedoppitems[i].MandateSelected;
            if(selectedoppitems[i].SPTselectedlabel!='' && selectedoppitems[i].SPTselectedlabel!=undefined)
                services.SPTselectedlabel=selectedoppitems[i].SPTselectedlabel;
            if(selectedoppitems[i].SPTNameselectedlabel!='' && selectedoppitems[i].SPTNameselectedlabel!=undefined)
                services.SPTNameselectedlabel=selectedoppitems[i].SPTNameselectedlabel;
            if(selectedoppitems[i].MandateSelectedlabel!='' && selectedoppitems[i].MandateSelectedlabel!=undefined)
                services.MandateSelectedlabel=selectedoppitems[i].MandateSelectedlabel;
            if(selectedoppitems[i].Description!='' && selectedoppitems[i].Description!=undefined)
                services.Description=selectedoppitems[i].Description;
            if(selectedoppitems[i].recipientIVE !=undefined && selectedoppitems[i].recipientIVE !='')
               services.recipientIvE = selectedoppitems[i].recipientIVE;
            //ADDED FOR NEW SERVICES 
            if(selectedoppitems[i].IsNewService !=undefined)
                services.IsNewService =selectedoppitems[i].IsNewService;
             if(selectedoppitems[i].serviceProduct!= undefined ){
               if(selectedoppitems[i].serviceProduct.product!=undefined)
                 services.ProductSelectedId = selectedoppitems[i].serviceProduct.product.Id;
             	if(selectedoppitems[i].serviceProduct.UnitPrice!=undefined)
                 services.ProductSelectedUnitPrice = selectedoppitems[i].serviceProduct.UnitPrice;
             }
             services.ServiceName = selectedoppitems[i].Service_Name__c;
             services.ServiceDescription = selectedoppitems[i].Service_Description__c;
        	SelectedProducts.push(services);
        } 
         POobject.oppproduct =SelectedProducts; 
        console.log('**Val of poobbject'+JSON.stringify(POobject));
         var action = component.get("c.CreatePO");
        action.setParams({          	                
                POwrapper : JSON.stringify(POobject),
            	Profile : profilename,
            	SubmitforApproval : submitforApproval,
            	isNoEntryPricebook : component.get("v.IsPricebookWithoutEntry"),
            	PricbeookIDForNoEntry : component.get("v.PricebookIdWithoutEntry"),
            	IsCSAAdminProfile : component.get("v.IsCSAAdminProfile")
        });	
         action.setCallback(this,function(response){           
            var state = response.getState();
             console.log('**Enterd in state');
             if(state==="SUCCESS"){
               component.set("v.ShowSpinner",false);
                var result=response.getReturnValue(); 
                 console.log(JSON.stringify(result));
                if(result.recordid!=null){
                    if(result.message =='record created'){
                    	 this.showToast('Success','Success','Purchase Order record has been created successfully!');                         
                    	this.redirecttoRecord(component,event,helper,result.recordid );
                         $A.get('e.force:refreshView').fire();
                    }
                    else 
                        this.showToast('Success','Success','Purchase Order record has been created and submitted for approval successfully!');    
                    	
                    	this.redirecttoRecord(component,event,helper,result.recordid );
                    	$A.get('e.force:refreshView').fire();
                    
                }
                
            }   
             else if(state=="ERROR"){
                 component.set("v.ShowSpinner",false);
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                this.showToast('Error', 'Error', strErr);
             }
         });
        $A.enqueueAction(action); 
    },    
  
    redirecttoRecord : function(component,event,helper,recordid){
         var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": recordid,
            "slideDevName": "related"
        });
        navEvt.fire();
        
    },
      closeCaseModel : function(component,event,helper){
        component.set("v.showCSACase",false);
        component.set("v.ConFirstName",'');
        component.set("v.HispanicEthnicity",'');
        component.set("v.ConLastName",'');
        component.set("v.DOB",'');
        component.set("v.Gender",'');
        component.set("v.Race",'');
        component.set("v.OasisClinetId",'');
        component.set("v.DSM",'');
        component.set("v.Medical",'');
        component.set("v.Referal",'');
        component.set("v.Clinical",'');
        component.set("v.Autism",'');
        component.set("v.TitleIVE",'');
        component.set("v.isIVECase",false);
        component.set("v.IsCSACase",false);
     	
    },
    fetchBeginDate : function(component,event,helper){
    	var action=component.get("c.FetchBeginDate");
         action.setParams({          	                
                campaignId : String(component.get("v.campaignId"))
        });	
         action.setCallback(this,function(response){           
            var state = response.getState();
            console.log('**State val'+state);
            if(state==="SUCCESS"){
                 var result=response.getReturnValue(); 
                 console.log(JSON.stringify(result));              
                if(result.accountingPeriodId!=null)
                    component.set("v.campaignId",result.accountingPeriodId);
                if(result.APstartDate!=null){
                    component.set("v.APbeginDate",result.APstartDate);
                    component.set("v.closeDate",result.APstartDate);
                }   
                if(result.APendDate!=null)
                    component.set("v.APEndDate",result.APendDate);
            } 
             else{
                 component.set("v.APbeginDate",'');
                 component.set("v.closeDate",'');
                 component.set("v.APEndDate",'');
             }
        });
        $A.enqueueAction(action); 
    },
    checkCaseRequiredFields : function(component,event,helper){
        var isCSA = component.get("v.IsCSACase");
        var isIVE = component.get("v.isIVECase");
        console.log('**VAl of hispanic'+component.get("v.HispanicEthnicity"));
        if(isCSA==true || isIVE==true){
            console.log('**inside iscsa nad isive');
            if(component.get("v.ConFirstName")==undefined || component.get("v.ConFirstName")==''){
          		component.set("v.ShowSpinner",false);
                component.set("v.CaseError",true);
        		this.showToast('Error','error','Please provide a Contact First Name.');     
            }
            else if(component.get("v.ConLastName")==undefined || component.get("v.ConLastName")==''){
          		component.set("v.ShowSpinner",false);
                component.set("v.CaseError",true);
        		this.showToast('Error','error','Please provide a Contact Last Name.');     
            }
          else if(component.get("v.DOB")==undefined || component.get("v.DOB")==''){
          		component.set("v.ShowSpinner",false);
              component.set("v.CaseError",true);
        		this.showToast('Error','error','Please provide a DOB.');     
            }
          else if(component.get("v.Gender")==undefined || component.get("v.Gender")==''){
          		component.set("v.ShowSpinner",false);
               component.set("v.CaseError",true);
        		this.showToast('Error','error','Please provide a Gender.');     
            }
           else if(component.get("v.Race")==undefined || component.get("v.Race")==''){
          		component.set("v.ShowSpinner",false);
                component.set("v.CaseError",true);
        		this.showToast('Error','error','Please provide a Race.');     
            }
           else if(component.get("v.HispanicEthnicity")==undefined || component.get("v.HispanicEthnicity")==''){
          		console.log('**entered in hispanic');
               component.set("v.ShowSpinner",false);
               component.set("v.CaseError",true);
        		this.showToast('Error','error','Please provide a Hispanic Ethnicity.');     
            }
          else if(component.get("v.TitleIVE")==undefined || component.get("v.TitleIVE")==''){
               component.set("v.ShowSpinner",false);
              component.set("v.CaseError",true);
        		this.showToast('Error','error','Please provide a Title IVE eligibility.');     
            }
        
            else if(isIVE==true){
               if(component.get("v.TitleIVE")!=undefined || component.get("v.TitleIVE")!=''){
                   if(component.get("v.TitleIVE")=='1'){
                        component.set("v.ShowSpinner",false);
                       component.set("v.CaseError",true);
                        this.showToast('Error','error','You cannot use category of type IV-E if its associated case title IV-E eligibility field has no value.');
                   }          
                }
                else if(component.get("v.OasisClinetId")==undefined || component.get("v.OasisClinetId")==''){
                   component.set("v.ShowSpinner",false);
                    component.set("v.CaseError",true);
                    this.showToast('Error','error','Please provide Oasis Client Id.');     
                }
                    
            }
           
            else if(isCSA==true){
                console.log('**Enterd in iscsa');
                if(component.get("v.DSM")==undefined || component.get("v.DSM")==''){
                   component.set("v.ShowSpinner",false);
                    component.set("v.CaseError",true);
                    this.showToast('Error','error','Please provide DSM.');     
                }
                else if(component.get("v.Medical")==undefined || component.get("v.Medical")==''){
                    console.log('***entered in medical');
                   component.set("v.ShowSpinner",false);
                    component.set("v.CaseError",true);
                    this.showToast('Error','error','Please provide Medical.');     
                }
                else if(component.get("v.Referal")==undefined || component.get("v.Referal")==''){
                   component.set("v.ShowSpinner",false);
                    component.set("v.CaseError",true);
                    this.showToast('Error','error','Please provide Referal.');     
                }
               else if(component.get("v.Clinical")==undefined || component.get("v.Clinical")==''){
                   component.set("v.ShowSpinner",false);
                   component.set("v.CaseError",true);
                    this.showToast('Error','error','Please provide Clinical.');     
                }
               else if(component.get("v.Autism")==undefined || component.get("v.Autism")==''){
                   component.set("v.ShowSpinner",false);
                   component.set("v.CaseError",true);
                    this.showToast('Error','error','Please provide Autism.');     
                }
                    
            }
           
        
        } 
    },
    updateCase : function(component,event,helper){
        var caseid = component.get("v.caseId");
        var casewrap ={};
        casewrap.contactFirstName = component.get("v.ConFirstName");
        casewrap.contactLastName = component.get("v.ConLastName");
        casewrap.DOB=component.get("v.DOB");
        casewrap.HispanicEthnicity=component.get("v.HispanicEthnicity");
        casewrap.Gender = component.get("v.Gender");
        casewrap.Race = component.get("v.Race");
        casewrap.OasisClinetId=component.get("v.OasisClinetId");
        casewrap.DSM=component.get("v.DSM");
        casewrap.Medical = component.get("v.Medical");
        casewrap.Referal = component.get("v.Referal");
        casewrap.Clinical=component.get("v.Clinical");
        casewrap.Autism=component.get("v.Autism");
        casewrap.titleIVE=component.get("v.TitleIVE");
     	 var action = component.get("c.UpdateCase"); 
        	action.setParams({ caseId : caseid.toString(),
                              caseWrapper : JSON.stringify(casewrap)});
         	action.setCallback(this,function(response){           
            var state = response.getState();
            console.log('**State val of case update'+state);
            if(state==="SUCCESS"){
                 var result=response.getReturnValue(); 
                 console.log(JSON.stringify(result));
                if(result!=null && result.caserec!=null){
                    component.set("v.CaseValue",result.caserec);
                    component.set("v.ShowSpinner",false);                  
                    this.showToast('Success','Success','Case record has been updated.');  
                    component.set("v.showCSACase",false);
                    component.set("v.isIVECase",false);
                    component.set("v.IsCSACase",false);
                } 
                else if(result!=null && result.message=='Case contact doesnt exist'){
                	component.set("v.CaseValue",'');
                    component.set("v.ShowSpinner",false);                  
                    this.showToast('Error','Error','First associate a contact with the case');  
                    component.set("v.showCSACase",false);
                    component.set("v.isIVECase",false);
                    component.set("v.IsCSACase",false);
                }
                else {
                    component.set("v.CaseValue",'');
                    component.set("v.ShowSpinner",false);                  
                    this.showToast('Error','Error','Failed to update the case record.');  
                   
                }    
            }  
              else if(state==="ERROR"){
              	component.set("v.CaseValue",'');
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                        component.set("v.CaseValue",'');
                    	component.set("v.ShowSpinner",false);                  
                    	this.showToast('Error','Error',errors[0].message); 
                       
                    }
                } else {
                    console.log("Unknown error");
                    component.set("v.CaseValue",'');
                    component.set("v.ShowSpinner",false);                  
                    this.showToast('Error','Error','Failed to update Case.');
                    
                    
                }
               
            }
         });
        $A.enqueueAction(action);             
    },
    updateCaseRollback : function(component,event,helper){
         component.set("v.showCSACase",false);
        component.set("v.ConFirstName",'');
        component.set("v.HispanicEthnicity",'');
        component.set("v.ConLastName",'');
        component.set("v.DOB",'');
        component.set("v.Gender",'');
        component.set("v.Race",'');
        component.set("v.OasisClinetId",'');
        component.set("v.DSM",'');
        component.set("v.Medical",'');
        component.set("v.Referal",'');
        component.set("v.Clinical",'');
        component.set("v.Autism",'');
        component.set("v.TitleIVE",'');
        component.set("v.isIVECase",false);
        component.set("v.IsCSACase",false);
    }
})