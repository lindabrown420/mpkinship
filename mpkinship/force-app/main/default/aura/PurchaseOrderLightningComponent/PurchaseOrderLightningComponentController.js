({
    doInit : function(component, event, helper) {
        console.log('**Entered in doinit');       
      
        var recordtypeId=component.get("v.recordtypeId");
        console.log('&&&&&&'+recordtypeId);
        var action = component.get("c.getRecordType");
         action.setParams({          	                
                recordTypeId : recordtypeId
        });	
        action.setCallback(this,function(response){           
            var state = response.getState();
            console.log('**State val'+state);
            if(state==="SUCCESS"){
                 var result=response.getReturnValue(); 
                 console.log('****rsult'+JSON.stringify(result));
                 if(result.DeveloperName!=null)
                   component.set("v.recordTypeName",result.DeveloperName);
                  var recordtype = component.get("v.recordTypeName");
                if(recordtype==='Purchase_Order'){
                   window.location = '/lightning/n/Create_POSO';
                   /* component.set("v.isCA",false);
                    component.set("v.ispo",true);  
                    helper.getInitialValues(component,event,helper); */
                }
                else if(recordtype==='Case_Actions'){
                	console.log('**Entered here in caseaciton part');
                    window.location = '/lightning/n/Create_Case_Action';
                    /*component.set("v.isCA",true);
                    component.set("v.ispo",false); */ 
                }

                else if(recordtype==='Staff_Operations'){
                    window.location = '/lightning/n/New_Invoice';
                    
                }
                else if(recordtype==='Report_of_Collection'){
                    window.location = '/lightning/n/Create_Report_of_Collections';
                    
                }
                
            }
         });
        $A.enqueueAction(action);  
      
    },
     caseChange : function(component,event,helper){
    	var caseid = component.find("Caseauraid").get("v.value");
        console.log(caseid);
        component.set("v.caseId",caseid);
        console.log(caseid);
        if(caseid!=null && caseid!='' && caseid!=undefined){
            helper.getCaseValue(component,event,helper,caseid);
        }
        else
            component.set("v.CaseValue",'');
    },
    CampaingdataChanged: function(component,event,helper){
         var vendorid = component.get("v.vendorId");
         component.set("v.campaignId",'');
        component.set("v.POselectedServices",[]);
        component.set("v.IsSave",false);
        component.set("v.APbeginDate",'');
        component.set("v.APEndDate",'');
        component.set("v.pricebookentries",[]);
        component.set("v.Pricebookfound",false);
        component.set("v.ShowAddServices",false);
        component.set("v.pricebookandentries",[]);
         component.set("v.Ispricebookmessage",false);
        component.set("v.IsPricebookWithoutEntry",false);
        component.set("v.PricebookIdWithoutEntry",'');
        var changedcampval = event.getParam("dataChange");       
        console.log('9999'+JSON.stringify(changedcampval));
         if(changedcampval!='' && changedcampval!=undefined && changedcampval.Id!=undefined){
         	component.set("v.campaignId",changedcampval.Id); 
              helper.fetchBeginDate(component,event,helper);
         }
        var campaignId = component.get("v.campaignId");
        //CHECK FOR PRICEBOOK IF CAMPAIGNID IS NOT NULL AND VENDORID IS ALSO NOT NULL
        if(campaignId!='' && campaignId!=undefined && vendorid!='' && vendorid!=undefined){
            console.log('**Entered in bring pricebook here in campaign');  
            helper.fetchPricebook(component,event,helper);
        }
     },     
    CampaignChange : function(component,event,helper){        
        console.log('**Entered here');
        // var campaignId = event.getSource().get('v.value');
        var campaignId = component.find("campid").get("v.value");
        component.set("v.campaignId",campaignId);        
        var vendorid = component.get("v.vendorId");
        component.set("v.POselectedServices",[]);
        component.set("v.IsSave",false);
        component.set("v.APbeginDate",'');
        component.set("v.APEndDate",'');
        component.set("v.pricebookentries",[]);
        component.set("v.Pricebookfound",false);
        component.set("v.ShowAddServices",false);
        component.set("v.pricebookandentries",[]);
        component.set("v.IsPricebookWithoutEntry",false);
        component.set("v.PricebookIdWithoutEntry",'');
        //CHANGE THE BEGIN DATE FIRST
        helper.fetchBeginDate(component,event,helper);
        //CHECK FOR PRICEBOOK IF CAMPAIGNID IS NOT NULL AND VENDORID IS ALSO NOT NULL
        if(campaignId!='' && campaignId!=undefined && vendorid!='' && vendorid!=undefined){
            console.log('**Entered in bring pricebook here in campaign');  
            helper.fetchPricebook(component,event,helper);
        }
        
    },
    VendorChange : function(component,event,helper){
        var vendorid = event.getSource().get('v.value');       
        component.set("v.vendorId",vendorid);        
        var campaignId=component.get("v.campaignId");
        component.set("v.POselectedServices",[]);
        component.set("v.pricebookentries",[]);
         component.set("v.IsSave",false);
        component.set("v.ShowAddServices",false);
        component.set("v.pricebookandentries",[]);
         component.set("v.Ispricebookmessage",false);
        component.set("v.Pricebookfound",false);
        component.set("v.IsPricebookWithoutEntry",false);
        component.set("v.PricebookIdWithoutEntry",'');
        //CHECK FOR PRICEBOOK IF CAMPAIGN AND VENDOR ID BOTH ARE NOT NULL
        if(vendorid!='' && vendorid!=undefined && campaignId!='' && campaignId!=undefined){
            console.log('**Entered in bring pricebook here in vendor');
            helper.fetchPricebook(component,event,helper);            
        } 
    },
    cancelpo :function(component,event,helper){
        helper.cancelForm(component,event,helper);
    },
    handleCancel:function(component,event,helper){
          helper.cancelForm(component,event,helper);
    },
    SelectedServices : function(component,event,helper){
     	var selectedServices = event.getParam("SelectedServices");
        console.log('**Entered in parent');
        console.log(JSON.stringify(selectedServices));
        component.set("v.POselectedServices",selectedServices);
        var ShowSave = event.getParam("IsShow");
        console.log(ShowSave);  
        console.log('**length of selectedservices'+ selectedServices.length);
        if(ShowSave==false && selectedServices!=undefined && selectedServices.length>0){
            component.set("v.IsSave",true);
            //CALCULATE TOTAL AMOUNT
            helper.CalculatePOSOTotal(component,event,helper,selectedServices);
        }    
        else
            component.set("v.IsSave",false);
    },
    CallSave :function(component,event,helper){
        component.set("v.Save",'Save');
        component.set("v.POError",false);
        component.set("v.ShowSpinner",true);
         component.set("v.IsCSAAdminProfile",false);
    	helper.validateForm(component,event,helper);  
        console.log('**VAl of poerror'+component.get("v.POError"));
       /* if(component.get("v.POError")==false){
            helper.savePO(component,event,helper,component.get("v.profileName"),false);
        } */
    },
    CallSaveAndSubmitforAproval : function(component,event,helper){
        component.set("v.Save",'Not Save');
        component.set("v.POError",false);
        component.set("v.ShowSpinner",true);
         component.set("v.IsCSAAdminProfile",false);
    	helper.validateForm(component,event,helper);  
       /*  if(component.get("v.POError")==false){
            helper.savePO(component,event,helper,component.get("v.profileName"),true);
        } */
    },
    CallSaveApproved : function(component,event,helper){
    	 component.set("v.Save",'Save');
        component.set("v.IsCSAAdminProfile",true);
        component.set("v.POError",false);
        component.set("v.ShowSpinner",true);
    	helper.validateForm(component,event,helper);      
    },
    update : function(component,event,helper){
        $A.get('e.force:refreshView').fire();
    },
    handleSendEmail : function(component,event,helper){
        var sendEmailvalue=component.find("sendEmailId").get("v.value");
        console.log('*******send email val'+sendEmailvalue);
        component.set("v.SendEmail",sendEmailvalue);
    },
    closeCaseModel : function(component,event,helper){
        component.set("v.showCSACase",false);
        component.set("v.ConFirstName",'');
        component.set("v.HispanicEthnicity",'');
        component.set("v.ConLastName",'');
        component.set("v.DOB",'');
        component.set("v.Gender",'');
        component.set("v.Race",'');
        component.set("v.OasisClinetId",'');
        component.set("v.DSM",'');
        component.set("v.Medical",'');
        component.set("v.Referal",'');
        component.set("v.Clinical",'');
        component.set("v.Autism",'');
        component.set("v.TitleIVE",'');
        component.set("v.isIVECase",false);
        component.set("v.IsCSACase",false);
     	
    },
    SaveUpdatedCase :function(component,event,helper){
        component.set("v.ShowSpinner",true);
        component.set("v.CaseError",false);
        helper.checkCaseRequiredFields(component,event,helper);
        if(component.get("v.CaseError")==false){
            console.log('**enterd here in caseerror no');
            helper.updateCase(component,event,helper);
        } 
    }
})