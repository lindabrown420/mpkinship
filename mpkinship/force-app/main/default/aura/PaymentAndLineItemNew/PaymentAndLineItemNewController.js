({
    doInit : function(component, event, helper) {        
        helper.getInoviceLineItem(component,event,helper);
        
    },
    getBeginDate:function(component,event,helper){
        console.log('inside begin date'+JSON.stringify(component.get("v.paymentandlineitmesClone")));
        component.set("v.ShowSpinner",true);
        var rowIndex =event.getSource().get("v.name");
        console.log("Row No : " + rowIndex); 
        var BeginDate = event.getSource().get("v.value");
        console.log('***begin date'+BeginDate);
        var monthBeginDate ='';
        if(BeginDate!=''){
            var splitbeginDate = BeginDate.split('-');
            monthBeginDate=splitbeginDate[0]+'-'+splitbeginDate[1];
            console.log('**monthBeginddate'+monthBeginDate);
        }
        var payitem=component.get("v.paymentandlineitems");
        var enddate;
        var showerror=false;
        var monthdigit='';
        var initialBeginDate='';
        for(var i=0; i<payitem.length ; i++){
            for(var j=0 ; j<payitem[i].PaymentRec.length ; j++){
                if(payitem[i].PaymentRec[j].Id==rowIndex){
                    monthdigit=payitem[i].MonthDigit;
                    enddate=payitem[i].PaymentRec[j].Service_End_Date__c;
                    break;
                }
            }
        }
        var payItemAlready = component.get("v.paymentandlineitmesClone");
        //console.log('clone items'+JSON.stringify(payItemAlready));
        for(var i=0; i<payItemAlready.length ; i++){
            for(var j=0 ; j<payItemAlready[i].PaymentRec.length ; j++){
                if(payItemAlready[i].PaymentRec[j].Id==rowIndex){
                    console.log('***clone paymentrec'+JSON.stringify(payItemAlready[i].PaymentRec[j]));
                    console.log('***clone'+payItemAlready[i].PaymentRec[j].Service_Begin_Date__c);
                    initialBeginDate=payItemAlready[i].PaymentRec[j].Service_Begin_Date__c;
                    break;
                }
            }
        }
        if(BeginDate >enddate){
            console.log('**b >E'+initialBeginDate);
            component.set("v.ShowSpinner",false);
             for(var i=0; i<payitem.length ; i++){
                for(var j=0 ; j<payitem[i].PaymentRec.length ; j++){
                    if(payitem[i].PaymentRec[j].Id==rowIndex){
                        console.log('**error part begin date val'+initialBeginDate);
                        payitem[i].PaymentRec[j].Service_Begin_Date__c=initialBeginDate;
                        break;
                    }
                }
        	}
            component.set("v.paymentandlineitems",payitem);
            showerror=true;
        	  helper.showToast('Error','Error','Service Begin Date should not be greater than the Service End Date.');    
        }
        else if(BeginDate < component.get("v.POcloseDate") && rowIndex==component.get("v.FirstInvoiceId")){        	 
             showerror=true;
            for(var i=0; i<payitem.length ; i++){
                for(var j=0 ; j<payitem[i].PaymentRec.length ; j++){
                    if(payitem[i].PaymentRec[j].Id==rowIndex){
                        console.log('**error part begin date val'+initialBeginDate);
                        payitem[i].PaymentRec[j].Service_Begin_Date__c=initialBeginDate;
                        break;
                    }
                }
        	}
            component.set("v.paymentandlineitems",payitem);

            component.set("v.ShowSpinner",false);
        	  helper.showToast('Error','Error','Service Begin Date should not be less than the Purchase Order Begin Date.');   
        }
        else if(monthBeginDate!=monthdigit){
        	 component.set("v.ShowSpinner",false);
            for(var i=0; i<payitem.length ; i++){
                for(var j=0 ; j<payitem[i].PaymentRec.length ; j++){
                    if(payitem[i].PaymentRec[j].Id==rowIndex){
                        console.log('**error part begin date val'+initialBeginDate);
                        payitem[i].PaymentRec[j].Service_Begin_Date__c=initialBeginDate;
                        break;
                    }
                }
        	}
             showerror=true;
        	  helper.showToast('Error','Error','Service Begin Date should fall under Payment month.');   
        }
        if(showerror==false){
            console.log('**Entered in change date ');
        	helper.changeDate(component,event,helper,rowIndex,BeginDate,'true');
        }    
       
    },
    getEndDate : function(component,event,helper){
        component.set("v.ShowSpinner",true);
    	var rowIndex =event.getSource().get("v.name");
        console.log("Row No : " + rowIndex); 
        var EndDate = event.getSource().get("v.value");
        console.log('***end date'+EndDate); 
        var monthEndDate ='';
        if(EndDate!=''){
            var splitendDate = EndDate.split('-');
            monthEndDate=splitendDate[0]+'-'+splitendDate[1];
            console.log('**monthEndddate'+monthEndDate);
        }
        var payitem=component.get("v.paymentandlineitems");
        var begindate;
        var showerror=false;
        var monthdigit='';
        var initialFinalDate='';
        for(var i=0; i<payitem.length ; i++){
            for(var j=0 ; j<payitem[i].PaymentRec.length ; j++){
                if(payitem[i].PaymentRec[j].Id==rowIndex){
                    console.log(payitem[i].MonthDigit);
                    monthdigit=payitem[i].MonthDigit;
                    begindate=payitem[i].PaymentRec[j].Service_Begin_Date__c;
                    break;
                }
            }
        }
         var payItemAlready = component.get("v.paymentandlineitmesClone");
        //console.log('clone items'+JSON.stringify(payItemAlready));
        for(var i=0; i<payItemAlready.length ; i++){
            for(var j=0 ; j<payItemAlready[i].PaymentRec.length ; j++){
                if(payItemAlready[i].PaymentRec[j].Id==rowIndex){
                    console.log('***clone paymentrec'+JSON.stringify(payItemAlready[i].PaymentRec[j]));
                    initialFinalDate=payItemAlready[i].PaymentRec[j].Service_End_Date__c;
                    break;
                }
            }
        }
        if(begindate >EndDate){
            component.set("v.ShowSpinner",false);
            for(var i=0; i<payitem.length ; i++){
                for(var j=0 ; j<payitem[i].PaymentRec.length ; j++){
                    if(payitem[i].PaymentRec[j].Id==rowIndex){
                        payitem[i].PaymentRec[j].Service_End_Date__c=initialFinalDate;
                        break;
                    }
                }
        	}
            component.set("v.paymentandlineitems",payitem);
            showerror=true;
        	  helper.showToast('Error','Error','Service Begin date should not be greater than Service End date.');    
        }
        else if(monthEndDate!=monthdigit){
        	 component.set("v.ShowSpinner",false);
            for(var i=0; i<payitem.length ; i++){
                for(var j=0 ; j<payitem[i].PaymentRec.length ; j++){
                    if(payitem[i].PaymentRec[j].Id==rowIndex){
                        payitem[i].PaymentRec[j].Service_End_Date__c=initialFinalDate;
                        break;
                    }
                }
        	}
            component.set("v.paymentandlineitems",payitem);
             showerror=true;
        	  helper.showToast('Error','Error','Service End date should fall under Payment month.');   
        }
        else if(EndDate > component.get("v.POEndDate") && rowIndex==component.get("v.LastInvoiceId")){        	 
            showerror=true;
            component.set("v.ShowSpinner",false);
            for(var i=0; i<payitem.length ; i++){
                for(var j=0 ; j<payitem[i].PaymentRec.length ; j++){
                    if(payitem[i].PaymentRec[j].Id==rowIndex){
                        payitem[i].PaymentRec[j].Service_End_Date__c=initialFinalDate;
                        break;
                    }
                }
        	}
            component.set("v.paymentandlineitems",payitem);
        	helper.showToast('Error','Error','Service End date should not be greater than Purchase Order End date.');   
        }
        if(showerror==false)  
        	helper.changeDate(component,event,helper,rowIndex,EndDate,'false');
    },
    handleFocus : function(component,event,helper){
       var rowIndex =event.getSource().get("v.name");  
       var InvoiceNumber = event.getSource().get("v.value");
        console.log('***invoicenumber'+ InvoiceNumber);
    },
    getInvoiceNumber : function(component,event,helper){
    	console.log('**Entered here@@');        
        var rowIndex =event.getSource().get("v.name");
        console.log('**VAl of rowIndex'+rowIndex);
        var InvoiceNumber = event.getSource().get("v.value");
        console.log('***begin date'+InvoiceNumber);
        if(InvoiceNumber!=''){
            console.log('**Entered in not null invoice number');
            component.set("v.ShowSpinner",true);
        	helper.updateInvoiceNumber(component,event,helper,rowIndex,InvoiceNumber);   
        }
    },
     UpdateDescription :function(component,event,helper){
    	 var rowIndex =event.getSource().get("v.name");
         var Description = event.getSource().get("v.value");
        console.log('**Vl of description'+Description);
        if(Description!=''){
        	component.set("v.ShowSpinner",true);
            helper.updateInvoiceDescription(component,event,helper,rowIndex,Description); 
        }
    },
   QuantityChange: function(component,event,helper){
        //THE DRAFT VALUE IS THE CHANGES IN THE CURRENT CELL
        console.log('**Enterd in cell change');
        var draftValues = event.getParam('draftValues');
        console.log('**VAl of draftValues'+JSON.stringify(draftValues));
        /**BLOCK FOR QUANTITY CHANGE STARTS HERE **/
        if(draftValues !=undefined && draftValues.length>0 && draftValues[0].Quantity_Billed__c!=undefined && draftValues[0].Quantity_Billed__c < 0){
            helper.displayError(component,event,helper,draftValues[0].Id,'Quantity Billed should not be negative.');    
        }
        if(draftValues !=undefined && draftValues.length >0 && draftValues[0].Quantity_Billed__c!=undefined && draftValues[0].Quantity_Billed__c >= 0){
           	console.log('**Enterd in line 207 of quantity billed sction');
            //GET ALL THE PAYMENT AND ITS ITEMS
            var paymentandlineitems = component.get("v.paymentandlineitems");
            //ITERATE ALL THE PAYMENT
            var currentPayment;
            var currentLineItem;
            for(var i=0 ; i< paymentandlineitems.length ;i++){
                for(var k=0; k< paymentandlineitems[i].PaymentRec.length; k++){
                    var lineItems=paymentandlineitems[i].PaymentRec[k].Invoice_Line_Items__r;
                    for(var j=0 ; j < lineItems.length ; j++){ 
                        //CHECK IF THIS IS THE CURRENT ITEM
                        if(lineItems[j].Id == draftValues[0].Id){
                            console.log('**Found'); 
                            currentLineItem=lineItems[j];
                            currentPayment=paymentandlineitems[i];                       
                        }
                    }
                }    
            }
            if(currentLineItem!=undefined){
                //MAX QUANTITY NEEDED
                var maxAuthorized =currentLineItem.Max_Quantity_Authorized__c;
                //PREVIOUSLY BILLED QUANTITY
                var previouslyBilled =currentLineItem.Previously_Billed__c!=undefined?currentLineItem.Previously_Billed__c:0;
                //THE REMAILING QUANITY NEEDED
                var remainingBilled = maxAuthorized -previouslyBilled;
                //OLD QUANITY BUILD
                var billedQuantity =currentLineItem.Quantity_Billed__c!=undefined?currentLineItem.Quantity_Billed__c:0;
                console.log('billedquantity new'+ billedQuantity);
                var changedQuanity = draftValues[0].Quantity_Billed__c - billedQuantity;
                console.log('**quantity billed from lineitem'+draftValues[0].Quantity_Billed__c);
                console.log('**Changedqunatity'+changedQuanity);
                //INCREASED QUANITY IS MORE THAN NEEDED THEN SHOW ERROR
                if(changedQuanity > remainingBilled){
                    
                    helper.displayError(component,event,helper,draftValues[0].Id,'Quantity Billed should not be greater than maximum authorized quantity.');
                }
                else if(changedQuanity <= remainingBilled){
                    helper.removeError(component,event,helper,draftValues[0].Id);
                    var remainingamount = (remainingBilled - changedQuanity)*currentLineItem.Unit_Price__c;
                    helper.calculateRemainingAmount(component,event,helper,currentLineItem,remainingamount); 
                    currentLineItem.Remaining_Quantity__c= remainingBilled - changedQuanity;
                    //ADDED ON 17TH AUG 
                    console.log('*VAl of unitprice'+ currentLineItem.Unit_Price__c);
                    console.log('**Val of changedquantity'+ changedQuanity);
                    /*var amountbilled = changedQuanity * currentLineItem.Unit_Price__c;
                    if(amountbilled.toString().startsWith('-'))
                        amountbilled = amountbilled.toString().slice(1); */
                    currentLineItem.LineItem_Amount_Billed__c= currentLineItem.Unit_Price__c * draftValues[0].Quantity_Billed__c;
                   
                }   
            }
        } 
        /** BLOCK FOR QUANTITY CHANGE ENDS HERE ***/
        /**BLOCK FOR AMOUNT BILLED CHANGE STARTS HERE **/
        if(draftValues!=undefined && draftValues.length > 0 && draftValues[0].LineItem_Amount_Billed__c!=undefined && draftValues[0].LineItem_Amount_Billed__c <= 0){
         	console.log('**Entered in else of amount billed');
            helper.displayErrorForAmountBilled(component,event,helper,draftValues[0].Id,'Amount Billed should be greater than zero.');    
        }
        if(draftValues!=undefined && draftValues.length >0 && draftValues[0].LineItem_Amount_Billed__c!=undefined && draftValues[0].LineItem_Amount_Billed__c > 0){
        	 console.log('**Entered in lineitemamount billed section'+ draftValues[0].LineItem_Amount_Billed__c);
            //GET ALL THE PAYMENT AND ITS ITEMS
            var paymentandlineitems = component.get("v.paymentandlineitems");
            //ITERATE ALL THE PAYMENT
            var currentPayment;
            var currentLineItem;
            for(var i=0 ; i< paymentandlineitems.length ;i++){
                for(var k=0; k< paymentandlineitems[i].PaymentRec.length; k++){
                    var lineItems=paymentandlineitems[i].PaymentRec[k].Invoice_Line_Items__r;
                    for(var j=0 ; j < lineItems.length ; j++){ 
                        //CHECK IF THIS IS THE CURRENT ITEM
                        if(lineItems[j].Id == draftValues[0].Id){
                            console.log('**Found'); 
                            currentLineItem=lineItems[j];
                            currentPayment=paymentandlineitems[i];                       
                        }
                    }
                }    
            } //FOR LOOP ENDS HERE 
            if(currentLineItem!=undefined){
                console.log('**enterd in currentlineitem amountbilled'+JSON.stringify(currentLineItem));
                //CHECK FOR AMOUNT BILLED VALUE
                currentLineItem.LineItem_Amount_Billed__c=draftValues[0].LineItem_Amount_Billed__c;
                //CALCULATE QUANTITY BILLED
                var quantitybilledcal = (currentLineItem.LineItem_Amount_Billed__c /  currentLineItem.Unit_Price__c);
                console.log('**Quanitytbilledcal'+quantitybilledcal);              
                quantitybilledcal = quantitybilledcal.toString().slice(0,(quantitybilledcal.toString().indexOf("."))+3);
               // quantitybilledcal = quantitybilledcal.toFixed(3).slice(0,-1);
                /*if(quantitybilledcal.toString().indexOf(".")==1){
                    var myTruncatedNumber = quantitybilledcal.toString().subString(0,quantitybilledcal.indexOf('.')+3);                   
                    quantitybilledcal=myTruncatedNumber;
                } */   
                //MAX QUANTITY NEEDED
                var maxAuthorized =currentLineItem.Max_Quantity_Authorized__c;
                //PREVIOUSLY BILLED QUANTITY
                var previouslyBilled =currentLineItem.Previously_Billed__c!=undefined?currentLineItem.Previously_Billed__c:0;
                //THE REMAILING QUANITY NEEDED
                var remainingBilled = maxAuthorized -previouslyBilled;
                //OLD QUANITY BUILD
                var billedQuantity =currentLineItem.quantitybilled!=undefined?currentLineItem.quantitybilled:0;
                console.log('**Val of billedquantity'+ billedQuantity);
                var changedQuanity = quantitybilledcal - billedQuantity;
				console.log('**Changedquantity val'+ changedQuanity);
                console.log('**remaining billed'+remainingBilled);
                //INCREASED QUANITY IS MORE THAN NEEDED THEN SHOW ERROR
                if(changedQuanity > remainingBilled){
                    helper.displayErrorForAmountBilled(component,event,helper,draftValues[0].Id,'Amount Billed should not exceed the maximum amount authorized.');   
                    //helper.displayError(component,event,helper,draftValues[0].Id,'Quantity Billed should not be greater than maximum authorized quantity.');
                }
               
                else if(changedQuanity <= remainingBilled){
                    console.log('**entered here in last else');
                    helper.removeError(component,event,helper,draftValues[0].Id);
                    var remainingamount = (remainingBilled - changedQuanity)*currentLineItem.Unit_Price__c;
                    helper.calculateRemainingAmount(component,event,helper,currentLineItem,remainingamount); 
                    currentLineItem.Remaining_Quantity__c= remainingBilled - changedQuanity;
                    //ADDED ON 17TH AUG 
                    console.log('*VAl of unitprice'+ currentLineItem.Unit_Price__c);
                    console.log('**Val of changedquantity'+ changedQuanity);
                     currentLineItem.Quantity_Billed__c= quantitybilledcal;
                                    	
                }   
            }
        }    
        /**BLOCK FOR AMOUNT BILLED CHANGE ENDS HERE **/
    },
    handleSave : function(component,event,helper){
        
        console.log('**Entedred in save');
        component.set("v.ShowSpinner",true);
        var draftValues = event.getParam('draftValues');
        console.log('***VAl of draft'+JSON.stringify(draftValues));
       // console.log('**vak if id'+draftValues[0].Id);
       // var payentlist = component.get("v.paymentandlineitems");
        console.log('**VAl of error'+JSON.stringify(component.get("v.errors")));
        var error = component.get("v.errors");
        if(error==null || error==undefined || (error!=null && error!=undefined && error.rows==undefined || (
            error.rows!=undefined && Object.keys(error.rows).length==0))){
            var quanititiesupdatedlist = [];
            console.log('draftvalues length'+draftValues.length);
            if(draftValues.length >0){                
                for(var i =0 ; i< draftValues.length ;i++){
                    console.log('****'+ draftValues[i].Quantity_Billed__c);
                    if(draftValues[i].Quantity_Billed__c >= 0){
                        console.log('**enterd in quanity billed greaterer than 0');
                        quanititiesupdatedlist.push(draftValues[i]);    
                    }
                    else if(draftValues[i].LineItem_Amount_Billed__c>0){
                        quanititiesupdatedlist.push(draftValues[i]);  
                    }
                }
            } // if of drafts ends here  
            console.log('**Val of quanititiesupdatedlist'+JSON.stringify(quanititiesupdatedlist));
            //NEED TO FIND OUT ACCORDION ID
            if(quanititiesupdatedlist.length >0){
                console.log('***list val'+quanititiesupdatedlist);  
                //SEND TO APEX
                helper.sendForSave(component,event,helper,quanititiesupdatedlist);                
            }
            else{
                component.set("v.ShowSpinner",false);
                console.log('*** length not greater than 0');  
            }
        }
        else{
            component.set("v.ShowSpinner",false);
        }
        
    },
    handleSectionToggle: function(cmp,event,helper) {	
        console.log('***handleSectionToggle toggle');
        
        cmp.set("v.reset",true);
        cmp.set("v.reset",false);
        var error= { rows: {}, table: {} };
        cmp.set("v.errors",error);
        var result=cmp.get("v.paymentandlineitems");
        console.log('***Valss'+JSON.stringify(result));
       
            for(var i=0; i<result.length ;i++){
                for(var k=0; k < result[i].PaymentRec.length; k++){
                    for(var j=0;j<result[i].PaymentRec[k].Invoice_Line_Items__r.length;j++){
                        result[i].PaymentRec[k].Invoice_Line_Items__r[j].Remaining_Quantity__c=result[i].PaymentRec[k].Invoice_Line_Items__r[j].remainingbackup;
                        result[i].PaymentRec[k].Invoice_Line_Items__r[j].Remaining_Amount__c = result[i].PaymentRec[k].Invoice_Line_Items__r[j].remainingamountbackup;
                    }
                }    
            }
        
    },
    handleCancel : function(component,event,helper){
        component.set("v.clickCancel",true);
        console.log("***EVENT LOCK"+event);	
        console.log('***Evnet stirngify'+JSON.stringify(event));
        var result=component.get("v.paymentandlineitems");
        console.log(JSON.stringify(result));
        for(var i=0; i<result.length ;i++){
            for(var k=0; k<result[i].PaymentRec.length; k++){
                for(var j=0;j<result[i].PaymentRec[k].Invoice_Line_Items__r.length;j++){
                    result[i].PaymentRec[k].Invoice_Line_Items__r[j].Remaining_Quantity__c=result[i].PaymentRec[k].Invoice_Line_Items__r[j].remainingbackup;
                    result[i].PaymentRec[k].Invoice_Line_Items__r[j].Remaining_Amount__c = result[i].PaymentRec[k].Invoice_Line_Items__r[j].remainingamountbackup;
                }
            }    
        }
        console.log(JSON.stringify(result));  
    },
    WriteOffPayment : function(component,event,helper){
        component.set("v.openModal",true);
        var paymentId =event.getSource().get("v.name"); 
        console.log('**paymentid'+paymentId);
        component.set("v.SelectedWriteOffPaymentId",paymentId);
    },
    closeModel : function(component,event,helper){
        component.set("v.openModal",false);
        component.set("v.SelectedWriteOffPaymentId",'');
    },
    save : function(component,event,helper){
         component.set("v.ShowSpinner",true);
        var paymentid = component.get("v.SelectedWriteOffPaymentId");
    	helper.writeOffPayment(component,event,helper,paymentid);    
    },
     // KIN 1064
    openPayrecord : function (component,event,helper){
        var objId = event.target.id;
        console.log('######objId'+objId);
        window.open('/'+objId);
    },
    openAccordion : function(component,event,helper){
        component.set("v.domelement",'');
        component.set("v.buttonId",'');
        var buttonElement = event.currentTarget; 
        console.log('**VAl of buttonelement '+ buttonElement);
        var buttonId = buttonElement.getAttribute("id");
        console.log('**buttonId'+buttonId);
        component.set("v.buttonId",buttonId);
        var parentOfButton = buttonElement.parentNode;
        console.log('**val of parent h2'+parentOfButton);
        var parentParentOfButton = parentOfButton.parentNode;
        console.log('**div element'+parentParentOfButton);
        var SectionParent = parentParentOfButton.parentNode;
        console.log('**one more parent'+ SectionParent.nodeName );
        //ITS CORRECT NODENAME GIVING ME SECTION.
        
        var childNodeval = SectionParent.hasChildNodes();
        console.log(childNodeval); // IT IS GIVING TRUE
        let children = SectionParent.childNodes;
        console.log(children);        
        for (let i = 0; i < children.length; i++) {
        	console.log(children[i].nodeName);    
        }  
        var secondChildren = children[1];
        component.set("v.domelement",buttonElement);
        console.log('**accodion section div'+ secondChildren);
        console.log('**accordion section div node name'+ secondChildren.nodeName);
        /**GET ALL CHILD OF SectionParent **/
        var childrecords = document.getElementsByClassName("accordionsections");
        console.log('***childrecords'+childrecords);
        for (var i = 0; i < childrecords.length; i++){            
            $A.util.removeClass(childrecords[i],"slds-show");
            $A.util.addClass(childrecords[i],"slds-hide");           
    	}
        console.log('**buttonelement'+buttonElement);
        console.log('contains hide class'+secondChildren.classList.contains("slds-hide"));
        $A.util.removeClass(secondChildren,"slds-hide");
        $A.util.addClass(secondChildren,"slds-show");
       
    },
    onRender : function(component,event,helper){
         var buttonId = component.get("v.buttonId");
        console.log('**VAl of butonid'+buttonId);
        var domelement = document.getElementById(buttonId);
        if(domelement!=undefined){
            var parentOfButton = domelement.parentNode;
            console.log('**val of parent h2'+parentOfButton);
            var parentParentOfButton = parentOfButton.parentNode;
            console.log('**div element'+parentParentOfButton);
            var SectionParent = parentParentOfButton.parentNode;
            console.log('**one more parent'+ SectionParent.nodeName );
            //ITS CORRECT NODENAME GIVING ME SECTION.
            
            var childNodeval = SectionParent.hasChildNodes();
            console.log(childNodeval); // IT IS GIVING TRUE
            let children = SectionParent.childNodes;
            console.log(children);        
            for (let i = 0; i < children.length; i++) {
                console.log(children[i].nodeName);    
            }  
            var secondChildren = children[1];
            console.log('**********************');
            console.log(secondChildren);
            if(secondChildren!=undefined && secondChildren.classList!=undefined){
                secondChildren.classList.remove("slds-hide");
                secondChildren.classList.add("slds-show");
                console.log('contains show class'+secondChildren.classList.contains("slds-show"));
            }     
        }
    },
    ResetInvoice :function(component,event,helper){
        component.set("v.ShowSpinner",true);
    	var paymentId =event.getSource().get("v.name"); 
        helper.resetPayment(component,event,helper,paymentId);
    }
})