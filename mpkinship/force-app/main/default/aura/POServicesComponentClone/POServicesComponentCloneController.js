({
    doInit : function(component, event, helper) {
        console.log('**Entered in po doinit');
        console.log(JSON.stringify(component.get("v.setSelectedservices")));
        var pricebookandEntries = component.get("v.pricebookentriesfromParent");
        if(pricebookandEntries!='' && pricebookandEntries!=undefined){
            component.set('v.pricebookentries',pricebookandEntries);            
        }    
       //component.set("v.setSelectedservices",[]);
       console.log('**Val of setselectedservices'+component.get("v.setSelectedservices").length);
        //FETCH UNIT MEASURE PICKLIST
       helper.fetchUnitMeasure(component,event,helper);
    },
     //KIN 1311
    opennewNote : function (component, event, helper) {
        var indexval = event.getSource().get("v.name"); 
        console.log('**opennote'+indexval);
        component.set("v.noteindex",indexval);
        var pricebookentries =component.get("v.pricebookentriesfromParent");
        console.log('pricebookentries'+JSON.stringify(pricebookentries));
        if(pricebookentries!=undefined && indexval!=undefined){
            var selectedRecord = pricebookentries[indexval];
            console.log('selectedRecord:'+JSON.stringify(selectedRecord));
            if(selectedRecord!='' && selectedRecord!=undefined){
                if(selectedRecord.Product2Id !='' && selectedRecord.Product2Id!=undefined){
                	component.set("v.serviceRecordid",selectedRecord.Product2Id);
                }
                else if(selectedRecord.serviceProduct !='' && selectedRecord.serviceProduct!=undefined){
                    component.set("v.serviceRecordid",selectedRecord.serviceProduct.product.Id);
                }
                    else{
                        console.log('new service'+ JSON.stringify(selectedRecord.serviceProduct));
                        component.set("v.serviceRecordid","");
                        
                    }
            }
        }
        var cmpTarget = component.find('NoteModalbox');
		var cmpBack = component.find('NoteModalbackdrop');
		$A.util.addClass(cmpTarget, 'slds-fade-in-open');
		$A.util.addClass(cmpBack, 'slds-backdrop--open');
    },
    closeNoteModal : function(component,event,helper){
        component.set("v.note",{'sobjectType': 'ContentNote','Title': '','Content': ''});
        component.set("v.serviceRecordid","");
        var cmpTarget = component.find('NoteModalbox');
		var cmpBack = component.find('NoteModalbackdrop');
		$A.util.removeClass(cmpBack,'slds-backdrop--open');
		$A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
    },
    createNote : function(component,event,helper){
        var newNote = component.get("v.note");
        console.log('newNote'+JSON.stringify(newNote));
        console.log('serviceRecordid:'+component.get("v.serviceRecordid"));
        if(component.get("v.serviceRecordid")!='' && component.get("v.serviceRecordid") !=undefined && component.get("v.note")!=null && component.get("v.note")!=undefined && component.get("v.note").Title != ""){
            console.log('note value sending'+JSON.stringify(component.get("v.note")));
            var action = component.get("c.createNoteRecord");
            action.setParams({
                nt : newNote,
                prntId : component.get("v.serviceRecordid")
            });
            action.setCallback(this,function(a){
                //get the response state
                var state = a.getState();
                //check if result is successfull
                if(state == "SUCCESS"){
                    //Reset Form
                    var not = {'sobjectType': 'ContentNote',
                                        'Title': '',
                                        'Content': ''
                                       };
                    //resetting the Values in the form
                    component.set("v.note",not);
                    component.set("v.serviceRecordid",'');
                    var noteindx=component.get("v.noteindex");
                    component.find("btnaddnote")[noteindx].set("v.iconName","utility:check");
                    //console.log('add note index='+noteindx);
                    
                    var cmpTarget = component.find('NoteModalbox');
                    var cmpBack = component.find('NoteModalbackdrop');
                    $A.util.removeClass(cmpBack,'slds-backdrop--open');
                    $A.util.removeClass(cmpTarget, 'slds-fade-in-open');
                } else if(state == "ERROR"){
                    console.log('Error in calling server side action');
                }
            });
            //adds the server-side action to the queue        
            $A.enqueueAction(action);
        }
        else if(component.get("v.note").Title == "" || component.get("v.note").Title ==null) {
            helper.showToast('Error', 'Error', 'Please Fill Title of Note');
                
            }
        if ((component.get("v.serviceRecordid")=='' || component.get("v.serviceRecordid") ==undefined) && component.get("v.note")!=null && component.get("v.note")!=undefined && component.get("v.note").Title != ""){
                console.log('adding new service note');
                var indx= component.get("v.noteindex");
                var pricebookentries =component.get("v.pricebookentriesfromParent");
                console.log('pricebookentries'+JSON.stringify(pricebookentries));
                if(pricebookentries!=undefined && indx!=undefined){
                    var selectedRecord = pricebookentries[indx];
                    console.log('selectedRecord:'+JSON.stringify(selectedRecord));
                    if(selectedRecord!='' && selectedRecord!=undefined){
                        console.log(newNote.Title + newNote.Content);
                        selectedRecord.noteTitle=newNote.Title;
                        selectedRecord.noteContent=newNote.Content;
                        component.set("v.pricebookentriesfromParent",pricebookentries);
                        console.log('pricebookentriesfromParentafternote'+component.get("v.pricebookentriesfromParent"));
                        //Reset Form
                        var not = {'sobjectType': 'ContentNote',
                                            'Title': '',
                                            'Content': ''
                                           };
                        //resetting the Values in the form
                        component.set("v.note",not);
                        var cmpTarget = component.find('NoteModalbox');
                        var cmpBack = component.find('NoteModalbackdrop');
                        $A.util.removeClass(cmpBack,'slds-backdrop--open');
                        $A.util.removeClass(cmpTarget, 'slds-fade-in-open');
                    }
                }
            }
        
    },
    // ends KIN 1311
    //KIN 228
    doAction : function(component, event) {
        var params = event.getParam('arguments');
        if (params) {
            var param1 = params.param1;
            console.log('param1'+param1);
        var action1= component.get("c.getCaseAge");
        	action1.setParams({
            	caseId: param1
        	});	   
            action1.setCallback(this, function(response){
            	var state = response.getState();
            	console.log('**VAl of state'+state);
            	console.log('***response'+response.getReturnValue());
                if (state === "SUCCESS") { 
                    var caseval = response.getReturnValue();
                     console.log('***case'+caseval);
                    console.log('***case'+caseval.Age__c);
                    if(caseval!=null){
                    	component.set("v.selectedCaseAge",caseval.Age__c);
                    }
                }
            });
        
        if(param1){
        $A.enqueueAction(action1);
        }
        
        }
    },
    //ends
    
    handleCancel : function(component,event,helper){
        component.set('v.selectedRowsCount',0);
        component.set('v.selectedRows','');
        component.set('v.selectedServices','');
    },
    handleNext : function(component,event,helper){
        console.log('**entered in selected next');
        console.log(component.get("v.selectedServices"));
        component.set("v.IsShowFirstTable",false);
    },
 
    cloneRow : function(component,event,helper){
        console.log('***enterd in clonerow');       
        var indexval = event.getSource().get("v.name");   
        // var indexval = event.currentTarget.getAttribute("data-value");     
        console.log('****'+indexval);
        var pricebookentries =component.get("v.pricebookentriesfromParent");
        var entries =[];
        var addrow ={};
        if(pricebookentries!=undefined && pricebookentries.length>0){
            for(var i =0; i< pricebookentries.length;i++){
                entries.push(pricebookentries[i]);
                 if(indexval!=undefined && indexval ==i){
                    console.log('**Enterd here');
                    console.log(pricebookentries[i].Service_Description__c);
                    addrow.Service_Name__c= pricebookentries[i].Service_Name__c;
                    if(pricebookentries[i].Service_Description__c!=undefined)
                        addrow.Service_Description__c =pricebookentries[i].Service_Description__c;
                    if(pricebookentries[i].Service_Unit_Measure__c!=undefined)
                        addrow.Service_Unit_Measure__c =pricebookentries[i].Service_Unit_Measure__c;
                    if(pricebookentries[i].Unit_Measure_Label__c!=undefined)
                        addrow.Unit_Measure_Label__c =pricebookentries[i].Unit_Measure_Label__c;
                    addrow.UnitPrice= pricebookentries[i].UnitPrice;
                    if(pricebookentries[i].Product2Id!=undefined)
                        addrow.Product2Id =pricebookentries[i].Product2Id;
                    if(pricebookentries[i].IsActive!=undefined)
                        addrow.IsActive =pricebookentries[i].IsActive;
                    if(pricebookentries[i].Pricebook2Id!=undefined)
                        addrow.Pricebook2Id =pricebookentries[i].Pricebook2Id;
                    if(pricebookentries[i].Id!=undefined)
                        addrow.Id =pricebookentries[i].Id;
                    addrow.quantity='';
                    addrow.categorySelection='';
                    //addrow.recipient='';
                    //addrow.serviceCheck =false;
                    
                    entries.push(addrow);
                }
            }
        }
        component.set("v.pricebookentriesfromParent",entries);
    },
    //KIN 1527 confirmation
    handleConfirmDialog : function(component, event, helper) {
        var indexval = event.getSource().get("v.name"); 
        console.log('**VAl of indexval row inactive'+ indexval);
        var indexval = event.getSource().get("v.name"); 
        component.set("v.inactiveindex",indexval);
        var pricebookentries =component.get("v.pricebookentriesfromParent");
        console.log('pricebookentries'+JSON.stringify(pricebookentries));
        if(pricebookentries!=undefined && indexval!=undefined){
            var selectedRecord = pricebookentries[indexval];
            var selRecId;
            console.log('selectedRecord:'+JSON.stringify(selectedRecord));
            if(selectedRecord!='' && selectedRecord!=undefined){
                /*if(selectedRecord.Product2Id !='' && selectedRecord.Product2Id!=undefined){
                	console.log(selectedRecord.Product2Id);
                    selRecId=selectedRecord.Product2Id;
                }
                else if(selectedRecord.serviceProduct !='' && selectedRecord.serviceProduct!=undefined){
                    console.log(selectedRecord.serviceProduct);
                    selRecId=selectedRecord.serviceProduct;
                }
                    else{
                        console.log('new service'+ JSON.stringify(selectedRecord.serviceProduct));
                        
                        
                    }*/
                selRecId=selectedRecord.Id;
            }
        component.set('v.inactiveRedId', selRecId);   
        component.set('v.showConfirmDialog', true);
        }
        
    },
     
    handleConfirmDialogYes : function(component, event, helper) {
        console.log('Yes');
        component.set('v.showConfirmDialog', false);
        var selRecId= component.get('v.inactiveRedId');
        if(selRecId!='' && selRecId!=undefined){
                var action= component.get("c.makeServiceInactive");
                action.setParams({
                    recId : selRecId
                });	
                action.setCallback(this,function(response){      
                    var state = response.getState();   
            		console.log('**VAl of inactive state'+state);
                });
                  $A.enqueueAction(action);                 
            }
        
        
        
        helper.rowDeletion(component,event,helper,component.get('v.inactiveindex'));
    },
     
    handleConfirmDialogNo : function(component, event, helper) {
        console.log('No');
        component.set('v.showConfirmDialog', false);
    },
    // KIN 1527 Confirmation ends
    // KIN 1527
    makeRowInactive : function(component,event,helper){
    	 var indexval = event.getSource().get("v.name"); 
        console.log('**VAl of indexval row inactive'+ indexval);
        var indexval = event.getSource().get("v.name"); 
        component.set("v.noteindex",indexval);
        var pricebookentries =component.get("v.pricebookentriesfromParent");
        console.log('pricebookentries'+JSON.stringify(pricebookentries));
        if(pricebookentries!=undefined && indexval!=undefined){
            var selectedRecord = pricebookentries[indexval];
            var selRecId;
            console.log('selectedRecord:'+JSON.stringify(selectedRecord));
            if(selectedRecord!='' && selectedRecord!=undefined){
                if(selectedRecord.Product2Id !='' && selectedRecord.Product2Id!=undefined){
                	console.log(selectedRecord.Product2Id);
                    selRecId=selectedRecord.Product2Id;
                }
                else if(selectedRecord.serviceProduct !='' && selectedRecord.serviceProduct!=undefined){
                    console.log(selectedRecord.serviceProduct);
                    selRecId=selectedRecord.serviceProduct;
                }
                    else{
                        console.log('new service'+ JSON.stringify(selectedRecord.serviceProduct));
                        
                        
                    }
            }
            if(selRecId!='' && selRecId!=undefined){
                var action= component.get("c.makeServiceInactive");
                action.setParams({
                    recId : selRecId
                });	
                action.setCallback(this,function(response){      
                    var state = response.getState();   
            		console.log('**VAl of inactive state');
                });
                  $A.enqueueAction(action);                 
            }
        }
        
        
        helper.rowDeletion(component,event,helper,indexval);
        
    },
     deleteRow : function(component,event,helper){
    	 var indexval = event.getSource().get("v.name"); 
        console.log('**VAl of indexval'+ indexval);
        helper.rowDeletion(component,event,helper,indexval);
    },
    quantityChange :function(component,event,helper){
        console.log('**Entered in quantity change');
        var validity=false;
        var quantityvalue = event.getSource().get("v.value");
        
        var index = event.getSource().get("v.name");
        //VALIDATION CHECKS
        var count=false;
        count=helper.DuplicateNameCheck(component,event,helper,index);
        if(count==false)
        helper.checkForServiceCreationError(component,event,helper,index);
       
    },
   CategorydataChanged: function(component,event,helper){
        var validity=false;
        console.log('**Entered in datachanged');
        var changedcatval = event.getParam("dataChange");       
        console.log('9999'+JSON.stringify(changedcatval));
       //MAKE CSA VALUES NULL
       helper.CSAvaluesBlank(component,event,helper);
       component.set("v.recipientIVE",'');
        var indexRow = event.getParam("IndexRow");
       console.log('**Val of indexrOW'+indexRow);
        var pricebookentries = component.get("v.pricebookentriesfromParent");
        var changedEntry = pricebookentries[indexRow];
       console.log('**VAl of changedentry'+JSON.stringify(changedEntry));
       changedEntry.showCostCenterLookup=false;
       changedEntry.CostCenterIds=[];
       changedEntry.costcenterSelection='';
       //SHOW CSA BUTTON
       if(changedcatval!='' && changedcatval!=undefined && changedcatval.Category_Type__c!=undefined && changedcatval.Category_Type__c=='CSA'){
           changedEntry.showCSA=true;
           changedEntry.showIVE=false;
           changedEntry.recipientIVE='';
           changedEntry.Fund='';	
           changedEntry.FundIVE='';
       } 
       if(changedcatval=='' || (changedcatval!='' && changedcatval!=undefined && changedcatval.Category_Type__c!=undefined && changedcatval.Category_Type__c!='CSA' 
           && changedcatval.Category_Type__c!='IVE' && changedcatval.RecordType!=undefined && changedcatval.RecordType.DeveloperName!='LASER_Category')){
           changedEntry.showCSA = false;
           changedEntry.showIVE=false;
           changedEntry.showFund=false;
           helper.MakeCSAValuesBlank(component,event,helper,indexRow);
           changedEntry.recipientIVE='';
           changedEntry.Fund='';
       }
       if(changedcatval!='' && changedcatval!=undefined && changedcatval.Category_Type__c!=undefined && changedcatval.Category_Type__c=='IVE'){
           console.log('**Entered in ive cate');
           changedEntry.showIVE=true;
           changedEntry.showCSA = false;
            changedEntry.showFund=false;
           helper.MakeCSAValuesBlank(component,event,helper,indexRow);
           changedEntry.FundIVE='Reimbursable';
           
       } 
      if(changedcatval!='' && changedcatval!=undefined && changedcatval.Category_Type__c!=undefined && changedcatval.Category_Type__c!='IVE' && 
         changedcatval.Category_Type__c!='CSA' && changedcatval.RecordType!=undefined && changedcatval.RecordType.DeveloperName!=undefined && changedcatval.RecordType.DeveloperName=='LASER_Category'){
          changedEntry.showCSA = false;
           changedEntry.showIVE=false;
           helper.MakeCSAValuesBlank(component,event,helper,indexRow);
           changedEntry.recipientIVE=''; 
           changedEntry.showFund=true;
          changedEntry.Fund='Reimbursable';
      }
        component.set("v.pricebookentriesfromParent",pricebookentries);
        //VALIDATION CHECKS
        var count=false;
        count=helper.DuplicateNameCheck(component,event,helper,indexRow);
        if(count==false)
        	helper.checkForServiceCreationError(component,event,helper,indexRow);
       		console.log('**After chaning cat'+JSON.stringify(component.get("v.pricebookentriesfromParent")));
          if(changedcatval!=undefined && changedcatval!='' && changedcatval.Id!=undefined){
               console.log('**Entered here in catval change');         
               helper.fetchCostCenters(component,event,helper,changedcatval.Id,component.get("v.additionalCriteria"),indexRow);
          }    
       
    },
    
    openCSA : function(component,event,helper){
        //KIN 237
        
        component.set("v.showMandateDesc",false);
        component.set("v.showSPTDesc",false);
        component.set("v.showSNCDesc",false);
        //ends
        console.log('**emterd in csa');
         component.set("v.IsFund",false)
        helper.CSAvaluesBlank(component,event,helper);
        
        component.set("v.showCostCenterLookup",false);
        component.set("v.CostCenterIds",[]);
        component.set("v.costcenterSelection",'');
        
        component.set("v.IsCSA",true);
    	var indexval = event.getSource().get("v.name"); 
        component.set("v.openModal",true);
        console.log('**opencsa'+indexval);
        var pricebookentries =component.get("v.pricebookentriesfromParent");
        if(pricebookentries!=undefined && indexval!=undefined){
            var selectedRecord = pricebookentries[indexval];
            console.log('***@@'+JSON.stringify(selectedRecord));
            component.set("v.SelectedServiceCSA",selectedRecord);
            if(selectedRecord!='' && selectedRecord!=undefined){      
                selectedRecord.Index=indexval;
                
                if(selectedRecord.CostCenterIds!='' && selectedRecord.CostCenterIds!=undefined){
                     component.set("v.CostCenterIds",selectedRecord.CostCenterIds);
                      console.log('**Costcetnerids inside'+JSON.stringify(component.get("v.CostCenterIds")));
                 	//component.set("v.showCostCenterLookup",true);
                 }
                 if(selectedRecord.costcenterSelection!='' && selectedRecord.costcenterSelection!=undefined){
                     	
            		component.set("v.costcenterSelection",selectedRecord.costcenterSelection);
                     console.log('**Costcetner selection inside'+JSON.stringify(component.get("v.CostCenterIds")));
                 } 
                 else{
                    component.set("v.costcenterSelection",''); 
                 }
                component.set("v.showCostCenterLookup",true);
               /* if(selectedRecord.CostCenterIds!='' && selectedRecord.CostCenterIds!=undefined){
                     
                 	component.set("v.showCostCenterLookup",true);
                 } */
                
                //CHECK IF SELECTED RECORD HAS VALUE ALREADY
                if(selectedRecord.SPTselected!='' && selectedRecord.SPTselected!=undefined && selectedRecord.MandateSelected!=undefined
                   && selectedRecord.MandateSelected!='' && selectedRecord.SPTNameselected!='' && selectedRecord.SPTNameselected!=undefined
                  && selectedRecord.recipient!='' && selectedRecord.recipient!=undefined){
                    console.log('**Eneted in first if');
                    //PREPOPULATE THE VALUES WITH THE ALREADY SELECTED ONE
                    helper.PrepopulateCSAvalues(component,event,helper,selectedRecord);
                }
                else{
                    console.log('**enterd in else');
                    if(selectedRecord.categorySelection!=undefined){
                        var categoryval = selectedRecord.categorySelection.Id;
                        console.log(categoryval);
                        if(categoryval!=null && categoryval!='' && categoryval!=undefined){
                            helper.fetchCSAValues(component,event,helper,categoryval);   
                        }
                        else{
                            //NEED TO THINK WHAT TO DO IF CATEGORY ID IS NULL
                        }
                    }    
                }    
            }
        }    
        
    },
    openIVE :function(component,event,helper){
        console.log('**Enterd in ive');
        component.set("v.recipientIVE",false);
         component.set("v.IsFund",false);
        component.set("v.IsCSA",false);
        
        component.set("v.showCostCenterLookup",false);
        component.set("v.CostCenterIds",[]);
        component.set("v.costcenterSelection",'');
        
    	var indexval = event.getSource().get("v.name"); 
        component.set("v.openModal",true); 
         var pricebookentries =component.get("v.pricebookentriesfromParent");
        if(pricebookentries!=undefined && indexval!=undefined){
            var selectedRecord = pricebookentries[indexval];
            component.set("v.SelectedServiceCSA",selectedRecord);
             if(selectedRecord!='' && selectedRecord!=undefined){      
                selectedRecord.Index=indexval;
                 //CHECK IF SELECTED RECORD HAS VALUE ALREADY
                if(selectedRecord.recipientIVE!='' && selectedRecord.recipientIVE!=undefined ){
                    console.log('**Eneted in first if');
                    //PREPOPULATE THE VALUES WITH THE ALREADY SELECTED ONE
                     component.set("v.recipientIVE",selectedRecord.recipientIVE);
                     component.set("v.CSAError",false);
                     selectedRecord.CSAError=false;
                }
                 if(selectedRecord.FundIVE!=undefined && selectedRecord.FundIVE!=''){
                     component.set("v.FundIVE",selectedRecord.FundIVE);
                 }
                 
                 if(selectedRecord.CostCenterIds!='' && selectedRecord.CostCenterIds!=undefined){
                     component.set("v.CostCenterIds",selectedRecord.CostCenterIds);
                      console.log('**Costcetnerids inside'+JSON.stringify(component.get("v.CostCenterIds")));
                 	//component.set("v.showCostCenterLookup",true);
                 }
                 if(selectedRecord.costcenterSelection!='' && selectedRecord.costcenterSelection!=undefined){
                     	
            			component.set("v.costcenterSelection",selectedRecord.costcenterSelection);
                     console.log('**Costcetner selection inside'+JSON.stringify(component.get("v.CostCenterIds")));
                 } 
                 else{
                    component.set("v.costcenterSelection",''); 
                 }
                 component.set("v.showCostCenterLookup",true);
               /* if(selectedRecord.CostCenterIds!='' && selectedRecord.CostCenterIds!=undefined){
                     
                 	component.set("v.showCostCenterLookup",true);
                 } */
             }    
        }    
    },
    openFund : function(component,event,helper){
    	component.set("v.IsCSA",false);
        component.set("v.recipientIVE",false);
        //component.set("v.Fund","Reimbursable");
        component.set("v.IsFund",true);
        
        component.set("v.showCostCenterLookup",false);
        component.set("v.CostCenterIds",[]);
        component.set("v.costcenterSelection",''); 
        
    	var indexval = event.getSource().get("v.name"); 
        component.set("v.openModal",true); 
         var pricebookentries =component.get("v.pricebookentriesfromParent");
        if(pricebookentries!=undefined && indexval!=undefined){
            var selectedRecord = pricebookentries[indexval];
            component.set("v.SelectedServiceCSA",selectedRecord);
             if(selectedRecord!='' && selectedRecord!=undefined){      
                selectedRecord.Index=indexval;
                 //CHECK IF SELECTED RECORD HAS VALUE ALREADY
                if(selectedRecord.Fund!='' && selectedRecord.Fund!=undefined ){
                    console.log('**Eneted in first if');
                    //PREPOPULATE THE VALUES WITH THE ALREADY SELECTED ONE
                     component.set("v.Fund",selectedRecord.Fund);
                     component.set("v.CSAError",false);
                     selectedRecord.CSAError=false;
                } 
                 
                if(selectedRecord.CostCenterIds!='' && selectedRecord.CostCenterIds!=undefined){
                     component.set("v.CostCenterIds",selectedRecord.CostCenterIds);
                      console.log('**Costcetnerids inside'+JSON.stringify(component.get("v.CostCenterIds")));
                 	//component.set("v.showCostCenterLookup",true);
                 }
                 if(selectedRecord.costcenterSelection!='' && selectedRecord.costcenterSelection!=undefined){
                     	
            			component.set("v.costcenterSelection",selectedRecord.costcenterSelection);
                     console.log('**Costcetner selection inside'+JSON.stringify(component.get("v.CostCenterIds")));
                 } 
                 else{
                    component.set("v.costcenterSelection",''); 
                 }
                 component.set("v.showCostCenterLookup",true);
               /* if(selectedRecord.CostCenterIds!='' && selectedRecord.CostCenterIds!=undefined){
                     
                 	component.set("v.showCostCenterLookup",true);
                 } */
                 
             }    
        }        
    },
    CostCenterdataChange :function(component,event,helper){
     	var changedcostcenterval = event.getParam("CostCenter");       
        console.log('9999'+JSON.stringify(changedcostcenterval)); 
        component.set("v.costcenterSelection",changedcostcenterval);   
    },
    RecipientChange :function(component,event,helper){
    	var selectedRecipient = event.getParam("value");
        console.log('**REcipient value'+selectedRecipient);
       component.set("v.recipient",selectedRecipient);
    },
    RecipientIVEChange :function(component,event,helper){
       var selectedRecipient = event.getParam("value");
       console.log('**REcipient value'+selectedRecipient);
       component.set("v.recipientIVE",selectedRecipient); 
    },	
    FundChange :function(component,event,helper){
    	var fundselected = event.getParam("value");
        component.set("v.Fund",fundselected);
    },
    FundChangeIVE:function(component,event,helper){
    	var fundchange =  event.getParam("value");
        component.set("v.FundIVE",fundchange);   
    },
    handleMandateChange : function(component,event,helper){
        var selectedMandate = event.getParam("value");
        console.log('**Val of mandate'+selectedMandate);
        if(selectedMandate==''){
             component.set("v.MandateSelectedlabel",'');
            //KIN 237
        	 component.set("v.showMandateDesc",false);
        }
        else{
            var options = component.get("v.Mandateoptions");
            var selectedlabel='';
            for(var i=0; i< options.length ; i++){
                if(options[i].value==selectedMandate){
                    selectedlabel=options[i].label;
                    break;
                }
            }
            if(selectedlabel!=''){
                component.set("v.MandateSelectedlabel",selectedlabel);
                // KIN 228
                var ageofCase=component.get("v.selectedCaseAge");
                console.log('ageofCase:'+ageofCase+'selectedlabel:'+selectedlabel);
                if((selectedlabel=='Foster Care Abuse/Neglect Prevention'||selectedlabel=='Foster Care Child in Need of Services (CHINS) Prevention'||
                  selectedlabel=='Foster Care CHINS – CSA Parental Agreement'|| selectedlabel=='Non-Mandated')&& ageofCase>18){
                    component.set("v.ShowSpinner",false);
                    component.set("v.AgeError",true);
                    component.set("v.AgeError1",false);
                    component.set("v.AgeError2",false);
                    helper.showToast('Error', 'Error', 'The age of the child must be 18 years or less to use this mandate type. Please update your selection.');
                }
                else if((selectedlabel=='Foster Care Abuse/Neglect – DSS Non-Custodial Agreement'||selectedlabel=='Foster Care Abuse/Neglect – Local DSS Entrustment/Custody'||
                  selectedlabel=='Foster Care CHINS – Entrustment/Custody'|| selectedlabel=='Foster Care – Court Ordered for Truancy' || selectedlabel=='Foster Care – Court Ordered for Delinquent Behaviors')&& ageofCase>21){
                    component.set("v.ShowSpinner",false);
                    component.set("v.AgeError1",true);
                    component.set("v.AgeError",false);
                    component.set("v.AgeError2",false);
                    helper.showToast('Error', 'Error', 'The person’s age on this case must be 21 years or less to use this mandate type. Please update your selection.');
                }
                else if((selectedlabel=='Kinship Guardianship'||selectedlabel=='Wrap-Around Services for Students with Disabilities'||
                  selectedlabel=='Special Education Services in an Approved Educational Placement')&& ageofCase>=22){
                    component.set("v.ShowSpinner",false);
                    component.set("v.AgeError2",true);
                    component.set("v.AgeError1",false);
                    component.set("v.AgeError",false);
                    helper.showToast('Error', 'Error', 'The person’s age on this case must be less than 22 years to use this mandate type. Please update your selection.');
                }
                //ends
            }
            //KIN 237
             var MandatedesMap= component.get("v.Mandatedescmap");
             console.log('MandatedesMap'+MandatedesMap);
             var Mandatearr=JSON.parse(JSON.stringify(MandatedesMap));
             var selMandateDesc;
             for(var i=0;i<Mandatearr.length;i++){
                 if(Mandatearr[i]['label']==selectedMandate){
                     console.log('selectedMandate= ',Mandatearr[i]['value']);
                     selMandateDesc = Mandatearr[i]['value'];
                     break;
                 }
             }
             console.log('selMandateDesc'+selMandateDesc);
             component.set("v.Mandatedescselected",selMandateDesc);
            component.set("v.showMandateDesc",true);
            // ends
        }    
        component.set("v.MandateSelected",selectedMandate);	
    },
     handleSPTChange : function(component,event,helper){
         //KIN 237
         component.set("v.showSNCDesc",false);
        var selectedOptionValue = event.getParam("value");  
         //KIN 237
         if(selectedOptionValue==''){
             
             component.set("v.showSPTDesc",false);
         }
         //ends
        component.set("v.SPTselected",selectedOptionValue);
        //SET LABEL TOO 
        var options = component.get("v.SPToptions");
        var selectedlabel='';
        for(var i=0; i< options.length ; i++){
            if(options[i].value==selectedOptionValue){
                selectedlabel=options[i].label;
                break;
            }
        }
        if(selectedlabel!='')
            component.set("v.SPTselectedlabel",selectedlabel);
         if(selectedOptionValue=='' || selectedOptionValue==undefined){
           component.set("v.SPTselectedlabel",'');            
                var items = [];
                var item = {                           
                        "label": "---None---",
                        "value": ''
                };
                items.push(item);
                component.set("v.SPTNames",items);  
                component.set("v.SPTNameselected",'');
                component.set("v.SPTNameselectedlabel",'');
           
        }
         else if(selectedOptionValue!='' && selectedOptionValue!=undefined){
              component.set("v.SPTNameselected",'');
              component.set("v.SPTNameselectedlabel",'');
              var items = [];
              var item = {                           
                        "label": "---None---",
                        "value": ''
                };
               items.push(item);
              component.set("v.SPTNames",items);
             //KIN 237
             var SPTdesMap= component.get("v.SPTdescmap");
             console.log('SPTdesMap'+SPTdesMap);
             var arr=JSON.parse(JSON.stringify(SPTdesMap));
             var selDesc;
             for(var i=0;i<arr.length;i++){
                 if(arr[i]['label']==selectedOptionValue){
                     console.log('selectedOptionValue= ',arr[i]['value']);
                     selDesc = arr[i]['value'];
                     break;
                 }
             }
             console.log('selDesc'+selDesc);
             component.set("v.SPTdescselected",selDesc);
             //ends
        	 helper.getSPTNamesforSPT(component,event,helper,selectedOptionValue);
             //KIN 237
             component.set("v.showSPTDesc",true);
        } // if of selectedoption ends here
    },
     handleSeviceNameChange : function(component,event,helper){
        var selectedOptionValue = event.getParam("value");
        console.log('**Selectedoptionval'+selectedOptionValue);
        if(selectedOptionValue=='')
             component.set("v.SPTNameselectedlabel",'');
        else{
            var options = component.get("v.SPTNames");
            var selectedlabel='';
            for(var i=0; i< options.length ; i++){
                if(options[i].value==selectedOptionValue){
                    selectedlabel=options[i].label;
                    break;
                }
            }
            if(selectedlabel!='')
                component.set("v.SPTNameselectedlabel",selectedlabel);
        }    
    	component.set("v.SPTNameselected",selectedOptionValue);	 
        //KIN 237
         helper.getSNCDesc(component,event,helper,selectedOptionValue);
    },
    SaveCSA : function(component,event,helper){
        component.set("v.ShowSpinner",true);
        if(component.get("v.IsCSA")==true){
            helper.checkCSASave(component,event,helper);
            //KIN 228
            if(component.get("v.AgeError")==true){
                helper.showToast('Error', 'Error', 'The age of the child must be 18 years or less to use this mandate type. Please update your selection.');
            }
            if(component.get("v.AgeError1")==true){
                helper.showToast('Error', 'Error', 'The person’s age on this case must be 21 years or less to use this mandate type. Please update your selection.');
            }
            if(component.get("v.AgeError2")==true){
                helper.showToast('Error', 'Error', 'The person’s age on this case must be less than 22 years to use this mandate type. Please update your selection.');
            }
            //ends
            //KIN 228 added age condition in if
            if(component.get("v.CSAError")==false && component.get("v.AgeError")==false && component.get("v.AgeError1")==false && component.get("v.AgeError2")==false){
                //ADD IT TO THE LIST
                var selectedrecord = component.get("v.SelectedServiceCSA");
                console.log('***selected record val in save csa');
                console.log(JSON.stringify(selectedrecord));
                selectedrecord.IsFund=false;
                selectedrecord.SPTselected=component.get("v.SPTselected");
                selectedrecord.MandateSelected=component.get("v.MandateSelected");
                selectedrecord.SPTNameselected=component.get("v.SPTNameselected");
                selectedrecord.SPToptions=component.get("v.SPToptions");
                selectedrecord.SPTNames=component.get("v.SPTNames");
                selectedrecord.Mandateoptions=component.get("v.Mandateoptions");
                selectedrecord.Description=component.get("v.Description");
                selectedrecord.SPTselectedlabel=component.get("v.SPTselectedlabel");
                selectedrecord.SPTNameselectedlabel=component.get("v.SPTNameselectedlabel");
                selectedrecord.MandateSelectedlabel=component.get("v.MandateSelectedlabel");
                selectedrecord.recipient=component.get("v.recipient");
                selectedrecord.CSAError=component.get("v.CSAError");
                if(component.get("v.costcenterSelection")!=undefined && component.get("v.costcenterSelection")!='')
                    selectedrecord.costcenterSelection=component.get("v.costcenterSelection");
                else 
                    selectedrecord.costcenterSelection='';
                // selectedcsa=selectedrecord;
                var entries = component.get("v.pricebookentriesfromParent");
                console.log(JSON.stringify(entries));
                console.log('**index val'+selectedrecord.Index);
                //if(selectedrecord.Index!=null && selectedrecord.Index!=''){
                    console.log('eneterd here');
                    var indexval =selectedrecord.Index;
                    console.log('&&&&&&&&'+indexval);
                    entries[indexval]=selectedrecord;
                    console.log(JSON.stringify(entries[indexval]));
                    component.set("v.pricebookentriesfromParent",entries);
                    component.set("v.ShowSpinner",false);
                    component.set("v.openModal",false);  
             
            }  
             else{
                    component.set("v.ShowSpinner",false); 
             }
        } 
         //FOR FUND
        else if(component.get("v.IsFund")==true){
            helper.checkFundSave(component,event,helper);
            if(component.get("v.CSAError")==false){
        		var selectedrecord = component.get("v.SelectedServiceCSA");
                selectedrecord.Fund=component.get("v.Fund");
            	selectedrecord.IsFund=true;
                selectedrecord.CSAError=component.get("v.CSAError");
            	 if(component.get("v.costcenterSelection")!=undefined && component.get("v.costcenterSelection")!='')
                    selectedrecord.costcenterSelection=component.get("v.costcenterSelection");
            	else
                    selectedrecord.costcenterSelection='';
                var entries = component.get("v.pricebookentriesfromParent");
                console.log(JSON.stringify(entries));
                console.log('**index val'+selectedrecord.Index);
                //if(selectedrecord.Index!=null && selectedrecord.Index!=''){
                    console.log('eneterd here fund');
                    var indexval =selectedrecord.Index;
                    console.log('&&&&&&&& fund'+indexval);
                    entries[indexval]=selectedrecord;
                    console.log(JSON.stringify(entries[indexval]));
                    component.set("v.pricebookentriesfromParent",entries);
                    component.set("v.ShowSpinner",false);
                    component.set("v.openModal",false); 
              } 
             else{
                    component.set("v.ShowSpinner",false); 
             }
        }
        //FOR IVE
        else {
        	helper.checkIVESave(component,event,helper); 
            if(component.get("v.CSAError")==false){
            	var selectedrecord = component.get("v.SelectedServiceCSA");
                 selectedrecord.recipientIVE=component.get("v.recipientIVE");
                selectedrecord.FundIVE = component.get("v.FundIVE");
                console.log('**selectedrecord funnd'+ component.get("v.FundIVE"));
                selectedrecord.IsFund=false;
                selectedrecord.CSAError=component.get("v.CSAError");
                if(component.get("v.costcenterSelection")!=undefined && component.get("v.costcenterSelection")!='')
                    selectedrecord.costcenterSelection=component.get("v.costcenterSelection");
                else
                    selectedrecord.costcenterSelection='';
                 var entries = component.get("v.pricebookentriesfromParent");
                console.log(JSON.stringify(entries));
                console.log('**index val'+selectedrecord.Index);
                //if(selectedrecord.Index!=null && selectedrecord.Index!=''){
                    console.log('eneterd here');
                    var indexval =selectedrecord.Index;
                    console.log('&&&&&&&&'+indexval);
                    entries[indexval]=selectedrecord;
                    console.log(JSON.stringify(entries[indexval]));
                    component.set("v.pricebookentriesfromParent",entries);
                    component.set("v.ShowSpinner",false);
                    component.set("v.openModal",false);  
             
            }  
             else{
                    component.set("v.ShowSpinner",false); 
             }
                
               
        }
        var validity=false;       
        var pricebookentries = component.get("v.pricebookentriesfromParent");
        if(selectedrecord!=undefined && selectedrecord.Index!=undefined){
             var indexval =selectedrecord.Index;
             entries[indexval]=selectedrecord;
             component.set("v.pricebookentriesfromParent",entries);
            //VALIDATIONS  
             var count=false;
            count=helper.DuplicateNameCheck(component,event,helper,indexval);
            if(count==false)     
        	helper.checkForServiceCreationError(component,event,helper,indexval);
        } 
        
    },
    closeModel : function(component,event,helper){
        component.set("v.openModal",false);
        helper.CSAvaluesBlank(component,event,helper);
    },
    addRow : function(component,event,helper){
    	 var addrow ={};
         addrow.Service_Name__c='';
         addrow.Service_Description__c='';
         addrow.UnitPrice='';
         addrow.Service_Unit_Measure__c ='';         
         addrow.quantity='';
         addrow.categorySelection='';
         addrow.showCSA=false;
         addrow.showIVE=false;
         //addrow.recipient='';
         addrow.IsNewService=true;
        var pricebookentries =component.get("v.pricebookentriesfromParent");
        console.log('**Total length till now before adding a row'+ pricebookentries.length);
        pricebookentries.push(addrow);
        component.set("v.pricebookentriesfromParent",pricebookentries);
        console.log('**After adding one row'+ component.get("v.pricebookentriesfromParent").length);
        
    },
    ProductNameChange :function(component,event,helper){
      var changedServiceNameval = event.getParam("dataChange");  
        console.log('***changedServiceNameval value'+JSON.stringify(changedServiceNameval));
       var changedText =  event.getParam("TextChange");  
        console.log('**Changetext'+changedText);
        var indexRow = event.getParam("IndexRow");
        var pricebookentries = component.get("v.pricebookentriesfromParent");
        var changedEntry = pricebookentries[indexRow];
        console.log('**VAl of changedentry'+JSON.stringify(changedEntry));
        if(changedEntry!=undefined && changedServiceNameval!='' ){
            changedEntry.IsReadOnly=true;
            changedEntry.Service_Name__c = changedServiceNameval.product.Name;
            if(changedServiceNameval!=undefined && changedServiceNameval.product!=undefined &&
               changedServiceNameval.product.Description!=undefined)
            	changedEntry.Service_Description__c=changedServiceNameval.product.Description ;
            if(changedServiceNameval!=undefined && changedServiceNameval.UnitPrice!=undefined)
         		changedEntry.UnitPrice=changedServiceNameval.UnitPrice;
            if(changedServiceNameval!=undefined && changedServiceNameval.product!=undefined &&
              changedServiceNameval.product.Unit_Measure__c!=undefined)
         		changedEntry.Service_Unit_Measure__c =changedServiceNameval.product.Unit_Measure__c;
        }
        else if(changedEntry!=undefined && changedText!='' && changedText!=undefined){
         	 changedEntry.Service_Name__c = changedText;
            changedEntry.IsReadOnly=false;
            changedEntry.Service_Description__c='';
            changedEntry.UnitPrice='';
            changedEntry.Service_Unit_Measure__c='';
        }
        else if (changedServiceNameval=='' && changedEntry!=undefined  ){
            changedEntry.IsReadOnly=false;
        	changedEntry.Service_Description__c='';
            changedEntry.UnitPrice='';
            changedEntry.Service_Unit_Measure__c='';
        }
       // component.set("v.pricebookentriesfromParent",pricebookentries);
        var validity=false;
        //CHECK FOR DUPLICATE SERVICE NAME
        if((changedEntry.Service_Name__c!=undefined || changedEntry.Service_Name__c!='') && 
          changedEntry.IsNewService!=undefined && changedEntry.IsNewService==true){
             console.log('**Entered in 2nd');
        	 var entries = component.get("v.pricebookentriesfromParent");
            
            for(var i=0 ; i <entries.length ; i++){
                if(i!=indexRow && entries[i]!=undefined && entries[i].Service_Name__c==changedEntry.Service_Name__c){
                	changedEntry.error=true;
                    changedEntry.message='This service Name already exist.'; 
                    validity=true;
                   helper.fireSaveEventOnUnCheck(component,event,helper,indexRow,validity,true); 
                    break;
                }    
            }
           
        } 
        component.set("v.pricebookentriesfromParent",pricebookentries);
        //VALIDATION CHECKS
        if(validity==false)
        	helper.checkForServiceCreationError(component,event,helper,indexRow);
    },
    serviceDescChange :function(component,event,helper){
    	var description = event.getSource().get("v.value");   
        var indexRow = event.getSource().get("v.name"); 
         //VALIDATION CHECKS
        var count=false;
        count=helper.DuplicateNameCheck(component,event,helper,indexRow);
        if(count==false)
        helper.checkForServiceCreationError(component,event,helper,indexRow);
    },   
    ServicePriceChange : function(component,event,helper){
    	var price = event.getSource().get("v.value");   
        var indexRow = event.getSource().get("v.name"); 
         //VALIDATION CHECKS
        var count=false;
        count=helper.DuplicateNameCheck(component,event,helper,indexRow);
        if(count==false)
        helper.checkForServiceCreationError(component,event,helper,indexRow);    
    },
    UnitMeasureChange :function(component,event,helper){
    	var measureval = event.getSource().get("v.value");   
        var indexRow = event.getSource().get("v.name"); 
         //VALIDATION CHECKS
         var count=false;
        count=helper.DuplicateNameCheck(component,event,helper,indexRow);
        if(count==false)
        	helper.checkForServiceCreationError(component,event,helper,indexRow);       
    },
    updatelocationChange:function(component,event,helper){
        $A.get('e.force:refreshView').fire(); 
    }
    
    
})