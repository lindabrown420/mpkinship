({
	handleLoad : function(component, event, helper) {
		var  caid=component.get("v.recordId");
        console.log('caid:'+caid);
        var eD=component.find("endDate").get("v.value");
        console.log('eD:'+eD);
        component.set("v.endDateInitial",eD);
	},
    onstRsnchange  : function(component, event, helper) {
        var stReason=component.find("statusreason").get("v.value");
        console.log('stReason:'+stReason);
        if(stReason==''){
        let button = component.find('ButtonId');
    					button.set('v.disabled',true);
            var toastEvent   = $A.get("e.force:showToast");
        
        			toastEvent.setParams({
                        mode: 'dismissible',
                        message: 'Please Select Status Reason',
                        type: 'Warning'
         			});
         			toastEvent.fire();
        }
        else{
            let button = component.find('ButtonId');
    					button.set('v.disabled',false);
        }
    },
    
    onEndDatechange : function(component, event, helper) {
    	var newEndDate=component.find("endDate").get("v.value");
        console.log('newEndDate:'+newEndDate);
        var oldEndDate=component.get("v.endDateInitial");
        if(newEndDate>oldEndDate){
            //component.find("endDate").set("v.value",oldEndDate);
            var toastEvent   = $A.get("e.force:showToast");
        
        			toastEvent.setParams({
                        mode: 'dismissible',
                        message: 'To terminate this case action, enter a date earlier than the current end date',
                        type: 'Warning'
         			});
         			toastEvent.fire();
            let button = component.find('ButtonId');
    					button.set('v.disabled',true);		
            
        }
        if(newEndDate<=oldEndDate){
            let button = component.find('ButtonId');
    					button.set('v.disabled',false);
            
        }
        
	},
    handleReset : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },
    handleSubmit : function(component, event, helper) {
        var newED=component.find("endDate").get("v.value");
        var oldEndDate=component.get("v.endDateInitial");
        var stReason=component.find("statusreason").get("v.value");
        var today = new Date().toISOString().slice(0, 10);
        console.log('stReason:'+stReason);
         console.log('newED:'+newED);
         console.log('oldEndDate:'+oldEndDate);
        console.log('today'+today);
        if(stReason=='' || stReason==null){
        
            var toastEvent   = $A.get("e.force:showToast");
        
        			toastEvent.setParams({
                        mode: 'dismissible',
                        message: 'Please Select Status Reason',
                        type: 'Warning'
         			});
         			toastEvent.fire();
        }
        else if(newED>oldEndDate){
            var toastEvent   = $A.get("e.force:showToast");
        
        			toastEvent.setParams({
                        mode: 'dismissible',
                        message: 'To terminate this case action, enter a date earlier than the current end date',
                        type: 'Warning'
         			});
         			toastEvent.fire();
            
            
        }
            else{
        console.log('submit');
        var bD=component.find("beginDate").get("v.value");
        console.log('beginDate'+bD);
        if(newED<bD){
                var toastEvent   = $A.get("e.force:showToast");
        
        			toastEvent.setParams({
                        mode: 'dismissible',
                        message: 'End Date should be greater than Begin date',
                        type: 'Warning'
         			});
         			toastEvent.fire();
         }
         else{
        var action=component.get("c.updatePO");
         action.setParams({          	                
             oppId : component.get("v.recordId"),
             newEndDate:newED,
             statusRsn:stReason,
             typeOfPO :'Case Action'
            	
        });	
        action.setCallback(this,function(response){           
            var state = response.getState();
            console.log('**State val'+state);
            if(state==="SUCCESS"){
               /* var toastEvent   = $A.get("e.force:showToast");
        
        		toastEvent.setParams({
                        mode: 'dismissible',
                        message: 'Updated Successfully',
                        type: 'success'
         		});
         		toastEvent.fire();*/
             //if(newED<=oldEndDate && newED!=today){
              var result=response.getReturnValue(); 
              if(result.msg=='Record updated'){  
                var toastEvent   = $A.get("e.force:showToast");
        
        			toastEvent.setParams({
                        mode: 'dismissible',
                        message: ' Case Action will be successfully closed on the provided end date',
                        type: 'success'
         			});
         			toastEvent.fire();
                
            }
            /*if(newED== today){
                var toastEvent   = $A.get("e.force:showToast");
        
        			toastEvent.setParams({
                        mode: 'dismissible',
                        message: ' Case Action has been successfully closed',
                        type: 'success'
         			});
         			toastEvent.fire();
                
            } */
                var navEvt = $A.get("e.force:navigateToSObject");
    				navEvt.setParams({
      				"recordId": component.get("v.recordId"),
      				"slideDevName": "related"
    				});
   					navEvt.fire();
            }
            else if(state==="ERROR"){
                    component.set("v.ShowSpinner",false);
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                     errors[0].message);
                            helper.showToast('Error','error',errors[0].message);  
                            $A.get("e.force:closeQuickAction").fire();
                        }
                    } else {
                        console.log("Unknown error");
                	}
                }    
        });
         $A.enqueueAction(action);
            }
    }
    }
  
})