({
    setColumns:function (cmp, event, helper){
        // KIN - 1064 change in line no 5(Invoice Line Item Name)
        cmp.set('v.columns', [
            {label: 'Invoice Line Item Name', fieldName: 'linkName', type: 'url',typeAttributes: {label: { fieldName: 'Name' }, target: '_blank'}, editable: false},
            {label: 'Product', fieldName: 'Product__c', type: 'text', editable: false},
            {label: 'Unit Price', fieldName: 'Unit_Price__c', type: 'currency', editable: false },
            {label: 'Unit Measure', fieldName: 'Unit_Measure__c', type: 'text', editable: false },
            {label: 'Max Quantity Authorized', fieldName: 'Max_Quantity_Authorized__c', type: 'number', editable: false },
            {label: 'Monthly Units', fieldName: 'Max_Hours_Authorized__c', type: 'number', editable: false },
            {label: 'Quantity Billed', fieldName: 'Quantity_Billed__c', type: 'number',typeAttributes: { 
                minimumFractionDigits: '2'
            }, editable: true },
            {label: 'Amount Billed', fieldName: 'LineItem_Amount_Billed__c', type: 'currency', editable :true},
            /* {label: 'Previously Quantity Billed', fieldName: 'Previously_Billed__c', type: 'number', editable: false }, */
            {label: 'Remaining Amount',fieldName: 'Remaining_Amount__c',type:'currency',editable: false},
            {label: 'Remaining Quantity',fieldName: 'Remaining_Quantity__c',type:'number',editable: false},
            {label : 'Total Quantity Billed', fieldName : 'Previously_Billed__c',type:'number',editable:false}
        ]); 
        // KIN - 1064 change in line no 22(Invoice Line Item Name)
        cmp.set('v.nonEditableColumn', [
            {label: 'Invoice Line Item Name', fieldName: 'linkName', type: 'url',typeAttributes: {label: { fieldName: 'Name' }, target: '_blank'}, editable: false},
            {label: 'Product', fieldName: 'Product__c', type: 'text', editable: false},
            {label: 'Unit Price', fieldName: 'Unit_Price__c', type: 'currency', editable: false },
            {label: 'Unit Measure', fieldName: 'Unit_Measure__c', type: 'text', editable: false },
            {label: 'Max Quantity Authorized', fieldName: 'Max_Quantity_Authorized__c', type: 'number', editable: false },
            {label: 'Monthly Units', fieldName: 'Max_Hours_Authorized__c', type: 'number', editable: false },
            {label: 'Quantity Billed', fieldName: 'Quantity_Billed__c', type: 'number', editable: false },
            {label: 'Amount Billed', fieldName: 'LineItem_Amount_Billed__c', type: 'currency', editable :false},
            /* {label: 'Previously Quantity Billed', fieldName: 'Previously_Billed__c', type: 'number', editable: false }, */
            {label: 'Remaining Amount',fieldName: 'Remaining_Amount__c',type:'currency',editable: false},
            {label: 'Remaining Quantity',fieldName: 'Remaining_Quantity__c',type:'number',editable: false},
            {label : 'Total Quantity Billed', fieldName : 'Previously_Billed__c',type:'number',editable:false}
        ]); 
        console.log('******'+typeof  cmp.get('v.columns'));
    },
    
    displayError: function (cmp, event, helper,draftval,messages) {
        //GET THE ERROR VAR
        var errors = cmp.get("v.errors");  
        //IF IT IS UNDEFINED OR NULL THEN INTI
        if(errors==undefined || errors==null ){
            errors = { rows: {}, table: {} }
        }else if(errors.rows==undefined){
            errors.rows={};
        } 
        
        //GET THE ERROR ROWS
        var rowsError = errors.rows; 
        console.log(rowsError);
        //ADD ERROR FOR THE CURRENT ROW
        rowsError[draftval] = {
            Id : draftval,
            //messages: ['Quantity Billed should not be greater than maximum authorized quantity'],
            messages : [messages],
            fieldNames:  ['Quantity_Billed__c'],
            title: 'Error'
        };
        //SET THE ERROR ON THE TABLE
        errors = {
            rows: rowsError,
            table: { 
                title : "Error!!",
                messages : ['Error on Quantity Billed']
            }
        };
        cmp.set("v.errors", errors); 
    },
     displayErrorForAmountBilled: function (cmp, event, helper,draftval,messages) {
        //GET THE ERROR VAR
        var errors = cmp.get("v.errors");  
        //IF IT IS UNDEFINED OR NULL THEN INTI
        if(errors==undefined || errors==null ){
            errors = { rows: {}, table: {} }
        }else if(errors.rows==undefined){
            errors.rows={};
        } 
        
        //GET THE ERROR ROWS
        var rowsError = errors.rows; 
        console.log(rowsError);
        //ADD ERROR FOR THE CURRENT ROW
        rowsError[draftval] = {
            Id : draftval,
            //messages: ['Quantity Billed should not be greater than maximum authorized quantity'],
            messages : [messages],
            fieldNames:  ['LineItem_Amount_Billed__c'],
            title: 'Error'
        };
        //SET THE ERROR ON THE TABLE
        errors = {
            rows: rowsError,
            table: { 
                title : "Error!!",
                messages : ['Error on Amount Billed']
            }
        };
        cmp.set("v.errors", errors); 
    },
    removeError : function(cmp, event, helper,draftval){
        //GET ERROR VARIABLE
        try{
            var error = cmp.get("v.errors");           
            //IF ERROR HAVE ROW ERROR
            if(error!=undefined && error!=null && error.rows!=undefined){
                console.log('**version3**Entered in not undefined');   
                console.log(error.rows.hasOwnProperty(draftval));
                if(error.rows.hasOwnProperty(draftval)){
                    delete error.rows[draftval];                  
                    if(Object.keys(error.rows).length ==0){
                        //error={};
                        error= { rows: {}, table: {} }
                        cmp.set("v.errors",error);
                        
                    }
                }
            }
            else if(error!=undefined && error!=null){
                error= { rows: {}, table: {} }
                cmp.set("v.errors",error);    
            }
        }catch(e){ console.log(e);}        
    },
    calculateRemainingAmount : function(cmp,event,helper,paymentlineitemrec,remainingamount){
        try{
            paymentlineitemrec.Remaining_Amount__c = remainingamount;
        }catch(e){ console.log(e);}    
    },
    sendForSave : function(component,event,helper,quanititiesupdatedlist){
       
        var action= component.get("c.updatePaymentLineitems");
        action.setParams({
            paymentlineitems: JSON.stringify(quanititiesupdatedlist),
            opportunityid :component.get("v.recordId")
        });	
        action.setCallback(this, function(response){
            component.set("v.ShowSpinner",false);
            var state = response.getState();
            console.log('**State val'+state);
            console.log(response);
            if (state === "SUCCESS") {  
                component.set("v.clickCancel",true);
                var result=response.getReturnValue();
                
                if(result!=null ){
                    component.set("v.paymentandlineitems",result);    
                    console.log(component.get("v.paymentandlineitems"));
                    component.set("v.POcloseDate",result[0].PaymentRec[0].npe01__Opportunity__r.CloseDate);
                	component.set("v.POEndDate",result[0].PaymentRec[0].npe01__Opportunity__r.Service_End_Date__c);
                    component.set("v.FirstInvoiceId",result[0].FirstInvoiceId);
                   	component.set("v.LastInvoiceId",result[0].LastInvoiceId);
                    var initialList= result;
                	var payanditemlist=JSON.parse(JSON.stringify(initialList));
                	component.set("v.paymentandlineitmesClone",payanditemlist); 
                    for(var i=0; i<result.length ;i++){
                        for(var k=0; k< result[i].PaymentRec.length; k++){
                            if(result[i].PaymentRec[k].npe01__Paid__c ==false &&  result[i].PaymentRec[k].npe01__Written_Off__c == false)
                                result[i].PaymentRec[k].columns = component.get("v.columns");
                            else{                               
                                result[i].PaymentRec[k].columns = component.get("v.nonEditableColumn");
                            }  
                            //added
                            for(var j=0;j<result[i].PaymentRec[k].Payment_Line_Items__r.length;j++){                            
                                result[i].PaymentRec[k].Payment_Line_Items__r[j].remainingbackup=result[i].PaymentRec[k].Payment_Line_Items__r[j].Remaining_Quantity__c;
                                console.log('$$$$$');
                                result[i].PaymentRec[k].Payment_Line_Items__r[j].remainingamountbackup=result[i].PaymentRec[k].Payment_Line_Items__r[j].Remaining_Amount__c;
                               //ADDED ON 17TH AUG 
                                result[i].PaymentRec[k].Payment_Line_Items__r[j].quantitybilled=result[i].PaymentRec[k].Payment_Line_Items__r[j].Quantity_Billed__c;
                              // KIN - 1064
                                 result[i].PaymentRec[k].Payment_Line_Items__r[j].linkName = '/'+result[i].PaymentRec[k].Payment_Line_Items__r[j].Id;
                            } 
                        }     
                    }
                }
            }    
        });
        $A.enqueueAction(action);     
        
    },
    getInoviceLineItem : function(component,event,helper){
        var theId=component.get("v.recordId");
        if(theId==undefined){
            var myPageRef = component.get("v.pageReference");
            var id = myPageRef.state.c__id;
            if(id!=undefined && id!='')
                component.set("v.recordId", id);
        }
        
        //CALL APEX SERVER TO GET THE PAYMENT AND ITS LINE ITEM
        var action= component.get("c.PaymentLineitems");
        action.setParams({
            opportunityid: component.get("v.recordId")
        });	
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {    
                //IF SUCCESSFULLY GOT THE RESULT
                var result=response.getReturnValue();  
                console.log('**VAl of result'+result);
                if(result!=null){
                    component.set("v.paymentandlineitems",result);
                    component.set("v.POcloseDate",result[0].PaymentRec[0].npe01__Opportunity__r.CloseDate);
                    component.set("v.POEndDate",result[0].PaymentRec[0].npe01__Opportunity__r.Service_End_Date__c);
                    component.set("v.FirstInvoiceId",result[0].FirstInvoiceId);
                    component.set("v.LastInvoiceId",result[0].LastInvoiceId);
                    //Added
                    var initialList= result;
                    var payanditemlist=JSON.parse(JSON.stringify(initialList));
                    component.set("v.paymentandlineitmesClone",payanditemlist);               
                    component.set("v.IscaseAction",result[0].IscaseAction);               
                    //METHOD TO SET COLUMNS                
                    helper.setColumns(component, event, helper); 
                    for(var i=0; i<result.length ;i++){
                        for(var k=0; k < result[i].PaymentRec.length ; k++){                        
                            if(result[i].PaymentRec[k].npe01__Paid__c ==false &&  result[i].PaymentRec[k].npe01__Written_Off__c == false)
                                result[i].PaymentRec[k].columns = component.get("v.columns");
                            else{                            
                                result[i].PaymentRec[k].columns = component.get("v.nonEditableColumn");
                            }
                            //CLONE THE REMAINING
                            for(var j=0;j<result[i].PaymentRec[k].Payment_Line_Items__r.length;j++){                          
                                result[i].PaymentRec[k].Payment_Line_Items__r[j].remainingbackup=result[i].PaymentRec[k].Payment_Line_Items__r[j].Remaining_Quantity__c;                            
                                result[i].PaymentRec[k].Payment_Line_Items__r[j].remainingamountbackup=result[i].PaymentRec[k].Payment_Line_Items__r[j].Remaining_Amount__c;
                                 //ADDED ON 17TH AUG 
                                result[i].PaymentRec[k].Payment_Line_Items__r[j].quantitybilled=result[i].PaymentRec[k].Payment_Line_Items__r[j].Quantity_Billed__c;
                                // KIN - 1064
                                 result[i].PaymentRec[k].Payment_Line_Items__r[j].linkName = '/'+result[i].PaymentRec[k].Payment_Line_Items__r[j].Id;
                            }
                        }        
                    }
                    console.log(JSON.stringify(result));
                }     
            }
        });
        $A.enqueueAction(action); 
    },
    changeDate : function(component,event,helper,rowIndex,BeginDate,isbegin){
        var action=component.get("c.changePaymentDate");
        action.setParams({
            Id: rowIndex,
            Dateval :BeginDate,
            IsBeginDate : isbegin
            
        });	
        action.setCallback(this, function(response){
        	var state = response.getState();
            console.log('**State val'+state);
            console.log(response);
            if (state === "SUCCESS"){
                component.set("v.ShowSpinner",false);
                var result=response.getReturnValue(); 
                console.log(JSON.stringify(result));
                if(result.updatedVal=='updated'){
                    if(result.paymentrec!=null){
                        var paymentrec = component.get("v.paymentandlineitmesClone");
                        console.log('**paymentrec'+JSON.stringify(paymentrec));
                        for(var i=0;i<paymentrec.length ; i++){
                            for(var j=0 ; j <paymentrec[i].PaymentRec.length;j++){
                                if(paymentrec[i].PaymentRec[j].Id == result.paymentrec.Id){
                                    console.log('**Found');
                                    if(result.IsBegin==true)
                                        paymentrec[i].PaymentRec[j].Service_Begin_Date__c=result.paymentrec.Service_Begin_Date__c;  
                                    else
                                        paymentrec[i].PaymentRec[j].Service_End_Date__c=result.paymentrec.Service_End_Date__c;
                                    break;
                                }
                            }        
                        }
                        component.set("v.paymentandlineitmesClone",paymentrec);
                        console.log('**Set val'+JSON.stringify(component.get("v.paymentandlineitmesClone")));  
                    }
                 	this.showToast('Success','Success','Date Updated successfully');   
                }
                else
                	this.showToast('Error','Error','Date Updation Failed');   
                
            } 
            else{
                component.set("v.ShowSpinner",false);
                this.showToast('Error','Error','Date Updation Failed');  
            }
        });
        $A.enqueueAction(action);     
            
    },
    updateInvoiceNumber:function(component,event,helper,rowIndex,InvoiceNumber){
        console.log('***update invoice number');
    	var action = component.get("c.fetchInvoiceNumber");
        action.setParams({
            rowIndex :  rowIndex,
            InvoiceNumber : InvoiceNumber          
        });	
        action.setCallback(this, function(response){
        	var state = response.getState();
            console.log('**State val'+state);
            console.log(response);
            if (state === "SUCCESS"){
                component.set("v.ShowSpinner",false);
                var result=response.getReturnValue(); 
                console.log(JSON.stringify(result));
                if(result=='updated'){
                 	this.showToast('Success','Success','Invoice number updated successfully!');   
                }
                else
                	this.showToast('Error','Error','Invoice number update action failed.');   
                
            } 
            else{
                component.set("v.ShowSpinner",false);
                this.showToast('Error','Error','Invoice number update action failed.');  
            }
        });
        $A.enqueueAction(action);       
    },
     showToast : function(type, title, message) {       
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : title,
            message: message,
            duration:' 4000',
            key: 'info_alt',
            type: type,
            mode: 'pester'
        });
        toastEvent.fire();
    },
   writeOffPayment :function(component,event,helper,paymentid){
    	var action = component.get("c.changePaymentToWriteOff");
        action.setParams({
            recordid :  paymentid,
            opportunityid : component.get("v.recordId")
                 
        });	
        action.setCallback(this, function(response){
        	var state = response.getState();
            console.log('**State val'+state);
            console.log(response);
            if (state === "SUCCESS"){
                component.set("v.ShowSpinner",false);
                component.set("v.openModal",false);
                var result=response.getReturnValue(); 
                console.log(JSON.stringify(result));
                
               
                 if(result!=null ){
                     this.showToast('Success','Success','Payment has been write-off successfully!'); 
                    component.set("v.paymentandlineitems",result);    
                    console.log(component.get("v.paymentandlineitems"));
                    component.set("v.POcloseDate",result[0].PaymentRec[0].npe01__Opportunity__r.CloseDate);
                	component.set("v.POEndDate",result[0].PaymentRec[0].npe01__Opportunity__r.Service_End_Date__c);
                    component.set("v.FirstInvoiceId",result[0].FirstInvoiceId);
                   	component.set("v.LastInvoiceId",result[0].LastInvoiceId);
                    var initialList= result;
                	var payanditemlist=JSON.parse(JSON.stringify(initialList));
                	component.set("v.paymentandlineitmesClone",payanditemlist); 
                    for(var i=0; i<result.length ;i++){
                        for(var k=0; k< result[i].PaymentRec.length; k++){
                            if(result[i].PaymentRec[k].npe01__Paid__c ==false &&  result[i].PaymentRec[k].npe01__Written_Off__c == false)
                                result[i].PaymentRec[k].columns = component.get("v.columns");
                            else{                               
                                result[i].PaymentRec[k].columns = component.get("v.nonEditableColumn");
                            }  
                            //added
                            for(var j=0;j<result[i].PaymentRec[k].Payment_Line_Items__r.length;j++){                            
                                result[i].PaymentRec[k].Payment_Line_Items__r[j].remainingbackup=result[i].PaymentRec[k].Payment_Line_Items__r[j].Remaining_Quantity__c;
                                console.log('$$$$$');
                                result[i].PaymentRec[k].Payment_Line_Items__r[j].remainingamountbackup=result[i].PaymentRec[k].Payment_Line_Items__r[j].Remaining_Amount__c;
                               //ADDED ON 17TH AUG 
                                result[i].PaymentRec[k].Payment_Line_Items__r[j].quantitybilled=result[i].PaymentRec[k].Payment_Line_Items__r[j].Quantity_Billed__c;
                               // KIN - 1064
                               console.log('link name '+result[i].PaymentRec[k].Payment_Line_Items__r[j].Id);
                                result[i].PaymentRec[k].Payment_Line_Items__r[j].linkName = '/'+result[i].PaymentRec[k].Payment_Line_Items__r[j].Id;
                            } 
                        }     
                    }
                }
                else{
                    this.showToast('Error','Error','Payment write-off update action failed.');   
                }
            } 
            else{
                component.set("v.ShowSpinner",false);
                component.set("v.openModal",false);
                this.showToast('Error','Error','Payment write-off update action failed.');  
            }
        });
        $A.enqueueAction(action);     
    }
    
    
})