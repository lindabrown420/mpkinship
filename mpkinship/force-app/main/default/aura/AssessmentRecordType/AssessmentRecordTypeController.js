({
	doInit: function(component, event, helper) {
        console.log('doInit'); 
        
        if(component.get("v.navigateToRecord") == false){
            var ref = component.get("v.pageReference");
            var state = ref.state; 
            var context = state.inContextOfRef;
            if (context.startsWith("1\.")) {
                context = context.substring(2);
                var addressableContext = JSON.parse(window.atob(context));
                /*Object.keys(addressableContext).forEach((key) => {
                  console.log(addressableContext[key]);
                });*/
                console.log('qaq: '+addressableContext.attributes.recordId);
                console.log('addressableContext----->'+JSON.stringify(addressableContext));   // you can get your recordId and other things here.
                component.set("v.caseId",addressableContext.attributes.recordId);
            }
            console.log(component.get("v.recordId"));
            var recordTypeId = component.get("v.pageReference").state.recordTypeId;
        	console.log('recordTypeId: '+recordTypeId);
            var action = component.get("c.getRT_Name");
            action.setParams({
                rtId: recordTypeId
            })
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    console.log('rt name: '+response.getReturnValue());
                    
                    component.set("v.recordTypeName",response.getReturnValue());
                    
                    if(response.getReturnValue() != 'Title IV-E'){
                        component.set('v.showFlow',false);
                        var createRecordEvent = $A.get("e.force:createRecord");
                        createRecordEvent.setParams({
                            "entityApiName": "Assessment__c",
                            "recordTypeId": recordTypeId,
                            "navigationLocation": "LOOKUP",
                            "defaultFieldValues": {
                                'Refresh_Page__c' : true,
                                'Case__c' : component.get("v.caseId")
                            }
                        });
                        createRecordEvent.fire(); 
                    }
                    
                }
                else {
                    console.log("Failed with state: " + state);
                }
            });
            $A.enqueueAction(action);
        }
        else{
            window.open('/'+component.get("v.recId"),'_blank');
            window.open('/lightning/page/home','_self');
        }
        
	},
    
    handleSubmit: function(component, event, helper) {
        event.preventDefault();       // stop the form from submitting
        var fields = event.getParam('fields');
        console.log('fields: '+fields);
        console.log('Case__c: '+fields.Case__c);
        
        var flow = component.find("flowData");
     	var inputVariables = [
            {
                name : "recordId",
                type : "String",
                value : fields.Case__c    
            },
            {
                name : "assessmentName",
                type : "String",
                value : fields.Name    
            }
    	];
        component.set("v.showFlow",false);
        flow.startFlow("Assessment_Flow",inputVariables);
        
        //flow.startFlow("Assessment_Flow");
    },
})