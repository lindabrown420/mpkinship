({
	init : function(component, event, helper) {
        console.log(component.get('v.recordId1'));
		component.set('v.columns', [
            {label: 'Payment Number', fieldName: 'Name', type: 'text'},
            {label: 'Scheduled Date', fieldName: 'npe01__Scheduled_Date__c', type: 'date-local'},
            {label: 'Encumbered Amount', fieldName: 'Amount_Paid__c', type: 'currency', cellAttributes: { alignment: 'left' }},
            
        ]);
        var action = component.get( "c.fetchRecs" );  
            action.setParams({
            	CAid: component.get( "v.recordId1" ),
            });
            action.setCallback(this, function(response) {  
            var state = response.getState();  
            if ( state === "SUCCESS" ) {  
            	var res = response.getReturnValue();
                console.log('**Val of res'+JSON.stringify(res));
                if(res!=null && res!=undefined ){
                	component.set("v.data",response.getReturnValue());
                }
            console.log('**Val of res'+JSON.stringify(component.get("v.data")));
            }
            });
            $A.enqueueAction(action);
	},
    viewRelatedFullList : function(component, event, helper) {
		
	}
})