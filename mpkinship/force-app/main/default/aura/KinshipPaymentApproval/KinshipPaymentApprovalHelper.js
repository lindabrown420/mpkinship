({
    showToast : function(type, title, message) {
        let strTitle = title.split('\n').join('');
        let strMessage = message.split('\n').join('');
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : strTitle,
            message: strMessage,
            duration:' 4000',
            key: 'info_alt',
            type: type, // success , error, info
            mode: 'pester'//'sticky'
        });
        toastEvent.fire();
    },
    fetchData: function(component, objName, apiName, strNon, strValue){
        let action = component.get("c.getPicklistvalues");
        action.setParams({
            'objectName': objName,
            'field_apiname': apiName,
            'strNone': strNon
        });
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS"){
                let d = a.getReturnValue();
                if (d) {
                    let data = [];
                    for(let i = 0 ; i < d.length ; i++) {
                        data.push({'label' : d[i], 'value':d[i]});
                    }
                    component.set("v." + strValue, data);
                }
            } 
        });
        $A.enqueueAction(action);
    },
    getData : function(component) {
        component.set('v.Spinner', true);
        let action = component.get("c.getApprovalPaymentData");
        
        //action.setParams({});
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let data = response.getReturnValue();
                if(data) {
                    component.set("v.data", data);
                    let preCateData = [];
                    let strTemp = '';
                    let total = 0;
                    for(let i = 0 ; i < data.length ; i++) {
                        if(strTemp.indexOf(data[i].category) !== -1) {
                            for(let k = 0 ; k < preCateData.length ; k++) {
                                if(preCateData[k].cate === data[i].category) {
                                    preCateData[k].total += data[i].amount;
                                    break;
                                }
                            }
                        } else {
                            preCateData.push({'cate': data[i].category, 'total': data[i].amount});
                            strTemp += ',' + data[i].category;
                        }
                        total += data[i].amount;
                    }
                    preCateData.push({'cate': 'TOTAL', 'total': total});
                    component.set('v.preCateData', preCateData);
                }else{
                    this.showToast('error', 'Error', 'Please contact your Administrator for more details.');
                }
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                this.showToast('error', 'Error', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    },
    updatePayment : function(component, status) {
        let data = component.find('paymentrecord').getSelectedRows();
        if(data.length <= 0) {
            this.showToast('info', 'Missing Record', 'Please choose a Check record before clicking Approve/Reject button.');
            return;
        }
        let lstId = [];
        let setId = '';
        for(let i = 0 ; i < data.length ; i++){
            if(setId.indexOf(data[i].id) === -1) {
                lstId.push(data[i].id);
                setId += data[i].id + ',';
            }
        }
        
        component.set('v.Spinner', true);
        let action = component.get("c.updatePaymentStatus");
        let strNote = component.get('v.arnotes');
        
        action.setParams({
            lstId: lstId,
            status: status,
            strNote: strNote
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let data = response.getReturnValue();
                if(data) {
                    let setid = new Set();
                    for(let i = 0 ; i < data.length ; i++){
                        setid.add(data[i].Id);
                    }
                    let olddata = component.get('v.data');
                    let newdata = [];
                    for(let i = 0 ; i < olddata.length ; i++) {
                        if(!setid.has(olddata[i].id)){
                            newdata.push(olddata[i]);
                        }
                    }
                    component.set("v.data", newdata);
                    this.showToast('success', 'Success!', data.length + ' payments have been successfully ' + status);
                    component.set('v.isApproveModel', false);
                    component.set('v.isRejectModel', false);
                }else{
                    this.showToast('error', 'Error', 'Please contact your Administrator for more details.');
                }
            } else {
                const errors = response.getError();
                let strErr = '';
                if(errors) {
                    if(errors[0] && errors[0].message){
                        strErr = errors[0].message;
                    }
                }
                this.showToast('error', 'Error', strErr);
            }
            component.set('v.Spinner', false);
        });
        $A.enqueueAction(action);
    }
})