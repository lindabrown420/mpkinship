({
    init : function(component, event, helper) {
        component.set('v.columns', [
            {label: 'Payable No', fieldName: 'paymentUrl', type: 'url', typeAttributes: { 
                label: {
                    fieldName: 'paymentNo'
                },
                target: '_blank'
            }},
            {label: 'Category', fieldName: 'category', type: 'text'},
            {label: 'Vendor', fieldName: 'vendorUrl', type: 'url', typeAttributes: { 
                label: {
                    fieldName: 'vendorName'
                },
                target: '_blank'
            }},
            {label: 'Opportunity', fieldName: 'oppUrl', type: 'url', typeAttributes: { 
                label: {
                    fieldName: 'oppName'
                },
                target: '_blank'
            }},
            {label: 'Payable Schedule Date', fieldName: 'paymentDate', type: 'date-local',
                typeAttributes:{
                    month: "2-digit",
                    day: "2-digit",
                    year:"numeric"
                }
            },
            {label: 'Status', fieldName: 'status', type: 'text'},
            {label: 'Amount', fieldName: 'amount', type: 'currency', typeAttributes: { currencyCode: 'USD' }},
            {label: 'Memo', fieldName: 'memo', type: 'text'},
            {label: 'Requested By', fieldName: 'requestedby', type: 'text'}
        ]);
        helper.getData(component);
    },
    approvedPayment : function(component, event, helper) {
        helper.updatePayment(component, 'Approved')
    },
    rejectPayment : function(component, event, helper) {
        helper.updatePayment(component, 'Rejected');
    },
    closeApproveModel : function(component, event, helper) {
        component.set('v.isApproveModel', false);
    },
    openApproveModel : function(component, event, helper) {
        let data = component.find('paymentrecord').getSelectedRows();
        if(data.length <= 0) {
            helper.showToast('info', 'Missing Record', 'Please choose a Check record before clicking "Approve" button.');
            return;
        }
        component.set('v.isApproveModel', true);
    },
    closeRejectModel : function(component, event, helper) {
        component.set('v.isRejectModel', false);
    },
    openRejectModel : function(component, event, helper) {
        let data = component.find('paymentrecord').getSelectedRows();
        if(data.length <= 0) {
            helper.showToast('info', 'Missing Record', 'Please choose a Check record before clicking "Reject" button.');
            return;
        }
        component.set('v.isRejectModel', true);
    },
})