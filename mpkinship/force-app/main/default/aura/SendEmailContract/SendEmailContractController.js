({
	onInit : function(component, event, helper) {
		var action = component.get("c.GenerateCustomPDFAndEmail");
        action.setParams({ recordId : component.get("v.recordId") });
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                console.log('result'+result);
                if(result.Email_to_Vendor__c == false){
                    let toast = $A.get('e.force:showToast');
                    toast.setParams({
                        'Title': 'Error',
                        'type': 'error',
                        'message': 'Email is disabled for the vendor.'
                    });
                    toast.fire();
                    component.set("v.isSent",false) ;
                    let quickActionClose = $A.get("e.force:closeQuickAction");
                    quickActionClose.fire();
                }
                else{
                    result.Id = result.AccountId;
                    result.Name = result.Account.Name;
                    component.set("v.selected", result) ;
                    component.set("v.isSent",true) ;
                }
                
                /*component.set("v.result", response.getReturnValue()) ;
                /*let toast = $A.get('e.force:showToast');
                    toast.setParams({
                        'Title': 'Success',
                        'type': 'success',
                        'message': response.getReturnValue()
                    });
                    toast.fire();*/
                component.set("v.isLoaded", true);
                /*let quickActionClose = $A.get("e.force:closeQuickAction");
                   quickActionClose.fire();*/
                   
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                component.set("v.result", response.getError()) ;
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        let toast = $A.get('e.force:showToast');
                        toast.setParams({
                            'Title': 'Error',
                            'type': 'error',
                            'message': response.getError()
                        });
                    toast.fire();
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        }); 
        $A.enqueueAction(action);
	},
    emailSent : function(component, event, helper) {
		let quickActionClose = $A.get("e.force:closeQuickAction");
        quickActionClose.fire();
                   
        
	},
})