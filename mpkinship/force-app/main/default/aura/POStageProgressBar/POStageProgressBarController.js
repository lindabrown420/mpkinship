({
	doInit : function(component, event, helper) {
		 $A.get('e.force:refreshView').fire();	
	},
    Refreshit : function(component,event,helper){
    	var action= component.get("c.getProgressValue");
        action.setParams({
            	recordId: component.get("v.recordId")
        });
         action.setCallback(this, function(response){
             console.log('***action');
            var state = response.getState();
             console.log('**VAl of state'+state);
            if (state === "SUCCESS"){
            	console.log('**'+response.getReturnValue());  
                if(response.getReturnValue()!=''){
                    if(response.getReturnValue().recordtypename=='Purchase_Order'){
                        component.set("v.recordtypename",response.getReturnValue().recordtypename);
                        component.set("v.isPo",true);
                    }  
                    else{
                        component.set("v.isPo",false);
                    }
                	if(response.getReturnValue().stagename=='Draft')
                        component.set("v.currentStepval","2");
                    else if(response.getReturnValue().stagename=='Awaiting Approval')
                        component.set("v.currentStepval","3");
                    else if(response.getReturnValue().stagename=='Approved')
                        component.set("v.currentStepval","4");
                    else if(response.getReturnValue().stagename=='Open')
                         component.set("v.currentStepval","5"); 
                    else if(response.getReturnValue().stagename=='Closed'){
                    	component.set("v.currentStepval","5"); 
                        var cmpTarget = component.find('changeIt');
                        console.log('*****'+cmpTarget);
                        $A.util.removeClass(cmpTarget,'slds-path__item slds-is-active slds-is-current');                       
                         $A.util.addClass(cmpTarget,'slds-path__item slds-is-complete'); 
                        if(response.getReturnValue().recordtypename=='Purchase_Order'){
                            let button = component.find('CloseButtonId');
                            console.log('****button'+button);
                            button.set('v.disabled',true);
                        }    
                       
                    }
                       
                }
                else{
                    component.set("v.currentStepval",1);
                }
            }
             else{
                  component.set("v.currentStepval",1);
             }
        });        
        $A.enqueueAction(action);     
    },
    handleClick : function(component,event,helper){
    	var action= component.get("c.ChangeStage");
        action.setParams({
            	recordId: component.get("v.recordId")
        });  
         action.setCallback(this, function(response){
             console.log('***action');
            var state = response.getState();
             console.log('**VAl of state'+state);
            if (state === "SUCCESS"){
            	console.log('**'+response.getReturnValue()); 
                if(response.getReturnValue()==true){
                	helper.showToast('Success','Record Updated','Record has been updated.'); 
                    $A.get('e.force:refreshView').fire();	
                }
                else{
                     helper.showToast('Error','Record Failure','Record Id is not found.'); 
                }
            }
            else if(state === "ERROR"){
                console.log('**Enterd in error');              
                var errors = action.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        //alert(errors[0].message);
                        helper.showToast('Error','Record Failure',errors[0].message);
                    }
                }
            }
            else{
            	helper.showToast('Error', 'Record Failure', 'Record has not been updated.');     
            }
        });        
        $A.enqueueAction(action);       
    }    
})