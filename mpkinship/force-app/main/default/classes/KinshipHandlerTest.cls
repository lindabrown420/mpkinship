@isTest
private class KinshipHandlerTest {
    @TestSetup
    static void initData(){
        TriggerActivation__c ta = new TriggerActivation__c(Name = 'KinshipCaseTrigger', isActive__c = true);
        insert ta;
    }

    @isTest
    static void insertCaseTest(){
        Contact c = new Contact();
        c.FirstName = 'FN';
        c.LastName = 'LN';
        insert c;
        Kinship_Case__c kc = new Kinship_Case__c();
        kc.Case_Code__c = 'Test FN';
        kc.ASAPS_Number__c = 'Test LN';
        kc.Name = 'Name';
        kc.Primary_Contact__c = c.id;
        Test.startTest();
        insert kc;
        Test.stopTest();
        List<Kinship_History__c> lstHis = [SELECT Id, Name, Action__c, Object_Id__c, Object_Name__c, 
                    Field_Name__c, New_Value__c, Old_Value__c , CreatedBy.Name, CreatedDate
                    FROM Kinship_History__c
                    WHERE Object_Id__c = :kc.id];
        System.assertNotEquals(0, lstHis.size(), 'The Case History list is equal 1');
    }

    @isTest
    static void updateCaseTest(){
        Contact c = new Contact();
        c.FirstName = 'FN';
        c.LastName = 'LN';
        insert c;
        Kinship_Case__c kc = new Kinship_Case__c();
        kc.Case_Code__c = 'Test FN';
        kc.ASAPS_Number__c = 'Test LN';
        kc.Name = 'Name';
        kc.Primary_Contact__c = c.id;
        insert kc;

        Test.startTest();
        kc.Case_Code__c = 'Test FN New';
        kc.ASAPS_Number__c = 'Test LN New';
        update kc;
        Test.stopTest();
        List<Kinship_History__c> lstHis = [SELECT Id, Name, Action__c, Object_Id__c, Object_Name__c, 
                    Field_Name__c, New_Value__c, Old_Value__c , CreatedBy.Name, CreatedDate
                    FROM Kinship_History__c
                    WHERE Object_Id__c = :kc.id];
        System.assertNotEquals(0, lstHis.size(), 'The Case History list is equal 1');
    }

    @isTest
    static void deleteCaseTest(){
        Contact c = new Contact();
        c.FirstName = 'FN';
        c.LastName = 'LN';
        insert c;
        Kinship_Case__c kc = new Kinship_Case__c();
        kc.Case_Code__c = 'Test FN';
        kc.ASAPS_Number__c = 'Test LN';
        kc.Name = 'Name';
        kc.Primary_Contact__c = c.id;
        insert kc;
        Test.startTest();
        delete kc;
        Test.stopTest();
        List<Kinship_History__c> lstHis = [SELECT Id, Name, Action__c, Object_Id__c, Object_Name__c, 
                    Field_Name__c, New_Value__c, Old_Value__c , CreatedBy.Name, CreatedDate
                    FROM Kinship_History__c
                    WHERE Object_Id__c = :kc.id];
        System.assertNotEquals(0, lstHis.size(), 'The Case History list is equal 1');
    }
}