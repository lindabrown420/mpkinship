@isTest
public class ChartOfAccountTriggerHandlerTest {
	@testSetup
    static void testRecords(){
        TriggerActivation__c  ta = new TriggerActivation__c();
        ta.name ='ChartOfAccountTrigger';
        ta.isActive__c = true;
        insert ta;
    }
    
    @isTest
    public static void testInsert(){
        Locality_Account__c la = new Locality_Account__c();
        la.Name='222';
        insert la;
        Category__c category = new Category__c();
        category.Name = '222';
        category.Locality_Account__c = la.id;
        category.Category_Type__c = 'Adoption';
        category.RecordTypeId = Schema.SObjectType.category__C.getRecordTypeInfosByDeveloperName().get('LASER_Category').getRecordTypeId();
        insert category;
        
        Cost_Center__c cc = new Cost_Center__c(Name = '12345');
        insert cc;
        
        try{
            Test.startTest();
            Chart_of_Account__c coa = new Chart_of_Account__c();
            coa.Locality_Account__c = la.Id;
            coa.Cost_Center__c = cc.id;
            insert coa;
            
            update coa;
            Test.stopTest();
        }catch(Exception e ){

        }
    }
}