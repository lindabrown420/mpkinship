@isTest
public class ServicePlanGoals_Test {
    @testSetup
    static void testecords(){
        
        contact con = new contact();
        con.LastName='contactrec';
        con.FirstName='test';
        con.Email='test@contact.com';       
        insert con;
        
        Kinship_Case__c caserec = new Kinship_Case__c();
        caserec.Primary_Contact__c=con.id;
        caserec.DOB__c=system.today().adddays(-100);
        caserec.Race__c='1';
        caserec.Gender_Preference__c='M';
        caserec.Hispanic_Ethnicity__c='1';
        caserec.DSM_V_Indicator_ICD_10_1__c='1';
        caserec.Clinical_Medication_Indicator_1__c='1';
        caserec.Medicaid_Indicator_1__c='1';
        caserec.Referral_Source__c='2';
        caserec.Autism_Flag_1__c='2';
        insert caserec;
        
        IFSP__c sp = new IFSP__c();
        sp.Case__c = caserec.id;
       // sp.Name = 'test';
        insert sp;
        
        Programs__c pr = new Programs__c();
        pr.Case__c = caserec.id;
        pr.Name = 'Test';
        insert pr;
        
        Program_Engagement__c pe = new Program_Engagement__c();
        pe.Case__c = caserec.id;
        pe.Programs__c = pr.id;
        insert pe;
        
        Goal_Template__c gt = new Goal_Template__c();
        gt.Programs__c = pr.id;
        gt.Name = 'Test';
        insert gt; 
        
        Goal__c g = new Goal__c();
        g.Goal_Template__c = gt.id;
        g.Service_Plan__c = sp.Id;
        insert g;
        
        Action_Item_Template__c ait = new Action_Item_Template__c();
        ait.Name = 'test';
        insert ait;
        
        Goal_Action_Item_Template__c goi = new Goal_Action_Item_Template__c();
        goi.Goal_Template__c = gt.id;
        goi.Action_Item_Template__c = ait.id;
        insert goi;
        
        Action_Item__c ai = new Action_Item__c();
        ai.Goal__c = g.id;
        ai.Name = 'test';
        ai.Action_Item_Template__c = ait.id;
        ai.Service_Plan__c = sp.Id;
        ai.Status__c = 'Not Started';
        ai.Due_Date__c = date.today();
        insert ai;
        
        
    }
    public static testmethod void testcreateGoals(){
        Kinship_Case__c c = [select id from Kinship_Case__c];
        //ServicePlanGoals.getGoalRecordsCase(c.Id);
        
        IFSP__c s = [Select id from IFSP__c];
        ServicePlanGoals.getGoalRecords(s.Id);
        
        Goal__c gl = [Select id from Goal__c];
        ServicePlanGoals.getGoalRecord(gl.Id);
        
        Goal_Action_Item_Template__c gait = [select id from Goal_Action_Item_Template__c];
        
        Goal_Template__c glt = [select id from Goal_Template__c];
        
        Action_Item__c acit = [select id,name,Goal__c,Action_Item_Template__c,Service_Plan__c,Status__c,Due_Date__c from Action_Item__c];
        string jsonString = '['+JSON.serialize(acit)+']';
        
        ServicePlanGoals.getGoalTemplates(s.Id);
        ServicePlanGoals.getGoalTemplatesCase(c.Id);
        ServicePlanGoals.getObjectiveTemplates(gl.Id,null);
        ServicePlanGoals.getObjectiveTemplates(null,gait.id);
        ServicePlanGoals.saveRecord(jsonString,s.id,'test',gl.id,'Not Started',glt.id);
        ServicePlanGoals.deleteRecords(gl.Id,'Goal');
        //ServicePlanGoals.deleteRecords(acit.Id,'ActionItem');
        
    }
}