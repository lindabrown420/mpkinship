@isTest
private class TransactionManagementControllerTest {
    @TestSetup
    static void initData(){
        FIPS__c fp = new FIPS__c();
        fp.name='test fips';
        insert fp;
        
        Campaign parent = new Campaign();
        parent.name = '2021';
        parent.StartDate = Date.Today().addDays(-10);
        parent.EndDate = Date.Today().addDays(10);
        parent.Fiscal_Year_Type__c = 'DSS';
        parent.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId();
        parent.FIPS__c = fp.id;
        parent.IsActive = true;
        insert parent;
        
        Campaign c = new Campaign();
        c.name = '2021';
        c.StartDate = Date.Today().addDays(-10);
        c.EndDate = Date.Today().addDays(10);
        c.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('LASER Reporting Period').getRecordTypeId();
        c.ParentId = parent.id;
        c.FIPS__c = fp.id;
        c.IsActive = true;
        insert c;

        Transaction_Journal__c tj = new Transaction_Journal__c();
        tj.status__c = 'Open';
        //tj.Amount__c = 1000;
        tj.Description__c = 'description';
        tj.Child_Campaign__c = c.id;
        tj.RecordTypeId = Schema.SObjectType.Transaction_Journal__c.getRecordTypeInfosByName().get('LASER').getRecordTypeId();
        insert tj;

        Account_Period_Transaction__c apt = new Account_Period_Transaction__c();
        apt.Transaction_Journal__c = tj.id;
        apt.LASER_Reporting_Period__c = c.id;
        insert apt;
    }

    @isTest
    static void testKinshipSearchLookup(){
        List<Campaign> lstData = [select id, name from Campaign Where RecordType.Name = 'LASER Reporting Period'];
        List<Id> fixedSearchResults= new List<Id>();
        fixedSearchResults.add(lstData[0].id);
        Test.setFixedSearchResults(fixedSearchResults);
        Test.startTest();
        List<Object> lstResult = TransactionManagementController.kinshipSearchLookup('20', new List<String>(), 'Campaign');
        Test.stopTest();
        System.assertEquals(1, lstResult.size(), 'The kinshipSearchLookup method return does not equal 1');
    }
    
    @isTest
    static void testKinshipSearchLookup_User(){
        List<User> lstData = [select id, name from User];
        List<Id> fixedSearchResults= new List<Id>();
        fixedSearchResults.add(lstData[0].id);
        Test.setFixedSearchResults(fixedSearchResults);
        Test.startTest();
        List<Object> lstResult = TransactionManagementController.kinshipSearchLookup('20', new List<String>(), 'User');
        Test.stopTest();
        System.assertNotEquals(null, lstResult, 'The kinshipSearchLookup method return does not equal 1');
    }

    @isTest
    static void testGetTransactionByAP(){
        List<String> lstId = new List<String>();
        for(Campaign c : [SELECT id FROM Campaign Where RecordType.Name = 'LASER Reporting Period']){
            lstId.add(c.id);
        } 
        Transaction_Journal__c tj = [SELECT Id, Status__c FROM Transaction_Journal__c LIMIT 1];
        tj.Status__c = 'Posted';
        update tj;
        Test.startTest();
        List<TransactionManagementController.TransJournal> lstTJ = TransactionManagementController.getTransactionByAP(lstId);
        Test.stopTest();

        System.assertEquals(1, lstTJ.size(), 'The List TransJournal does not equal 1');
    }
    
    @isTest
    static void testGetDefaultAP(){
        List<String> lstId = new List<String>();
        for(Campaign c : [SELECT id FROM Campaign Where RecordType.Name = 'LASER Reporting Period']){
            lstId.add(c.id);
        }
        Test.startTest();
        TransactionManagementController.getDefaultAP(lstId[0]);
        Test.stopTest();
    }

    @isTest
    static void testUpdateTransactionStatus(){
        List<Transaction_Journal__c> lstTJ = [SELECT id FROM Transaction_Journal__c];
        list<String> lstId = new List<String>();
        for(Transaction_Journal__c tj : lstTJ) {
            lstId.add(tj.id);
        }
        Test.startTest();
        TransactionManagementController.updateTransactionStatus(lstId, 'Posted');
        Test.stopTest();

        lstTJ = [SELECT id FROM Transaction_Journal__c WHERE status__c = 'Posted'];
        System.assertEquals(1, lstTJ.size(), 'The List Posted Transaction_Journal__c does not equal 1');
    }
    
    @isTest
    static void testCreateLASERCertificationReport(){
        List<Transaction_Journal__c> lstTJ = [SELECT id FROM Transaction_Journal__c];
        list<String> lstId = new List<String>();
        for(Transaction_Journal__c tj : lstTJ) {
            lstId.add(tj.id);
        }
        TransactionManagementController.updateTransactionStatus(lstId, 'Posted');
        
        Campaign c = [SELECT id FROM Campaign Where RecordType.Name = 'LASER Reporting Period'];
        
        Test.startTest();
        LASER_Report_TJ__c report = TransactionManagementController.createLASERCertificationReport(c.id);
        TransactionManagementController.insertTR(report);
        Test.stopTest();

        System.assertNotEquals(null, report, 'The createLASERCertificationReport is null');
    }
    
    @isTest
    static void testCreateLASERReconciliationReport(){
        List<Transaction_Journal__c> lstTJ = [SELECT id FROM Transaction_Journal__c];
        list<String> lstId = new List<String>();
        for(Transaction_Journal__c tj : lstTJ) {
            lstId.add(tj.id);
        }
        TransactionManagementController.updateTransactionStatus(lstId, 'Posted');
        
        Campaign c = [SELECT id FROM Campaign Where RecordType.Name = 'LASER Reporting Period'];
        
        Test.startTest();
        LASER_Report_TJ__c report = TransactionManagementController.createLASERReconciliationReport(c.id);
        TransactionManagementController.insertTR(report);
        Test.stopTest();

        System.assertNotEquals(null, report, 'The createLASERReconciliationReport is null');
    }
    
    @isTest
    static void testGetExportData(){
        List<Transaction_Journal__c> lstTJ = [SELECT id FROM Transaction_Journal__c];
        list<String> lstId = new List<String>();
        for(Transaction_Journal__c tj : lstTJ) {
            lstId.add(tj.id);
        }
        TransactionManagementController.updateTransactionStatus(lstId, 'Posted');
        
        Campaign c = [SELECT id FROM Campaign Where RecordType.Name = 'LASER Reporting Period'];
        
        Test.startTest();
        List<TransactionManagementController.DataExport> lstResult = TransactionManagementController.getExportData(c.id);
        Test.stopTest();

        System.assertNotEquals(null, lstResult.size(), 'The getExportData is null');
    }
    
    @isTest
    static void testSubmitCampaignForApproval(){
        List<Transaction_Journal__c> lstTJ = [SELECT id FROM Transaction_Journal__c];
        list<String> lstId = new List<String>();
        for(Transaction_Journal__c tj : lstTJ) {
            lstId.add(tj.id);
        }
        TransactionManagementController.updateTransactionStatus(lstId, 'Posted');
        Campaign c = [SELECT id FROM Campaign Where RecordType.Name = 'LASER Reporting Period'];
        LASER_Report_TJ__c report = TransactionManagementController.createLASERCertificationReport(c.id);
        
        Test.startTest();
        TransactionManagementController.submitCampaignForApproval(c.Id, UserInfo.getUserId(), TransactionManagementController.LASER_CERT, report);
        List<TransactionManagementController.CampaignApproval> result = TransactionManagementController.getApprovalCampignData();
        Test.stopTest();

        System.assertNotEquals(null, result, 'The submitCampaignForApproval is null');
    }
    
    @isTest
    static void testSubmitCampaignForApproval2(){
        List<Transaction_Journal__c> lstTJ = [SELECT id FROM Transaction_Journal__c];
        list<String> lstId = new List<String>();
        for(Transaction_Journal__c tj : lstTJ) {
            lstId.add(tj.id);
        }
        TransactionManagementController.updateTransactionStatus(lstId, 'Posted');
        Campaign c = [SELECT id FROM Campaign Where RecordType.Name = 'LASER Reporting Period'];
        LASER_Report_TJ__c report = TransactionManagementController.createLASERReconciliationReport(c.id);
        
        Test.startTest();
        TransactionManagementController.submitCampaignForApproval(c.Id, UserInfo.getUserId(), TransactionManagementController.LASER_RECON, report);
        List<TransactionManagementController.CampaignApproval> result = TransactionManagementController.getApprovalCampignData();
        Test.stopTest();

        System.assertNotEquals(null, result, 'The submitCampaignForApproval is null');
    }
    
    @isTest
    static void testUpdateCampaignStatus(){
        List<Transaction_Journal__c> lstTJ = [SELECT id FROM Transaction_Journal__c];
        list<String> lstId = new List<String>();
        for(Transaction_Journal__c tj : lstTJ) {
            lstId.add(tj.id);
        }
        TransactionManagementController.updateTransactionStatus(lstId, 'Posted');
        Campaign c = [SELECT id FROM Campaign Where RecordType.Name = 'LASER Reporting Period'];
        List<String> lstCamp = new List<String>();
        lstCamp.add(c.id + '_' + TransactionManagementController.LASER_CERT);
        
        Test.startTest();
        List<Campaign> result = TransactionManagementController.updateCampaignStatus(lstCamp, 'Approved', 'Notes');
        Test.stopTest();

        System.assertNotEquals(null, result, 'The submitCampaignForApproval is null');
    }
    
    @isTest
    static void testUpdateCampaignStatus2(){
        List<Transaction_Journal__c> lstTJ = [SELECT id FROM Transaction_Journal__c];
        list<String> lstId = new List<String>();
        for(Transaction_Journal__c tj : lstTJ) {
            lstId.add(tj.id);
        }
        TransactionManagementController.updateTransactionStatus(lstId, 'Posted');
        Campaign c = [SELECT id FROM Campaign Where RecordType.Name = 'LASER Reporting Period'];
        List<String> lstCamp = new List<String>();
        lstCamp.add(c.id + '_' + TransactionManagementController.LASER_RECON);
        
        Test.startTest();
        List<Campaign> result = TransactionManagementController.updateCampaignStatus(lstCamp, 'Approved', 'Notes');
        Test.stopTest();

        System.assertNotEquals(null, result, 'The submitCampaignForApproval is null');
    }
    @isTest
    static void testPostAP(){
        Campaign c = [SELECT id, Name, StartDate, EndDate, RecordTypeId, ParentId, Parent.ParentId , FIPS__c, Cate_Type__c FROM Campaign Where RecordType.Name = 'LASER Reporting Period'];
        List<String> lstCamp = new List<String>();
        lstCamp.add(c.id);
        Test.startTest();
        TransactionManagementController.postedAP(lstCamp);
        TransactionManagementController.checkPostAP(lstCamp);
        Test.stopTest();
    }
    
    @isTest
    static void testGetLASERReportData(){
        List<String> lstId = new List<String>();
        for(Campaign c : [SELECT id FROM Campaign Where RecordType.Name = 'LASER Reporting Period']){
            lstId.add(c.id);
        }
        Test.startTest();
        List<TransactionManagementController.ReportData> lstTJ = TransactionManagementController.getLASERReportData(lstId);
        Test.stopTest();

        System.assertNotEquals(null, lstTJ, 'The List ReportData is null');
    }
}