public class CheckRunHelper{

    public static boolean IsRun=false;
    
    public static void ChangeInvoicePaymentDates(set<id> checkRunIds){
        ISRun= true;
        Paymentrecordhelper.IsPaymentDateUpdate=true;
        TransactionJournalService.byPassCheckPosted = true ;
        list<Kinship_Check__c> checks=[select id,Issue_Date__c,Check_Run__c,Check_Run__r.Check_Date__c from Kinship_Check__c 
                                       where Check_Run__c in:checkRunIds];
                                           
       list<Kinship_Check__c> checksToUpdate = new list<Kinship_Check__c>(); 
       set<id> UpdatedChecksIds = new set<id>();                               
        for(Kinship_Check__c checkrec : checks){
            if(checkrec.Issue_Date__c!= checkrec.Check_Run__r.Check_Date__c){
                Kinship_Check__c updateCheck = new Kinship_Check__c ();
                updateCheck.id = checkrec.id;
                updateCheck.Issue_Date__c=checkrec.Check_Run__r.Check_Date__c;
                checksToUpdate.add(updateCheck);
                UpdatedChecksIds.add(checkrec.id);
            }
        } 
        
        if(checksToUpdate.size()>0)
            update checksToUpdate;   
        
        list<Payment__c> payments =[select id,Payment_Date__c,Kinship_Check__c,Kinship_Check__r.Issue_Date__c from
                    Payment__c where Kinship_Check__c in: UpdatedChecksIds];
        list<Payment__c> PaymentsToUpdate = new list<Payment__c>();            
        for(Payment__c payment : payments){
            if(payment.Payment_Date__c!= payment.Kinship_Check__r.Issue_Date__c){
                Payment__c  updatePayment = new Payment__c();
                updatePayment.id = payment.id;
                updatePayment.Payment_Date__c = payment.Kinship_Check__r.Issue_Date__c;
                PaymentsToUpdate.add(updatePayment);
                
            }
        }  
        
        if(PaymentsToUpdate.size()>0)
            update PaymentsToUpdate;         
  
    }
}