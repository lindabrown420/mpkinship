@isTest
public class BudgetTransactionTrgTest {
    public static testmethod void testRun(){
        
        TriggerActivation__c  ta1 = new TriggerActivation__c();
        ta1.name='Budget Transaction Trigger';
        ta1.isActive__c=true;
        insert ta1;
        
        Budgets__c rec = new Budgets__c();
        rec.Name = 'Test 2';
        insert rec;
        
        
        Budget_Transactions__c rec2 = new Budget_Transactions__c();
        rec2.Name = 'Test 2';
        rec2.Budgets__c = rec.Id;
        insert rec2;
        
    }
}