@IsTest
public class InvoiceLineItemTrgTest {
    public static testmethod void test1(){
        
        contact con = new contact();
        con.LastName='contactrec';
        con.FirstName='test';
        con.Email='test@contact.com';        
        insert con;

        Kinship_Case__c caserec = new Kinship_Case__c();
        caserec.Primary_Contact__c=con.id;
        insert caserec;
        account accountrec = new account();
        accountrec.Name='test vendor';
        accountrec.Tax_ID__c='222-22-3333';
        accountrec.Entity_Type__c='Individual';
        insert accountrec;
        id recType = Schema.SObjectType.opportunity.getRecordTypeInfosByDeveloperName().get('Purchase_Order').getRecordTypeId();
         opportunity opprecord = new opportunity();
        opprecord.CloseDate=date.today();
        opprecord.StageName='Draft';
        opprecord.Name='test opportunity';
        opprecord.Kinship_Case__c=caserec.id;
        //opprecord.Payment_Type__c='Credit Card';
        //opprecord.campaignid=camp[0].id;
        opprecord.Amount=20000;
        //opprecord.Category__c =categories[0].id;
        opprecord.AccountId=accountrec.id;
        opprecord.RecordTypeId=recType;
        opprecord.Number_of_Months_Units__c=3;
        //opprecord.Pricebook2Id =pb.id;        
        insert opprecord;
        
        id payrecType = Schema.SObjectType.Payment__c.getRecordTypeInfosByName().get('Vendor Invoice').getRecordTypeId();
        Payment__c payment = new Payment__c();
        payment.Opportunity__c=opprecord.id;
        payment.Payment_Amount__c=2000;
        payment.vendor__c = accountrec.Id;
        payment.RecordTypeId=payrecType;
        insert payment;
        
        Payment_Line_Item__c item= new Payment_Line_Item__c();
        item.Invoice__c=payment.id;
        insert item;
        Payment_Line_Item__c item1= new Payment_Line_Item__c();
        item1.Invoice__c=payment.id;
        insert item1;
        
        item.LineItem_Amount_Billed__c=0;
        item1.LineItem_Amount_Billed__c=0;
        update item;
        update item1;
        
    }
}