public class TaxRateTrgHelper{

    public static boolean IsRun=false;
    public static boolean isRunUpdate = false;
    
    public static void CheckActiveRate(list<Tax_Rates__c> taxRateList,set<id> taxrateids){
        if(Trigger.isInsert)
            IsRun=true;
        if(trigger.isUpdate)
            isRunUpdate =true;    
         set<id> SocialSecurityIds = new set<id>();
         set<id> MedicareIds = new set<id>();
         set<id> FICAIds= new set<id>();
         set<id> FUTAIds = new set<id>();
         set<id> SUTAIds = new set<id>();
         
         
         list<Tax_Rates__c> taxRates = new list<Tax_Rates__c>();
        if(Trigger.isInsert){
            taxRates=[select id,Active__c,Type__c,recordtypeid from Tax_Rates__c where active__c=:true];
            system.debug('**VAl of taxrates'+taxRates);
        }    
         else
             taxRates= [select id,Active__c,Type__c,recordtypeid from Tax_Rates__c where active__c=:true and id not in: taxrateids];   
        for(Tax_Rates__c tr : taxRates){
              system.debug('**Val of tr'+tr);
              system.debug('**Val of type'+tr.type__c);
               if(tr.Type__c=='FICA')
                    FICAIds.add(tr.id);     
                else if(tr.Type__c=='Social Security'){
                    SocialSecurityIds.add(tr.id);
                }
                else if(tr.Type__c=='Medicare')
                    MedicareIds.add(tr.id);   
            
                else if(tr.Type__c=='FUTA')
                    FUTAIds.add(tr.id);
            
                else if(tr.Type__c=='SUTA')
                    SUTAIds.add(tr.id);
                        
        }
        
        for(Tax_Rates__c tr : taxRateList){
            if( tr.Type__c=='Social Security' && SocialSecurityIds.size()>0)
                tr.addError('You already have an active FICA Tax Rate of Social Security Type.');
            else if ( tr.Type__c=='Medicare' && MedicareIds.size()>0)
                tr.addError('You already have an active FICA Tax Rate of Medicare Type.');
             else if ( tr.Type__c=='FICA' && FICAIds.size()>0)
                tr.addError('You already have an active FICA Tax Rate');
             else if ( tr.Type__c=='FUTA' && FUTAIds.size()>0)
                tr.addError('You already have an active FICA Tax Rate of FUTA Type.');
             else if ( tr.Type__c=='SUTA' && SUTAIds.size()>0)
                tr.addError('You already have an active FICA Tax Rate of SUTA Type.');         
        }
        
    }
    
    
}