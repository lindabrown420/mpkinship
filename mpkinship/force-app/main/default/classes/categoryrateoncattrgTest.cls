@IsTest
public class categoryrateoncattrgTest {
    public static testmethod void categoryRateonCatmethod(){
        Id LEDRSRecTypeId = Schema.SObjectType.category__C.getRecordTypeInfosByDeveloperName().get('LEDRS_Category').getRecordTypeId();
        Service_Names_Code__c snc = new Service_Names_Code__c();
        snc.name='snc test';
        snc.Service_Name_Code__c ='2';
        insert snc;
        Category__c category = new Category__c();
        category.Name='test';
        category.Category_Type__c='CSA';
        category.RecordTypeId=LEDRSRecTypeId;
        insert category;
        
        Category__c category1 = new Category__c();
        category1.Name='test';
        category1.Category_Type__c='General Cases';
        category1.Service_Names_Code__c=snc.Id;
        Id LASERRecTypeId = Schema.SObjectType.Category__c.getRecordTypeInfosByName().get('LASER Category').getRecordTypeId();
        category1.RecordTypeId=LASERRecTypeId;
        insert category1;
        Category__c category11 = new Category__c();
        category11.Name='test';
        category11.Category_Type__c='CSA';
        category11.RecordTypeId=LEDRSRecTypeId;
        insert category11;
        Service_Placement_Types__c spt = new Service_Placement_Types__c();
        spt.name='test spt';
        spt.Code__c='1';
        insert spt;
         CategoryAndSPT__c catspt= new CategoryAndSPT__c();
        catspt.Category__c=category1.Id;
		catspt.Service_Placement_Type__c=spt.Id;
        insert catspt;
        Category_Rate__c cr = new Category_Rate__c();
        cr.Category__c = category.id;
        cr.Status__c='Active';
        cr.Price__c=5000;
        cr.Effective_Date__c=system.today();
        cr.Min_Age__c=5;
        cr.Max_Age__c=15;
        cr.Prorate_First_Payment__c=true;
        cr.Prorate_final_payment__c=true;
        cr.Service_Placement_Type__c=spt.Id;
        cr.RecordTypeId = Schema.SObjectType.Category_Rate__c.getRecordTypeInfosByDeveloperName().get('Service_Names').getRecordTypeId(); 
        insert cr;
        Category_Rate__c cr1 = new Category_Rate__c();
        cr1.Category__c = category1.id;
        cr1.Status__c='Active';
        cr1.Price__c=5000;
        cr1.Effective_Date__c=system.today();
        cr1.Min_Age__c=2;
        cr1.Max_Age__c=4;
        cr1.Prorate_First_Payment__c=true;
        cr1.Prorate_final_payment__c=true;
        cr1.Service_Placement_Type__c=spt.Id;
        cr1.RecordTypeId = Schema.SObjectType.Category_Rate__c.getRecordTypeInfosByDeveloperName().get('Service_Names').getRecordTypeId(); 
        insert cr1;
        //try{
        //insert cr1;
        //}
        //catch(exception ex){}
        Category_Rate__c cr2 = new Category_Rate__c();
        cr2.Category__c = category1.id;
        cr2.Status__c='Inactive';
        cr2.Price__c=5000;
        cr2.Effective_Date__c=system.today();
        cr2.Min_Age__c=2;
        cr2.Max_Age__c=4;
        cr2.Prorate_First_Payment__c=true;
        cr2.Prorate_final_payment__c=true;

        cr2.RecordTypeId = Schema.SObjectType.Category_Rate__c.getRecordTypeInfosByDeveloperName().get('Service_Placement_Types').getRecordTypeId(); 
        try{
            insert cr2;}
        catch(exception ex){}
        
        Category_Rate__c cr11 = new Category_Rate__c();
        cr11.Category__c = category1.id;
        cr11.Status__c='Inactive';
        cr11.Price__c=5000;
        cr11.Effective_Date__c=system.today();
        cr11.Min_Age__c=4;
        cr11.Max_Age__c=8;
        cr11.Prorate_First_Payment__c=true;
        cr11.Prorate_final_payment__c=true;
        cr11.RecordTypeId = Schema.SObjectType.Category_Rate__c.getRecordTypeInfosByDeveloperName().get('Service_Names').getRecordTypeId(); 
        insert cr11;
        cr11.Status__c='Active';
        update cr11;
    }
}