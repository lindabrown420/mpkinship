public class BudgetTriggerHelper {
    public static Boolean bypassTrigger = false;
    public static void updateBudgetName(List<Budgets__c> lstNew) {
        if(BudgetTriggerHelper.bypassTrigger) return;
        List<Budgets__c> budgetUpdate = new List<Budgets__c>();
        for(Budgets__c budget : lstNew){
            budgetUpdate.add(new Budgets__c(Id = budget.Id, Name = budget.Name_Format__c));
        } 
        BudgetTriggerHelper.bypassTrigger = true;
        update budgetUpdate;
        BudgetTriggerHelper.bypassTrigger = false;
    }
}