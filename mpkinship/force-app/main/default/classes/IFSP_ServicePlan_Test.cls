@isTest
public class IFSP_ServicePlan_Test {
	@testSetup
    static void testecords(){
        
        Account acc = new Account();
        acc.Name = 'Test';
        insert acc;
        
        contact con = new contact();
        con.LastName='contactrec';
        con.FirstName='test';
        con.Email='test@contact.com';       
        insert con;
        
        Kinship_Case__c caserec = new Kinship_Case__c();
        caserec.Primary_Contact__c=con.id;
        caserec.DOB__c=system.today().adddays(-100);
        caserec.Race__c='1';
        caserec.Gender_Preference__c='M';
        caserec.Hispanic_Ethnicity__c='1';
        caserec.DSM_V_Indicator_ICD_10_1__c='1';
        caserec.Clinical_Medication_Indicator_1__c='1';
        caserec.Medicaid_Indicator_1__c='1';
        caserec.Referral_Source__c='2';
        caserec.Autism_Flag_1__c='2';
        insert caserec;
        
        IFSP__c sp = new IFSP__c();
        sp.Case__c = caserec.id;
        //sp.Name = 'test';
        insert sp;
        
        Education__c edu = new Education__c();
        edu.Case__c = caserec.id;
        insert edu;
        
        Health__c hlt = new Health__c();
        hlt.Case__c = caserec.id;
        insert hlt;
        
        Requisition__c req = new Requisition__c();
        //req.Name = 'Test';
        req.Case__c = caserec.id;
        req.Vendor__c = acc.id;
        insert req;
        
    }
    
    public static testmethod void testcreateGoals(){
        Kinship_Case__c c = [select id from Kinship_Case__c];
        //ServicePlanGoals.getGoalRecordsCase(c.Id);
        
        IFSP__c s = [Select id from IFSP__c];
        
        Education__c e = [Select Id,Case__c from Education__c];
        
        Health__c h = [Select Id,Case__c from Health__c];
        
        Requisition__c r = [Select Id,Case__c from Requisition__c];
        
        IFSP_ServicePlan.getServicePlan(s.id);
        
        IFSP_ServicePlan.getCaseRecord(c.id);
        
        IFSP_ServicePlan.getEducationalInformationIds(c.id);
        
        IFSP_ServicePlan.getHealthInformationIds(c.id);
        
        IFSP_ServicePlan.getRequisitionInformationIds(c.id);
        
        IFSP_ServicePlan.updateEducation(e.id, c.id);
        
        IFSP_ServicePlan.updateHealth(h.id, c.id);
        
        IFSP_ServicePlan.updateRequisition(r.id, c.id);        
        
        //IFSP_ServicePlan.deleteRecords(c.id);
    }
}