@isTest(SeeAllData=true)
public class ProcessDdpExtTest {
    @isTest
    private static void TestProcessDdpExt(){
        Kinship_Check__c kc = new Kinship_Check__c();
        kc.Check_No__c = '99999999';
        kc.Status__c = 'Created';
        insert kc;

        Loop__DDP__c ddid = [SELECT Id, Name, Loop__Scheduled_Delivery_Option__c FROM Loop__DDP__c limit 1];
        Loop__DDP_Integration_Option__c doid = [SELECT Id, Name, Loop__DDP__c FROM Loop__DDP_Integration_Option__c WHERE Loop__DDP__c = :ddid.id limit 1];

        PageReference pageRef = Page.ProcessDdp;
        Test.setCurrentPage(pageRef);
        ApexPages.CurrentPage().getParameters().put('ddpId', ddid.id);
        ApexPages.CurrentPage().getParameters().put('deliveryOptionId', doid.id);
        ApexPages.CurrentPage().getParameters().put('ids', kc.id);
        ApexPages.CurrentPage().getParameters().put('sObjectType', 'Kinship_Check__c');
        Test.startTest();
        try{
        ProcessDdpExt pe = new ProcessDdpExt();
        }catch(Exception e){}
        ProcessDdpExt.RecordInfo ri = new ProcessDdpExt.RecordInfo(kc, null);
        Test.stopTest();

        System.assertNotEquals(null, ri);
    }
}