@isTest
public class ContactControllerTest {
	public static testMethod void test(){
        Account acc= new Account();
        acc.Name = 'test acc';
        insert acc;
        
        List<Account> selectedOptions = new List<Account>();
        selectedOptions.add(acc);
        
        Contact c = new Contact();
        Contact c1 = new Contact();
        c.LastName = 'test';
        c.Email = 'test@test.com';
        insert c;
        c1.LastName = 'test1';
        c1.Email = 'test1@gmail.com';
        insert c1;
        List<Contact> clist = new List<Contact>();
        clist = [Select id,LastName from Contact];
		ContactController.getContacts('test',clist);
        List<User> u = new List<User>();
        u = [Select Id from User];
        ContactController.getUsers('test',u);
        ContactController.getAccount('Name',  selectedOptions);
    }
    public static testMethod void test1(){
        Contact c = new Contact();
        Contact c1 = new Contact();
        c.LastName = 'test';
        c.Email = 'test@test.com';
        insert c;
        List<Contact> clist = new List<Contact>();
		ContactController.getContacts('test',clist);
        List<User> u = new List<User>();
        User uName = [Select Id,Name from User Limit 1];
        ContactController.getUsers(uName.Name,u);
    }
    
}