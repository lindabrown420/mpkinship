public class POStageProgressBarCtr{
    @auraEnabled
    public static POwrap getProgressValue(string recordId){
         string progressstage='';
         POwrap powraprec = new POwrap();
         list<opportunity> opportunities = new list<opportunity>();
         if(recordid!='' && recordid!=null){
            opportunities=[select id,name,stagename,recordtypeid,recordtype.developername from opportunity where id=:recordid];            
             if(opportunities.size()>0 && opportunities[0].recordtype.developername !=null)
                 powraprec.recordtypename=  opportunities[0].recordtype.developername; 
         }
        if(opportunities.size()>0 && opportunities[0].stagename=='Draft')
          progressstage='Draft'; 
        if(opportunities.size()>0 && opportunities[0].stagename=='Awaiting Approval')
            progressstage='Awaiting Approval';
        if(opportunities.size()>0 && opportunities[0].stagename=='Approved')
            progressstage ='Approved';
        if(opportunities.size()>0 && opportunities[0].stagename=='Open')
            progressstage='Open';
        if(opportunities.size()>0 && opportunities[0].stagename=='Closed')
            progressstage='Closed';
        powraprec.stagename=progressstage; 
       
         return powraprec;
    }
    
    @auraEnabled
    public static boolean ChangeStage(String recordId){
        string msg='';
        boolean isUpdated=false;
        try{ 
            list<opportunity> opplist=[select id,stagename from opportunity where id=:recordId];
            opportunity opp = new opportunity();
            opp.id=opplist[0].id;
            opp.stagename='Closed';
            update opp;
            isUpdated=true;
            return isUpdated;          
        }       
       catch(Exception e){           
            if(!Test.isRunningTest())
                throw new AuraHandledException(e.getMessage());
            else
                return null;
        }
    }
    
    public class POwrap{
     @auraEnabled
     public string stagename;
     @auraEnabled
     public string recordtypename;
    }

}