@isTest
public class CSAagendaTrgTest {
    @testSetup
    static void testRecords(){
        contact con = new contact();
        con.LastName='contactrec';
        con.FirstName='test';
        con.Email='test@contact.com';
        insert con;
        
        CSA_Agenda_Team__c csateam = new CSA_Agenda_Team__c();
        csateam.Name='test team';
        insert csateam;
        
        CSA_Meeting_Agenda__c meetingagenda = new CSA_Meeting_Agenda__c();
        meetingagenda.Location_of_Meeting__c='california';
        meetingagenda.Name='fapt meeting';
        meetingagenda.Meeting_Date__c=system.today().adddays(10);
        meetingagenda.Meeting_Start3_Time__c ='8:00:00.000';
        meetingagenda.Meeting_End_Time2__c='8:15:00.000';
        insert meetingagenda;
        
        CSA_Meeting_Members__c meetingmembers = new CSA_Meeting_Members__c();
        meetingmembers.Contact__c=con.id;
        meetingmembers.CSA_Meeting_Agenda__c= meetingagenda.id;
        //meetingmembers.CSA_Team__c=csateam.id;
        insert meetingmembers;
    }  
    public static testmethod void test1(){
        list<contact> conlist=[select id,firstname,lastname,name,email from contact ];
        list<CSA_Meeting_Agenda__c> agenda=[select id,Location_of_Meeting__c,Meeting_Status__c,name,Meeting_Date__c,Meeting_Start3_Time__c,
                                           Meeting_End_Time2__c from CSA_Meeting_Agenda__c];
        list<CSA_Meeting_Members__c> members=[select id,contact__c,CSA_Meeting_Agenda__c from CSA_Meeting_Members__c];
        agenda[0].Meeting_Status__c='Cancelled';
        update agenda[0];
        
    }
    
    public static testmethod void test2(){
        list<contact> conlist=[select id,firstname,lastname,name,email from contact ];
        list<CSA_Meeting_Agenda__c> agenda=[select id,Location_of_Meeting__c,Meeting_Status__c,name,Meeting_Date__c,Meeting_Start3_Time__c,
                                           Send_Notification__c,Meeting_End_Time2__c from CSA_Meeting_Agenda__c];
        list<CSA_Meeting_Members__c> members=[select id,contact__c,CSA_Meeting_Agenda__c from CSA_Meeting_Members__c];
        agenda[0].Send_Notification__c=true;
        update agenda[0];
        
    }
}