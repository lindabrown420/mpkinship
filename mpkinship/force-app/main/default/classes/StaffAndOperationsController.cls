public class StaffAndOperationsController {
    
    @AuraEnabled(continuation = true)
    Public static List<Payment__c>  insertStaffAndOperation(String uploadIds,String base64,String filename,String invoiceNumber, String vendor, String paymentMethod, String parentfiscalyearid,String paymentAmount, String localityCode, String invoiceStage, String fund, String selectedCate, String recordTypeIdStr,String recordId,String description,String opportunity){
        List<String> listUploadIds = new List<String>();
        if(uploadIds != null && uploadIds != ''){
            if(uploadIds.contains(',')){
                listUploadIds = uploadIds.split(',');
            }
            else{
                listUploadIds.add(uploadIds);
            }
        }
        List<String> listSelectCat = selectedCate.split(',');
        Payment__c opp = new Payment__c();
        List<Payment__c> oppInsertList = new List<Payment__c>();
        List<Payment__c> oppUpdateList = new List<Payment__c>();
        for(String s : listSelectCat){
            opp = new Payment__c();
            opp.Opportunity__c = opportunity;
            opp.Invoice_Number__c = invoiceNumber;
            opp.Vendor__c = vendor; 
            opp.Payment_Method__c = paymentMethod; 
            opp.Invoice_Stage__c = invoiceStage;
            opp.Accounting_Period__c = parentfiscalyearid;                                                                   
            opp.Payment_Amount__c = Decimal.valueof(paymentAmount);
            opp.Fund__c = fund; 
            opp.Category_lookup__c = s; 
            opp.Description__c = description;
            if(recordId != null  && recordId != ''){
                opp.Id = recordId;
                oppUpdateList.add(opp);
            }
            else{
                opp.RecordTypeId = recordTypeIdStr;
                oppInsertList.add(opp);
            }
            
        }
        if(oppUpdateList.size() >0){
            try{
                update oppUpdateList;
                return oppUpdateList;
            }catch(exception e){
                System.debug(e.getMessage());
            }
        }
        Set<Id> oppids = new Set<Id>();
        Set<Id> cdlids = new Set<Id>();
        if(oppInsertList.size() >0){
            try{
                insert oppInsertList;
                for(Payment__c opp1 : oppInsertList){
                    oppids.add(opp1.Id);
                }
                ContentDocumentLink cdl = new ContentDocumentLink();
                List<ContentDocumentLink> cdlList = new List<ContentDocumentLink>();
                List<ContentVersion> conList = new List<ContentVersion>();
                if(listUploadIds.size()>0){
                    conList = [SELECT ContentDocumentId FROM ContentVersion WHERE Id IN: listUploadIds];
                    for(ContentVersion con :conList){
                        cdlids.add(con.ContentDocumentId);
                    }
                    for(Id ids:oppids){
                        for(Id idss:cdlids){
                            cdl = new ContentDocumentLink();
                            cdl.LinkedEntityId = ids;
                            cdl.ContentDocumentId = idss;
                            cdl.ShareType = 'V';
                            cdlList.add(cdl);
                        }
                    }
                    try {
                        if(cdlList.size()>0){
                            insert cdlList;   
                        }
                        
                    } catch(DMLException e) {
                        System.debug(e);
                        return null;
                    }
                }
                
                                
                return oppInsertList;
            }catch(exception e){
                System.debug(e.getMessage());
            }
        }
        
        return oppInsertList;
    } 
    @AuraEnabled(continuation = true)
    Public static List<Payment__c> getInvoices(ID OppId){
        List<Payment__c> tempList = [Select Id,Invoice_Stage__c, Record_view__c, Invoice_Number__c, Payment_Date__c, Name,Fiscal_Year_Name__c,Vendor_Name__c, Description__c, 
                Payment_Amount__c, Accounting_Period__c,Vendor__c,Category_Name__c,Payment_Method__c ,Accounting_Period__r.IsActive, Residental__c,Locality_Account_Codes__c, Category_lookup__r.RecordType.Name
                From Payment__c Where Opportunity__c =: OppId
               AND Recordtype.Name = 'Staff & Operations' AND Accounting_Period__r.IsActive = true];
        for(Payment__c item: tempList){
            item.Residental__c = item.Category_lookup__r.Recordtype.Name;
        }
        return tempList;
    } 
    
    @AuraEnabled(continuation = true)
    Public static Payment__c getInvoiceRecord(ID InvoiceId){
        return [Select Id, Name, Fiscal_Year_Name__c,Payment_Date__c, Vendor_Name__c, Category_lookup__c, Description__c, fund__c,
                Payment_Amount__c, Locality_Account_Codes__c, Accounting_Period__c,Vendor__c,Category_Name__c,Payment_Method__c 
                From Payment__c Where Opportunity__c =: InvoiceId limit 1];
    } 
    
    @AuraEnabled(continuation = true)
    Public static Payment__c getInvoiceId(ID InvoiceId){
        return [Select Id, Name,Invoice_Number__c,Invoice_Stage__c,Scheduled_Date__c,Cost_Center__c,Cost_Center__r.Name,Fiscal_Year_Name__c,fund__c, Vendor_Name__c, Category_lookup__c, Category_lookup__r.Name, Description__c, Payment_Date__c,
                Payment_Amount__c, Locality_Account_Codes__c, Accounting_Period__c,Vendor__c,Category_Name__c,Payment_Method__c, PO_Parent_Fiscal_Year__c
                From Payment__c Where Id =: InvoiceId];
    }
        @AuraEnabled(continuation = true)
    Public static List<FileInfoWrapper> getFileName(ID InvoiceId){
        FileInfoWrapper fiw = new FileInfoWrapper();
        List<FileInfoWrapper> listFIW = new List<FileInfoWrapper>();
        List<ContentDocumentLink> listCDI = new List<ContentDocumentLink>();
        listCDI = [Select Id,ContentDocument.Id,ContentDocument.title,ContentDocument.FileExtension,ContentDocument.Owner.Name,ContentDocument.Owner.Id  From ContentDocumentLink Where LinkedEntityId =: InvoiceId];
        for(ContentDocumentLink cdi : listCDI){
            fiw = new FileInfoWrapper();
            fiw.title = String.valueof(cdi.ContentDocument.title)+'.'+String.valueof(cdi.ContentDocument.FileExtension);
            fiw.owner = String.valueof(cdi.ContentDocument.Owner.Name);
            fiw.titleLink ='/lightning/r/ContentDocument/'+String.valueof(cdi.ContentDocument.Id)+'/view';
            fiw.ownerLink ='/lightning/r/User/'+String.valueof(cdi.ContentDocument.Owner.Id)+'/view';
            listFIW.add(fiw);
        }
       return listFIW;
    }
     @AuraEnabled(continuation = true)
    Public static Opportunity getOpportunityRecord(ID OppId){
        return [Select Id, Name,  CampaignId, CloseDate, Voucher_Number__c, Accounting_Period__c, StageName from Opportunity
                where id =: OppId];
    }
    @AuraEnabled(continuation = true)
    Public static Opportunity getOpportunityRecordByComp(ID campId){
        return [Select Id, Name,  CampaignId, CloseDate, Voucher_Number__c, StageName from Opportunity
                where Accounting_Period__c =: campId Limit 1];
    } 
    
    @AuraEnabled(continuation = true)
    Public static string deleteInvoice(ID InvoiceID){
        try{
            Payment__c r = new Payment__c(); 
            r.Id = InvoiceID;
            delete r;
            return 'true';
        }catch(exception e){
            return e.getMessage();
        }
        
    }
    @AuraEnabled(continuation = true)
    Public static string deleteAllInvoice(List<ID> InvoiceIDs){
        try{
            List<Payment__c> dltInv = new List<Payment__c>(); 
            for(Id iId: InvoiceIDs){
                dltInv.add(new Payment__c(Id = iId));
            }
            delete dltInv;
            return 'true';
        }catch(exception e){
            return e.getMessage();
        }
        
    }
    public class InvoiceClass{
        @AuraEnabled public string Id = '';
        @AuraEnabled public string Name = '';
        @AuraEnabled public string Recordview = null;
        @AuraEnabled public string InvoiceNumber = '';
        @AuraEnabled public string kvendor = '';
        @AuraEnabled public string Description = '';
        @AuraEnabled public string Category = '';
        @AuraEnabled public string PaymentMethod = '';
        @AuraEnabled public string Stage = '';
        @AuraEnabled public Decimal Amount = 0.0;
        @AuraEnabled public string  localityCode= '';
        @AuraEnabled public List<AllocationClass> Allocations;
        public InvoiceClass(){
            Allocations =  new List<AllocationClass>();
        }        
    }
    
    public class AllocationClass{
        @AuraEnabled public string Id = '';
        @AuraEnabled public string Name = '';
        @AuraEnabled public string GAUID = '';
        @AuraEnabled public string GAUName = '';
        @AuraEnabled public Decimal Amount = 0.0;        
    }
    /*@AuraEnabled(continuation = true)
    Public static boolean isGUAAllocationCreated(String invoiceId){
        try{
            List<npsp__Allocation__c> Allocations = [Select id from npsp__Allocation__c Where
                                            npsp__Payment__c = : invoiceId];
            if(Allocations.size() > 0){
                return true;
            }
        }catch(Exception e){
            return false;     
        }
        return false;
    }*/

    
    public class PaymentClass{
        @AuraEnabled
        public id localityaccountid {get;set;}
        @AuraEnabled
        public string localityaccountName {get;set;}
        @AuraEnabled
        public List<InvoiceClass> payments {get;set;}

        public PaymentClass(){
            payments = new List<InvoiceClass>();
        }
    }

    @AuraEnabled(continuation = true)
    Public static List<PaymentClass> getPaymentInvoices(ID OppId){
        Set<Id>  LocalityAccountIds = new Set<Id>();
        List<AggregateResult> AggList =  [SELECT Category_lookup__r.Locality_Account__r.Id accid, sum(Payment_Amount__c) amt
                                           FROM Payment__c where Category_lookup__r.Locality_Account__r.Name <> null
                                           AND  Opportunity__c =: OppId AND (Recordtype.Name = 'Staff & Operations' OR Recordtype.Name = 'Adjustments')
                                           GROUP BY Category_lookup__r.Locality_Account__r.Id ];
        for(AggregateResult rec: AggList){
            //re.get('amt');
            LocalityAccountIds.add((string)rec.get('accid'));
        }

        List<Payment__c> Invoices =  [Select 
                                                 Id, Invoice_Stage__c,Invoice_Number__c, Record_view__c, Vendor__r.Name, Payment_Date__c, Name,Vendor_Name__c, Description__c,Locality_Account_Codes__c,
                                            Payment_Amount__c,Vendor__c,Category_Name__c,Payment_Method__c , Category_lookup__r.RecordType.Name, Category_lookup__r.Locality_Account__r.Id ,Category_lookup__r.Locality_Account__r.Name
                                            From Payment__c 
                                                Where Opportunity__c =: OppId AND (Recordtype.Name = 'Staff & Operations' OR Recordtype.Name = 'Adjustments')];
        
        PaymentClass  payment = new   PaymentClass();
        List<PaymentClass>  Allpayments = new  List<PaymentClass>();
        
        InvoiceClass Invoice = new InvoiceClass();
        List<InvoiceClass> InvoiceList = new List<InvoiceClass>(); 


        for(Id AccId : LocalityAccountIds){
            payment = new  PaymentClass(); 
            InvoiceList = new List<InvoiceClass>(); 
            payment.localityaccountid = AccId;
            //
            for(Payment__c vr : Invoices){
                                      
                if(vr.Category_lookup__r.Locality_Account__r.Id == AccId){
                        Invoice = new InvoiceClass();
                        payment.localityaccountName = vr.Locality_Account_Codes__c;
                        system.debug('(vr.Category_lookup__r.Locality_Account__r.Id>>>>>' + vr.Category_lookup__r.Locality_Account__r.Id);
                        system.debug('AccId>>>>>' + AccId);
                        system.debug('invoiceFill>>>>>');
                        Invoice.Id = vr.Id;
                        Invoice.kvendor = vr.Vendor__r.Name;
                        Invoice.Name = vr.Name;
                        Invoice.Amount = vr.Payment_Amount__c;
                        Invoice.Description = vr.Description__c; 
                        Invoice.Category = vr.Category_Name__c;
                        Invoice.PaymentMethod = vr.Payment_Method__c;
                        Invoice.Stage = vr.Invoice_Stage__c;
                        Invoice.Recordview = vr.Record_view__c;
                        Invoice.InvoiceNumber = vr.Invoice_Number__c;
                        Invoice.localityCode = vr.Locality_Account_Codes__c;
                        //Invoice.Allocations = AllocMap.get(Invoice.ID);                
                        InvoiceList.add(Invoice);
                            
                        system.debug('InvoiceList>>>>>' + InvoiceList);                    
                }     
                         
            } 
              
               
               
                //InvoiceList.clear(); 
                payment.payments = InvoiceList;
                Allpayments.add(payment);
        }
        return Allpayments;
    }
    @AuraEnabled(continuation = true)
    Public static List<InvoiceClass> getInvoicesList(ID OppId){
        system.debug('Called ');

        

        List<Payment__c> Invoices =  [Select 
                                                 Id, Invoice_Stage__c,Invoice_Number__c, Record_view__c, Vendor__r.Name, Payment_Date__c, Name,Vendor_Name__c, Description__c,Locality_Account_Codes__c,
                                            Payment_Amount__c,Vendor__c,Category_Name__c,Payment_Method__c , Category_lookup__r.RecordType.Name, Category_lookup__r.Locality_Account__r.Id
                                            From Payment__c 
                                                Where Opportunity__c =: OppId AND (Recordtype.Name = 'Staff & Operations' OR Recordtype.Name = 'Adjustments')];
        
        List<Id> InvoiceIds = new List<Id>();
        for(Payment__c inv: Invoices){
            InvoiceIds.add(inv.id);    
        }
        
        /*List<npsp__Allocation__c> Allocations = [Select id, npsp__General_Accounting_Unit__c, npsp__Amount__c, GUA_Name__c, Name,
                                                    npsp__Percent__c, npsp__Payment__c from npsp__Allocation__c Where
                                        npsp__Payment__c IN: InvoiceIds];*/
        List<InvoiceClass> InvoiceList = new List<InvoiceClass>(); 
        
        system.debug('Invoices ????'+ Invoices.size());
        //system.debug('Allocations ??? '+ Allocations.size());
        
        Map<ID, List<AllocationClass>> AllocMap = new Map<ID, List<AllocationClass>>();
        InvoiceClass Invoice = new InvoiceClass();
        
        
        List<AllocationClass> AllocationList = new List<AllocationClass>();
        AllocationClass RC = new AllocationClass();
       /* for(npsp__Allocation__c Alloc: Allocations){
          List<AllocationClass> tempList = AllocMap.get(Alloc.npsp__Payment__c); 
            if(tempList == null){
                tempList = new List<AllocationClass>(); 
            }
            
            RC.id = Alloc.Id;
            RC.Name = Alloc.Name;
            RC.GAUID = Alloc.npsp__General_Accounting_Unit__c;
            RC.GAUName = Alloc.GUA_Name__c;
            RC.Amount = Alloc.npsp__Amount__c;
            tempList.add(RC);
            AllocMap.put(Alloc.npsp__Payment__c, tempList);
            RC = new AllocationClass();    
            
        }*/
        
        for(Payment__c vr: Invoices){           
            Invoice.Id = vr.Id;
            Invoice.kvendor = vr.Vendor__r.Name;
            Invoice.Name = vr.Name;
            Invoice.Amount = vr.Payment_Amount__c;
            Invoice.Description = vr.Description__c; 
            Invoice.Category = vr.Category_Name__c;
            Invoice.PaymentMethod = vr.Payment_Method__c;
            Invoice.Stage = vr.Invoice_Stage__c;
            Invoice.Recordview = vr.Record_view__c;
            Invoice.InvoiceNumber = vr.Invoice_Number__c;
            Invoice.localityCode = vr.Locality_Account_Codes__c;
            //Invoice.Allocations = AllocMap.get(Invoice.ID);                
            InvoiceList.add(Invoice);
            Invoice = new InvoiceClass();                
        }
        
        return InvoiceList;

    }
   @AuraEnabled(continuation = true)
    public static void uploadFile(String recordId, String uploadIds ) {
        System.debug('recordId'+recordId);
         System.debug('uploadIds'+uploadIds);
        List<String> listUploadIds = new List<String>();
        if(uploadIds != null && uploadIds != ''){
            if(uploadIds.contains(',')){
                listUploadIds = uploadIds.split(',');
            }
            else{
                listUploadIds.add(uploadIds);
            }
        }
        ContentDocumentLink cdl = new ContentDocumentLink();
        List<ContentDocumentLink> cdlList = new List<ContentDocumentLink>();
        List<ContentVersion> conList = new List<ContentVersion>();
        if(listUploadIds.size()>0){
            conList = [SELECT ContentDocumentId FROM ContentVersion WHERE Id IN: listUploadIds];
            Set<Id> cdlids = new Set<Id>();
            for(ContentVersion con :conList){
                cdlids.add(con.ContentDocumentId);
            }
            for(Id ids:cdlids){
                cdl = new ContentDocumentLink();
                cdl.LinkedEntityId = recordId;
                cdl.ContentDocumentId = ids;
                cdl.ShareType = 'V';
                cdlList.add(cdl);
            }
            
            try {
                if(cdlList.size()>0){
                    insert cdlList;   
                }
                
            } catch(DMLException e) {
                System.debug(e);
            }
        }
        
    }
    public class FileInfoWrapper{
        @auraEnabled
        public string title;
        @auraEnabled
        public string owner;
        @auraEnabled
        public string titleLink;
        @auraEnabled
        public string ownerLink;
    }
    @AuraEnabled(continuation = true)
    public static Campaign getFIPS(ID parentfiscalyearid){
        return [Select id,FIPS__c from Campaign where Id =: parentfiscalyearid];
    }
}