@istest
public class PaymentBTriggerTest {
    @testSetup
    static void testRecords(){
        TriggerActivation__c  ta = new TriggerActivation__c();
        ta.name='PurchaseOrder Trigger';
        ta.isActive__c=false;
        insert ta;
        
        
         TriggerActivation__c  ta1 = new TriggerActivation__c();
        ta1.name='UpdateCSACoordinatorSign';
        ta1.isActive__c=false;
        insert ta1;   
        
        contact con = new contact();
        con.LastName='contactrec';
        con.FirstName='test';
        con.Email='test@contact.com';        
        insert con;   
        
        Kinship_Case__c caserec = new Kinship_Case__c();
        caserec.Primary_Contact__c=con.id;
        insert caserec;
        
        account accountrec = new account();
        accountrec.Name='test vendor';
        accountrec.Tax_ID__c='222-22-3333';
        accountrec.Entity_Type__c='Individual';
        insert accountrec;
         Locality_Account__c la = new Locality_Account__c();
        la.name='33';
        insert la;
        id catrecType = Schema.SObjectType.Category__c.getRecordTypeInfosByName().get('LASER Category').getRecordTypeId();
        
        Category__c category = new Category__c();
        category.Name='222';
        category.Category_Type__c='Adoption';
        category.RecordTypeId = catrecType;
        category.Locality_Account__c=la.id;
        insert category; 
        
        TriggerActivation__c  ta2 = new TriggerActivation__c ();
        ta2.name='PaymentTrigger';
        ta2.isActive__c=true;
        insert ta2;
        
        
        Bank_Account__c ba = new Bank_Account__c();
        ba.Name='test account';
        ba.Bank_Account_Type__c='Purchasing Card';
        ba.Bank_Name__c='test bank';
        insert ba;
        
        Bank_Statement__c bs = new Bank_Statement__c();
        bs.name='test statement';
        bs.Bank_Account__c =ba.Id;
        insert bs;
        
        id opprecType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Report of Collections').getRecordTypeId();
        opportunity opprecord = new opportunity();
        opprecord.CloseDate=date.today();
        opprecord.StageName='Draft';
        opprecord.Name='test opportunity';        
        opprecord.Amount=20000;
        opprecord.AccountId=accountrec.id;
        opprecord.RecordTypeId=opprecType;
        opprecord.End_Date__c = date.today();
        insert opprecord;
        Claims__c c = new Claims__c();
            c.Claim_Type__c = 'ADMINISTRATIVE';
            c.Claim_Amount__c = 30.0;
            c.Claim_Date__c = system.today();
            c.Comments__c = 'test';
            c.Amount_Received__c = 30;
            insert c;
           
            Special_Welfare_Account__c swa = new Special_Welfare_Account__c();
            swa.Fund_Name__c = 'test';        
            insert swa;
        
        id payrecType = Schema.SObjectType.Payment__c.getRecordTypeInfosByName().get('Vendor Invoice').getRecordTypeId();
        Payment__c payment = new Payment__c();
        payment.Opportunity__c=opprecord.id;
        payment.Payment_Amount__c=2000;
        payment.vendor__c = accountrec.Id;
        payment.RecordTypeId=payrecType;
        payment.Bank_Statement__c=bs.id;
        payment.Claims__c = c.id;
        payment.Special_Welfare_Account__c = SWA.Id;
        insert payment;
        


    }
    public static testmethod void PaymentwithBankStatementTest1(){
        
         account accountrec = new account();
        accountrec.Name='test vendor1';
        accountrec.Tax_ID__c='232-22-3333';
        accountrec.Entity_Type__c='Individual';
        insert accountrec;
        
        id opprecType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Report of Collections').getRecordTypeId();
        opportunity opprecord = new opportunity();
        opprecord.CloseDate=date.today();
        opprecord.StageName='Draft';
        opprecord.Name='test opportunity';        
        opprecord.Amount=20000;
        opprecord.AccountId=accountrec.id;
        opprecord.Beginning_Receipt__c = '100';
        opprecord.Ending_Receipt__c = '150';
        opprecord.RecordTypeId=opprecType;
        opprecord.End_Date__c = date.today() + 10;
        insert opprecord;
        
        
        id payrecType = Schema.SObjectType.Payment__c.getRecordTypeInfosByName().get('Vendor Invoice').getRecordTypeId();
        Payment__c payment = new Payment__c();
        payment.Opportunity__c=opprecord.id;
        payment.Payment_Amount__c=2000;
        payment.vendor__c = accountrec.Id;
        payment.RecordTypeId=payrecType;
        payment.Prior_Refund1__c = 20;
        insert payment;
        
        
        id refundrecType = Schema.SObjectType.Payment__c.getRecordTypeInfosByName().get('Vendor Refund').getRecordTypeId();
        Payment__c Refund = new Payment__c();
        Refund.Opportunity__c=opprecord.id;
        Refund.Payment_Amount__c=2000;
        refund.Payment_Date__c = date.today();
        refund.Report_Number__c = '101';
        Refund.vendor__c = accountrec.Id;
        Refund.RecordTypeId=refundrecType;
        Refund.payment__c = payment.Id;
        insert Refund;
        
        Delete payment;
    }
    public static testmethod void PaymentwithBankStatementTest(){
       list<TriggerActivation__c> talist=[select id,name,isActive__C from TriggerActivation__c where isActive__c=:true 
                                           and name=:'PaymentTrigger'];
        list<TriggerActivation__c> talist1=[select id,name,isActive__C from TriggerActivation__c where isActive__c=:true 
                                            and name=:'PurchaseOrder Trigger'];   
        list<Kinship_Case__c> cases=[select id from Kinship_Case__c];
        list<account> accounts=[select id,recordtypeid from account where name='test vendor'];        
        list<Category__c> categories=[select id from Category__c];
        
        list<Bank_Account__c> bankAccount =[select id,name from Bank_Account__c];
        list<Bank_Statement__c> bstatement=[select id,name,bank_Account__C from Bank_Statement__c where 
                                            bank_Account__C=:bankAccount[0].id];
        
        
        
        Product2 p = new product2(name='x',isactive=true);
        p.Unit_Measure__c = '1';
        insert p;
        
        Product2 p2 = new product2(name='y',isactive=true);
        p2.Unit_Measure__c = '1';
        insert p2;
        
        //define the standart price for the product
        id stdPb = Test.getStandardPricebookId();
        insert new PricebookEntry(pricebook2id = stdPb, product2id = p.id,unitprice=1.0, isActive=true);
        
        Pricebook2 pb = new pricebook2(name='test',Vendor__c=accounts[0].id,IsActive=true,
                                       Effective_Date__c=system.today().addmonths(-2));
        pb.Accounting_Period__c = stdPb;
        //insert pb;
        
        //PricebookEntry pbe = new PricebookEntry(pricebook2id=pb.id, product2id=p.id,unitprice=1.0, isActive=true);
        //insert pbe; 
        opportunity opprecord = new opportunity();
        opprecord.CloseDate=date.today();
        opprecord.StageName='Draft';
        opprecord.Name='test opportunity';
        opprecord.Kinship_Case__c=cases[0].id;
        opprecord.Amount=20000;
        opprecord.Category__c =categories[0].id;
        opprecord.AccountId=accounts[0].id;
        opprecord.Number_of_Months_Units__c=3;
        opprecord.Pricebook2Id =pb.id;        
        insert opprecord;
        
        
        //NOW CREATING LINE ITEMS
        opportunitylineitem opplineitem = new opportunitylineitem();
        opplineitem.Unit_Measure__c='2';
        opplineitem.Product2Id=p.id;
        opplineitem.PricebookEntryId=pb.id;
        opplineitem.Quantity=2;
        opplineitem.OpportunityId=opprecord.id;
        opplineitem.TotalPrice = 2000;
        //insert opplineitem;
        //Create an approval request for the po
        test.startTest();
        Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitrequest();
        app.setObjectId(opprecord.Id);    
         // Submit the approval request for the po    
        Approval.ProcessResult result = Approval.process(app);    
        // Verify that the results are as expected    
        System.assert(result.isSuccess());
           Claims__c c = new Claims__c();
            c.Claim_Type__c = 'ADMINISTRATIVE';
            c.Claim_Amount__c = 30.0;
            c.Claim_Date__c = system.today();
            c.Comments__c = 'test';
            c.Amount_Received__c = 30;
            insert c;
           
            Special_Welfare_Account__c swa = new Special_Welfare_Account__c();
            swa.Fund_Name__c = 'test';        
            insert swa;
        
        id payrecType = Schema.SObjectType.Payment__c.getRecordTypeInfosByName().get('Vendor Invoice').getRecordTypeId();
        Payment__c payment = new Payment__c();
        payment.Opportunity__c=opprecord.id;
        payment.Payment_Amount__c=2000;
        payment.vendor__c = accounts[0].id;
        payment.RecordTypeId=payrecType;
        payment.Special_Welfare_Account__c = swa.Id;
        payment.Bank_Statement__c=bstatement[0].id;
        payment.Claims__c = c.id;
        insert payment;
        
        if(result.isSuccess()){
         
            opprecord.stagename='Approved';           
            update opprecord;
           test.stopTest();
            list<Payment__c> payments=[select id,Written_Off__c,Bank_Statement__c,Paid__c from Payment__c
                                                    where Opportunity__c=:opprecord.Id limit 1];
            payments[0].Bank_Statement__c=bstatement[0].id;
            payments[0].Claims__c = c.id;
            payments[0].Special_Welfare_Account__c = SWA.Id;
            update payments[0];

        }    
    }   
   
  
}