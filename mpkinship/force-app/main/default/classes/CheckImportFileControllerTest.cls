@isTest
public class CheckImportFileControllerTest {
	
     public static testmethod void testRecords(){
        CheckImportFileController cifc = new CheckImportFileController();
        List<CheckImportFileController.CheckListWrapper> clwList = new List<CheckImportFileController.CheckListWrapper>();
        CheckImportFileController.CheckListWrapper clw = new CheckImportFileController.CheckListWrapper(); 
        clw.acct = 'test';
        clw.amount = '123';
        clw.cashAcct = '123';
        clw.checkNumber = '123';
        clw.dateIssued = '12/27/2009';
        clw.description = '123';
        clw.vendor = '123';
        clwList.add(clw); 
         
        CheckImportFileController.insertCheckList(clwList);
    }
    
}