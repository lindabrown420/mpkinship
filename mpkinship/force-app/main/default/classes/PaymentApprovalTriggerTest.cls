@isTest
public class PaymentApprovalTriggerTest {
    @TestSetup
    static void initData(){
        contact con = new contact();
        con.LastName='contactrec';
        con.FirstName='test';
        con.Email='test@contact.com';
        insert con;
        
        Kinship_Case__c caserec = new Kinship_Case__c();
        caserec.Primary_Contact__c=con.id;
        insert caserec;
        id vendorecordtypeid=Schema.SObjectType.account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        Account acc = new Account(Name = 'test acc', Tax_ID__c = '123-56-7901', Entity_Type__c = 'Individual',RecordTypeId =vendorecordtypeid );
        insert acc;
        Locality_Account__c la = new Locality_Account__c();
        la.Name='222';
        insert la;
        
        Category__c category = new Category__c();
        category.Name='222';
        category.Category_Type__c='Administrative';
        category.Locality_Account__c=la.id;
        category.RecordTypeId = Schema.SObjectType.Category__c.getRecordTypeInfosByName().get('LASER Category').getRecordTypeId();
        insert category;

        TriggerActivation__c ta = new TriggerActivation__c(Name = 'PaymentApprovalTrigger', isActive__c = true);
        insert ta;
    }
    @isTest
    private static void TestProcessDdpExt(){
        list<TriggerActivation__c> talist=[select id,name,isActive__C from TriggerActivation__c where isActive__c=:true];
        list<Kinship_Case__c> cases=[select id from Kinship_Case__c];
               id vendorecordtypeid=Schema.SObjectType.account.getRecordTypeInfosByDeveloperName().get('Vendor').getRecordTypeId();
        Account acc1 = new Account(Name = 'test acc1', Tax_ID__c = '113-56-7901', Entity_Type__c = 'Individual',recordtypeid=vendorecordtypeid);
        insert acc1;
        list<Category__c> categories=[select id from Category__c];
        opportunity opprecord = new opportunity();
        opprecord.CloseDate=date.today();
        opprecord.StageName='Approved';
        opprecord.Name='test opportunity';
        opprecord.Kinship_Case__c=cases[0].id;
        opprecord.Amount=20000;
        opprecord.Category__c =categories[0].id;
        opprecord.AccountId=acc1.id;
        opprecord.Number_of_Months_Units__c=2;
        insert opprecord;
        opprecord.StageName='Approved';
        update opprecord;
        Payment__c payment = new Payment__c(Opportunity__c = opprecord.id, 
                                            Scheduled_Date__c = Date.today(), 
                                            Status__c = 'Submitted For Approval',
                                            Invoice_Stage__c = 'Ready to Pay',
                                            Written_Off__c = false,
                                            Payment_Method__c = 'Check',
                                            Category_lookup__c = categories[0].id,
                                            Payment_Amount__c = 100,
                                           	RecordTypeId = Schema.SObjectType.Payment__c.getRecordTypeInfosByName().get('Vendor Invoice').getRecordTypeId());
		insert payment;
        Payment_Approval__c pa = new Payment_Approval__c();
        pa.Status__c = 'Created';
        insert pa;

        Payment_Approval_Step__c pas = new Payment_Approval_Step__c();
        pas.Invoice__c = payment.id;
        pas.Payment_Approval__c = pa.id;
        insert pas;

        Test.startTest();
        pa.Status__c = 'Approved';
        update pa;
        Test.stopTest();

        Payment__c p = [SELECT id, status__c FROM Payment__c WHERE id = :payment.id];
        System.assertEquals('Approved', p.Status__c);
    }
}