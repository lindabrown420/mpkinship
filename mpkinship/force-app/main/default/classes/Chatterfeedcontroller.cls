public with sharing class Chatterfeedcontroller {

    public String RecordId { get; set; }
    public Chatterfeedcontroller(){
        if(ApexPages.currentPage().getParameters().get('RecordId') != null){
            RecordId = ApexPages.currentPage().getParameters().get('RecordId');
        }else{
            RecordId = 'a1M180000022Q0vEAE';
        }
    }
}