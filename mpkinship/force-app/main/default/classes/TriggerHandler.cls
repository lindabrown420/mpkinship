public with sharing virtual class TriggerHandler implements ITriggerHandler {

    /**
     * Called prior to BEFORE trigger. Use to cache any data required
     * input maps prior to execution of the BEFORE trigger
     */
    public virtual void bulkBefore() {}

    /**
     * Called prior to AFTER trigger. Use to cache any data required
     * input maps prior to execution of the AFTER trigger
     */
    public virtual void bulkAfter() {}

    /**
     * Handles before insert operations for all records being inserted.
     * Should only be used in BEFORE Trigger context
     */
    public virtual void beforeInsert() {}

    /**
     * Handles after insert operations for all records being inserted.
     * Should only be used in AFTER Trigger context
     */
    public virtual void afterInsert() {
    }

    /**
     * Handles before update operations for all records being updated.
     * Should only be used in BEFORE Trigger context
     */
    public virtual void beforeUpdate() {}

    /**
     * Handles after update operations for all records being updated.
     * Should only be used in AFTER Trigger context
     */
    public virtual void afterUpdate() {
    }

    /**
     * Handles before delete operations for all records being deleted.
     * Should only be used in BEFORE Trigger context
     */
    public virtual void beforeDelete() {}

    /**
     * Handles after delete operations for all records being deleted.
     * Should only be used in AFTER Trigger context
     */
    public virtual void afterDelete() {}

    /**
     * Call once all records have been processed by the trigger handler.
     * Use this to execute all final operations like DML of other records.
     */
    public virtual void andFinally() {
        //upsert or insert records
        this.insertHistory();
    }

    /**
     * Called to check the Changed of Fields in Trigger.
     */
    public void insertHistory(){
        if(Trigger.isAfter){
            List<SObject> newList = Trigger.new;
            List<SObject> oldList = Trigger.old;
            //Map<Id, SObject> oldMap = Trigger.oldMap;
            if(Trigger.isInsert) {
                for(sObject obj : newList) {
                    KinshipHistoryServices.insertHistory(obj.Id, obj, '', '', KinshipHistoryServices.STR_NEW, '');
                }
            } else if(Trigger.isDelete) {
                for(sObject obj : oldList) {
                    KinshipHistoryServices.insertHistory(obj.Id, obj, '', '', KinshipHistoryServices.STR_DELETE, '');
                }
            } else if(Trigger.isUpdate) {
                List<Schema.DescribeFieldResult> fieldToTrack = KinshipHistoryServices.getFieldToTrack(newList[0]);
                String strNew, strOld;
                system.debug('--TriggerHandler--' + fieldToTrack.size());
                for(Integer i = 0 ; i < newList.size() ; i++) {
                    for(Schema.DescribeFieldResult field : fieldToTrack) {
                        system.debug('--TriggerHandler--' + field.getName());
                        strNew = parseString(newList[i], field.getName());
                        strOld = parseString(oldList[i], field.getName());
                        if(strNew != strOld) {
                            KinshipHistoryServices.insertHistory(newList[i].Id, newList[i], strNew, strOld, KinshipHistoryServices.STR_UPDATE, field.getLabel());
                        }
                    }
                }
            }
            system.debug('--TriggerHandler end--');
            KinshipHistoryServices.flushHistory();
        }
    }

    public String parseString(sObject s, String fieldName) {
        Object value;
        try{
            value = s.get(fieldName);
        }catch(Exception e){}
        if(value == null) return '';
        return String.valueOf(value);
    } 


}