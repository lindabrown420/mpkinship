@istest
public class WriteOffPaymentCtrTest {
	@testSetup
    static void testecords(){
          TriggerActivation__c  ta = new TriggerActivation__c();
        ta.name='PurchaseOrder Trigger';
        ta.isActive__c=true;
        insert ta;
        TriggerActivation__c  ta1 = new TriggerActivation__c();
        ta1.name='PaymentTrigger';
        ta1.isActive__c=true;
        insert ta1;
        contact con = new contact();
        con.LastName='contactrec';
        con.FirstName='test';
        con.Email='test@contact.com';
        insert con;
        
        Kinship_Case__c caserec = new Kinship_Case__c();
        caserec.Primary_Contact__c=con.id;
        caserec.DOB__c=system.today().adddays(-100);
        caserec.Race__c='1';
        caserec.Gender_Preference__c='M';
        caserec.Hispanic_Ethnicity__c='1';
        caserec.DSM_V_Indicator_ICD_10_1__c='1';
        caserec.Clinical_Medication_Indicator_1__c='1';
        caserec.Medicaid_Indicator_1__c='1';
        caserec.Referral_Source__c='1';
        caserec.Autism_Flag_1__c='1';
        insert caserec;
       
        account accountrec = new account();
        accountrec.Name='test vendor';
        accountrec.Tax_ID__c='333-44-3333';
        accountrec.Entity_Type__c='Individual';
        insert accountrec;
        Locality_Account__c la = new Locality_Account__c();
        la.Name='222';
        insert la;
        Category__c category = new Category__c();
        category.Name='222';
        category.Locality_Account__c=la.id;
        category.Category_Type__c='Adoption';
         category.RecordTypeId= Schema.SObjectType.category__C.getRecordTypeInfosByDeveloperName().get('LASER_Category').getRecordTypeId();
        insert category;
         FIPS__c fp = new FIPS__c();
        fp.name='test fips';
        insert fp;
        id recTypeid = Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Fiscal_Year').getRecordTypeId();
        campaign cap = new campaign();
        cap.FIPS__c=fp.id;
        cap.RecordTypeId=recTypeid;
        cap.Fiscal_Year_Type__c='Local';
        cap.name='test';
        cap.Begin_Date__c=system.today().addmonths(-1);
        cap.EndDate=system.today().addyears(1);
        insert cap;
        
    }    
    
     public static testmethod void test11(){
        list<TriggerActivation__c> talist =[select id,name,isactive__C from TriggerActivation__c where name='PurchaseOrder Trigger'];
        list<TriggerActivation__c> tapaylist =[select id,name,isactive__C from TriggerActivation__c where name='PaymentTrigger'];
        
        list<Kinship_Case__c> cases=[select id from Kinship_Case__c];
        list<account> accounts=[select id,recordtypeid from account where name='test vendor'];
     
        list<Category__c> categories=[select id from Category__c];
        //id opprecType = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Case_Action').getRecordTypeId();
            Product2 p = new product2(name='x',isactive=true,Unit_Measure__c='2');
            insert p;
            
            Product2 p2 = new product2(name='y',isactive=true,Unit_Measure__c='1');
            insert p2;
            
            //define the standart price for the product
            id stdPb = Test.getStandardPricebookId();
          list<fips__C> fip=[select id,name from fips__C];
        list<campaign> camp=[select id,fips__c,Begin_Date__c,enddate,Fiscal_Year_Type__c from campaign];
            insert new PricebookEntry(pricebook2id = stdPb, product2id = p.id,unitprice=1.0, isActive=true);
            insert new PricebookEntry(pricebook2id = stdPb, product2id = p2.id,unitprice=1.0, isActive=true);
            Pricebook2 pb = new pricebook2(name='test',Vendor__c=accounts[0].id,IsActive=true,
                                      Accounting_Period__c=camp[0].id);
            insert pb;
        
            PricebookEntry pbe = new PricebookEntry(pricebook2id=pb.id, product2id=p.id,unitprice=100.0, isActive=true);
            insert pbe; 
           PricebookEntry pbe1 = new PricebookEntry(pricebook2id=pb.id, product2id=p2.id,unitprice=1000.0, isActive=true);
            insert pbe1; 
   		test.startTest();
        opportunity opprecord = new opportunity();
        opprecord.CloseDate=date.today();
        opprecord.StageName='Draft';
        opprecord.Name='test opportunity';
        opprecord.Kinship_Case__c=cases[0].id;
        opprecord.Amount=20000;
        opprecord.AccountId=accounts[0].id;
        opprecord.Number_of_Months_Units__c=2;
        insert opprecord;   
         //NOW CREATING LINE ITEMS
            opportunitylineitem opplineitem = new opportunitylineitem();
            opplineitem.Unit_Measure__c='1';
            opplineitem.Product2Id=p2.id;
            opplineitem.PricebookEntryId=pbe1.id;
            opplineitem.Category__c=categories[0].id;
            opplineitem.Quantity=3;
            opplineitem.OpportunityId=opprecord.id;
            opplineitem.TotalPrice = 20000;
            insert opplineitem;
            
            opportunitylineitem opplineitem1 = new opportunitylineitem();
            opplineitem1.Unit_Measure__c='1';
            opplineitem1.Product2Id=p.id;
            opplineitem1.Category__c=categories[0].id;
            opplineitem1.PricebookEntryId=pbe.id;
            opplineitem1.Quantity=2;
            opplineitem1.OpportunityId=opprecord.id;
            opplineitem1.TotalPrice = 2000;
            insert opplineitem1;
        
            Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitrequest();
            app.setObjectId(opprecord.Id);    
             // Submit the approval request for the po    
            Approval.ProcessResult result = Approval.process(app);    
            // Verify that the results are as expected    
            System.assert(result.isSuccess());
            
            if(result.isSuccess()){
                opprecord.stagename='Approved';
                update opprecord;
              test.stopTest();       
                list<opportunity> opplist=[select id,stagename,Number_of_Months_Units__c from opportunity];
                system.debug('**VAl of opplist'+opplist);
                opprecord.stagename='Open';
                update opprecord;
                
             list<Payment__c> payments=[select id,name,Opportunity__c,Payment_Amount__c,Scheduled_Date__c,Amount_Paid__c,Paid__c,
                                              Written_Off__c,(select id,name,Previously_Billed__c,LineItem_Index__c,Remaining_Amount__c,
                                              Quantity_Billed__c,Unit_Price__c,Product__c,Unit_Measure__c,
                                               Max_Quantity_Authorized__c from Invoice_Line_Items__r) 
                                              from Payment__c
                                             where  Opportunity__c=:opprecord.Id];
             
                
              WriteOffPaymentCtr.WriteOffInvoice(payments[0].id); 
              WriteOffPaymentCtr.getInvoiceType(payments[0].id);
         }       
          
    }
    
    public static testmethod void test1(){
     	WriteOffPaymentCtr.WriteOffInvoice(null);
        
    }
    
}