public class CategoryTriggerHandler extends TriggerHandler{
	public static Boolean bypassTrigger = false;
    
    public override void beforeInsert() {
        if(CategoryTriggerHandler.bypassTrigger) return;
        List<Category__c> lstNew = (List<Category__c>)Trigger.new;
        Set<Id> setLA = new Set<Id>();
        for(Category__c ca : lstNew) {
            setLA.add(ca.Locality_Account__c);
        }
        Map<String, Chart_of_Account__c> mapCOA = new Map<String, Chart_of_Account__c>();
        for(Chart_of_Account__c coa : [SELECT Id, Name, Cost_Center__c, Locality_Account__c, FIPS__c FROM Chart_of_Account__c WHERE Locality_Account__c IN :setLA]){
            mapCOA.put(coa.Locality_Account__c, coa);
        }
        Chart_of_Account__c temp;
        for(Category__c ca : lstNew) {
            temp = mapCOA.get(ca.Locality_Account__c);
            if(temp != null) {
                ca.Status__c = 'Active';
                ca.Inactive__c = false;
            } else {
                ca.Status__c = 'Pending';
                ca.Inactive__c = true;
            }
        }
    }
    
    public override void beforeUpdate() {
        if(CategoryTriggerHandler.bypassTrigger) return;
        List<Category__c> lstNew = (List<Category__c>)Trigger.new;
        List<Category__c> lstOld = (List<Category__c>)Trigger.old;
        Set<Id> setLA = new Set<Id>();
        for(Integer i = 0 ; i < lstNew.size() ; i++) {
            if(lstNew[i].Locality_Account__c != lstOld[i].Locality_Account__c) {
            	setLA.add(lstNew[i].Locality_Account__c);
            }
        }
        Map<String, Chart_of_Account__c> mapCOA = new Map<String, Chart_of_Account__c>();
        for(Chart_of_Account__c coa : [SELECT Id, Name, Cost_Center__c, Locality_Account__c, FIPS__c FROM Chart_of_Account__c WHERE Locality_Account__c IN :setLA]){
            mapCOA.put(coa.Locality_Account__c, coa);
        }
        Chart_of_Account__c temp;
        for(Integer i = 0 ; i < lstNew.size() ; i++) {
            if(lstNew[i].Locality_Account__c != lstOld[i].Locality_Account__c) {
            	temp = mapCOA.get(lstNew[i].Locality_Account__c);
                if(temp != null) {
                    lstNew[i].Status__c = 'Active';
                    lstNew[i].Inactive__c = false;
                } else {
                    lstNew[i].Status__c = 'Pending';
                    lstNew[i].Inactive__c = true;
                }
            }
        }
    }
}