@isTest
public class UISettingControllerTest {
    public static testMethod void test(){
        Id profileId = userinfo.getProfileId();
        String profileName = [Select Id,Name from Profile where Id=:profileId].Name;
        Section_Setting__c ss = new Section_Setting__c();
        ss.Name = 'test';
        ss.Sort__c = 1;
        ss.Size__c = '1';
        ss.Background_Color__c = 'test';
        ss.Font_Color__c = 'test';
		ss.Visible_to__c = profileName;
        ss.Active__c = true;
        insert ss;
        
        Link_Setting__c ls = new Link_Setting__c();
        ls.Name = 'test';
        ls.Sort__c = 1;
        ls.Filter_ID__C = '123';
        ls.Custom_Tab_Name__c = 'test';
        ls.Action_Name__c = 'new';
        ls.Icon__c = true;
        ls.Icon_Name__c = 'test';
        ls.Link_Type__c = 'Navigation';
        ls.Navigation_Type__c = 'standard__objectPage';
        ls.Related_List_Name__c = 'test';
        ls.Object_Api_Name__c = 'test';
        ls.Record_ID__c = 'test';
        //ls.Record_Type__c = 'test';
        ls.URL__c = 'test';
        ls.Section_Setting__c = ss.Id;
        ls.Visible_to__c =profileName;
        ls.Active__c = true;
        insert ls;
        
        UISettingController uic = new UISettingController();
        UISettingController.getSectionDetail();
    }
    
}