public class CloseCABatch implements Database.Batchable<SObject>{
	
    public Database.QueryLocator start(Database.BatchableContext bc){
        String openstage='Open';
        String draftstage='draft';
        Id CARecTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Case Action').getRecordTypeId();
        Date todaydate=date.Today();

        return Database.getQueryLocator('Select Id, Name,StageName from Opportunity Where (StageName=\'Open\' OR StageName=\'Draft\') AND End_Date__c=:todaydate AND recordTypeId=:CARecTypeId');
    }
    public void execute(Database.BatchableContext BC, List<Opportunity> CAList) {
        system.debug('**LIST of ca'+CAList);
        for(Opportunity opp:CAList){
            opp.StageName='Closed';
        } 
        update CAList;
    } 
    public void finish(Database.BatchableContext BC) {
        // execute any post-processing operations
    }
}