//CATEGORY RATE CHANGE BATCH BASED ON AGE OF CHILD
public class CategoryRateChangeBatch implements Database.Batchable<sObject> {
    //BASED ON 1ST OF THE MONTH AFTER DATE OF BIRTH
    public Database.QueryLocator start(Database.BatchableContext BC) {
        integer todaymonth = date.today().month();
        integer oneMontheBeforeToday= date.today().addmonths(-1).month();        
        integer todayday=date.today().day();
        boolean Inactive=false;
        String query='';  
       
        Integer dayofmonth;
        dayofmonth= (Integer)Day_of_Month_For_Rates__c.getInstance().Day_of_Month__c;             
        //if(todayday==1){ 
          if(dayofmonth!=null && dayofmonth>0){   
              system.debug('***Entered in if first');
            query = 'SELECT Id,Name,DOB__c,Primary_Contact__c FROM Kinship_Case__c where DOB__c!=null and CALENDAR_MONTH(DOB__C)=:oneMontheBeforeToday and InActive__c=:Inactive';
            return Database.getQueryLocator(query);
        }      
         return Database.getQueryLocator('select ID from Kinship_Case__c  limit 0');
    }
    
    public void execute(Database.BatchableContext BC, List<Kinship_Case__c> cases) {
        system.debug('**VAl fo cases'+cases);
        if(cases.size()>0)
            CategoryRateChangeBatchHelper.FetchCases(cases);   
             
    }   
    
    public void finish(Database.BatchableContext BC) {
        // execute any post-processing operations
    }
}