@isTest
public class TaxRateTrgTest {

    public static  testmethod void test1(){
        try{
            TriggerActivation__c tra = new TriggerActivation__c();
            tra.Name='TaxRate';
            tra.isActive__c=true;
            insert tra;
       list<Tax_Rates__c> trlist = new list<Tax_Rates__c>();    
       Tax_Rates__c tr = new Tax_Rates__c();
        tr.RecordTypeId= Schema.SObjectType.Tax_Rates__c.getRecordTypeInfosByName().get('FICA').getRecordTypeId();
        tr.Active__c=true;
        tr.Employee_Rate__c=12;
        tr.Employer_Rate__c=20;
        tr.Type__c='Medicare';
        trlist.add(tr);
            
        Tax_Rates__c tr1 = new Tax_Rates__c();
        tr1.RecordTypeId= Schema.SObjectType.Tax_Rates__c.getRecordTypeInfosByName().get('FICA').getRecordTypeId();
        tr1.Active__c=true;
        tr1.Employee_Rate__c=120;
        tr1.Employer_Rate__c=20;
        tr1.Type__c='Medicare';
        trlist.add(tr1);
        insert trlist;    
        }
        catch(Exception e){}
    }
    
   public static testmethod void test2(){
           try{
            TriggerActivation__c tra = new TriggerActivation__c();
            tra.Name='TaxRate';
            tra.isActive__c=true;
            insert tra;
       Tax_Rates__c tr = new Tax_Rates__c();
        tr.RecordTypeId= Schema.SObjectType.Tax_Rates__c.getRecordTypeInfosByName().get('FICA').getRecordTypeId();
        tr.Active__c=true;
        tr.Employee_Rate__c=12;
        tr.Employer_Rate__c=20;
        tr.Type__c='Medicare';
        insert tr;
            
        Tax_Rates__c tr1 = new Tax_Rates__c();
        tr1.RecordTypeId= Schema.SObjectType.Tax_Rates__c.getRecordTypeInfosByName().get('FICA').getRecordTypeId();
        tr1.Active__c=false;
        tr1.Employee_Rate__c=120;
        tr1.Employer_Rate__c=20;
        tr1.Type__c='Medicare';
        insert tr1;  
        tr1.Active__c=true;
        update tr1;
        }
        catch(Exception e){}
    }
     public static testmethod void test3(){
           try{
            TriggerActivation__c tra = new TriggerActivation__c();
            tra.Name='TaxRate';
            tra.isActive__c=true;
            insert tra;
       Tax_Rates__c tr = new Tax_Rates__c();
        tr.RecordTypeId= Schema.SObjectType.Tax_Rates__c.getRecordTypeInfosByName().get('FICA').getRecordTypeId();
        tr.Active__c=true;
        tr.Employee_Rate__c=12;
        tr.Employer_Rate__c=20;
        tr.Type__c='Social Security';
        insert tr;
            
        Tax_Rates__c tr1 = new Tax_Rates__c();
        tr1.RecordTypeId= Schema.SObjectType.Tax_Rates__c.getRecordTypeInfosByName().get('FICA').getRecordTypeId();
        tr1.Active__c=false;
        tr1.Employee_Rate__c=120;
        tr1.Employer_Rate__c=20;
        tr1.Type__c='Social Security';
        insert tr1;  
        tr1.Active__c=true;
        update tr1;
        }
        catch(Exception e){}
    } 
     public static  testmethod void test5(){
        try{
            TriggerActivation__c tra = new TriggerActivation__c();
            tra.Name='TaxRate';
            tra.isActive__c=true;
            insert tra;
       Tax_Rates__c tr = new Tax_Rates__c();
        tr.RecordTypeId= Schema.SObjectType.Tax_Rates__c.getRecordTypeInfosByName().get('FICA').getRecordTypeId();
        tr.Active__c=true;
        tr.Employee_Rate__c=12;
        tr.Employer_Rate__c=20;
        tr.Type__c='FICA';
        insert tr;
	    system.debug('***VAl of tr test'+tr);          
        Tax_Rates__c tr1 = new Tax_Rates__c();
        tr1.RecordTypeId= Schema.SObjectType.Tax_Rates__c.getRecordTypeInfosByName().get('FICA').getRecordTypeId();
        tr1.Active__c=false;
        tr1.Employee_Rate__c=120;
        tr1.Employer_Rate__c=20;
        tr1.Type__c='FICA';
        insert tr1; 
         tr1.Active__c=true;
         update tr1;   
        system.debug('**val of tr1'+tr1);
        }
        catch(Exception e){
            system.debug('**Val of exception'+ e);
        }
    } 
}