public class SchedulePaymentCA {
    @AuraEnabled 
    public static List <Payment__c > fetchRecs(String CAid){
        List<Opportunity > CA= [Select Id,name,CloseDate from Opportunity where Id=:CAid];
        Date begindate;
        if(CA.size()>0)
        begindate=CA[0].CloseDAte;
        System.debug('begindate'+begindate);
        
        List<Payment__c> paymentList= new List<Payment__c>();
        paymentList= [Select Id,Name,Scheduled_Date__c,Amount_Paid__c from Payment__c where Opportunity__c =: CAid AND Scheduled_Date__c>=:begindate Order By Scheduled_Date__c];
        return paymentList;
    }
}