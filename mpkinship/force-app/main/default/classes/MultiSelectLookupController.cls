public with sharing class MultiSelectLookupController {
    public MultiSelectLookupController () {
         
    }
    
    @AuraEnabled(continuation=true)
    public static string UpdateSobjectList(List<Payment__c> records, string recnumber,string vparentFiscalYear, string payMethod, string receiptType, string doreceipt, string OppId, String Description, String chknumber)
    {
        System.debug('records'+records);
        List<Payment__c> Payments = new List<Payment__c>();
        List<Payment__c> RefundList = new List<Payment__c>();
        List<Payment__c> UpdateRefundList = new List<Payment__c>();
        Payment__c Refund = new Payment__c();
        Id RecordTypeId = Schema.SObjectType.Payment__c.getRecordTypeInfosByName().get('Vendor Refund').getRecordTypeId() ;      
        TransactionJournalService.byPassCheckPosted = True;
        Decimal UniqueIdentifier = 0;
        for(Payment__c Payment: records){
            system.debug('payment ' + ' payment record >>>>');
            
            if(payment.Kinship_Case__c !=null){
                Refund.Recipient__c = 'Case';
            }else if(payment.Vendor__c !=null){
                Refund.Recipient__c = 'Vendor';
            }
            
            Refund.Invoice_Stage__c =payment.Invoice_Stage__c;
            Refund.Kinship_Case__c = payment.Kinship_Case__c;
            Refund.Kinship_Check__c = payment.Kinship_Check__c;
            Refund.Receipt_Type__c = payment.Receipt_Type__c;
            Refund.Payment_Date__c = Date.ValueOf(doreceipt);
            Refund.Report_Number__c = recnumber;
            Refund.Check_Reference_Number__c = chknumber;
            
            //Refund.Accounting_Period__c = vparentFiscalYear;
            //COMMENTED FOR KIN-1194
            //Refund.Pay_Method__c = payMethod;
            Refund.Payment_Method__c=payMethod;
            
            Refund.Description__c = Description;
            Refund.Vendor__c = payment.Vendor__c;
            Refund.Category_lookup__c = payment.Category_lookup__c;
            Refund.Payment_Amount__c = payment.Refund_Amount__c;
            Refund.Kinship_Check__c = payment.Kinship_Check__c;
            Refund.Contact__c = payment.Contact__c;
            Refund.Opportunity__c = OppId;
            Refund.RecordTypeid = RecordTypeId;
            Refund.Payment__c = payment.Id;
            System.debug('payment.Opportunity__r.Campaign.FIPS__c'+payment.Opportunity__c);
            //Refund.FIPS_Code__c  = payment.Opportunity__c;
            //Refund.Description__c = payment.Description__c;
            //Refund.UniqueIdentifier__c= UniqueIdentifier; 
            RefundList.add(Refund);
            
            if(Payment.Prior_Refund1__c == null){
                Payment.Prior_Refund1__c = 0;
            }
            Payment.Prior_Refund1__c += Payment.Refund_Amount__c;
            Payment.Refund_Amount__c = null;
            Payments.add(Payment);
            Refund = new Payment__c();
            //UniqueIdentifier++;
        }
        try{
            if(RefundList.size() > 0){
                insert RefundList;
                System.debug('RefundList'+RefundList);
                System.debug('Payments'+Payments);
                TransactionJournalService.byPassCheckPosted = true;
                update Payments;
                TransactionJournalService.byPassCheckPosted = false;
                
                System.debug('Payments'+Payments);
                Set<Id> ids = new Set<Id>();
                for(Payment__c opp:RefundList){ids.add(opp.Id);
                }
                 set<id> categoryids = new set<id>();
                 set<id> fipsids = new set<id>();
                id costcenterid;
                
                List<Payment__c> Invoice = [Select id, FIPS_Code__c, Opportunity__r.Campaign.FIPS__c,Category_lookup__c  from Payment__c where id IN: ids];
                for(Payment__c opp : Invoice){
                    if(opp.Opportunity__r.Campaign.FIPS__c != null){
                        Categoryids.add(opp.Category_lookup__c);
                        fipsids.add(opp.Opportunity__r.Campaign.FIPS__c);
                    }
                }    
                Map<Payment__c, id> resultMap = FetchCostcode(Invoice,  categoryids, fipsids);
                
                for(Payment__c opp : Invoice){
                    if(opp.Opportunity__r.Campaign.FIPS__c != null){
                        opp.FIPS_Code__c = opp.Opportunity__r.Campaign.FIPS__c;
                       
                        costcenterid = resultMap.get(Opp);
                        if(costcenterid !=null){
                            opp.Cost_Center__c = costcenterid;
                               
                        }
                        UpdateRefundList.add(opp);
                    }
                }
                if(UpdateRefundList.size() > 0){
                    System.debug('UpdateRefundList'+UpdateRefundList);
                    update UpdateRefundList;
                    System.debug('UpdateRefundList'+UpdateRefundList);
                }
            }
            
            return 'Done';
        }catch(exception e){
            System.debug('e.getMessage()'+e.getMessage());
            return e.getMessage();
        } 
        
    }

    @AuraEnabled(continuation=true)
    public static string SetPaymentCategoryAndFIPS(List<Id> paymentIsds){
        List<Payment__c> UpdateRefundList = new List<Payment__c>();
        set<id> categoryids = new set<id>();
        set<id> fipsids = new set<id>();
        id costcenterid;
        
        List<Payment__c> Invoice = [Select id, FIPS_Code__c, Opportunity__r.Campaign.FIPS__c,Category_lookup__c  from Payment__c where id IN: paymentIsds];
        for(Payment__c opp : Invoice){
            if(opp.Opportunity__r.Campaign.FIPS__c != null){
                Categoryids.add(opp.Category_lookup__c);
                fipsids.add(opp.Opportunity__r.Campaign.FIPS__c);
            }
        }    
        Map<Payment__c, id> resultMap = FetchCostcode(Invoice,  categoryids, fipsids);
        
        for(Payment__c opp : Invoice){
            if(opp.Opportunity__r.Campaign.FIPS__c != null){
                opp.FIPS_Code__c = opp.Opportunity__r.Campaign.FIPS__c;
                
                costcenterid = resultMap.get(Opp);
                if(costcenterid !=null){
                    opp.Cost_Center__c = costcenterid;
                        
                }
                UpdateRefundList.add(opp);
            }
        }
        if(UpdateRefundList.size() > 0){
            System.debug('UpdateRefundList'+UpdateRefundList);
            update UpdateRefundList;
            System.debug('UpdateRefundList'+UpdateRefundList);
        }
        return 'created sucessfully';
    }
    
    
        /**POPULATE COSTCODE**/
   public static Map<Payment__c, id> FetchCostcode(list<Payment__c> payments, set<id> categoryids, set<id> fipsids){   
       Map<Payment__c, id> MapPaymentCostCenter = new Map<Payment__c, id>();
       
              list<Category__c> categories=[select id,Locality_Account__c from category__C where id in: categoryids and Locality_Account__c!=null];
       system.debug(categories + ' categories in  FetchCostcode ???? ' );
       
       map<id,list<category__C>> LocalityCategoryMap = new map<id,list<category__C>>();
       for(Category__C category : categories){
           if(LocalityCategoryMap.containskey(category.Locality_Account__c)){
              LocalityCategoryMap.get(category.Locality_Account__c).add(category);   
           }
           else
              LocalityCategoryMap.put(category.Locality_Account__c,new list<category__C>{category});
       }
       
       system.debug(LocalityCategoryMap + ' LocalityCategoryMap in  FetchCostcode ????  ');
       system.debug(LocalityCategoryMap.keyset() + ' LocalityCategoryMap.keyset() in  FetchCostcode ????  ');
       system.debug(fipsids + 'fipsids ');
       
       map<string,id> CatFipsWithCostCodeMap = new map<String,id>();
       list<Chart_of_Account__c> charts=[select id,Locality_Account__c,Cost_Center__c,FIPS__c from Chart_of_Account__c where 
                                         Locality_Account__c in:LocalityCategoryMap.keyset() and  FIPS__c in: fipsids and Cost_Center__c!=null];
       system.debug(charts + ' charts in  FetchCostcode ????  ');
       
       for(Chart_of_Account__c chart : charts){
            
            if(LocalityCategoryMap.size()>0 && LocalityCategoryMap.containskey(chart.Locality_Account__c)){               
                for(category__c categoryrec : LocalityCategoryMap.get(chart.Locality_Account__c)){                    
                    if(!CatFipsWithCostCodeMap.containskey(categoryrec.id+'-'+chart.FIPS__c ))
                        CatFipsWithCostCodeMap.put(categoryrec.id+'-'+chart.FIPS__c,chart.Cost_Center__c);
                }
            }
        
        } 
         system.debug(CatFipsWithCostCodeMap + ' CatFipsWithCostCodeMap in  FetchCostcode ????  ');
        for(Payment__c paymentrec : payments){           
            if(CatFipsWithCostCodeMap.size()>0 && CatFipsWithCostCodeMap.containskey(paymentrec.Category_lookup__c+'-'+paymentrec.Opportunity__r.Campaign.FIPS__c)){                
                paymentrec.Cost_Center__c =CatFipsWithCostCodeMap.get(paymentrec.Category_lookup__c+'-'+paymentrec.Opportunity__r.Campaign.FIPS__c);
                system.debug( paymentrec.Cost_Center__c + '  paymentrec.Cost_Center__c in  FetchCostcode ????  ');
                MapPaymentCostCenter.put(paymentrec, paymentrec.Cost_Center__c);
            }
        } 
       return MapPaymentCostCenter;
   }
    
    
    @AuraEnabled(continuation=true)
    public static List<sObject> getSobjectListResults(String ObjectName, List<String> fieldNames, String WhereClause) {
        List<String> resultList = new List<String>();
        system.debug('fieldNames????' + fieldNames);
        system.debug('WhereClause????' + WhereClause);
        if(String.isNotEmpty(WhereClause)) {
            String fieldString = String.join(fieldNames, ', ');
            String query = 'Select Id,' + fieldString + ' FROM '+ ObjectName  + ' WHERE ' + WhereClause;
            return Database.Query(query);             
        }
        return null;//resultList;
    }

    
    @AuraEnabled(cacheable=true)
    public static List<sObject> getSobjectListResultsCache(String ObjectName, List<String> fieldNames, String WhereClause) {
        List<String> resultList = new List<String>();
        system.debug('fieldNames????' + fieldNames);
        system.debug('WhereClause????' + WhereClause);
        if(String.isNotEmpty(WhereClause)) {
            String fieldString = String.join(fieldNames, ', ');
            String query = 'Select Id,' + fieldString + ' FROM '+ ObjectName  + ' WHERE ' + WhereClause;
            System.debug('query'+query);
            return Database.Query(query);             
        }
        return null;//resultList;
    }

  
    @AuraEnabled(cacheable=true)
    public static List<SObJectResult> getResults(String ObjectName, List<String> fieldNames, String value, List<String> selectedRecId, String whereStr,String parentId,String likeString,List<String>  costCenterIds) {
        List<SObJectResult> sObjectResultList = new List<SObJectResult>();
        if(selectedRecId == null)
            selectedRecId = new List<String>();
        if(String.isNotEmpty(value)) {
            String fieldString = String.join(fieldNames, ', ');
            String queryValue = '%' + value.trim() + '%';
            String query ='';
            //String recordTypeName ='Fiscal Year';
            ///String fiscalYearName = '%Local%';
            if(ObjectName != 'Campaign'){
                String fieldQuery = fieldNames[0] + ' LIKE :queryValue ';
                if(ObjectName == 'Payment__c')
                {
                    List<Payment__c> paymentChildList = [Select id,Name from Payment__c where Report_Number__c LIKE :queryValue AND RecordType.Name IN ('Receipt','Vendor Refund', 'Special Welfare Reimbursement')];
                    Set<Id> paymentchildids = new Set<Id>();
                    for(Payment__c p: paymentChildList){
                        paymentchildids.add(p.Id);
                    }

                query = 'Select Id,' + fieldString + ' FROM '+ ObjectName + ' WHERE Id IN: paymentchildids';
                System.debug('??Query???? Payment'+query);
                System.debug('paymentchildids?????? '+paymentchildids);
                System.debug('parentID????????? '+parentId);
                for(sObject so : Database.Query(query)) {
                    System.debug(so+'so???');
                    String fieldvalue = (String)so.get(fieldNames[0]);
                    // System.debug('??whereStr??'+whereStr);
                    // whereStr += query;
                
                    sObjectResultList.add(new SObjectResult(fieldvalue, so.Id, '','',''));
                }
                return sObjectResultList;
            }
            //System.debug('??Query???? Payment out'+query);
                if(fieldNames.size() > 1) {
                    if(fieldNames[1] == 'FIPS_Code__c'){
                        fieldQuery = ' (' + fieldNames[0] + ' LIKE :queryValue) ';
                    }
                    else{
                        fieldQuery = ' (' + fieldNames[0] + ' LIKE :queryValue OR ' + fieldNames[1] + ' LIKE :queryValue) ';
                    }
                    
                }
                query = 'Select Id,' + fieldString + ' FROM '+ ObjectName +' WHERE ' + fieldQuery + ' and ID NOT IN: selectedRecId';
            }
            else{
                if(parentId != null && parentId != ''){
                    List<Campaign> childList = [Select id,Name from Campaign where ParentId =: parentId ];
                    Set<Id> childids = new Set<Id>();
                    for(Campaign c: childList){
                        childids.add(c.Id);
                    }
                    
                    query = 'Select Id,' + fieldString + ' FROM '+ ObjectName +' WHERE ' + fieldNames[0] + ' LIKE :queryValue and ID NOT IN: selectedRecId and ParentId IN: childids';
                    
                }
                else if(likeString != null && likeString != ''){
                    query = 'Select Id,' + fieldString + ' FROM '+ ObjectName +' WHERE ' + fieldNames[0] + ' LIKE :queryValue and ID NOT IN: selectedRecId and Name LIKE: likeString';
                }
                else{
                    System.debug('??fieldNames[0]'+fieldNames[0]);
                    System.debug('??fieldString'+fieldString);
                    query = 'Select Id,' + fieldString + ' FROM '+ ObjectName +' WHERE ' + fieldNames[0] + ' LIKE :queryValue and ID NOT IN: selectedRecId';
                    System.debug('??query??'+query);
                }
            }
            
            if(whereStr != null && whereStr != ''){System.debug('??whereStr??'+whereStr);query += whereStr;
            }
            //System.debug('??costCenterIds??'+costCenterIds.size());
            if(costCenterIds.size() > 0){
                query += ' AND Id IN: costCenterIds';
            }
            
            System.debug('??query??'+query);
            for(sObject so : Database.Query(query)) {
                System.debug(so+'so???');
                System.debug((String)so.get(fieldNames[0])+'(String)so.get(fieldNames[0])???');
                
                String fieldvalue = (String)so.get(fieldNames[0]);
                /*for (String fieldName: fieldNames) {
                    fieldvalue += (String)so.get(fieldName) + ' ';
                }*/
                String secondValue = '';
                String thirdValue = '';
                String forthValue = '';
                if(fieldNames.size() > 1) {
                    secondValue = (String)so.get(fieldNames[1]);
                    //System.debug(secondValue = (String)so.get(fieldNames[1])+'secondValue = (String)so.get(fieldNames[1])???');
                }
                if(fieldNames.size() > 2) {
                    thirdValue = (String)so.get(fieldNames[2]);
                }
                if(fieldNames.size() > 3) {
                    forthValue = (String)so.get(fieldNames[3]);
                }
                sObjectResultList.add(new SObjectResult(fieldvalue, so.Id, secondValue,thirdValue,forthValue));
            }
        }
        else{
            String fieldString = String.join(fieldNames, ', ');
            String query ='';
            if(ObjectName == 'Cost_Center__c'){
                query = 'Select Id,' + fieldString + ' FROM '+ ObjectName +' WHERE ID NOT IN: selectedRecId';
            }
            System.debug('??costCenterIds??'+costCenterIds.size());
            if(costCenterIds.size() > 0){
                query += ' AND Id IN: costCenterIds';
            }
            
            System.debug('??query??'+query);
            for(sObject so : Database.Query(query)) {
                String fieldvalue = (String)so.get(fieldNames[0]);
                String secondValue = '';
                String thirdValue = '';
                String forthValue = '';
                if(fieldNames.size() > 1) {
                    secondValue = (String)so.get(fieldNames[1]);
                }
                if(fieldNames.size() > 2) {
                    thirdValue = (String)so.get(fieldNames[2]);
                }
                if(fieldNames.size() > 3) {
                    forthValue = (String)so.get(fieldNames[3]);
                }
                sObjectResultList.add(new SObjectResult(fieldvalue, so.Id, secondValue,thirdValue,forthValue));
            }
        }
       
        return sObjectResultList;
    }
    
    public class SObjectResult {
        @AuraEnabled
        public String recName;
        
        @AuraEnabled
        public String secondName;
        
        @AuraEnabled
        public String thirdName;
        
        @AuraEnabled
        public String forthName;

        @AuraEnabled
        public Id recId;
        
        public SObJectResult(String recNameTemp, Id recIdTemp, String secondName,String thirdName,String forthName) {
            this.recName = recNameTemp;
            this.recId = recIdTemp;
            this.secondName = secondName;
            this.thirdName = thirdName;
            this.forthName = forthName;
        }
    }
}