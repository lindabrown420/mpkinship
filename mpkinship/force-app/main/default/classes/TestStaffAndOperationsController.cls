@isTest
public class TestStaffAndOperationsController {
    static testmethod void testmethod1(){
        TriggerActivation__c  ta = new TriggerActivation__c();
        ta.name='PurchaseOrder Trigger';
        ta.isActive__c=false;
        insert ta;
        
        
         TriggerActivation__c  ta1 = new TriggerActivation__c();
        ta1.name='UpdateCSACoordinatorSign';
        ta1.isActive__c=false;
        insert ta1;    
        account accountrec = new account();
        accountrec.Name='test vendor';
        accountrec.Tax_ID__c='333-33-3333';
        accountrec.Entity_Type__c='Individual';
        insert accountrec;
        
        Locality_Account__c la = new Locality_Account__c();
        la.name='33';
        insert la;
        
        id catrecType = Schema.SObjectType.Category__c.getRecordTypeInfosByName().get('LASER Category').getRecordTypeId();
        
        Category__c category = new Category__c();
        category.Name='222';
        category.Category_Type__c='Adoption';
        category.RecordTypeId = catrecType;
        category.Locality_Account__c=la.id;
        insert category;
        
        Campaign parent = new Campaign();
        parent.name = '2021';
        parent.StartDate = Date.Today().addDays(-10);
        parent.EndDate = Date.Today().addDays(10);
        parent.Fiscal_Year_Type__c = 'DSS';
        parent.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId();
        insert parent;
        id recType = Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('LASER_Reporting_Period').getRecordTypeId();
        campaign camp = new campaign();
        camp.name = 'May';
        camp.recordtypeid = recType;
        camp.Begin_Date__c=system.today();
        camp.EndDate=system.today().adddays(2);
        camp.parentId = parent.id;
        insert camp;
        
        id opprecType = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Staff_Operations').getRecordTypeId();
        /*opportunity opprecord = new opportunity();
        opprecord.CloseDate=date.today();
        opprecord.StageName='Draft';
        opprecord.Name='test opportunity';        
        opprecord.Amount=20000;
        opprecord.AccountId=accountrec.id;
        opprecord.Accounting_Period__c = camp.id;
        opprecord.RecordTypeId=opprecType;
        insert opprecord;
        */
        
       
        opportunity opprecord = new opportunity();
        opprecord.CloseDate=date.today();
        opprecord.StageName='Draft';
        opprecord.Name='test opportunity';
        opprecord.Amount=20000;
        opprecord.AccountId=accountrec.id;
        opprecord.RecordTypeId=opprecType;
        opprecord.Accounting_Period__c = camp.id;
        opprecord.Number_of_Months_Units__c=3; 
        opprecord.Category__c = category.Id;      
        insert opprecord;
       
        id payrecType = Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Staff_Operations').getRecordTypeId();
        Payment__c payment = new Payment__c();
        payment.Opportunity__c=opprecord.id;
        payment.Payment_Amount__c=2000;
        payment.vendor__c = accountrec.Id;
        payment.RecordTypeId=payrecType;
        payment.Category_lookup__c = category.Id;
        insert payment;
        
        system.debug('payment ??????' + payment);
        StaffAndOperationsController cont = new StaffAndOperationsController();
        StaffAndOperationsController.getInvoices(opprecord.Id);
        StaffAndOperationsController.getInvoiceRecord(opprecord.Id);
        StaffAndOperationsController.getInvoiceId(payment.Id);
        StaffAndOperationsController.getOpportunityRecord(opprecord.Id);
        StaffAndOperationsController.getOpportunityRecordByComp(camp.Id);
        
        //StaffAndOperationsController.isGUAAllocationCreated(payment.Id);
        StaffAndOperationsController.getInvoicesList(opprecord.Id);
        StaffAndOperationsController.getPaymentInvoices(opprecord.Id);
        Test.startTest();
           
            Blob bodyBlob=Blob.valueOf('Unit Test ContentVersion Body to be insert in test class for testing the'); 
            
            ContentVersion contentVersion_1 = new ContentVersion(
                Title='SampleTitle', 
                PathOnClient ='SampleTitle.jpg',
                VersionData = bodyBlob, 
                origin = 'H'
            );
            insert contentVersion_1;
            
            ContentVersion contentVersion_2 = [SELECT Id, Title, ContentDocumentId 
                            FROM ContentVersion WHERE Id = :contentVersion_1.Id LIMIT 1];
            
            ContentDocumentLink contentlink = new ContentDocumentLink();
            contentlink.LinkedEntityId = payment.id;
            contentlink.contentdocumentid = contentVersion_2.contentdocumentid;
            contentlink.ShareType = 'V';
            insert contentlink;
       
            string filename = '';
        String body = 'SampleTitle';
        String uploadIds = contentVersion_2.id;
        String base64 = 'Unit Test ContentVersion Body to be insert in test class for testing the';
          
        StaffAndOperationsController.insertStaffAndOperation( uploadIds, base64, filename, '222', accountrec.id, 'check', camp.id, '10','', 'Pending', null, Category.Id, payrecType ,payment.id,'test', opprecord.id);
    	StaffAndOperationsController.getFileName(payment.Id);
        StaffAndOperationsController.uploadFile(payment.id,  uploadIds ) ;
        StaffAndOperationsController.deleteInvoice(payment.Id);
        List<Id> invoiceIdList = new List<Id>();
        invoiceIdList.add(payment.Id);
        StaffAndOperationsController.deleteAllInvoice(invoiceIdList);
        system.assertEquals(20000, opprecord.amount);  
    }
}