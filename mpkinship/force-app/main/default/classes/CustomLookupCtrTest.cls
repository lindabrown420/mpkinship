@isTest
public class CustomLookupCtrTest {
  
    public static testmethod void fetchLookUpValuesTest(){
        contact cc = new contact();
        cc.lastname='test contact';
        cc.Email='test@gmail.com';
        insert cc;
        Locality_Account__c la = new Locality_Account__c();
        la.name='33';
        insert la;
        LASER_Cost_Code__c lc = new LASER_Cost_Code__c();
        lc.name='222';
        insert lc;
        FIPS__c fp = new FIPS__c();
        fp.name='covington-2022';
        insert fp;
        CustomLookupCtr.fetchLookUpValues('test','Contact',fp.id);
        category__C cat = new category__c();
        cat.recordtypeid=Schema.SObjectType.category__C.getRecordTypeInfosByDeveloperName().get('LASER_Category').getRecordTypeId();
        cat.name='test cat';
        cat.Locality_Account__c=la.id;
        cat.LASER_Cost_Code__c=lc.id;
        cat.Category_Type__c='IVE';
        insert cat;
        CustomLookupCtr.fetchLookUpValues('test','Category__c',fp.id);
       CustomLookupCtr.fetchLookUpValuesForCaseAction('test','Category__c','',fp.id,null);
        product2 pro = new product2();
        pro.name='test';
        pro.Unit_Measure__c='2';
        pro.IsActive=true;
        insert pro;
        CustomLookupCtr.fetchProductValues('test','Product2');
        CSA_Category__c ca = new CSA_Category__c();
        ca.Name='1a';
        ca.Category_Code__c='22';
        insert ca;
        category__C cat1 = new category__c();
        cat1.recordtypeid=Schema.SObjectType.category__C.getRecordTypeInfosByDeveloperName().get('LEDRS_Category').getRecordTypeId();
        cat1.name='test cat';
        cat1.CSA_Category__c=ca.id;
        cat1.FIPS_Code__c=fp.id;
        cat1.Locality_Account__c=la.id;
        cat1.LASER_Cost_Code__c=lc.id;
        cat1.Category_Type__c='CSA';
        
        insert cat1;
        CustomLookupCtr.fetchLookUpValues('test','Category__c',fp.id);
        system.assertEquals(fp.id,  cat1.FIPS_Code__c);
    }
}