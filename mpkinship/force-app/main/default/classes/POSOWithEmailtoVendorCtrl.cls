public class POSOWithEmailtoVendorCtrl {
	 @AuraEnabled
    public static List <Opportunity> fetchOpportunity(Integer pageSize, Integer pageNumber) {
        Integer offset = (pageNumber - 1) * pageSize;
        //Qyery 10 accounts
        Id porecTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Purchase_Order').getRecordTypeId();
        List<Opportunity> oppList = [SELECT Id, Name, AccountId, 
                                    StageName, CloseDate, amount, OwnerId, CreatedById, CreatedBy.FirstName, CreatedBy.LastName, CreatedDate,Kinship_Case__c,Kinship_Case__r.Name  from Opportunity where StageName =:'Open' AND RecordTypeId=:porecTypeId LIMIT :pageSize OFFSET :offset];
        //return list of accounts
        return oppList;
    }
}