@IsTest
public class UpdateCSATest {
    static testmethod void testmethod1(){
        CaseActionPOController.IsCaseActionScreen=true;
        contact con = new contact();
        con.LastName='contactrec';
        con.FirstName='test';
        con.Email='test@contact.com';       
        insert con;
        Campaign parent = new Campaign();
        parent.name = '2021';
        parent.StartDate = Date.Today().addDays(-10);
        parent.EndDate = Date.Today().addDays(10);
        parent.Fiscal_Year_Type__c = 'Local';
        parent.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId();
        insert parent;
        Kinship_Case__c caserec = new Kinship_Case__c();
        caserec.Primary_Contact__c=con.id;
        caserec.DOB__c=system.today().adddays(-100);
        caserec.Race__c='1';
        caserec.Gender_Preference__c='M';
        caserec.Hispanic_Ethnicity__c='1';
        caserec.DSM_V_Indicator_ICD_10_1__c='2';
        caserec.Clinical_Medication_Indicator_1__c='2';
        caserec.Medicaid_Indicator_1__c='2';
        caserec.Referral_Source__c='5';
        caserec.Autism_Flag_1__c='2';
        insert caserec;
        
        Special_Welfare_Account__c swa = new Special_Welfare_Account__c();
        swa.Fund_Name__c = 'test';     
        swa.Case_KNP__c = caserec.Id;
        swa.Date_Closed__c= date.today().addDays(5);
        swa.Initial_Balance_For_Child_Support_Paymen__c =2000;
        insert swa;
        
        account accountrec = new account();
        accountrec.Name='test vendor';
        accountrec.Tax_ID__c='333-33-3333';
        accountrec.Entity_Type__c='Individual';
        insert accountrec;
         Locality_Account__c la = new Locality_Account__c();
        la.Name='222';
        insert la;
        
        Category__c category = new Category__c();
        category.Name='222';
        category.Locality_Account__c=la.id;
        category.Category_Type__c='CSA';
        category.Inactive__c=false;
        insert category;
        
        Category__c category1 = new Category__c();
        category1.RecordTypeId =Schema.SObjectType.Category__c.getRecordTypeInfosByName().get('LOCAL Category').getRecordTypeId();
        category1.Name='2221';
        category1.Category_Type__c='Special Welfare';
        //category1.Special_Welfare_Reimbursement_Vendor__c= accountrec.id;
        insert category1;
        
        Category_Rate__c cr = new Category_Rate__c();
        cr.Category__c = category.id;
        cr.Status__c='Active';
        cr.Price__c=5000;
        cr.Effective_Date__c=system.today();
        cr.RecordTypeId = Schema.SObjectType.Category_Rate__c.getRecordTypeInfosByDeveloperName().get('Other_Rate').getRecordTypeId();
        insert cr;
        
        opportunity opprecord = new opportunity();
        opprecord.CloseDate=date.today();
        opprecord.StageName='Draft';
        opprecord.Name='test opportunity';
        opprecord.Kinship_Case__c=caserec.id;
        opprecord.Amount=20000;
        opprecord.Category__c =category.id;
        opprecord.AccountId=accountrec.id;
        opprecord.Number_of_Months_Units__c=3;
        //opprecord.npe01__Do_Not_Automatically_Create_Payment__c=true;
        insert opprecord;
        
        opportunity opprecord1 = new opportunity();
        opprecord1.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Case Action').getRecordTypeId();
        opprecord1.CloseDate=date.today();
        opprecord1.StageName='Draft';
        opprecord1.Name='test opportunity';
        opprecord1.Kinship_Case__c=caserec.id;
        opprecord1.Amount=200;
        opprecord1.Category__c =category1.id;
        opprecord1.AccountId=accountrec.id;
        opprecord1.Number_of_Months_Units__c=3;
        //opprecord1.npe01__Do_Not_Automatically_Create_Payment__c=true;
        insert opprecord1;
        
        Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitrequest();
        app.setObjectId(opprecord.Id);    
         // Submit the approval request for the po    
        Approval.ProcessResult result = Approval.process(app);    
        // Verify that the results are as expected    
        System.assert(result.isSuccess());
       /* opprecord.name='testoneopportunity';
        update opprecord; */
        //system.assertEquals(opprecord.StageName,'Approved');  
        if(result.isSuccess()){
            opprecord.stagename='Approved';
            update opprecord;
        }      
    }
}