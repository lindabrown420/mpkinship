@isTest
public class OpportunityLineItemsTrgTest {
	 @testSetup
    static void testecords(){
        TriggerActivation__c  ta = new TriggerActivation__c();
        ta.name='OpportunityLineItemTrigger';
        ta.isActive__c=true;
        insert ta;
        
        contact con = new contact();
        con.LastName='contactrec';
        con.FirstName='test';
        con.Email='test@contact.com';        
        insert con;
        
        Kinship_Case__c caserec = new Kinship_Case__c();
        caserec.Primary_Contact__c=con.id;
        caserec.DOB__c=system.today().adddays(-100);
        caserec.Race__c='1';
        caserec.Gender_Preference__c='M';
        caserec.Hispanic_Ethnicity__c='1';
        caserec.DSM_V_Indicator_ICD_10_1__c='2';
        caserec.Clinical_Medication_Indicator_1__c='2';
        caserec.Medicaid_Indicator_1__c='2';
        caserec.Referral_Source__c='5';
        caserec.Autism_Flag_1__c='2';
        insert caserec;
     
        account accountrec = new account();
        accountrec.Name='test vendor';
        accountrec.Tax_ID__c='333-33-3333';
        accountrec.Entity_Type__c='Individual';
        insert accountrec;
        
        Locality_Account__c la = new Locality_Account__c();
        la.Name='222';
        insert la;
        
        Category__c category = new Category__c();
        category.Name='222';
        category.Locality_Account__c=la.id;
        category.Category_Type__c='Adoption';
        category.recordtypeid=Schema.SObjectType.category__C.getRecordTypeInfosByDeveloperName().get('LASER_Category').getRecordTypeId();
        insert category;
     
    }
     public static testmethod void test1(){
     	list<TriggerActivation__c> talist =[select id,name,isactive__C from TriggerActivation__c where name='OpportunityLineItemTrigger'];
        list<Kinship_Case__c> cases=[select id from Kinship_Case__c];
        list<account> accounts=[select id,recordtypeid from account where name='test vendor'];
     
        list<Category__c> categories=[select id from Category__c];
      
        Product2 p = new product2(name='x',isactive=true,Unit_Measure__c='2');
        insert p;
            
        Product2 p2 = new product2(name='y',isactive=true,Unit_Measure__c='4');
        insert p2;
        Campaign parent = new Campaign();
            parent.name = '2021';
            parent.StartDate = Date.Today().addDays(-10);
            parent.EndDate = Date.Today().addDays(10);
            parent.Fiscal_Year_Type__c = 'Local';
            parent.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId();
            insert parent;    
            //define the standart price for the product
            id stdPb = Test.getStandardPricebookId();
            insert new PricebookEntry(pricebook2id = stdPb, product2id = p.id,unitprice=1.0, isActive=true);
            insert new PricebookEntry(pricebook2id = stdPb, product2id = p2.id,unitprice=1.0, isActive=true);
            Pricebook2 pb = new pricebook2(name='test',Vendor__c=accounts[0].id,IsActive=true,
                                           Accounting_Period__c = parent.id );
            insert pb;
            PricebookEntry pbe = new PricebookEntry(pricebook2id=pb.id, product2id=p.id,unitprice=100.0, isActive=true);
            insert pbe; 
           PricebookEntry pbe1 = new PricebookEntry(pricebook2id=pb.id, product2id=p2.id,unitprice=1000.0, isActive=true);
            insert pbe1; 
         Locality_Account__c la = new Locality_Account__c();
        la.Name='222';
        insert la;
    Category__c category = new Category__c();
        category.Name='222';
        category.Locality_Account__c=la.id;
        category.Category_Type__c='Adoption';
        category.recordtypeid=Schema.SObjectType.category__C.getRecordTypeInfosByDeveloperName().get('LASER_Category').getRecordTypeId();
        insert category;
         
         String orgId = UserInfo.getOrganizationId();
    String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
    Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
    String uniqueName = orgId + dateString + randomInt;
        
         Profile profilename = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'standt', Email='standarduser1@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = profilename.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName=uniqueName+'@testorg.com');
        system.runAs(u){
        opportunity opprecord = new opportunity();
        opprecord.CloseDate=date.today();
        opprecord.StageName='Draft';
        opprecord.Name='test opportunity';
        opprecord.Kinship_Case__c=cases[0].id;
        opprecord.Amount=20000;
        opprecord.AccountId=accounts[0].id;
        opprecord.Number_of_Months_Units__c=2;
        insert opprecord;   
         //NOW CREATING LINE ITEMS
            opportunitylineitem opplineitem = new opportunitylineitem();
            opplineitem.Unit_Measure__c='1';
            opplineitem.Product2Id=p2.id;
        	opplineitem.UnitPrice=1000;
            opplineitem.PricebookEntryId=pbe1.id;
           opplineitem.Category__c=category.id;
            opplineitem.Quantity=3;
            opplineitem.OpportunityId=opprecord.id;
            //opplineitem.TotalPrice = 3000;
            insert opplineitem;
            
            opportunitylineitem opplineitem1 = new opportunitylineitem();
            opplineitem1.Unit_Measure__c='1';
            opplineitem1.Product2Id=p.id;
            opplineitem1.PricebookEntryId=pbe.id;
            opplineitem1.Quantity=2;
        	opplineitem1.Category__c=category.id;
            opplineitem1.UnitPrice=100;
            opplineitem1.OpportunityId=opprecord.id;
            //opplineitem1.TotalPrice = 200;
            insert opplineitem1;
        
        	Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitrequest();
            app.setObjectId(opprecord.Id);    
             // Submit the approval request for the po    
            Approval.ProcessResult result = Approval.process(app);    
            // Verify that the results are as expected    
            System.assert(result.isSuccess());
          
            if(result.isSuccess()){
                opprecord.stagename='Approved';
                update opprecord;
                try{
                	  //opplineitem1.Quantity=3;	 
                    // update opplineitem1;
                    delete opplineitem1;
                }
                catch(Exception e){}
            } 
        }   
    }
      public static testmethod void test2(){
     	list<TriggerActivation__c> talist =[select id,name,isactive__C from TriggerActivation__c where name='OpportunityLineItemTrigger'];
        list<Kinship_Case__c> cases=[select id from Kinship_Case__c];
        list<account> accounts=[select id,recordtypeid from account where name='test vendor'];
     
        list<Category__c> categories=[select id from Category__c];
      
        Product2 p = new product2(name='x',isactive=true,Unit_Measure__c='2');
        insert p;
            
        Product2 p2 = new product2(name='y',isactive=true,Unit_Measure__c='2');
        insert p2;
           Campaign parent = new Campaign();
            parent.name = '2021';
            parent.StartDate = Date.Today().addDays(-10);
            parent.EndDate = Date.Today().addDays(10);
            parent.Fiscal_Year_Type__c = 'Local';
            parent.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId();
            insert parent;    
            
            //define the standart price for the product
            id stdPb = Test.getStandardPricebookId();
            insert new PricebookEntry(pricebook2id = stdPb, product2id = p.id,unitprice=1.0, isActive=true);
            insert new PricebookEntry(pricebook2id = stdPb, product2id = p2.id,unitprice=1.0, isActive=true);
            Pricebook2 pb = new pricebook2(name='test',Vendor__c=accounts[0].id,IsActive=true,
                                          Accounting_Period__c = parent.id  );
            insert pb;
            PricebookEntry pbe = new PricebookEntry(pricebook2id=pb.id, product2id=p.id,unitprice=100.0, isActive=true);
            insert pbe; 
           PricebookEntry pbe1 = new PricebookEntry(pricebook2id=pb.id, product2id=p2.id,unitprice=1000.0, isActive=true);
            insert pbe1; 
          String orgId = UserInfo.getOrganizationId();
    String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
    Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
    String uniqueName = orgId + dateString + randomInt;
   		 Profile profilename = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'standt', Email='standarduser1@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = profilename.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName=uniqueName+'@testorg.com');
        system.runAs(u){
        opportunity opprecord = new opportunity();
        opprecord.CloseDate=date.today();
        opprecord.StageName='Draft';
        opprecord.Name='test opportunity';
        opprecord.Kinship_Case__c=cases[0].id;
        opprecord.Amount=20000;
        opprecord.AccountId=accounts[0].id;
        opprecord.Number_of_Months_Units__c=2;
        insert opprecord;   
         //NOW CREATING LINE ITEMS
            opportunitylineitem opplineitem = new opportunitylineitem();
            opplineitem.Unit_Measure__c='1';
            opplineitem.Product2Id=p2.id;
          	opplineitem.Category__c=categories[0].id;
            opplineitem.PricebookEntryId=pbe1.id;
            opplineitem.Quantity=3;
            opplineitem.UnitPrice=1000;
            opplineitem.OpportunityId=opprecord.id;
            //opplineitem.TotalPrice = 3000;
            insert opplineitem;
            
            opportunitylineitem opplineitem1 = new opportunitylineitem();
            opplineitem1.Unit_Measure__c='1';
            opplineitem1.Product2Id=p.id;
            opplineitem1.PricebookEntryId=pbe.id;
          opplineitem1.Category__c=categories[0].id;
            opplineitem1.Quantity=2;
           opplineitem1.UnitPrice=100;
            opplineitem1.OpportunityId=opprecord.id;
            //opplineitem1.TotalPrice = 200;
            insert opplineitem1;
        
        	Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitrequest();
            app.setObjectId(opprecord.Id);    
             // Submit the approval request for the po    
            Approval.ProcessResult result = Approval.process(app);    
            // Verify that the results are as expected    
            System.assert(result.isSuccess());
          
            if(result.isSuccess()){
                opprecord.stagename='Approved';
                update opprecord;
                try{
                	opplineitem1.Quantity=3;	 
                    update opplineitem1;
                    
                }
                catch(Exception e){}
            } 
        }   
      }   
    public static testmethod void test4(){
     	list<TriggerActivation__c> talist =[select id,name,isactive__C from TriggerActivation__c where name='OpportunityLineItemTrigger'];
        list<Kinship_Case__c> cases=[select id from Kinship_Case__c];
        list<account> accounts=[select id,recordtypeid from account where name='test vendor'];
     
        list<Category__c> categories=[select id from Category__c];
      
        Product2 p = new product2(name='x',isactive=true,Unit_Measure__c='2');
        insert p;
            
        Product2 p2 = new product2(name='y',isactive=true,Unit_Measure__c='4');
        insert p2;
        Campaign parent = new Campaign();
            parent.name = '2021';
            parent.StartDate = Date.Today().addDays(-10);
            parent.EndDate = Date.Today().addDays(10);
            parent.Fiscal_Year_Type__c = 'Local';
            parent.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId();
            insert parent;    
            //define the standart price for the product
            id stdPb = Test.getStandardPricebookId();
            insert new PricebookEntry(pricebook2id = stdPb, product2id = p.id,unitprice=1.0, isActive=true);
            insert new PricebookEntry(pricebook2id = stdPb, product2id = p2.id,unitprice=1.0, isActive=true);
            Pricebook2 pb = new pricebook2(name='test',Vendor__c=accounts[0].id,IsActive=true,
                                           Accounting_Period__c = parent.id );
            insert pb;
            PricebookEntry pbe = new PricebookEntry(pricebook2id=pb.id, product2id=p.id,unitprice=100.0, isActive=true);
            insert pbe; 
           PricebookEntry pbe1 = new PricebookEntry(pricebook2id=pb.id, product2id=p2.id,unitprice=1000.0, isActive=true);
            insert pbe1; 
         Locality_Account__c la = new Locality_Account__c();
        la.Name='222';
        insert la;
    	Category__c category = new Category__c();
        category.Name='222';
        category.Locality_Account__c=la.id;
        category.Category_Type__c='IVE';
        category.recordtypeid=Schema.SObjectType.category__C.getRecordTypeInfosByDeveloperName().get('LASER_Category').getRecordTypeId();
        insert category;
        String orgId = UserInfo.getOrganizationId();
    String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
    Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
    String uniqueName = orgId + dateString + randomInt;
         Profile profilename = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'standt', Email='standarduser1@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = profilename.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName=uniqueName+'@testorg.com');
        system.runAs(u){
        try{
        opportunity opprecord = new opportunity();
        opprecord.CloseDate=date.today();
        opprecord.StageName='Draft';
        opprecord.Name='test opportunity';
        opprecord.Kinship_Case__c=cases[0].id;
        opprecord.Amount=20000;
        opprecord.AccountId=accounts[0].id;
        opprecord.Number_of_Months_Units__c=2;
        insert opprecord;   
         //NOW CREATING LINE ITEMS
            opportunitylineitem opplineitem = new opportunitylineitem();
            opplineitem.Unit_Measure__c='1';
            opplineitem.Product2Id=p2.id;
        	opplineitem.UnitPrice=1000;
            opplineitem.PricebookEntryId=pbe1.id;
           opplineitem.Category__c=category.id;
            opplineitem.Quantity=3;
            opplineitem.OpportunityId=opprecord.id;
            //opplineitem.TotalPrice = 3000;
            insert opplineitem;
            
            opportunitylineitem opplineitem1 = new opportunitylineitem();
            opplineitem1.Unit_Measure__c='1';
            opplineitem1.Product2Id=p.id;
            opplineitem1.PricebookEntryId=pbe.id;
            opplineitem1.Quantity=2;
        	opplineitem1.Category__c=category.id;
            opplineitem1.UnitPrice=100;
            opplineitem1.OpportunityId=opprecord.id;
            //opplineitem1.TotalPrice = 200;
            insert opplineitem1;
        
        	Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitrequest();
            app.setObjectId(opprecord.Id);    
             // Submit the approval request for the po    
            Approval.ProcessResult result = Approval.process(app);    
            // Verify that the results are as expected    
            System.assert(result.isSuccess());
          
            if(result.isSuccess()){
                opprecord.stagename='Approved';
                update opprecord;
               
            }    
        }
        catch(Exception e){}
        }    
    }
    
     public static testmethod void test11(){
     	list<TriggerActivation__c> talist =[select id,name,isactive__C from TriggerActivation__c where name='OpportunityLineItemTrigger'];
        list<Kinship_Case__c> cases=[select id from Kinship_Case__c];
        list<account> accounts=[select id,recordtypeid from account where name='test vendor'];
     
        list<Category__c> categories=[select id from Category__c];
      
        Product2 p = new product2(name='x',isactive=true,Unit_Measure__c='2');
        insert p;
            
        Product2 p2 = new product2(name='y',isactive=true,Unit_Measure__c='2');
        insert p2;
           Campaign parent = new Campaign();
            parent.name = '2021';
            parent.StartDate = Date.Today().addDays(-10);
            parent.EndDate = Date.Today().addDays(10);
            parent.Fiscal_Year_Type__c = 'Local';
            parent.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId();
            insert parent;    
            
            //define the standart price for the product
            id stdPb = Test.getStandardPricebookId();
            insert new PricebookEntry(pricebook2id = stdPb, product2id = p.id,unitprice=1.0, isActive=true);
            insert new PricebookEntry(pricebook2id = stdPb, product2id = p2.id,unitprice=1.0, isActive=true);
            Pricebook2 pb = new pricebook2(name='test',Vendor__c=accounts[0].id,IsActive=true,
                                          Accounting_Period__c = parent.id  );
            insert pb;
            PricebookEntry pbe = new PricebookEntry(pricebook2id=pb.id, product2id=p.id,unitprice=100.0, isActive=true);
            insert pbe; 
           PricebookEntry pbe1 = new PricebookEntry(pricebook2id=pb.id, product2id=p2.id,unitprice=1000.0, isActive=true);
            insert pbe1; 
         try{
              Locality_Account__c la = new Locality_Account__c();
             la.Name='222';
             insert la;
             Category__c category = new Category__c();
             category.Locality_Account__c=la.id;
            category.Name='222';
            category.Category_Type__c='CSA';
            category.recordtypeid=Schema.SObjectType.category__C.getRecordTypeInfosByDeveloperName().get('LEDRS_Category').getRecordTypeId();
            insert category;
            opportunity opprecord = new opportunity();
            opprecord.CloseDate=date.today();
            opprecord.StageName='Draft';
            opprecord.Name='test opportunity';
            opprecord.Kinship_Case__c=cases[0].id;
            opprecord.Amount=20000;
            opprecord.AccountId=accounts[0].id;
            opprecord.Number_of_Months_Units__c=2;
            insert opprecord;   
             //NOW CREATING LINE ITEMS
                opportunitylineitem opplineitem = new opportunitylineitem();
                opplineitem.Unit_Measure__c='1';
                opplineitem.Product2Id=p2.id;
                opplineitem.Category__c=categories[0].id;
                opplineitem.PricebookEntryId=pbe1.id;
                opplineitem.Quantity=3;
                opplineitem.UnitPrice=1000;
                opplineitem.OpportunityId=opprecord.id;
                //opplineitem.TotalPrice = 3000;
                insert opplineitem;
                opplineitem.Category__c=category.id;
               update opplineitem;
            
        
         }catch(Exception e){}   
      } 
    /* public static testmethod void testValidatePOSOscreen(){
         account accountrec = new account();
        accountrec.Name='test vendornew';
        accountrec.Tax_ID__c='322-33-3333';
        accountrec.Entity_Type__c='Individual';
        insert accountrec;
        Category__c category1 = new Category__c();
        category1.RecordTypeId =Schema.SObjectType.Category__c.getRecordTypeInfosByName().get('LOCAL Category').getRecordTypeId();
        category1.Name='2221';
        category1.Category_Type__c='Special Welfare';
         category1.Special_Welfare_Reimbursement_Vendor__c=accountrec.id;
        insert category1;
        
        Contact con = new contact();
        con.LastName='contactrec111';
         con.Phone='566666544';
        con.Email='test@contact1.com';       
        insert con;
        
       
        
        Kinship_Case__c caserec = new Kinship_Case__c();
        caserec.Primary_Contact__c=con.id;
        caserec.DOB__c=system.today().adddays(-100);
        caserec.Race__c='1';
        caserec.Gender_Preference__c='M';
        caserec.Hispanic_Ethnicity__c='1';
        caserec.DSM_V_Indicator_ICD_10_1__c='2';
        caserec.Clinical_Medication_Indicator_1__c='2';
        caserec.Medicaid_Indicator_1__c='2';
        caserec.Referral_Source__c='5';
        caserec.Autism_Flag_1__c='2';
        insert caserec;
        
        Special_Welfare_Account__c swa = new Special_Welfare_Account__c();
        swa.Fund_Name__c = 'test';     
        swa.Case_KNP__c = caserec.Id;
        swa.Date_Closed__c= date.today().addDays(5);
        swa.Initial_Balance_For_Child_Support_Paymen__c=20000;
        swa.Initial_Balance__c=200000;
        insert swa;
        
        
        opportunity opprecord1 = new opportunity();
        opprecord1.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Purchase of Services Order').getRecordTypeId();
        opprecord1.CloseDate=date.today();
        opprecord1.StageName='Draft';
        opprecord1.Name='test opportunity';
        opprecord1.Kinship_Case__c=caserec.id;
        opprecord1.Amount=200;
        opprecord1.AccountId=accountrec.id;
        opprecord1.Number_of_Months_Units__c=3;
        opprecord1.npe01__Do_Not_Automatically_Create_Payment__c=true;
        insert opprecord1;
        
        Product2 p2 = new product2(name='y',isactive=true,Unit_Measure__c='2');
        insert p2;
        Campaign parent = new Campaign();
            parent.name = '2021';
            parent.StartDate = Date.Today().addDays(-10);
            parent.EndDate = Date.Today().addDays(10);
            parent.Fiscal_Year_Type__c = 'Local';
            parent.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId();
            insert parent;
        id stdPb = Test.getStandardPricebookId();
            insert new PricebookEntry(pricebook2id = stdPb, product2id = p2.id,unitprice=1.0, isActive=true);
        
        Pricebook2 pb = new pricebook2(name='test',Vendor__c=accountrec.id,IsActive=true,
                                          Accounting_Period__c = parent.id  );
        insert pb;
        
        PricebookEntry pbe1 = new PricebookEntry(pricebook2id=pb.id, product2id=p2.id,unitprice=1000.0, isActive=true);
            insert pbe1;
         String orgId = UserInfo.getOrganizationId();
         String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
    Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
    String uniqueName = orgId + dateString + randomInt;
         Profile profilename = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'standt', Email='standarduser1@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = profilename.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName=uniqueName+'@testorg.com');
        system.runAs(u){ 
        opportunitylineitem opplineitem = new opportunitylineitem();
                opplineitem.Unit_Measure__c='1';
        		opplineitem.PricebookEntryId=pbe1.id;
                opplineitem.Product2Id=p2.id;
                opplineitem.Category__c=category1.id;
                opplineitem.Quantity=3;
                opplineitem.OpportunityId=opprecord1.id;
               opplineitem.TotalPrice = 300;
               insert opplineitem;
        
    } */
     public static testmethod void test3(){
     	list<TriggerActivation__c> talist =[select id,name,isactive__C from TriggerActivation__c where name='OpportunityLineItemTrigger'];
        list<Kinship_Case__c> cases=[select id from Kinship_Case__c];
        list<account> accounts=[select id,recordtypeid from account where name='test vendor'];
     
        list<Category__c> categories=[select id from Category__c];
      
        Product2 p = new product2(name='x',isactive=true,Unit_Measure__c='2');
        insert p;
            
        Product2 p2 = new product2(name='y',isactive=true,Unit_Measure__c='4');
        insert p2;
        Campaign parent = new Campaign();
            parent.name = '2021';
            parent.StartDate = Date.Today().addDays(-10);
            parent.EndDate = Date.Today().addDays(10);
            parent.Fiscal_Year_Type__c = 'Local';
            parent.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId();
            insert parent;    
            //define the standart price for the product
            id stdPb = Test.getStandardPricebookId();
            insert new PricebookEntry(pricebook2id = stdPb, product2id = p.id,unitprice=1.0, isActive=true);
            insert new PricebookEntry(pricebook2id = stdPb, product2id = p2.id,unitprice=1.0, isActive=true);
            Pricebook2 pb = new pricebook2(name='test',Vendor__c=accounts[0].id,IsActive=true,
                                           Accounting_Period__c = parent.id );
            insert pb;
            PricebookEntry pbe = new PricebookEntry(pricebook2id=pb.id, product2id=p.id,unitprice=100.0, isActive=true);
            insert pbe; 
           PricebookEntry pbe1 = new PricebookEntry(pricebook2id=pb.id, product2id=p2.id,unitprice=1000.0, isActive=true);
            insert pbe1; 
         Locality_Account__c la = new Locality_Account__c();
        la.Name='222';
        insert la;
    Category__c category = new Category__c();
        category.Name='222';
        category.Locality_Account__c=la.id;
        category.Category_Type__c='Adoption';
        category.recordtypeid=Schema.SObjectType.category__C.getRecordTypeInfosByDeveloperName().get('LASER_Category').getRecordTypeId();
        insert category;
         
         String orgId = UserInfo.getOrganizationId();
         String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
    Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
    String uniqueName = orgId + dateString + randomInt;
        
         Profile profilename = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'standt', Email='standarduser1@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = profilename.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName=uniqueName+'@testorg.com');
        system.runAs(u){
        opportunity opprecord = new opportunity();
        opprecord.CloseDate=date.today();
        opprecord.StageName='Draft';
        opprecord.Name='test opportunity';
        opprecord.Kinship_Case__c=cases[0].id;
        opprecord.Amount=20000;
        opprecord.AccountId=accounts[0].id;
        opprecord.Number_of_Months_Units__c=2;
        insert opprecord;  
            try{
         //NOW CREATING LINE ITEMS
            opportunitylineitem opplineitem = new opportunitylineitem();
            opplineitem.Unit_Measure__c='1';
            opplineitem.Product2Id=p2.id;
        	opplineitem.UnitPrice=100000;
           
            opplineitem.PricebookEntryId=pbe1.id;
           opplineitem.Category__c=category.id;
            opplineitem.Quantity=3;
            opplineitem.OpportunityId=opprecord.id;
            //opplineitem.TotalPrice = 3000;
            insert opplineitem;
            
            opportunitylineitem opplineitem1 = new opportunitylineitem();
            opplineitem1.Unit_Measure__c='1';
            opplineitem1.Product2Id=p.id;
            opplineitem1.PricebookEntryId=pbe.id;
            opplineitem1.Quantity=2;
        	opplineitem1.Category__c=category.id;
            opplineitem1.UnitPrice=100;
            opplineitem1.OpportunityId=opprecord.id;
            //opplineitem1.TotalPrice = 200;
            insert opplineitem1;
            }
            catch(Exception e){}
        	
        }  
    }
   
}