public with sharing class NotifyController {
    
    
    @AuraEnabled
    public static List<User> getContactOwners(String recId){
        
        Contact con = [Select Id, Name, Email, OwnerId, CreatedById, LastModifiedById from Contact where Id =:recId];
        system.debug('con:::'+con);
        String OwnerId = con.OwnerId;

        User owner = [Select Id, Name from User where Id =: OwnerId];
        List<User> userList = new List<User>();
        userList.add(owner);
        system.debug('owner:::'+owner);
        system.debug('userList:::'+userList);
        return userList;
    }
    
    @AuraEnabled
    public static String sendEmailApex(List<String> toAddress, List<String> ccAddress, String subject, String body, List<String> files, String recordId) {
        string result;
        List<String> toAdd = new List<String>();
        List<Account> accList = [Select Id,Email__c from Account where Id IN: toAddress];
        if(accList.size() > 0){
            for(Account acc: accList){
                toAdd.add(acc.Email__c);
            }
        }
        
        List<String> ccAdd = new List<String>();
        List<Contact> conList = [Select Id,Email from Contact where Id IN: ccAddress];
        if(conList.size() > 0){
            for(Contact con: conList){
                ccAdd.add(con.Email);
            }
        }
        
        List<User> usList = [Select Id,Email from User where Id IN: ccAddress];
        if(usList.size() > 0){
            for(User us: usList){
                ccAdd.add(us.Email);
            }
        }
        Contract con = [ Select id, Accountid, Account.Name,Title__c,Status, Account.Email__c, Email_to_Vendor__c from Contract where id = :recordId limit 1];
        if(con.Email_to_Vendor__c == True)
        {
            try{
            PageReference defaultPage = new PageReference('/apex/Contracts'); 
            defaultPage.getParameters().put('id',recordId);
            
            Blob pageData; //variable to hold binary PDF data.
            if(!Test.isRunningTest()){ // for code coverage 
                pageData = defaultPage.getContentAsPDF();
            } else {
                pageData = Blob.valueOf('This is a test.');
            }
            //create attachment
            
            String filename ='Contract ' + DateTime.now().formatLong() + '.pdf';
            String vendorEmail = con.Account.Email__c;
            Attachment att = new Attachment(
                                    ParentId=recordId,
                                    Body=pageData,
                                    Name= filename
                                );
            insert att;
                
            //create and send email 
            Messaging.EmailFileAttachment mailAtt = new Messaging.EmailFileAttachment();
            mailAtt.setBody(pageData);
            mailAtt.setContentType('application/pdf');
            mailAtt.setFileName(filename);
            Messaging.SingleEmailMessage mess = new Messaging.SingleEmailMessage();
            mess.setSubject(subject);
            mess.setToAddresses(toAdd);
            mess.setCcAddresses(ccAdd);
            mess.setHtmlBody(body);
            mess.setEntityAttachments(files);
            mess.setFileAttachments(new Messaging.EmailFileAttachment[]{mailAtt});
            Messaging.sendEmail(new List<messaging.SingleEmailMessage> {mess});     
            result = 'Email has been sent successfully!!';
                if(con.Status == 'Draft'){
                    con.Status = 'In Approval Process';
                    update con;
                }
            }catch(exception e){
                return e.getMessage();
            }    
        }else{
          result = 'Email is disabled for the vendor.';  
        }
        return  result;
        /*List<Id> ids = new List<Id>();
        ids.add('0687F000008zGL5QAM');/
        
        Messaging.reserveSingleEmailCapacity(1);
        try{
        messaging.SingleEmailMessage mail = new messaging.SingleEmailMessage();
        
        //toAddress.add('krrish.kishore02@gmail.com');
        //ccAddress = new List<String>(); //{'bala.bandanadam@birlasoft.com'};
                
        mail.setToAddresses(toAddress);
        mail.setCcAddresses(ccAddress);
        //mail.setReplyTo('bala.bandanadam@birlasoft.com');
        //mail.setSenderDisplayName('Kishore');
        mail.setSubject(subject);
        mail.setHtmlBody(body);
        mail.setEntityAttachments(files);
        Messaging.sendEmail(new List<messaging.SingleEmailMessage> {mail});
        }
        catch (exception e){
            throw new AuraHandledException(e.getMessage());
            //return null;
        }*/
    }
    
    @AuraEnabled
    public static string getfirstName{
      get {
                return UserInfo.getFirstname() ;
          }         
       }
    @AuraEnabled
    public static string getLastName{
      get {
                return UserInfo.getLastname() ;
          }         
       }
     @AuraEnabled  
     public static string getEmail{
      get {
                return UserInfo.getUserEmail() ;
          }         
       }
     @AuraEnabled
    public static String getUserInfo(){
        String userInfo='';
        String FirstName = getfirstName;
        String LastName  = getLastName;
        String userEmail = getEmail;
        userInfo = FirstName + ' ' + LastName + ' ' + userEmail;
        return userInfo;
    }
}