public class KinshipHistoryServices {
    public static final String STR_NEW = 'New';
    public static final String STR_UPDATE = 'Update';
    public static final String STR_DELETE = 'Delete';

    private static List<Kinship_History__c> lstHis = null;

    public static List<Schema.DescribeFieldResult> getFieldToTrack(sObject obj) {
        Map<String, Schema.SObjectField> allFieldsMap = obj.getSObjectType().getDescribe().fields.getMap();
        List<Schema.DescribeFieldResult> fieldToTrack = new List<Schema.DescribeFieldResult>();

        for(Schema.SObjectField field : allFieldsMap.values()) {
            Schema.DescribeFieldResult describeResult = field.getDescribe();
            if(!Label.TRIGGER_SYSTEMFIELDS.contains(describeResult.getName()) &&
                describeResult.isAccessible() && !describeResult.isCalculated()) {
                    fieldToTrack.add(describeResult);
                }
        }

        return fieldToTrack;
    }

    public static void insertHistory(String id, sObject obj, String newValue, String oldValue, String action, String field) {
        Kinship_History__c history = new Kinship_History__c();
        history.Action__c = action;
        history.New_Value__c = getMaxLength(newValue, 255);
        history.Old_Value__c = getMaxLength(oldValue, 255);
        history.Object_Name__c = obj.getSObjectType().getDescribe().getName();
        history.Object_Id__c = id;
        history.Field_Name__c = field;
        // add to history
        if (lstHis == null) {
            lstHis = new List<Kinship_History__c>();
        }
        lstHis.add(history);
    }

    public static void flushHistory() {
        try{
            if (lstHis != null && !lstHis.isEmpty() && (Limits.getDMLStatements() + 1) < 150) {
                Database.insert(lstHis);
            }
        } catch(DMLException e) {
            System.debug('LoggingService DMLException: ' + e);
        }
        lstHis = new List<Kinship_History__c>();
    }

    @testVisible
    private static String getMaxLength(String str, Integer maxlength) {
        if (!String.isBlank(str) && str.length() > maxlength) {
            return str.substring(0, maxlength);
        } else {
            return str;
        }
    }
}