@isTest
private class CSACalendarControllerTest {
    @isTest
    static void testGetAllAgenda(){
        CSA_Meeting_Agenda__c meeting = new CSA_Meeting_Agenda__c();
        meeting.name = 'Test Name';
        meeting.Meeting_Date__c = Date.Today().addDays(1);
        meeting.Meeting_Start3_Time__c = '8:00:00.000';
        meeting.Meeting_End_Time__c = Time.newInstance(18, 2, 3, 0);
        meeting.Meeting_End_Time2__c = '8:30:00.000';
        meeting.Location_of_Meeting__c='Virginia';
        meeting.RecordTypeId = Schema.SObjectType.CSA_Meeting_Agenda__c.getRecordTypeInfosByName().get('CSA Meeting').getRecordTypeId();
        insert meeting;

        Test.startTest();
        List<CSA_Meeting_Agenda__c> lstMeeting = CSACalendarController.getAllAgenda();
        Test.stopTest();

        System.assertEquals(1, lstMeeting.size(), 'The FAPT_Meeting_Agenda__c return does not equal 1');
    }

    @isTest
    static void testCheckProfile(){
        Test.startTest();
        Boolean result = CSACalendarController.checkProfile();
        Test.stopTest();

        System.assertEquals(true, result, 'The Current Profile does not have access to FAPT_Meeting_Agenda__c');
    }
}