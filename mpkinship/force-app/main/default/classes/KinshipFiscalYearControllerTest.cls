@isTest
private class KinshipFiscalYearControllerTest {
@TestSetup
    static void initData(){
        Id profileId1 = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User u = new User(FirstName='Test',LastName= 'User',profileid=profileId1 , email='abc123@test.com',
                         username='abc123@test.com.prod',Alias='zbd',LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', EmailEncodingKey='UTF-8',
                          TimeZoneSidKey='America/Los_Angeles');
        
        Account acc = new Account(Name = 'test acc', Tax_ID__c = '123-56-7901', Entity_Type__c = 'Individual');
        insert acc;

        Locality_Account__c locality = new Locality_Account__c(Name = '12345');
        insert locality;

        Category__c cate = new Category__c(Name = 'test cate', Category_Type__c = 'CSA', Locality_Account__c = locality.id , RecordTypeId = Schema.SObjectType.Category__c.getRecordTypeInfosByName().get('LEDRS Category').getRecordTypeId());
        insert cate;
        
        FIPS__c fips = new FIPS__c(name = 'FIPS Test', FIPS_Code__c='1');
        insert fips;
        
        Kinship_Case__c kcase = new Kinship_Case__c(Primary_Social_Worker__c = u.id);

        Opportunity opp = new Opportunity(Name = 'test Opp', AccountId = acc.id, Category__c = cate.id, CloseDate = Date.today().addDays(10),
                                          StageName='Open', Kinship_Case__c = kcase.id);
        insert opp;

        Payment__c payment = new Payment__c(Opportunity__c = opp.id, 
                                                                Scheduled_Date__c = Date.today(), 
                                                                Status__c = 'Submitted For Approval',
                                                                Invoice_Stage__c = 'Ready to Pay',
                                                               	Written_Off__c = false);
        insert payment;
    }

    @isTest
    static void testKinshipSearchLookup_WithUser(){
        List<User> lstData = [select id, name from User];
        List<Id> fixedSearchResults= new List<Id>();
        fixedSearchResults.add(lstData[0].id);
        Test.setFixedSearchResults(fixedSearchResults);
        Test.startTest();
        List<Object> lstResult = KinshipFiscalYearController.kinshipSearchLookup('test', new List<String>(), 'User');
        Test.stopTest();
        System.assertEquals(1, lstResult.size(), 'The kinshipSearchLookup method return does not equal 1');
    }
    
    @isTest
    static void testKinshipSearchLookup_WithCamp(){
        Campaign c = new Campaign(name = 'CSA - 2020', Fiscal_Year_Type__c = 'Local');
        insert c;
        List<Id> fixedSearchResults= new List<Id>();
        fixedSearchResults.add(c.id);
        Test.setFixedSearchResults(fixedSearchResults);
        Test.startTest();
        List<Object> lstResult = KinshipFiscalYearController.kinshipSearchLookup('test', new List<String>(), 'Campaign');
        Test.stopTest();
        System.assertEquals(1, lstResult.size(), 'The kinshipSearchLookup method return does not equal 1');
    }
    
    @isTest
    static void testKinshipSearchLookup_WithFIPS(){
        List<FIPS__c> lstFIPS = [SELECT id, name FROM FIPS__c];
        List<Id> fixedSearchResults= new List<Id>();
        fixedSearchResults.add(lstFIPS[0].id);
        Test.setFixedSearchResults(fixedSearchResults);
        Test.startTest();
        List<Object> lstResult = KinshipFiscalYearController.kinshipSearchLookup('test', new List<String>(), 'FIPS');
        Test.stopTest();
        System.assertEquals(1, lstResult.size(), 'The kinshipSearchLookup method return does not equal 1');
    }
    
    @isTest
    private static void testGetPicklistvalues(){
        Test.startTest();
        List<String> lstResult = KinshipFiscalYearController.getPicklistvalues('Kinship_Check__c', 'Status__c', '--none--');
        Test.stopTest();

        System.assertEquals(5, lstResult.size(), 'The method does not return 5 expected status on the picklist');
    }
    
    @isTest
    private static void testInitData(){
        Test.startTest();
        Object obj = KinshipFiscalYearController.initData();
        Test.stopTest();

        System.assertNotEquals(null, obj, 'The method is return null value');
    }
    
    @isTest
    private static void testSaveFiscalYear_Local(){
        KinshipFiscalYearController.FiscalYearData data = KinshipFiscalYearController.initData();
        data.parentCamp.startDate = Date.today().addMonths(-2);
        data.parentCamp.endDate = Date.today().addMonths(2);
        data.parentCamp.fyType = 'Local';
        Test.startTest();
        data = KinshipFiscalYearController.saveFiscalYear(data);
        Test.stopTest();

        System.assertNotEquals(null, data, 'The method is return null value');
    }
    
    @isTest
    private static void testSaveFiscalYear_CSA(){
        List<FIPS__c> lstFIPS = [SELECT id, name FROM FIPS__c];
        Campaign fy = new Campaign(StartDate = Date.today().addMonths(-4), EndDate = Date.today().addMonths(4),
                                  	Fiscal_Year_Type__c = 'Local', FIPS__c = lstFIPS[0].id, Name = 'FY 2022');
        insert fy;
        KinshipFiscalYearController.FiscalYearData data = KinshipFiscalYearController.initData();
        data.parentCamp.startDate = Date.today().addMonths(-2);
        data.parentCamp.endDate = Date.today().addMonths(2);
        data.parentCamp.fyType = 'CSA';
        data.fips = new List<String>();
        data.fips.add(lstFIPS[0].id);
        
        KinshipFiscalYearController.CampData child = new KinshipFiscalYearController.CampData();
        child.startDate = Date.today().addMonths(-1);
        child.endDate = Date.today().addMonths(1);
        child.isSendDueDate = false;
        data.lstChild.add(child);
        Test.startTest();
        data = KinshipFiscalYearController.saveFiscalYear(data);
        Test.stopTest();

        System.assertNotEquals(null, data, 'The method is return null value');
    }
    
    @isTest
    private static void testSaveFiscalYear_DSS(){
        List<FIPS__c> lstFIPS = [SELECT id, name FROM FIPS__c];
        Campaign fy = new Campaign(StartDate = Date.today().addMonths(-4), EndDate = Date.today().addMonths(4),
                                  	Fiscal_Year_Type__c = 'Local', FIPS__c = lstFIPS[0].id, Name = 'FY 2022');
        insert fy;
        KinshipFiscalYearController.FiscalYearData data = KinshipFiscalYearController.initData();
        data.parentCamp.startDate = Date.today().addMonths(-2);
        data.parentCamp.endDate = Date.today().addMonths(2);
        data.parentCamp.fyType = 'DSS';
        data.fips.add(lstFIPS[0].id);
        
        KinshipFiscalYearController.CampData child = new KinshipFiscalYearController.CampData();
        child.startDate = Date.today().addMonths(-1);
        child.endDate = Date.today().addMonths(1);
        child.isSendDueDate = false;
        data.lstChild = new List<KinshipFiscalYearController.CampData>();
        data.lstChild.add(child);
        Test.startTest();
        data = KinshipFiscalYearController.saveFiscalYear(data);
        Test.stopTest();

        System.assertNotEquals(null, data, 'The method is return null value');
    }
}