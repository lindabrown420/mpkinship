@IsTest
public class CategoryRateChangeBatchTest {
    public static testmethod void catratechangemethod(){
        contact con = new contact();       
        con.LastName='contactrec';
        con.FirstName='test';
        con.Email='test@contact.com';
        insert con;
      
        Kinship_Case__c caserec1 = new Kinship_Case__c();
        caserec1.Name='test one';
        caserec1.Primary_Contact__c=con.id;
        caserec1.Hispanic_Ethnicity__c='1';
        caserec1.DSM_V_Indicator_ICD_10_1__c='1';
        caserec1.Gender_Preference__c='F';
        caserec1.Race__c='1';
        caserec1.Autism_Flag_1__c='1';
        caserec1.DSM_V_Indicator_ICD_10_1__c='1';
        caserec1.Clinical_Medication_Indicator_1__c='1';
        caserec1.Medicaid_Indicator_1__c='1';
        caserec1.OASIS_Client_ID__c='222';
        caserec1.Referral_Source__c='2';
        caserec1.InActive__c=false;
        caserec1.DOB__c=date.today().addMonths(-1);
        insert caserec1;
        Service_Names_Code__c sc = new Service_Names_Code__c();
         sc.name='test';
         sc.Service_Name_Code__c='2';
         insert sc;  
         Locality_Account__c la = new Locality_Account__c();
        la.Name='222';
        insert la;
        Category__c category = new Category__c();
        category.Name='222';
        category.Category_Type__c='IVE';
        category.Locality_Account__c=la.id;
        category.Service_Names_Code__c=sc.id;
        category.Inactive__c=false;
        category.RecordTypeId= Schema.SObjectType.category__C.getRecordTypeInfosByDeveloperName().get('LASER_Category').getRecordTypeId();
        insert category;   
         
        Category_Rate__c cr = new Category_Rate__c();
        cr.Category__c = category.id;
        cr.Status__c='Active';
        cr.Price__c=5000;            
        cr.Effective_Date__c=system.today();
        cr.Max_Age__c=3;
        cr.Category__c=category.id;
        cr.DOB_Effective_Date__c='1st of the following month following Date of Birth';
        cr.Prorate_First_Payment__c=true;
        cr.Prorate_final_payment__c=true;
        cr.Min_Age__c=0;
        cr.RecordTypeId = Schema.SObjectType.Category_Rate__c.getRecordTypeInfosByDeveloperName().get('Service_Names').getRecordTypeId();
        insert cr;
        Category_Rate__c cr1 = new Category_Rate__c();
        cr1.Category__c = category.id;
        cr1.Status__c='Active';
        cr1.Price__c=5000;            
        cr1.Effective_Date__c=system.today();
        cr1.Max_Age__c=6;
        cr1.Category__c=category.id;
        cr1.DOB_Effective_Date__c='1st of the following month following Date of Birth';
        cr1.Prorate_First_Payment__c=true;
        cr1.Prorate_final_payment__c=true;
        cr1.Min_Age__c=4;
        cr1.RecordTypeId = Schema.SObjectType.Category_Rate__c.getRecordTypeInfosByDeveloperName().get('Service_Names').getRecordTypeId();
        insert cr1;    
         account accountrec = new account();
        accountrec.Name='test vendor';
        accountrec.Tax_ID__c='333-33-3333';
        accountrec.Entity_Type__c='Individual';
        insert accountrec;
        
        Day_of_Month_For_Rates__c dm = new Day_of_Month_For_Rates__c();
        dm.Day_of_Month__c=date.today().day();
        insert dm;
        opportunity opp = new opportunity();
        opp.recordtypeid=Schema.SObjectType.opportunity.getRecordTypeInfosByDeveloperName().get('Case_Actions').getRecordTypeId();
        date dateval = date.newInstance(2020, 03, 01);
        opp.CloseDate=dateval;        
        opp.End_Date__c =date.today().addmonths(3);
        opp.name='test opp';
        opp.StageName='draft';
        opp.AccountId=accountrec.id;
        opp.Kinship_Case__c=caserec1.id;
        opp.Category__c=category.id;
        opp.Type_of_Case_Action__c='Ongoing';
        opp.Day_of_Month__c='1st';
        opp.Amount=3000;
        opp.Prorate_First_Payment__c=true;
        opp.Prorate_Last_Payment__c=true;
        opp.Prorated_First_Payment_Amount__c=400;
        opp.Prorated_Last_Payment_Amount__c=500;
        opp.Number_of_Months_Units__c=4;      
        insert opp;
        Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitrequest();
        app.setObjectId(opp.Id);    
         // Submit the approval request for the po    
        Approval.ProcessResult result = Approval.process(app);    
        // Verify that the results are as expected    
        System.assert(result.isSuccess());
       /* opprecord.name='testoneopportunity';
        update opprecord; */
        //system.assertEquals(opprecord.StageName,'Approved');  
        if(result.isSuccess()){
            opp.stagename='Approved';
            update opp;
        } 
        opp.StageName='Open';
        update opp; 
        Payment__c np = new Payment__c();
        np.Service_Begin_Date__c=opp.CloseDate;
        date servicedateval = date.newInstance(2020, 03, 31);
        np.Service_End_Date__c=servicedateval;
         np.Opportunity__c=opp.id;
        np.Payment_Amount__c=400;
        np.Invoice_Stage__c='Ready to Pay';
        np.Category_lookup__c=category.id;
        insert np;
        Payment__c np1 = new Payment__c();
        date servicebegindateval2=date.newInstance(2020, 04, 1);
        np1.Service_Begin_Date__c=servicebegindateval2;
         np1.Opportunity__c=opp.id;
        np1.Category_lookup__c=category.id;
         date servicedateval2 = date.newInstance(2020, 04, 30);
        np1.Service_End_Date__c=servicedateval;
        np1.Payment_Amount__c=3000;
        np1.Invoice_Stage__c='Ready to Pay';
        insert np1;
         Payment__c np2 = new Payment__c();
        date servicebegindateval3=date.newInstance(2020, 05, 1);
        np2.Service_Begin_Date__c=servicebegindateval3;
        date servicedateval3 = date.newInstance(2020, 05, 31);
        np2.Service_End_Date__c=servicedateval3;
        np2.Payment_Amount__c=3000;
        np2.Opportunity__c=opp.id;
        np2.Category_lookup__c=category.id;
        np2.Invoice_Stage__c='Ready to Pay';
        insert np2;
        Payment__c np3 = new Payment__c();
        date servicebegindateval4=date.newInstance(2020, 06, 1);
        np3.Service_Begin_Date__c=servicebegindateval4;
        np3.Service_End_Date__c=opp.End_Date__c;
        np3.Payment_Amount__c=500;
        np3.Opportunity__c=opp.id;
        np3.Category_lookup__c=category.id;
        np3.Invoice_Stage__c='Ready to Pay';
        insert np3;
       
        Test.startTest();

            CategoryRateChangeBatch obj = new CategoryRateChangeBatch();
            DataBase.executeBatch(obj); 
            
        Test.stopTest();
    }
    
    public static testmethod void catratechangemethod1(){
        contact con = new contact();
        //con.recordtypeid=recType;
        con.LastName='contactrec';
        con.FirstName='test';
        con.Email='test@contact.com';
        //con.Race_of_the_Foster_Parent__c='White';
        //con.Gender_Preference__c='Male';
        insert con;
      //  for(integer i=0;i<10;i++){
       /* Kinship_Case__c caserec = new Kinship_Case__c();
        caserec.Name='test';
        caserec.Primary_Contact__c=con.id;
        caserec.Hispanic_Ethnicity__c='1';
        caserec.DSM_V_Indicator_ICD_10_1__c='1';
        caserec.Gender_Preference__c='M';
        caserec.Race__c='1';
        caserec.Autism_Flag_1__c='1';
        caserec.DSM_V_Indicator_ICD_10_1__c='1';
        caserec.Clinical_Medication_Indicator_1__c='1';
        caserec.Medicaid_Indicator_1__c='1';
        caserec.Referral_Source__c='2';
        caserec.DOB__c=date.today().addMonths(-1);
        caserec.InActive__c=false;
        insert caserec; */
        Kinship_Case__c caserec1 = new Kinship_Case__c();
        caserec1.Name='test';
        caserec1.Primary_Contact__c=con.id;
        caserec1.Hispanic_Ethnicity__c='1';
        caserec1.DSM_V_Indicator_ICD_10_1__c='1';
        caserec1.Gender_Preference__c='M';
        caserec1.Race__c='1';
        caserec1.Autism_Flag_1__c='1';
        caserec1.DSM_V_Indicator_ICD_10_1__c='1';
        caserec1.Clinical_Medication_Indicator_1__c='1';
        caserec1.Medicaid_Indicator_1__c='1';
        caserec1.OASIS_Client_ID__c='222';
        caserec1.Referral_Source__c='2';
        caserec1.InActive__c=false;
        caserec1.DOB__c=date.today().addMonths(-1);
        insert caserec1;
        Service_Names_Code__c sc = new Service_Names_Code__c();
         sc.name='test';
         sc.Service_Name_Code__c='2';
         insert sc;   
         Locality_Account__c la = new Locality_Account__c();
        la.Name='444';
        insert la;
        Category__c category = new Category__c();
        category.Name='224';
        category.Locality_Account__c=la.id;
        category.Category_Type__c='IVE';
        category.Service_Names_Code__c=sc.id;
        category.RecordTypeId= Schema.SObjectType.category__C.getRecordTypeInfosByDeveloperName().get('LASER_Category').getRecordTypeId();
        insert category;   
         
        Category_Rate__c cr = new Category_Rate__c();
        cr.Category__c = category.id;
        cr.Status__c='Active';
        cr.Price__c=5000;            
        cr.Effective_Date__c=system.today();
        cr.Max_Age__c=3;
        cr.Category__c=category.id;
        cr.DOB_Effective_Date__c='1st of the following month following Date of Birth';
        cr.Prorate_First_Payment__c=true;
        cr.Prorate_final_payment__c=true;
        cr.Min_Age__c=0;
        cr.RecordTypeId = Schema.SObjectType.Category_Rate__c.getRecordTypeInfosByDeveloperName().get('Service_Names').getRecordTypeId();
        insert cr;
        Category_Rate__c cr1 = new Category_Rate__c();
        cr1.Category__c = category.id;
        cr1.Status__c='Active';
        cr1.Price__c=5000;            
        cr1.Effective_Date__c=system.today();
        cr1.Max_Age__c=6;
        cr1.Category__c=category.id;
        cr1.DOB_Effective_Date__c='1st of the following month following Date of Birth';
        cr1.Prorate_First_Payment__c=true;
        cr1.Prorate_final_payment__c=true;
        cr1.Min_Age__c=4;
        cr1.RecordTypeId = Schema.SObjectType.Category_Rate__c.getRecordTypeInfosByDeveloperName().get('Service_Names').getRecordTypeId();
        insert cr1;    
         account accountrec = new account();
        accountrec.Name='test vendor';
        accountrec.Tax_ID__c='333-33-3333';
        accountrec.Entity_Type__c='Individual';
        insert accountrec;
        Day_of_Month_For_Rates__c dm = new Day_of_Month_For_Rates__c();
        dm.Day_of_Month__c=date.today().day();
        insert dm;
        opportunity opp = new opportunity();
        opp.recordtypeid=Schema.SObjectType.opportunity.getRecordTypeInfosByDeveloperName().get('Case_Actions').getRecordTypeId();
        date dateval = date.newInstance(2020, 03, 01);
        opp.CloseDate=dateval;        
        opp.End_Date__c=date.today().addmonths(3);
        opp.name='test opp';
        opp.StageName='draft';
        opp.AccountId=accountrec.id;
        opp.Kinship_Case__c=caserec1.id;
        opp.Category__c=category.id;
        opp.Type_of_Case_Action__c='One time';
        opp.Day_of_Month__c='1st';
        opp.Amount=3000;
        opp.Prorate_First_Payment__c=true;
        opp.Prorate_Last_Payment__c=true;
        opp.Prorated_First_Payment_Amount__c=400;
        opp.Prorated_Last_Payment_Amount__c=500;
        opp.Number_of_Months_Units__c=4;      
        insert opp;
        Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitrequest();
        app.setObjectId(opp.Id);    
         // Submit the approval request for the po    
        Approval.ProcessResult result = Approval.process(app);    
        // Verify that the results are as expected    
        System.assert(result.isSuccess());
       /* opprecord.name='testoneopportunity';
        update opprecord; */
        //system.assertEquals(opprecord.StageName,'Approved');  
        if(result.isSuccess()){
            opp.stagename='Approved';
            update opp;
        } 
        opp.StageName='Open';
        update opp; 
        Payment__c np = new Payment__c();
        np.Service_Begin_Date__c=opp.CloseDate;
        np.Service_End_Date__c=opp.End_Date__c;
        np.Opportunity__c=opp.id;
        np.Payment_Amount__c=400;
        np.Invoice_Stage__c='Ready to Pay';
        np.Category_lookup__c=category.id;
        insert np;
        
        Test.startTest();

            CategoryRateChangeBatch obj = new CategoryRateChangeBatch();
            DataBase.executeBatch(obj); 
            
        Test.stopTest();
    }
     public static testmethod void catratechangemethod2(){
        contact con = new contact();
        //con.recordtypeid=recType;
        con.LastName='contactrec';
        con.FirstName='test';
        con.Email='test@contact.com';
        //con.Race_of_the_Foster_Parent__c='White';
        //con.Gender_Preference__c='Male';
        insert con;
      
        Kinship_Case__c caserec1 = new Kinship_Case__c();
        caserec1.Name='test';
        caserec1.Primary_Contact__c=con.id;
        caserec1.Hispanic_Ethnicity__c='1';
        caserec1.DSM_V_Indicator_ICD_10_1__c='1';
        caserec1.Gender_Preference__c='M';
        caserec1.Race__c='1';
        caserec1.Autism_Flag_1__c='1';
        caserec1.DSM_V_Indicator_ICD_10_1__c='1';
        caserec1.Clinical_Medication_Indicator_1__c='1';
        caserec1.Medicaid_Indicator_1__c='1';
        caserec1.OASIS_Client_ID__c='222';
        caserec1.Referral_Source__c='2';
        caserec1.InActive__c=false;
        caserec1.DOB__c=date.today().addMonths(-1);
        insert caserec1;
        Service_Names_Code__c sc = new Service_Names_Code__c();
         sc.name='test';
         sc.Service_Name_Code__c='2';
         insert sc;  
         Locality_Account__c la = new Locality_Account__c();
        la.Name='444';
        insert la;
        Category__c category = new Category__c();
        category.Name='222';
         category.Locality_Account__c=la.id;
        category.Category_Type__c='IVE';
        category.Service_Names_Code__c=sc.id;
        category.RecordTypeId= Schema.SObjectType.category__C.getRecordTypeInfosByDeveloperName().get('LASER_Category').getRecordTypeId();
        insert category;   
         
        Category_Rate__c cr = new Category_Rate__c();
        cr.Category__c = category.id;
        cr.Status__c='Active';
        cr.Price__c=5000;            
        cr.Effective_Date__c=system.today();
        cr.Max_Age__c=3;
        cr.Category__c=category.id;
        cr.DOB_Effective_Date__c='1st of the following month following Date of Birth';
        cr.Prorate_First_Payment__c=true;
        cr.Prorate_final_payment__c=true;
        cr.Min_Age__c=0;
        cr.RecordTypeId = Schema.SObjectType.Category_Rate__c.getRecordTypeInfosByDeveloperName().get('Service_Names').getRecordTypeId();
        insert cr;
        Category_Rate__c cr1 = new Category_Rate__c();
        cr1.Category__c = category.id;
        cr1.Status__c='Active';
        cr1.Price__c=5000;            
        cr1.Effective_Date__c=system.today();
        cr1.Max_Age__c=6;
        cr1.Category__c=category.id;
        cr1.DOB_Effective_Date__c='1st of the following month following Date of Birth';
        cr1.Prorate_First_Payment__c=true;
        cr1.Prorate_final_payment__c=true;
        cr1.Min_Age__c=4;
        cr1.RecordTypeId = Schema.SObjectType.Category_Rate__c.getRecordTypeInfosByDeveloperName().get('Service_Names').getRecordTypeId();
        insert cr1;    
         account accountrec = new account();
        accountrec.Name='test vendor';
        accountrec.Tax_ID__c='333-33-3333';
        accountrec.Entity_Type__c='Individual';
        insert accountrec;
         Day_of_Month_For_Rates__c dm = new Day_of_Month_For_Rates__c();
        dm.Day_of_Month__c=date.today().day();
        insert dm;
        opportunity opp = new opportunity();
        opp.recordtypeid=Schema.SObjectType.opportunity.getRecordTypeInfosByDeveloperName().get('Case_Actions').getRecordTypeId();
        date dateval = date.newInstance(2020, 03, 01);
        opp.CloseDate=dateval;        
        opp.End_Date__c=date.today().addmonths(3);
        opp.name='test opp';
        opp.StageName='draft';
        opp.AccountId=accountrec.id;
        opp.Kinship_Case__c=caserec1.id;
        opp.Category__c=category.id;
        opp.Type_of_Case_Action__c='Ongoing';
        opp.Day_of_Month__c='1st';
        opp.Amount=3000;
        opp.Prorate_First_Payment__c=true;
        opp.Prorate_Last_Payment__c=true;
        opp.Prorated_First_Payment_Amount__c=400;
        opp.Prorated_Last_Payment_Amount__c=500; 
        opp.Number_of_Months_Units__c=4;
        insert opp;
        Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitrequest();
        app.setObjectId(opp.Id);    
         // Submit the approval request for the po    
        Approval.ProcessResult result = Approval.process(app);    
        // Verify that the results are as expected    
        System.assert(result.isSuccess());
       /* opprecord.name='testoneopportunity';
        update opprecord; */
        //system.assertEquals(opprecord.StageName,'Approved');  
        if(result.isSuccess()){
            opp.stagename='Approved';
            update opp;
        } 
        opp.StageName='Open';
        update opp; 
        Payment__c np = new Payment__c();
        np.Service_Begin_Date__c=opp.CloseDate;
        np.Service_End_Date__c=opp.End_Date__c;
        np.Opportunity__c=opp.id;
        np.Payment_Amount__c=400;
         np.Category_lookup__c=category.id;
        np.Invoice_Stage__c='Ready to Pay';
        insert np;
        
       /*  list<npe01__OppPayment__c> payments=[select id,name,FIPS_Code__c,Invoice_Stage__c,Service_Begin_Date__c,Service_End_Date__c,npe01__Payment_Amount__c,npe01__Paid__c,npe01__Written_Off__c from npe01__OppPayment__c where npe01__Opportunity__c=:opp.id];  
        system.debug('**payments size'+payments.size());
        system.debug('**payment zero servicebegin date and enddate'+payments[0].Service_Begin_Date__c);
        system.debug('**payment zero service end date '+payments[0].Service_End_Date__c);
        caserec1.DOB__c=date.today().addMonths(-1);
            update caserec1; */
        //}
        Test.startTest();

            CategoryRateChangeBatch obj = new CategoryRateChangeBatch();
            DataBase.executeBatch(obj); 
            
        Test.stopTest();
    }
     public static testmethod void catratechangemethod3(){
        contact con = new contact();
        //con.recordtypeid=recType;
        con.LastName='contactrec';
        con.FirstName='test';
        con.Email='test@contact.com';
        //con.Race_of_the_Foster_Parent__c='White';
        //con.Gender_Preference__c='Male';
        insert con;
      //  for(integer i=0;i<10;i++){
       /* Kinship_Case__c caserec = new Kinship_Case__c();
        caserec.Name='test';
        caserec.Primary_Contact__c=con.id;
        caserec.Hispanic_Ethnicity__c='1';
        caserec.DSM_V_Indicator_ICD_10_1__c='1';
        caserec.Gender_Preference__c='M';
        caserec.Race__c='1';
        caserec.Autism_Flag_1__c='1';
        caserec.DSM_V_Indicator_ICD_10_1__c='1';
        caserec.Clinical_Medication_Indicator_1__c='1';
        caserec.Medicaid_Indicator_1__c='1';
        caserec.Referral_Source__c='2';
        caserec.DOB__c=date.today().addMonths(-1);
        caserec.InActive__c=false;
        insert caserec; */
        Kinship_Case__c caserec1 = new Kinship_Case__c();
        caserec1.Name='test';
        caserec1.Primary_Contact__c=con.id;
        caserec1.Hispanic_Ethnicity__c='1';
        caserec1.DSM_V_Indicator_ICD_10_1__c='1';
        caserec1.Gender_Preference__c='M';
        caserec1.Race__c='1';
        caserec1.Autism_Flag_1__c='1';
        caserec1.DSM_V_Indicator_ICD_10_1__c='1';
        caserec1.Clinical_Medication_Indicator_1__c='1';
        caserec1.Medicaid_Indicator_1__c='1';
        caserec1.OASIS_Client_ID__c='222';
        caserec1.Referral_Source__c='2';
        caserec1.InActive__c=false;
        caserec1.DOB__c=date.today().addMonths(-1);
        insert caserec1; 
        Service_Names_Code__c sc = new Service_Names_Code__c();
         sc.name='test';
         sc.Service_Name_Code__c='2';
         insert sc; 
         Locality_Account__c la = new Locality_Account__c();
        la.Name='444';
        insert la;
        Category__c category = new Category__c();
        category.Name='222';
        category.Locality_Account__c=la.id;
        category.Category_Type__c='IVE';
        category.Service_Names_Code__c=sc.id;
        category.RecordTypeId= Schema.SObjectType.category__C.getRecordTypeInfosByDeveloperName().get('LASER_Category').getRecordTypeId();
        insert category;   
        Day_of_Month_For_Rates__c dm = new Day_of_Month_For_Rates__c();
        dm.Day_of_Month__c=date.today().day();
        insert dm; 
        Category_Rate__c cr = new Category_Rate__c();
        cr.Category__c = category.id;
        cr.Status__c='Active';
        cr.Price__c=5000;            
        cr.Effective_Date__c=system.today();
        cr.Max_Age__c=3;
        cr.Category__c=category.id;
        cr.DOB_Effective_Date__c='1st of the following month following Date of Birth';
        cr.Prorate_First_Payment__c=true;
        cr.Prorate_final_payment__c=true;
        cr.Min_Age__c=0;
        cr.RecordTypeId = Schema.SObjectType.Category_Rate__c.getRecordTypeInfosByDeveloperName().get('Service_Names').getRecordTypeId();
        insert cr;
        Category_Rate__c cr1 = new Category_Rate__c();
        cr1.Category__c = category.id;
        cr1.Status__c='Active';
        cr1.Price__c=5000;            
        cr1.Effective_Date__c=system.today();
        cr1.Max_Age__c=6;
        cr1.Category__c=category.id;
        cr1.DOB_Effective_Date__c='1st of the following month following Date of Birth';
        cr1.Prorate_First_Payment__c=true;
        cr1.Prorate_final_payment__c=true;
        cr1.Min_Age__c=4;
        cr1.RecordTypeId = Schema.SObjectType.Category_Rate__c.getRecordTypeInfosByDeveloperName().get('Service_Names').getRecordTypeId();
        insert cr1;    
         account accountrec = new account();
        accountrec.Name='test vendor';
        accountrec.Tax_ID__c='333-33-3333';
        accountrec.Entity_Type__c='Individual';
        insert accountrec;
        opportunity opp = new opportunity();
        opp.recordtypeid=Schema.SObjectType.opportunity.getRecordTypeInfosByDeveloperName().get('Case_Actions').getRecordTypeId();
        date dateval = date.newInstance(2020, 03, 03);
        opp.CloseDate=dateval;        
        opp.End_Date__c=date.today().addmonths(3);
        opp.name='test opp';
        opp.StageName='draft';
        opp.AccountId=accountrec.id;
        opp.Kinship_Case__c=caserec1.id;
        opp.Category__c=category.id;
        opp.Type_of_Case_Action__c='Ongoing';
        opp.Day_of_Month__c='1st';
        opp.Amount=3000;
        opp.Prorate_First_Payment__c=true;
        opp.Prorate_Last_Payment__c=true;
        opp.Prorated_First_Payment_Amount__c=400;
        opp.Prorated_Last_Payment_Amount__c=500;
        opp.Number_of_Months_Units__c=4;       
        insert opp;
        Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitrequest();
        app.setObjectId(opp.Id);    
         // Submit the approval request for the po    
        Approval.ProcessResult result = Approval.process(app);    
        // Verify that the results are as expected    
        System.assert(result.isSuccess());
       /* opprecord.name='testoneopportunity';
        update opprecord; */
        //system.assertEquals(opprecord.StageName,'Approved');  
        if(result.isSuccess()){
            opp.stagename='Approved';
            update opp;
        } 
        opp.StageName='Open';
        update opp; 
        Payment__c np = new Payment__c();
        np.Service_Begin_Date__c=opp.CloseDate;
        np.Service_End_Date__c=opp.End_Date__c;
        np.Opportunity__c=opp.id;
        np.Payment_Amount__c=400;
        np.Category_lookup__c=category.id;
        np.Invoice_Stage__c='Ready to Pay';
        insert np;
        
       /*  list<npe01__OppPayment__c> payments=[select id,name,FIPS_Code__c,Invoice_Stage__c,Service_Begin_Date__c,Service_End_Date__c,npe01__Payment_Amount__c,npe01__Paid__c,npe01__Written_Off__c from npe01__OppPayment__c where npe01__Opportunity__c=:opp.id];  
        system.debug('**payments size'+payments.size());
        system.debug('**payment zero servicebegin date and enddate'+payments[0].Service_Begin_Date__c);
        system.debug('**payment zero service end date '+payments[0].Service_End_Date__c);
        caserec1.DOB__c=date.today().addMonths(-1);
            update caserec1; */
        //}
        Test.startTest();

            CategoryRateChangeBatch obj = new CategoryRateChangeBatch();
            DataBase.executeBatch(obj); 
            
        Test.stopTest();
    }
    
     public static testmethod void catratechangemethod4(){
        
        Test.startTest();

            CategoryRateChangeBatch obj = new CategoryRateChangeBatch();
            DataBase.executeBatch(obj); 
            
        Test.stopTest();
    }
}