@isTest
public class CheckTrgHelperTest {
    @testSetup
    static void testRecords(){
		id recType = Schema.SObjectType.contact.getRecordTypeInfosByDeveloperName().get('Contacts').getRecordTypeId();
        contact con = new contact();
        con.recordtypeid=recType;
        con.LastName='contactrec';
        con.FirstName='test';
        con.Email='test@contact.com';        
        insert con;
        
        Kinship_Case__c caserec = new Kinship_Case__c();
        caserec.Primary_Contact__c=con.id;
        insert caserec;
        
        //id vendorrecType = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Vendor').getRecordTypeId();
        account accountrec = new account();
        //accountrec.recordtypeid=vendorrecType;
        accountrec.Name='test vendor';
        accountrec.Tax_ID__c='222-22-3333';
        accountrec.Entity_Type__c='Individual';
        insert accountrec;
        
         Locality_Account__c la = new Locality_Account__c();
        la.Name='222';
        insert la;
        
        Category__c category = new Category__c();
        category.Name='222';
        category.Locality_Account__c =la.id;
        category.Category_Type__c='Adoption';
        category.RecordTypeId= Schema.SObjectType.category__C.getRecordTypeInfosByDeveloperName().get('LASER_Category').getRecordTypeId();
        insert category;  
        
        TriggerActivation__c  ta = new TriggerActivation__c ();
        ta.name='PaymentTrigger';
        ta.isActive__c=true;
        insert ta;
        TriggerActivation__c  ta1 = new TriggerActivation__c();
        ta1.name='PurchaseOrder Trigger';
        ta1.isActive__c=true;
        insert ta1;
        
        FIPS__c fp = new FIPS__c();
        fp.name='test fips';
        insert fp;
        id recTypeid = Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Fiscal_Year').getRecordTypeId();
        campaign cap = new campaign();
        cap.FIPS__c=fp.id;
        cap.RecordTypeId=recTypeid;
        cap.Fiscal_Year_Type__c='Local';
        cap.name='test';
        cap.Begin_Date__c=system.today().addmonths(-1);
        cap.EndDate=system.today().addyears(1);
        insert cap;
        Check_Run__c cr = new Check_Run__c();
        cr.Check_Date__c =system.today();
        cr.Number_Of_Check__c=1;
        insert cr;
        
        Kinship_Check__c kc = new Kinship_Check__c();
        kc.Check_Run__c=cr.id;
        kc.Issue_Date__c=system.today();
        insert kc;
    }        
   
    
      public static testmethod void Test3(){
      list<TriggerActivation__c> talist=[select id,name,isActive__C from TriggerActivation__c where isActive__c=:true 
                                           and name=:'PaymentTrigger'];
        list<TriggerActivation__c> talist1=[select id,name,isActive__C from TriggerActivation__c where isActive__c=:true 
                                            and name=:'PurchaseOrder Trigger'];
        TriggerActivation__c talist2 = new TriggerActivation__c();
        talist2.name='Check Trigger';
        talist2.isActive__c=true;
        insert talist2;
        
        list<Kinship_Case__c> cases=[select id from Kinship_Case__c];
        list<account> accounts=[select id,recordtypeid from account where name='test vendor'];   
        Locality_Account__c la = new Locality_Account__c();
        la.Name='222';
        insert la;
        
        Category__c category = new Category__c();
        category.Name='222';
        category.Locality_Account__c =la.id;
        category.Category_Type__c='Companion Payroll';
        category.RecordTypeId= Schema.SObjectType.category__C.getRecordTypeInfosByDeveloperName().get('LASER_Category').getRecordTypeId();
        insert category; 
       
         Campaign parent = new Campaign();
            parent.name = '2021';
            parent.StartDate = Date.Today().addDays(-10);
            parent.EndDate = Date.Today().addDays(10);
            parent.Fiscal_Year_Type__c = 'Local';
            parent.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId();
            insert parent;
        list<Category__c> categories=[select id from Category__c];
         //insert a new product
        Product2 p = new product2(name='x',isactive=true, Unit_Measure__c = '10');
        insert p;
        
        Product2 p2 = new product2(name='y',isactive=true, Unit_Measure__c = '10');
        insert p2;
        
        //define the standart price for the product
        id stdPb = Test.getStandardPricebookId();
        insert new PricebookEntry(pricebook2id = stdPb, product2id = p.id,unitprice=1.0, isActive=true);
        
        Pricebook2 pb = new pricebook2(name='test',Vendor__c=accounts[0].id,IsActive=true,
                                       Effective_Date__c=system.today().addmonths(-2), Accounting_Period__c = parent.id );
        insert pb;
        
        PricebookEntry pbe = new PricebookEntry(pricebook2id=pb.id, product2id=p.id,unitprice=1.0, isActive=true);
        insert pbe; 
        opportunity opprecord = new opportunity();
        opprecord.CloseDate=date.today();
        opprecord.End_date__C=date.today().addmonths(2);
        opprecord.StageName='Draft';
        opprecord.Name='test opportunity';
        opprecord.Kinship_Case__c=cases[0].id;
        opprecord.Amount=20000;
        opprecord.Category__c =categories[0].id;
        opprecord.AccountId=accounts[0].id;
        opprecord.Number_of_Months_Units__c=3;
        opprecord.Pricebook2Id =pb.id;        
        insert opprecord;
        //NOW CREATING LINE ITEMS
        opportunitylineitem opplineitem = new opportunitylineitem();
        opplineitem.Unit_Measure__c='1';
        opplineitem.Product2Id=p.id;
        opplineitem.PricebookEntryId=pbe.id;
        opplineitem.Quantity=2;
        opplineitem.OpportunityId=opprecord.id;
        opplineitem.TotalPrice = 2000;
        opplineitem.category__C = category.id;
        opplineitem.Month_Units__c='2020-07:2,2020-08:4';
        insert opplineitem;
        //Create an approval request for the po
        POSO_Created_Date__c POSOcs = new POSO_Created_Date__c();
        POSOcs.Created_Date__c=date.today().adddays(-2);
        insert POSOcs;
        Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitrequest();
        app.setObjectId(opprecord.Id);    
         // Submit the approval request for the po 
           
        Approval.ProcessResult result = Approval.process(app);    
        // Verify that the results are as expected    
        System.assert(result.isSuccess());
      
        //system.assertEquals(opprecord.StageName,'Approved');  
        test.startTest();
        if(result.isSuccess()){
            opprecord.stagename='Approved';
            update opprecord;
        } 	 
        Tax_Rates__c tr = new Tax_Rates__c();
         tr.Type__c='FICA';
         TR.Employee_Rate__c=10;
         TR.Active__c=TRUE;
         INSERT TR;
        Claims__c cc = new Claims__c();
        cc.Payable_to__c=accounts[0].id;
        cc.Case__c=cases[0].id;
        cc.Claim_Type__c='CSA';
        insert cc;
 		
        Payment__c payment = new Payment__c();
        payment.Opportunity__c=opprecord.id;
        payment.Invoice_Stage__c='Ready to Pay';
        payment.Category_lookup__c=category.id;
        payment.Amount_Paid__c=3000;
        payment.Payment_Amount__c=3000;
         payment.Employee_Rate__c=20;
        insert payment;
       
      
            Check_Run__c cr = new Check_Run__c();
            cr.Check_Date__c =system.today();
            cr.Number_Of_Check__c=1;
            insert cr;
            
            Kinship_Check__c kc = new Kinship_Check__c();
            kc.Check_Run__c=cr.id;
            kc.Issue_Date__c=system.today();
            insert kc;
         
            //Payment__c payrec = new Payment__c();
          list<Payment__c> payrec=[select id,paid__c,employee_Rate__C,invoice_stage__c,Kinship_Check__c,category_lookup__c,category_lookup__R.name from payment__C where id =:payment.id];
          payrec[0].Paid__c=true;
          payrec[0].Category_lookup__c=category.id;
          payrec[0].Invoice_Stage__c='Paid';
          payrec[0].Kinship_Check__C=kc.id;
          update payrec[0];
           test.stoptest();  
          /*  payrec.id = payment.id;
            payrec.Payment_Amount__c=2000;
            payrec.Paid__c=true;
            payrec.Kinship_Check__c=kc.id;
            payrec.Invoice_Stage__c='Paid';
            update payrec; */
            
         
            list<Kinship_Check__c> check =[select id,Status__c from Kinship_Check__c where id=:kc.id];
            Kinship_Check__c checkupdate = new Kinship_Check__c();
            checkupdate.id=check[0].id;
            checkupdate.Status__c='Cancelled';
            update checkupdate;
           
           list<Payment__c> payments=[select id,FICA_Payment_Id__c from Payment__c where FICA_Payment_Id__c=:payrec[0].id];
          system.debug('**Val of payments'+payments);
         //system.assertEquals(4, payments.size());
            
       } 
     public static testmethod void Test1(){
      list<TriggerActivation__c> talist=[select id,name,isActive__C from TriggerActivation__c where isActive__c=:true 
                                           and name=:'PaymentTrigger'];
        list<TriggerActivation__c> talist1=[select id,name,isActive__C from TriggerActivation__c where isActive__c=:true 
                                            and name=:'PurchaseOrder Trigger'];
        TriggerActivation__c talist2 = new TriggerActivation__c();
        talist2.name='Check Trigger';
        talist2.isActive__c=true;
        insert talist2;
        
        list<Kinship_Case__c> cases=[select id from Kinship_Case__c];
        list<account> accounts=[select id,recordtypeid from account where name='test vendor'];        
        list<Category__c> categories=[select id from Category__c];
          //insert a new product
        Product2 p = new product2(name='x',isactive=true,Unit_Measure__c='1');
        insert p;
        
        Product2 p2 = new product2(name='y',isactive=true,Unit_Measure__c='1');
        insert p2;
        list<fips__C> fip=[select id,name from fips__C];
        list<campaign> camp=[select id,fips__c,Begin_Date__c,enddate,Fiscal_Year_Type__c from campaign];
        //define the standart price for the product
        id stdPb = Test.getStandardPricebookId();
        insert new PricebookEntry(pricebook2id = stdPb, product2id = p.id,unitprice=1.0, isActive=true);
        
        Pricebook2 pb = new pricebook2(name='test',Vendor__c=accounts[0].id,IsActive=true,
                                       accounting_period__c=camp[0].id);
        insert pb;
        
        PricebookEntry pbe = new PricebookEntry(pricebook2id=pb.id, product2id=p.id,unitprice=1.0, isActive=true);
        insert pbe; 
        id recType = Schema.SObjectType.opportunity.getRecordTypeInfosByDeveloperName().get('Purchase_Order').getRecordTypeId();
       test.starttest();
        opportunity opprecord = new opportunity();
        opprecord.CloseDate=date.today();
        opprecord.StageName='Draft';
        opprecord.Name='test opportunity';
        opprecord.Kinship_Case__c=cases[0].id;
        opprecord.Amount=20000;
        opprecord.RecordTypeId=recType;
        opprecord.campaignid=camp[0].id;
        opprecord.Category__c =categories[0].id;
        opprecord.AccountId=accounts[0].id;
        opprecord.Number_of_Months_Units__c=3;
        opprecord.Pricebook2Id =pb.id;        
        insert opprecord;
        //NOW CREATING LINE ITEMS
        opportunitylineitem opplineitem = new opportunitylineitem();
        opplineitem.Unit_Measure__c='1';
        opplineitem.Product2Id=p.id;
        opplineitem.PricebookEntryId=pbe.id;
        opplineitem.Quantity=2;
        opplineitem.Category__c=categories[0].id;
        opplineitem.OpportunityId=opprecord.id;
        opplineitem.TotalPrice = 2000;
        insert opplineitem;
        //Create an approval request for the po
    
        Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitrequest();
        app.setObjectId(opprecord.Id);    
         // Submit the approval request for the po    
        Approval.ProcessResult result = Approval.process(app);    
        // Verify that the results are as expected    
        System.assert(result.isSuccess());
       /* opprecord.name='testoneopportunity';
        update opprecord; */
        //system.assertEquals(opprecord.StageName,'Approved');  
        if(result.isSuccess()){
            opprecord.stagename='Approved';
            update opprecord;
         test.stopTest();   
            list<Payment__c> payments=[select id,Written_Off__c,Paid__c,Invoice_Stage__c,Kinship_Check__c from Payment__c
                                                    where Opportunity__c=:opprecord.Id];
            system.debug('**VAl of payments'+payments.size());
            list<Payment_Line_Item__c> invoicelineitem =[select id,Max_Quantity_Authorized__c,Billed_Amount__c from Payment_Line_Item__c
                                                        where invoice__C=:payments[0].id];
            
            Check_Run__c cr = new Check_Run__c();
            cr.Check_Date__c =system.today();
            cr.Number_Of_Check__c=1;
            insert cr;
            
            Kinship_Check__c kc = new Kinship_Check__c();
            kc.Check_Run__c=cr.id;
            kc.Issue_Date__c=system.today();
            insert kc;
            
            Payment__c payrec = new Payment__c();
            payrec.id = payments[0].id;
            payrec.Paid__c=true;
            payrec.Kinship_Check__c=kc.id;
            payrec.Invoice_Stage__c='Paid';
            update payrec;
            
            list<Kinship_Check__c> check =[select id,Status__c from Kinship_Check__c where id=:kc.id];
            Kinship_Check__c checkupdate = new Kinship_Check__c();
            checkupdate.id=check[0].id;
            checkupdate.Status__c='Cancelled';
            update checkupdate;
            
             list<Payment__c> paymentslist=[select id from Payment__c where Opportunity__c=:opprecord.id];
            system.debug('val of paymentslist'+paymentslist.size());
            system.assertequals(5,paymentslist.size());
        } 
	}
    
    public static testmethod void Test2(){
      list<TriggerActivation__c> talist=[select id,name,isActive__C from TriggerActivation__c where isActive__c=:true 
                                           and name=:'PaymentTrigger'];
        list<TriggerActivation__c> talist1=[select id,name,isActive__C from TriggerActivation__c where isActive__c=:true 
                                            and name=:'PurchaseOrder Trigger'];
        TriggerActivation__c talist2 = new TriggerActivation__c();
        talist2.name='Check Trigger';
        talist2.isActive__c=true;
        insert talist2;
        
        list<Kinship_Case__c> cases=[select id from Kinship_Case__c];
        list<account> accounts=[select id,recordtypeid from account where name='test vendor'];        
        list<Category__c> categories=[select id from Category__c];
          //insert a new product
        Product2 p = new product2(name='x',isactive=true,Unit_Measure__c='1');
        insert p;
        
        Product2 p2 = new product2(name='y',isactive=true,Unit_Measure__c='1');
        insert p2;
        list<fips__C> fip=[select id,name from fips__C];
        list<campaign> camp=[select id,fips__c,Begin_Date__c,enddate,Fiscal_Year_Type__c from campaign];
        //define the standart price for the product
        id stdPb = Test.getStandardPricebookId();
        insert new PricebookEntry(pricebook2id = stdPb, product2id = p.id,unitprice=1.0, isActive=true);
        
        Pricebook2 pb = new pricebook2(name='test',Vendor__c=accounts[0].id,IsActive=true,
                                       accounting_period__c=camp[0].id);
        insert pb;
        
        PricebookEntry pbe = new PricebookEntry(pricebook2id=pb.id, product2id=p.id,unitprice=1.0, isActive=true);
        insert pbe; 
        id recType = Schema.SObjectType.opportunity.getRecordTypeInfosByDeveloperName().get('Purchase_Order').getRecordTypeId();
       test.starttest();
        opportunity opprecord = new opportunity();
        opprecord.CloseDate=date.today();
        opprecord.StageName='Draft';
        opprecord.Name='test opportunity';
        opprecord.Kinship_Case__c=cases[0].id;
        opprecord.Amount=20000;
        opprecord.RecordTypeId=recType;
        opprecord.campaignid=camp[0].id;
        opprecord.Category__c =categories[0].id;
        opprecord.AccountId=accounts[0].id;
        opprecord.Number_of_Months_Units__c=3;
        opprecord.Pricebook2Id =pb.id;        
        insert opprecord;
        //NOW CREATING LINE ITEMS
        opportunitylineitem opplineitem = new opportunitylineitem();
        opplineitem.Unit_Measure__c='1';
        opplineitem.Product2Id=p.id;
        opplineitem.PricebookEntryId=pbe.id;
        opplineitem.Quantity=2;
        opplineitem.Category__c=categories[0].id;
        opplineitem.OpportunityId=opprecord.id;
        opplineitem.TotalPrice = 2000;
        insert opplineitem;
        //Create an approval request for the po
    
        Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitrequest();
        app.setObjectId(opprecord.Id);    
         // Submit the approval request for the po    
        Approval.ProcessResult result = Approval.process(app);    
        // Verify that the results are as expected    
        System.assert(result.isSuccess());
       /* opprecord.name='testoneopportunity';
        update opprecord; */
        //system.assertEquals(opprecord.StageName,'Approved');  
        if(result.isSuccess()){
            opprecord.stagename='Approved';
            update opprecord;
         test.stopTest();   
            list<Payment__c> payments=[select id,Written_Off__c,Paid__c,Invoice_Stage__c,Kinship_Check__c from Payment__c
                                                    where Opportunity__c=:opprecord.Id];
            system.debug('**VAl of payments'+payments.size());
            list<Payment_Line_Item__c> invoicelineitem =[select id,Max_Quantity_Authorized__c,Billed_Amount__c from Payment_Line_Item__c
                                                        where invoice__C=:payments[0].id];
            
            Check_Run__c cr = new Check_Run__c();
            cr.Check_Date__c =system.today();
            cr.Number_Of_Check__c=1;
            insert cr;
            
            Kinship_Check__c kc = new Kinship_Check__c();
            kc.Check_Run__c=cr.id;
            kc.Issue_Date__c=system.today();
            insert kc;
            
            Payment__c payrec = new Payment__c();
            payrec.id = payments[0].id;
            payrec.Paid__c=true;
            payrec.Kinship_Check__c=kc.id;
            payrec.Invoice_Stage__c='Paid';
            update payrec;
            
            list<Kinship_Check__c> check =[select id,Status__c from Kinship_Check__c where id=:kc.id];
            Kinship_Check__c checkupdate = new Kinship_Check__c();
            checkupdate.id=check[0].id;
            checkupdate.Status__c='Cancelled & Reissued';
            update checkupdate;
            
             list<Payment__c> paymentslist=[select id from Payment__c where Opportunity__c=:opprecord.id];
            system.debug('val of paymentslist'+paymentslist.size());
             system.assertequals(5,paymentslist.size());
        } 
	}
     
}