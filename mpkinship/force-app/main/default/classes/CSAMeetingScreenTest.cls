@isTest
public class CSAMeetingScreenTest {
    @testSetup
    static void testRecords(){
        contact con = new contact();
        con.LastName='contactrec';
        con.FirstName='test';
        con.Email='test@contact.com';
        insert con;
        Kinship_Case__c caserec = new Kinship_Case__c();
        caserec.Primary_Contact__c=con.id;
        insert caserec;
        
        CSA_Agenda_Team__c csateam = new CSA_Agenda_Team__c();
        csateam.Name='test team';
        insert csateam;
        
        Team_Member__c tm = new Team_Member__c();
        tm.Contact__c=con.id;
        tm.CSA_Team__c= csateam.id;
        insert tm;
        
        CSA_Meeting_Agenda__c meetingagenda = new CSA_Meeting_Agenda__c();
        meetingagenda.Location_of_Meeting__c='california';
        meetingagenda.Name='fapt meeting';
        meetingagenda.Meeting_Date__c=system.today().adddays(10);
        meetingagenda.Meeting_Start3_Time__c ='8:00:00.000';
        meetingagenda.Meeting_End_Time2__c='8:15:00.000';
        insert meetingagenda;
        
        CSA_Meeting_Members__c meetingmembers = new CSA_Meeting_Members__c();
        meetingmembers.Contact__c=con.id;
        meetingmembers.CSA_Meeting_Agenda__c= meetingagenda.id;
        //meetingmembers.CSA_Team__c=csateam.id;
        insert meetingmembers;
        
        CSA_Meeting_Agenda_Item__c caitem = new CSA_Meeting_Agenda_Item__c();
        caitem.CSA_Meeting_Agenda__c=meetingagenda.id;
        caitem.Time3__c='8:00:00.000';
        caitem.Case_KNP__c =caserec.id;
        insert caitem;
        
    }  
    public static testmethod void agendaTest(){
        list<CSA_Meeting_Agenda__c> csmeetingagenda =[select Id, Name, Location_of_Meeting__c,Meeting_Date__c,Meeting_End_Time2__c,Meeting_Start3_Time__c,
                Meeting_Status__c,Review_Type__c,Send_Notification__c,Team__c From CSA_Meeting_Agenda__c];
        CSAMeetingScreen.getAgendaRecord(csmeetingagenda[0].id);
    }
    
    public static testmethod void getmemberTest(){
        list<CSA_Agenda_Team__c>  teamlist=[select id,name from CSA_Agenda_Team__c];
        list<Team_Member__c> teammemberlist =[select id,name,Contact__c,CSA_Team__c from Team_Member__c 
                                              where id=: teamlist[0].id];
        CSAMeetingScreen.getTeamMembers(teamlist[0].id);
    }
    public static testmethod void AddAgendaMemberTest(){
       list<CSA_Meeting_Agenda__c> csmeetingagenda =[select Id, Name, Location_of_Meeting__c,Meeting_Date__c,Meeting_End_Time2__c,Meeting_Start3_Time__c,
                Meeting_Status__c,Review_Type__c,Send_Notification__c,Team__c From CSA_Meeting_Agenda__c];
        list<contact> conlist=[select id,email,name from contact];
        list<CSA_Agenda_Team__c> csagendateamlist =[select id,name from CSA_Agenda_Team__c];
        CSAMeetingScreen.AddAgendaMember(csmeetingagenda[0].id,conlist[0].id,csagendateamlist[0].id);
    }
    
    public static testmethod void AddAgendaMembersTest(){
       list<CSA_Meeting_Agenda__c> csmeetingagenda =[select Id, Name, Location_of_Meeting__c,Meeting_Date__c,Meeting_End_Time2__c,Meeting_Start3_Time__c,
                Meeting_Status__c,Review_Type__c,Send_Notification__c,Team__c From CSA_Meeting_Agenda__c];
        list<contact> conlist=[select id,email,name from contact];
        list<CSA_Agenda_Team__c> csagendateamlist =[select id,name from CSA_Agenda_Team__c];
        list<id> conids = new list<id>();
        conids.add(conlist[0].id);
        CSAMeetingScreen.AddAgendaMembers(csmeetingagenda[0].id,conids);
    }
    
    public static testmethod void getTeamMembersFromAgendaTest(){
         list<CSA_Meeting_Agenda__c> csmeetingagenda =[select Id, Name, Location_of_Meeting__c,Meeting_Date__c,Meeting_End_Time2__c,Meeting_Start3_Time__c,
                Meeting_Status__c,Review_Type__c,Send_Notification__c,Team__c From CSA_Meeting_Agenda__c];
        list<CSA_Meeting_Members__c> cmm=[select id,Contact__c,CSA_Meeting_Agenda__c from CSA_Meeting_Members__c ];
        CSAMeetingScreen.getTeamMembersFromAgenda(csmeetingagenda[0].id);
    }
    
    public static testmethod void getAgendaItemsTest(){
         list<CSA_Meeting_Agenda__c> csmeetingagenda =[select Id, Name, Location_of_Meeting__c,Meeting_Date__c,Meeting_End_Time2__c,Meeting_Start3_Time__c,
                Meeting_Status__c,Review_Type__c,Send_Notification__c,Team__c From CSA_Meeting_Agenda__c];
        list<CSA_Meeting_Agenda_Item__c> casitem=[Select Id, Name,Agenda_Comments__c,Time3__c, Case_KNP__c,Review__c,VACMS_Number__c,Case_Name__c,Review_Type__c, Time__c,IFSP__c,Time2__c From CSA_Meeting_Agenda_Item__c 
                Where CSA_Meeting_Agenda__c =: csmeetingagenda[0].id];
        CSAMeetingScreen.getAgendaItems(csmeetingagenda[0].id);
    } 
    
    public static testmethod void deleteTeamTest(){
        list<CSA_Agenda_Team__c> csagendateamlist =[select id,name from CSA_Agenda_Team__c];
        CSAMeetingScreen.deleteteam(csagendateamlist[0].id);
    }
    
    public static testmethod void SendNotificationTest(){
        list<CSA_Meeting_Agenda__c> csmeetingagenda =[select Id, Name, Location_of_Meeting__c,Meeting_Date__c,Meeting_End_Time2__c,Meeting_Start3_Time__c,
                Meeting_Status__c,Review_Type__c,Send_Notification__c,Team__c From CSA_Meeting_Agenda__c];
        CSAMeetingScreen.SendNotification(csmeetingagenda[0].id);
    }
    
    public static testmethod void UpdateCancellationTest(){
        list<CSA_Meeting_Agenda__c> csmeetingagenda =[select Id, Name, Location_of_Meeting__c,Meeting_Date__c,Meeting_End_Time2__c,Meeting_Start3_Time__c,
                Meeting_Status__c,Review_Type__c,Send_Notification__c,Team__c From CSA_Meeting_Agenda__c];
        CSAMeetingScreen.UpdateCancellation(csmeetingagenda[0].id);
    }
    
    public static testmethod void AddExstmemberTest(){
        list<contact> conlist=[select id,email,name from contact];
        list<CSA_Agenda_Team__c>  teamlist=[select id,name from CSA_Agenda_Team__c];
         list<CSA_Meeting_Agenda__c> csmeetingagenda =[select Id, Name, Location_of_Meeting__c,Meeting_Date__c,Meeting_End_Time2__c,Meeting_Start3_Time__c,
                Meeting_Status__c,Review_Type__c,Send_Notification__c,Team__c From CSA_Meeting_Agenda__c];
        CSAMeetingScreen.AddExstmember(conlist[0].id,teamlist[0].id,csmeetingagenda[0].id);
    }
    public static testmethod void deleteitemTest(){
        list<CSA_Meeting_Agenda__c> csmeetingagenda =[select Id, Name, Location_of_Meeting__c,Meeting_Date__c,Meeting_End_Time2__c,Meeting_Start3_Time__c,
                Meeting_Status__c,Review_Type__c,Send_Notification__c,Team__c From CSA_Meeting_Agenda__c];
        list<CSA_Meeting_Agenda_Item__c> casitem=[Select Id, Name,Agenda_Comments__c,Time3__c, Case_KNP__c,Review__c,VACMS_Number__c,Case_Name__c,Review_Type__c, Time__c,IFSP__c,Time2__c From CSA_Meeting_Agenda_Item__c 
                Where CSA_Meeting_Agenda__c =: csmeetingagenda[0].id];
        CSAMeetingScreen.deleteitem(casitem[0].id);
    }
    public static testmethod void CheckExstmemberTest(){
        list<contact> conlist=[select id,email,name from contact];
        list<CSA_Agenda_Team__c>  teamlist=[select id,name from CSA_Agenda_Team__c];
         list<Team_Member__c> teammemberlist =[select id,name,Contact__c,CSA_Team__c from Team_Member__c 
                                              where id=: teamlist[0].id];
       
        CSAMeetingScreen.CheckExstmember(conlist[0].id,teamlist[0].id);
    }
    public static testmethod void CheckExstmemberNotExistTest(){
        contact con = new contact();
        con.lastname='con';
        con.FirstName='test';
        con.Email='con@gmail.com';
        insert con;
        list<CSA_Agenda_Team__c>  teamlist=[select id,name from CSA_Agenda_Team__c];
        
        CSAMeetingScreen.CheckExstmember(con.id,teamlist[0].id);
    }
    //METHODS TO COVER EXCEPTION LINES
    public static testmethod void getTeamMembersTest1(){
        CSA_Agenda_Team__c csateam = new CSA_Agenda_Team__c();
        csateam.Name='test team1';
        insert csateam;
        
       // try{
            CSAMeetingScreen.getTeamMembers(csateam.id);
        //} catch(Exception e){}   
    }
    
     public static testmethod void AddAgendaMemberTest1(){
       list<CSA_Meeting_Agenda__c> csmeetingagenda =[select Id, Name, Location_of_Meeting__c,Meeting_Date__c,Meeting_End_Time2__c,Meeting_Start3_Time__c,
                Meeting_Status__c,Review_Type__c,Send_Notification__c,Team__c From CSA_Meeting_Agenda__c];
        list<contact> conlist=[select id,email,name from contact];
        list<CSA_Agenda_Team__c> csagendateamlist =[select id,name from CSA_Agenda_Team__c];
        CSAMeetingScreen.AddAgendaMember(null,null,null);
    }
    
    public static testmethod void AddAgendaMembersTest1(){
       list<CSA_Meeting_Agenda__c> csmeetingagenda =[select Id, Name, Location_of_Meeting__c,Meeting_Date__c,Meeting_End_Time2__c,Meeting_Start3_Time__c,
                Meeting_Status__c,Review_Type__c,Send_Notification__c,Team__c From CSA_Meeting_Agenda__c];
        list<contact> conlist=[select id,email,name from contact];
        list<CSA_Agenda_Team__c> csagendateamlist =[select id,name from CSA_Agenda_Team__c];
        list<id> conids = new list<id>();
        conids.add(conlist[0].id);
        CSAMeetingScreen.AddAgendaMembers(null,null);
    }
    public static testmethod void deleteitemtest1(){
        CSAMeetingScreen.deleteitem(null);
    }
    
    public static testmethod void SendNotificationTest1(){
        CSAMeetingScreen.SendNotification(null);
    }
    
    public static testmethod void UpdateCancellationTest1(){
        CSAMeetingScreen.UpdateCancellation(null);
    }
    public static testmethod void checkMeetingStartTime(){
        CSAMeetingScreen.CheckMeetingStartTimeForToday(system.today(),'9:30:00.000');
    }
}