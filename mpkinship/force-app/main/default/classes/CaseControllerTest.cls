@IsTest
public class CaseControllerTest {
	public static testmethod void testRecords(){
        
        contact con = new contact();
        con.LastName='contactrec';
        con.FirstName='test';
        con.Email='test@contact.com';       
        insert con;
        Campaign parent = new Campaign();
        parent.name = '2021';
        parent.StartDate = Date.Today().addDays(-10);
        parent.EndDate = Date.Today().addDays(10);
        parent.Fiscal_Year_Type__c = 'Local';
        parent.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId();
        insert parent;
        Kinship_Case__c caserec = new Kinship_Case__c();
        caserec.Primary_Contact__c=con.id;
        caserec.DOB__c=system.today().adddays(-100);
        caserec.Race__c='1';
        caserec.Gender_Preference__c='M';
        caserec.Hispanic_Ethnicity__c='1';
        caserec.DSM_V_Indicator_ICD_10_1__c='1';
        caserec.Clinical_Medication_Indicator_1__c='1';
        caserec.Medicaid_Indicator_1__c='1';
        caserec.Referral_Source__c='2';
        caserec.Autism_Flag_1__c='2';
        insert caserec;
        
        account accountrec = new account();
        accountrec.Name='test vendor';
        accountrec.Tax_ID__c='333-33-3333';
        accountrec.Entity_Type__c='Individual';
        insert accountrec;
        Locality_Account__c la = new Locality_Account__c();
        la.Name='222';
        insert la;
        
        ContentVersion content=new ContentVersion(); 
        content.Title='Header_Picture1'; 
        content.PathOnClient='/' + content.Title + '.jpg'; 
        Blob bodyBlob=Blob.valueOf('Unit Test ContentVersion Body'); 
        content.VersionData=bodyBlob; 
        //content.LinkedEntityId=sub.id;
        content.origin = 'H';
        insert content;
        ContentDocumentLink contentlink=new ContentDocumentLink();
        contentlink.LinkedEntityId=caserec.id;
        contentlink.contentdocumentid=[select contentdocumentid from contentversion where id =: content.id].contentdocumentid;
        contentlink.ShareType = 'I';
        contentlink.Visibility = 'AllUsers'; 
        
        
        insert contentlink;
        
        test.startTest();
        Kinship_Case__c KCase = CaseController.getCaseDetail('123');
        //RelatedList__mdt rl = CaseController.getRelatedListDetail('Vendor');
        ContentVersion cv = CaseController.getFileName(caserec.id);
        CaseController.deleteProfilePic(caserec.id);
        //CaseController.getProgramList('123');
        test.stopTest();

    }
}