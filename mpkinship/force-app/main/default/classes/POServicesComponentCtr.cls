public class POServicesComponentCtr{
    //KIN 1527 
  @AuraEnabled
    public static void makeServiceInactive (String recId){
        System.debug('recId:'+recId);
        List<PricebookEntry> serviceList=new List<PricebookEntry>();
        if(recId!=null && recId!=''){
            serviceList=[Select id,name,IsActive from PricebookEntry where id=:recId];
        }
        if(serviceList.size()>0){
            serviceList[0].IsActive=false;
            update serviceList;
        }
    }
    //KIN 1311 
  @AuraEnabled
    public static void createNoteRecord (ContentNote nt, id prntId){
        try{
            if(nt != null){
                insert nt;
                ContentDocument cd=[select id from ContentDocument where id=:nt.Id];
                ContentDocumentLink cdl=new ContentDocumentLink();
                cdl.ContentDocumentId=cd.id;
                cdl.LinkedEntityId=prntId;
                cdl.ShareType='V';
                cdl.Visibility='AllUsers';
                insert cdl;
            }
        } catch (Exception ex){

        }
    } 
    //KIN 228
    @auraEnabled
    public static Kinship_Case__c getCaseAge(string caseId){
        list<Kinship_Case__c> cases=[select id,DOB__c,Age__c from Kinship_Case__c where id=:caseId];
        if(cases.size()>0){ 
            return cases[0];
        }
        else{
            return null;
        }
    }  
    @auraEnabled
    public static CSAwrapclass getCategoryandSPT(string categoryval){
        system.debug('**Entered in getcatandspt'+ categoryval);
        CSAwrapclass wrap = new CSAwrapclass();   
        list<option> sptdesirednames = new list<option>();
        //KIN 237
        list<option> sptdesireddesc = new list<option>();
        //KIN 237
        list<CategoryAndSPT__c> mappedSPTSs =[select id,Category__c,Category__r.name,Service_Placement_Type__r.name,
                                                      Service_Placement_Type__r.Code__c,Service_Placement_Type__r.Description__c from CategoryAndSPT__c where
                                                     category__C =:categoryval and Service_Placement_Type__c!=null ];
            //QUERY LIST FOR MANDATE TYPE
            //KIN 237
            list<option> mandateDesiredDesc = new list<option>();
            list<option> mandateDesiredNames = new list<option>();
        //KIN 237
            list<Mandate_Type_And_Category__c> mappedMandate =[select id,Category__c,category__r.name,Mandate_Type__r.name,
                                                               Mandate_Type__r.Mandate_Code__c,Mandate_Type__r.Description__c from Mandate_Type_And_Category__c
                                                              where Category__c=:categoryval and Mandate_Type__c!=null];
            if(mappedSPTSs.size()>0){              
                for(CategoryAndSPT__c catSpt : mappedSPTSs){
                    option optionrec = new option();
                    optionrec.label=catSpt.Service_Placement_Type__r.name;
                    optionrec.value=catSpt.Service_Placement_Type__r.Code__c;
                    sptdesirednames.add(optionrec);
                    //KIN 237
                    option optionrec1 = new option();
                    optionrec1.label=catSpt.Service_Placement_Type__r.Code__c;
                    if(catSpt.Service_Placement_Type__r.Description__c== null){
                        optionrec1.value='';
                    }
                    else{
                        optionrec1.value=catSpt.Service_Placement_Type__r.Description__c;
                    }
                    
                    sptdesireddesc.add(optionrec1);
                    //ends
                }
                if(sptdesirednames.size()>0){                  
                    wrap.Names=sptdesirednames;
                }   
                // KIN 237
                if(sptdesireddesc.size()>0){                  
                    wrap.SPTDesc=sptdesireddesc;
                } 
                //ends
            } //mappedspts size loop ends here 
        else{
                wrap.Names=sptdesirednames ; 
            	// KIN 237
            	wrap.SPTDesc=sptdesireddesc;
        }
            //CHECK MANDATE 
            if(mappedMandate.size()>0){
              for(Mandate_Type_And_Category__c catman : mappedMandate){
                    option optionrec = new option();
                    optionrec.label=catman.Mandate_Type__r.name;
                    optionrec.value=catman.Mandate_Type__r.Mandate_Code__c;
                    mandateDesiredNames.add(optionrec);
                  
                    //KIN 237
                    option optionrec1 = new option();
                    optionrec1.label=catman.Mandate_Type__r.Mandate_Code__c;
                    System.debug('@@@@@'+catman.Mandate_Type__r.Description__c);
                  	if(catman.Mandate_Type__r.Description__c==null){
                        optionrec1.value='';
                    }
                    else{
                        optionrec1.value=catman.Mandate_Type__r.Description__c;  
                    }
                    
                    mandateDesiredDesc.add(optionrec1);
                    //ends
                }
                if(mandateDesiredNames.size()>0){                  
                    wrap.MandateNames=mandateDesiredNames;
                }   
                // KIN 237
                if(mandateDesiredDesc.size()>0){                  
                    wrap.MandateDesc=mandateDesiredDesc;
                } 
                //ends
            } //mappedmandate size loop ends here 
        else{
                wrap.MandateNames=mandateDesiredNames;
            //KIN 237
            wrap.MandateDesc=mandateDesiredDesc;
        }
             system.debug('wrap val'+ wrap);
            return wrap;                
    }
    //KIN 237
    @auraEnabled
    public static String getSNCDesc(string sncValue){
        system.debug('value of sncValue'+sncValue);
        if(sncValue!=''){
            List<Service_Names_Code__c> snclist= [Select id,name,Description__c from Service_Names_Code__c where Service_Name_Code__c=:sncValue];
        	if(snclist.size()>0)
                return snclist[0].Description__c;
            else
                return null;
        }
        return null;
    }
    //ends
    
    //GETTING MATCHING SEVICE NAMES BASED ON SPT VALUE
    @auraEnabled
     public static list<option> getSPTNames(string sptValue){
         system.debug('value of sptvalue'+sptValue);
         if(sptValue!=''){
             list<Service_Name_and_Service_Type__c> snameslist = [select id,Service_Placement_Type__r.name,
                                                                Service_Names_Code__r.Service_Name_Code__c,Service_Names_Code__r.name,
                                                               Service_Placement_Type__r.Code__c from Service_Name_and_Service_Type__c
                                                               where Service_Placement_Type__r.code__c=:sptValue];
             system.debug('***Val of list'+snameslist);
             list<option> spnamedesirednames = new list<option>();
             for(Service_Name_and_Service_Type__c servicenametype : snameslist){
                option optionrec = new option();
                 optionrec.label=servicenametype.Service_Names_Code__r.name;
                 optionrec.value=servicenametype.Service_Names_Code__r.Service_Name_Code__c;
                 spnamedesirednames.add(optionrec);        
             }
             if(spnamedesirednames.size()>0)
                return spnamedesirednames;
         }
         return null;
     } 
     
   @auraEnabled
   public static UnitMeasureWrap getUnitMeasure(){
        UnitMeasureWrap uwrap = new UnitMeasureWrap();
        Schema.DescribeFieldResult fieldResult = Product2.Unit_Measure__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
       list<unitmeasureOptionsWrap> unitMeasureOptons = new list<unitmeasureOptionsWrap>(); 
        for( Schema.PicklistEntry pickListVal : ple){
            unitmeasureOptionsWrap uwrapoption = new unitmeasureOptionsWrap();
            uwrapoption.label =pickListVal.getLabel();
            uwrapoption.value = pickListVal.getValue();
            unitMeasureOptons.add(uwrapoption);
        }     
        
        if(unitMeasureOptons.size()>0){           
            uwrap.UnitMeasures= unitMeasureOptons;
            uwrap.message ='Unit Measuer found';  
        }
        else
            uwrap.message='Not found';
       return uwrap;     
      
   }
   
   //GET COST CENTER
    @auraEnabled
    public static list<id> getCostCenters(string categoryId,string FipsId){
        system.debug('**VAl of categoryid'+ categoryId);
        system.debug('**Val of fipsids'+ FipsId);
          list<id> costcenterids = new list<id>();
          list<Category__c> categories=[select id,Locality_Account__c,Category_Type__c from category__C where id =: categoryId and Locality_Account__c!=null];
          system.debug('**Val of categories'+categories);
          if(categories.size()>0){
              string csacontains= '%CSA%';
              list<Chart_of_Account__c> charts = new list<Chart_of_Account__c>();
              if(categories[0].Category_Type__c !=null && categories[0].Category_Type__c=='CSA'){
                  charts=[select id,Locality_Account__c,Cost_Center__c,Cost_Center__r.Name,FIPS__c from Chart_of_Account__c where 
                                         Locality_Account__c =:categories[0].Locality_Account__c and FIPS__c=:FipsId
                                         and Cost_Center__c!=null and Cost_Center__r.name like: csacontains];
              }
              else
                  charts=[select id,Locality_Account__c,Cost_Center__c,FIPS__c from Chart_of_Account__c where 
                                         Locality_Account__c =:categories[0].Locality_Account__c and FIPS__c=:FipsId
                                         and Cost_Center__c!=null and (Not cost_center__r.Name Like: csacontains)];                           
              system.debug('**Val of charts'+ charts);                           
              for(Chart_of_Account__c coa : charts){
                  if(!costcenterids.contains(coa.Cost_Center__c)){
                     costcenterids.add(coa.Cost_Center__c);     
                  }   
              }                          
          }                               
         if(costcenterids.size()>0)
             return costcenterids;
        return null;        
    }
    
    //GET COSTCENTER IF ONE
    @auraEnabled
    public static Cost_Center__c getOneCostCenter(string costcenterId){
        system.debug('**VAl of one costcenterid'+costcenterId);
        list<Cost_Center__c> cc =[select id,name from Cost_Center__c where id=:costcenterId];
        if(cc.size()>0)
            return cc[0];
        return null;    
    }
    
     public class CSAwrapclass{    
       @auraEnabled 
        public string message;      
        @auraEnabled
        public list<option> Names;
        @auraEnabled
        public list<option> SPTNamesCodes;
        @auraEnabled
        public list<option> MandateNames;
        // KIN 237
        @auraEnabled
        public list<option> MandateDesc;
         @auraEnabled
        public list<option> SPTDesc;
         @auraEnabled
        public list<option> serviceDesc;
       	// ends
       
    } 
     public class option{
        @auraEnabled
        public string label;
        @auraEnabled
        public string value;
       
    }
    
    public class UnitMeasureWrap{
       @auraEnabled
       public  list<unitmeasureOptionsWrap> UnitMeasures;
       @auraEnabled
       public string message;
   }
   
   public class unitmeasureOptionsWrap{
       @auraEnabled
       public string label;
       @auraEnabled
       public string value;
   
   }
}