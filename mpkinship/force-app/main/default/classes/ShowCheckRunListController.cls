public class ShowCheckRunListController {
    public static final String CHECK = 'CHECK';
    public static final String PAYMENT = 'PAYMENT';

    @AuraEnabled
    public static List<checkObj> getCheckList(String strId, String strType){
        List<checkObj> lstResult = new List<checkObj>();
        Set<Id> setId = new Set<Id>();
        if(String.isNotBlank(strType)){
            if(CHECK.equals(strType)) {
                Kinship_Check__c kc = [SELECT id, Check_Run__c FROM Kinship_Check__c WHERE id =:strId];
                setId.add(kc.Check_Run__c);
            } else if(PAYMENT.equals(strType)){
                for(Payment__c payment : [SELECT id, Kinship_Check__c,Kinship_Check__r.Check_Run__c  
                                                    FROM Payment__c WHERE id = :strId]) {
                    if(String.isNotBlank(payment.Kinship_Check__c)) {
                        setId.add(payment.Kinship_Check__r.Check_Run__c);
                    }
                }
            }
        }
        if(setId.size() == 0) return lstResult;
        List<Check_Run__c> lstCheckRun = [SELECT id, name, createdby.name, createddate 
                                                FROM Check_Run__c 
                                                WHERE id IN :setId
                                                ORDER BY createddate DESC];
        for(Check_Run__c doc :lstCheckRun) {
            checkObj obj = new checkObj();
            obj.id = doc.id;
            obj.name = doc.Name;
            obj.nameUrl = '/' + doc.id;
            obj.createdBy = doc.CreatedBy.Name;
            obj.createdDate = doc.createddate;

            lstResult.add(obj);
        }
        return lstResult;
    }

    @AuraEnabled
    public static List<exportData> getExportData(String checkrunid){
        List<exportData> result = new List<exportData>();
        for(Payment__c data : [SELECT id, FIPS_Code__c, FIPS_Code__r.Locality_Code__c, Category_lookup__c, Category_lookup__r.Locality_Account__c, 
                                            Opportunity__c, Opportunity__r.AccountId, Opportunity__r.Account.Vendor_Code__c, 
                                            Kinship_Check__c, Kinship_Check__r.Issue_Date__c,Kinship_Check__r.Description__c, Kinship_Check__r.Amount__c, 
                                            Opportunity__r.Kinship_Case__r.Case_No__c , Opportunity__r.Kinship_Case__r.Name,
                                            Cost_Center__r.Cost_Center_Code__c,Category_lookup__r.Locality_Account__r.Name,
                                            Vendor__r.Locality_Vendor_Number__c,Payment_Date__c,Name,Payment_Amount__c,Case_Name__c,
                                            PO_Number__c,Invoice_Number__c,Description__c,Opportunity__r.RecordType.Name
                                        FROM Payment__c 
                                        WHERE Kinship_Check__r.Check_Run__c = :checkrunid
                                        AND Opportunity__r.RecordType.Name != 'Report of Collections'
                                        AND RecordType.Name != 'Adjustments']) {
            exportData temp = new exportData();
            temp.id = data.id;
            temp.fipsCode = data.FIPS_Code__r.Locality_Code__c;
            temp.accountCode = data.Category_lookup__r.Locality_Account__r.Name;
            temp.vendorCode = data.Opportunity__r.Account.Vendor_Code__c;
            temp.checkDate = data.Kinship_Check__r.Issue_Date__c;
            temp.memo = data.Kinship_Check__r.Description__c;
            temp.amount = data.Kinship_Check__r.Amount__c;
            temp.caseNo = data.Opportunity__r.Kinship_Case__r.Name;

            temp.costCenterCode = data.Cost_Center__r.Cost_Center_Code__c;
            temp.localityAccountNumber = data.Category_lookup__r.Locality_Account__r.Name;
            temp.localityVendorNumber = data.Vendor__r.Locality_Vendor_Number__c;
            temp.paymentDate = data.Payment_Date__c;
            temp.poNumber = data.PO_Number__c;
            temp.paymentNumber = data.Name;
            temp.caseName = data.Case_Name__c;
            temp.invoiceNumber = data.Invoice_Number__c;
            temp.description = data.Description__c;
            temp.amount = data.Payment_Amount__c;
            temp.recordTypeName = data.Opportunity__r.RecordType.Name;

            result.add(temp);
        }
        return result;
    }

    public class exportData{
        @AuraEnabled
        public String id{get;set;}

        @AuraEnabled
        public String fipsCode{get;set;}

        @AuraEnabled
        public String accountCode{get;set;}

        @AuraEnabled
        public String vendorCode{get;set;}

        @AuraEnabled
        public Date checkDate{get;set;}

        @AuraEnabled
        public String memo{get;set;}

        @AuraEnabled
        public Decimal amount{get;set;}

        @AuraEnabled
        public String caseNo{get;set;}

        @AuraEnabled
        public String costCenterCode{get;set;}

        @AuraEnabled
        public String localityAccountNumber{get;set;}

        @AuraEnabled
        public String localityVendorNumber{get;set;}

        @AuraEnabled
        public Date paymentDate{get;set;}

        @AuraEnabled
        public String poNumber{get;set;}

        @AuraEnabled
        public String paymentNumber{get;set;}

        @AuraEnabled
        public String caseName{get;set;}

        @AuraEnabled
        public String invoiceNumber{get;set;}

        @AuraEnabled
        public String description{get;set;}

        @AuraEnabled
        public String recordTypeName{get;set;}
    }

    public class checkObj{
        @AuraEnabled
        public String id{get;set;}

        @AuraEnabled
        public String nameUrl{get;set;}

        @AuraEnabled
        public String name{get;set;}

        @AuraEnabled
        public String createdBy{get;set;}

        @AuraEnabled
        public Datetime createdDate{get;set;}
    }
}