@isTest
public class AssessmentControllerTest {
    
    @isTest
    public static void testMethod1()
    {
        Domain__c d = new Domain__c();
        d.name = 'Test';
        d.Order__c = 1;
        insert d;
        
        VEMAT_Assessment__c va = new VEMAT_Assessment__c();
        
        va.Domain_Lookup__c = d.Id;
        va.Category__c = 'Minimal';
        va.Points__c = 2;
        insert va;
        
        AssessmentController.getAssessmentSettings();

        Assessment__c assess = new Assessment__c(); 
        assess.Name = 'Test Assess';
        assess.Refresh_Page__c = true;
        insert assess;
        
        Assessment_Line_Item__c al = new Assessment_Line_Item__c();
        Assessment_Line_Item__c al1 = new Assessment_Line_Item__c();
        
        List<Assessment_Line_Item__c> lstAL = new List<Assessment_Line_Item__c>();
        
        al.Apply_Score__c = 'Yes';
        al.Score__c = 3;
        al.Assessment__c = assess.Id;
        lstAL.add(al);  
        
        al1.Apply_Score__c = 'Yes';
        al1.Score__c = 4;
        al1.Assessment__c = assess.Id;
        lstAL.add(al1);  
        
        //insert lstAL;

        AssessmentController.saveAssessment(lstAL, assess.id);
        AssessmentController.showTotalPointsButtonMethod(assess.id);
        AssessmentController.updateAssessment(assess.id, 56);
        AssessmentController.showStartButtonMethod(assess.id);
        AssessmentController.getRefreshPage(assess.id);
    }
}