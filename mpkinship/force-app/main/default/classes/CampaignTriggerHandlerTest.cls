@isTest
private class CampaignTriggerHandlerTest {
    @TestSetup
    static void initData(){
        TriggerActivation__c ta = new TriggerActivation__c(Name = 'CampaignTrigger', isActive__c = true);
        insert ta;
		Campaign parent = new Campaign();
       	parent.name = '2021';
       	parent.StartDate = Date.Today().addDays(-10);
       	parent.EndDate = Date.Today().addDays(10);
       	parent.Fiscal_Year_Type__c = 'DSS';
       	parent.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId();
       	insert parent;
        Campaign c = new Campaign();
        c.name = '2021';
        c.StartDate = Date.Today().addDays(-10);
        c.EndDate = Date.Today().addDays(10);
        c.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('LASER Reporting Period').getRecordTypeId();
        c.parentId = parent.id;
        insert c;

        Transaction_Journal__c tj = new Transaction_Journal__c();
        tj.status__c = 'Open';
        //tj.Amount__c = 1000;
        tj.Description__c = 'description';
        tj.Child_Campaign__c = c.id;
        insert tj;

        Account_Period_Transaction__c apt = new Account_Period_Transaction__c();
        apt.Transaction_Journal__c = tj.id;
        apt.LASER_Reporting_Period__c = c.id;
        insert apt;
    }

    @isTest
    static void testInsertCampaign(){
        Campaign parent = [Select id From Campaign where recordType.Name = 'Fiscal Year'];
        Campaign c = new Campaign();
        c.name = '2021 Test';
        c.StartDate = Date.Today().addDays(-5);
        c.EndDate = Date.Today().addDays(5);
        c.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('LASER Reporting Period').getRecordTypeId();
        c.ParentId = parent.id;
        
        Test.startTest();
        insert c;
        Test.stopTest();

        List<Account_Period_Transaction__c> lstTJ = [SELECT id FROM Account_Period_Transaction__c];
        System.assertNotEquals(0, lstTJ.size(), 'The Account_Period_Transaction__c insert does not equal 2');
    }

    @isTest
    static void testUpdateCampaign(){
        Campaign parent = [Select id From Campaign where recordType.Name = 'Fiscal Year'];
        Campaign c = new Campaign();
        c.name = '2021 Test';
        c.StartDate = Date.Today().addDays(5);
        c.EndDate = Date.Today().addDays(10);
        c.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('LASER Reporting Period').getRecordTypeId();
        c.ParentId = parent.id;
        insert c;

        Test.startTest();
        c.startDate = Date.Today().addDays(-5);
        c.endDate = Date.Today().addDays(5);
        update c;
        Test.stopTest();

        List<Account_Period_Transaction__c> lstTJ = [SELECT id FROM Account_Period_Transaction__c];
        System.assertNotEquals(0, lstTJ.size(), 'The Account_Period_Transaction__c insert does not equal 2');
    }

    @isTest
    static void testDeleteCampaign(){
        Campaign c = [Select id from Campaign Where recordtype.Name = 'LASER Reporting Period'];

        Test.startTest();
        delete c;
        Test.stopTest();

        List<Account_Period_Transaction__c> lstTJ = [SELECT id FROM Account_Period_Transaction__c];
        //System.assertEquals(0, lstTJ.size(), 'The Account_Period_Transaction__c insert does not equal 0');
    }
}