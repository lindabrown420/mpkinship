@IsTest
public class CaseCloseControllerTest {
    @testSetup
    static void testecords(){
        
        contact con = new contact();
        con.LastName='contactrec';
        con.FirstName='test';
        con.Email='test@contact.com';       
        insert con;
        Campaign parent = new Campaign();
        parent.name = '2021';
        parent.StartDate = Date.Today().addDays(-10);
        parent.EndDate = Date.Today().addDays(10);
        parent.Fiscal_Year_Type__c = 'Local';
        parent.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId();
        insert parent;
        Kinship_Case__c caserec = new Kinship_Case__c();
        caserec.Primary_Contact__c=con.id;
        caserec.DOB__c=system.today().adddays(-100);
        caserec.Race__c='1';
        caserec.Gender_Preference__c='M';
        caserec.Hispanic_Ethnicity__c='1';
        caserec.DSM_V_Indicator_ICD_10_1__c='1';
        caserec.Clinical_Medication_Indicator_1__c='1';
        caserec.Medicaid_Indicator_1__c='1';
        caserec.Referral_Source__c='2';
        caserec.Autism_Flag_1__c='2';
        insert caserec;
        
        account accountrec = new account();
        accountrec.Name='test vendor';
        accountrec.Tax_ID__c='333-33-3333';
        accountrec.Entity_Type__c='Individual';
        insert accountrec;
        Locality_Account__c la = new Locality_Account__c();
        la.Name='222';
        insert la;
        
        Category__c category = new Category__c();
        category.Name='222';
        category.Locality_Account__c=la.id;
        category.Category_Type__c='CSA';
        insert category;
        
        Category_Rate__c cr = new Category_Rate__c();
        cr.Category__c = category.id;
        cr.Status__c='Active';
        cr.Price__c=5000;
        cr.Effective_Date__c=system.today();
        cr.RecordTypeId = Schema.SObjectType.Category_Rate__c.getRecordTypeInfosByDeveloperName().get('Other_Rate').getRecordTypeId();
        insert cr;
        
        
    }
    public static testmethod void testcaseclose(){
         opportunity opprecord = new opportunity();
        opprecord.CloseDate=Date.newInstance(date.today().addmonths(-5).year(), date.today().addmonths(-5).month(), 1);
        System.debug('begin date'+Date.newInstance(date.today().addmonths(-5).year(), date.today().addmonths(-5).month(), 1));
        opprecord.End_Date__c=Date.newInstance(date.today().addmonths(5).year(), date.today().addmonths(5).month(), 1);
        System.debug('end date'+Date.newInstance(date.today().addmonths(5).year(), date.today().addmonths(5).month(), 1));
        opprecord.StageName='Draft';
        opprecord.Name='test opportunity';
        opprecord.Type_of_Case_Action__c='Ongoing';
        opprecord.Amount=2000;
        opprecord.Prorate_Last_Payment__c=true;
        //commented for npsp removal
        //opprecord.npe01__Do_Not_Automatically_Create_Payment__c=true;
        insert opprecord;
        opportunity opprecord1 = new opportunity();
         Date firstDayOfMonth = Date.newInstance(date.today().year(), date.today().addmonths(5).month(), 1);
        System.debug('firstDayOfMonth'+firstDayOfMonth);
        opprecord1.CloseDate=firstDayOfMonth;
        opprecord1.StageName='Draft';
        opprecord1.Name='test1 opportunity';
        opprecord1.Amount=2000;
        opprecord1.Prorate_Last_Payment__c=true;
        //commented for npsp removal
        //opprecord1.npe01__Do_Not_Automatically_Create_Payment__c=true;
        insert opprecord1;
        Locality_Account__c la = new Locality_Account__c();
        la.Name='222';
        insert la;
         Id LEDRSRecTypeId = Schema.SObjectType.category__C.getRecordTypeInfosByDeveloperName().get('LEDRS_Category').getRecordTypeId();
        Category__c category1 = new Category__c();
        category1.Name='222';
        category1.Locality_Account__c=la.id;
        category1.Inactive__c=false;
        category1.Category_Type__c='CSA';
        category1.RecordTypeId=LEDRSRecTypeId;
        insert category1;                    

        list<Payment__c> paymentlist = new list<Payment__c>();
                 for(integer i=0; i<3 ;i++){
                     Payment__c payment = new Payment__c();
                     payment.Opportunity__c=opprecord.id;
                     payment.Payment_Amount__c=2000;
                     payment.Service_Begin_Date__c= date.today().addDays(5);
                     payment.Service_End_Date__c= date.today().addDays(10);
                     payment.Category_lookup__c=category1.id;
                     paymentlist.add(payment);
                 }
                for(integer i=0; i<3 ;i++){
                     Payment__c payment = new Payment__c();
                     payment.Opportunity__c=opprecord.id;
                     payment.Payment_Amount__c=2000;
                     payment.Service_Begin_Date__c= date.today().addDays(-1);
                     payment.Service_End_Date__c= date.today().addDays(10);
                    payment.Category_lookup__c=category1.id;
                     paymentlist.add(payment);
                 }
                 Payment__c payment = new Payment__c();
                     payment.Opportunity__c=opprecord1.id;
                     payment.Payment_Amount__c=2000;
                     payment.Service_Begin_Date__c= date.today();
                     payment.Service_End_Date__c= date.today().addDays(10);
        			payment.Category_lookup__c=category1.id;
                     paymentlist.add(payment);
                 insert paymentlist;
        Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitrequest();
        app.setObjectId(opprecord.Id);    
         // Submit the approval request for the po    
        Approval.ProcessResult result = Approval.process(app);    
        // Verify that the results are as expected    
        System.assert(result.isSuccess());
       /* opprecord.name='testoneopportunity';
        update opprecord; */
        //system.assertEquals(opprecord.StageName,'Approved');  
        if(result.isSuccess()){
            opprecord.stagename='Approved';
            update opprecord;
        }      
        CaseCloseController.updatePO(opprecord.Id,date.today(),'Case in Review','Case Action');
        CaseCloseController.updatePO(opprecord.Id,date.today().addDays(5),'Case in Review','Case Action');
        CaseCloseController.updatePO(opprecord1.Id,date.today().addMonths(10),'Case in Review','Case Action');
        CaseCloseController.updatePO(null,date.today().addMonths(10),'Case in Review','Case Action');
    }
    public static testmethod void testcreatePOSOBatch(){
    	list<TriggerActivation__c> talist =[select id,name,isactive__C from TriggerActivation__c where name='PurchaseOrder Trigger'];
        list<Kinship_Case__c> cases=[select id from Kinship_Case__c limit 1];
        list<account> accounts=[select id,recordtypeid from account where name='test vendor' limit 1];
        Campaign parent = new Campaign();
        parent.name = '2021';
        parent.StartDate = Date.Today().addDays(-10);
        parent.EndDate = Date.Today().addDays(10);
        parent.Fiscal_Year_Type__c = 'Local';
        parent.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId();
        insert parent;           
        
        list<Category__c> categories=[select id from Category__c limit 1];
        
        Product2 p = new product2(name='x',isactive=true, Unit_Measure__c = '10');
        insert p;
        
        Product2 p2 = new product2(name='y',isactive=true, Unit_Measure__c = '10');
        insert p2;
        
        //define the standart price for the product
        id stdPb = Test.getStandardPricebookId();
        insert new PricebookEntry(pricebook2id = stdPb, product2id = p.id,unitprice=1.0, isActive=true);
        insert new PricebookEntry(pricebook2id = stdPb, product2id = p2.id,unitprice=1.0, isActive=true);
        Pricebook2 pb = new pricebook2(name='test',Vendor__c=accounts[0].id,IsActive=true,
                                       Effective_Date__c=system.today().addmonths(-2), Accounting_Period__c = parent.id );
        insert pb;
        PricebookEntry pbe = new PricebookEntry(pricebook2id=pb.id, product2id=p.id,unitprice=100.0, isActive=true);
        insert pbe; 
        PricebookEntry pbe1 = new PricebookEntry(pricebook2id=pb.id, product2id=p2.id,unitprice=1000.0, isActive=true);
        insert pbe1; 
   		test.startTest();
        opportunity opprecord = new opportunity();
        opprecord.CloseDate=date.today();
        opprecord.StageName='Draft';
        opprecord.Name='test opportunity';
        opprecord.Kinship_Case__c=cases[0].id;
        opprecord.End_Date__c=date.today();
        opprecord.Amount=20000;
        opprecord.Category__c =categories[0].id;
        opprecord.AccountId=accounts[0].id;
        opprecord.Number_of_Months_Units__c=2;
        insert opprecord;   
        //NOW CREATING LINE ITEMS
        opportunitylineitem opplineitem = new opportunitylineitem();
        opplineitem.Unit_Measure__c='1';
        opplineitem.Product2Id=p2.id;
        opplineitem.PricebookEntryId=pbe1.id;
        opplineitem.Quantity=3;
        opplineitem.OpportunityId=opprecord.id;
        opplineitem.TotalPrice = 20000;
        insert opplineitem;
        
        opportunitylineitem opplineitem1 = new opportunitylineitem();
        opplineitem1.Unit_Measure__c='1';
        opplineitem1.Product2Id=p.id;
        opplineitem1.PricebookEntryId=pbe.id;
        opplineitem1.Quantity=2;
        opplineitem1.OpportunityId=opprecord.id;
        opplineitem1.TotalPrice = 2000;
        insert opplineitem1;
        test.stopTest();
        Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitrequest();
        app.setObjectId(opprecord.Id);    
        // Submit the approval request for the po    
        Approval.ProcessResult result = Approval.process(app);    
        // Verify that the results are as expected    
        System.assert(result.isSuccess());
        
        if(result.isSuccess()){
            opprecord.stagename='Approved';
            update opprecord;
            list<opportunity> opplist=[select id,stagename from opportunity];
            system.debug('**VAl of opplist'+opplist);
            opprecord.stagename='Open';
            // opprecord.Number_of_Months_Units__c=1;
            update opprecord;
            
            list<Payment__c> payments=[select id,Written_Off__c,Paid__c from Payment__c
                                                 where Opportunity__c=:opprecord.Id];
            
            system.debug('**VAl of payments'+payments);
            list<Payment__c> paymentsupdte = new list<Payment__c>();
            for(Payment__c pay : payments){
                pay.Written_Off__c=true;
                paymentsupdte.add(pay);
            }
            update paymentsupdte;
          	 ClosePOSOBatch cb = new ClosePOSOBatch();
             database.executeBatch(cb);
         
            
        }  
         
    } 
    public static testmethod void closePOSOschedulartest(){
        Test.startTest();
        Datetime dt = Datetime.now().addMinutes(1);
        String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
        
            String jobId = System.schedule('ScheduleApexClassTest',
                                           CRON_EXP, 
                                           new ClosePOSOBatchScheduler());
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                              FROM CronTrigger WHERE id = :jobId];
            System.assertEquals(CRON_EXP, 
                                ct.CronExpression);
            
            Test.stopTest();   
        
    }  
}