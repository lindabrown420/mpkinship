public with sharing class IFSP_ServicePlan {

    @AuraEnabled
    public static IFSP__c getServicePlan(string recId) {

        IFSP__c sp = [Select id,Name,Case__c,CSA_Team__c,Case__r.Name
                                From IFSP__c
                                Where Id=: recId];
        return sp;
    }

    @AuraEnabled
    public static Kinship_Case__c getCaseRecord(string caseId) {

        Kinship_Case__c c = [Select id,Name,DOB__c,Gender_Preference__c,Age__c,Race__c
                                From Kinship_Case__c
                                Where Id=: caseId];
        return c;
    }

    @AuraEnabled
    public static List<Education__c> getEducationalInformationIds(string caseId){
        system.debug('getEducationalInformationIds');
        Map<Id,Education__c> educationMap = new Map<Id,Education__c>([Select id,Name from Education__c where Case__c =: caseId]);
        system.debug(educationMap);        
        return educationMap.values();
    }

    @AuraEnabled
    public static List<Health__c> getHealthInformationIds(string caseId){
        system.debug('getHealthInformationIds');
        Map<Id,Health__c> healthMap = new Map<Id,Health__c>([Select id,Name from Health__c where Case__c =: caseId]);
        system.debug(healthMap);        
        return healthMap.values();
    }

    @AuraEnabled
    public static List<Requisition__c> getRequisitionInformationIds(string caseId){
        system.debug('getRequisitionInformationIds');
        Map<Id,Requisition__c> requisitionhMap = new Map<Id,Requisition__c>([Select id,Name from Requisition__c where Case__c =: caseId]);
        system.debug(requisitionhMap);        
        return requisitionhMap.values();
    }

    @AuraEnabled
    public static string updateEducation(string recId, string caseId){
        Education__c edu = [Select id,case__c from Education__c where id=:recId];
        edu.case__c = caseId;

        try {
            update edu;
            return 'Success';
        } catch (Exception e) {
            return 'Failed';
        }
        
    }

    @AuraEnabled
    public static string updateHealth(string recId, string caseId){
        Health__c hlth = [Select id,case__c from Health__c where id=:recId];
        hlth.Case__c = caseId;

        try {
            update hlth;
            return 'Success';
        } catch (Exception e) {
            return 'Failed';
        }
    }

    @AuraEnabled
    public static string updateRequisition(string recId, string caseId){
        Requisition__c req = [Select id,case__c from Requisition__c where id=:recId];
        req.Case__c = caseId;

        try {
            update req;
            return 'Success';
        } catch (Exception e) {
            return 'Failed';
        }
    }

    /*@AuraEnabled
    public static string deleteRecords(string recId){
        sObject q = Id.valueof(recId).getSobjectType().newSobject(recId);
        delete q;

        return 'Success';
    }*/
}