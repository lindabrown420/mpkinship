@istest
public class POStageProgressBarCtrTest {
	 @testSetup
    static void testRecords(){
    	id recType = Schema.SObjectType.contact.getRecordTypeInfosByDeveloperName().get('Contacts').getRecordTypeId();
        contact con = new contact();
        con.recordtypeid=recType;
        con.LastName='contactrec';
        con.FirstName='test';
        con.Email='test@contact.com';
        //con.Race_of_the_Foster_Parent__c='White';
        //con.Gender_Preference__c='Male';
        insert con;
        
        Kinship_Case__c caserec = new Kinship_Case__c();
        caserec.Primary_Contact__c=con.id;
        insert caserec;
        
        //id vendorrecType = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Vendor').getRecordTypeId();
        account accountrec = new account();
        //accountrec.recordtypeid=vendorrecType;
        accountrec.Name='test vendor';
        accountrec.Tax_ID__c='222-22-3333';
        accountrec.Entity_Type__c='Individual';
        insert accountrec;
        
        Category__c category = new Category__c();
        category.Name='222';
        category.Category_Type__c='General Cases';
        category.recordtypeid=Schema.SObjectType.category__C.getRecordTypeInfosByDeveloperName().get('LASER_Category').getRecordTypeId();
        insert category;  
        
    }
    public static testmethod void POProgressBarTest(){
       
        list<Kinship_Case__c> cases=[select id from Kinship_Case__c];
        list<account> accounts=[select id,recordtypeid from account where name='test vendor'];        
        list<Category__c> categories=[select id from Category__c];
        Campaign parent = new Campaign();
            parent.name = '2021';
            parent.StartDate = Date.Today().addDays(-10);
            parent.EndDate = Date.Today().addDays(10);
            parent.Fiscal_Year_Type__c = 'Local';
            parent.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId();
            insert parent;

          //insert a new product
        Product2 p = new product2(name='x',isactive=true,Unit_Measure__c='2');
        insert p;
        
        Product2 p2 = new product2(name='y',isactive=true,Unit_Measure__c='1');
        insert p2;
        
        //define the standart price for the product
        id stdPb = Test.getStandardPricebookId();
        insert new PricebookEntry(pricebook2id = stdPb, product2id = p.id,unitprice=1.0, isActive=true);
        
          Pricebook2 pb = new pricebook2(name='test',Vendor__c=accounts[0].id,IsActive=true,
                                       Effective_Date__c=system.today().addmonths(-2), Accounting_Period__c = parent.id);
        insert pb;
        
        PricebookEntry pbe = new PricebookEntry(pricebook2id=pb.id, product2id=p.id,unitprice=1.0, isActive=true);
        insert pbe; 
    	opportunity opprecord = new opportunity();
        opprecord.CloseDate=date.today();
        opprecord.StageName='Draft';
        opprecord.Name='test opportunity';
        opprecord.Kinship_Case__c=cases[0].id;
        opprecord.Amount=20000;
        opprecord.Category__c =categories[0].id;
        opprecord.AccountId=accounts[0].id;
        opprecord.Number_of_Months_Units__c=3;
		opprecord.Pricebook2Id =pb.id;        
        insert opprecord;
        //NOW CREATING LINE ITEMS
        opportunitylineitem opplineitem = new opportunitylineitem();
        opplineitem.Unit_Measure__c='2';
        opplineitem.Product2Id=p.id;
        opplineitem.PricebookEntryId=pbe.id;
        opplineitem.Quantity=2;
        opplineitem.OpportunityId=opprecord.id;
        opplineitem.TotalPrice = 2000;
        insert opplineitem;
        POStageProgressBarCtr.getProgressValue(opprecord.id);
        system.assertEquals(opprecord.Number_of_Months_Units__c, 3);
        Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitrequest();
    	app.setObjectId(opprecord.Id);    
   		 // Submit the approval request for the po    
    	Approval.ProcessResult result = Approval.process(app);    
    	// Verify that the results are as expected    
    	System.assert(result.isSuccess());       
        if(result.isSuccess()){
            opprecord.stagename='Approved';
            update opprecord;
        } 
        POStageProgressBarCtr.getProgressValue(opprecord.id);
        opprecord.StageName='Open';
        update opprecord;
        POStageProgressBarCtr.getProgressValue(opprecord.id);
        opprecord.StageName='Closed';
        update opprecord;
         POStageProgressBarCtr.getProgressValue(opprecord.id);
        POStageProgressBarCtr.ChangeStage(opprecord.id);
        
        POStageProgressBarCtr.ChangeStage('test');
       
    }	    
}