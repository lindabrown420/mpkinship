@istest
public class FIPSTriggerHandlerTest {

    @istest
    public static void FIPSTriggerTest1(){
        TriggerActivation__c  ta = new TriggerActivation__c();
        ta.name = 'FIPSTrigger';
        ta.isActive__c = true;
        insert ta;

        FIPS__c fips = new FIPS__c();
        fips.Name = 'Test 1';
        fips.FIPS_Code__c = '5';
        fips.Locality__c = 'Locality';
        fips.Is_Default__c = true;
        insert fips;

        FIPS__c fips2 = new FIPS__c();
        fips2.Name = 'Test 2';
        fips2.FIPS_Code__c = '580';
        fips2.Locality__c = 'Locality 2';
        fips2.Is_Default__c = false;
        insert fips2;

        Campaign parent = new Campaign();
       	parent.name = '2021';
       	parent.StartDate = Date.Today().addDays(-10);
       	parent.EndDate = Date.Today().addDays(10);
        parent.Fiscal_Year_Type__c = 'DSS';
        parent.FIPS__c = fips.id;
       	parent.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId();
       	insert parent;

        Test.StartTest();
        fips.Locality__c = 'Locality 2';
        update fips;

        //fips2.Is_Default__c = true;
        //update fips2;
        Test.StopTest();

        parent = [SELECT Id, Name, FY_Name__c FROM Campaign WHERE id = :parent.Id];
        System.assertEquals(parent.Name, parent.FY_Name__c, 'The FIPS trigger does not running.');
    }
}