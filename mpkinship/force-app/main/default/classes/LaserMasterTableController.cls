public class LaserMasterTableController {
    public static String NA = 'n/a';
    public static String NA2 = 'na';
    @AuraEnabled
    public static List<LaserMasterWrapper> getListFile(){
        List<LaserMasterWrapper> result = new List<LaserMasterWrapper>();
        Map<String, Date> mapPD = new Map<String, Date>();
        for(LASER_MT_Date__c cv : [SELECT id,Name, Public_Date__c
                                FROM LASER_MT_Date__c]){
            mapPD.put(cv.name, cv.Public_Date__c);
        }
        KinshipAdmin__c laserMasterTable = KinshipAdmin__c.getAll().get('Laser Master Table');
        for(ContentDocumentLink cdl : [SELECT id,ContentDocumentId, LinkedEntityId, ContentDocument.Title, ContentDocument.CreatedDate, ContentDocument.CreatedBy.Name 
                                        FROM ContentDocumentLink 
                                        WHERE LinkedEntityId = :laserMasterTable.id
                                        ORDER BY ContentDocument.CreatedDate DESC]){
            LaserMasterWrapper temp = new LaserMasterWrapper();
            temp.id = cdl.ContentDocumentId;
            temp.fileName = cdl.ContentDocument.Title;
            temp.createdDate = cdl.ContentDocument.CreatedDate;
            temp.createdBy = cdl.ContentDocument.CreatedBy.Name;
            temp.publicDate = mapPD.get(cdl.ContentDocumentId);
            temp.url = URL.getSalesforceBaseUrl().toExternalForm() + '/sfc/servlet.shepherd/document/download/' + cdl.ContentDocumentId + '?operationContext=S1'; //https://acdss--kinshipdev.lightning.force.com/sfc/servlet.shepherd/document/download/' + cdl.ContentDocumentId + '?operationContext=S1';
            result.add(temp);
        }

        return result;
    }

    @AuraEnabled
    public static Boolean updateData(String strVD, Date pulicDate){
        system.debug('--updateData---' + strVD);
        List<ContentVersion>  lstData = [SELECT Id, Title, VersionData, ContentSize, FileType, TagCsv, ContentDocumentId, Public_Date__c
                                            FROM ContentVersion 
                                            WHERE id = :strVD];
        if(lstData.size() > 0) {
            System.debug('---updateData---' + EncodingUtil.base64Encode(lstData[0].VersionData));//lstData[0].VersionData.toString());
            List<String> csvData = lstData[0].VersionData.toString().split('\n');//EncodingUtil.base64Encode(lstData[0].VersionData).split('\n');
            System.debug('---updateData---' + csvData.size());
            if(csvData.size() > 1) {
                Boolean isFalse = false;
                try{
                    List<Laser_Master_Table__c> currentData = [SELECT Id, Name, CFDA__c, LASER_Alias_Name__c, Cost_Code__c, 
                                                                Allocated__c, Cost_Code_Description__c, 
                                                                PRGM__c, Grant__c, Project__c, B_Line__c, Exp_Type__c, State_0100__c, 
                                                                Federal_1000__c, local_0500__c, Cases__c, Children__c, Recipient__c, 
                                                                Adult__c, Child_In_Res_Fac__c, Special_Fund__c
                                                                FROM Laser_Master_Table__c];
                    Map<String, Laser_Master_Table__c> mapData = new Map<String, Laser_Master_Table__c>();
                    for(Laser_Master_Table__c mt : currentData) {
                        mapData.put(mt.Cost_Code__c, mt);
                    }
                    List<Laser_Master_Table__c> lstUpsert = new List<Laser_Master_Table__c>();
                    
                    String costCode;
                    for(Integer i = 1 ; i < csvData.size() ; i++){
                        List<String> data = csvData[i].split(',');
                        System.debug('---updateData---' + i + '-' + data.size());
                        if(data.size() == 20) {
                            costCode = leftPad(data[2], 5);
                            Laser_Master_Table__c lmt = mapData.get(costCode);
                            if(lmt == null) lmt = new Laser_Master_Table__c();
                            lmt.name = 'LASER Master Table Name';
                            lmt.Effective_Date__c = pulicDate;
                            lmt.CFDA__c = leftPad(data[0], 5);
                            lmt.LASER_Alias_Name__c = leftPad(data[1], 5);
                            lmt.Cost_Code__c = costCode;
                            lmt.Allocated__c = data[3];
                            lmt.Cost_Code_Description__c = data[4];
                            lmt.PRGM__c = leftPad(data[5], 5);
                            lmt.Grant__c = leftPad(data[6], 7);
                            lmt.Project__c = leftPad(data[7], 5);
                            lmt.B_Line__c = leftPad(data[8], 3);
                            lmt.Exp_Type__c = data[9];
                            lmt.State_0100__c = parseFunding(data[10]);
                            lmt.local_0500__c = parseFunding(data[11]);
                            lmt.Federal_1000__c = parseFunding(data[12]);
                            lmt.Special_Fund__c = parseFunding(data[13]);
                            lmt.Cases__c = parseBoolean(data[15]);
                            lmt.Children__c = parseBoolean(data[16]);
                            lmt.Adult__c = parseBoolean(data[17]);
                            lmt.Recipient__c = parseBoolean(data[18]);
                            lmt.Child_In_Res_Fac__c = parseBoolean(data[19]);
                            lstUpsert.add(lmt);
                        } else {// else for != 19 
                            isFalse = true;
                            break;
                        }
                    }
                    if(!isFalse && lstUpsert.size() > 0) {
                        upsert lstUpsert;
                    }
                    LASER_MT_Date__c lmtd = new LASER_MT_Date__c();
                    lmtd.name = lstData[0].ContentDocumentId;
                    lmtd.Public_Date__c = pulicDate;
                    insert lmtd;
                } catch (Exception e) {
                    System.debug('--updateData Error---' + e);
                    isFalse = true;
                }
                if(isFalse){
                    try{
                        ContentDocument cd = new ContentDocument(id=lstData[0].ContentDocumentId);
                        delete cd;
                    }catch (Exception e) {
                        System.debug('--updateData Error---' + e);
                    }
                }
                return !isFalse;
            }
        }
        return true;
    }

    @testVisible
    private static String leftPad(String str, Integer length) {
        if(String.isBlank(str)) str = '';
        return str.trim().leftPad(length, '0');
    }

    @testVisible
    private static Decimal parseFunding(String str) {
        if(String.isBlank(str) || NA.equals(str.toLowerCase()) || NA2.equals(str.toLowerCase())) return 0;
        return Decimal.valueOf(str.replace('%', ''));
    }

    @testVisible
    private static Boolean parseBoolean(String str) {
        if(String.isBlank(str) || NA.equals(str.toLowerCase()) || NA2.equals(str.toLowerCase())) return false;
        return true;
    }

    public class LaserMasterWrapper{
        @AuraEnabled
        public String id{get;set;}
        @AuraEnabled
        public String fileName{get;set;}
        @AuraEnabled
        public String url{get;set;}
        @AuraEnabled
        public String createdBy{get;set;}
        @AuraEnabled
        public Datetime createdDate{get;set;}
        @AuraEnabled
        public Date publicDate{get;set;}
    }
}