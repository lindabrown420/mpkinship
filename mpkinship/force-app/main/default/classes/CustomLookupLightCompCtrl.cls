public class CustomLookupLightCompCtrl {
         
        @AuraEnabled
        public static List < sObject > fetchLookUpValues(String searchKeyWord, String ObjectName) {
            List < sObject > returnList = new List <sObject>();
            List < sObject > lstOfRecords =  new List<sObject>();
           
            String searchKey = searchKeyWord + '%';
            
                String sQuery;
                if(ObjectName == 'Campaign'){
                     string typeval ='Local';
                     string tempInput = '%' + searchKey + '%';
                     Id campRecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Fiscal_Year').getRecordTypeId();
                    sQuery='select id, Name, StartDate, EndDate,FIPS__c from Campaign where Name LIKE: tempInput and Fiscal_Year_Type__c=:typeval and recordtypeid=:campRecordTypeId and isactive=true ';
                   
                }
                else{
                    sQuery =  'select id, Name from ' +ObjectName + ' where Name LIKE: searchKey ';
                }
               
                sQuery += ' order by createdDate DESC limit 5';
                System.debug('SQUERY'+' '+sQuery);
                lstOfRecords = Database.query(sQuery);
                System.debug('sObject Records'+' '+lstOfRecords);
                 for (sObject obj: lstOfRecords) {
                    returnList.add(obj);
                  }
            
            
            return returnList;
        }
        

         

}