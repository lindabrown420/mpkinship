public class PaymentApprovalTriggerHandler extends TriggerHandler{
    /**
     * Called prior to BEFORE trigger. Use to cache any data required
     * input maps prior to execution of the BEFORE trigger
     */
    public override void bulkBefore() {}

    /**
     * Called prior to AFTER trigger. Use to cache any data required
     * input maps prior to execution of the AFTER trigger
     */
    public override void bulkAfter() {}

    /**
     * Handles before insert operations for all records being inserted.
     * Should only be used in BEFORE Trigger context
     */
    public override void beforeInsert() {}

    /**
     * Handles after insert operations for all records being inserted.
     * Should only be used in AFTER Trigger context
     */
    public override void afterInsert() {
    }

    /**
     * Handles before update operations for all records being updated.
     * Should only be used in BEFORE Trigger context
     */
    public override void beforeUpdate() {
    }

    /**
     * Handles after update operations for all records being updated.
     * Should only be used in AFTER Trigger context
     */
    public override void afterUpdate() {
        PaymentApprovalServices.updatePaymentStatus(Trigger.new, Trigger.old);
    }

    /**
     * Handles before delete operations for all records being deleted.
     * Should only be used in BEFORE Trigger context
     */
    public override void beforeDelete() {
    }

    /**
     * Handles after delete operations for all records being deleted.
     * Should only be used in AFTER Trigger context
     */
    public override void afterDelete() {}

    /**
     * Call once all records have been processed by the trigger handler.
     * Use this to execute all final operations like DML of other records.
     */
    public override void andFinally() {
        //upsert or insert records
        //this.insertHistory();
    }
}