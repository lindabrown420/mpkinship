public class LEDRSReportController {
    private final static Integer MAX_RESULTS = 5;
    public final static String CAMP = 'Campaign';
    public final static String CAMP2 = 'Campaign2';

    @AuraEnabled(cacheable=true)
    public static List<LookupSearchResult> kinshipSearchLookup(String searchTerm, List<String> selectedIds, String anOptionalParam) {
        // Prepare query paramters
        searchTerm += '*';

        // Prepare results
        List<LookupSearchResult> results = new List<LookupSearchResult>();
        List<List<SObject>> searchResults;

        if(String.isNotBlank(anOptionalParam)) {
            if(CAMP.equals(anOptionalParam)) {
                searchResults = [FIND :searchTerm IN ALL FIELDS RETURNING
                                Campaign (Id, Name, Status WHERE id NOT IN :selectedIds AND RecordType.Name = 'LEDRS Reporting Period' AND IsActive = true ORDER BY StartDate DESC)
                                LIMIT :MAX_RESULTS];
                String cItem = 'standard:campaign';
                Campaign[] objs = ((List<Campaign>) searchResults[0]);
                for (Campaign o : objs) {
                    results.add(new LookupSearchResult(o.Id, 'Campaign', cItem, o.Name, o.Status));
                }
            } else if(CAMP2.equals(anOptionalParam)) {
                searchResults = [FIND :searchTerm IN ALL FIELDS RETURNING
                                Campaign (Id, Name, Status WHERE id NOT IN :selectedIds AND RecordType.Name = 'Fiscal Year' AND Fiscal_Year_Type__c = 'DSS' AND IsActive = true ORDER BY StartDate DESC)
                                LIMIT :MAX_RESULTS];
                String cItem = 'standard:campaign';
                Campaign[] objs = ((List<Campaign>) searchResults[0]);
                for (Campaign o : objs) {
                    results.add(new LookupSearchResult(o.Id, 'Campaign', cItem, o.Name, ''));
                }
            }
        }

        return results;
    }

    @AuraEnabled 
    public static List<Campaign> getAllAP(String parentId){
        return [SELECT Id, Name, StartDate, EndDate FROM Campaign WHERE ParentId = :parentId ORDER BY StartDate ASC];
    }

    @AuraEnabled 
    public static Boolean checkPostAP(List<String> lstId){
        return TransactionJournalService.checkPostedTJ(lstId);
    }

    @AuraEnabled 
    public static void postedAP(List<String> lstId){
        List<Campaign> lstCamp = [SELECT id, Name, StartDate, EndDate, RecordTypeId, ParentId, Parent.ParentId, Status, FIPS__c, Cate_Type__c
                                    FROM Campaign  
                                    WHERE id IN :lstId];
        TransactionJournalService.autoPostedTJ(lstCamp);
    }

    @AuraEnabled
    public static List<LEDRSReport> getLEDRSData(List<String> lstCamp){
        List<LEDRSReport> lstReturn = new List<LEDRSReport>();
        List<String> lstFundingSource = new List<String>();
        if(lstCamp.size() == 1) {
            lstFundingSource.add('CSA');
        } else {
            lstFundingSource.add('IVE');
        }
        Map<String, String> mapServiceNameCode = new Map<String, String>();
        for(Service_Names_Code__c sc : [SELECT id, Service_Name_Code__c, LASER_Account_Code__c FROM Service_Names_Code__c WHERE LASER_Account_Code__c != '']){
            mapServiceNameCode.put(sc.LASER_Account_Code__c, sc.Service_Name_Code__c);
        }
        List<Transaction_Journal__c> lstTJ = [SELECT id,name, Invoice__r.RecordType.Name,Invoice__r.RecordTypeId,
                                                    Invoice__c,Invoice__r.Opportunity__c,Invoice__r.Kinship_Case__c,
                                                    FIPS_Code__c,
                                                    Invoice__r.Kinship_Case__r.Kinship_Case_Number__c,
                                                    Invoice__r.Kinship_Case__r.SSN__c,
                                                    Invoice__r.Kinship_Case__r.Student_Identifier__c,
                                                    Invoice__r.Kinship_Case__r.Primary_Contact__r.FirstName,
                                                    Invoice__r.Kinship_Case__r.Primary_Contact__r.LastName,
                                                    Invoice__r.Kinship_Case__r.Primary_Contact__r.MiddleName,
                                                    Invoice__r.Kinship_Case__r.Primary_Contact__r.suffix,
                                                    Invoice__r.Kinship_Case__r.OASIS_Client_ID__c,
                                                    Invoice__r.Kinship_Case__r.DOB__c,
                                                    Invoice__r.Kinship_Case__r.Gender_Preference__c,
                                                    Invoice__r.Kinship_Case__r.Race__c,
                                                    Invoice__r.Kinship_Case__r.Hispanic_Ethnicity__c,
                                                    Invoice__r.Kinship_Case__r.Discharge_Reason_Code__c,
                                                    Invoice__r.Kinship_Case__r.Referral_Source__c,
                                                    Invoice__r.Kinship_Case__r.Autism_Flag_1__c,
                                                    Invoice__r.Kinship_Case__r.DSM_V_Indicator_ICD_10_1__c,
                                                    Invoice__r.Kinship_Case__r.Clinical_Medication_Indicator_1__c,
                                                    Invoice__r.Kinship_Case__r.Medicaid_Indicator_1__c,
                                                    Invoice__r.Opportunity__r.Account.Name,
                                                    Invoice__r.Opportunity__r.Account.Tax_ID__c,
                                                    Invoice__r.Opportunity__r.Account.ShippingStreet,
                                                    Invoice__r.Opportunity__r.Account.ShippingCity,
                                                    Invoice__r.Opportunity__r.Account.ShippingState,
                                                    Invoice__r.Opportunity__r.Account.ShippingPostalCode,
                                                    Invoice__r.Opportunity__r.Account.ShippingCountry,
                                                    Invoice__r.Opportunity__r.Account.Phone,
                                                    Invoice__r.Opportunity__r.Account.BillingStreet,
                                                    Invoice__r.Opportunity__r.Account.BillingCity,
                                                    Invoice__r.Opportunity__r.Account.BillingState,
                                                    Invoice__r.Opportunity__r.Account.BillingPostalCode,
                                                    Invoice__r.Opportunity__r.Account.Service_Phone__c,
                                                    Invoice__r.Opportunity__r.Account.Type_of_Provider__c,
                                                    Invoice__r.Opportunity__r.Account.Locality_Provider_Identifier__c,
                                                    Invoice__r.PO_Number__c,
                                                    Invoice__r.Accounting_Period__r.StartDate,
                                                    Invoice__r.Accounting_Period__r.EndDate,
                                                    //Invoice__r.Opportunity__r.Locality_Service_Record_Identifier__c,
                                                    Invoice__r.Opportunity__r.Parent_Recipient__c,
                                                    Invoice__r.Service_Begin_Date__c, Invoice__r.Opportunity__r.CloseDate,
                                                    Invoice__r.Service_End_Date__c, Invoice__r.Opportunity__r.End_Date__c, 
                                                    Invoice__r.Opportunity__r.Service_Placement_Type_Code__c,
                                                    Invoice__r.Opportunity__r.Mandate_Type_Code__c,
                                                    Invoice__r.Opportunity__r.Service_Name_Code_Value__c,
                                                    Invoice__r.Opportunity__r.Service_Description_Other__c,
                                              		Invoice__r.Opportunity__r.Number_of_Months_Units__c,
                                              		Invoice__r.Opportunity__r.RecordType.Name,
                                                    //Invoice__r.Locality_Payment_Identifier__c,
                                                    Invoice__r.Payment_Date__c,
                                                    Warrant__c,
                                                    Invoice__r.Payment_Amount__c,
                                                    Invoice__r.Other_Transaction_Description__c,
                                                    Invoice__r.Category_lookup__r.LASER_Cost_Code__r.Name,Invoice__r.Category_lookup__r.CSA_Category__r.Category_Code__c,
                                                    Invoice__r.Category_lookup__r.LASER_Cost_Code__r.Cost_Code_Title__c,Invoice__r.Category_lookup__r.CSA_Category__r.Name,
                                                    Invoice__r.Category_lookup__r.Category_Type__c,
                                                    Invoice__r.Kinship_Case__r.Title_IVE_Eligibility__c,
                                                    Invoice__r.Accounting_Period__r.Parent.EndDate,
                                                    Invoice__r.Opportunity__r.Campaign.EndDate,
                                                    Invoice__r.Payment_Source__c,
                                                    Expenditure_Refund_Code__c,
                                                    Invoice__r.Locality_Invoice_Identifier__c, Invoice__r.Locality_Service_Identifier__c,
                                              		Invoice__r.Category_lookup__r.LASER_Account_Code__c
                                                FROM Transaction_Journal__c
                                                WHERE Child_Campaign__c IN :lstCamp
                                                AND Invoice__r.Category_lookup__r.Category_Type__c IN :lstFundingSource];
        System.debug('---getLEDRSData---' + lstTJ);
        System.debug('---getLEDRSData---' + lstCamp);
        if(lstTJ.size() == 0) return lstReturn;
        Set<id> setPaymentId = new Set<id>();
        Set<Id> setOpp = new Set<Id>();
        List<LEDRSReport> lstTemp = new List<LEDRSReport>();
        for(Transaction_Journal__c tj : lstTJ) {
            if(tj.Invoice__r.Payment_Amount__c <= 0 &&
              !(tj.Invoice__r.RecordType.Name == 'Adjustments' && 
                tj.Invoice__r.Payment_Amount__c < 0 && 
                (tj.Invoice__r.Payment_Source__c == '2' || tj.Invoice__r.Payment_Source__c == '12'))) continue;
            LEDRSReport result = new LEDRSReport();
            result.id = tj.id;
            result.tjurl = '/' + tj.id;
            result.name = tj.Name;
            result.localityIdentifier = tj.FIPS_Code__c;
            result.localityChildIdentifier = replaceString(tj.Invoice__r.Kinship_Case__r.Kinship_Case_Number__c, 'KPC-', '');
            result.ssn = tj.Invoice__r.Kinship_Case__r.SSN__c;
            result.studentIdentifier = tj.Invoice__r.Kinship_Case__r.Student_Identifier__c;
            result.lastName = tj.Invoice__r.Kinship_Case__r.Primary_Contact__r.LastName;
            result.firstName = tj.Invoice__r.Kinship_Case__r.Primary_Contact__r.FirstName;
            result.middleName = tj.Invoice__r.Kinship_Case__r.Primary_Contact__r.MiddleName;
            result.suffix = tj.Invoice__r.Kinship_Case__r.Primary_Contact__r.suffix;
            result.oasisClientId = tj.Invoice__r.Kinship_Case__r.OASIS_Client_ID__c;
            result.dob = tj.Invoice__r.Kinship_Case__r.DOB__c;
            result.gender = tj.Invoice__r.Kinship_Case__r.Gender_Preference__c;
            result.race = tj.Invoice__r.Kinship_Case__r.Race__c;
            result.hispanicEthnicity = tj.Invoice__r.Kinship_Case__r.Hispanic_Ethnicity__c;
            result.dischargeReasonCode = replaceString(tj.Invoice__r.Kinship_Case__r.Discharge_Reason_Code__c, 'None', '');
            result.referralSource = tj.Invoice__r.Kinship_Case__r.Referral_Source__c;
            result.autsmFlag = tj.Invoice__r.Kinship_Case__r.Autism_Flag_1__c;
            result.dsmVIndicator = tj.Invoice__r.Kinship_Case__r.DSM_V_Indicator_ICD_10_1__c;
            result.clinicalMedication = tj.Invoice__r.Kinship_Case__r.Clinical_Medication_Indicator_1__c;
            result.medicaidIndicator = tj.Invoice__r.Kinship_Case__r.Medicaid_Indicator_1__c;
            result.providerName = tj.Invoice__r.Opportunity__r.Account.Name;
            result.taxID = replaceString(tj.Invoice__r.Opportunity__r.Account.Tax_ID__c,'-','');
            result.serviceAddress1 = tj.Invoice__r.Opportunity__r.Account.ShippingStreet;
            result.serviceAddress2 = '';//String.isNotBlank(tj.Receipt__c)?tj.Receipt__r.Vendor__r.Name:tj.Invoice__r.Opportunity__r.Account.Name;
            result.serviceCity = tj.Invoice__r.Opportunity__r.Account.ShippingCity;
            result.serviceState = tj.Invoice__r.Opportunity__r.Account.ShippingState;
            result.serviceZip = tj.Invoice__r.Opportunity__r.Account.ShippingPostalCode;
            result.billingUS = getCountryCode(tj.Invoice__r.Opportunity__r.Account.ShippingCountry);
            result.billingPhone = tj.Invoice__r.Opportunity__r.Account.Phone;
            result.billingAddress1 = tj.Invoice__r.Opportunity__r.Account.BillingStreet;
            result.billingAddress2 = '';//String.isNotBlank(tj.Receipt__c)?tj.Receipt__r.Vendor__r.Name:tj.Invoice__r.Opportunity__r.Account.Name;
            result.billingCity = tj.Invoice__r.Opportunity__r.Account.BillingCity;
            result.billingState = tj.Invoice__r.Opportunity__r.Account.BillingState;
            result.billingZip = tj.Invoice__r.Opportunity__r.Account.BillingPostalCode;
            result.servicePhone = tj.Invoice__r.Opportunity__r.Account.Service_Phone__c;
            result.typeOfProvider = tj.Invoice__r.Opportunity__r.Account.Type_of_Provider__c;
            result.localityProviderIdentifier = tj.Invoice__r.Opportunity__r.Account.Locality_Provider_Identifier__c;
            result.purchaseOrder = replaceString(replaceString(replaceString(tj.Invoice__r.PO_Number__c,'PO-',''),'CA-',''),'VN-','');
            result.poBeginDate = tj.Invoice__r.Opportunity__r.CloseDate;
            result.poEndDate = tj.Invoice__r.Opportunity__r.End_Date__c;
            result.dateOfServiceBeginning = tj.Invoice__r.Service_Begin_Date__c;//tj.Invoice__r.Opportunity__r.Date_of_Service_Beginning__c;
            result.dateOfServiceEnd = tj.Invoice__r.Service_End_Date__c;//tj.Invoice__r.Opportunity__r.Date_of_Service_End__c;
            if(tj.Invoice__r.Opportunity__r.RecordType.Name == 'Case Actions') {
                result.poMaxQuantity = tj.Invoice__r.Opportunity__r.Number_of_Months_Units__c;
                result.poUnitPrice = KinshipUtil.getAmountValue(tj.Invoice__r.Payment_Amount__c);
                result.poUnitMeasure = 'Monthly';
                result.quantityBilled = 1;
            } else if(tj.Invoice__r.Opportunity__r.RecordType.Name == 'Staff & Operations' || tj.Invoice__r.RecordType.Name == 'Special Welfare Reimbursement') {
                result.poMaxQuantity = 1;
                result.poUnitPrice = KinshipUtil.getAmountValue(tj.Invoice__r.Payment_Amount__c);
                result.poUnitMeasure = 'Each';
                result.quantityBilled = 1;
                result.dateOfServiceBeginning  = tj.Invoice__r.Accounting_Period__r.StartDate;
                result.dateOfServiceEnd  = tj.Invoice__r.Accounting_Period__r.EndDate;
            } else {
                result.poMaxQuantity = 0;
                result.poUnitPrice = 0;
                result.poUnitMeasure = '';
                result.quantityBilled = 0; // payment line item
            }
            result.localityServiceRecord = tj.Invoice__r.Locality_Service_Identifier__c;
            result.parentRecipient = tj.Invoice__r.Opportunity__r.Parent_Recipient__c;
            result.spt = tj.Invoice__r.Opportunity__r.Service_Placement_Type_Code__c;
            result.mandateType = tj.Invoice__r.Opportunity__r.Mandate_Type_Code__c;
            if(lstCamp.size() == 3) {
                result.serviceNameCode = mapServiceNameCode.get(tj.Invoice__r.Category_lookup__r.LASER_Account_Code__c);
            } else {
            	result.serviceNameCode = tj.Invoice__r.Opportunity__r.Service_Name_Code_Value__c;
            }
            result.serviceDesOther = tj.Invoice__r.Opportunity__r.Service_Description_Other__c;
            result.localityPaymentIdentifier = tj.Invoice__r.Locality_Invoice_Identifier__c;
            result.adjustmentDate = tj.Invoice__r.Payment_Date__c;
            result.warrant = replaceString(tj.Warrant__c,'PMT-','');
            result.adjustmentAmount = KinshipUtil.getAmountValue(tj.Invoice__r.Payment_Amount__c);
            if(tj.Invoice__r.RecordType.Name == 'Adjustments' && tj.Invoice__r.Payment_Amount__c < 0 && (tj.Invoice__r.Payment_Source__c == '2' || tj.Invoice__r.Payment_Source__c == '12')){
                result.adjustmentAmount = -tj.Invoice__r.Payment_Amount__c;
            }
            result.transactionCode = tj.Invoice__r.Payment_Source__c;//'1';//tj.Transaction_Code_Number__c;
            result.transactionDescription = tj.Invoice__r.Other_Transaction_Description__c;
            if(tj.Invoice__r.Category_lookup__r.Category_Type__c == 'CSA') {
                result.expenditureCategory = tj.Invoice__r.Category_lookup__r.CSA_Category__r.Category_Code__c;
                result.categoryDes = tj.Invoice__r.Category_lookup__r.CSA_Category__r.Name;
            } else {
                result.expenditureCategory = tj.Invoice__r.Category_lookup__r.LASER_Cost_Code__r.Name;
                result.categoryDes = tj.Invoice__r.Category_lookup__r.LASER_Cost_Code__r.Cost_Code_Title__c;
            }
            Date d = tj.Invoice__r.Opportunity__r.Campaign.EndDate;
            result.programYear = '';
            if(d != null) {
                result.programYear = String.valueOf(d.Year());
            }
            result.fundingSource = tj.Invoice__r.Category_lookup__r.Category_Type__c;
            result.eligibilityFlag = tj.Invoice__r.Kinship_Case__r.Title_IVE_Eligibility__c;
            if(Label.KINSHIP_RECEIPT_ID.contains(tj.Invoice__r.RecordTypeID)) {
                result.receiptid = tj.Invoice__c;
                result.paymentid = '';
            } else {
                result.paymentid = tj.Invoice__c;
                result.receiptid = '';
            }
            result.transCode = tj.Invoice__r.Payment_Source__c;
            result.expCode = tj.Expenditure_Refund_Code__c;
            result.recordType = tj.Invoice__r.RecordType.Name;
            lstTemp.add(result);

            if(String.isNotBlank(tj.Invoice__c)) {
                setPaymentId.add(tj.Invoice__c);
                setOpp.add(tj.Invoice__r.Opportunity__c);
            }
        }

        // get Opp Line Item
        List<OpportunityLineItem> lstOLI = [SELECT Id, OpportunityId, Mandate_Type_Code__c, Mandate_Type__c, Parent_Recipient__c, 
                                                        Service_Description_Other__c, Service_Name_Code_Value__c, Service_Name_Code__c, 
                                                        Service_Placement_Type_Code__c, Service_Placement_Type__c
                                            FROM OpportunityLineItem
                                            WHERE OpportunityId IN :setOpp];
        Map<String, OpportunityLineItem> mapOLI = new Map<String, OpportunityLineItem>();
        for(OpportunityLineItem oli : lstOLI) {
            mapOLI.put(oli.id, oli);
        }

        List<Payment_Line_Item__c> lstPLI = [SELECT Id, Name, Billed_Amount__c, Quantity_Billed__c, LineItem_Amount_Billed__c,
                                            Opportunity_Product__c, Invoice__c, Unit_Measure__c, Unit_Price__c, Product__c, Max_Quantity_Authorized__c,
                                            Invoice__r.Opportunity__r.RecordType.Name,Locality_Invoice_Identifier__c, Locality_Service_Identifier__c
                                            FROM Payment_Line_Item__c
                                            WHERE Invoice__c IN :setPaymentId];
        System.debug('---getLEDRSData---' + lstPLI);
        if(lstPLI.size() == 0) return lstTemp;
        Map<String, List<Payment_Line_Item__c>> mapPLI = new Map<String, List<Payment_Line_Item__c>>();
        List<Payment_Line_Item__c> tPLI;
        for(Payment_Line_Item__c pli : lstPLI){
            tPLI = mapPLI.get(pli.Invoice__c);
            if(tPLI == null) {
                tPLI = new List<Payment_Line_Item__c>();
            }
            tPLI.add(pli);
            mapPLI.put(pli.Invoice__c, tPLI);
        }
        LEDRSReport temp;
        for(LEDRSReport ledrs : lstTemp){
            tPLI = mapPLI.get(ledrs.paymentid);
            if(tPLI != null) {
                for(Payment_Line_Item__c pli : tPLI){
                    temp = cloneLEDRS(ledrs);
                    temp.quantityBilled = pli.Quantity_Billed__c;
                    temp.poMaxQuantity = pli.Max_Quantity_Authorized__c;
                    temp.poUnitPrice = pli.Unit_Price__c;
                    temp.poUnitMeasure = pli.Unit_Measure__c;
                    if(pli.Invoice__r.Opportunity__r.RecordType.Name == 'Purchase of Services Order') {
                        temp.adjustmentAmount = pli.LineItem_Amount_Billed__c;
                        OpportunityLineItem oli = mapOLI.get(pli.Opportunity_Product__c);
                        if(oli != null) {
                            temp.parentRecipient = oli.Parent_Recipient__c;
                            temp.spt = oli.Service_Placement_Type_Code__c;
                            temp.mandateType = oli.Mandate_Type_Code__c;
                            temp.serviceNameCode = oli.Service_Name_Code_Value__c;
                            temp.serviceDesOther = oli.Service_Description_Other__c;
                        }
                    }
                    temp.localityPaymentIdentifier = pli.Locality_Invoice_Identifier__c;
                    temp.localityServiceRecord = pli.Locality_Service_Identifier__c;
                    if(temp.adjustmentAmount > 0) {
                        lstReturn.add(temp);
                    }
                }
            } else {
                lstReturn.add(ledrs);
            }
        }
        System.debug('---getLEDRSData---' + lstReturn);

        return lstReturn;
    }

    @testVisible
    private static String getCountryCode(String str) {
        if(String.isNotBlank(str) && str == 'US') {
            return '1';
        }
        return '0';
    }
    
    @testVisible
    private static String replaceString(String str, String strFrom, String strTo) {
        System.debug('---replaceString---' + str);
        if(String.isNotBlank(str)) {
            return str.replaceAll(strFrom, strTo);
        }
        return '';
    }

    private static LEDRSReport cloneLEDRS(LEDRSReport ledrs){
        LEDRSReport result = new LEDRSReport();
        result.id = ledrs.id;
        result.name = ledrs.name;
        result.tjurl = ledrs.tjurl;
        result.localityIdentifier = ledrs.localityIdentifier;
        result.localityChildIdentifier = ledrs.localityChildIdentifier;
        result.ssn = ledrs.ssn;
        result.studentIdentifier = ledrs.studentIdentifier;
        result.lastName = ledrs.lastName;
        result.firstName = ledrs.firstName;
        result.middleName = ledrs.middleName;
        result.suffix = ledrs.suffix;
        result.oasisClientId = ledrs.oasisClientId;
        result.dob = ledrs.dob;
        result.gender = ledrs.gender;
        result.race = ledrs.race;
        result.hispanicEthnicity = ledrs.hispanicEthnicity;
        result.dischargeReasonCode = ledrs.dischargeReasonCode;
        result.referralSource = ledrs.referralSource;
        result.autsmFlag = ledrs.autsmFlag;
        result.dsmVIndicator = ledrs.dsmVIndicator;
        result.clinicalMedication = ledrs.clinicalMedication;
        result.medicaidIndicator = ledrs.medicaidIndicator;
        result.providerName = ledrs.providerName;
        result.taxID = ledrs.taxID;
        result.billingAddress1 = ledrs.billingAddress1;
        result.billingAddress2 = ledrs.billingAddress2;
        result.billingCity = ledrs.billingCity;
        result.billingState = ledrs.billingState;
        result.billingZip = ledrs.billingZip;
        result.billingUS = ledrs.billingUS;
        result.billingPhone = ledrs.billingPhone;
        result.serviceAddress1 = ledrs.serviceAddress1;
        result.serviceAddress2 = ledrs.serviceAddress2;
        result.serviceCity = ledrs.serviceCity;
        result.serviceState = ledrs.serviceState;
        result.serviceZip = ledrs.serviceZip;
        result.servicePhone = ledrs.servicePhone;
        result.typeOfProvider = ledrs.typeOfProvider;
        result.localityProviderIdentifier = ledrs.localityProviderIdentifier;
        result.purchaseOrder = ledrs.purchaseOrder;
        result.poBeginDate = ledrs.poBeginDate;
        result.poEndDate = ledrs.poEndDate;
        result.poMaxQuantity = ledrs.poMaxQuantity;
        result.poUnitPrice = ledrs.poUnitPrice;
        result.poUnitMeasure = ledrs.poUnitMeasure;
        result.localityServiceRecord = ledrs.localityServiceRecord;
        result.parentRecipient = ledrs.parentRecipient;
        result.dateOfServiceBeginning = ledrs.dateOfServiceBeginning;
        result.dateOfServiceEnd = ledrs.dateOfServiceEnd;
        result.spt = ledrs.spt;
        result.mandateType = ledrs.mandateType;
        result.serviceNameCode = ledrs.serviceNameCode;
        result.serviceDesOther = ledrs.serviceDesOther;
        result.localityPaymentIdentifier = ledrs.localityPaymentIdentifier;
        result.adjustmentDate = ledrs.adjustmentDate;
        result.warrant = ledrs.warrant;
        result.quantityBilled = ledrs.quantityBilled;
        result.adjustmentAmount = ledrs.adjustmentAmount;
        result.transactionCode = ledrs.transactionCode;
        result.transactionDescription = ledrs.transactionDescription;
        result.expenditureCategory = ledrs.expenditureCategory;
        result.programYear = ledrs.programYear;
        result.fundingSource = ledrs.fundingSource;
        result.paymentid = ledrs.paymentid;
        result.receiptid = ledrs.receiptid;
        result.eligibilityFlag = ledrs.eligibilityFlag;
        result.categoryDes = ledrs.categoryDes;
        result.transCode = ledrs.transCode;
        result.expCode = ledrs.expCode;
        result.recordType = ledrs.recordType;
        
        return result;
    }

    @AuraEnabled
    public static Map<String, String> getPicklistvalues(String objectName, String field_apiname){
        Map<String, String> optionlist = new Map<String, String>();       
        Map<String,Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
        Map<String, Schema.SObjectField> field_map = gd.get(objectName.toLowerCase()).getDescribe().fields.getMap();        
        List<Schema.PicklistEntry> picklistValues = field_map.get(field_apiname).getDescribe().getPickListValues(); 
        for (Schema.PicklistEntry pv : picklistValues) {
            optionlist.put(pv.getValue(), pv.getLabel());
        }
        return optionlist;
    }

    public class LEDRSReport{
        @AuraEnabled
        public String id{get;set;}

        @AuraEnabled
        public String tjurl{get;set;}

        @AuraEnabled
        public String name{get;set;}

        @AuraEnabled
        public String localityIdentifier{get;set;}

        @AuraEnabled
        public String localityChildIdentifier{get;set;}

        @AuraEnabled
        public String ssn{get;set;}

        @AuraEnabled
        public String studentIdentifier{get;set;}

        @AuraEnabled
        public String lastName{get;set;}

        @AuraEnabled
        public String firstName{get;set;}

        @AuraEnabled
        public String middleName{get;set;}

        @AuraEnabled
        public String suffix{get;set;}

        @AuraEnabled
        public String oasisClientId{get;set;}

        @AuraEnabled
        public Date dob{get;set;}

        @AuraEnabled
        public String gender{get;set;}

        @AuraEnabled
        public String race{get;set;}

        @AuraEnabled
        public String hispanicEthnicity{get;set;}

        @AuraEnabled
        public String dischargeReasonCode{get;set;}

        @AuraEnabled
        public String referralSource{get;set;}

        @AuraEnabled
        public String autsmFlag{get;set;}

        @AuraEnabled
        public String dsmVIndicator{get;set;}

        @AuraEnabled
        public String clinicalMedication{get;set;}

        @AuraEnabled
        public String medicaidIndicator{get;set;}

        @AuraEnabled
        public String localityProviderIdentifier{get;set;}

        @AuraEnabled
        public String providerName{get;set;}

        @AuraEnabled
        public String taxID{get;set;}

        @AuraEnabled
        public String billingAddress1{get;set;}

        @AuraEnabled
        public String billingAddress2{get;set;}

        @AuraEnabled
        public String billingCity{get;set;}

        @AuraEnabled
        public String billingState{get;set;}

        @AuraEnabled
        public String billingZip{get;set;}

        @AuraEnabled
        public String billingUS{get;set;}

        @AuraEnabled
        public String billingPhone{get;set;}

        @AuraEnabled
        public String serviceAddress1{get;set;}

        @AuraEnabled
        public String serviceAddress2{get;set;}

        @AuraEnabled
        public String serviceCity{get;set;}

        @AuraEnabled
        public String serviceState{get;set;}

        @AuraEnabled
        public String serviceZip{get;set;}

        @AuraEnabled
        public String servicePhone{get;set;}

        @AuraEnabled
        public String typeOfProvider{get;set;}

        @AuraEnabled
        public String purchaseOrder{get;set;}

        @AuraEnabled
        public Date poBeginDate{get;set;}

        @AuraEnabled
        public Date poEndDate{get;set;}

        @AuraEnabled
        public Decimal poMaxQuantity{get;set;}

        @AuraEnabled
        public Decimal poUnitPrice{get;set;}

        @AuraEnabled
        public String poUnitMeasure{get;set;}

        @AuraEnabled
        public String localityServiceRecord{get;set;}

        @AuraEnabled
        public String parentRecipient{get;set;}

        @AuraEnabled
        public Date dateOfServiceBeginning{get;set;}

        @AuraEnabled
        public Date dateOfServiceEnd{get;set;}

        @AuraEnabled
        public String spt{get;set;}

        @AuraEnabled
        public String mandateType{get;set;}

        @AuraEnabled
        public String serviceNameCode{get;set;}

        @AuraEnabled
        public String serviceDesOther{get;set;}

        @AuraEnabled
        public String localityPaymentIdentifier{get;set;}

        @AuraEnabled
        public Date adjustmentDate{get;set;}

        @AuraEnabled
        public String warrant{get;set;}

        @AuraEnabled
        public Decimal quantityBilled{get;set;}

        @AuraEnabled
        public Decimal adjustmentAmount{get;set;}

        @AuraEnabled
        public String transactionCode{get;set;}

        @AuraEnabled
        public String transactionDescription{get;set;}

        @AuraEnabled
        public String expenditureCategory{get;set;}

        @AuraEnabled
        public String programYear{get;set;}

        @AuraEnabled
        public String fundingSource{get;set;}

        @AuraEnabled
        public String paymentid{get;set;}

        @AuraEnabled
        public String receiptid{get;set;}

        @AuraEnabled
        public String eligibilityFlag{get;set;}

        @AuraEnabled
        public String categoryDes{get;set;}

        @AuraEnabled
        public String transCode{get;set;}

        @AuraEnabled
        public String expCode{get;set;}

        @AuraEnabled
        public String recordType{get;set;}
    }
}