@isTest
private class KinshipHistoryControllerTest {
    @TestSetup
    static void initData(){
        TriggerActivation__c ta = new TriggerActivation__c(Name = 'KinshipCaseTrigger', isActive__c = true);
        insert ta;
    }

    @isTest
    static void getListHistoryTest(){
        Contact c = new Contact();
        c.FirstName = 'FN';
        c.LastName = 'LN';
        insert c;
        Kinship_Case__c kc = new Kinship_Case__c();
        kc.Case_Code__c = 'Test FN';
        kc.ASAPS_Number__c = 'Test LN';
        kc.Name = 'Name';
        kc.Primary_Contact__c = c.id;
        insert kc;
        
        Test.startTest();
        List<KinshipHistoryController.HistoryWrapper> lstWrapper = KinshipHistoryController.getListHistory(kc.id);
        Test.stopTest();
        System.assertNotEquals(0, lstWrapper.size(), 'The Case History Wrapper list is equal 0');
    }
}