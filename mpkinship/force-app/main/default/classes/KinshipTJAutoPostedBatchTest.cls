@isTest
private class KinshipTJAutoPostedBatchTest {
    @TestSetup
    static void initData(){
        TriggerActivation__c ta = new TriggerActivation__c(Name = 'PaymentTrigger', isActive__c = true);
        insert ta;
        TriggerActivation__c  ta1 = new TriggerActivation__c();
        ta1.name='PurchaseOrder Trigger';
        ta1.isActive__c=true;
        insert ta1;
    }
    
    @isTest
    static void testAutoPostedTJ(){
        id recType = Schema.SObjectType.contact.getRecordTypeInfosByDeveloperName().get('Contacts').getRecordTypeId();
        contact con = new contact();
        con.recordtypeid=recType;
        con.LastName='contactrec';
        con.FirstName='test';
        con.Email='test@contact.com';        
        insert con;
        
        Kinship_Case__c caserec = new Kinship_Case__c();
        caserec.Primary_Contact__c=con.id;
        insert caserec;
        
        //id vendorrecType = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Vendor').getRecordTypeId();
        account accountrec = new account();
        //accountrec.recordtypeid=vendorrecType;
        accountrec.Name='test vendor';
        accountrec.Tax_ID__c='222-22-3333';
        accountrec.Entity_Type__c='Individual';
        insert accountrec;
        
        Locality_Account__c locality = new Locality_Account__c(Name = '12345');
        insert locality;
        
        Category__c category = new Category__c();
        category.Name='222';
        category.Locality_Account__c = locality.id;
        category.Category_Type__c='Adoption';
        category.RecordTypeId = Schema.SObjectType.Category__c.getRecordTypeInfosByName().get('LASER Category').getRecordTypeId();
        insert category;  
        
        FIPS__c fp = new FIPS__c();
        fp.name='test fips';
        insert fp;
        
        Campaign fy = new Campaign();
        fy.name = '2021';
        fy.StartDate = Date.Today().addDays(-10);
        fy.EndDate = Date.Today().addDays(10);
        fy.Fiscal_Year_Type__c = 'Local';
        fy.FIPS__c = fp.id;
        fy.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId();
        insert fy;
        
        Campaign parent = new Campaign();
        parent.name = '2021';
        parent.StartDate = Date.Today().addDays(-10);
        parent.EndDate = Date.Today().addDays(10);
        parent.Fiscal_Year_Type__c = 'DSS';
        parent.FIPS__c = fp.id;
        parent.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId();
        insert parent;
        recType = Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('LASER_Reporting_Period').getRecordTypeId();
        Campaign c = new Campaign();
        c.name = '2021';
        c.startdate=system.today().adddays(-3);
        c.EndDate=system.today().adddays(20);
        c.RecordTypeId=recType;
        c.ParentId = parent.id;
        c.FIPS__c = fp.id;
        insert c;
        list<TriggerActivation__c> talist=[select id,name,isActive__C from TriggerActivation__c where isActive__c=:true 
                                           and name=:'PaymentTrigger'];
        list<TriggerActivation__c> talist1=[select id,name,isActive__C from TriggerActivation__c where isActive__c=:true 
                                            and name=:'PurchaseOrder Trigger'];
        list<Kinship_Case__c> cases=[select id from Kinship_Case__c];
        list<account> accounts=[select id,recordtypeid from account where name='test vendor'];        
        list<Category__c> categories=[select id from Category__c];
        //insert a new product
        Product2 p = new product2(name='x',isactive=true, Unit_Measure__c='1');
        insert p;
        
        Product2 p2 = new product2(name='y',isactive=true, Unit_Measure__c='1');
        insert p2;
        
        //define the standart price for the product
        id stdPb = Test.getStandardPricebookId();
        insert new PricebookEntry(pricebook2id = stdPb, product2id = p.id,unitprice=1.0, isActive=true);
        
        Pricebook2 pb = new pricebook2(name='test',Vendor__c=accounts[0].id,IsActive=true,
                                       Effective_Date__c=system.today().addmonths(-2),Accounting_Period__c = fy.id);
        insert pb;
        
        PricebookEntry pbe = new PricebookEntry(pricebook2id=pb.id, product2id=p.id,unitprice=1.0, isActive=true);
        insert pbe; 
        id recTypeid = Schema.SObjectType.opportunity.getRecordTypeInfosByDeveloperName().get('Purchase_Order').getRecordTypeId();
        opportunity opprecord = new opportunity();
        opprecord.CloseDate=date.today();
        opprecord.StageName='Draft';
        opprecord.Name='test opportunity';
        opprecord.Kinship_Case__c=cases[0].id;
        opprecord.Amount=20000;
        opprecord.RecordTypeId=recTypeid;
        opprecord.AccountId=accounts[0].id;
        opprecord.Number_of_Months_Units__c=3;
        opprecord.Pricebook2Id =pb.id;        
        insert opprecord;
        //NOW CREATING LINE ITEMS
        opportunitylineitem opplineitem = new opportunitylineitem();
        opplineitem.Unit_Measure__c='2';
        opplineitem.Product2Id=p.id;
        opplineitem.PricebookEntryId=pbe.id;
        opplineitem.Quantity=2;
        opplineitem.Category__c=categories[0].id;
        opplineitem.OpportunityId=opprecord.id;
        opplineitem.TotalPrice = 2000;
        insert opplineitem;
        //Create an approval request for the po
        Test.startTest();
        Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitrequest();
        app.setObjectId(opprecord.Id);
        Approval.ProcessResult result = Approval.process(app);
        
        if(result.isSuccess()){
            opprecord.stagename='Approved';           
            update opprecord;
            
            list<Payment__c> payments=[select id,Written_Off__c,Paid__c,Payment_Date__c  from Payment__c
                                                 where Opportunity__c=:opprecord.Id];
            system.debug('**VAl of payments'+payments.size());
            payments[0].Paid__c=true;
            payments[0].Payment_Date__c = Date.today();
            payments[0].Accounting_Period__c = c.id;
            payments[0].FIPS_Code__c = fp.id;
            update payments[0];
        }
        
        KinshipTJAutoPostedBatch batch = new KinshipTJAutoPostedBatch();
        Database.executeBatch(batch, 100);
        String chron = '0 0 23 * * ?';
        System.schedule('test', chron, batch);
        
        List<Campaign> lstC = [SELECT id, Name, StartDate, EndDate, RecordTypeId, ParentId, Parent.ParentId, FIPS__c, Cate_Type__c
                               from Campaign Where RecordType.Name = 'LASER Reporting Period' Limit 1];
        TransactionJournalService.autoPostedTJ(lstC);
        Test.stopTest();
        List<Transaction_Journal__c> lstTJ = [SELECT id FROM Transaction_Journal__c];
        System.assertNotEquals(null, lstTJ, 'The Transaction_Journal__c insert does not equal 1');
    } 
    
    @isTest
    static void testAutoPostedTJ2(){
        id recType = Schema.SObjectType.contact.getRecordTypeInfosByDeveloperName().get('Contacts').getRecordTypeId();
        contact con = new contact();
        con.recordtypeid=recType;
        con.LastName='contactrec';
        con.FirstName='test';
        con.Email='test@contact.com';        
        insert con;
        
        Kinship_Case__c caserec = new Kinship_Case__c();
        caserec.Primary_Contact__c=con.id;
        insert caserec;
        
        account accountrec = new account();
        accountrec.Name='test vendor';
        accountrec.Tax_ID__c='222-22-3333';
        accountrec.Entity_Type__c='Individual';
        insert accountrec;
        
        Locality_Account__c locality = new Locality_Account__c(Name = '12345');
        insert locality;
        
        Category__c category = new Category__c();
        category.Name='222';
        category.Locality_Account__c = locality.id;
        category.Category_Type__c='Adoption';
        category.RecordTypeId = Schema.SObjectType.Category__c.getRecordTypeInfosByName().get('LASER Category').getRecordTypeId();
        insert category;   
        
        FIPS__c fp = new FIPS__c();
        fp.name='test fips';
        insert fp;
        
        Campaign fy = new Campaign();
        fy.name = '2021';
        fy.StartDate = Date.Today().addDays(-10);
        fy.EndDate = Date.Today().addDays(10);
        fy.Fiscal_Year_Type__c = 'Local';
        fy.FIPS__c = fp.id;
        fy.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId();
        insert fy;
        
        Campaign parent = new Campaign();
        parent.name = '2021';
        parent.StartDate = Date.Today().addDays(-10);
        parent.EndDate = Date.Today().addDays(10);
        parent.Fiscal_Year_Type__c = 'DSS';
        parent.FIPS__c = fp.id;
        parent.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId();
        insert parent;
        recType = Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('LASER_Reporting_Period').getRecordTypeId();
        Campaign c2 = new Campaign();
        c2.name = '2021';
        c2.startdate=system.today().adddays(-3);
        c2.EndDate=system.today().adddays(20);
        c2.RecordTypeId=recType;
        c2.ParentId = parent.id;
        c2.FIPS__c = fp.id;
        insert c2;
        
        Campaign c = new Campaign();
        c.name = 'LASER2021';
        c.startdate=system.today().adddays(-3);
        c.EndDate=system.today().adddays(20);
        c.RecordTypeId=recType;
        c.ParentId = parent.id;
        c.Next_LASER_AP__c = c2.id;
        c.FIPS__c = fp.id;
        insert c;
        // insert Staff & Operations
        
    	Opportunity opprecord = new opportunity();
        opprecord.CloseDate=date.today();
        opprecord.StageName='Open';
        opprecord.Name='test opportunity';
        opprecord.AccountId=accountrec.id;
        opprecord.Accounting_Period__c = c2.id;
        opprecord.Category__c = category.id;
        opprecord.RecordTypeId=Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Staff_Operations').getRecordTypeId();
        insert opprecord;
        
        opprecord = new opportunity();
        opprecord.CloseDate=date.today();
        opprecord.StageName='Open';
        opprecord.Name='test opportunity';
        opprecord.AccountId=accountrec.id;
        opprecord.Accounting_Period__c = c.id;
        opprecord.Category__c = category.id;
        opprecord.RecordTypeId=Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Staff_Operations').getRecordTypeId();
        insert opprecord;
        
        Kinship_Check__c kc = new Kinship_Check__c();
        insert kc;
   
        Payment__c payment = new Payment__c();
        payment.Opportunity__c=opprecord.id;
        payment.Payment_Amount__c=2000;
        payment.vendor__c = accountrec.Id;
        payment.Accounting_Period__c = c.id;
        payment.RecordTypeId=Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Staff_Operations').getRecordTypeId();
        payment.Payment_Method__c = 'Check';
        payment.Kinship_Check__c = kc.id;
        payment.Payment_Date__c = Date.Today();
        payment.Category_lookup__c = category.id;
        payment.FIPS_Code__c = fp.id;
        insert payment;
        
        payment = new Payment__c();
        payment.Opportunity__c=opprecord.id;
        payment.Payment_Amount__c=2000;
        payment.vendor__c = accountrec.Id;
        payment.Accounting_Period__c = c.id;
        payment.RecordTypeId=Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Staff_Operations').getRecordTypeId();
        payment.Payment_Method__c = 'Check';
        payment.Payment_Date__c = Date.Today();
        payment.Category_lookup__c = category.id;
        payment.FIPS_Code__c = fp.id;
        insert payment;
        Test.startTest();
        
        KinshipTJAutoPostedBatch batch = new KinshipTJAutoPostedBatch();
        Database.executeBatch(batch, 100);
        String chron = '0 0 23 * * ?';
        System.schedule('test', chron, batch);
        
        List<Campaign> lstC = [SELECT id, Name, StartDate, EndDate, RecordTypeId, ParentId, Parent.ParentId , FIPS__c, Cate_Type__c
                               from Campaign Where RecordType.Name = 'LASER Reporting Period' and Next_LASER_AP__c <> '' Limit 1];
        system.debug('--Test---' + lstC);
        TransactionJournalService.autoPostedTJ(lstC);
        batch.execute(null, new List<Campaign>());
        Test.stopTest();
        List<Transaction_Journal__c> lstTJ = [SELECT id FROM Transaction_Journal__c];
        System.assertNotEquals(null, lstTJ, 'The Transaction_Journal__c insert does not equal 1');
    } 
}