Public class KinshipCaseTriggerHelper{
    //public static boolean isRun=false;
    //public static boolean isSend=false;
    public static boolean isTrgRun=false;
    public static boolean isIVErun=false;
    
    // KIN 1431
    public static void claimsnSWAUpdate(set<id> caserecids){
        List<Claims__c> claimstoUpdate= new List<Claims__c>();
        List<Special_Welfare_Account__c> SWAtoUpdate= new List<Special_Welfare_Account__c>();
        list<Kinship_Case__c> cases=[select id,name,(Select id,name,Name__c from Claims__r),(Select id,name, SWA_Name__c from Special_Welfare_Associations__r) from Kinship_Case__c where id in:caserecids];
         for(Kinship_Case__c caserec : cases){
             
                for(Claims__c clm : caserec.Claims__r){
                    if(clm.Name__c!=null && caserec.Name!=null  ){
                        clm.name=clm.Name__c + ' '+caserec.Name ;
                    	claimstoUpdate.add(clm);
                    }
                    
                }
                 for(Special_Welfare_Account__c swa:caserec.Special_Welfare_Associations__r){
                     if(swa.SWA_Name__c!=null && caserec.Name!=null ){
                        swa.name=swa.SWA_Name__c + ' '+caserec.Name ;
                    	SWAtoUpdate.add(swa);
                    }
                 }
             
         }
        if(claimstoUpdate.size()>0)
        update claimstoUpdate;
        if(SWAtoUpdate.size()>0)
        update SWAtoUpdate;
    }
    //CHECK FOR TITLE IVE ELIGIBILITY CHANGE ATTACHED TO CA OR PO
  /*  public static void createNotification(set<id> caserecids){
        isIVErun=true;
        map<id,Kinship_Case__c> caseMap = new map<id,Kinship_Case__c>();
        set<id> opportunityIds = new set<id>();
        list<Kinship_Case__c> cases=[select id,Title_IVE_Eligibility__c,Primary_Contact__r.name,OwnerId,(Select id,Kinship_Case__c from Purchase_Orders__r where
                         stagename!='closed'),(select id,Case__c,Category__c,Category__r.Category_Type__c  from CaseActions__r where npsp__Status__c!='Closed')
                                    from Kinship_Case__c where id in:caserecids];
        
        set<id> casesids = new set<id>();
        for(Kinship_Case__c caserec : cases){
            caseMap.put(caserec.id,caserec);
            for(opportunity opp : caserec.Purchase_Orders__r){
                opportunityIds.add(opp.id);    
            }
            for(npe03__Recurring_Donation__c ca : caserec.CaseActions__r){
                if(ca.Category__r.Category_Type__c=='CSA' || ca.Category__r.Category_Type__c=='IVE'){
                    casesids.add(caserec.id);
                    break;
                }    
            }
        } 
        
       
        list<opportunity> opplist=[select id,Kinship_Case__c,(Select id,category__c,category__r.Category_Type__c from opportunitylineitems where category__c!=null) from
                            opportunity where id in: opportunityIds];                        
        for(opportunity opp : opplist){
            for(opportunitylineitem oppitem : opp.opportunitylineitems){
                if(oppitem.category__r.Category_Type__c=='CSA' ||  oppitem.category__r.Category_Type__c=='IVE'){
                    casesids.add(opp.Kinship_Case__c);
                    break;
                }
            }
        }  
        
        list<Task> taskslist = new list<Task>();
        for(id caseid : casesids){
            if(caseMap.containskey(caseid)){
                task taskrec = new task();
                taskrec.whatid= caseid;
                taskrec.OwnerId = caseMap.get(caseid).ownerid;
                taskrec.subject = 'Title IV E eligibility has changed for case -'+casemap.get(caseid).Primary_Contact__r.name;
                taskrec.Priority='High';
                taskrec.IsReminderSet=true;
                taskrec.Description='This case is related to PO or Case action where CSA or IVE categories has been used';
                taskrec.ActivityDate=date.today();
                taskrec.ReminderDateTime=DateTime.now().adddays(1);
                taskslist.add(taskrec);
            }
        }
        
        if(taskslist.size()>0)
            insert taskslist;                           
    } */
    
    public static void populateChildIdentifier(list<Kinship_Case__c> casesTobeUpdated){
        list<Kinship_Case__c > casesUpdatedlist = new list<Kinship_Case__c>();
        isTrgRun=true;
        for(Kinship_Case__c caserec : casesTobeUpdated){
            Kinship_Case__c caseupdate = new Kinship_Case__c();
            caseupdate.id = caserec.id;
            caseupdate.Locality_Child_Provider__c =KinshipCaseTriggerHelper.getRandomNumber();
            casesUpdatedlist.add(caseupdate);  
        }
        if(casesUpdatedlist.size()>0)
            update casesUpdatedlist;
    }
    
    public static string getRandomNumber(){
        string result = '';
        integer length =15;
        while(result.length() < length){
             blob privateKey = crypto.generateAesKey(256);
             string randomString = EncodingUtil.base64Encode(crypto.generateMac('hmacSHA512',privateKey,privateKey));
             result += randomString.replaceAll('[^0-9]','');
        }
        result = result.substring(0,length);
        return result;
    }
    
  
    
}