public class ReportOfCollectionController {

    public id collId;
    public Decimal cash{get; set;}
    public Decimal MO{get; set;}
    public Decimal check{get; set;}
    public Decimal EFT   {get; set;}
    public Decimal checkSWR{get; set;}
    public Decimal TotalAmount{get; set;}
    
    public Decimal payTotal {get; set;}
    public Decimal TotalLocalityCode {get; set;}
    
    public Decimal TotalAdoption            {get; set;}  
    public Decimal TotalAdultServices       {get; set;}  
    public Decimal TotalBenefits            {get; set;} 
    public Decimal TotalChildProtection     {get; set;}  
    public Decimal TotalEmploymentServices  {get; set;}
    public Decimal TotalCSA                 {get; set;}     
    public Decimal TotalIVE                 {get; set;}     
    public Decimal TotalGeneralCases        {get; set;}  
    public Decimal TotalAdministrative      {get; set;} 
    public Decimal TotalPOSASSIST           {get; set;} 
    
    public Decimal TotalSpecialWelfare      {get; set;}
    public Decimal TotalSpecialWelfareR      {get; set;} 
    public Decimal TotalCompanionPayroll    {get; set;}
    public Decimal TotalClaimsPayable       {get; set;}
    public List<PaymentWrapper> receiptsList{get; set;}
    PaymentWrapper receipts =  new PaymentWrapper();
    PaymentWrapper receipt2 =  new PaymentWrapper();
    public List<Payment__c> receiptsLists{get; set;}
    public Map<string, decimal> ReceiptTypeMap  {get; set;}
    public Map<string, decimal> ReceiptTypeMapSWR  {get; set;}
    public Map<string, decimal> LocalityCodeMap  {get; set;}
    public Map<string, decimal> LocalityCodeMapSWR  {get; set;}
    public id invoiceRT = Schema.SObjectType.Payment__c.getRecordTypeInfosByName().get('Receipt').getRecordTypeId();
    public ReportOfCollectionController(){}
    
    
    public Decimal TotalAllSWA {get; set;}
     public Decimal TotalNOTSWA {get; set;}
    public ReportOfCollectionController(ApexPages.StandardController stdController) {
        cash           = 0.00;
        check          = 0.00;
        MO             = 0.00;
        EFT            = 0.00;
        checkSWR       = 0.00;
        TotalAmount    = 0.00;
           
        TotalAdoption       = 0.00;  
        TotalAdultServices  = 0.00;  
        TotalBenefits       = 0.00; 
        TotalChildProtection    = 0.00;  
        TotalEmploymentServices = 0.00; 
        TotalCSA        = 0.00;     
        TotalIVE        = 0.00;     
        TotalGeneralCases   = 0.00;  
        TotalAdministrative = 0.00; 
        TotalPOSASSIST = 0.00;
        TotalSpecialWelfare = 0.00;
        TotalSpecialWelfareR = 0.00;
        TotalCompanionPayroll   = 0.00; 
        TotalClaimsPayable = 0.0;
        TotalAllSWA = 0.00;
        TotalNOTSWA = 0.00;
        
        payTotal = 0.00;
        TotalLocalityCode = 0.00;
        this.collId = ApexPages.currentPage().getParameters().get('id');
        try{  
            ReceiptTypeMap = new Map<string, decimal> ();
            ReceiptTypeMapSWR = new Map<string, decimal> ();
            LocalityCodeMap  = new Map<string, decimal> ();
            LocalityCodeMapSWR  = new Map<string, decimal> ();
            receiptsLists = [select id,
                                    name,
                                    Report_Number__c,
                                    Payment_Date__c,
                                    Kinship_Case__r.Name,
                                    Kinship_Case__r.Case_No__c,
                                    Payee__c,
                                    Description__c,
                                    Receipt_Type__c,
                                    Payment_Method__c,
                                    Payment_Amount__c,
                                    Locality_Account_Codes__c,
                                    Special_Welfare_Account__r.Name,
                                    Kinship_Case__r.VACMS_Number__c,
                                    Category_lookup__r.Locality_Account__r.Name,
                                    RecordType.Name
                            from Payment__c
                        where Opportunity__c =: collId AND 
                            (Payment__c.RecordType.Name = 'Receipt' OR Payment__c.RecordType.Name = 'Vendor Refund' OR Payment__c.RecordType.Name = 'Special Welfare Reimbursement')
                        order by Payment_Date__c];
           /* receiptsLists = [select id,
                                    name,
                                    Report_Number__c,
                                    Payment_Date__c,
                                    Kinship_Case__r.Name,
                                    Kinship_Case__r.Case_No__c,
                                    Payee__c,
                                    Description__c,
                                    Receipt_Type__c,
                                    Pay_Method__c,
                                    Payment_Amount__c,
                                    Locality_Account_Codes__c,
                                    Special_Welfare_Account__r.Name,
                                    Kinship_Case__r.VACMS_Number__c,
                                    Category_lookup__r.Locality_Account__r.Name,
                                    RecordType.Name
                            from Payment__c
                        where Opportunity__c =: collId AND 
                            (Payment__c.RecordType.Name = 'Receipt' OR Payment__c.RecordType.Name = 'Vendor Refund' OR Payment__c.RecordType.Name = 'Special Welfare Reimbursement')
                        order by Payment_Date__c]; */
            receiptsList = new List<PaymentWrapper>();    
            for(Payment__c rc: receiptsLists){
                if(rc.id != null){
                    receipts.id = rc.id;
                    receipt2.id = rc.id;
                }
                if(rc.name != null && rc.name != ''){
                    receipts.name = rc.name;
                    receipt2.name = rc.name;
                }
                if(rc.Report_Number__c != null && rc.Report_Number__c != ''){
                    receipts.ReportNumber = rc.Report_Number__c;
                    receipt2.ReportNumber = rc.Report_Number__c;
                }
               /* if(rc.Pay_Method__c != null && rc.Pay_Method__c != ''){
                    receipts.PayMethod = rc.Pay_Method__c;
                    receipt2.PayMethod = rc.Pay_Method__c;
                } */
                if(rc.Payment_Method__c!= null && rc.Payment_Method__c!= ''){
                    receipts.PayMethod = rc.Payment_Method__c;
                    receipt2.PayMethod = rc.Payment_Method__c;
                }
                if(rc.Payee__c != null && rc.Payee__c != ''){
                    receipts.Payee = rc.Payee__c;
                    receipt2.Payee = rc.Payee__c;
                }
                if(rc.Description__c != null && rc.Description__c != ''){
                    receipts.Description = rc.Description__c;
                    receipt2.Description = rc.Description__c;
                }
                if(rc.Payment_Amount__c != null){
                    receipts.npe01PaymentAmount = rc.Payment_Amount__c;
                    receipt2.npe01PaymentAmount = rc.Payment_Amount__c;
                }
                if(rc.Locality_Account_Codes__c != null && rc.Locality_Account_Codes__c != ''){
                    receipts.LocalityAccountCodes = rc.Locality_Account_Codes__c;
                    receipt2.LocalityAccountCodes = rc.Locality_Account_Codes__c;
                }
                if(rc.Special_Welfare_Account__r.Name != null && rc.Special_Welfare_Account__r.Name != ''){
                    receipts.SpecialWelfareAccountName = rc.Special_Welfare_Account__r.Name;
                    receipt2.SpecialWelfareAccountName = rc.Special_Welfare_Account__r.Name;
                }
                if(rc.Kinship_Case__r.VACMS_Number__c != null && rc.Kinship_Case__r.VACMS_Number__c != ''){
                    receipts.KinshipCaseVACMSNumber = rc.Kinship_Case__r.VACMS_Number__c;
                    receipt2.KinshipCaseVACMSNumber = rc.Kinship_Case__r.VACMS_Number__c;
                }
                if(rc.Category_lookup__r.Locality_Account__r.Name != null && rc.Category_lookup__r.Locality_Account__r.Name != ''){
                    receipts.CategorylookupLocalityAccountName = rc.Category_lookup__r.Locality_Account__r.Name;
                    receipt2.CategorylookupLocalityAccountName = rc.Category_lookup__r.Locality_Account__r.Name;
                }
                if(rc.RecordType.Name != null && rc.RecordType.Name != ''){
                    receipts.RecordTypeName = rc.RecordType.Name;
                    receipt2.RecordTypeName = rc.RecordType.Name;
                }
                if(rc.Payment_Date__c != null){
                    receipts.npe01PaymentDate = rc.Payment_Date__c;
                    receipt2.npe01PaymentDate = rc.Payment_Date__c;
                }
                if(rc.Receipt_Type__c != null){
                    receipts.ReceiptType = rc.Receipt_Type__c;
                    receipt2.ReceiptType = rc.Receipt_Type__c;
                }
                receipts.checkRT = false;
                receipt2.checkRT = true;
                if(rc.RecordType.Name =='Special Welfare Reimbursement'){ 
                    List<String> swaSplit;
                    if(rc.Special_Welfare_Account__r.Name != null){
                        swaSplit = rc.Special_Welfare_Account__r.Name.split(' ',2);
                    }
                    if(rc.Kinship_Case__r.Name != null && rc.Kinship_Case__r.Name != ''){
                        receipts.KinshipCaseCaseNo = rc.Kinship_Case__r.Name;
                        receipt2.KinshipCaseCaseNo = swaSplit[0];
                    }
                    system.debug('Case Name>>>>>>>>>>>>>>>>>' + rc.Kinship_Case__r.Name);
                    if(rc.Kinship_Case__r.Case_No__c != null && rc.Kinship_Case__r.Case_No__c != ''){
                        receipts.KinshipCaseName = rc.Kinship_Case__r.Case_No__c;
                        receipt2.KinshipCaseName = swaSplit[1];
                    }
                    receiptsList.add(receipt2);
                    receiptsList.add(receipts);
                }
                else{
                    List<String> swaSplit;
                    if(rc.Special_Welfare_Account__r.Name != null){
                        swaSplit = rc.Special_Welfare_Account__r.Name.split(' ',2);
                    }
                    system.debug('Case Name>>>>>>>>>>>>>>>>>' + rc.Kinship_Case__r.Name);
                    if(rc.Kinship_Case__r.Name != null && rc.Kinship_Case__r.Name != ''){
                        receipts.KinshipCaseCaseNo = rc.Kinship_Case__r.Name;
                        system.debug('receipts.KinshipCaseCaseNo>>>>>>>>>>>>>>>>>' + receipts.KinshipCaseCaseNo);
                       

                    }
                    system.debug('Case Name>>>>>>>>>>>>>>>>>' + rc.Kinship_Case__r.Name);
                    if(rc.Kinship_Case__r.Case_No__c != null && rc.Kinship_Case__r.Case_No__c != ''){
                        receipts.KinshipCaseName = rc.Kinship_Case__r.Case_No__c;
                       
                    }
                  
                    receiptsList.add(receipts);
                }
            
                receipts = new PaymentWrapper();
                receipt2 = new PaymentWrapper();
            }        
            
            
            for(PaymentWrapper rc: receiptsList) {
                
                if(rc.CategorylookupLocalityAccountName!=null){
                    Decimal amt;
                    Decimal amtR;
                    if(rc.checkRT == true){
                        amt = LocalityCodeMap.get(rc.CategorylookupLocalityAccountName);
                        amtR = LocalityCodeMapSWR.get(rc.CategorylookupLocalityAccountName+'(SWR)');
                    }
                    else{
                        amt = LocalityCodeMap.get(rc.CategorylookupLocalityAccountName);
                    }
                    if(amt == null){
                        amt = 0;    
                    }
                    if(amtR == null){
                        amtR = 0;    
                    }
                    if(rc.npe01PaymentAmount != null && rc.checkRT == false){
                        amt += rc.npe01PaymentAmount;
                        LocalityCodeMap.put(rc.CategorylookupLocalityAccountName,  amt);
                        TotalLocalityCode += rc.npe01PaymentAmount;
                    }
                    if(rc.npe01PaymentAmount != null && rc.checkRT == true){
                        system.debug('-amtR>>>>>>><'+ amtR);
                        amtR -= rc.npe01PaymentAmount;
                        LocalityCodeMapSWR.put(rc.CategorylookupLocalityAccountName+'(SWR)',  amtR);
                        TotalLocalityCode -= rc.npe01PaymentAmount;
                    }
                }//system.debug('rc.npe01PaymentAmount???' + rc.npe01PaymentAmount);
                system.debug('rc.PayMethod???' + rc.PayMethod);
                system.debug('rc.checkRT' + rc.checkRT);
                if(rc.PayMethod == 'Cash' && rc.checkRT == false) {
                    if(rc.npe01PaymentAmount != null){
                    cash += rc.npe01PaymentAmount;
                    }
                }else if(rc.PayMethod == 'Check' && rc.checkRT == false) {if(rc.npe01PaymentAmount != null) check += rc.npe01PaymentAmount;
                }else if(rc.PayMethod == 'Money Order' && rc.checkRT == false) {if(rc.npe01PaymentAmount != null) MO += rc.npe01PaymentAmount;
                }else if(rc.PayMethod == 'EFT' && rc.checkRT == false) {if(rc.npe01PaymentAmount != null) EFT += rc.npe01PaymentAmount;
                }else if(rc.PayMethod == 'Check' && rc.checkRT == true) {if(rc.npe01PaymentAmount != null) checkSWR -= rc.npe01PaymentAmount;
                }
                payTotal = cash + check + MO + EFT + checkSWR; 
                //System.debug('rc.ReceiptType'+rc.ReceiptType);
                //System.debug('rc.ReceiptType'+rc.checkRT);
                if(rc.ReceiptType== 'Adoption' && rc.checkRT == false) {
                    TotalAdoption   += rc.npe01PaymentAmount;
                }else if(rc.ReceiptType== 'Adult Services' && rc.checkRT == false) {
                    TotalAdultServices   += rc.npe01PaymentAmount;
                }else if(rc.ReceiptType== 'Benefits' && rc.checkRT == false) {TotalBenefits += rc.npe01PaymentAmount;
                }else if(rc.ReceiptType== 'Child Protection' && rc.checkRT == false) { TotalChildProtection += rc.npe01PaymentAmount;
                }else if(rc.ReceiptType== 'Employment Services' && rc.checkRT == false) { TotalEmploymentServices += rc.npe01PaymentAmount;
                }else if(rc.ReceiptType== 'CSA' && rc.checkRT == false) { TotalCSA += rc.npe01PaymentAmount;
                }else if(rc.ReceiptType== 'IVE' && rc.checkRT == false) { TotalIVE += rc.npe01PaymentAmount;
                }else if(rc.ReceiptType== 'General Cases' && rc.checkRT == false) { TotalGeneralCases += rc.npe01PaymentAmount;
                }else if(rc.ReceiptType== 'Administrative' && rc.checkRT == false) { TotalAdministrative += rc.npe01PaymentAmount;
                }else if(rc.ReceiptType== 'POS/ASSIST' && rc.checkRT == false) { TotalPOSASSIST += rc.npe01PaymentAmount;
                }else if(rc.ReceiptType== 'Claims Payable' && rc.checkRT == false){
                        TotalClaimsPayable  += rc.npe01PaymentAmount;                                    
                }else if((rc.ReceiptType== 'Special Welfare' || rc.ReceiptType== 'Local Case - SWF') && rc.checkRT == false) 
                { 
                    TotalSpecialWelfare += rc.npe01PaymentAmount;
                }else if(rc.ReceiptType== 'Companion Payroll' && rc.checkRT == false) { TotalCompanionPayroll += rc.npe01PaymentAmount;
                }
                    
                if(rc.ReceiptType <> 'Special Welfare'  && rc.ReceiptType<> 'Local Case - SWF'&& rc.checkRT == false && rc.ReceiptType != 'Claims Payable'){
                    if(rc.npe01PaymentAmount != null){
                        TotalNOTSWA += rc.npe01PaymentAmount; 
                    } 
                }
                if(rc.npe01PaymentAmount != null && rc.checkRT == false){
                    TotalAmount += rc.npe01PaymentAmount; 
                }
                if(rc.npe01PaymentAmount != null && rc.checkRT == true){
                    TotalAmount -= rc.npe01PaymentAmount; 
                }
                if(rc.npe01PaymentAmount != null && rc.RecordTypeName =='Special Welfare Reimbursement' && rc.checkRT == true){
                    //TotalSpecialWelfare -= rc.npe01PaymentAmount;
                    TotalSpecialWelfareR -= rc.npe01PaymentAmount;
                }
            }
            

            if(TotalAdoption > 0) {ReceiptTypeMap.put( 'Adoption', TotalAdoption );}
            if(TotalAdultServices > 0) { ReceiptTypeMap.put( 'Adult Services' , TotalAdultServices );}
            if(TotalBenefits > 0) {ReceiptTypeMap.put( 'Benefits' ,  TotalBenefits );}
            if(TotalChildProtection > 0){ReceiptTypeMap.put('Child Protection' ,  TotalChildProtection);}
            if(TotalEmploymentServices > 0){ReceiptTypeMap.put('Employment Services' ,  TotalEmploymentServices);}
            if(TotalCSA > 0){ReceiptTypeMap.put( 'CSA' ,  TotalCSA);}
            if(TotalIVE > 0){ReceiptTypeMap.put('IVE' , TotalIVE );}
            if(TotalGeneralCases > 0){ReceiptTypeMap.put( 'General Cases' ,  TotalGeneralCases);}
            if(TotalAdministrative > 0){ReceiptTypeMap.put( 'Administrative' ,  TotalAdministrative);}
            if(TotalPOSASSIST > 0){ReceiptTypeMap.put( 'POS/ASSIST' ,  TotalPOSASSIST);}
            if(TotalSpecialWelfare > 0){ReceiptTypeMap.put( 'Special Welfare' ,  TotalSpecialWelfare);}
            if(TotalCompanionPayroll > 0){ReceiptTypeMap.put( 'Companion Payroll' , TotalCompanionPayroll );}
            if(TotalClaimsPayable   > 0){ ReceiptTypeMap.put( 'Claims Payable' , TotalClaimsPayable ); }
            if(TotalSpecialWelfareR < 0){ReceiptTypeMapSWR.put( 'SWR' ,  TotalSpecialWelfareR);}
            
            TotalAllSWA = TotalSpecialWelfare + TotalSpecialWelfareR + TotalClaimsPayable ;
           
            
        }catch(Exception e){
            system.debug('***Exception e'+e);
        }
    }
    public class PaymentWrapper{
        @auraEnabled
        public string id {get;set;}
        @auraEnabled
        public string name {get;set;}
        @auraEnabled
        public string ReportNumber {get;set;}
        @auraEnabled
        public string KinshipCaseName {get;set;}
        @auraEnabled
        public string KinshipCaseCaseNo {get;set;}
        @auraEnabled
        public string Payee {get;set;}
        @auraEnabled
        public string Description {get;set;}
        @auraEnabled
        public string ReceiptType {get;set;}
        @auraEnabled
        public string PayMethod {get;set;}
        @auraEnabled
        public Decimal npe01PaymentAmount {get;set;}
        @auraEnabled
        public string LocalityAccountCodes {get;set;}
        @auraEnabled
        public string SpecialWelfareAccountName {get;set;}
        @auraEnabled
        public string KinshipCaseVACMSNumber {get;set;}
        @auraEnabled
        public string CategorylookupLocalityAccountName {get;set;}
        @auraEnabled
        public string RecordTypeName {get;set;}
        @auraEnabled
        public Date npe01PaymentDate {get;set;}
        @auraEnabled
        public Boolean checkRT {get;set;}
    }
}