public with sharing class SWRController {
    public SWRController() {

    }

    @AuraEnabled(continuation = true)
    public static List<Payment__c> getVendorInvoiceByFilter(
        String accountingPeriod, 
        String endDate,
        String startDate,
        String recipient
        
    ){
        Id devRecordTypeId = Schema.SObjectType.Payment__c.getRecordTypeInfosByName().get('Vendor Invoice').getRecordTypeId();
        String whereClause = ' Id <> null ';

        if(!String.isBlank(accountingPeriod)){
            whereClause += ' AND Accounting_Period__c =:accountingPeriod ';
        } 
            
        if(!String.isBlank(startDate)){
            Date tempStart = Date.valueOf(startDate);
            whereClause += ' AND Payment_Date__c >= :tempStart ';
        }
            
        if(!String.isBlank(endDate)){
            Date tempEnd = Date.valueOf(endDate);
            whereClause += ' AND Payment_Date__c <= :tempEnd ';
        }
        if(!String.isBlank(recipient)){
            whereClause += ' AND Kinship_Case__c = :recipient ';
        }
        
        try {
            return Database.query('SELECT Id, Vendor__r.Name,Payment_Source__c, CreatedDate, Kinship_Case__c,Kinship_Case__r.Title_IVE_Eligibility__c, Check_Number__c, Payment_Date__c, Balance_Remaining__c, Category_Name__c, Category_lookup__c, Payment_Amount__c, Refund_Amount__c, Prior_Refund1__c,Category_lookup__r.Category_Type__c FROM Payment__c WHERE ' + whereClause + ' AND RecordTypeId =: devRecordTypeId AND Invoice_Stage__c != \'Cancelled\' AND Category_lookup__r.Category_Type__c = \'CSA\' And  Paid__c = true AND Kinship_Check__r.Status__c <> \'Cancelled\' order by CreatedDate desc ');
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
        
    }
    @AuraEnabled(continuation=true)
    public static Special_Welfare_Account__c getSWACaseId(String SWAID){
        try {
            return [Select Case_KNP__c,Case_KNP__r.Title_IVE_Eligibility__c, Current_Balance__c,Child_Support_Funds_Remaining__c,SSA_SSI_Funds_Remaining__c from Special_Welfare_Account__c where id =:SWAID Limit 1];
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled(continuation=true)
    public static String createReimbursements(List<Payment__c> reimbursements){
        System.debug('reimbursements'+reimbursements);
        List<Payment__c> Payments = new List<Payment__c>();
        Payment__c payment = new Payment__c();
        try {
            for(Payment__c pymnt: reimbursements){
                System.debug('pymnt'+pymnt.Id);
                System.debug('pymnt.Refund_Amount__c;'+pymnt.Refund_Amount__c);
                System.debug('pymnt.Prior_Refund1__c;'+pymnt.Prior_Refund1__c);
                if(pymnt.Prior_Refund1__c == null){
                    payment.Prior_Refund1__c = 0;
                }
                payment.Prior_Refund1__c = pymnt.Prior_Refund1__c + pymnt.Payment_Amount__c;
                payment.Refund_Amount__c = null;
                payment.Id = pymnt.Payment__c;

                Payments.add(payment);
                payment = new Payment__c();
            }
            insert reimbursements;
            TransactionJournalService.byPassCheckPosted = true;
            System.debug('PaymentsPayments'+Payments);
            update Payments;
            TransactionJournalService.byPassCheckPosted = false;
            list<Id> paymentIds = new list<Id>();
            for(Payment__c pymnt: reimbursements){
                paymentIds.add(pymnt.id);
            }
            
            MultiSelectLookupController.SetPaymentCategoryAndFIPS(paymentIds);
            return 'Records are created.';
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    
}