@isTest
public class POServicesComponentCtrTest {
    @testSetup
    static void testecords(){
        Locality_Account__c la = new Locality_Account__c();
        la.Name='222';
        insert la;
    	 Category__c category = new Category__c();
        category.Name='222';
        category.Locality_Account__c=la.id;
        category.Category_Type__c='CSA';
         category.recordtypeid=Schema.SObjectType.category__C.getRecordTypeInfosByDeveloperName().get('LEDRS_Category').getRecordTypeId();
        insert category;    
    }    
    public static testmethod void test1(){
        
         list<Category__c> categories=[select id,category_Type__C,Locality_Account__c from Category__c];
        Mandate_Type__c mt = new Mandate_Type__c();
        mt.Name='test mandate';
        mt.Mandate_Code__c='2';
        insert mt;
        Mandate_Type_And_Category__c mc = new Mandate_Type_And_Category__c();
        mc.Category__c=categories[0].id;
        mc.Mandate_Type__c=mt.id;
        insert mc;
         Service_Placement_Types__c sc = new Service_Placement_Types__c();
        sc.Name='test spt';
        sc.Code__c='1';
        insert sc;
        Service_Names_Code__c sn = new Service_Names_Code__c();
        sn.Name='test sn';
        sn.Service_Name_Code__c='3';
        insert sn;
        Service_Name_and_Service_Type__c stn = new Service_Name_and_Service_Type__c();
        stn.Service_Names_Code__c=sn.id;
        stn.Service_Placement_Type__c=sc.id;
        insert stn;
        CategoryAndSPT__c cs = new CategoryAndSPT__c();
        cs.Category__c=categories[0].id;
        cs.Service_Placement_Type__c=sc.id;
        insert cs;
        fips__C fip = new fips__C();
        fip.name='test fip';
        fip.FIPS_Code__c='1';
        insert fip;
        Cost_Center__c cc = new Cost_Center__c();
        cc.name='CSA COSTCENTER';
        insert cc;
        Chart_of_Account__c ca = new Chart_of_Account__c();
        ca.Cost_Center__c=cc.id;
        ca.FIPS__c=fip.id;
        ca.Locality_Account__c=categories[0].Locality_Account__c;
        POServicesComponentCtr.getCategoryandSPT(categories[0].id);
        POServicesComponentCtr.getCategoryandSPT(categories[0].id);
        POServicesComponentCtr.getSPTNames('1');
        POServicesComponentCtr.getUnitMeasure();
        poservicescomponentctr.getCostCenters(categories[0].id,fip.id);
        poservicescomponentctr.createNoteRecord(new ContentNote(title='test'), cc.Id);
        poservicescomponentctr.getCaseAge(cc.Id);
        poservicescomponentctr.getSNCDesc(sn.Id);
    }
}