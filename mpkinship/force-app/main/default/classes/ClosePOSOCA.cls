public class ClosePOSOCA{

    @auraEnabled
    public static OpportunityWrap FetchInitialValues(string recordid){
        list<opportunity> opplist=[select id,End_Date__c,Action_Selected__c from opportunity where id=:recordid];
        if(opplist.size()>0){
            OpportunityWrap owrap = new OpportunityWrap();
            owrap.opp = opplist[0];
            if(opplist[0].Action_Selected__c !=null && opplist[0].Action_Selected__c=='Await Final Invoice'){
                Id vendorRecordTypeId= Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Vendor_Invoice').getRecordTypeId(); 
                list<Payment__c> payments=[select id,name,Service_Begin_Date__c,Service_End_Date__c,Amount_Paid__c,Scheduled_Date__c,
                           Payment_Amount__c,Category_lookup__c,Category_lookup__r.name,Category_Name__c from Payment__c where Opportunity__c=:recordid
                           and Paid__c=:false and Written_Off__c=:false and recordtypeid=:vendorRecordTypeId];
                if(payments.size()>0)
                    owrap.invoices= payments;  
                          
            }
            return owrap;   
        }
           
            
        return null;          
    }
    
    @auraEnabled
    public static list<Payment__c> FetchInvoices(string recordid){
        Id vendorRecordTypeId= Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Vendor_Invoice').getRecordTypeId(); 
        list<Payment__c> payments=[select id,name,Service_Begin_Date__c,Service_End_Date__c,Amount_Paid__c,Scheduled_Date__c,
                   Payment_Amount__c,Category_lookup__c,Category_lookup__r.name,Category_Name__c from Payment__c where Opportunity__c=:recordid
                   and Paid__c=:false and Written_Off__c=:false and recordtypeid=:vendorRecordTypeId];
        if(payments.size()>0)
            return payments;
                
        return null;
    }
    
    @auraEnabled
    public static SaveWrapper SaveRecords(string recordid, string picklistval, string EndDate, string invoiceIds){
        system.debug('***enterd in saverecords'+EndDate);
        list<string> selectedInvoiceIds= (list<string>)JSON.deserialize(invoiceIds,list<String>.class);
        system.debug('***val of selectedinvoiceids'+selectedInvoiceIds);
         SaveWrapper saw = new SaveWrapper(); 
        opportunity opp = new opportunity();
        opp.id=recordid;
        if(picklistval!=null)
            opp.Action_Selected__c=picklistval;
         
          saw.picklistval=picklistval;
            
         list<Payment__c> updatedPayments = new list<Payment__c>();       
        if(picklistval!=null && picklistval=='Close Immediately'){
             Id vendorRecordTypeId= Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Vendor_Invoice').getRecordTypeId(); 
            list<Payment__c> payments=[select id,Written_Off__c,recordtypeid,Invoice_Stage__c,Opportunity__c from Payment__c where recordtypeid=:vendorRecordTypeId and Paid__c=:false and
                            Written_Off__c=:false and Opportunity__c=:recordid];
                        
            for( Payment__c payment : payments){
                Payment__c paymentrec = new Payment__c();
                paymentrec.id = payment.id;
                paymentrec.Written_Off__c=true;
                paymentrec.Invoice_Stage__c='Write off';
                updatedPayments.add(paymentrec);
                
            }    
            
            if(updatedPayments.size()>0)
                update updatedPayments;
            else if(updatedPayments.size()==0)
                opp.stagename='Closed';        
              
        }
        else if(picklistval!=null && picklistval=='Await Final Invoice'){
            if(selectedInvoiceIds.size()>0){
                 Id vendorRecordTypeId= Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Vendor_Invoice').getRecordTypeId(); 
                 list<Payment__c> payments=[select id,Written_Off__c,recordtypeid,Invoice_Stage__c,Opportunity__c from Payment__c where recordtypeid=:vendorRecordTypeId and Paid__c=:false and
                            Written_Off__c=:false and id not in: selectedInvoiceIds and Opportunity__c=:recordid];  
                             
                for(Payment__c payment : payments){
                    Payment__c paymentrec = new Payment__c();
                    paymentrec.id = payment.id;
                    paymentrec.Written_Off__c=true;
                    paymentrec.Invoice_Stage__c='Write off';
                    updatedPayments.add(paymentrec);
                    
                } 
                
                if(updatedPayments.size() >0)
                    update updatedPayments; 
                            
            }   
        }
        else if(picklistval!=null && picklistval=='Change End Date'){
            if(EndDate!=null && enddate < string.valueof(date.today())){
   
                Id vendorRecordTypeId= Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Vendor_Invoice').getRecordTypeId(); 
                list<Payment__c> payments=[select id,Written_Off__c,recordtypeid,Invoice_Stage__c,Opportunity__c from Payment__c where recordtypeid=:vendorRecordTypeId and Paid__c=:false and
                                Written_Off__c=:false and Opportunity__c=:recordid];
                            
                for( Payment__c payment : payments){
                    Payment__c paymentrec = new Payment__c();
                    paymentrec.id = payment.id;
                    paymentrec.Written_Off__c=true;
                    paymentrec.Invoice_Stage__c='Write off';
                    updatedPayments.add(paymentrec);
                    
                }    
                
                if(updatedPayments.size()>0)
                    update updatedPayments;  
                else if(updatedPayments.size()==0)
                    opp.stagename='Closed';      
            }
            else{
                opp.End_Date__c=date.valueof(EndDate);
                list<string> endDateVal=new list<string>();
                endDateVal=EndDate.split('-');
                system.debug('**Val of endDateval'+endDateVal);
                integer endDateMonth;
                integer endDateYear;
                if(endDateVal.size()>0){
                   endDateMonth= integer.valueof(endDateVal[1]);
                   endDateYear=integer.valueof(endDateVal[0]);
                }
                  
            
                Id vendorRecordTypeId= Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Vendor_Invoice').getRecordTypeId(); 
                list<Payment__c> payments = New list<Payment__c>();
                if(endDateMonth!=null && endDateYear!=null)
                    payments=[select id,Written_Off__c,recordtypeid,Invoice_Stage__c,Opportunity__c from Payment__c where recordtypeid=:vendorRecordTypeId and Paid__c=:false and
                            Written_Off__c=:false and CALENDAR_MONTH(Scheduled_Date__c)>=: endDateMonth and CALENDAR_YEAR(Scheduled_Date__c)>=: endDateYear and Opportunity__c=:recordid];
                for(Payment__c payment : payments){
                    Payment__c paymentrec = new Payment__c ();
                    paymentrec.id=payment.id;
                    paymentrec.Written_Off__c=true;
                    updatedPayments.add(paymentrec);
                } 
                
                if(updatedPayments.size()>0)
                    update updatedPayments;           
                           
            }
            if(updatedPayments.size()>0)
                saw.invoices =updatedPayments;
           
        }
        update opp;
        return saw; 
        
    }
    
    public class OpportunityWrap{
        @auraEnabled
        public opportunity opp;
        @auraEnabled
        public list<Payment__c> invoices;
    }
    
    public class SaveWrapper{
        @auraEnabled
        public string picklistval;
        @auraEnabled
        public list<Payment__c> invoices;
    }
}