@isTest
public class trgGenerateAutoNumberTest {
    public static testmethod void testcreateOpp(){
        Id CARecTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Case Actions').getRecordTypeId();
        account accountrec = new account();
       // accountrec.recordtypeid=vendorrecType;
        accountrec.Name='test vendor';
        accountrec.Tax_ID__c='333-33-3333';
        accountrec.Entity_Type__c='Individual';
        insert accountrec;
        List<opportunity> oppList= new List<opportunity>();
        for(integer i=0;i<10;i++){
            opportunity opprecord = new opportunity();
          opprecord.CloseDate=date.today();
          opprecord.StageName='Draft';
          opprecord.Name='test opportunity';
            opprecord.AccountId=accountrec.id;
      oppList.add(opprecord);
        }
        for(integer i=0;i<10;i++){
            opportunity opprecord1 = new opportunity();
          opprecord1.CloseDate=date.today();
          opprecord1.StageName='Draft';
          opprecord1.Name='test opportunity';
            opprecord1.AccountId=accountrec.id;
            opprecord1.RecordTypeId=CARecTypeId;
      oppList.add(opprecord1);
        }
        insert oppList;
    }
}