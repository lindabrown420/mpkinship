@isTest
public class CategoryTrgTest {
	 @testSetup
    static void testRecords(){
        id recType = Schema.SObjectType.contact.getRecordTypeInfosByDeveloperName().get('Contacts').getRecordTypeId();
        contact con = new contact();
        con.recordtypeid=recType;
        con.LastName='contactrec';
        con.FirstName='test';
        con.Email='test@contact.com';        
        insert con;
        
        Kinship_Case__c caserec = new Kinship_Case__c();
        caserec.Primary_Contact__c=con.id;
        insert caserec;
         
        //id vendorrecType = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Vendor').getRecordTypeId();
        account accountrec = new account();
        //accountrec.recordtypeid=vendorrecType;
        accountrec.Name='test vendor';
        accountrec.Tax_ID__c='222-22-3333';
        accountrec.Entity_Type__c='Individual';
        insert accountrec;
        
         Locality_Account__c la = new Locality_Account__c();
        la.Name='222';
        insert la;
        
        Category__c category = new Category__c();
        category.Name='222';
        category.Locality_Account__c =la.id;
        category.Category_Type__c='Adoption';
        category.RecordTypeId= Schema.SObjectType.category__C.getRecordTypeInfosByDeveloperName().get('LASER_Category').getRecordTypeId();
        insert category;  
        
        TriggerActivation__c  ta = new TriggerActivation__c();
        ta.name='CategoryTrigger';
        ta.isActive__c=true;
        insert ta;
        
        TriggerActivation__c  ta1 = new TriggerActivation__c();
        ta1.name='PurchaseOrder Trigger';
        ta1.isActive__c=true;
        insert ta1;
        
        FIPS__c fp = new FIPS__c();
        fp.name='test fips';
        insert fp;
        id recTypeid = Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Fiscal_Year').getRecordTypeId();
        campaign cap = new campaign();
        cap.FIPS__c=fp.id;
        cap.RecordTypeId=recTypeid;
        cap.Fiscal_Year_Type__c='Local';
        cap.name='test';
        cap.Begin_Date__c=system.today().addmonths(-1);
        cap.EndDate=system.today().addyears(1);
        insert cap;
    }
      public static testmethod void test1(){
       list<TriggerActivation__c> talist=[select id,name,isActive__C from TriggerActivation__c where isActive__c=:true 
                                           and name=:'CategoryTrigger'];
        list<TriggerActivation__c> talist1=[select id,name,isActive__C from TriggerActivation__c where isActive__c=:true 
                                            and name=:'PurchaseOrder Trigger'];
        list<Kinship_Case__c> cases=[select id from Kinship_Case__c];
        list<account> accounts=[select id,recordtypeid from account where name='test vendor'];        
        list<Category__c> categories=[select id from Category__c where name=:'222'];
          //insert a new product
        Product2 p = new product2(name='x',isactive=true,Unit_Measure__c='1');
        insert p;
        
        Product2 p2 = new product2(name='y',isactive=true,Unit_Measure__c='1');
        insert p2;
        list<fips__C> fip=[select id,name from fips__C];
        list<campaign> camp=[select id,fips__c,Begin_Date__c,enddate,Fiscal_Year_Type__c from campaign];
        //define the standart price for the product
        id stdPb = Test.getStandardPricebookId();
        insert new PricebookEntry(pricebook2id = stdPb, product2id = p.id,unitprice=1.0, isActive=true);
        
        Pricebook2 pb = new pricebook2(name='test',Vendor__c=accounts[0].id,IsActive=true,
                                      Accounting_Period__c=camp[0].id);
        insert pb;
        
        PricebookEntry pbe = new PricebookEntry(pricebook2id=pb.id, product2id=p.id,unitprice=1.0, isActive=true);
        insert pbe; 
        id recType = Schema.SObjectType.opportunity.getRecordTypeInfosByDeveloperName().get('Purchase_Order').getRecordTypeId();
       
         opportunity opprecord = new opportunity();
        opprecord.CloseDate=date.today();
        opprecord.StageName='Draft';
        opprecord.Name='test opportunity';
        opprecord.Kinship_Case__c=cases[0].id;
        opprecord.Payment_Type__c='Credit Card';
        opprecord.campaignid=camp[0].id;
        opprecord.Amount=20000;
        //opprecord.Category__c =categories[0].id;
        opprecord.AccountId=accounts[0].id;
        opprecord.RecordTypeId=recType;
        opprecord.Number_of_Months_Units__c=3;
        opprecord.Pricebook2Id =pb.id;        
        insert opprecord;
        //NOW CREATING LINE ITEMS
        opportunitylineitem opplineitem = new opportunitylineitem();
        opplineitem.Unit_Measure__c='2';
        opplineitem.Product2Id=p.id;
        opplineitem.PricebookEntryId=pbe.id;
        opplineitem.Quantity=2;
        opplineitem.Category__c=categories[0].id;
        opplineitem.OpportunityId=opprecord.id;
        opplineitem.TotalPrice = 2000;
        insert opplineitem;
         system.debug('**Cateogries '+ categories[0]); 
          try{
        list<category__C> catlist =[select id,inactive__C from category__C where name=:'222'];
         category__C cat = new category__c();
          cat.id= catlist[0].id;
          cat.Inactive__c=true;
          update cat;
          }
          catch(Exception e){}
      } 
    
    public static testmethod void test2(){
       list<TriggerActivation__c> talist=[select id,name,isActive__C from TriggerActivation__c where isActive__c=:true 
                                           and name=:'CategoryTrigger'];
        list<TriggerActivation__c> talist1=[select id,name,isActive__C from TriggerActivation__c where isActive__c=:true 
                                            and name=:'PurchaseOrder Trigger'];
        list<Kinship_Case__c> cases=[select id from Kinship_Case__c];
        list<account> accounts=[select id,recordtypeid from account where name='test vendor'];        
       
        
        list<fips__C> fip=[select id,name from fips__C];
        list<campaign> camp=[select id,fips__c,Begin_Date__c,enddate,Fiscal_Year_Type__c from campaign];
       
        id recType = Schema.SObjectType.opportunity.getRecordTypeInfosByDeveloperName().get('Case_Actions').getRecordTypeId();
        Locality_Account__c la = new Locality_Account__c();
        la.Name='222';
        insert la;
        
        Category__c category = new Category__c();
        category.Name='test name';
        category.Locality_Account__c =la.id;
        category.Category_Type__c='Adoption';
        category.RecordTypeId= Schema.SObjectType.category__C.getRecordTypeInfosByDeveloperName().get('LASER_Category').getRecordTypeId();
        insert category;  
		list<Category__c> categories=[select id from Category__c where name=:'222'];
        opportunity opprecord1 = new opportunity();
        opprecord1.CloseDate=date.today();
        opprecord1.End_Date__c=date.today().addmonths(2);
        opprecord1.StageName='Draft';
        opprecord1.CampaignId=camp[0].id;
        opprecord1.Name='test opportunity1';
        opprecord1.Kinship_Case__c=cases[0].id;
        opprecord1.Parent_Recipient__c='1';
        opprecord1.RecordTypeId=recType;
        opprecord1.Category__c =categories[0].id;
        opprecord1.AccountId=accounts[0].id;
        opprecord1.Type_of_Case_Action__c='one time';
        opprecord1.Number_of_Months_Units__c=3;       
        insert opprecord1;
        system.debug('**opprecord val'+opprecord1.Category__c);
         
          try{
        //list<category__C> catlist =[select id,inactive__C from category__C where name=:'222'];
         category__C cat = new category__c();
          cat.id= opprecord1.Category__c;
          cat.Inactive__c=true;
          update cat;
          system.debug('category id'+cat.id);
          }
          catch(Exception e){}
      }     
}