@isTest
public class ServicePlanTriggerTest {
    static testmethod void testmethod1(){
        
        Contact conTest = new Contact();
        conTest.WorkPhone__c = '12343';
        conTest.LastName = 'Test Last';        
        insert conTest;
        
        Kinship_Case__c caseTest = new Kinship_Case__c();
        caseTest.Name = 'Test case';
        caseTest.Primary_Contact__c = conTest.Id;
        insert caseTest;
        
        CSA_Meeting_Agenda__c csaAgenda = new CSA_Meeting_Agenda__c();
        csaAgenda.Name = 'Test Agenda';
        csaAgenda.Meeting_Date__c = Date.newInstance(2022, 10, 06);
        insert csaAgenda;
        
        CSA_Agenda_Team__c teamTest = new CSA_Agenda_Team__c();
        teamTest.Name = 'Test case';
        insert teamTest;
        
        
       

        
    

        Test.startTest();
        
        IFSP__c ifsp = new IFSP__c();
        ifsp.Case__c = caseTest.Id;
        ifsp.CSA_Team__c = teamTest.Id;
        ifsp.Status__c = 'Draft';
        ifsp.FAPT_Date__c = Date.newInstance(2022, 10, 06);
       //  ifsp.FAPT_Date__c = Date.newInstance(2021, 10, 06);
       // update ifsp; 
        insert ifsp;
        
        Test.stopTest();
        
    }
}