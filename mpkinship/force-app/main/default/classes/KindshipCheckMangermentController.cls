public class KindshipCheckMangermentController {
    public final static STRING ACC = 'Account';
    public final static STRING CATE = 'Cate';
    public final static STRING FIPS = 'FIPS';
    public final static STRING KCASE = 'Case';
    private final static Integer MAX_RESULTS = 5;

    @AuraEnabled(cacheable=true)
    public static List<LookupSearchResult> kinshipSearchLookup(String searchTerm, List<String> selectedIds, String anOptionalParam) {
        // Prepare query paramters
        searchTerm += '*';

        // Prepare results
        List<LookupSearchResult> results = new List<LookupSearchResult>();
        List<List<SObject>> searchResults;

        if(String.isNotBlank(anOptionalParam)) {
            if(ACC.equals(anOptionalParam)) {
                searchResults = [FIND :searchTerm IN ALL FIELDS RETURNING
                                Account (Id, Name, BillingCity WHERE id NOT IN :selectedIds)
                                LIMIT :MAX_RESULTS];
                String accountIcon = 'standard:account';
                Account [] accounts = ((List<Account>) searchResults[0]);
                for (Account account : accounts) {
                    results.add(new LookupSearchResult(account.Id, 'Account', accountIcon, account.Name, ''));
                }
            } else if(CATE.equals(anOptionalParam)) {
                searchResults = [FIND :searchTerm IN ALL FIELDS RETURNING
                                Category__c (Id, Name, Category_Account_Description__c, Category_Type__c WHERE id NOT IN :selectedIds)
                                LIMIT :MAX_RESULTS];
                String categoryIcon = 'standard:category';
                Category__c[] cates = ((List<Category__c>) searchResults[0]);
                for (Category__c cate : cates) {
                    results.add(new LookupSearchResult(cate.Id, 'Category__c', categoryIcon, cate.Name, ''));
                }
            }/* else if(FIPS.equals(anOptionalParam)) {
                searchResults = [FIND :searchTerm IN ALL FIELDS RETURNING
                                FIPS__c (Id, Name WHERE id NOT IN :selectedIds)
                                LIMIT :MAX_RESULTS];
                String icon = 'standard:category';
                FIPS__c[] items = ((List<FIPS__c>) searchResults[0]);
                for (FIPS__c i : items) {
                    results.add(new LookupSearchResult(i.Id, 'FIPS__c', icon, i.Name, ''));
                }
            } else if(KCASE.equals(anOptionalParam)) {
                searchResults = [FIND :searchTerm IN ALL FIELDS RETURNING
                                Kinship_Case__c (Id, Name WHERE id NOT IN :selectedIds)
                                LIMIT :MAX_RESULTS];
                String icon = 'standard:account';
                Kinship_Case__c[] items = ((List<Kinship_Case__c>) searchResults[0]);
                for (Kinship_Case__c i : items) {
                    results.add(new LookupSearchResult(i.Id, 'Kinship_Case__c', icon, i.Name, ''));
                }
            }*/
        }
        return results;
    }

    @AuraEnabled
    public static string getSessionId(){
        return Userinfo.getSessionId();
    }

    @AuraEnabled
    public static List<CheckObj> searchCheckData(List<String> lstVendor, String searchStatus, Date startDate, Date endDate){
        String query = 'SELECT Id, Name, Check_No__c, Issue_Date__c, Vendor__c,Vendor__r.Name, Description__c, ' +
                        'Amount__c, Status__c, Is_Printed__c ' +
                        ' FROM Kinship_Check__c ';
        String strWhere = '';
        if(lstVendor != null && lstVendor.size() > 0) {
            strWhere += ' AND Vendor__c IN :lstVendor';
        }
        if(String.isNotBlank(searchStatus)) {
            strWhere += ' AND Status__c = :searchStatus';
        }
        if(startDate != null) {
            strWhere += ' AND Issue_Date__c >= :startDate';
        }
        if(endDate != null) {
            strWhere += ' AND Issue_Date__c <= :endDate';
        }
        if(String.isNotBlank(strWhere)) {
            query += ' WHERE ' + strWhere.replaceFirst('AND', ' ');
        }
        query += ' ORDER BY Vendor__c LIMIT 10000 ';

        System.debug('---searchCheckData---' + query);

        List<CheckObj> lstResult = new List<CheckObj>();
        for(Kinship_Check__c kc : Database.query(query)) {
            CheckObj temp = new CheckObj();
            temp.id = kc.id;
            temp.Name = kc.Name;
            temp.checkUrl = '/' + kc.id;
            temp.checkNo = kc.Check_No__c;
            temp.issueDate = kc.Issue_Date__c;
            temp.vendorName = String.isNotBlank(kc.Vendor__c)?kc.Vendor__r.Name:'';
            temp.vendorUrl = String.isNotBlank(kc.Vendor__c)?'/' + kc.Vendor__c:'';
            temp.description = kc.Description__c;
            temp.amount = kc.Amount__c;
            //temp.organization = kc.Organization__c;
            temp.status = kc.Status__c;
            temp.isPrinted = kc.Is_Printed__c;

            lstResult.add(temp);
        }

        return lstResult;
    }

    @AuraEnabled
    public static List<Kinship_Check__c> updateCheckNo(List<String> lstId, Integer startNumber){
        try {
            List<Kinship_Check__c> lstUpdate = new List<Kinship_Check__c>();
            for(Integer i = 0 ; i < lstId.size() ; i++) {
                Kinship_Check__c temp = new Kinship_Check__c(Id = lstId[i], Check_No__c = String.valueOf(startNumber + i));
                temp.Is_Printed__c = true;
                lstUpdate.add(temp);
            }
            if(lstUpdate.size() > 0) {
                update lstUpdate;
            }
            return lstUpdate;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static List<CheckObj> getCheckData(String startNo, String endNo, String searchStatus,
                                             Date checkDate, Decimal amountFrom, Decimal amountTo){
        List<String> lstRS = new List<String>();
        lstRS.add('Completed');
        lstRS.add('');
        String query = 'SELECT Id, Name, Check_No__c, Issue_Date__c, Vendor__c,Vendor__r.Name, Description__c, ' +
                        'Amount__c, Status__c, Is_Printed__c ' +
                        ' FROM Kinship_Check__c ' +
                        ' WHERE Check_No__c <> null AND Request_Support__c IN :lstRS ';
        String strWhere = '';
        if(String.isNotBlank(startNo)) {
            strWhere += ' AND Check_No__c = :startNo';
        }
        if(String.isNotBlank(endNo)) {
            strWhere += ' AND Check_No__c <= :endNo';
        }
        if(String.isNotBlank(searchStatus)) {
            strWhere += ' AND Status__c = :searchStatus';
        }
        if(checkDate != null) {
            strWhere += ' AND Issue_Date__c = :checkDate';
        }
        if(amountFrom != null) {
            strWhere += ' AND Amount__c = :amountFrom';
        }
        if(amountTo != null) {
            strWhere += ' AND Amount__c <= :amountTo';
        }
        query += strWhere + ' ORDER BY Issue_Date__c DESC LIMIT 10000 ';

        System.debug('---getCheckData---' + query);
		List<Kinship_Check__c> lstCheck = Database.query(query);
        List<CheckObj> lstResult = new List<CheckObj>();
        if(lstCheck.size() > 0) {
            Set<Id> setId = new Set<Id>();
            for(Kinship_Check__c kc : lstCheck) {
                setId.add(kc.Id);
            }
           	Map<String, List<Payment__c>> mapPayment = new Map<String, List<Payment__c>>();
            List<Payment__c> lstTemp;
            for(Payment__c payment : [SELECT Id,Name, recordType.Name, Kinship_Check__c, Opportunity__c, Opportunity__r.AccountId, Opportunity__r.Account.Name,
                                                Opportunity__r.Kinship_Case__c,Opportunity__r.FIPS_Code__c,Opportunity__r.Category__c,
                                                Opportunity__r.Kinship_Case__r.Name, Opportunity__r.FIPS_Code__r.Name, Opportunity__r.Category__r.Name,
                                                Vendor__c, FIPS_Code__c, Category_lookup__c, Kinship_Case__c, Service_Begin_Date__c, Service_End_Date__c, 
                                                Payment_Date__c,Payment_Amount__c,Opportunity__r.Legacy_Category_Code__c,Paid__c,Invoice_Stage__c,
                                                Payment_Method__c,Scheduled_Date__c,Cost_Center__c
                                               	FROM Payment__c
                                                WHERE Kinship_Check__c IN :setId]){
            	lstTemp = mapPayment.get(payment.Kinship_Check__c);
                if(lstTemp == null) lstTemp = new List<Payment__c>();
                lstTemp.add(payment);
                mapPayment.put(payment.Kinship_Check__c, lstTemp);
            }
            for(Kinship_Check__c kc : lstCheck) {
                CheckObj temp = new CheckObj();
                temp.id = kc.id;
                temp.checkUrl = '/' + kc.id;
                temp.checkNo = kc.Check_No__c;
                temp.issueDate = kc.Issue_Date__c;
                temp.vendorName = String.isNotBlank(kc.Vendor__c)?kc.Vendor__r.Name:'';
                temp.vendorUrl = String.isNotBlank(kc.Vendor__c)?'/' + kc.Vendor__c:'';
                temp.vendorId = kc.Vendor__c;
                temp.description = kc.Description__c;
                temp.amount = kc.Amount__c;
                //temp.organization = kc.Organization__c;
                temp.status = kc.Status__c;
                temp.isPrinted = kc.Is_Printed__c;
    			lstTemp = mapPayment.get(kc.id);
                if(lstTemp == null) lstTemp = new List<Payment__c>();
                temp.lstPayment = lstTemp;
                lstResult.add(temp);
            }
        }

        return lstResult;
    }

    @AuraEnabled
    public static Date initData(){
        List<Kinship_Check__c> lstCheck = [SELECT id,Issue_Date__c FROM Kinship_Check__c ORDER BY Issue_Date__c DESC LIMIT 1];
        if(lstCheck.size() > 0) {
            return lstCheck[0].Issue_Date__c;
        }
        return null;
    }

    @AuraEnabled
    public static void updateChecks(List<String> lstCheck, String status){
        try {
            if(lstCheck == null || lstCheck.size() == 0) return;
            List<Kinship_Check__c> lstUpdate = new List<Kinship_Check__c>();
            for(String str : lstCheck) {
                Kinship_Check__c temp = new Kinship_Check__c();
                temp.Id = str;
                temp.Status__c = status;
                lstUpdate.add(temp);
            }
            update lstUpdate;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static void updateChecksPrinted(List<String> lstCheck){
        try {
            if(lstCheck == null || lstCheck.size() == 0) return;
            List<Kinship_Check__c> lstUpdate = new List<Kinship_Check__c>();
            for(String str : lstCheck) {
                Kinship_Check__c temp = new Kinship_Check__c();
                temp.Id = str;
                temp.Is_Printed__c = true;
                lstUpdate.add(temp);
            }
            update lstUpdate;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static List<exportData> getExportData(List<String> lstCheck){
        List<exportData> result = new List<exportData>();
        Set<Id> setId = new Set<Id>();
        List<Payment__c> lstPayment = [SELECT id, FIPS_Code__c, FIPS_Code__r.Locality_Code__c, Category_lookup__c, Category_lookup__r.Locality_Account__c, 
                                                        Opportunity__c,Opportunity__r.AccountId,Opportunity__r.Account.Vendor_Code__c, 
                                                        Kinship_Check__c, Kinship_Check__r.Issue_Date__c,Kinship_Check__r.Description__c, Kinship_Check__r.Amount__c, 
                                                        Opportunity__r.Kinship_Case__r.Case_No__c,Opportunity__r.Kinship_Case__r.Name,
                                                        Cost_Center__r.Cost_Center_Code__c,Category_lookup__r.Locality_Account__r.Name,
                                                        Payment_Date__c,Name,Payment_Amount__c,Case_Name__c,
                                                        PO_Number__c,Invoice_Number__c,Description__c,Opportunity__r.RecordType.Name, Service_Period__c,
                                                        Kinship_Check__r.Check_Run__c,Vendor__c
                                                    FROM Payment__c 
                                                    WHERE Kinship_Check__c IN :lstCheck
                                                    AND Opportunity__r.RecordType.Name != 'Report of Collections'
                                                    AND RecordType.Name != 'Adjustments'];
        for(Payment__c d : lstPayment) {
            setId.add(d.Vendor__c);
        }
        Map<String, Locality_Vendor_Number__c> mapLVN = new Map<String, Locality_Vendor_Number__c>();
        for(Locality_Vendor_Number__c lvn : [SELECT id, Name, Vendor__c, FIPS__c FROM Locality_Vendor_Number__c WHERE Vendor__c IN :setId]) {
            mapLVN.put(lvn.Vendor__c + '-' + lvn.FIPS__c, lvn);
        }

        for(Payment__c data : lstPayment) {
            exportData temp = new exportData();
            Locality_Vendor_Number__c lvn = mapLVN.get(data.Vendor__c + '-' + data.FIPS_Code__c);
            temp.id = data.id;
            temp.fipsCode = data.FIPS_Code__r.Locality_Code__c;
            temp.accountCode = data.Category_lookup__r.Locality_Account__r.Name;
            temp.vendorCode = data.Opportunity__r.Account.Vendor_Code__c;
            temp.checkDate = data.Kinship_Check__r.Issue_Date__c;
            temp.memo = data.Kinship_Check__r.Description__c;
            temp.amount = data.Kinship_Check__r.Amount__c;
            temp.caseNo = data.Opportunity__r.Kinship_Case__r.Name;
            temp.costCenterCode = data.Cost_Center__r.Cost_Center_Code__c;
            temp.localityAccountNumber = data.Category_lookup__r.Locality_Account__r.Name;
            if(lvn == null) {
                temp.localityVendorNumber = '';
            } else {
                temp.localityVendorNumber = lvn.Name;
            }
            temp.paymentDate = data.Payment_Date__c;
            temp.poNumber = data.PO_Number__c;
            temp.paymentNumber = data.Name;
            temp.caseName = data.Case_Name__c;
            temp.invoiceNumber = data.Invoice_Number__c;
            temp.description = data.Description__c;
            temp.amount = data.Payment_Amount__c;
            temp.recordTypeName = data.Opportunity__r.RecordType.Name;
            temp.servicePeriod = data.Service_Period__c;
            temp.CheckRun = data.Kinship_Check__r.Check_Run__c;
            
            result.add(temp);
        }
        return result;
    }

    @AuraEnabled
    public static List<Check_Run__c>  getCheckRun(Date checkDate){
        return [SELECT Id, Name, Check_Date__c, Number_Of_Check__c, FIPS__c,FIPS_Name__c, Check_Run_Total__c FROM Check_Run__c WHERE Check_Date__c = :checkDate];   
    }

    @AuraEnabled
    public static List<String> getPicklistvalues(String objectName, String field_apiname, String strNone){
        List<String> optionlist = new List<String>();       
        Map<String,Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
        Map<String, Schema.SObjectField> field_map = gd.get(objectName.toLowerCase()).getDescribe().fields.getMap();        
        List<Schema.PicklistEntry> picklistValues = field_map.get(field_apiname).getDescribe().getPickListValues();       
        if(String.isNotBlank(strNone)){
            optionlist.add(strNone);
        }       
        for (Schema.PicklistEntry pv : picklistValues) {
            optionlist.add(pv.getValue());
        }
        return optionlist;
    }
    
    @AuraEnabled
    public static String changeCheckStatus(String recordid, String status){
        Kinship_Check__c kc = [Select id,status__c from Kinship_Check__c where Id =: recordid];
        try {
            kc.status__c = status;
            update kc;
            return 'Done';
        } catch (Exception e) {
            if(!Test.isRunningTest())
                throw new AuraHandledException(e.getMessage());
            else
                return null;
        }
    }

    @AuraEnabled
    public static List<CheckObj> getMergeChecks(CheckObj check){
        List<CheckObj> lstResult = new List<CheckObj>();
        for(Kinship_Check__c kc : [SELECT Id, Name, Check_No__c, Issue_Date__c, Vendor__c,Vendor__r.Name, Description__c, Amount__c, Status__c, Is_Printed__c
                                    FROM Kinship_Check__c
                                    WHERE Issue_Date__c = :check.issueDate
                                    AND Vendor__c = :check.vendorId
                                    AND Id != :check.id
                                    AND Status__c = 'Issued']) {
            CheckObj temp = new CheckObj();
            temp.id = kc.id;
            temp.checkUrl = '/' + kc.id;
            temp.checkNo = kc.Check_No__c;
            temp.issueDate = kc.Issue_Date__c;
            temp.vendorName = String.isNotBlank(kc.Vendor__c)?kc.Vendor__r.Name:'';
            temp.vendorUrl = String.isNotBlank(kc.Vendor__c)?'/' + kc.Vendor__c:'';
            temp.description = kc.Description__c;
            temp.amount = kc.Amount__c;
            temp.status = kc.Status__c;
            temp.isPrinted = kc.Is_Printed__c;

            lstResult.add(temp);
        }
        return lstResult;
    }

    @AuraEnabled
    public static void saveMergeCheck(List<String> lstId, String mainCheck){
        try{
            List<Kinship_Check__c> lstCheck = [SELECT Id, Name, Name__c, Check_No__c, Issue_Date__c, Vendor__c, Amount__c, Status__c, Check_Run__c,
                                                    Check_Run__r.Number_Of_Check__c, Check_Run__r.Check_Run_Total__c
                                                FROM Kinship_Check__c
                                                WHERE id IN :lstId OR Id = :mainCheck];
            Kinship_Check__c checkUpdate;
            Check_Run__c mainCheckRun = new Check_Run__c();
            for(Kinship_Check__c c : lstCheck) {
                if(c.Id == mainCheck) {
                    checkUpdate = c;
                    mainCheckRun.Id = c.Check_Run__c;
                    mainCheckRun.Number_Of_Check__c = c.Check_Run__r.Number_Of_Check__c;
                    mainCheckRun.Check_Run_Total__c = c.Check_Run__r.Check_Run_Total__c;
                    break;
                }
            }
            System.debug('---saveMergeCheck---' + lstCheck.size() + ' ' + checkUpdate);
            List<Kinship_Check__c> lstDelete = new List<Kinship_Check__c>();
            Set<Id> setId = new Set<Id>();
            Map<String, Check_Run__c> mapCR = new Map<String, Check_Run__c>();
            List<Check_Run__c> lstCRUdate = new List<Check_Run__c>();
            List<Check_Run__c> lstCRDelete = new List<Check_Run__c>();
            for(Kinship_Check__c c : lstCheck) {
                if(c.Id != mainCheck) {
                    checkUpdate.Amount__c += c.Amount__c;
                    lstDelete.add(c);
                    if(c.Check_Run__c != mainCheckRun.Id) {
                        mainCheckRun.Check_Run_Total__c += c.Amount__c;
                        Check_Run__c temp = mapCR.get(c.Check_Run__c);
                        if(temp == null) {
                            temp = new Check_Run__c();
                            temp.id = c.Check_Run__c;
                            temp.Number_Of_Check__c = c.Check_Run__r.Number_Of_Check__c;
                            temp.Check_Run_Total__c = c.Check_Run__r.Check_Run_Total__c;
                            mapCR.put(c.Check_Run__c, temp);
                        }
                        temp.Number_Of_Check__c -= 1;
                        temp.Check_Run_Total__c -= c.Amount__c;
                    } else {
                        mainCheckRun.Number_Of_Check__c -= 1;
                    }
                }
            }
            System.debug('---saveMergeCheck---' + mapCR.values());
            for(Check_Run__c cr : mapCR.values()){
                if(cr.Number_Of_Check__c <= 0) {
                    lstCRDelete.add(cr);
                } else {
                    lstCRUdate.add(cr);
                }
            }
            // udpate payment
            List<Payment__c> lstPayment = [SELECT id,Kinship_Check__c FROM Payment__c WHERE Kinship_Check__c IN :lstId];
            for(Payment__c payment : lstPayment) {
                payment.Kinship_Check__c = mainCheck;
            }
            checkUpdate.Name = checkUpdate.Name__c + ' ' + checkUpdate.Amount__c;
            update checkUpdate;
            if(lstDelete.size() > 0) delete lstDelete;
            update mainCheckRun;
            if(lstCRDelete.size() > 0) delete lstCRDelete;
            if(lstCRUdate.size() > 0) update lstCRUdate;
            if(lstPayment.size() > 0 ) {
                PaymentTriggerHandler.bypassOppUpdate = true;
                TransactionJournalService.byPassCheckPosted = true;
                Paymentrecordhelper.IsPaymentDateUpdate = true;
                PaymentTriggerHandler.bypassBTrigger = true;
                update lstPayment;
                PaymentTriggerHandler.bypassBTrigger = false;
                TransactionJournalService.byPassCheckPosted = false;
                PaymentTriggerHandler.bypassOppUpdate = false;
                Paymentrecordhelper.IsPaymentDateUpdate = false;
            }
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static void saveMigrationCheck(CheckObj migrationCheck){
        List<Opportunity> lstOppUpdate = new List<Opportunity>();
        Set<Id> setId = new Set<Id>();
        for(Integer i = 0 ; i < migrationCheck.lstPayment.size() ; i++) {
            if(!setId.contains(migrationCheck.lstPayment[i].Opportunity__c)) {
                lstOppUpdate.add(new Opportunity(Id = migrationCheck.lstPayment[i].Opportunity__c,
                                                Category__c = migrationCheck.lstPayment[i].Opportunity__r.Category__c));
                setId.add(migrationCheck.lstPayment[i].Opportunity__c);
            }
        }
        system.debug('---saveMigrationCheck---' + lstOppUpdate);
        system.debug('---saveMigrationCheck---' + migrationCheck.lstPayment);
        PaymentTriggerHandler.bypassOppUpdate = true;
        TransactionJournalService.byPassCheckPosted = true;
        Paymentrecordhelper.IsPaymentDateUpdate = true;
        update lstOppUpdate;
        update migrationCheck.lstPayment;
        TransactionJournalService.byPassCheckPosted = false;
        PaymentTriggerHandler.bypassOppUpdate = false;
        Paymentrecordhelper.IsPaymentDateUpdate = false;
    }
    
    @AuraEnabled
    public static void updateCheckNo(List<CheckObj> lstData) {
        List<Kinship_Check__c> lstCheck = new List<Kinship_Check__c>();
        for(CheckObj obj : lstData) {
            lstCheck.add(new Kinship_Check__c(id = obj.id, Check_No__c = obj.checkNo));
        }
        if(lstCheck.size() > 0) {
            update lstCheck;
        }
    }

    @AuraEnabled
    public static List<PaymentObj> getPaymentCreated(String strId){
        List<PaymentObj> lstResult = new List<PaymentObj>();
        for(Payment__c p : [SELECT id, name, Payment__c, Payment__r.Kinship_Check__c, Opportunity__c, 
                                    Opportunity__r.Name, Payment_Amount__c, Payment_Date__c
                                    FROM Payment__c 
                                    WHERE Payment__r.Kinship_Check__c = :strId]) {
            PaymentObj temp = new PaymentObj();
            temp.id = p.id;
            temp.paymentUrl = '/' + p.id;
            temp.paymentNo = p.Name;
            temp.paymentDate = p.Payment_Date__c;
            temp.oppName = p.Opportunity__r.Name;
            temp.oppUrl = '/' + p.Opportunity__c;
            temp.amount = p.Payment_Amount__c;

            lstResult.add(temp);
        }
        return lstResult;
    }

    @AuraEnabled
    public static void requestSupport(String recordid, String status){
        try {
            Kinship_Check__c kc = new Kinship_Check__c();
            kc.id = recordid;
            kc.Request_Support__c = status;
            kc.Request_Support_User__c = Userinfo.getUserId();
            update kc;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    public class exportData{
        @AuraEnabled
        public String id{get;set;}

        @AuraEnabled
        public String fipsCode{get;set;}

        @AuraEnabled
        public String accountCode{get;set;}

        @AuraEnabled
        public String vendorCode{get;set;}

        @AuraEnabled
        public Date checkDate{get;set;}

        @AuraEnabled
        public String memo{get;set;}

        @AuraEnabled
        public Decimal amount{get;set;}

        @AuraEnabled
        public String caseNo{get;set;}

        @AuraEnabled
        public String costCenterCode{get;set;}

        @AuraEnabled
        public String localityAccountNumber{get;set;}

        @AuraEnabled
        public String localityVendorNumber{get;set;}

        @AuraEnabled
        public Date paymentDate{get;set;}

        @AuraEnabled
        public String poNumber{get;set;}

        @AuraEnabled
        public String paymentNumber{get;set;}

        @AuraEnabled
        public String caseName{get;set;}

        @AuraEnabled
        public String invoiceNumber{get;set;}

        @AuraEnabled
        public String description{get;set;}

        @AuraEnabled
        public String recordTypeName{get;set;}

        @AuraEnabled
        public String servicePeriod{get;set;}
        
        @AuraEnabled
        public String checkRun{get;set;}
    }

    public class CheckObj{
        @AuraEnabled
        public String id{get;set;}

        @AuraEnabled
        public String checkNo{get;set;}

        @AuraEnabled
        public String checkUrl{get;set;}

        @AuraEnabled
        public String Name{get;set;}

        @AuraEnabled
        public Decimal amount{get;set;}

        @AuraEnabled
        public Date issueDate{get;set;}

        @AuraEnabled
        public String vendorName{get;set;}

        @AuraEnabled
        public String vendorId{get;set;}

        @AuraEnabled
        public String vendorUrl{get;set;}

        @AuraEnabled
        public String description{get;set;}

        @AuraEnabled
        public String organization{get;set;}

        @AuraEnabled
        public String status{get;set;}

        @AuraEnabled
        public Boolean isPrinted{get;set;}

        @AuraEnabled
        public Boolean isShowCate{get;set;}

        @AuraEnabled
        public Boolean isShowAcc{get;set;}

        @AuraEnabled
        public Boolean isShowCase{get;set;}

        @AuraEnabled
        public Boolean isShowFIPS{get;set;}
        
        @AuraEnabled
        public List<Payment__c> lstPayment{get;set;}
    }

    public class PaymentObj{
        @AuraEnabled
        public String id{get;set;}

        @AuraEnabled
        public String paymentNo{get;set;}

        @AuraEnabled
        public String paymentUrl{get;set;}

        @AuraEnabled
        public Decimal amount{get;set;}

        @AuraEnabled
        public Date paymentDate{get;set;}

        @AuraEnabled
        public String oppName{get;set;}

        @AuraEnabled
        public String oppUrl{get;set;}
    }
}