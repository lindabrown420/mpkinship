public class CategoryRateChangeBatchHelper{

    public static void FetchCases(List<Kinship_Case__c> cases){
        try{
            set<id> selectedCaseIds = new set<id>(); 
            for(Kinship_Case__c  caseRec : cases){
                selectedCaseIds.add(caseRec.id);
            }
        
            //FIND OUT ALL THE CASE ACTIONS RELATED TO THIS CASE
            Id porecTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Case_Actions').getRecordTypeId();
            Id categoryRecTypeId = Schema.SObjectType.Category__c.getRecordTypeInfosByDeveloperName().get('LASER_Category').getRecordTypeId(); 
            list<opportunity> caseActions = [select id,Kinship_Case__r.Age__c,Kinship_Case__r.dob__C,stagename,Kinship_Case__c,category__c,amount from opportunity where
                            recordtypeid=:porecTypeId and stagename=:'Open' and category__c!=null and Kinship_Case__c IN: selectedCaseIds
                            and category__r.recordtypeid=:categoryRecTypeId];
            system.debug('***Caseactions'+ caseactions.size());
            set<id> categoryids  = new set<id>();                
            for(opportunity caseactionRec : caseActions){
                categoryids.add(caseactionRec.category__c);
            } 
         
            //NOW FETCH CATEGORY RATES
            Id RateRecTypeId = Schema.SObjectType.Category_Rate__c.getRecordTypeInfosByDeveloperName().get('Service_Names').getRecordTypeId(); 
            list<Category_Rate__c> catRates=[select id,status__C,Category__c,DOB_Effective_Date__c,Min_Age__c,Max_Age__c,Prorate_First_Payment__c,
                                    Prorate_final_payment__c,Price__c from Category_Rate__c where status__C=:'Active' and                
                            Category__c IN :categoryids  and  DOB_Effective_Date__c=:'1st of the following month following Date of Birth' and Min_Age__c!=null
                            and Max_Age__c!=null and price__C!=null and recordtypeid=:RateRecTypeId and category__r.InActive__c=:False];
            map<id,list<Category_Rate__c>> catAndRateMap = new map<id,list<Category_Rate__c>>();
            for(Category_Rate__c catrate :  catRates){
                if( catAndRateMap.containskey(catrate.Category__c))
                    catAndRateMap.get(catrate.Category__c).add(catrate);
               else
                    catAndRateMap.put(catrate.category__C,new list<Category_Rate__c>{catrate});         
            } 
            system.debug('**VAl ofcateratemap'+catAndRateMap);
            //NOW FIND THE RATE ASSOCIATED WITH CHILD FROM CASEACTIONS
            map<id,Category_Rate__c> opportunityCatRateMap = new map<id,Category_Rate__c>();
            set<id> opporutnityid =new set<id>();
            for( opportunity caseaction : caseActions){
                system.debug('caseaciton id'+caseaction);
                if(caseaction.Kinship_Case__r.Age__c!=null && caseaction.Kinship_Case__r.dob__C!=null){
                    if(catAndRateMap.size()>0 && catAndRateMap.containskey(caseaction.category__C) &&
                     catAndRateMap.get(caseaction.category__C).size()>0){
                         system.debug('entered in first if');
                         //NOW RATES
                         for(Category_Rate__c cr : catAndRateMap.get(caseaction.category__C)){
                             system.debug('***Cr is there'+ cr);
                             if(caseaction.Kinship_Case__r.Age__c>=cr.Min_Age__c && caseaction.Kinship_Case__r.Age__c<=cr.Max_Age__c){
                                 //CHECKING IF AGE FALLS IN ANOTHER RATE NOW.
                                 system.debug('***VAl of cr'+ cr.id);
                                 if(caseaction.amount!=cr.Price__c){
                                     system.debug('***entered in amount not equal to price');
                                     opportunityCatRateMap.put(caseaction.id,cr);  
                                     opporutnityid.add(caseaction.id);  
                                     break;
                                 }
                             }
                         }
                     }
                }
            }       //FOR LOOP ENDS HERE
            system.debug('***VAl of opp ratemap'+opportunityCatRateMap);
            if(opportunityCatRateMap.size()>0){
                list<opportunity> opplist=[select id,name,category__C,closedate,amount,Type_of_Case_Action__c,Prorate_Last_Payment__c,End_Date__c,Prorated_Last_Payment_Amount__c,(Select id,Service_Begin_Date__c,Service_End_Date__c,Payment_Amount__c,
                Amount_Paid__c,Scheduled_Date__c from Payment_OppPayment__r where Paid__c=:False and Written_Off__c=:False) from
                opportunity where id in:opporutnityid];
                system.debug('**opplist'+ opplist);
                system.debug('**Catrate map'+ opportunityCatRateMap);
                list<Payment__c> Payments = new list<Payment__c>();
                for(opportunity opp : opplist){
                    
                    system.debug('**price'+opportunityCatRateMap.get(opp.id));
                    if(opp.Type_of_Case_Action__c=='One time'){
                        if(opp.Payment_OppPayment__r.size()>0){
                            Payment__c opppayment = new Payment__c();
                            opppayment.id = opp.Payment_OppPayment__r[0].id;
                            opppayment.Payment_Amount__c = opportunityCatRateMap.get(opp.id).Price__c; 
                            opppayment.Sending_again_for_approval_Reason__c='Age has been changed';  
                            opppayment.Invoice_Stage__c='Pending';                    
                            Payments.add(opppayment);  
                        }
                    } // ONE TIME PAYMENT ENDS HERE
                    else{
                        system.debug('**Entered in ongooing');
                        for(Payment__c payment : opp.Payment_OppPayment__r){
                           system.debug('value of payment'+ payment);
                            //FOR LAST PAYMENT IF THERE
                            if(payment.Service_End_Date__c == opp.End_Date__c){
                                system.debug('**Enterd in last payment');
                                //FIRST AND LAST PAYMENT THEN PRORATING NOT NEEDED FOR LAST
                                if(payment.Service_Begin_Date__c==opp.closedate){
                                    if(opportunityCatRateMap.get(opp.id).Price__c!=null){
                                         Payment__c opppayment = new Payment__c();
                                         opppayment.id =payment.id;
                                         if(opp.closedate.day()!=1 && opportunityCatRateMap.get(opp.id).Prorate_First_Payment__c==true){
                                              Integer numberDays = Date.daysInMonth(opp.closedate.year(), opp.closedate.month());
                                               integer daysleft = (numberDays - opp.closedate.day())+1;
                                               if(daysleft!=0){
                                                   decimal amountval =  (opportunityCatRateMap.get(opp.id).Price__c * daysleft)/numberDays; 
                                                   opppayment.Payment_Amount__c = amountval;
                                               }    
                                         }
                                         else 
                                             opppayment.Payment_Amount__c = opportunityCatRateMap.get(opp.id).Price__c;
                                         opppayment.Sending_again_for_approval_Reason__c='Age has been changed';  
                                         opppayment.Invoice_Stage__c='Pending';  
                                         Payments.add(opppayment);
                                    }
                                }
                                //PRORATE LAST PAYMNET IF NEEDED
                                else {
                                     system.debug('**Eneted in last payment else');
                                     Payment__c opppayment = new Payment__c();
                                     opppayment.id =payment.id;
                                    if(opportunityCatRateMap.get(opp.id).Prorate_final_payment__c==true ){                                                                       
                                          Integer numberDays = Date.daysInMonth(opp.End_Date__c.year(), opp.End_Date__c.month());  
                                          decimal amountval =  (opportunityCatRateMap.get(opp.id).Price__c * opp.End_Date__c.day())/numberDays;  
                                         opppayment.Payment_Amount__c =amountval;  
                                          opppayment.Sending_again_for_approval_Reason__c='Age has been changed';  
                                          opppayment.Invoice_Stage__c='Pending';  
                                          Payments.add(opppayment);                                    
    
                                    }
                                    else{
                                        opppayment.Payment_Amount__c =opportunityCatRateMap.get(opp.id).Price__c;
                                        opppayment.Sending_again_for_approval_Reason__c='Age has been changed';  
                                        opppayment.Invoice_Stage__c='Pending';  
                                        Payments.add(opppayment);
                                    }
                                  
                                }
                                
                            }
                            //FOR THE MIDDLE PAYMENTS 
                            else if(payment.Service_End_Date__c < opp.End_Date__c && opp.closedate != payment.Service_Begin_Date__c){
                                 Payment__c opppayment = new Payment__c();
                                 opppayment.id =payment.id;  
                                 opppayment.Payment_Amount__c = opportunityCatRateMap.get(opp.id).Price__c; 
                                 opppayment.Sending_again_for_approval_Reason__c='Age has been changed';  
                                 opppayment.Invoice_Stage__c='Pending';  
                                 Payments.add(opppayment);     
                            }
                            //FOR FIRST PAYMENT WHICH IS NOT LAST TOO
                            else if (payment.Service_Begin_Date__c==opp.closedate && payment.Service_End_Date__c !=opp.End_Date__c){
                                 Payment__c opppayment = new Payment__c();
                                 opppayment.id =payment.id;
                                 if(opp.closedate.day()!=1 && opportunityCatRateMap.get(opp.id).Prorate_First_Payment__c==true){
                                      Integer numberDays = Date.daysInMonth(opp.closedate.year(), opp.closedate.month());
                                       integer daysleft = (numberDays - opp.closedate.day())+1;
                                       if(daysleft!=0){
                                           decimal amountval =  (opportunityCatRateMap.get(opp.id).Price__c * daysleft)/numberDays; 
                                           opppayment.Payment_Amount__c = amountval;
                                       }    
                                 }
                                 else 
                                     opppayment.Payment_Amount__c = opportunityCatRateMap.get(opp.id).Price__c;
                                opppayment.Sending_again_for_approval_Reason__c='Age has been changed';  
                                opppayment.Invoice_Stage__c='Pending';  
                                Payments.add(opppayment);    
                            }
                        }
                                    
                    }
                }
                
                if(payments.size()>0)
                    update payments;
                    
                //SENDING FOR APPROVAL     
                set<id> updatedPaymentsOppids = new set<id>();  
                set<id> paymentids = new set<id>();  
                for( Payment__c payment : payments){
                    paymentids.add(payment.id); 
                } 
                system.debug('**Val of paymentids'+ paymentids);
               for(Payment__c payment : [select id,Opportunity__c from Payment__c where id in: paymentids]){
                   updatedPaymentsOppids.add(payment.Opportunity__c);
               }
                system.debug('ids of opp'+updatedPaymentsOppids);
               List<Approval.ProcessSubmitRequest> requests = new List<Approval.ProcessSubmitRequest>();    
               list<opportunity> opptobeUpdatedlist = [select id,stagename,recordtypeid from opportunity where id in: updatedPaymentsOppids];
                  system.debug('**opplistsize'+opptobeUpdatedlist.size());
               for(opportunity opp : opptobeUpdatedlist ){
                   Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
                   req.setComments('Submitting request because of age change of child');
                   req.setObjectId(opp.Id);
                   requests.add(req);               
               }
               system.debug('**VAl of requests'+requests);
               Approval.ProcessResult[] processResults = null;
               processResults = Approval.process(requests);
               system.debug('preoxess request'+processResults );
                     
            }
        }
        catch(Exception e){
            system.debug('**Val'+e);
        }        
                        
    }

}