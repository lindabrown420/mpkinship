public interface ITriggerHandler {

    /**
     * Called prior to BEFORE trigger. Use to cache any data required
     * input maps prior to execution of the BEFORE trigger
     */
    void bulkBefore();

    /**
     * Called prior to AFTER trigger. Use to cache any data required
     * input maps prior to execution of the AFTER trigger
     */
    void bulkAfter();

    /**
     * Handles before insert operations for all records being inserted.
     * Should only be used in BEFORE Trigger context/
     */
    void beforeInsert();

    /**
     * Handles before update operations for all records being updated.
     * Should only be used in BEFORE Trigger context/
     */
    void beforeUpdate();

    /**
     * Handles before delete operations for all records being deleted.
     * Should only be used in BEFORE Trigger context/
     */
    void beforeDelete();

    /**
     * Handles after insert operations for all records being inserted.
     * Should only be used in AFTER Trigger context/
     */
    void afterInsert();

    /**
     * Handles after update operations for all records being updated.
     * Should only be used in AFTER Trigger context/
     */
    void afterUpdate();

    /**
     * Handles after delete operations for all records being deleted.
     * Should only be used in AFTER Trigger context/
     */
    void afterDelete();

    /**
     * Called once all records have been processed by the trigger handler.
     * Use this to execute all final operations like DML of other records.
     */
    void andFinally();
}