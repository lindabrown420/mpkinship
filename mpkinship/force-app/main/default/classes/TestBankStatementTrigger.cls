@isTest
public class TestBankStatementTrigger {
    static testmethod void testmethod1(){
        account accountrec = new account();
        accountrec.Name='test vendor';
        accountrec.Tax_ID__c='333-33-3333';
        accountrec.Entity_Type__c='Individual';
        insert accountrec;
        
        id opprecType = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Staff_Operations').getRecordTypeId();
    	opportunity opprecord = new opportunity();
        opprecord.CloseDate=date.today();
        opprecord.StageName='Draft';
        opprecord.Name='test opportunity';        
        opprecord.Amount=20000;
        opprecord.AccountId=accountrec.id;
        opprecord.RecordTypeId=opprecType;
        insert opprecord;
        
         Bank_Statement__c BS = new Bank_Statement__c();
        BS.Name = 'BS k1';
        BS.Statement_Date__c = system.today();
        insert BS;
        
        id payrecType = Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Vendor_Invoice').getRecordTypeId();
        Payment__c payment = new Payment__c();
        payment.Opportunity__c=opprecord.id;
        payment.Payment_Amount__c=2000;
        payment.vendor__c = accountrec.Id;
        payment.RecordTypeId=payrecType;
        payment.Bank_Statement__c = BS.Id;
        insert payment;
        
        update BS;
        
    }
}