public class LoggerWrapper {
    public static void error(String message,Sobject record){
        Nebula.Logger.error(message,record);
        Nebula.Logger.saveLog();
    }
    public static void warn(String message,Sobject record){
        Nebula.Logger.warn(message,record);
        Nebula.Logger.saveLog();
    }
    public static void info(String message,Sobject record){
        Nebula.Logger.info(message,record);
        Nebula.Logger.saveLog();
    }
    public static void debug(String message,Sobject record){
        Nebula.Logger.debug(message,record);
        Nebula.Logger.saveLog();
    }
    public static void fine(String message,Sobject record){
        Nebula.Logger.fine(message,record);
        Nebula.Logger.saveLog();
    }
    public static void finer(String message,Sobject record){
        Nebula.Logger.finer(message,record);
        Nebula.Logger.saveLog();
    }
    public static void finest(String message,Sobject record){
        Nebula.Logger.finest(message,record);
        Nebula.Logger.saveLog();
    }
 	
}