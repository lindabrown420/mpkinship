@isTest
public class ActivePricebookTrgTest {
	 @testSetup
    static void testecords(){
        TriggerActivation__c ta = new TriggerActivation__c();
        ta.name='Pricebook Trigger';
        ta.isActive__c=true;
        insert ta;
        account acc = new account();
        acc.name='test account';
        acc.Tax_ID__c='222-33-4444';
        acc.Entity_Type__c='Individual';
        insert acc;
        
        Category__c category = new Category__c();
        category.Name='222';
        category.Category_Type__c='General Cases';
        category.recordtypeid=Schema.SObjectType.category__C.getRecordTypeInfosByDeveloperName().get('LASER_Category').getRecordTypeId();
        insert category;
        
        
    }   
    
    public static testmethod void test1(){
        list<TriggerActivation__c> talist=[select id,name from TriggerActivation__c where name='Pricebook Trigger'
                                          and isactive__c=true];
        list<Account> acclist=[select id,name from account];
        list<category__C> category =[select id,name from category__c];
        try{
            FIPS__c fp = new FIPS__c();
            fp.Is_Default__c=true;
            fp.Name='test fips';
            insert fp;
            campaign camp = new campaign();
            camp.recordtypeid= Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Fiscal_Year').getRecordTypeId();
            camp.Fiscal_Year_Type__c='Local';
            camp.StartDate=system.today().addmonths(-1);
            camp.Name='test camp';
            camp.FIPS__c = fp.id;
            camp.EndDate =system.today().addmonths(12);
            insert camp;
            pricebook2 pricebookrec = new pricebook2();
            pricebookrec.name='test pricebook';
            pricebookrec.Accounting_Period__c=camp.id;
            pricebookrec.Vendor__c=acclist[0].id;
            pricebookrec.IsActive=true;
            insert pricebookrec;
        }catch(Exception e){}    
    }
    
     public static testmethod void test2(){
        list<TriggerActivation__c> talist=[select id,name from TriggerActivation__c where name='Pricebook Trigger'
                                          and isactive__c=true];
        list<Account> acclist=[select id,name from account];
        list<category__C> category =[select id,name from category__c];
        try{
            FIPS__c fp = new FIPS__c();
            fp.Is_Default__c=true;
            fp.Name='test fips';
            insert fp;
            campaign camp = new campaign();
            camp.recordtypeid= Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Fiscal_Year').getRecordTypeId();
            camp.Fiscal_Year_Type__c='Local';
            camp.StartDate=system.today().addmonths(-1);
            camp.Name='test camp';
            camp.FIPS__c = fp.id;
            camp.EndDate =system.today().addmonths(12);
            insert camp;
            pricebook2 pricebookrec = new pricebook2();
            pricebookrec.name='test pricebook';
            pricebookrec.Accounting_Period__c=camp.id;
            pricebookrec.Vendor__c=acclist[0].id;
            pricebookrec.IsActive=false;
            insert pricebookrec;
            pricebookrec.isactive=true;
            update pricebookrec;
        }catch(Exception e){}    
    }
    public static testmethod void test3(){
        list<TriggerActivation__c> talist=[select id,name from TriggerActivation__c where name='Pricebook Trigger'
                                          and isactive__c=true];
        list<Account> acclist=[select id,name from account];
        list<category__C> category =[select id,name from category__c];
        try{
            FIPS__c fp = new FIPS__c();
            fp.Is_Default__c=true;
            fp.Name='test fips';
            insert fp;
            campaign camp = new campaign();
            camp.recordtypeid= Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Fiscal_Year').getRecordTypeId();
            camp.Fiscal_Year_Type__c='Local';
            camp.StartDate=system.today().addmonths(-1);
            camp.Name='test camp';
            camp.FIPS__c = fp.id;
            camp.EndDate =system.today().addmonths(12);
            insert camp;
            pricebook2 pricebookrec = new pricebook2();
            pricebookrec.name='test pricebook';
            pricebookrec.Accounting_Period__c=camp.id;
            pricebookrec.Vendor__c=acclist[0].id;
            pricebookrec.IsActive=true;
            insert pricebookrec;
           
            pricebook2 pricebookrec1 = new pricebook2();
            pricebookrec1.name='test pricebook';
            pricebookrec1.Accounting_Period__c=camp.id;
            pricebookrec1.Vendor__c=acclist[0].id;
            pricebookrec1.IsActive=false;
            insert pricebookrec1;
            pricebookrec1.IsActive=true;
            update pricebookrec1;
        }catch(Exception e){}    
    }
}