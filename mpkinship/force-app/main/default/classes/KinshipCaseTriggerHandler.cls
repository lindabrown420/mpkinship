public class KinshipCaseTriggerHandler extends TriggerHandler{
    public static Boolean bypassTrigger = false;
    
    /**
     * Handles before insert operations for all records being inserted.
     * Should only be used in BEFORE Trigger context
     */
    public override void beforeInsert() {
        if(KinshipCaseTriggerHandler.bypassTrigger) return;
        List<Kinship_Case__c> lstNew = (List<Kinship_Case__c>)Trigger.new;
        for(Kinship_Case__c c : lstNew) {
            if(c.Race__c == '6') {
                c.Hispanic_Ethnicity__c = '2';
            } else {
                c.Hispanic_Ethnicity__c = '1';
            }
        }
    }

    /**
     * Handles before update operations for all records being updated.
     * Should only be used in BEFORE Trigger context
     */
    public override void beforeUpdate() {
        if(KinshipCaseTriggerHandler.bypassTrigger) return;
        List<Kinship_Case__c> lstNew = (List<Kinship_Case__c>)Trigger.new;
        for(Kinship_Case__c c : lstNew) {
            if(c.Race__c == '6') {
                c.Hispanic_Ethnicity__c = '2';
            } else {
                c.Hispanic_Ethnicity__c = '1';
            }
        }
    }
}