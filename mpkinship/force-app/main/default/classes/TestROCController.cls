@isTest
public class TestROCController {
    
     static testmethod void testmethod1(){
        
        TriggerActivation__c  ta = new TriggerActivation__c();
        ta.name='PurchaseOrder Trigger';
        ta.isActive__c=false;
        insert ta;
        
        
         TriggerActivation__c  ta1 = new TriggerActivation__c();
        ta1.name='UpdateCSACoordinatorSign';
        ta1.isActive__c=false;
        insert ta1;  
        
        account accountrec = new account();
        accountrec.Name='test vendor';
        accountrec.Tax_ID__c='333-33-3333';
        accountrec.Entity_Type__c='Individual';
        insert accountrec;
        
        id catrecType = Schema.SObjectType.Category__c.getRecordTypeInfosByName().get('LASER Category').getRecordTypeId();
        
         Locality_Account__c la = new Locality_Account__c();
        la.name='33';
        insert la;
         
        string catType = SObjectType.Category__c.Fields.Category_Type__c.PicklistValues[0].getValue(); 
        
        Kinship_Check__c Check = new Kinship_Check__c();
        
        Special_Welfare_Account__c swa = new Special_Welfare_Account__c();
         
        swa.Name ='SWA';
        insert swa;
         
         
        Category__c category = new Category__c();
        category.Name='222';
        category.Category_Type__c= catType;
        category.RecordTypeId = catrecType;
        category.Locality_Account__c=la.id;
        insert category;
        
         fips__c fip= new fips__c();
        fip.name = 'Alleghany';
        insert fip;
         
        Campaign parent = new Campaign();
        parent.name = '2021';
        parent.StartDate = Date.Today().addDays(-10);
        parent.EndDate = Date.Today().addDays(10);
        parent.Fiscal_Year_Type__c = 'DSS';
        parent.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId();
        insert parent;
        id recType = Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('LASER_Reporting_Period').getRecordTypeId();
        campaign camp = new campaign();
        camp.name = 'May';
        camp.recordtypeid = recType;
        camp.Begin_Date__c=system.today();
        camp.EndDate=system.today().adddays(2);
        camp.parentId = parent.id;
        camp.FIPS__c =  fip.id;
        insert camp;
        
        
        
        
         
        Cost_Center__c cc = new Cost_Center__c();
        cc.Name='test';
        insert cc;
        Chart_of_Account__c ca = new Chart_of_Account__c();
        ca.Locality_Account__c=la.id;
        ca.FIPS__c=fip.id;
        ca.Cost_Center__c=cc.id;
        insert ca;
         
        id opprecType = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Staff_Operations').getRecordTypeId();
        opportunity opprecord = new opportunity();
        opprecord.CloseDate=date.today();
        opprecord.End_Date__c=date.today();
        opprecord.StageName='Draft';
        opprecord.Name='test opportunity';        
        opprecord.Amount=20000;
        opprecord.AccountId=accountrec.id;
        opprecord.Accounting_Period__c = camp.id;
        opprecord.RecordTypeId=opprecType;
        opprecord.Category__c = category.Id;
        insert opprecord;
        opprecord.ROC_Id__c = opprecord.Id;
        update opprecord;
        id ROCType = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Report_of_Collection').getRecordTypeId();
        opportunity ROCrecord = new opportunity();
        ROCrecord.CloseDate=date.today();
        ROCrecord.End_Date__c=date.today();
        ROCrecord.StageName='Draft';
        ROCrecord.Name='test opportunity';        
        ROCrecord.Amount=20000;
        ROCrecord.AccountId=accountrec.id;
        ROCrecord.Accounting_Period__c = camp.id;
        ROCrecord.RecordTypeId=ROCType;
        ROCrecord.Category__c = category.Id;
        insert ROCrecord;
        
        id payrecType = Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Receipt').getRecordTypeId();
        Payment__c payment = new Payment__c();
        payment.Opportunity__c=ROCrecord.id;
        payment.Payment_Amount__c=2000;
        payment.vendor__c = accountrec.Id;
        payment.Payment_Date__c = date.today();
        payment.RecordTypeId=payrecType;
        payment.Category_lookup__c = category.Id;
        payment.Paid__c = true;
        insert payment;
        Payment__c payment1 = new Payment__c();
        payment1.Opportunity__c=opprecord.id;
        payment1.Payment_Amount__c=2000;
        payment1.vendor__c = accountrec.Id;
        payment1.Payment_Date__c = System.today();
        payment1.RecordTypeId=payrecType;
        payment1.Category_lookup__c = category.Id;
        insert payment1; 
         
        id payrecType1 = Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Vendor_Refund').getRecordTypeId();
        Payment__c payment11 = new Payment__c();
        payment11.Opportunity__c=opprecord.id;
        payment11.Payment_Amount__c=2000;
        payment11.vendor__c = accountrec.Id;
        payment11.Payment_Date__c = System.today();
        payment11.RecordTypeId=payrecType1;
        payment11.Category_lookup__c = category.Id;
        insert payment11;
       
        Payment__c payment12 = new Payment__c();
        payment12.Opportunity__c=opprecord.id;
        payment12.Payment_Amount__c=2000;
        payment12.vendor__c = accountrec.Id;
        payment12.Payment_Date__c = System.today();
        payment12.RecordTypeId=payrecType1;
        payment12.Category_lookup__c = category.Id;
         
        insert payment12; 
         contact con = new contact();
        con.LastName='contactrec';
        con.FirstName='test';
        con.Email='test@contact.com';        
        insert con;   
        
        Kinship_Case__c caserec = new Kinship_Case__c();
        caserec.Primary_Contact__c=con.id;
        insert caserec;
         
        Claims__c cl = new Claims__c();
        cl.Name = 'test';
        cl.Claim_Type__c = 'ADMINISTRATIVE';
        cl.Payable_to__c = accountrec.Id;
        cl.Claim_Amount__c = 123;
        cl.Case__c = caserec.Id;
        insert cl;
        
        Test.startTest();
         
        Id claimsId = cl.Id;
        Id contactId = con.Id;
        List<String> ReceiptsIDs = new List<String>();
        ReceiptsIDs.add(payment.Id) ;
        String VendorRefundID;
         
         try{
			ROCController.cloneROC('a12345678912589');             
         }catch(Exception e){}
         
        ROCController cont = new ROCController();
        ROCController.checkPayable(claimsId);
        ROCController.getClaim(contactId);
        ROCController.getReceiptsSumbyID(ReceiptsIDs); 
        ROCController.UpdateReceiptsbyVendorRefund(VendorRefundId, ReceiptsIDs);
        ROCController.getChecksbyOpportunityId(opprecord.Id);
        ROCController.getdefaultFiscalYear();
        ROCController.GetSWCategory();
        ROCController.updateFipsCodeinInvoices(payment.Id);
         //ROCController.updateFipsCodeinInvoices(payment.Id +','+payment12.Id);
        //ROCController.getPaymentsbyCheckID(opprecord.Id, Check.Id);
		ROCController.getReceiptsFromROC(opprecord.Id);
        ROCController.getVRefundsFromROC(opprecord.Id, payment.Id);
        ROCController.getReceiptRecord(payment.Id);
        ROCController.getROCRecord(opprecord.Id);
        ROCController.isAmountsEqual(opprecord.Id);
        ROCController.markCompleteStage(opprecord.Id);
        ROCController.markOpenStage(opprecord.Id);
        ROCController.deleteReceipt(payment.Id);
        ROCController.getCategoryType(category.Id);
        ROCController.getCategoryName(category.Id);
        ROCController.checkFundingSource('a12345678912589');
        ROCController.getrecordTypeId(); 
        ROCController.getPaymentsbyCheckID(payment.Id, Check.Id);
        ROCController.getDatefromAccountingPeriod('a12345678912589');
        ROCController.checkPaidCaseAction(null);
        ROCController.getCaseId(swa.Id, swa.Name);
        ROCController.getCostCenter(category.Id);
         ROCController.getAdjustmentsFromROC(opprecord.Id);
         try{
         ROCController.getReceipts(ROCrecord.Id);
        }catch(Exception e){}
         try{
         ROCController.isReceiptNumberEsixt('1', opprecord.Id, payment.Id);
         
         }catch(Exception e){}
        try{
         ROCController.checkReceiptRange(1,2, opprecord.Id);
        }catch(Exception e){} 
        try{   
		 ROCController.deleteROC(ROCrecord.Id);
            
        }catch(Exception e){}  
           /*
        
        ROCController.getROCRecord(Report.Id);
        ROCController.isAmountsEqual(Report.Id);
        ROCController.markCompleteStage(Report.Id);
        ROCController.markOpenStage(Report.Id);
        ROCController.isReceiptNumberEsixt(rep.Report_Number__c, Report.Id);
        ROCController.deleteReceipt(rep.id);
        ROCController.checkReceiptRange(101,300,  Report.id);*/
        Test.stopTest();
    }
    static testmethod void testmethod2(){
        
        TriggerActivation__c  ta = new TriggerActivation__c();
        ta.name='PurchaseOrder Trigger';
        ta.isActive__c=false;
        insert ta;
        
        
         TriggerActivation__c  ta1 = new TriggerActivation__c();
        ta1.name='UpdateCSACoordinatorSign';
        ta1.isActive__c=false;
        insert ta1;  
        
        account accountrec = new account();
        accountrec.Name='test vendor';
        accountrec.Tax_ID__c='333-33-3333';
        accountrec.Entity_Type__c='Individual';
        insert accountrec;
        
        id catrecType = Schema.SObjectType.Category__c.getRecordTypeInfosByName().get('LASER Category').getRecordTypeId();
        
         Locality_Account__c la = new Locality_Account__c();
        la.name='33';
        insert la;
         
        string catType = SObjectType.Category__c.Fields.Category_Type__c.PicklistValues[0].getValue(); 
        
        Kinship_Check__c Check = new Kinship_Check__c();
         
         
         
        Category__c category = new Category__c();
        category.Name='222';
        category.Category_Type__c= catType;
        category.RecordTypeId = catrecType;
        category.Locality_Account__c=la.id;
        insert category;
        
         fips__c fip= new fips__c();
        fip.name = 'Alleghany';
        insert fip;
         
        Campaign parent = new Campaign();
        parent.name = '2021';
        parent.StartDate = Date.Today().addDays(-10);
        parent.EndDate = Date.Today().addDays(10);
        parent.Fiscal_Year_Type__c = 'DSS';
        parent.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId();
        insert parent;
        id recType = Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('LASER_Reporting_Period').getRecordTypeId();
        campaign camp = new campaign();
        camp.name = 'May';
        camp.recordtypeid = recType;
        camp.Begin_Date__c=system.today();
        camp.EndDate=system.today().adddays(2);
        camp.parentId = parent.id;
        camp.FIPS__c =  fip.id;
        insert camp;
        
        
        
        
         
        Cost_Center__c cc = new Cost_Center__c();
        cc.Name='test';
        insert cc;
        Chart_of_Account__c ca = new Chart_of_Account__c();
        ca.Locality_Account__c=la.id;
        ca.FIPS__c=fip.id;
        ca.Cost_Center__c=cc.id;
        insert ca;
         
        id opprecType = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Staff_Operations').getRecordTypeId();
        opportunity opprecord = new opportunity();
        opprecord.CloseDate=date.today();
        opprecord.End_Date__c=date.today();
        opprecord.StageName='Draft';
        opprecord.Name='test opportunity';        
        opprecord.Amount=20000;
        opprecord.AccountId=accountrec.id;
        opprecord.Accounting_Period__c = camp.id;
        opprecord.RecordTypeId=opprecType;
        opprecord.Category__c = category.Id;
        insert opprecord;
        opprecord.ROC_Id__c = opprecord.Id;
        update opprecord;
        id ROCType = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Report_of_Collection').getRecordTypeId();
        opportunity ROCrecord = new opportunity();
        ROCrecord.CloseDate=date.today();
        ROCrecord.End_Date__c=date.today();
        ROCrecord.StageName='Draft';
        ROCrecord.Name='test opportunity';        
        ROCrecord.Amount=20000;
        ROCrecord.AccountId=accountrec.id;
        ROCrecord.Accounting_Period__c = camp.id;
        ROCrecord.RecordTypeId=ROCType;
        ROCrecord.Category__c = category.Id;
        insert ROCrecord;
        
        id payrecType = Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Receipt').getRecordTypeId();
        Payment__c payment = new Payment__c();
        payment.Opportunity__c=ROCrecord.id;
        payment.Payment_Amount__c=2000;
        payment.vendor__c = accountrec.Id;
        payment.Payment_Date__c = date.today();
        payment.RecordTypeId=payrecType;
        payment.Category_lookup__c = category.Id;
        payment.Paid__c = true;
        insert payment;
        Payment__c payment1 = new Payment__c();
        payment1.Opportunity__c=opprecord.id;
        payment1.Payment_Amount__c=2000;
        payment1.vendor__c = accountrec.Id;
        payment1.Payment_Date__c = System.today();
        payment1.RecordTypeId=payrecType;
        payment1.Category_lookup__c = category.Id;
        insert payment1; 
         
        id payrecType1 = Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Vendor_Refund').getRecordTypeId();
        Payment__c payment11 = new Payment__c();
        payment11.Opportunity__c=opprecord.id;
        payment11.Payment_Amount__c=2000;
        payment11.vendor__c = accountrec.Id;
        payment11.Payment_Date__c = System.today();
        payment11.RecordTypeId=payrecType1;
        payment11.Category_lookup__c = category.Id;
        insert payment11;
       
        Payment__c payment12 = new Payment__c();
        payment12.Opportunity__c=opprecord.id;
        payment12.Payment_Amount__c=2000;
        payment12.vendor__c = accountrec.Id;
        payment12.Payment_Date__c = System.today();
        payment12.RecordTypeId=payrecType1;
        payment12.Category_lookup__c = category.Id;
         
        insert payment12; 
        
        Test.startTest();
        List<String> ReceiptsIDs = new List<String>();
        ReceiptsIDs.add(payment.Id) ;
        String VendorRefundID;
         
         try{
			ROCController.cloneROC(payment.Id);             
         }catch(Exception e){}
         
        ROCController cont = new ROCController();
        ROCController.getReceiptsSumbyID(ReceiptsIDs); 
        ROCController.UpdateReceiptsbyVendorRefund(VendorRefundId, ReceiptsIDs);
        ROCController.getChecksbyOpportunityId(opprecord.Id);
        ROCController.getdefaultFiscalYear();
        ROCController.GetSWCategory();
        ROCController.updateFipsCodeinInvoices(payment.Id);
         //ROCController.updateFipsCodeinInvoices(payment.Id +','+payment12.Id);
        //ROCController.getPaymentsbyCheckID(opprecord.Id, Check.Id);
		ROCController.getReceiptsFromROC(opprecord.Id);
        ROCController.getVRefundsFromROC(opprecord.Id, payment.Id);
        ROCController.getReceiptRecord(payment.Id);
        ROCController.getROCRecord(opprecord.Id);
        ROCController.isAmountsEqual(opprecord.Id);
        ROCController.markCompleteStage(opprecord.Id);
        ROCController.markOpenStage(opprecord.Id);
        ROCController.deleteReceipt(payment.Id);
        ROCController.getCategoryType(category.Id);
        ROCController.getCategoryName(category.Id);
        ROCController.checkFundingSource(category.Id);
        ROCController.getrecordTypeId(); 
        ROCController.getDatefromAccountingPeriod(camp.id);
        ROCController.checkPaidCaseAction(opprecord.Id);
        ROCController.getReceiptInfo(opprecord.Id);
        
        //ROCController.getvRefundFromROC(opprecord.Id);
        ROCController.getCostCenter(category.Id);
         try{
         ROCController.getReceipts(ROCrecord.Id);
        }catch(Exception e){}
         try{
         ROCController.isReceiptNumberEsixt('1', opprecord.Id, payment.Id);
         
         }catch(Exception e){}
        try{
         ROCController.checkReceiptRange(1,2, opprecord.Id);
        }catch(Exception e){} 
        try{   
		 ROCController.deleteROC(ROCrecord.Id);
            
        }catch(Exception e){}  
        Test.stopTest();   
    }
}