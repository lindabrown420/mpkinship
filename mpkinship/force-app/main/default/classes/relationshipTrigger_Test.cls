@isTest
public class relationshipTrigger_Test {
    
    public static testMethod void testMethod1(){
        Contact cont = new Contact(LastName='Test');
        insert cont;
        
        Contact cont1 = new Contact(LastName='Test1');
        insert cont1;
        
        List<Relationship__c> relList = new List<Relationship__c>();
        Relationship__c r = createRelationship(cont, cont1);
        r.Type__c = 'Parent';
		relList.add(r);
		Relationship__c r1 = createRelationship(cont, cont1);
        r1.Type__c = 'Child';
		relList.add(r1);
		Relationship__c r2 = createRelationship(cont, cont1);
        r2.Type__c = 'Grandparent';
		relList.add(r2);
		Relationship__c r3 = createRelationship(cont, cont1);
        r3.Type__c = 'Grandchild';
		relList.add(r3);
		Relationship__c r4 = createRelationship(cont, cont1);
        r4.Type__c = 'Employer';
		relList.add(r4);
		Relationship__c r5 = createRelationship(cont, cont1);
        r5.Type__c = 'Employee';
		relList.add(r5);
		Relationship__c r6 = createRelationship(cont, cont1);
        r6.Type__c = 'Husband';
		relList.add(r6);
		Relationship__c r7 = createRelationship(cont, cont1);
        r7.Type__c = 'Wife';
		relList.add(r7);
		Relationship__c r8 = createRelationship(cont, cont1);
        r8.Type__c = 'Family';
		relList.add(r8);        
        insert relList;
        
        relationshipViewer.getContactWithRelationship(cont1.id);
        
        delete relList;
    }
    
    public static Relationship__c createRelationship(Contact cont, Contact cont1){
        Relationship__c r = new Relationship__c();
        r.Contact__c = cont.id;
        r.Related_Contact__c = cont1.id;
        r.status__c = 'Current';
        r.Is_Reciprocal_Relationship__c = true;        
        r.Is_Reciprocal_Relationship__c = false;
        
        return r;
    }

}