public class KinshipHistoryController {
    @AuraEnabled
    public static List<HistoryWrapper> getListHistory(String strId){
        List<HistoryWrapper> result = new List<HistoryWrapper>();
        for(Kinship_History__c his : [SELECT Id, Name, Action__c, Object_Id__c, Object_Name__c, 
                                    Field_Name__c, New_Value__c, Old_Value__c , CreatedBy.Name, CreatedDate
                                    FROM Kinship_History__c
                                    WHERE Object_Id__c = :strId
                                    ORDER BY CreatedDate DESC]) {
            HistoryWrapper temp = new HistoryWrapper();
            temp.fieldName = his.Field_Name__c;
            temp.oldValue = his.Old_Value__c;
            temp.newValue = his.New_Value__c;
            temp.action = his.Action__c;
            temp.createdBy = his.CreatedBy.Name;
            temp.id = his.id;
            temp.createdDate = his.CreatedDate;
            result.add(temp);
        }
        return result;
    }

    public class HistoryWrapper{
        @AuraEnabled
        public String id{get;set;}
        @AuraEnabled
        public String fieldName{get;set;}
        @AuraEnabled
        public String oldValue{get;set;}
        @AuraEnabled
        public String newValue{get;set;}
        @AuraEnabled
        public String action{get;set;}
        @AuraEnabled
        public String createdBy{get;set;}
        @AuraEnabled
        public Datetime createdDate{get;set;}
    }
}