public class AddCategoryController {
	@AuraEnabled(continuation = true)       
    public static List<Chart_of_Account__c> getCOA(String strId){
        return [SELECT Id, Name, Cost_Center__c, Locality_Account__c, FIPS__c FROM Chart_of_Account__c WHERE Locality_Account__c = :strId];
    } 
}