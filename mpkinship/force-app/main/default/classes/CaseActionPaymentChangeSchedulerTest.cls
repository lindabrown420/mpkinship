@IsTest
public class CaseActionPaymentChangeSchedulerTest{
    public static testmethod void testschedular(){
        Test.startTest();
        Datetime dt = Datetime.now().addMinutes(1);
        String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
        
            String jobId = System.schedule('ScheduleApexClassTest',
                                           CRON_EXP, 
                                           new CaseActionPaymentChangeScheduler());
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                              FROM CronTrigger WHERE id = :jobId];
            System.assertEquals(CRON_EXP, 
                                ct.CronExpression);
            
            Test.stopTest();   
        
    }  

}