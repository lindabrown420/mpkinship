/***REMAINING PAYMENTS WRITEOFF CODE CLASS ***/
public class PaymentTriggerLineItemHandler{
    public static boolean isItemPayment=false;
   
    public static void updatePayments(set<id> paymentids){
        system.debug('**enterd in updatepayments');
        isItemPayment=true;

        set<id> opportunityids = new set<id>();  
        //GET PAID PAYMENTS OPPORTUNITIES IDS
        list<Payment__c> paymentsrecs =[select id,Paid__c,Amount_Paid__c,Invoice_Stage__c,Payment_Amount__c,Opportunity__c 
                                              from Payment__c where id IN: paymentids];
        for(Payment__c payment : paymentsrecs){
            opportunityids.add(payment.Opportunity__c);
        } 
        
        list<Payment__c> payments=[select id,(select id,Quantity_Billed__c,Billed_Amount__c,LineItem_Amount_Billed__c from Invoice_Line_Items__r) from Payment__c where Opportunity__c in:opportunityids
                                            and Paid__c=true and Payment_Amount__c!=null and Invoice_Stage__c='Paid'];
        system.debug('**Val of payments'+ payments);
        system.debug('**payment size'+payments.size());
        
        map<id,decimal> paymentAmountTotalMap = new map<id,decimal>();
        for(Payment__c payment : payments){
            system.debug('**Val of payment'+ payment);
            decimal totalamount =0;
            for(Payment_Line_Item__c ilitem : payment.Invoice_Line_Items__r){
                system.debug('**ilietem vla'+ ilitem);
                //if(ilitem.Billed_Amount__c!=null && ilitem.Quantity_Billed__c!=null &&  ilitem.Quantity_Billed__c!=0){
                 if(ilitem.LineItem_Amount_Billed__c!=null && ilitem.Quantity_Billed__c!=null &&  ilitem.Quantity_Billed__c!=0){
                
                    system.debug('**enterd in if');
                    totalamount +=ilitem.LineItem_Amount_Billed__c.setscale(2);
                }
            }
            
            system.debug('** totalamount'+ totalamount);
            if(!paymentAmountTotalMap.containskey(payment.id) && totalamount!=0){
                system.debug('totalamount'+ totalamount);
                system.debug('paymentid'+ payment.id);
                system.debug('**Entered here in paymenatmountmap'+ payment);
                paymentAmountTotalMap.put(payment.id,totalamount); 
            }
        
        }  
        system.debug('**paymentamounttotalmap sieze'+ paymentAmountTotalMap.size());
        system.debug('**paymentamounttotalmap'+ paymentAmountTotalMap);
         set<id> opportunityIdsWhosePaymenttoCancel = new set<id>();
         //GET ALL PAYMENTS HAVING PAID TRUE TO CALCULATE IF TOTAL AMOUNT OF PAYMENTS EQUALS TO OPPORTUNITY AMOUNT
        //ADDING RECORDTYPE TO WORK ONLY FOR POSO 
        id posorectypeid = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Purchase_Order').getRecordTypeId();
        list<opportunity> opportunities =[select id,amount,(Select id,Paid__c,Amount_Paid__c,Payment_Amount__c,Invoice_Stage__c from Payment_OppPayment__r
                                          where Paid__c=true and Payment_Amount__c!=null and Invoice_Stage__c='Paid') from opportunity where id in: opportunityids
                                          and recordtypeid=:posorectypeid];
        for(opportunity opp : opportunities){
              decimal totalamount =0; boolean isFull=false;
            for(Payment__c payment : opp.Payment_OppPayment__r){
                if(paymentAmountTotalMap.containskey(payment.id)){
                    totalamount +=paymentAmountTotalMap.get(payment.id).setscale(2);
                    system.debug('**Val oftotal maount'+ totalamount);
                    system.debug('**Val of opp amount'+ opp.amount);
                    if(totalamount ==opp.amount){
                        isFull=true;
                        break;
                    }
                }               
            } 
            if(isFull==true)
                opportunityIdsWhosePaymenttoCancel.add(opp.id);          
        }  //END OF FOR LOOP
        
        system.debug('**Val of paymentcancel'+opportunityIdsWhosePaymenttoCancel);
         //FOR WRITE OFF PAYMENTS
        list<Payment__c> paymentsToUpdate = new list<Payment__c>();
        list<Payment__c> paymentlist=[select id,Written_Off__c,Invoice_Stage__c from Payment__c where 
                                   Paid__c=false and Written_Off__c=false and Opportunity__c
                                   IN:opportunityIdsWhosePaymenttoCancel];
        system.debug('**Val of paymentlist to writeoff'+ paymentlist.size());                           
        for(Payment__c paymentrec : paymentlist){
             Payment__c payment = new Payment__c();
             payment.id= paymentrec.id;
             payment.Written_Off__c =true; 
             payment.Invoice_Stage__c='Write off';
             paymentsToUpdate.add(payment);   
        }
        system.debug('**size of paymentstoupdate'+paymentsToUpdate);
        if(paymentsToUpdate.size()>0)
            update paymentsToUpdate;                         
                                                                      
    }
}