public with sharing class ServicePlanGoals {

    @AuraEnabled
    public static List<Goal__c> getGoalRecords(string recId){
        system.debug(recId);
        if(recId != null && recId != ''){
            return [Select id,Service_Plan__r.Case__c                
                    FROM Goal__c 
                    WHERE Service_Plan__c =: recId];
        }
        else{
            return null;
        }
    }

    @AuraEnabled
    public static Goal__c getGoalRecord(string recId){
        system.debug(recId);
        if(recId != null && recId != ''){
            return [Select id,Name,Status__c,Goal_Template__c,Goal_Template__r.Name,
                    (Select id,Name,Due_Date__c,Case__c,Case__r.Name,Status__c,Action_Item_Template__c,Action_Item_Template__r.Name from Action_Items__r) 
                    FROM Goal__c 
                    WHERE Id =: recId][0];
        }
        else{
            return null;
        }
    }

    @AuraEnabled
    public static string deleteRecords(string recId, string objectName){
        if(objectName == 'Goal'){
            Goal__c g = [Select id from Goal__c where id =: recId];
            delete g;
        }
        else{
            Action_Item__c ai = [Select id from Action_Item__c where id =: recId];
            delete ai;
        }
        
        return null;
    }

    @AuraEnabled
    public static Goal__c saveRecord(String jsonString, String servicePlanId, String goalName, String goalId, String goalStatus, String goalTemplateId){
        Goal__c g = new Goal__c(Id=goalId, Name=goalName, Status__c=goalStatus, Service_Plan__c=servicePlanId);
        if(goalTemplateId != null){
            g.Goal_Template__c = goalTemplateId;
            g.Name = [Select id,name from Goal_Template__c where id =:goalTemplateId ].Name;
        }
        system.debug(g);
        upsert g;

        List<Action_Item__c> objectiveList = (List<Action_Item__c>)JSON.deserialize(jsonString,List<Action_Item__c>.class);
        Set<String> objTempIds = new Set<String>();
        for(Action_Item__c obj: objectiveList){
            system.debug(obj);
            obj.Service_Plan__c = servicePlanId;
            //obj.Case__c = caseId;
            objTempIds.add(obj.Action_Item_Template__c);
            if(goalId == null || goalId == '')
                obj.Goal__c = g.Id;
        }
        Map<Id,Action_Item_Template__c> objTempMap = new Map<Id,Action_Item_Template__c>([Select Id,Name from Action_Item_Template__c where ID IN: objTempIds]);
        for(Action_Item__c obj: objectiveList){
            if(objTempMap.containsKey(obj.Action_Item_Template__c)){
                obj.Name = objTempMap.get(obj.Action_Item_Template__c).Name;
            }
        }
        system.debug('objectiveList: '+objectiveList);
        upsert objectiveList;
        return getGoalRecord(g.Id);

    }

    @AuraEnabled
    public static List<Goal_Template__c> getGoalTemplates(string recId){
        IFSP__c sp = [Select id, Case__c from IFSP__c where Id =: recId];
        system.debug('sp: '+sp);
        List<Program_Engagement__c> listProgEng = [Select id,Programs__c from Program_Engagement__c where Case__c =: sp.Case__c];
        system.debug('listProgEng: '+listProgEng);
        Set<String> progIds = new Set<String>();

        for(Program_Engagement__c pe: listProgEng){
            progIds.add(pe.Programs__c);
        }
        system.debug('progIds: '+progIds);
        List<Goal_Template__c> listGoalTemplate = [Select id,name from Goal_Template__c where Programs__c IN: progIds];
        system.debug('listGoalTemplate: '+listGoalTemplate);
        return listGoalTemplate;
    }

    @AuraEnabled
    public static List<Goal_Template__c> getGoalTemplatesCase(string recId){

        List<Program_Engagement__c> listProgEng = [Select id,Programs__c from Program_Engagement__c where Case__c =: recId];
        system.debug('listProgEng: '+listProgEng);
        Set<String> progIds = new Set<String>();

        for(Program_Engagement__c pe: listProgEng){
            progIds.add(pe.Programs__c);
        }
        system.debug('progIds: '+progIds);
        List<Goal_Template__c> listGoalTemplate = [Select id,name from Goal_Template__c where Programs__c IN: progIds];
        system.debug('listGoalTemplate: '+listGoalTemplate);
        return listGoalTemplate;
    }

    @AuraEnabled
    public static List<Action_Item_Template__c> getObjectiveTemplates(string recId, string gtId){
        system.debug(recId);   
        Goal__c g;
        if(recId != null && recId != ''){
            g =  [Select id,Name,Status__c,Goal_Template__c
                    FROM Goal__c 
                    WHERE Id =: recId][0];
        }
        
        List<Goal_Action_Item_Template__c> goalObjectiveList = new List<Goal_Action_Item_Template__c>();

        if(g != null){
            if(g.Goal_Template__c != null){
                goalObjectiveList = [Select id,Action_Item_Template__c from Goal_Action_Item_Template__c where Goal_Template__c =: g.Goal_Template__c];
            }
            else{
                return null;
            }            
        }
        else{
            goalObjectiveList = [Select id,Action_Item_Template__c from Goal_Action_Item_Template__c where Goal_Template__c =: gtId];
        }      

        Set<String> objTempIds = new Set<String>();
        for(Goal_Action_Item_Template__c goalObj: goalObjectiveList){
            objTempIds.add(goalObj.Action_Item_Template__c);
        }

        List<Action_Item_Template__c> objTempList = [Select Id,Name from Action_Item_Template__c where ID IN: objTempIds];
        return objTempList.size()>0 ? objTempList : null;
        
    }


}