@isTest
public class AddServiceNamesToLaserCodesCtrTest {
    public static testmethod void test1(){
        
    	LASER_Account_Codes__c lac = new LASER_Account_Codes__c();
        lac.Account_Title__c='test lac';
        lac.Included_for_IVE_Categories__c=true;
        lac.Name='222';
        insert lac;
        
        LASER_Cost_Code__c lc = new LASER_Cost_Code__c();
        lc.Name='44';
        insert lc;
         Service_Names_Code__c sc = new Service_Names_Code__c();
         sc.name='test';
         sc.LASER_Account_Code__c=lac.Id;
         sc.Service_Name_Code__c='2';
         insert sc;  
         Category__c category = new Category__c();
        category.Name='222';
        category.LASER_Cost_Code__c=lc.id;
        category.Category_Type__c='IVE';
        category.RecordTypeId= Schema.SObjectType.category__C.getRecordTypeInfosByDeveloperName().get('LASER_Category').getRecordTypeId();
        insert category; 
        AddServiceNamesToLaserCodesCtr.getLaserAccountCodes(category.id);
        AddServiceNamesToLaserCodesCtr.UpdateLaserAccountCode(category.id,'22','333');
    }
     public static testmethod void test2(){
        
    	LASER_Account_Codes__c lac = new LASER_Account_Codes__c();
        lac.Account_Title__c='test lac';
        lac.Included_for_IVE_Categories__c=true;
        lac.Name='222';
        insert lac;
        
        LASER_Cost_Code__c lc = new LASER_Cost_Code__c();
        lc.Name='44';
        insert lc;
         Service_Names_Code__c sc = new Service_Names_Code__c();
         sc.name='test';
         sc.LASER_Account_Code__c=lac.Id;
         sc.Service_Name_Code__c='2';
         insert sc;  
         Category__c category = new Category__c();
        category.Name='222';
        category.LASER_Cost_Code__c=lc.id;
        category.Category_Type__c='IVE';
         category.LASER_Account_Code__c=lac.id;
        category.RecordTypeId= Schema.SObjectType.category__C.getRecordTypeInfosByDeveloperName().get('LASER_Category').getRecordTypeId();
        insert category; 
        AddServiceNamesToLaserCodesCtr.getLaserAccountCodes(category.id);
        AddServiceNamesToLaserCodesCtr.UpdateLaserAccountCode(category.id,'222','33');
         AddServiceNamesToLaserCodesCtr.UpdateCategory(category.id, '2','222');
    }
     public static testmethod void test3(){
        
    	LASER_Account_Codes__c lac = new LASER_Account_Codes__c();
        lac.Account_Title__c='test lac';
        lac.Included_for_IVE_Categories__c=true;
        lac.Name='222';
        insert lac;
        
        LASER_Cost_Code__c lc = new LASER_Cost_Code__c();
        lc.Name='44';
        insert lc;
         Service_Names_Code__c sc = new Service_Names_Code__c();
         sc.name='test';
         sc.LASER_Account_Code__c=lac.Id;
         sc.Service_Name_Code__c='2';
         insert sc;  
         Category__c category = new Category__c();
        category.Name='222';
        category.LASER_Cost_Code__c=lc.id;
        category.Category_Type__c='IVE';
         category.LASER_Account_Code__c=lac.id;
        category.RecordTypeId= Schema.SObjectType.category__C.getRecordTypeInfosByDeveloperName().get('LASER_Category').getRecordTypeId();
        insert category; 
         Category__c category1 = new Category__c();
        category1.Name='222';
        category1.Category_Type__c='IVE';
         category1.LASER_Account_Code__c=lac.id;
        category1.RecordTypeId= Schema.SObjectType.category__C.getRecordTypeInfosByDeveloperName().get('LASER_Category').getRecordTypeId();
        insert category1; 
        AddServiceNamesToLaserCodesCtr.UpdateLaserAccountCode(category.id,lac.id,'test lac');
         AddServiceNamesToLaserCodesCtr.UpdateLaserAccountCode(category.id,null,'null');
          AddServiceNamesToLaserCodesCtr.getLaserAccountCodes(category1.id);
       
    }
}