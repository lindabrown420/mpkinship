@isTest
private class KindshipCheckMangermentControllerTest {
    @TestSetup
    static void initData(){
        Id profileId1 = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User u = new User(FirstName='Test',LastName= 'User',profileid=profileId1 , email='abc123@test.com',
                         username='abc123@test.com.prod',Alias='zbd',LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', EmailEncodingKey='UTF-8',
                          TimeZoneSidKey='America/Los_Angeles');
        Account acc = new Account(Name = 'test acc', Tax_ID__c = '123-56-7901', Entity_Type__c = 'Individual');
        insert acc;
        
        Locality_Account__c locality = new Locality_Account__c(Name = '12345');
        insert locality;

        Category__c cate = new Category__c(Name = 'test cate', Category_Type__c = 'CSA', Locality_Account__c = locality.id , RecordTypeId = Schema.SObjectType.Category__c.getRecordTypeInfosByName().get('LEDRS Category').getRecordTypeId());
        insert cate;
        
        Kinship_Case__c kcase = new Kinship_Case__c(Primary_Social_Worker__c = u.id);

        Opportunity opp = new Opportunity(Name = 'test Opp', AccountId = acc.id, Category__c = cate.id, CloseDate = Date.today().addDays(10),
                                          StageName='Open', Kinship_Case__c = kcase.id);
        insert opp;
        
        Check_Run__c cr = new Check_Run__c();
        cr.Number_Of_Check__c = 1;
        cr.Check_Run_Total__c = 100;
        insert cr;
        
        Kinship_Check__c kc = new Kinship_Check__c();
        kc.Check_No__c = '2000';
        kc.Status__c = 'Issued';
        kc.Vendor__c = acc.id;
        kc.Issue_Date__c = Date.today().addDays(-4);
        kc.Amount__c = 100;
        kc.Check_Run__c = cr.id;
        insert kc;
        Payment__c payment = new Payment__c(Opportunity__c = opp.id, 
                                            Scheduled_Date__c = Date.today(), 
                                            Status__c = 'Submitted For Approval',
                                            Invoice_Stage__c = 'Ready to Pay',
                                            Written_Off__c = false,
                                            Payment_Method__c = 'Check',
                                            Category_lookup__c = cate.id,
                                            Kinship_Check__c = kc.id);
        insert payment;

        cr = new Check_Run__c();
        cr.Number_Of_Check__c = 1;
        cr.Check_Run_Total__c = 100;
        insert cr;
        
        kc = new Kinship_Check__c();
        kc.Check_No__c = '2001';
        kc.Status__c = 'Issued';
        kc.Vendor__c = acc.id;
        kc.Issue_Date__c = Date.today().addDays(1);
        kc.Amount__c = 100;
        kc.Check_Run__c = cr.id;
        insert kc;
        payment = new Payment__c(Opportunity__c = opp.id, 
                                 Scheduled_Date__c = Date.today(), 
                                 Status__c = 'Submitted For Approval',
                                 Invoice_Stage__c = 'Ready to Pay',
                                 Written_Off__c = false,
                                 Payment_Method__c = 'Check',
                                 Category_lookup__c = cate.id,
                                 Kinship_Check__c = kc.id);

        
        insert payment;
    }

    @isTest
    static void testKinshipSearchLookup(){
        List<Account> lstData = [select id, name from Account];
        List<Id> fixedSearchResults= new List<Id>();
        fixedSearchResults.add(lstData[0].id);
        Test.setFixedSearchResults(fixedSearchResults);
        Test.startTest();
        List<Object> lstResult = KindshipCheckMangermentController.kinshipSearchLookup('test', new List<String>(), 'Account');
        Test.stopTest();
        System.assertEquals(1, lstResult.size(), 'The kinshipSearchLookup method return does not equal 1');
    }
    
    @isTest
    static void testKinshipSearchLookup_Cate(){
        List<Category__c> lstData = [select id, name from Category__c];
        List<Id> fixedSearchResults= new List<Id>();
        fixedSearchResults.add(lstData[0].id);
        Test.setFixedSearchResults(fixedSearchResults);
        Test.startTest();
        List<Object> lstResult = KindshipCheckMangermentController.kinshipSearchLookup('test', new List<String>(), 'Cate');
        Test.stopTest();
        System.assertEquals(1, lstResult.size(), 'The kinshipSearchLookup method return does not equal 1');
    }

    @isTest
    private static void testSearchCheckData(){
        List<String> lstVendor = new List<String>();
        List<Account> lstAcc = [SELECT id, name FROM Account];
        for(Account acc : lstAcc){
            lstVendor.add(acc.id);
        }
        Test.startTest();
        List<Object> lstResult = KindshipCheckMangermentController.searchCheckData(lstVendor, 'Issued', Date.today().addDays(-1), Date.today().addDays(4));
        KindshipCheckMangermentController.CheckObj migrationCheck = (KindshipCheckMangermentController.CheckObj)lstResult[0];
        migrationCheck.lstPayment = new List<Payment__c>();
        KindshipCheckMangermentController.saveMigrationCheck(migrationCheck);
        Test.stopTest();

        System.assertEquals(1, lstResult.size(), 'The method does not return 1 expected result');
    }

    @isTest
    private static void testGetSessionId(){
        Test.startTest();
        String sessionId = KindshipCheckMangermentController.getSessionId();
        Test.stopTest();

        System.assertNotEquals(null, sessionId, 'The method does not return the current User Session Id');
    }

    @isTest
    private static void testGetCheckData(){
        Test.startTest();
        List<Object> lstResult = KindshipCheckMangermentController.getCheckData('2001', '2001', 'Issued', Date.today().addDays(1), 100, 1000);
        Test.stopTest();

        System.assertEquals(1, lstResult.size(), 'The method does not return 1 expected result');
    }

    @isTest
    private static void testUpdateChecks(){
        List<Kinship_Check__c> lstCheck = [Select id From Kinship_Check__c limit 1];
        List<String> lstId = new List<String>();
        lstId.add(lstCheck[0].id);

        Test.startTest();
        KindshipCheckMangermentController.getExportData(lstId);
        KindshipCheckMangermentController.updateChecks(lstId, 'Cancelled');
        KindshipCheckMangermentController.updateCheckNo(lstId, 1);
        Test.stopTest();

        lstCheck = [Select id, Status__c From Kinship_Check__c Where Id IN :lstId];
        System.assertEquals('Cancelled', lstCheck[0].Status__c, 'The method does not update the check status to Void');
    }

    @isTest
    private static void testUpdateChecksPrinted(){
        List<Kinship_Check__c> lstCheck = [Select id From Kinship_Check__c limit 1];
        List<String> lstId = new List<String>();
        lstId.add(lstCheck[0].id);

        Test.startTest();
        KindshipCheckMangermentController.updateChecksPrinted(lstId);
        Test.stopTest();

        lstCheck = [Select id, Is_Printed__c From Kinship_Check__c Where Id IN :lstId];
        System.assertEquals(true, lstCheck[0].Is_Printed__c, 'The method does not update the check Is_Printed__c to true');
    }

    @isTest
    private static void testGetPicklistvalues(){
        Test.startTest();
        List<String> lstResult = KindshipCheckMangermentController.getPicklistvalues('Kinship_Check__c', 'Status__c', '--none--');
        Test.stopTest();

        System.assertEquals(5, lstResult.size(), 'The method does not return 4 expected status on the picklist');
    }
    
    @isTest
    private static void testGetMergeChecks(){
        List<KindshipCheckMangermentController.CheckObj> lstData = KindshipCheckMangermentController.getCheckData('2001', '2001', 'Issued', Date.today().addDays(1), 100, 1000);
		List<Kinship_Check__c> lstChecks = [SELECT id, Issue_date__c FROM Kinship_Check__c Limit 2];
        for(Kinship_Check__c c : lstChecks) {
            c.Issue_date__c = Date.today().addDays(1);
        }
        update lstChecks;
        Test.startTest();
        List<Object> lstResult = KindshipCheckMangermentController.getMergeChecks(lstData[0]);
        Test.stopTest();

        System.assertNotEquals(null, lstResult, 'The method does not return 1 expected result');
    }
    
    @isTest
    private static void testSaveMergeCheck(){
        List<Kinship_Check__c> lstChecks = [SELECT id FROM Kinship_Check__c Limit 2];
        List<String> lstId = new List<String>();
        lstId.add(lstChecks[0].id);
        Test.startTest();
        KindshipCheckMangermentController.saveMergeCheck(lstId, lstChecks[1].id);
        Test.stopTest();
    }
    
    @isTest
    private static void testInitData(){
        Test.startTest();
        KindshipCheckMangermentController.initData();
        Test.stopTest();
    }
    
    @isTest
    private static void testUpdateCheckNo(){
        List<Kinship_Check__c> lstChecks = [SELECT id, Check_No__c FROM Kinship_Check__c Limit 1];
        List<KindshipCheckMangermentController.checkObj> lstData = new List<KindshipCheckMangermentController.checkObj>();
        KindshipCheckMangermentController.checkObj data = new KindshipCheckMangermentController.checkObj();
        data.id = lstChecks[0].id;
        data.checkNo = '3000';
        lstData.add(data);
        Test.startTest();
        KindshipCheckMangermentController.updateCheckNo(lstData);
        Test.stopTest();
    }
}