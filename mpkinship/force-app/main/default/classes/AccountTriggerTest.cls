@isTest
public class AccountTriggerTest {
	 @testSetup
    static void testRecords(){
        
    	contact con = new contact();
        con.LastName='contactrec';
        con.FirstName='test';
        con.Email='test@contact.com';        
        insert con;   
        
        Kinship_Case__c caserec = new Kinship_Case__c();
        caserec.Primary_Contact__c=con.id;
        insert caserec;
        
        account accountrec = new account();
        accountrec.Name='test vendor';
        accountrec.Tax_ID__c='222-22-3333';
        accountrec.Entity_Type__c='Individual';
        insert accountrec;
        
        Category__c category = new Category__c();
        category.Name='222';
        category.Category_Type__c='General Cases';
        category.RecordTypeId = Schema.SObjectType.Category__c.getRecordTypeInfosByName().get('LASER Category').getRecordTypeId();
        insert category;  
     
        TriggerActivation__c  ta = new TriggerActivation__c ();
        ta.name='AccountTrigger';
        ta.isActive__c=true;
        insert ta;
    }
    public static testmethod void test1(){
        AccountTriggerHandler at = new AccountTriggerHandler();
        at.bulkBefore();
        list<TriggerActivation__c> talist=[select id,name,isActive__c from TriggerActivation__c where name='AccountTrigger'];
        list<Account> acclist=[select id,name,Tax_ID__c,Entity_Type__c from account];
        acclist[0].Tax_ID__c='222-22-4333';
        acclist[0].entity_Type__c='Individual';
        update acclist[0];
    }
}