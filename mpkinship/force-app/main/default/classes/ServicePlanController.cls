public class ServicePlanController {
	
    @AuraEnabled
    public static String getRT_Name(string rtId){
        RecordType rt = [Select id,name from RecordType where id=: rtId];
        return rt.Name;        
    }
    
    @AuraEnabled
    public static Boolean getRefreshPage(string recId){
        IFSP__c sp = [Select id,Refresh_Page__c from IFSP__c where id=: recId];
        if(sp.Refresh_Page__c == true){
            sp.Refresh_Page__c = false;
            update sp;
            return true; 
        }
        else{
            return false;
        }            
    }
}