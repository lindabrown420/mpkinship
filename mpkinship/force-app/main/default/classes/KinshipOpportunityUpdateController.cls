public class KinshipOpportunityUpdateController {
    public final static STRING CC = 'CC';
    public final static STRING FIPS = 'FIPS';
    public final static STRING PO = 'PO';
    public final static STRING PAYMENT = 'Payment';
    private final static Integer MAX_RESULTS = 5;

    @AuraEnabled(cacheable=true)
    public static List<LookupSearchResult> kinshipSearchLookup(String searchTerm, List<String> selectedIds, String anOptionalParam) {
        // Prepare query paramters
        searchTerm += '*';

        // Prepare results
        List<LookupSearchResult> results = new List<LookupSearchResult>();
        List<List<SObject>> searchResults;

        if(String.isNotBlank(anOptionalParam)) {
            if(FIPS.equals(anOptionalParam)) {
                searchResults = [FIND :searchTerm IN ALL FIELDS RETURNING
                                FIPS__c(Id, Name, FIPS_Code__c, Locality__c WHERE id NOT IN :selectedIds)
                                LIMIT :MAX_RESULTS];
                String uItem = 'standard:category';
                FIPS__c[] fips = ((List<FIPS__c>) searchResults[0]);
                for (FIPS__c u : fips) {
                    results.add(new LookupSearchResult(u.Id, 'FIPS__c', uItem, u.Name, u.FIPS_Code__c));
                }
            } else if(CC.equals(anOptionalParam)) {
                searchResults = [FIND :searchTerm IN ALL FIELDS RETURNING
                                Cost_Center__c(Id, Name, Cost_Center_Code__c WHERE id NOT IN :selectedIds)
                                LIMIT :MAX_RESULTS];
                String iItem = 'standard:user';
                Cost_Center__c[] items = ((List<Cost_Center__c>) searchResults[0]);
                for (Cost_Center__c i : items) {
                    results.add(new LookupSearchResult(i.Id, 'Cost_Center__c', iItem, i.Name, i.Cost_Center_Code__c));
                }
            } else if(PO.equals(anOptionalParam)) {
                searchResults = [FIND :searchTerm IN ALL FIELDS RETURNING
                                Opportunity (Id, Name, StageName WHERE id NOT IN :selectedIds)
                                LIMIT :MAX_RESULTS];
                String opptyIcon = 'standard:opportunity';
                Opportunity [] opptys = ((List<Opportunity>) searchResults[0]);
                for (Opportunity oppty : opptys) {
                    results.add(new LookupSearchResult(oppty.Id, 'Opportunity', opptyIcon, oppty.Name, ''));
                }
            } else if(PAYMENT.equals(anOptionalParam)) {
                searchResults = [FIND :searchTerm IN ALL FIELDS RETURNING
                                Payment__c (Id, Name WHERE id NOT IN :selectedIds)
                                LIMIT :MAX_RESULTS];
                String pmIcon = 'standard:checkout';
                Payment__c[] payments = ((List<Payment__c>) searchResults[0]);
                for (Payment__c pm : payments) {
                    results.add(new LookupSearchResult(pm.Id, 'Payment__c', pmIcon, pm.Name, ''));
                }
            }
        }

        return results;
    }

    @AuraEnabled
    public static List<OppWrapper> getOpportunitis(List<String> lstPO, List<String> lstPayment){
        try {
            List<OppWrapper> lstResult = new List<OppWrapper>();
            String strQuery = 'SELECT id,name,recordtype.name, StageName, End_Date__c, CloseDate, CampaignId, ' +
                                'Campaign.Name, Amount, CreatedBy.Name, Posting_Period__c, FIPS_Code__c, FIPS_Code__r.Name, FIPS_Code__r.Locality__c '+
                                'FROM Opportunity ';
            String strWhere = '';
            if(lstPO.size() > 0) {
                strWhere += 'AND Id IN :lstPO ';
            }
            List<String> strOppId = new List<String>();
            if(lstPayment.size() > 0) {
                List<Payment__c> lstP = [SELECT id, Opportunity__c FROM Payment__c WHERE Id IN :lstPayment];
                if(lstP.Size() > 0) {
                    for(Payment__c p : lstP) {
                        strOppId.add(p.Opportunity__c);
                    }
                    strWhere += 'AND Id IN :strOppId ';
                }
            }
            if(String.isNotBlank(strWhere)) {
                strQuery += strWhere.replaceFirst('AND ', 'WHERE ');
            }
            
            for(Opportunity roc : Database.Query(strQuery)){
                OppWrapper temp = new OppWrapper();
                temp.id = roc.id;
                temp.name = roc.name;
                temp.rocurl = '/' + roc.id;
                temp.beginDate = roc.CloseDate;
                temp.endDate = roc.End_Date__c;
                temp.amount = roc.Amount;
                temp.stage = roc.StageName;
                temp.fiscalYear = roc.Campaign.Name;
                temp.fyurl = roc.CampaignId;
                temp.createdBy = roc.CreatedBy.Name;
                temp.postingPeriod = roc.Posting_Period__c;
                temp.fipsid = roc.FIPS_Code__c;
                temp.fipsurl = '/' + roc.FIPS_Code__c;
                temp.fipsname = roc.FIPS_Code__r.Name;
                temp.fipsLocality = roc.FIPS_Code__r.Locality__c;
                lstResult.add(temp);
            }
            return lstResult;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static void updateFIPSonPOSO(String fipsId, String ccId, List<OppWrapper> lstPO) {
        try{
            // get AP
            Map<String, Campaign> mapAP = new Map<String, Campaign>();
            String fipsLocality = '';
            for(Campaign c : [SELECT id, Name, RecordType.Name, FIPS__c, FIPS__r.Name, FIPS__r.Locality__c 
                                FROM Campaign 
                                WHERE FIPS__c = :fipsId AND IsActive = true]){
                mapAP.put(c.Name, c);
                fipsLocality = c.FIPS__r.Locality__c;
            }
            List<Opportunity> lstPOUpdate = new List<Opportunity>();
            String strTemp = '';
            Campaign cTemp;
            Set<Id> setId = new Set<Id>();
            for(OppWrapper opp : lstPO) {
                strTemp = opp.fiscalYear.replaceFirst(opp.fipsLocality, fipsLocality);
                cTemp = mapAP.get(strTemp);
                if(cTemp != null) {
                    lstPOUpdate.add(new Opportunity(Id = opp.id, FIPS_Code__c = fipsId, CampaignId = cTemp.Id));
                }
                setId.add(opp.id);
            }
            Set<Id> setCheck = new Set<Id>();
            Map<String, String> mapCheckToFIPS = new Map<String, String>();
            List<Payment__c> lstPaymentUpdate = [SELECT id, name, FIPS_Code__c, FIPS_Code__r.Locality__c, 
                                                            Accounting_Period__c, Accounting_Period__r.Name, Cost_Center__c, Kinship_Check__c,
                                                            Parent_Fiscal_Year__c, Parent_Fiscal_Year__r.Name
                                                            FROM Payment__c 
                                                            WHERE Opportunity__c IN :setId];
            for(Payment__c p : lstPaymentUpdate) {
                p.FIPS_Code__c = fipsId;
                p.Cost_Center__c = ccId;
                if(String.isNotBlank(p.Accounting_Period__c) && String.isNotBlank(p.FIPS_Code__r.Locality__c)) {
                    strTemp = p.Accounting_Period__r.Name.replaceFirst(p.FIPS_Code__r.Locality__c, fipsLocality);
                    cTemp = mapAP.get(strTemp);
                    if(cTemp != null) {
                        p.Accounting_Period__c = cTemp.id;
                    }
                }
                if(String.isNotBlank(p.Parent_Fiscal_Year__c) && String.isNotBlank(p.FIPS_Code__r.Locality__c)) {
                    strTemp = p.Parent_Fiscal_Year__r.Name.replaceFirst(p.FIPS_Code__r.Locality__c, fipsLocality);
                    cTemp = mapAP.get(strTemp);
                    if(cTemp != null) {
                        p.Parent_Fiscal_Year__c = cTemp.id;
                    }
                }
                setCheck.add(p.Kinship_Check__c);
                mapCheckToFIPS.put(p.Kinship_Check__c, p.FIPS_Code__r.Locality__c);
            }
            List<Transaction_Journal__c> lstTJUpdate = [SELECT id, name, Invoice__r.FIPS_Code__c, Invoice__r.FIPS_Code__r.Locality__c, 
                                                            Child_Campaign__c, Child_Campaign__r.Name 
                                                            FROM Transaction_Journal__c 
                                                            WHERE Invoice__r.Opportunity__c IN :setId];
            for(Transaction_Journal__c tj : lstTJUpdate) {
                if(String.isNotBlank(tj.Child_Campaign__c) && String.isNotBlank(tj.Invoice__r.FIPS_Code__r.Locality__c)) {
                    strTemp = tj.Child_Campaign__r.Name.replaceFirst(tj.Invoice__r.FIPS_Code__r.Locality__c, fipsLocality);
                    cTemp = mapAP.get(strTemp);
                    if(cTemp != null) {
                        tj.Child_Campaign__c = cTemp.id;
                    }
                }
            }

            List<Kinship_Check__c> lstCheck = [SELECT id, name, Fiscal_Year__c, Fiscal_Year__r.Name, FIPS__c, FIPS__r.Name
                                                            FROM Kinship_Check__c 
                                                            WHERE Id IN :setCheck];
            for(Kinship_Check__c c : lstCheck) {
                String checkFips = c.FIPS__r.Name;
                if(String.isBlank(checkFips)) {
                    checkFips = mapCheckToFIPS.get(c.Id);
                }
                if(String.isNotBlank(c.Fiscal_Year__r.Name) && String.isNotBlank(checkFips)) {
                    strTemp = c.Fiscal_Year__r.Name.replaceFirst(checkFips, fipsLocality);
                    cTemp = mapAP.get(strTemp);
                    if(cTemp != null) {
                        c.Fiscal_Year__c = cTemp.id;
                    }
                }
                c.FIPS__c = fipsId;
            }
            PaymentTriggerHandler.bypassOppUpdate = true;
            TransactionJournalService.byPassCheckPosted = true;
            update lstPOUpdate;
            update lstPaymentUpdate;
            update lstTJUpdate;
            update lstCheck;
            TransactionJournalService.byPassCheckPosted = false;
            PaymentTriggerHandler.bypassOppUpdate = false;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    public class OppWrapper{
        @AuraEnabled
        public string id{get;set;}

        @AuraEnabled
        public string name{get;set;}

        @AuraEnabled
        public string rocurl{get;set;}

        @AuraEnabled
        public Date beginDate{get;set;}

        @AuraEnabled
        public Date endDate{get;set;}

        @AuraEnabled
        public Decimal amount{get;set;}

        @AuraEnabled
        public string fyUrl{get;set;}

        @AuraEnabled
        public string fiscalYear{get;set;}

        @AuraEnabled
        public string fipsid{get;set;}

        @AuraEnabled
        public string fipsurl{get;set;}

        @AuraEnabled
        public string fipsname{get;set;}

        @AuraEnabled
        public string stage{get;set;}

        @AuraEnabled
        public string createdBy{get;set;}

        @AuraEnabled
        public string postingPeriod{get;set;}

        @AuraEnabled
        public string fipsLocality{get;set;}
    }
}