@isTest
public class ContactTriggerHandlerTest {
    @isTest
	public static void testContact1(){
        TriggerActivation__c  ta = new TriggerActivation__c();
        ta.name='ContactTrigger';
        ta.isActive__c=true;
        insert ta;
     
        contact con = new contact();       
        con.LastName='contactrec';
        con.FirstName='test';
        con.Email='test@contact.com';      
        insert con;
        
        Kinship_Case__c caserec = new Kinship_Case__c();
        caserec.Primary_Contact__c=con.id;
        caserec.DOB__c=system.today().adddays(-100);
        caserec.Race__c='1';
        caserec.Gender_Preference__c='M';
        caserec.Hispanic_Ethnicity__c='1';
        caserec.DSM_V_Indicator_ICD_10_1__c='2';
        caserec.Clinical_Medication_Indicator_1__c='2';
        caserec.Medicaid_Indicator_1__c='2';
        caserec.Referral_Source__c='5';
        caserec.Autism_Flag_1__c='2';
        insert caserec;
        
        Test.startTest();
        con.LastName = 'newContact';
        update con;
        Test.stopTest();
    }
}