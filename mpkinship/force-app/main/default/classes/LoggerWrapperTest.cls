@isTest
public class LoggerWrapperTest {
    static testmethod void testmethod1(){
        Account acc = new Account();
        LoggerWrapper.error('message', acc);
        LoggerWrapper.warn('message', acc);
        LoggerWrapper.info('message', acc);
        LoggerWrapper.debug('message', acc);
        LoggerWrapper.fine('message', acc);
        LoggerWrapper.finer('message', acc);
        LoggerWrapper.finest('message', acc);
    }
}