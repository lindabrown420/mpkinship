@isTest
public class RelatedListControllerTest {
    static testmethod void testmethod1(){
        RelatedListController con = new RelatedListController();
         Campaign parent = new Campaign();
        parent.name = 'Local - 2022 - Covington';
        parent.StartDate = Date.Today().addDays(-10);
        parent.EndDate = Date.Today().addDays(90);
        parent.Fiscal_Year_Type__c = 'Local';
        parent.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId();
        insert parent;
        
        Campaign child = new Campaign();
        child.name = 'DSS - 2022 - Covington';
        child.StartDate = Date.Today().addDays(-10);
        child.EndDate = Date.Today().addDays(90);
        child.Fiscal_Year_Type__c = 'DSS';
        child.ParentId = parent.id;
        child.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId();
        insert child;
        String strCriteria = null;  String selectStr = '';
        RelatedListController.fetchRecs( child.id, 'Campaign', 'Name' ,  strCriteria,  selectStr ) ;  
    }
}