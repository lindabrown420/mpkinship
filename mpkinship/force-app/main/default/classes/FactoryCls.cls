/*********
It's a factory class for all kind of common methods
***********/   

public class FactoryCls {
    
    // Method to get formatted currency string value
    public static String formatCurr(Decimal curr)
    {
        if(curr != null)
        {
            //5,000,000.555
            String decimalStr = curr.format();
            
            //5000000.56
            String decimalStrWithScale = curr.setScale(2).format();
            
            //5,000,000.56
            String decimalStrWithScaleFormat = curr.setScale(2).format();
            
            //$5,000,000.56
            String decimalStrWithDollarSign = string.format('${0}', new string[]{decimalStrWithScaleFormat});
            if(!decimalStrWithDollarSign.contains('.'))
            {
                decimalStrWithDollarSign += '.00';  
            }
            
            return decimalStrWithDollarSign;
        }
        else
        {
            return null;
        }
        
    }
}