@isTest
public class ServicePlanController_Test {
    public static testmethod void test1(){
        RecordType rt = [Select id,name from RecordType limit 1];
        ServicePlanController.getRT_Name(rt.Id);
    }
    
    public static testmethod void test2(){
        Contact conTest = new Contact();
        conTest.WorkPhone__c = '12343';
        conTest.LastName = 'Test Last';        
        insert conTest;
        
        Kinship_Case__c caseTest = new Kinship_Case__c();
        caseTest.Name = 'Test case';
        caseTest.Primary_Contact__c = conTest.Id;
        insert caseTest;    
        
        IFSP__c ifsp = new IFSP__c();
        ifsp.Case__c = caseTest.Id;
        ifsp.Refresh_Page__c = true;
        insert ifsp;
        
        ServicePlanController.getRefreshPage(ifsp.Id);
        
    }
}