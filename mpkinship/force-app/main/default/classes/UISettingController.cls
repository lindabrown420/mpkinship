public class UISettingController {
	@AuraEnabled(continuation = true)
    public static List<WrapperSectionDetail> getSectionDetail(){
        Id profileId = userinfo.getProfileId();
        String profileName = [Select Id,Name from Profile where Id=:profileId].Name;
        List<Section_Setting__c> listOfSectionSetting = new List<Section_Setting__c>();
        String likeProfile = profileName;
        listOfSectionSetting = [Select Id,Name,Sort__c,Size__c,Background_Color__c,Font_Color__c from Section_Setting__c where Visible_to__c includes(:likeProfile) AND Active__c = True order by Sort__c];
        System.debug('listOfSectionSetting'+listOfSectionSetting);
        List<Link_Setting__c> listOfLinkSetting = new List<Link_Setting__c>();
        listOfLinkSetting = [Select Id,Name,Sort__c,Filter_ID__C,Custom_Tab_Name__c,Action_Name__c,Icon__c,Icon_Name__c,Link_Type__c,Navigation_Type__c,Related_List_Name__c,
                             Object_Api_Name__c,Record_ID__c,Record_Type__c,URL__c,Section_Setting__r.Name from Link_Setting__c where Section_Setting__c IN: listOfSectionSetting AND Visible_to__c includes(:profileName) AND Active__c = True order by Sort__c ]; 
        
        WrapperSectionDetail wsd = new WrapperSectionDetail();
        List<WrapperSectionDetail> wsdList = new List<WrapperSectionDetail>();
        WrapperLinkDetail wld = new WrapperLinkDetail();
        List<WrapperLinkDetail> wldList = new List<WrapperLinkDetail>();
        for(Section_Setting__c ss : listOfSectionSetting ){
            wsd =  new WrapperSectionDetail();
            wsd.Id = ss.Id;
            wsd.heading = ss.Name;
            wsd.sortNumber = Integer.valueOf(ss.Sort__c);
            wsd.backgroundColor = ss.Background_Color__c;
            wsd.fontColor = ss.Font_Color__c; 
            wsd.size = ss.Size__c;
            wldList = new List<WrapperLinkDetail>();
            for(Link_Setting__c ls : listOfLinkSetting ){
                if(ss.Name == ls.Section_Setting__r.Name){
                    wld = new WrapperLinkDetail();
                    wld.text = ls.Name;
                    wld.apiName = ls.Custom_Tab_Name__c;
                    wld.relationshipApiName = ls.Related_List_Name__c;
                    wld.actionName = ls.Action_Name__c;
                    wld.isIcon = String.valueof(ls.Icon__c);
                    wld.iconName = ls.Icon_Name__c;
                    wld.type = ls.Link_Type__c;
                    wld.navigationType = ls.Navigation_Type__c;
                    wld.objectApiName = ls.Object_Api_Name__c;
                    wld.recordId = ls.Record_ID__c;
                    wld.filterName = ls.Filter_ID__C;
                    if(ls.Record_Type__c != null && ls.Record_Type__c != ''){
                    	wld.recordTypeId = Schema.getGlobalDescribe().get(ls.Object_Api_Name__c).getDescribe().getRecordTypeInfosByName().get(ls.Record_Type__c).getRecordTypeId();
                    }
                    wld.url = ls.URL__c;
                    wldList.add(wld);
                }
            }
            wsd.WrapperLinkDetail = wldList;
            System.debug('wsd'+wsd);
            wsdList.add(wsd);
        }
        System.debug('wsd.wsdList'+wsdList);
        return wsdList;
    }
    public Class WrapperSectionDetail{
        @AuraEnabled public String Id {get;set;}
        @AuraEnabled public String heading {get;set;}
        @AuraEnabled public String backgroundColor {get;set;}
        @AuraEnabled public String fontColor {get;set;}
        @AuraEnabled public String size {get;set;}
        public Integer sortNumber {get;set;}
        @AuraEnabled public List<WrapperLinkDetail> WrapperLinkDetail {get;set;}
        public WrapperSectionDetail(){}
    }
    public Class WrapperLinkDetail{
        @AuraEnabled public String Id {get;set;}
        @AuraEnabled public String apiName {get;set;}
        @AuraEnabled public String relationshipApiName {get;set;}
        @AuraEnabled public String text {get;set;}
        @AuraEnabled public String actionName {get;set;}
        @AuraEnabled public String isIcon {get;set;}
        @AuraEnabled public String iconName {get;set;}
        @AuraEnabled public String type {get;set;}
        @AuraEnabled public String navigationType {get;set;}
        @AuraEnabled public String objectApiName {get;set;}
        @AuraEnabled public String recordId {get;set;}
        @AuraEnabled public String recordTypeId {get;set;}
        @AuraEnabled public String url {get;set;}
        @AuraEnabled public String filterName {get;set;}
        
    }
}