@isTest
public class DepositSlipControllerTest {
     @isTest static void ReportCollectionTestMethod() {
        
        test.startTest();
         Deposits__c d= new Deposits__c();
         d.Status__c= 'In Progress';
         insert d;
         
          Opportunity roc = new Opportunity();
        
        //roc.Begin_Date__c      = Date.today();
        //roc.Begin_Receipt__c   = 1000;
        roc.Beginning_Receipt__c = '1';
        roc.Ending_Receipt__c = '20000';
        roc.Name               = 'Test!23';
        roc.StageName          = 'Draft';
        roc.Date_Posted__c     = Date.today();
        roc.End_Date__c        = Date.today().addDays(20);
        roc.Amount             = 1000;
        roc.CloseDate          = Date.today().addDays(7);
        roc.deposit__c         = d.id;
        
        insert roc;
        
         id invoiceRT = Schema.SObjectType.Payment__c.getRecordTypeInfosByName().get('Receipt').getRecordTypeId();
        
        Payment__c receipt = new Payment__c();
        
        receipt.Payment_Date__c          = DATE.today().addDays(8);
        receipt.Payment_Amount__c    = 1000;
        receipt.Report_Number__c            = '111';
        receipt.Description__c              = 'test';
        //receipt.Pay_Method__c               = 'Cash';
        receipt.Payment_Method__c ='Cash';
        receipt.Type_of_Receipt__c          = 'Refund';
        receipt.Opportunity__c       = roc.id;
        receipt.Receipt_Type__c             = 'Administrative';
        receipt.RecordTypeID                = invoiceRT;        
        insert receipt;
            
        PageReference myVfPage = Page.deposit_Slip;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('id',d.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(d);
        
        DepositSlipController deposit = new DepositSlipController(sc);
}
    
     @isTest static void ReportCollectionTestMethod1() {
        
        test.startTest();
         Deposits__c d= new Deposits__c();
         d.Status__c= 'In Progress';
         insert d;
         
          Opportunity roc = new Opportunity();
        
        //roc.Begin_Date__c      = Date.today();
        //roc.Begin_Receipt__c   = 1000;
        roc.Beginning_Receipt__c = '1';
        roc.Ending_Receipt__c = '20000';
        roc.Name               = 'Test!23';
        roc.StageName          = 'Draft';
        roc.Date_Posted__c     = Date.today();
        roc.End_Date__c        = Date.today().addDays(20);
        roc.Amount             = 1000;
        roc.CloseDate          = Date.today().addDays(7);
        roc.deposit__c         = d.id;
        
        insert roc;
        
         id invoiceRT = Schema.SObjectType.Payment__c.getRecordTypeInfosByName().get('Receipt').getRecordTypeId();
        
        Payment__c receipt = new Payment__c();
        
        receipt.Payment_Date__c          = DATE.today().addDays(8);
        receipt.Payment_Amount__c    = 1000;
        receipt.Report_Number__c            = '111';
        receipt.Description__c              = 'test';
        //receipt.Pay_Method__c               = 'Check';
        receipt.Payment_Method__c ='Check';
        receipt.Type_of_Receipt__c          = 'Refund';
        receipt.Opportunity__c       = roc.id;
        receipt.Receipt_Type__c             = 'Administrative';
        receipt.RecordTypeID                = invoiceRT;        
        insert receipt;
            
        PageReference myVfPage = Page.deposit_Slip;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('id',d.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(d);
        
        DepositSlipController deposit = new DepositSlipController(sc);
}
     @isTest static void ReportCollectionTestMethod2() {
        
        test.startTest();
         Deposits__c d= new Deposits__c();
         d.Status__c= 'In Progress';
         insert d;
         
          Opportunity roc = new Opportunity();
        
        //roc.Begin_Date__c      = Date.today();
        //roc.Begin_Receipt__c   = 1000;
        roc.Beginning_Receipt__c = '1';
        roc.Ending_Receipt__c = '20000';
        roc.Name               = 'Test!23';
        roc.StageName          = 'Draft';
        roc.Date_Posted__c     = Date.today();
        roc.End_Date__c        = Date.today().addDays(20);
        roc.Amount             = 1000;
        roc.CloseDate          = Date.today().addDays(7);
        roc.deposit__c         = d.id;
        
        insert roc;
        
         id invoiceRT = Schema.SObjectType.Payment__c.getRecordTypeInfosByName().get('Receipt').getRecordTypeId();
        
        Payment__c receipt = new Payment__c();
        
        receipt.Payment_Date__c          = DATE.today().addDays(8);
        receipt.Payment_Amount__c    = 1000;
        receipt.Report_Number__c            = '111';
        receipt.Description__c              = 'test';
        //receipt.Pay_Method__c               = 'Money Order';
        receipt.Payment_Method__c ='Money Order';
        receipt.Type_of_Receipt__c          = 'Refund';
        receipt.Opportunity__c       = roc.id;
        receipt.Receipt_Type__c             = 'Administrative';
        receipt.RecordTypeID                = invoiceRT;        
        insert receipt;
            
        PageReference myVfPage = Page.deposit_Slip;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('id',d.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(d);
        
        DepositSlipController deposit = new DepositSlipController(sc);
}
}