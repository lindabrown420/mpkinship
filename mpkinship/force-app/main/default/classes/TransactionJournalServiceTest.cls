@IsTest
private class TransactionJournalServiceTest {
	@TestSetup
    static void initData(){
        TriggerActivation__c ta = new TriggerActivation__c(Name = 'PaymentTrigger', isActive__c = true);
        insert ta;
        Id profileId1 = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User u = new User(FirstName='Test',LastName= 'User',profileid=profileId1 , email='abc123@test.com',
                         username='abc123@test.com.prod',Alias='zbd',LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', EmailEncodingKey='UTF-8',
                          TimeZoneSidKey='America/Los_Angeles');
        
        Account acc = new Account(Name = 'test acc', Tax_ID__c = '123-56-7901', Entity_Type__c = 'Individual');
        insert acc;

        Locality_Account__c locality = new Locality_Account__c(Name = '12345');
        insert locality;

        Category__c cate = new Category__c(Name = 'test cate', Category_Type__c = 'CSA', Locality_Account__c = locality.id , RecordTypeId = Schema.SObjectType.Category__c.getRecordTypeInfosByName().get('LEDRS Category').getRecordTypeId());
        insert cate;
        
        Kinship_Case__c kcase = new Kinship_Case__c(Primary_Social_Worker__c = u.id);
        
        FIPS__c fips1 = new FIPS__c(Name = 'Covington', Locality__c = 'Covington');
        insert fips1;
        
        FIPS__c fips2 = new FIPS__c(Name = 'Alleghany', Locality__c = 'Alleghany');
        insert fips2;
        
        Campaign Pc = new Campaign(name = 'Local - 2020',Status='Unposted', Fiscal_Year_Type__c = 'Local', RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId());
        insert Pc;
        
        Campaign parentC = new Campaign(name = 'DSS - 2020', Fiscal_Year_Type__c = 'DSS',ParentId = Pc.id, RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId());
        insert parentC;
        
        Campaign c = new Campaign(name = 'LASER - 2020 - Alleghany',Status='Open',StartDate = Date.Today().addDays(-2), EndDate = Date.Today().addDays(2), ParentId = parentC.id,FIPS__c = fips1.id, RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('LASER Reporting Period').getRecordTypeId());
        insert c;
        
        c = new Campaign(name = 'LASER - 2020 - Covington',Status='Open', StartDate = Date.Today().addDays(-2), EndDate = Date.Today().addDays(2), ParentId = parentC.id,FIPS__c = fips2.id, RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('LASER Reporting Period').getRecordTypeId());
        insert c;
        

        Opportunity opp = new Opportunity(Name = 'test Opp', AccountId = acc.id, Category__c = cate.id, CloseDate = Date.today().addDays(10),
                                          StageName='Open', Kinship_Case__c = kcase.id, FIPS_Code__c = fips1.id,CampaignId = Pc.Id,
                                          RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Purchase of Services Order').getRecordTypeId());
        insert opp;

        Payment__c payment = new Payment__c(Opportunity__c = opp.id, 
                                                                Scheduled_Date__c = Date.today(), 
                                                                Status__c = 'Submitted For Approval',
                                                                Invoice_Stage__c = 'Ready to Pay',
                                                                Written_Off__c = false,
                                                                Payment_Method__c = 'Check',
                                                                Category_lookup__c = cate.id,
                                                                Payment_Amount__c = 100,
                                                              	FIPS_Code__c = fips1.id,
                                                               	Payment_Date__c = Date.Today(),
                                                                RecordTypeId = Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Vendor_Invoice').getRecordTypeId());
        insert payment;
        
        Cost_Center__c cc = new Cost_Center__c(Name = 'Cost Center', Cost_Center_Code__c = '12345');
        insert cc;
    }
    
    @isTest
    static void testGetRefundPostCondition(){
        Map<Integer, String> mapMonthToString = new Map<Integer, String>();
        mapMonthToString.put(1, 'January');
        mapMonthToString.put(2, 'February');
        mapMonthToString.put(3, 'March');
        mapMonthToString.put(4, 'April');
        mapMonthToString.put(5, 'May');
        mapMonthToString.put(6, 'June');
        mapMonthToString.put(7, 'July');
        mapMonthToString.put(8, 'August');
        mapMonthToString.put(9, 'September');
        mapMonthToString.put(10, 'October');
        mapMonthToString.put(11, 'November');
        mapMonthToString.put(12, 'December');
        List<Campaign> lstCamp = [SELECT id, name, FIPS__c, endDate, Cate_Type__c FROM Campaign WHERE Name LIKE '%LASER%'];
        Test.startTest();
        TransactionJournalService.getRefundPostCondition(lstCamp, mapMonthToString);
        Test.stopTest();
    }
    
    @isTest
    static void testGetAP(){
        List<Payment__c> lstPayment = [SELECT id, Payment_Date__c, Name FROM Payment__c];
        List<Campaign> lstCamp = [SELECT id, name, FIPS__c,StartDate, endDate, Cate_Type__c FROM Campaign WHERE Name LIKE '%LASER%'];
        Test.startTest();
        TransactionJournalService.getAP(lstCamp, lstPayment[0]);
        Test.stopTest();
    }
    
    @isTest
    static void testAutoPost(){
        Payment__c payment = [SELECT id,Invoice_Stage__c, Payment_Date__c, Name FROM Payment__c limit 1];
        Test.startTest();
        payment.Invoice_Stage__c = 'Paid';
        update payment;
        Test.stopTest();
    }
   @isTest
    static void insertAccountPeriodtest(){
        TEST.startTest();
    	Campaign Pc1 = new Campaign(name = 'Local - 2020', Fiscal_Year_Type__c = 'DSS',startDate=date.today().adddays(-2),enddate=date.today().adddays(-1), RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId());
        insert Pc1; 
    	Campaign Pc = new Campaign(name = 'Local - 2020',ParentId=PC1.ID, Fiscal_Year_Type__c = 'LOCAL',startDate=date.today().adddays(-2),enddate=date.today().adddays(-1), RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('LASER Reporting Period').getRecordTypeId());
        insert Pc; 
        list<Campaign> camplist = new list<Campaign>();
        camplist.add(pc);
        TransactionJournalService.insertAccountPeriod(camplist); 
        TEST.stopTest();
    }  
    @isTest
    static void getBudgetMapTest(){
     list<Campaign> camp=[select id from campaign where name like 'Local%'];
        Budgets__c bb = new Budgets__c();
        bb.Fiscal_Year__c= camp[0].id;
        bb.Name='test budget';
        insert bb;
        TransactionJournalService.getBudgetMap(camp);
    }
    
    @isTest
    static void updateAccountPeriodTest(){
    	list<Campaign> camp=[select id,status from campaign where name like 'Local%']; 
        system.debug('**VAl of camp'+camp);
        list<Campaign> camp1=[select id,status from campaign where name like 'LASER%' limit 1];
        system.debug('**val of camp1'+camp1);
        payment__C pay=[select id from payment__C ];
        Transaction_Journal__c tj = new Transaction_Journal__c();
        tj.Invoice__c=pay.id;
        tj.Child_Campaign__c=camp1[0].id;
        insert tj;
		TransactionJournalService.updateAccountPeriod(camp1,camp);
    }
    
    @isTest
    public static void checkPostedTJTest(){
    	list<Campaign> camp=[select id,status,startDate,enddate from campaign where name like '%Alleghany']; 
        list<opportunity> opp=[select id from opportunity];
        list<id> campidlist = new list<id>();
        campidlist.add(camp[0].id);
        list<Category__C> cate=[select id from category__C ];
        list<fips__C> fips1=[select id from fips__C];
          Payment__c payment = new Payment__c(Opportunity__c = opp[0].id, 
                                                                Scheduled_Date__c = Date.today(), 
                                                                Status__c = 'Approved',
                                                                Invoice_Stage__c = 'Pending',
                                                                Written_Off__c = false,
                                                                Payment_Method__c = 'Check',
                                                                Category_lookup__c = cate[0].id,
                                                                Payment_Amount__c = 100,
                                                              	FIPS_Code__c = fips1[0].id,
                                                               	Payment_Date__c = Date.Today(),
                                                                RecordTypeId = Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Vendor_Invoice').getRecordTypeId());
        insert payment;
        TransactionJournalService.checkPostedTJ(campidlist);
          
    }
    
    @isTest
    static void autoPostedTJTest(){
        list<fips__C> fips1=[select id from fips__C];
        list<campaign> grandpcamp=[select id,FIPS__c,Cate_Type__c from campaign where name like 'Local%'];
        list<campaign> camp1=[select id,parentid,FIPS__c,Cate_Type__c from campaign where name like 'DSS%'];
     	list<Campaign> camp=[select id,status,startDate,enddate,parentid,Parent.parentid,FIPS__c,Cate_Type__c from campaign where name like '%Alleghany']; 
        list<opportunity> opp=[select id from opportunity];
       
        list<Category__C> cate=[select id from category__C ];
        
          Payment__c payment = new Payment__c(Opportunity__c = opp[0].id, 
                                                                Scheduled_Date__c = Date.today(), 
                                                                Status__c = 'Approved',
                                                                Invoice_Stage__c = 'Pending',
                                                                Written_Off__c = false,
                                                                Payment_Method__c = 'Check',
                                                                Category_lookup__c = cate[0].id,
                                              					Accounting_Period__c=camp[0].id,
                                                                Payment_Amount__c = 100,
                                                              	FIPS_Code__c = fips1[0].id,
                                                               	Payment_Date__c = Date.Today(),
                                                                RecordTypeId = Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Vendor_Invoice').getRecordTypeId());
        insert payment;
        TransactionJournalService.autoPostedTJ(camp);   
    }
}