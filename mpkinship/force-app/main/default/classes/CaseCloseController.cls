public class CaseCloseController {

    @auraEnabled
    public static ResponseWrap updatePO(string oppId, Date newEndDate,String statusRsn,string typeOfPO){
      string msg='';
      try{
        List<Opportunity> PO=[Select Id,Name,Reason__c,Type_of_Case_Action__c,End_Date__c,closeDate,Amount,Prorate_First_Payment__c,Prorate_Last_Payment__c,Prorated_First_Payment_Amount__c,Prorated_Last_Payment_Amount__c from Opportunity where Id=:oppId LIMIT 1];
        System.debug('PO Id'+PO[0].Id);
        List<Payment__c> payList=[Select Id,Name,Invoice_Stage__c,Service_Begin_Date__c,Service_End_Date__c,Written_Off__c,Paid__c from Payment__c where Opportunity__c=:PO[0].Id];
        
        List<Payment__c> payListtowriteOff= new List<Payment__c>();
        List<Payment__c> payListtoChangeAmount= new List<Payment__c>();
        
        if(PO.size()>0){ 
            for(Payment__c pay:payList){
                System.debug('pay:'+pay);
                
                if(newEndDate < pay.Service_Begin_Date__c && newEndDate < pay.Service_End_Date__c && pay.Paid__c==false && pay.Written_Off__c==false){
                    payListtowriteOff.add(pay);
                    System.debug('payListtowriteOff'+pay);
                }
                
               /**ALWAYS GIVES ONE PAYMENT RECORD **/ 
               if(typeOfPO=='Case Action' && newEndDate < pay.Service_End_Date__c && newEndDate > pay.Service_Begin_Date__c && pay.Paid__c==false && pay.Written_Off__c==false
               && newEndDate.month() == pay.Service_Begin_Date__c.month()){
                    System.debug('payListtoChangeAmount:'+pay);
                    payListtoChangeAmount.add(pay);
                } 
            }
            //COMMON LIST FOR UPDATION
            list<Payment__c> paymentsToUpdate = new list<Payment__c>();
            
            for(Payment__c pay : payListtowriteOff){
                pay.Written_Off__c=true;
                pay.Invoice_Stage__c='Write off';
                paymentsToUpdate.add(pay);
            }
            
            //CHECKING FOR PO AMOUNT TO BE CHANGED IF PAYLISTTOCHANGEAMOUNT IS GREATER THAN ZERO
            if(payListtoChangeAmount.size()>0 && PO[0].Type_of_Case_Action__c=='Ongoing' && PO[0].Prorate_Last_Payment__c==true){
                Integer monthsBetween = PO[0].CloseDate.monthsBetween(newEndDate);
                if(monthsBetween==0){
                    PO[0].Prorated_Last_Payment_Amount__c=0.0; // because there will be only one invoice
                }
                else{
                    Integer numberDays = Date.daysInMonth(newEndDate.year(), newEndDate.month());  
                   decimal amountval =  (PO[0].amount * newEndDate.day())/numberDays; 
                   System.debug('amountval::'+amountval);
                   PO[0].Prorated_Last_Payment_Amount__c=amountval; 
                }
            }
            
            //FOR CHANGING THE AMOUNT VALUE WITH LAST PRORATING DATE
            for(Payment__c pay : payListtoChangeAmount){               
                pay.Service_End_Date__c = newEndDate;
                System.debug('First_Paymen'+PO[0].Prorate_First_Payment__c);
                              
                 if(PO[0].Type_of_Case_Action__c=='Ongoing' && PO[0].Prorate_Last_Payment__c==true){
                     Integer monthsBetween = PO[0].CloseDate.monthsBetween(newEndDate);
                     System.debug('monthsBetween:::'+monthsBetween);
                     if(monthsBetween > 0){                          
                          Integer numberDays = Date.daysInMonth(newEndDate.year(), newEndDate.month());  
                          decimal amountval =  (PO[0].amount * newEndDate.day())/numberDays; 
                          System.debug('amountval::'+amountval);                               
                          pay.Payment_Amount__c=amountval.setscale(2); 
                                               
                     }
                }
                paymentsToUpdate.add(pay);              
            }
            
            if(paymentsToUpdate.size()>0)
                update paymentsToUpdate;
           
            PO[0].End_Date__c=newEndDate;
            
            
           /* if(typeOfPO=='Case Action')
                PO[0].Status_Reason__c=statusRsn;
            else
                PO[0].Reason__c=statusRsn;  */  
            update PO[0];
       
         ResponseWrap wrap = new ResponseWrap();
                wrap.msg='Record updated';
                wrap.opp= PO[0];
                return wrap;
            }  
          return null;  
        } //try ends here                           
         catch(DmlException  e){
            for (Integer i = 0; i < e.getNumDml(); i++) {
                //Get Validation Rule & Trigger Error Messages
                msg =+ e.getDmlMessage(i) +  '\n' ;
            }
          
            //throw DML exception message
            if(!Test.isRunningTest())
                throw new AuraHandledException(msg);
            else
                return null;
        }catch(Exception e){
            system.debug('***Exception e'+e.getStackTraceString());
            system.debug('***msg'+e.getMessage());
            //throw all other exception message
            if(!Test.isRunningTest())
                throw new AuraHandledException(e.getMessage());
            else
                return null;
        }    
    }
    
    public class ResponseWrap{
        @auraEnabled
        public string msg;
        @auraEnabled
        public opportunity opp;
    }
}