@isTest
public class ContactAttachmentsTest {
	public static testMethod void test(){
        Contact c = new Contact();
        c.LastName = 'test';
        insert c;
        ContentNote nt  = new ContentNote();
        nt.Title = 'test';
        insert nt;
        ContentDocument cd=[select id from ContentDocument where id=:nt.Id];
        ContentDocumentLink cdl=new ContentDocumentLink();
        cdl.ContentDocumentId=cd.id;
        cdl.LinkedEntityId=c.Id;
        cdl.ShareType='V';
        cdl.Visibility='AllUsers';
        insert cdl;
        ContactAttachments.getAttachments(c.Id);
        ContactAttachments.deleteAttachments(cd.Id);
    }
    public static testMethod void test1(){
        Contact c = new Contact();
        c.LastName = 'test';
        insert c;
        ContentNote nt  = new ContentNote();
        nt.Title = 'test';
        insert nt;
        ContentDocument cd=[select id from ContentDocument where id=:nt.Id];
        ContentDocumentLink cdl=new ContentDocumentLink();
        cdl.ContentDocumentId=cd.id;
        cdl.LinkedEntityId=c.Id;
        cdl.ShareType='V';
        cdl.Visibility='AllUsers';
        insert cdl;
        ContactAttachments.getAttachments(nt.Id);
    }
}