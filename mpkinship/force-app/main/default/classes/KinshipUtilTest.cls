@isTest
private class KinshipUtilTest {
	@isTest
    static void KinshipUtilTest(){
        Campaign fy = new Campaign();
        fy.name = '2021';
        fy.StartDate = Date.Today().addDays(-10);
        fy.EndDate = Date.Today().addDays(10);
        fy.Fiscal_Year_Type__c = 'Local';
        fy.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId();
        insert fy;
        List<Campaign> lstCamp = new List<Campaign>();
        lstCamp.add(fy);
        
        Test.startTest();
        sObject originalSObject = (sObject) fy;
        List<sObject> result = KinshipUtil.cloneObjects(lstCamp, originalSObject.getsObjectType());
        Test.stopTest();
        
        System.assertNotEquals(null, result, 'The result is null.');
    }
    
    @isTest
    public static void getMaxLengthTest1(){
        String str = '0123456789';
        String strExpected = '01234';
        Test.startTest();
        String result = KinshipUtil.getMaxLength(str, 5);
        Test.stopTest();

        System.assertEquals(strExpected, result, 'The GetMaxLength Method does not return ' + strExpected + ' with maxlength is 5');
    }

    @isTest
    public static void getMaxLengthTest2(){
        String strExpected = null;
        Test.startTest();
        String result = KinshipUtil.getMaxLength(null, 5);
        Test.stopTest();

        System.assertEquals(strExpected, result, 'The GetMaxLength Method does not return ' + strExpected + ' with maxlength is 5');
    }
    
    @isTest
    public static void getAmountValue1(){
        Test.startTest();
        Decimal result = KinshipUtil.getAmountValue(5);
        Test.stopTest();

        System.assertEquals(5, result, 'The getAmountValue Method does not return expected value.');
    }
    
    @isTest
    public static void getAmountValue2(){
        Test.startTest();
        Decimal result = KinshipUtil.getAmountValue(-5);
        Test.stopTest();

        System.assertEquals(5, result, 'The getAmountValue Method does not return expected value.');
    }
}