@istest
public class XLSXGeneratorTest {
    public static testmethod void SXGeneratortestmethod(){
         account accountrec = new account();
        accountrec.Name='test vendor';
        accountrec.Tax_ID__c='222-22-3333';
        accountrec.Entity_Type__c='Individual';
        insert accountrec;
        
                id recType = Schema.SObjectType.contact.getRecordTypeInfosByDeveloperName().get('Contacts').getRecordTypeId();
        contact con = new contact();
        con.recordtypeid=recType;
        con.LastName='contactrec';
        con.FirstName='test';
        con.Email='test@contact.com';        
        insert con;
        
        Kinship_Case__c caserec = new Kinship_Case__c();
        caserec.Primary_Contact__c=con.id;
        insert caserec;

        Locality_Account__c la = new Locality_Account__c();
        la.Name='222';
        insert la;
        
        Category__c category = new Category__c();
        category.Name='222';
        category.Locality_Account__c=la.id;
        category.Category_Type__c='Adoption';
        category.RecordTypeId = Schema.SObjectType.Category__c.getRecordTypeInfosByName().get('LASER Category').getRecordTypeId();
        insert category;  
        
        FIPS__c fp = new FIPS__c();
        fp.name='test fips';
        insert fp;
        id recTypeid = Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Fiscal_Year').getRecordTypeId();
        campaign cap = new campaign();
        cap.FIPS__c=fp.id;
        cap.RecordTypeId=recTypeid;
        cap.Fiscal_Year_Type__c='Local';
        cap.name='test';
        cap.Begin_Date__c=system.today().addmonths(-1);
        cap.EndDate=system.today().addyears(1);
        insert cap;
        
         opportunity opprecord = new opportunity();
        opprecord.CloseDate=date.today();
        opprecord.StageName='Draft';
        opprecord.End_Date__c=date.today().addmonths(3);
        opprecord.Name='test opportunity';
        opprecord.Kinship_Case__c= caserec.id;
        opprecord.Amount=20000;
        opprecord.Category__c = category.id;
        opprecord.AccountId= accountrec.id;
        opprecord.Number_of_Months_Units__c=3;
        insert opprecord;
        
 
        id payrecType = Schema.SObjectType.Payment__c.getRecordTypeInfosByName().get('Vendor Invoice').getRecordTypeId();
        Payment__c payment = new Payment__c();
        payment.Opportunity__c=opprecord.id;
        payment.Payment_Amount__c=2000;
        payment.Kinship_Case__c = caserec.id;
        payment.vendor__c = accountrec.id;
        payment.RecordTypeId=payrecType;
        payment.Service_Begin_Date__c = system.today() ;
        payment.Service_End_Date__c = system.today();
        payment.Invoice_Stage__c = 'Paid';
        insert payment;
        
        
        List<String> i = new List<String>();
        i.add(caserec.id);
    XLSXGenerator.generate(i, '', '');
    SPR_Report.getData(i, '', '');
        
        XLSXGenerator.caseWrapper controller = new XLSXGenerator.caseWrapper();
    
         PageReference testPage = Page.XLSXTemplate; 
       testPage.getParameters().put('caseIds', Json.serialize(i));
       testPage.getParameters().put('startDate', '');
       testPage.getParameters().put('endDate', '');
       
        Test.setCurrentPage(testPage);
        
       XLSXTemplateController s = new XLSXTemplateController();
       
    }
    
}