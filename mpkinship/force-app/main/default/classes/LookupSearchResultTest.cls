@isTest
private class LookupSearchResultTest {
    @isTest
    static void testInitLookupSearchResult(){
        Account acc = new Account(Name = 'test acc', Tax_ID__c = '123-56-7901', Entity_Type__c = 'Individual');
        insert acc;

        Test.startTest();
        LookupSearchResult lsr = new LookupSearchResult(acc.id, 'Account', 'icon', 'test acc', 'des');
        System.assertEquals(acc.id, lsr.getId(), 'The LookupSearchResult getId method return does not return correct value');
        System.assertEquals('Account', lsr.getSObjectType(), 'The LookupSearchResult getSObjectType method return does not return correct value');
        System.assertEquals('icon', lsr.getIcon(), 'The LookupSearchResult getIcon method return does not return correct value');
        System.assertEquals('test acc', lsr.getTitle(), 'The LookupSearchResult getTitle method return does not return correct value');
        System.assertEquals('des', lsr.getSubtitle(), 'The LookupSearchResult getSubtitle method return does not return correct value');
        Test.stopTest();
    }
}