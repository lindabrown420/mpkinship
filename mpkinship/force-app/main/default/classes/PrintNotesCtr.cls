/*********************
Name : PrintNotesCtr
Description - class for Print Case Notes vf page

*****************/
Public class PrintNotesCtr{

    public boolean Isnotes {get;set;}
    
    public PrintNotesCtr(ApexPages.StandardController controller){
        Isnotes =false;
        String caseId= ApexPages.CurrentPage().getParameters().get('id');
        if(caseId!=''){
            list<Kinship_Case_Notes__c> caseNotes =[select id,name from Kinship_Case_Notes__c where Kinship_Case__c=:caseId limit 50000];
            if(caseNotes.size()>0)
                Isnotes=true;
        }
    }
}