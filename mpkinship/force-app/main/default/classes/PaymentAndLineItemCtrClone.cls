public class PaymentAndLineItemCtrClone {
    public static boolean IsReset=false;
    
    @auraEnabled
    public static list<PaymentWrap1> PaymentLineitems(string opportunityid){
        /**CHECK IF IT RELATED TO CASE ACTION OR NOT**/
        boolean iscaseAction=false;
        list<opportunity> opplist=[select id,recordtypeid,recordtype.developername from opportunity where id=:opportunityid];
        if(opplist.size()>0){
            
             if(opplist[0].recordtype.developername=='Case_Actions')
                  iscaseAction=true;
             else
                  iscaseAction=false;                       
        }
         //LOGGED IN USER PROFILE
        Id profileId=userinfo.getProfileId();
        String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
        //KIN 1468
        list<Payment__c> payments =[select id,name,Payment_Date__c,Cancelled__c,Check_Number__c,Check_Stub__c,Opportunity__c,FICA_Withholding__c,Employee_Rate__c,Category_lookup__r.Category_Type__c,Opportunity__r.closedate,Opportunity__r.End_Date__c,Payment_Amount__c,Scheduled_Date__c,Amount_Paid__c,Paid__c,Service_Begin_Date__c,Service_End_Date__c,
                                              Written_Off__c,Description__c,Invoice_Number__c,Category_lookup__r.Name,Category_lookup__c,Case_Name__c,Service_Period__c,(select id,name,invoice__r.Service_Begin_Date__c,invoice__r.Service_End_Date__c,Previously_Billed__c,Max_Hours_Authorized__c,LineItem_Index__c,Remaining_Amount__c,
                                              Quantity_Billed__c,Unit_Price__c,Product__c,Unit_Measure__c,Unit__c,Remaining_Quantity__c,
                                               LineItem_Amount_Billed__c,Max_Quantity_Authorized__c from Invoice_Line_Items__r) 
                                              from Payment__c
                                             where  Opportunity__c=:opportunityid and Invoice_Stage__c!='Cancelled' and Cancelled__c=false order by Scheduled_Date__c asc];  
      
        map<string,list<Payment__c>> DatePaymentsMap = new map<String,list<Payment__c>>();
        
        list<PaymentWrap1> paywrapList = new list<PaymentWrap1>();
        
        for(Payment__c payment : payments){            
            if(payment.Scheduled_Date__c!=null && DatePaymentsMap.containskey(payment.Scheduled_Date__c.month()+
            '-'+payment.Scheduled_Date__c.year())){
                DatePaymentsMap.get(payment.Scheduled_Date__c.month()+'-'+payment.Scheduled_Date__c.year()).add(payment);
            }
            else
                 DatePaymentsMap.put(payment.Scheduled_Date__c.month()+'-'+payment.Scheduled_Date__c.year(),
                  new list<Payment__c>{payment});
        }
        
        //MAP PARSING
        list<PaymentWrap1> PaymentWrap1List = new list<PaymentWrap1>();
        for(string s : DatePaymentsMap.keyset()){
            PaymentWrap1 Paymentwraprec = new PaymentWrap1();
            PaymentWraprec.MonthDigit=convertMonthDigit(s);
            Paymentwraprec.Month = convertMonthTextToNumber(s);
            PaymentWraprec.PaymentRec=DatePaymentsMap.get(s);
            PaymentWraprec.IscaseAction=iscaseAction;
            if(profileName!=null && profileName=='System Administrator')
                PaymentWraprec.IsAdminProfile=true;
            else
                PaymentWraprec.IsAdminProfile=false;    
             //FIRST AND LAST INVOICE
            if(payments.size()>0){
                integer i = payments.size()-1;
                    PaymentWraprec.FirstInvoiceId= payments[0].id;
                    PaymentWraprec.LastInvoiceId = payments[i].id;
            }
            PaymentWrap1List.add( Paymentwraprec);    
        }
        if(PaymentWrap1List.size()>0)
            return PaymentWrap1List;
        return null;
    }
    
     @auraEnabled
      public static list<PaymentWrap1> updatePaymentLineitems(string paymentlineitems, string opportunityid){
        Savepoint sp = Database.setSavepoint();
        string msg='';
        try{ 
            List<Payment_Line_Item__c> paymentItemsList= (List<Payment_Line_Item__c>) JSON.deserialize(paymentlineitems, 
                                                            List<Payment_Line_Item__c>.class);
            system.debug('**VAl of paymentItemsList'+  paymentItemsList);                                              
            //MAP OF CHANGED QUANITY AND LINEITEM
            map<id,decimal> paymentlineitemMap = new map<id,decimal>();
            map<id,decimal> paymentlineitemAmountMap= new map<id,decimal>();
            for(Payment_Line_Item__c item : paymentItemsList){
                if(item.Quantity_Billed__c!=null)
                    paymentlineitemMap.put(item.Id, item.Quantity_Billed__c);   
               else if(item.LineItem_Amount_Billed__c!=null)
                   paymentlineitemAmountMap.put(item.Id,item.LineItem_Amount_Billed__c);      
            }
            system.debug('***map'+ paymentlineitemMap);
            system.debug('**amount map'+paymentlineitemAmountMap);
            //QUERY FROM LINEITEMS PRESENT IN THE MAP
            list<Payment_Line_Item__c> lineitems =[select id,Previously_Billed__c,Remaining_Amount__c,Quantity_Billed__c,
                                    LineItem_Index__c,Unit_Price__c,Max_Quantity_Authorized__c,invoice__C,Unit__c,
                                    invoice__r.Amount_Paid__c,invoice__r.Payment_Amount__c,Remaining_Quantity__c,invoice__r.Category_lookup__c,
                                    LineItem_Amount_Billed__c from Payment_Line_Item__c where id in: paymentlineitemMap.keyset() or id in: paymentlineitemAmountMap.keyset()];
           system.debug('************'+lineitems.size());                         
                                    
            //PAYMENT WHOSE AMOUNT PAID NEEDS TO BE UDPATED                        
            list<Payment__c> paymentToUpdate = new list<Payment__c>();                        
            id changeItemPaymentID; id categoryId;
                           
            list< Payment_Line_Item__c> paymentItemstobeUpdated = new list<Payment_Line_Item__c>();
            
             Payment__c payment = new Payment__c();  
            
            //MAP FOR UPDATING REMAINING LINEITEMS
            map<decimal,decimal> paymentLineItemsPrevBilledMap = new map<decimal,decimal>();
                             
            for(Payment_Line_Item__c payitem :  lineitems){
                //ALL IDS ARE OF SAME PAYMENT
                changeItemPaymentID = payitem.invoice__C;
                if(payitem.invoice__r.Category_lookup__c !=null)
                    categoryId = payitem.invoice__r.Category_lookup__c;
                Payment_Line_Item__c item = new Payment_Line_Item__c();
                item.id = payitem.id;
                if(paymentlineitemMap.containskey(payitem.id)){
                    if(payitem.Quantity_Billed__c ==null)
                        payitem.Quantity_Billed__c=0;
                    if(paymentlineitemMap.get(payitem.id)==null)
                        paymentlineitemMap.put(payitem.id,0);
                    if(payitem.Previously_Billed__c ==null)
                        payitem.Previously_Billed__c =0;
                    decimal changeinquantity = ((paymentlineitemMap.get(payitem.id)).setScale(2) - payitem.Quantity_Billed__c).setScale(2);        
                    item.Previously_Billed__c = (payitem.Previously_Billed__c + changeinquantity).setscale(2);
                    item.Quantity_Billed__c = paymentlineitemMap.get(payitem.id).setscale(2);
                    item.Remaining_Quantity__c= (payitem.Max_Quantity_Authorized__c - item.Previously_Billed__c).setscale(2);
                    item.Remaining_Amount__c =((payitem.Max_Quantity_Authorized__c - item.Previously_Billed__c) 
                                               * payitem.Unit_Price__c).setscale(2);  
                    item.LineItem_Amount_Billed__c =  (item.Quantity_Billed__c * payitem.Unit_Price__c).setscale(2);                        
                    paymentItemstobeUpdated.add(item);
                    paymentLineItemsPrevBilledMap.put(payitem.LineItem_Index__c,item.Previously_Billed__c);
                    
                }
                else if(paymentlineitemAmountMap.containskey(payitem.id)){
                    if(payitem.LineItem_Amount_Billed__c ==null)
                        payitem.LineItem_Amount_Billed__c =0;
                   if(payitem.Quantity_Billed__c ==null)
                        payitem.Quantity_Billed__c=0;
                    if(paymentlineitemAmountMap.get(payitem.id)==null)
                        paymentlineitemAmountMap.put(payitem.id,0);
                    if(payitem.Previously_Billed__c ==null)
                        payitem.Previously_Billed__c =0;
                    decimal quantityval =  (paymentlineitemAmountMap.get(payitem.id) /payitem.Unit_Price__c);
                    string quantitytostring = string.valueof(quantityval);
                     String myTruncatedNumber='';
                    if(quantitytostring.contains('.')){
                        if(quantitytostring.substringAfter('.').length()>1)                      
                            myTruncatedNumber = quantitytostring.subString(0,quantitytostring.indexOf('.')+3);
                         else
                            myTruncatedNumber = quantitytostring.subString(0,quantitytostring.indexOf('.')+2);
                        if(myTruncatedNumber!='')         
                            quantityval  = Decimal.valueOf(myTruncatedNumber);
                    }
                    /* if ((paymentlineitemAmountMap.get(payitem.id) /payitem.Unit_Price__c).toString().contains('.')){
                         string quanval = paymentlineitemAmountMap.get(payitem.id) /payitem.Unit_Price__c).toString();
                         list<string> stringvalue = quanval.split('.');
                         string decimalstring = stringvalue[0].substring(0,2);
                         string finalstring = stringvalue[1]+'.'+decimalstring;
                         quantityval= decimal.valueof(finalstring); 
                     } */
                    decimal changeinquantity = (quantityval  - payitem.Quantity_Billed__c).setscale(2);        
                    item.Previously_Billed__c = (payitem.Previously_Billed__c + changeinquantity).setscale(2);
                    item.Quantity_Billed__c = quantityval;
                    item.Remaining_Quantity__c= (payitem.Max_Quantity_Authorized__c - item.Previously_Billed__c).setscale(2);
                    item.Remaining_Amount__c =((payitem.Max_Quantity_Authorized__c - item.Previously_Billed__c) 
                                               * payitem.Unit_Price__c).setscale(2);  
                    item.LineItem_Amount_Billed__c =  paymentlineitemAmountMap.get(payitem.id).setscale(2);  
                    system.debug('**lineitem amountbilled val'+ item.LineItem_Amount_Billed__c );                     
                    paymentItemstobeUpdated.add(item);
                    paymentLineItemsPrevBilledMap.put(payitem.LineItem_Index__c,item.Previously_Billed__c);
                
                }
                
            }  // for loop ends here
            system.debug('************lineitem'+paymentItemstobeUpdated.size());
            system.debug('*******prevbilledmap '+ paymentLineItemsPrevBilledMap);
            system.debug('********prevbilled map size'+paymentLineItemsPrevBilledMap.size());
             //UPDATE PAYMENTITEMS                    
           if(paymentItemstobeUpdated.size()>0)
                update paymentItemstobeUpdated;
                system.debug('**Val of paymentlineitem'+ paymentItemstobeUpdated);
           list<Payment__c> currentpayments=[select id,Amount_Paid__c,Payment_Amount__c,Invoice_Stage__c,FICA_Withholding__c,Employee_Rate__c,(select id,name,Previously_Billed__c,LineItem_Index__c,
                                               Remaining_Amount__c,Quantity_Billed__c,Unit_Price__c,Product__c,Unit_Measure__c,LineItem_Amount_Billed__c,
                                               Max_Quantity_Authorized__c,Unit__c from Invoice_Line_Items__r) from Payment__c
                                               where id=:changeItemPaymentID];
                                               
            list<Payment_Line_Item__c > paylineitemstoUpdate = new list<Payment_Line_Item__c >();                                   
            for( Payment__c paymentrec : currentpayments){
                Payment__c  payrec = new Payment__c ();
                payrec.id = paymentrec.id;
                decimal totalAmount =0;
                for(Payment_Line_Item__c paylineitem : paymentrec.Invoice_Line_Items__r){
                    if(paylineitem.Quantity_Billed__c!=null && paylineitem.Unit_Price__c!=null)
                       // totalAmount +=(paylineitem.Quantity_Billed__c * paylineitem.Unit_Price__c);
                       totalAmount +=paylineitem.LineItem_Amount_Billed__c;
                    else if (paylineitem.Quantity_Billed__c==null){
                        Payment_Line_Item__c pi = new Payment_Line_Item__c();
                        pi.id = paylineitem.id;
                        pi.Quantity_Billed__c=0;
                        pi.LineItem_Amount_Billed__c=0;
                        paylineitemstoUpdate.add(pi);
                    }   
                }
               // payrec.Amount_Paid__c=totalAmount;
               if(paymentrec.Employee_Rate__c!= null && paymentrec.FICA_Withholding__c!=null && totalAmount > 0)
                   payrec.Payment_Amount__c = (totalAmount - paymentrec.FICA_Withholding__c).setScale(2);
               else    
                   payrec.Payment_Amount__c=totalAmount.setscale(2);
                if(totalAmount >0)
                    payrec.Invoice_Stage__c ='Ready to Pay';
                paymentToUpdate.add(payrec);
            }                                  
                                               
            //paymentToUpdate.add(payment);
           
            //UPDATE PAYMENT
            if( paymentToUpdate.size()>0)
                update paymentToUpdate; 
                
            //UPDATE LINE ITEMS TOO 
            if(paylineitemstoUpdate.size()>0)
                update paylineitemstoUpdate;
                 
            // REMAINING PAYMENTS
            list<Payment__c> payments =[select id,name,cancelled__c,Opportunity__c,Payment_Amount__c,Category_lookup__c,
                                                  Scheduled_Date__c,Amount_Paid__c,(select id,name,Previously_Billed__c,
                                                  LineItem_Index__c,Remaining_Amount__c,LineItem_Amount_Billed__c,Quantity_Billed__c,Unit_Price__c,Unit__c,Remaining_Quantity__c,
                                                  Product__c,Unit_Measure__c,Max_Quantity_Authorized__c from Invoice_Line_Items__r) 
                                                  from Payment__c where  Opportunity__c=:opportunityid
                                                  and id !=: changeItemPaymentID and category_lookup__c=:categoryId and Invoice_Stage__c!='Cancelled' and cancelled__c=false];  
            system.debug('**Val of payments'+ payments);                                       
            list< Payment_Line_Item__c> remainingLineItems = new list<Payment_Line_Item__c>();                                      
            for(Payment__c paymentrec : payments){
                for(Payment_Line_Item__c item : paymentrec.Invoice_Line_Items__r){
                    Payment_Line_Item__c payitem = new Payment_Line_Item__c ();
                    payitem.id = item.id;
                    
                    if(paymentLineItemsPrevBilledMap.size() >0 && paymentLineItemsPrevBilledMap.containskey(item.LineItem_Index__c)){
                        
                        payitem.Previously_Billed__c = paymentLineItemsPrevBilledMap.get(item.LineItem_Index__c).setscale(2);
                        payitem.Remaining_Quantity__c= (item.Max_Quantity_Authorized__c  - payitem.Previously_Billed__c).setscale(2);
                        payitem.Remaining_Amount__c = ((item.Max_Quantity_Authorized__c  - payitem.Previously_Billed__c) * item.Unit_Price__c).setscale(2);
                        remainingLineItems.add(payitem);
                    }
                }
            }
            
            if(remainingLineItems.size()>0)
                update remainingLineItems;
            
            //list<PaymentWrap> Paymentwrapper = new list<PaymentWrap>();
            list<PaymentWrap1> Paymentwrapper = new list<PaymentWrap1>();
            paymentwrapper =  PaymentLineitems(opportunityid);
            return paymentwrapper;  
        }// try ends here 
        catch(DmlException  e){
            Database.rollback(sp);
            for (Integer i = 0; i < e.getNumDml(); i++) {
                //Get Validation Rule & Trigger Error Messages
                msg =+ e.getDmlMessage(i) +  '\n' ;
            }
            //throw DML exception message
            if(!Test.isRunningTest())
                throw new AuraHandledException(msg);
            else
                return null;
        }catch(Exception e){
            system.debug('***Exception e'+e.getStackTraceString());
            Database.rollback(sp);
            //throw all other exception message
            if(!Test.isRunningTest())
                throw new AuraHandledException(e.getMessage());
            else
                return null;
        }
                
    }
    
   public class PaymentWrap1{
        @auraEnabled
        public string Month;
        @auraEnabled
        public string MonthDigit;
        @auraEnabled
        public boolean IscaseAction;
        @auraEnabled
        public  list<Payment__c> PaymentRec;
        @auraEnabled
        public string FirstInvoiceId;
        @auraEnabled
        public string LastInvoiceId;
        @auraEnabled
        public boolean IsAdminProfile;
    }
    
    public class PaymentWrap{
        @auraEnabled
        public list<Payment__c> PaymentRec;
      /*  @auraEnabled
        public list<Payment_Line_Item__c> paymentItems;*/
    }
    
    @testvisible
    private static string convertMonthTextToNumber(String s){
        if(s.contains('-')){
            list<String> splitstring = s.split('-');
        
            if(splitstring[0]== '1' || splitstring[0]=='01'){
                return 'January'+' '+splitstring[1];   
            }else if(splitstring[0]== '2' || splitstring[0]=='02'){
                return 'February'+' '+splitstring[1];  
            }else if(splitstring[0]== '3' || splitstring[0]=='03'){
                return 'March'+' '+splitstring[1];   
            }else if(splitstring[0]== '4' || splitstring[0]=='04'){
                return 'April'+' '+splitstring[1];     
            }else if(splitstring[0]== '5' || splitstring[0]=='05'){
                return 'May'+' '+splitstring[1];        
            }else if(splitstring[0]== '6' || splitstring[0]=='06'){            
                return 'June'+' '+splitstring[1];    
            }else if(splitstring[0]== '7' || splitstring[0]=='07'){
                return 'July'+' '+splitstring[1];    
            }else if(splitstring[0]== '8' || splitstring[0]=='08'){
                return 'August'+' '+splitstring[1];    
            }else if(splitstring[0]== '9' || splitstring[0]=='09'){
                return 'September'+' '+splitstring[1];    
            }else if(splitstring[0]== '10'){
                return 'October'+' '+splitstring[1];   
            }else if(splitstring[0]== '11'){
                return 'November'+' '+splitstring[1];  
            }else{
                return 'December'+' '+splitstring[1]; 
            }
        }
        return '';
    }
    @testVisible
    private static string convertMonthDigit(String s){
        if(s.contains('-')){
            list<String> splitstring = s.split('-');
        
            if(splitstring[0]== '1' || splitstring[0]=='01'){
                return splitstring[1]+'-'+'01';   
            }else if(splitstring[0]== '2' || splitstring[0]=='02'){
                return splitstring[1]+'-'+'02';   
            }else if(splitstring[0]== '3' || splitstring[0]=='03'){
                return splitstring[1]+'-'+'03';     
            }else if(splitstring[0]== '4' || splitstring[0]=='04'){
                return splitstring[1]+'-'+'04';     
            }else if(splitstring[0]== '5' || splitstring[0]=='05'){
                return splitstring[1]+'-'+'05';         
            }else if(splitstring[0]== '6' || splitstring[0]=='06'){            
                return splitstring[1]+'-'+'06';  
            }else if(splitstring[0]== '7' || splitstring[0]=='07'){
                return splitstring[1]+'-'+'07';   
            }else if(splitstring[0]== '8' || splitstring[0]=='08'){
                return splitstring[1]+'-'+'08';     
            }else if(splitstring[0]== '9' || splitstring[0]=='09'){
                return splitstring[1]+'-'+'09';      
            }else if(splitstring[0]== '10'){
                return splitstring[1]+'-'+'10';     
            }else if(splitstring[0]== '11'){
                return splitstring[1]+'-'+'11';   
            }else{
                return splitstring[1]+'-'+'12';  
            }
        }
        return '';
    }
    
    @auraEnabled
    public static DateWrap changePaymentDate(string Id,date Dateval,string IsBeginDate){
        DateWrap datewraprec = new DateWrap();
        if(isBeginDate=='true')
            datewraprec.IsBegin=true;
        else
            datewraprec.IsBegin=false;    
        list<Payment__c> paymenttoUpdate = new list<Payment__c>();
        list<Payment__c> payments =[select id,name,Opportunity__c,Payment_Amount__c,Service_Begin_Date__c,Service_End_Date__c                                             
                                              from Payment__c where id=:Id];
          if(payments.size()>0){
                
                 Payment__c paymentrec = new Payment__c();
                 paymentrec.id = payments[0].id;
                 if(IsBeginDate=='true')
                     paymentrec.Service_Begin_Date__c=Dateval;
                 else
                     paymentrec.Service_End_Date__c=Dateval;    
                 paymenttoUpdate.add(paymentrec);
              
          } 
          
          if(paymenttoUpdate.size()>0){
              update paymenttoUpdate;
              datewraprec.paymentrec= paymenttoUpdate[0];
              datewraprec.updatedVal = 'updated'; 
              return datewraprec;
           }  
         else{
              datewraprec.updatedVal ='not updated';
         }
             
         return datewraprec;                                      
    }
    
    @auraEnabled
    public static string fetchInvoiceNumber(string rowIndex,string InvoiceNumber){
        system.debug('***Enterd here');
       
       list<Payment__c> paymenttoUpdate = new list<Payment__c>();
          list<Payment__c> payments =[select id,name,Opportunity__c,Payment_Amount__c,Invoice_Number__c                                             
                                              from Payment__c where id=:rowIndex];
          if(payments.size()>0){
              
                 Payment__c paymentrec = new Payment__c();
                 paymentrec.id = payments[0].id;
                 paymentrec.Invoice_Number__c =invoiceNumber;   
                 paymenttoUpdate.add(paymentrec);
              
          } 
          
          if(paymenttoUpdate.size()>0){
              update paymenttoUpdate; 
              return 'updated';
           }  
           
           return 'not updated';                                      
    }
    
     @auraEnabled
    public static string updateDescription(string rowIndex,string description){
        system.debug('***Enterd here in description');
       
       list<Payment__c> paymenttoUpdate = new list<Payment__c>();
          list<Payment__c> payments =[select id,name,Opportunity__c,Payment_Amount__c,Description__c
                                              from Payment__c where id=:rowIndex];
          if(payments.size()>0){
              
                 Payment__c paymentrec = new Payment__c();
                 paymentrec.id = payments[0].id;
                 paymentrec.Description__c=description;   
                 paymenttoUpdate.add(paymentrec);
              
          } 
          
          if(paymenttoUpdate.size()>0){
              update paymenttoUpdate; 
              return 'updated';
           }  
           
           return 'not updated';                                      
    }
    
    @auraEnabled
    public static list<PaymentWrap1> changePaymentToWriteOff(string recordid,string opportunityid){
       list<Payment__c> paymenttoUpdate = new list<Payment__c>();
          list<Payment__c> payments =[select id,name,Opportunity__c,Invoice_Stage__c,Payment_Amount__c,Invoice_Number__c,Written_Off__c                                             
                                              from Payment__c where id=:recordid];
          if(payments.size()>0){
              
                 Payment__c paymentrec = new Payment__c();
                 paymentrec.id = payments[0].id;
                 paymentrec.Written_Off__c=true;   
                 paymentrec.Invoice_Stage__c ='Write off';
                 paymenttoUpdate.add(paymentrec);
              
          } 
          
          if(paymenttoUpdate.size()>0){
              update paymenttoUpdate; 
               list<PaymentWrap1> Paymentwrapper = new list<PaymentWrap1>();
              paymentwrapper =  PaymentLineitems(opportunityid);
              return paymentwrapper;
           }  
           
           return null;    
    
    }
    

    //RESET INVOICE
    @auraEnabled
    public static list<PaymentWrap1> ResetPaymentData(string recordid, string opportunityid){
        IsReset = true;
        Savepoint sp = Database.setSavepoint();
        string msg='';
        try{ 
            list<Payment_Line_Item__c> lineitems =[select id,Previously_Billed__c,Remaining_Amount__c,Quantity_Billed__c,
                                    LineItem_Index__c,Unit_Price__c,Max_Quantity_Authorized__c,Invoice__c,Unit__c,
                                    invoice__r.Amount_Paid__c,Invoice__r.Payment_Amount__c,Remaining_Quantity__c,Invoice__r.Category_lookup__c,
                                    LineItem_Amount_Billed__c from Payment_Line_Item__c where invoice__C=:recordid];
                                    
          //PAYMENT WHOSE AMOUNT PAID NEEDS TO BE UDPATED                        
            list<Payment__c> paymentToUpdate = new list<Payment__c>();                        
            id changeItemPaymentID; id categoryId;
                           
            list< Payment_Line_Item__c> paymentItemstobeUpdated = new list<Payment_Line_Item__c>();
            
            Payment__c payment = new Payment__c();  
              //MAP FOR UPDATING REMAINING LINEITEMS
            map<decimal,decimal> paymentLineItemsPrevBilledMap = new map<decimal,decimal>();
            for(Payment_Line_Item__c payitem :  lineitems){
                //ALL IDS ARE OF SAME PAYMENT
                changeItemPaymentID = payitem.Invoice__c;
                if(payitem.Invoice__r.Category_lookup__c !=null)
                    categoryId = payitem.Invoice__r.Category_lookup__c;
                Payment_Line_Item__c item = new Payment_Line_Item__c();
                item.id = payitem.id;
                
                    if(payitem.Quantity_Billed__c ==null)
                        payitem.Quantity_Billed__c=0;
                   
                    if(payitem.Previously_Billed__c ==null)
                        payitem.Previously_Billed__c =0;
                    decimal changeinquantity = (0 - payitem.Quantity_Billed__c).setScale(2);        
                    item.Previously_Billed__c = (payitem.Previously_Billed__c + changeinquantity).setscale(2);
                    item.Quantity_Billed__c = 0;
                    item.Remaining_Quantity__c= (payitem.Max_Quantity_Authorized__c - item.Previously_Billed__c).setscale(2);
                    item.Remaining_Amount__c =((payitem.Max_Quantity_Authorized__c - item.Previously_Billed__c) 
                                               * payitem.Unit_Price__c).setscale(2);  
                    item.LineItem_Amount_Billed__c =  (item.Quantity_Billed__c * payitem.Unit_Price__c).setscale(2);                        
                    paymentItemstobeUpdated.add(item);
                    paymentLineItemsPrevBilledMap.put(payitem.LineItem_Index__c,item.Previously_Billed__c);
               
               
                
            }  // for loop ends here
            system.debug('&&************lineitem'+paymentItemstobeUpdated.size());  
             //UPDATE PAYMENTITEMS                    
           if(paymentItemstobeUpdated.size()>0)
                update paymentItemstobeUpdated;    
            list<Payment__c> currentpayments=[select id,Amount_Paid__c,Payment_Amount__c,Invoice_Stage__c,FICA_Withholding__c,Employee_Rate__c,(select id,name,Previously_Billed__c,LineItem_Index__c,
                                               Remaining_Amount__c,Quantity_Billed__c,Unit_Price__c,Product__c,Unit_Measure__c,LineItem_Amount_Billed__c,
                                               Max_Quantity_Authorized__c,Unit__c from Invoice_Line_Items__r) from Payment__c
                                               where id=:changeItemPaymentID];
            list<Payment_Line_Item__c > paylineitemstoUpdate = new list<Payment_Line_Item__c >();                                   
            for( Payment__c paymentrec : currentpayments){
                Payment__c  payrec = new Payment__c ();
                payrec.id = paymentrec.id;
                decimal totalAmount =0;
                for(Payment_Line_Item__c paylineitem : paymentrec.Invoice_Line_Items__r){
                    if (paylineitem.Quantity_Billed__c==null){
                        Payment_Line_Item__c pi = new Payment_Line_Item__c();
                        pi.id = paylineitem.id;
                        pi.Quantity_Billed__c=0;
                        pi.LineItem_Amount_Billed__c=0;
                        paylineitemstoUpdate.add(pi);
                    }   
                }
                payrec.Payment_Amount__c=totalAmount;
                if(payrec.Invoice_Stage__c!='Scheduled')
                    payrec.Invoice_Stage__c ='Scheduled';
                paymentToUpdate.add(payrec);
                
            
            }                                                                
            //UPDATE PAYMENT
            if( paymentToUpdate.size()>0)
                update paymentToUpdate;  
            system.debug('**VAl of paymenttoupdate'+ paymentToUpdate);    
             //UPDATE LINE ITEMS TOO 
            if(paylineitemstoUpdate.size()>0)
                update paylineitemstoUpdate;
              // REMAINING PAYMENTS
            list<Payment__c> payments =[select id,name,cancelled__c,Opportunity__c,Payment_Amount__c,Category_lookup__c,
                                                  Scheduled_Date__c,Amount_Paid__c,(select id,name,Previously_Billed__c,
                                                  LineItem_Index__c,Remaining_Amount__c,LineItem_Amount_Billed__c,Quantity_Billed__c,Unit_Price__c,Unit__c,Remaining_Quantity__c,
                                                  Product__c,Unit_Measure__c,Max_Quantity_Authorized__c from Invoice_Line_Items__r) 
                                                  from Payment__c where  Opportunity__c=:opportunityid
                                                  and id !=: changeItemPaymentID and category_lookup__c=:categoryId and Invoice_Stage__c!='Cancelled' and cancelled__c=false];  
            system.debug('**Val of payments'+ payments);                                       
            list< Payment_Line_Item__c> remainingLineItems = new list<Payment_Line_Item__c>();                                      
            for(Payment__c paymentrec : payments){
                for(Payment_Line_Item__c item : paymentrec.Invoice_Line_Items__r){
                    Payment_Line_Item__c payitem = new Payment_Line_Item__c ();
                    payitem.id = item.id;
                    
                    if(paymentLineItemsPrevBilledMap.size() >0 && paymentLineItemsPrevBilledMap.containskey(item.LineItem_Index__c)){
                        
                        payitem.Previously_Billed__c = paymentLineItemsPrevBilledMap.get(item.LineItem_Index__c).setscale(2);
                        payitem.Remaining_Quantity__c= (item.Max_Quantity_Authorized__c  - payitem.Previously_Billed__c).setscale(2);
                        payitem.Remaining_Amount__c = ((item.Max_Quantity_Authorized__c  - payitem.Previously_Billed__c) * item.Unit_Price__c).setscale(2);
                        remainingLineItems.add(payitem);
                    }
                }
            }
            
            if(remainingLineItems.size()>0)
                update remainingLineItems;
            
            //list<PaymentWrap> Paymentwrapper = new list<PaymentWrap>();
            list<PaymentWrap1> Paymentwrapper = new list<PaymentWrap1>();
            paymentwrapper =  PaymentLineitems(opportunityid);
            return paymentwrapper; 
            }// try ends here 
        catch(DmlException  e){
            Database.rollback(sp);
            for (Integer i = 0; i < e.getNumDml(); i++) {
                //Get Validation Rule & Trigger Error Messages
                msg =+ e.getDmlMessage(i) +  '\n' ;
            }
            //throw DML exception message
            if(!Test.isRunningTest())
                throw new AuraHandledException(msg);
            else
                return null;
        }catch(Exception e){
            system.debug('***Exception e'+e.getStackTraceString());
            Database.rollback(sp);
            //throw all other exception message
            if(!Test.isRunningTest())
                throw new AuraHandledException(e.getMessage());
            else
                return null;
        }    
                                                 
    }
    
    public class DateWrap{
        @auraEnabled
        public Payment__c  paymentrec;
        @auraEnabled
        public string updatedVal;
        @auraEnabled
        public boolean IsBegin;
    
    }
    
    
}