@isTest
public class MergedCheckTriggerTest {
	 @testSetup
    static void testRecords(){
        id recType = Schema.SObjectType.contact.getRecordTypeInfosByDeveloperName().get('Contacts').getRecordTypeId();
        contact con = new contact();
        con.recordtypeid=recType;
        con.LastName='contactrec';
        con.FirstName='test';
        con.Email='test@contact.com';        
        insert con;
        
        Kinship_Case__c caserec = new Kinship_Case__c();
        caserec.Primary_Contact__c=con.id;
        insert caserec;
        
        //id vendorrecType = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Vendor').getRecordTypeId();
        account accountrec = new account();
        //accountrec.recordtypeid=vendorrecType;
        accountrec.Name='test vendor';
        accountrec.Tax_ID__c='222-22-3333';
        accountrec.Entity_Type__c='Individual';
        insert accountrec;
        
         Locality_Account__c la = new Locality_Account__c();
        la.Name='222';
        insert la;
        
        Category__c category = new Category__c();
        category.Name='222';
        category.Locality_Account__c =la.id;
        category.Category_Type__c='Adoption';
        category.RecordTypeId= Schema.SObjectType.category__C.getRecordTypeInfosByDeveloperName().get('LASER_Category').getRecordTypeId();
        insert category;  
        
        TriggerActivation__c  ta = new TriggerActivation__c ();
        ta.name='PaymentTrigger';
        ta.isActive__c=true;
        insert ta;
        TriggerActivation__c  ta1 = new TriggerActivation__c();
        ta1.name='PurchaseOrder Trigger';
        ta1.isActive__c=true;
        insert ta1;
        
        FIPS__c fp = new FIPS__c();
        fp.name='test fips';
        insert fp;
        id recTypeid = Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Fiscal_Year').getRecordTypeId();
        campaign cap = new campaign();
        cap.FIPS__c=fp.id;
        cap.RecordTypeId=recTypeid;
        cap.Fiscal_Year_Type__c='Local';
        cap.name='test';
        cap.Begin_Date__c=system.today().addmonths(-1);
        cap.EndDate=system.today().addyears(1);
        insert cap;
        Check_Run__c cr = new Check_Run__c();
        cr.Check_Date__c =system.today();
        cr.Number_Of_Check__c=1;
        insert cr;
        
        Kinship_Check__c kc = new Kinship_Check__c();
        kc.Check_Run__c=cr.id;
        kc.Issue_Date__c=system.today();
        insert kc;
        
    }   
    
    public static testmethod void CheckRunTest(){
    	list<TriggerActivation__c> talist=[select id,name,isActive__C from TriggerActivation__c where isActive__c=:true 
                                           and name=:'PaymentTrigger'];
        list<TriggerActivation__c> talist1=[select id,name,isActive__C from TriggerActivation__c where isActive__c=:true 
                                            and name=:'PurchaseOrder Trigger'];
        TriggerActivation__c  ta1 = new TriggerActivation__c();
        ta1.name='MergedCheck';
        ta1.isActive__c=true;
        insert ta1;
        list<Kinship_Case__c> cases=[select id from Kinship_Case__c];
        list<account> accounts=[select id,recordtypeid from account where name='test vendor'];        
        list<Category__c> categories=[select id from Category__c];
          //insert a new product
        Product2 p = new product2(name='x',isactive=true,Unit_Measure__c='1');
        insert p;
        
        Product2 p2 = new product2(name='y',isactive=true,Unit_Measure__c='1');
        insert p2;
        list<fips__C> fip=[select id,name from fips__C];
        list<campaign> camp=[select id,fips__c,Begin_Date__c,enddate,Fiscal_Year_Type__c from campaign];
        //define the standart price for the product
        id stdPb = Test.getStandardPricebookId();
        insert new PricebookEntry(pricebook2id = stdPb, product2id = p.id,unitprice=1.0, isActive=true);
        
        Pricebook2 pb = new pricebook2(name='test',Vendor__c=accounts[0].id,IsActive=true,
                                      accounting_period__c=camp[0].id);
        insert pb;
        
        PricebookEntry pbe = new PricebookEntry(pricebook2id=pb.id, product2id=p.id,unitprice=1000.0, isActive=true);
        insert pbe; 
        id recType = Schema.SObjectType.opportunity.getRecordTypeInfosByDeveloperName().get('Purchase_Order').getRecordTypeId();
       test.starttest();
         opportunity opprecord = new opportunity();
        opprecord.CloseDate=date.today();
        opprecord.StageName='Draft';
        opprecord.Name='test opportunity';
        opprecord.RecordTypeId=recType;
        opprecord.Kinship_Case__c=cases[0].id;
        opprecord.CampaignId=camp[0].id;
        opprecord.Amount=2000;
        opprecord.Category__c =categories[0].id;
        opprecord.AccountId=accounts[0].id;
        opprecord.Number_of_Months_Units__c=1;
        opprecord.Pricebook2Id =pb.id;        
        insert opprecord;
        //NOW CREATING LINE ITEMS
        opportunitylineitem opplineitem = new opportunitylineitem();
        opplineitem.Unit_Measure__c='1';
        opplineitem.Product2Id=p.id;
        opplineitem.PricebookEntryId=pbe.id;
        opplineitem.Quantity=2;
        opplineitem.Category__c=categories[0].id;
        opplineitem.OpportunityId=opprecord.id;
        opplineitem.TotalPrice = 2000;
        insert opplineitem;
        //Create an approval request for the po
    
        Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitrequest();
        app.setObjectId(opprecord.Id);    
     
        Approval.ProcessResult result = Approval.process(app);    

        System.assert(result.isSuccess());
      
        if(result.isSuccess()){
            opprecord.stagename='Approved';
            update opprecord;
         test.stopTest();   
            list<Payment__c> payments=[select id,Written_Off__c,Amount_Paid__c,Paid__c,Kinship_Check__c,Payment_Date__c
                                                 from Payment__c
                                                    where Opportunity__c=:opprecord.Id];
          
           Check_Run__c cr = new Check_Run__c();
            cr.Check_Date__c =system.today();
            cr.Number_Of_Check__c=1;
            insert cr;
            
            Kinship_Check__c kc = new Kinship_Check__c();
            kc.Check_Run__c=cr.id;
            kc.Issue_Date__c=system.today();
            insert kc;
            
            payments[0].Kinship_Check__c = kc.id;
            update payments[0];
            try{
            	payments[0].Payment_Date__c = system.today().adddays(-1);
                update payments[0];
            }
            catch(Exception e){}
            
            cr.Check_Date__c = system.today().adddays(1);
            update cr;
            list<Payment__c> payments1 =[select id,Payment_Date__c from Payment__c where id=:payments[0].id];
            system.debug('**VAl of payment'+payments1[0]);
            system.assertEquals(payments1[0].Payment_Date__c, system.today().adddays(1));
           
           
    	}    
    }
}