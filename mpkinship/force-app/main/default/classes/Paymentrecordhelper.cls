/*****
It's a helper class for PaymentTrigger 
*********/
public class Paymentrecordhelper{
    public static boolean isRun=false;
    public static boolean isOpportunity=false;
    public static boolean ischange=false;
    public static boolean isidupdate = false;
    public static boolean isPaymentItemRun=false;
    public static boolean isWriteoffChange = false;
    public static boolean isPaymentCArun = false;
    public static boolean isPaymentUpdated=false;
    //FOR PAYMENT DATE CHANGE
    public static boolean IsPaymentDateUpdate=false;
    public static boolean IsDateUpdateRun=false;
    public static boolean IsClaimUpdateInsert=false;
    public static boolean IsClaimUpdateDelete=false;
    public static boolean IsClaimUpdadtPaymentPaid=false;
    public static boolean IsFicaPayments=false;
    public static boolean IsPaymentDelete=false;
    public static boolean isSWAUpdate=false;
    public static boolean isSWADelete=false;
    public static boolean isNameChange=false;
    public static boolean isNameChangeUpdate = false;
    
   /**POPULATE COSTCODE**/
   /*public static void FetchCostcode(list<payment__c> payments,set<id> categoryids,set<id> fipsids){
       list<Category__c> categories=[select id,Locality_Account__c from category__C where id in: categoryids and Locality_Account__c!=null];
       
       map<id,list<category__C>> LocalityCategoryMap = new map<id,list<category__C>>();
       for(Category__C category : categories){
           if(LocalityCategoryMap.containskey(category.Locality_Account__c)){
              LocalityCategoryMap.get(category.Locality_Account__c).add(category);   
           }
           else
              LocalityCategoryMap.put(category.Locality_Account__c,new list<category__C>{category});
       }
        
       map<string,id> CatFipsWithCostCodeMap = new map<String,id>();
       list<Chart_of_Account__c> charts=[select id,Locality_Account__c,Cost_Center__c,FIPS__c from Chart_of_Account__c where 
                                         Locality_Account__c in:LocalityCategoryMap.keyset() and  FIPS__c in: fipsids and Cost_Center__c!=null];
        for(Chart_of_Account__c chart : charts){
            
            if(LocalityCategoryMap.size()>0 && LocalityCategoryMap.containskey(chart.Locality_Account__c)){               
                for(category__c categoryrec : LocalityCategoryMap.get(chart.Locality_Account__c)){                    
                    if(!CatFipsWithCostCodeMap.containskey(categoryrec.id+'-'+chart.FIPS__c ))
                        CatFipsWithCostCodeMap.put(categoryrec.id+'-'+chart.FIPS__c,chart.Cost_Center__c);
                }
            }
        
        }  
        
        for(Payment__c paymentrec : payments){           
            if(CatFipsWithCostCodeMap.size()>0 && CatFipsWithCostCodeMap.containskey(paymentrec.Category_lookup__c+'-'+paymentrec.FIPS_Code__c)){                
                paymentrec.Cost_Center__c =CatFipsWithCostCodeMap.get(paymentrec.Category_lookup__c+'-'+paymentrec.FIPS_Code__c);
            }
        } 
   } */
  
    /****CHANGE STAGE ****/
    public static void changeStage(set<id> opportunitiesids,string change){
        system.debug('enterd in stage change class method');
       if(change=='Paid')
           ischange=true;
       else 
           isWriteoffChange=true;
       map<opportunity,integer> oppaymentmap = new map<opportunity,integer>();
       for(opportunity opprecord:[select id,stagename,(select id,Payment_Amount__c,Scheduled_Date__c,Paid__c,
                          Written_Off__c from Payment_OppPayment__r) from opportunity where id in: opportunitiesids]){
           oppaymentmap.put(opprecord,opprecord.Payment_OppPayment__r.size());                   
       }
       system.debug('**Val of oppaypamentmap'+oppaymentmap);
       list<opportunity> opplist = new list<opportunity>();
       for(opportunity opprecord:[select id,stagename,(select id,Payment_Amount__c,Scheduled_Date__c,Paid__c,
                          Written_Off__c from Payment_OppPayment__r where Paid__c=true or Written_Off__c=true ) from opportunity where id in: opportunitiesids]){

           if(oppaymentmap.containskey(opprecord)){
               system.debug('**Val of payment total count'+oppaymentmap.get(opprecord));
               system.debug('**VAl of paid and unpaid'+opprecord.Payment_OppPayment__r.size());
               if(oppaymentmap.get(opprecord)==opprecord.Payment_OppPayment__r.size()){
                   opportunity opp = new opportunity();
                   opp.id =opprecord.id;
                   opp.stagename='Closed';
                   opplist.add(opp);
               }    
           }             
       } 
       if(opplist.size()>0)
           update opplist;    
    }
    
    public static void populaterecordid(list<Payment__c> payments){
        Id swrRecordTypeId =Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Special_Welfare_Reimbursement').getRecordTypeId();
        Id adjRecordTypeId =Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Adjustments').getRecordTypeId();  
        isidupdate =true;

        // get date for reciept number.
        List<String> dtLst = system.today().format().split('/');
        String dt = dtLst[0] + dtLst[2].substring(2);
        
        list<Payment__c> paymentstoupdate = new list<Payment__c>();
        for(Payment__c payment : payments){
            Payment__c  paymentrec = new Payment__c();
            paymentrec.id = payment.id;
            
            // set reciept number.
            if(swrRecordTypeId == payment.RecordTypeId)
            {
                 paymentrec.Report_Number__c = 'SWR-'+ dt + '-' + payment.Name.split('-')[1]; 
            }
            else if(adjRecordTypeId == payment.RecordTypeId)
            {
                 paymentrec.Report_Number__c = 'ADJ-'+ dt + '-' + payment.Name.split('-')[1]; 
            }
            paymentrec.Locality_Payment_Identifier__c =string.valueof(payment.id).substring(0,15); 
            paymentstoupdate.add(paymentrec );
        } 
       if(paymentstoupdate.size()>0)
           update paymentstoupdate;        
        
    }
    
    /*********METHOD TO CREATE LINE ITEMS OF PAYMENTS ***/
   /* public static void createPaymentLineItems(set<id> paymentids){
        isPaymentItemRun=true;
        set<id> opportunitiesids = new set<id>();
        
        list<Payment__c> payments=[select id,Opportunity__c,Category_lookup__c from Payment__c where id in:paymentids];
        for(Payment__c payment : payments){
            opportunitiesids.add(payment.Opportunity__c);    
        }
       Id porecTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Purchase_Order').getRecordTypeId(); 
       list<opportunity> opportunities =[select id,name,Number_of_Months_Units__c,(Select id,OpportunityId,Product2Id,ProductCode,Unit_Measure__c,
                                        Quantity,UnitPrice,Category__c,opportunity.Number_of_Months_Units__c from OpportunityLineItems) from opportunity 
                                           where id IN :opportunitiesids and recordtypeid=:porecTypeId ]; 
       map<id,list<OpportunityLineItem>> oppOppLineItemMap = new map<id,list<OpportunityLineItem>>();
       for(opportunity opp : opportunities){
           for(OpportunityLineItem opplineitem : opp.OpportunityLineItems){
               if(oppOppLineItemMap.containskey(opp.id)){
                   oppOppLineItemMap.get(opp.id).add(opplineitem);
               }  
               else
                   oppOppLineItemMap.put(opp.id,new list<OpportunityLineItem>{opplineitem});
           }
       } 
       
       list<Payment_Line_Item__c> payitemlist = new list<Payment_Line_Item__c>();
       //CHECK IF MAP NOT NULL
       if( oppOppLineItemMap.size()>0){
           for(Payment__c  payment : payments){
               if(oppOppLineItemMap.containskey(payment.Opportunity__c)){
                   integer count=0;
                   for(OpportunityLineItem oppitem: oppOppLineItemMap.get(payment.Opportunity__c)){
                       system.debug('**oppitem cat'+ oppitem.category__c);
                       system.debug('**payment cat'+ payment.Category_lookup__c);
                       if(oppitem.Category__c==payment.Category_lookup__c){
                           Payment_Line_Item__c pitem = new Payment_Line_Item__c();
                           pitem.Opportunity_Product__c = oppitem.id;
                           pitem.invoice__c = payment.id;
                           pitem.LineItem_Index__c = count;
                           //pitem.Maximum_Quantity_Authorized_text__c=oppitem.quantity;
                           pitem.Max_Hours_Authorized__c= oppitem.quantity/oppitem.opportunity.Number_of_Months_Units__c >0 ? oppitem.quantity/oppitem.opportunity.Number_of_Months_Units__c : 0;
                           count = count+1;
                           payitemlist.add(pitem);  
                       }                     
                   }
               }           
           }
       } 
       
       if(payitemlist.size()>0)
           insert payitemlist;                                  
                                          
        
    } */
    
     /*********METHOD TO CREATE LINE ITEMS OF PAYMENTS ***/
    public static void createPaymentLineItems1(set<id> paymentids){
        isPaymentItemRun=true;
        set<id> opportunitiesids = new set<id>();
        
        list<Payment__c> payments=[select id,Opportunity__c,Category_lookup__c,Scheduled_Date__c from Payment__c where id in:paymentids];
        for(Payment__c payment : payments){
            opportunitiesids.add(payment.Opportunity__c);    
        }
       Id porecTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Purchase_Order').getRecordTypeId(); 
       list<opportunity> opportunities =[select id,name,Number_of_Months_Units__c,(Select id,OpportunityId,Product2Id,ProductCode,Unit_Measure__c,
                                        Quantity,UnitPrice,Category__c,opportunity.Number_of_Months_Units__c,Month_Units__c from OpportunityLineItems) from opportunity 
                                           where id IN :opportunitiesids and recordtypeid=:porecTypeId ]; 
       map<id,list<OpportunityLineItem>> oppOppLineItemMap = new map<id,list<OpportunityLineItem>>();
       for(opportunity opp : opportunities){
           for(OpportunityLineItem opplineitem : opp.OpportunityLineItems){
               if(oppOppLineItemMap.containskey(opp.id)){
                   oppOppLineItemMap.get(opp.id).add(opplineitem);
               }  
               else
                   oppOppLineItemMap.put(opp.id,new list<OpportunityLineItem>{opplineitem});
           }
       } 
       
       list<Payment_Line_Item__c> payitemlist = new list<Payment_Line_Item__c>();
       /**CHECK IF MAP NOT NULL ***/
       if( oppOppLineItemMap.size()>0){
           for(Payment__c  payment : payments){
               if(oppOppLineItemMap.containskey(payment.Opportunity__c)){
                   integer count=0;
                   for(OpportunityLineItem oppitem: oppOppLineItemMap.get(payment.Opportunity__c)){
                       system.debug('**oppitem cat'+ oppitem.category__c);
                       system.debug('**payment cat'+ payment.Category_lookup__c);
                       if(oppitem.Category__c==payment.Category_lookup__c){
                           Payment_Line_Item__c pitem = new Payment_Line_Item__c();
                           pitem.Opportunity_Product__c = oppitem.id;
                           pitem.invoice__c = payment.id;
                           pitem.LineItem_Index__c = count;
                           if(oppitem.Month_Units__c!=null){
                               if(payment.Scheduled_Date__c!=null){
                                   string datestring = string.valueof(payment.Scheduled_Date__c);
                                   list<string> splitstring = datestring.split('-');
                                   string monthval1 = splitstring[0]+'-'+splitstring[1];
                                   list<String> monthunits=oppitem.Month_Units__c.split(',');
                                   boolean counter=false;
                                   for(string s : monthunits){
                                       list<String> splitmonthvalue=s.split(':');
                                       if(splitmonthvalue.size()>1 && splitmonthvalue[0]!=null && splitmonthvalue[0]==monthval1){
                                           if(splitmonthvalue[1]!=null)
                                               pitem.Max_Hours_Authorized__c=decimal.valueof(splitmonthvalue[1]);
                                              counter=true; 
                                              break;    
                                       }
                                   }
                                   if(counter==false)
                                       pitem.Max_Hours_Authorized__c= oppitem.quantity/oppitem.opportunity.Number_of_Months_Units__c >0 ? oppitem.quantity/oppitem.opportunity.Number_of_Months_Units__c : 0;
                               }
                           }
                           //pitem.Maximum_Quantity_Authorized_text__c=oppitem.quantity;
                           else
                               pitem.Max_Hours_Authorized__c= oppitem.quantity/oppitem.opportunity.Number_of_Months_Units__c >0 ? oppitem.quantity/oppitem.opportunity.Number_of_Months_Units__c : 0;
                           count = count+1;
                           payitemlist.add(pitem);  
                       }                     
                   }
               }           
           }
       } 
       
       if(payitemlist.size()>0)
           insert payitemlist;                                  
                                          
        
    }
    
    //PAYMENT CLAIM UPDATE ON INSERT OF PAYMENT 
    public static void ClaimUpdate(set<id> paymentids){
        if(trigger.isInsert)
            IsClaimUpdateInsert=true;
       /* if(trigger.isDelete)
            IsClaimUpdateDelete=true; */
        if(Trigger.isupdate)
            IsClaimUpdadtPaymentPaid=true;           
        map<id,list<Payment__c>> claimPaymentsMap = new map<id,list<Payment__c>>();
         Id  caseActionrecordTypeId =Schema.SObjectType.opportunity.getRecordTypeInfosByDeveloperName().get('Case_Actions').getRecordTypeId();  
        list<Payment__c> payments=[select id,Opportunity__c,Claims__c,Payment_Amount__c,Opportunity__r.recordtypeid,
                                        Paid__c,Written_Off__c,Invoice_Stage__c from Payment__c
                                where id in: paymentids and Opportunity__r.recordtypeid=: caseActionrecordTypeId ];
        system.debug('**VAl of payments'+ payments);
        for(Payment__c payment : payments){
            if(claimPaymentsMap.size()>0 && claimPaymentsMap.containskey(payment.Claims__c)){
                claimPaymentsMap.get(payment.Claims__c).add(payment);
            }
            else
               claimPaymentsMap.put(payment.Claims__c ,new list<Payment__c>{payment});
        
        } 
        system.debug('**VAl of claimpaymentmap'+claimPaymentsMap);
        list<Claims__c> claims=[select id,Claims_Payable_Due__c,Claims_Payable_Paid__c from Claims__c where id in: claimPaymentsMap.keyset()];
        list<claims__c> claimToUpdate = new list<claims__c>();
        for(Claims__c claim : claims){
            if(claimPaymentsMap.size() >0 && claimPaymentsMap.containskey(claim.id)){
                decimal paidpaymentsAmount=0;
                decimal unpaidPaymentsAmount=0;
                for(Payment__c payment : claimPaymentsMap.get(claim.id)){
                    if(payment.Paid__c==true && payment.Invoice_Stage__c=='Paid' && payment.Payment_Amount__c!=0){
                         paidpaymentsAmount=payment.Payment_Amount__c;   
                    }
                    else if(payment.Paid__c==false && payment.Invoice_Stage__c=='Ready to Pay')
                        unpaidPaymentsAmount=payment.Payment_Amount__c;
                }
                system.debug('**Val of paidpaymentsamount'+paidpaymentsAmount);
                system.debug('**VAl of unpaidpaymentamount'+ unpaidPaymentsAmount);
               Claims__c  claimupdate = new Claims__c ();
               claimupdate.id=claim.id;
               if(Trigger.isInsert){
                   if(claim.Claims_Payable_Paid__c!=null)                      
                       claimupdate.Claims_Payable_Paid__c= claim.Claims_Payable_Paid__c + paidpaymentsAmount;
                   else
                       claimupdate.Claims_Payable_Paid__c= paidpaymentsAmount;
                   if(claim.Claims_Payable_Due__c!=null)    
                       claimupdate.Claims_Payable_Due__c= claim.Claims_Payable_Due__c + unpaidPaymentsAmount; 
                   else
                       claimupdate.Claims_Payable_Due__c= unpaidPaymentsAmount;    
               }
            /*   else if(trigger.isDelete){
                    system.debug('**Val of claim payable due'+   claim.Claims_Payable_Due__c);
                  system.debug('**VAl of unpaidamount'+  unpaidPaymentsAmount);
                    if(claim.Claims_Payable_Paid__c!=null) 
                       claimupdate.Claims_Payable_Paid__c= claim.Claims_Payable_Paid__c - paidpaymentsAmount;
                    else
                       claimupdate.Claims_Payable_Paid__c= 0;
                       
                    if(claimupdate.Claims_Payable_Due__c!=null)       
                       claimupdate.Claims_Payable_Due__c= claim.Claims_Payable_Due__c - unpaidPaymentsAmount;
                   else
                       claimupdate.Claims_Payable_Due__c=0;    
                 
               } */
               else if(Trigger.isUpdate){
                   if(claim.Claims_Payable_Paid__c!=null)                      
                       claimupdate.Claims_Payable_Paid__c= claim.Claims_Payable_Paid__c + paidpaymentsAmount;
                   else
                       claimupdate.Claims_Payable_Paid__c= paidpaymentsAmount;
                   if(claim.Claims_Payable_Due__c!=null)
                       claimupdate.Claims_Payable_Due__c = claim.Claims_Payable_Due__c - paidpaymentsAmount;    
                       
               } 
               claimToUpdate.add(claimupdate); 
            }
        }
        
        if(claimToUpdate.size()>0)
            update claimToUpdate;
                                
    
    }
    
  
    public static void CreateCaseActionsForFICAPayments(set<id> paymentids){
        IsFicaPayments=true;
        //id caseActionTypeId= Schema.SObjectType.opportunity.getRecordTypeInfosByDeveloperName().get('NPSP_Default').getRecordTypeId();
        id POSOrecordTypeId= Schema.SObjectType.opportunity.getRecordTypeInfosByDeveloperName().get('Purchase_Order').getRecordTypeId();
        list<Payment__c> payments=[select id,Payment_Method__c,Category_lookup__c,Category_lookup__r.Category_Type__c,Opportunity__c,Cost_Center__c,Opportunity__r.recordtypeid,
                                    Opportunity__r.name,Opportunity__r.closedate, Opportunity__r.End_Date__c,Opportunity__r.Number_of_Months_Units__c,
                                     Accounting_Period__c,Kinship_Case__c,FICA_Withholding__c,FIPS_Code__c,Opportunity__r.campaignid,Scheduled_Date__c from 
                                     Payment__c where id in: paymentids and Category_lookup__r.Category_Type__c='Companion Payroll' and FICA_Withholding__c >:0
                                     and Opportunity__r.recordtypeid=:POSOrecordTypeId];
        
        //id ficaTaxRatetypeId = Schema.SObjectType.Tax_Rates__c.getRecordTypeInfosByDeveloperName().get('FICA').getRecordTypeId();
        list<Tax_Rates__c> ficas=[select id,Type__c,Employee_Rate__c,Vendor__c,Category__c from Tax_Rates__c where Type__c='FICA'
                                  and Active__c=:true and Employee_Rate__c >0];
        Id caseActionRecordtypeid =  Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Case_Actions').getRecordTypeId();                          
         
          map<Payment__c,list<opportunity>> PaymentCaseActionMap = new map<Payment__c,list<opportunity>>();
          list<opportunity> caseactionsToInsert = new list<opportunity>();
        if(ficas.size() >0){                           
            for(Payment__c payment : payments){
                Opportunity caseaction1 = new opportunity();
                caseaction1.name='x';
                caseaction1.recordtypeid= caseActionRecordtypeid;
                if(ficas[0].Vendor__c!=null)
                    caseaction1.accountid = ficas[0].Vendor__c;
                if(ficas[0].Category__c !=null)
                    caseaction1.Category__c=ficas[0].Category__c;
                caseaction1.Type_of_Case_Action__c='One time';
                caseaction1.closedate = payment.Opportunity__r.closedate;
                caseaction1.End_Date__c=payment.Opportunity__r.End_Date__c;
                caseaction1.Number_of_Months_Units__c=payment.Opportunity__r.Number_of_Months_Units__c;
                caseaction1.campaignid = payment.Opportunity__r.campaignid;
                caseaction1.Kinship_Case__c=payment.Kinship_Case__c;
                caseaction1.amount = payment.FICA_Withholding__c;
                caseaction1.Fund__c='Non-reimbursable';
                caseaction1.description='EMPLOYEE FICA';
                caseaction1.stagename='Open';
                caseaction1.FIPS_Code__c=payment.FIPS_Code__c;
                caseaction1.Cost_Center__c= payment.Cost_Center__c;
               
                if(PaymentCaseActionMap.size()>0 && PaymentCaseActionMap.containskey(payment))
                    PaymentCaseActionMap.get(payment).add(caseaction1);
                else
                    PaymentCaseActionMap.put(payment,new list<opportunity>{caseaction1});  
                caseactionsToInsert.add(caseaction1);     
                
               //CREATING SECOND CASEACTION 
               opportunity caseaction2 = new opportunity();
                caseaction2.recordtypeid= caseActionRecordtypeid;
                caseaction2.name='x';
                if(ficas[0].Vendor__c!=null)
                    caseaction2.accountid = ficas[0].Vendor__c;
                if(ficas[0].Category__c !=null)
                    caseaction2.Category__c=ficas[0].Category__c;
                caseaction2.Type_of_Case_Action__c='One time';
                caseaction2.closedate = payment.Opportunity__r.closedate;
                caseaction2.End_Date__c=payment.Opportunity__r.End_Date__c;
                caseaction2.Number_of_Months_Units__c=payment.Opportunity__r.Number_of_Months_Units__c;
                caseaction2.campaignid = payment.Opportunity__r.campaignid;
                caseaction2.Kinship_Case__c=payment.Kinship_Case__c;
                caseaction2.amount = payment.FICA_Withholding__c;
                caseaction2.Fund__c='Reimbursable';
                caseaction2.description='EMPLOYER FICA';
                caseaction2.stagename='Open';
                caseaction2.FIPS_Code__c=payment.FIPS_Code__c;
                caseaction2.Cost_Center__c= payment.Cost_Center__c; 
                
                if(PaymentCaseActionMap.size()>0 && PaymentCaseActionMap.containskey(payment))
                    PaymentCaseActionMap.get(payment).add(caseaction2);
                else
                    PaymentCaseActionMap.put(payment,new list<opportunity>{caseaction2});         
                caseactionsToInsert.add(caseaction2);    
            } 
        }  
        
        if(caseactionsToInsert.size()>0)
            insert caseactionsToInsert; 
            
        //NOW CREATING PAYMENTS FOR EACH CREATED CASE ACTIONS
        list<Payment__c> paymentsOfCaseActions = new list<Payment__c>();
        Id VendorInvoicerecordTypeId =Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Vendor_Invoice').getRecordTypeId(); 
        //for(id paymentid : PaymentCaseActionMap.keyset()){
        for(Payment__c paymentrec : PaymentCaseActionMap.keyset()){
            for(opportunity opp : PaymentCaseActionMap.get(paymentrec)){
                Payment__c payment = new Payment__c();
                payment.recordtypeid = VendorInvoicerecordTypeId ;
                payment.Opportunity__c= opp.id;
                payment.FIPS_Code__c=opp.FIPS_Code__c;
                payment.Vendor__c=opp.accountid;
                payment.Kinship_Case__c=opp.Kinship_Case__c;
                payment.Service_Begin_Date__c=opp.closedate;
                payment.Service_End_Date__c=opp.End_Date__c;
                payment.Scheduled_Date__c=opp.closedate;
                payment.Amount_Paid__c=opp.amount;
                payment.Payment_Amount__c=opp.amount;
                if(opp.Cost_Center__c!=null)
                    payment.Cost_Center__c= opp.Cost_Center__c;
                if(opp.Category__c !=null)
                    payment.Category_lookup__c=opp.Category__c ;    
                payment.Description__c=opp.description;
                payment.Fund__c=opp.Fund__c;
                payment.FICA_Payment_Id__c=paymentrec.id;  
                if(ficas.size()>0)
                    payment.Tax_Rate__c=ficas[0].id;
                payment.Payment__c=paymentrec.id;    
                //CHANGED FOR KIN-1478  
                payment.Invoice_Stage__c='Pending';
                payment.Payment_Date__c=date.today();
                //ADDING IT FROM PAYMENT PREVIOUS
                payment.Payment_Method__c=paymentrec.Payment_Method__c;
               // payment.npe01__Payment_Method__c='Journal Entry';    
                paymentsOfCaseActions.add(payment);
            }
        } 
        
        if(paymentsOfCaseActions.size()>0)
            insert paymentsOfCaseActions;                                                   
    } 
    
    public static void DeleteCaseActionsOfFICA(set<id> paymentids){
        IsPaymentDelete=true;
        //id caseActionTypeId= Schema.SObjectType.opportunity.getRecordTypeInfosByDeveloperName().get('NPSP_Default').getRecordTypeId();
        id POSOrecordTypeId= Schema.SObjectType.opportunity.getRecordTypeInfosByDeveloperName().get('Purchase_Order').getRecordTypeId();
        list<Payment__c> payments=[select id,FICA_Payment_Id__c,Category_lookup__c,Opportunity__r.recordtypeid,Category_lookup__r.Category_Type__c 
                                            from Payment__c where id in:paymentids and Category_lookup__r.Category_Type__c=:'Companion Payroll'
                                            and Opportunity__r.recordtypeid=:POSOrecordTypeId];
        set<id> Parentpaymentids= new set<id>();
        for(Payment__c payment : payments){
            Parentpaymentids.add(payment.id);
        }  
        
        list<Payment__c> caseActionPayments=[select id,FICA_Payment_Id__c,Opportunity__c from Payment__c where FICA_Payment_Id__c in:paymentids];
        list<opportunity> caseActionsToDelete = new list<opportunity>();
        for(Payment__c paymentrec : caseActionPayments){
            opportunity caseaction = new opportunity();
            caseaction.id =paymentrec.Opportunity__c;
            caseActionsToDelete.add(caseaction);
        } 
                                         
        if(caseActionsToDelete.size()>0)
            delete caseActionsToDelete;                                    
    }
    
    //UPDATE SWA VALUES NEW APPROACH
   /* public static void updateSWA(set<id> swaids){
        system.debug('**Swa ids'+ swaids);
        if(trigger.isupdate)
            isSWAUpdate=true;
        if(trigger.isdelete)
            isSWADelete=true; 
            
        list<Special_Welfare_Account__c> swasToUpdate = new list<Special_Welfare_Account__c>();    
        list<Special_Welfare_Account__c> swas =[select id,Total_Receipts__c,Total_Payments__c,Child_Support__c,Child_Support_Payments__c,
                             SSA__c,SSA_SSI_Payments__c from Special_Welfare_Account__c where id in: swaids];
        system.debug('**VAl of swas'+ swas);                                        
        try{
            List<AggregateResult>  rece = [SELECT RecordType.developerName rtname, Special_Welfare_Account__c, sum(Payment_Amount__c) amt, Payment_Source__c pstype 
                                           FROM Payment__c WHERE Special_Welfare_Account__c IN : swaids and Category_lookup__r.Category_Type__c='Special Welfare'
                                           GROUP BY RecordType.developerName, Special_Welfare_Account__c, Payment_Source__c ]; 
            system.debug('**VAl off rece'+ rece);                                           
            for(Special_Welfare_Account__c swa: swas){
                Special_Welfare_Account__c  swarec = new Special_Welfare_Account__c();
                swarec.id= swa.id;
                swarec.Total_Receipts__c = 0.0;
                swarec.Total_Payments__c = 0.0;
                swarec.Child_Support__c= 0.0;
                swarec.Child_Support_Payments__c= 0.0;
                swarec.SSA__c= 0.0;
                swarec.SSA_SSI_Payments__c= 0.0;
                
                for(AggregateResult re: rece) {
                    system.debug('**Al of re1'+ re); 
                    if(swa.Id == re.get('Special_Welfare_Account__c')){
                        system.debug('**Enterd here');
                        if(re.get('rtname') == 'Vendor_Refund' || re.get('rtname') == 'Receipt')
                        {    system.debug('**Val of re entere in eceiptpart');
                           system.debug('*Decval'+ (Decimal)re.get('amt'));
                            //swarec.Total_Receipts__c += (Decimal)re.get('amt');
                            system.debug('**Toreceipts'+swarec.Total_Receipts__c);
                            if(re.get('pstype') == '5')
                            {
                                swarec.Child_Support__c+= (Decimal)re.get('amt');
                            }
                            else if(re.get('pstype') == '6')
                            {
                                swarec.SSA__c+= (Decimal)re.get('amt');
                            }
                            
                        }
                        
                        else if(re.get('rtname') == 'Vendor_Invoice' || re.get('rtname')=='Special_Welfare_Reimbursement')
                        {  system.debug('**Val of re enred in vi part');
                           system.debug('*total payments'+ (Decimal)re.get('amt'));
                           // swarec.Total_Payments__c += (Decimal)re.get('amt'); 
                            
                            if(re.get('pstype') == '5')
                            {
                                swarec.Child_Support_Payments__c+= (Decimal)re.get('amt');
                            }
                            else if(re.get('pstype') == '6')
                            {
                                swarec.SSA_SSI_Payments__c+= (Decimal)re.get('amt');
                            }
                        }
                           
                    }                                   
                } 
                system.debug('**VAl of swarec'+ swarec);
                swasToUpdate.add(swarec);    
            }
            
            if(swasToUpdate.size()>0)
                update swasToUpdate;
            
        }Catch(Exception e){}
    } */
    
    //UPDATE SWA VALUES
   /* public static void updateSWAValues(list<npe01__OppPayment__c> paymentlist){
        system.debug('**emterd here'+paymentlist);
        system.debug('paymentlist size'+paymentlist.size());
        Set<Id> paymentIds = new Set<Id>();
        Decimal paymentAmount = 0;
        if(Trigger.isupdate)
            isSWAUpdate=true;
        if(trigger.isdelete)
            isSWADelete=true;
        set<id> specialwelfareaccountids = new set<id>();
        for(npe01__OppPayment__c payment: paymentlist){
            if(payment.Special_Welfare_Account__c!=null)
                specialwelfareaccountids.add(payment.Special_Welfare_Account__c);    
        }
        
        //list<npe01__OppPayment__c> payments=[select id,Special_Welfare_Account__c,Special_Welfare_Account__r.Total_Receipts__c,
                           // Special_Welfare_Account__r.Child_Support__c,Special_Welfare_Account__r.SSA__c,Special_Welfare_Account__r.Total_Payments__c,
                           // Special_Welfare_Account__r.Child_Support_Payments__c,Special_Welfare_Account__r.SSA_SSI_Payments__c,recordtypeid,Payment_Source__c,npe01__Payment_Amount__c, */
     /* map<id,Special_Welfare_Account__c> swamap = new map<id,Special_Welfare_Account__c>();
       list<Special_Welfare_Account__c> swas=[select id,Child_Support__c,SSA__c,Total_Receipts__c, SSA_SSI_Payments__c,                   
                                            Total_Payments__c,Child_Support_Payments__c from Special_Welfare_Account__c where id in:specialwelfareaccountids];
              for(Special_Welfare_Account__c swa : swas){
                 swamap.put(swa.id,swa);     
              } 
         system.debug('***specialwelfaremap'+swamap);      
        Id ReceiptrecordTypeId =Schema.SObjectType.npe01__OppPayment__c.getRecordTypeInfosByDeveloperName().get('Receipt').getRecordTypeId();                                
        Id vendorRefundRTId = Schema.SObjectType.npe01__OppPayment__c.getRecordTypeInfosByName().get('Vendor Refund').getRecordTypeId();
        Id vendorInvoiceRTId = Schema.SObjectType.npe01__OppPayment__c.getRecordTypeInfosByName().get('Vendor Invoice').getRecordTypeId();
        id specialwelfarereimburseRTId=  Schema.SObjectType.npe01__OppPayment__c.getRecordTypeInfosByDeveloperName().get('Special_Welfare_Reimbursement').getRecordTypeId();
       list<Special_Welfare_Account__c > swatoupdate = new list<Special_Welfare_Account__c>();
       for(npe01__OppPayment__c payment : paymentlist){
           if(swamap.containskey(payment.Special_Welfare_Account__c)){
               system.debug('**VAl of specilawelfare inside'+payment.Special_Welfare_Account__c); 
               Special_Welfare_Account__c swa = swamap.get(payment.Special_Welfare_Account__c);
               Special_Welfare_Account__c swanew = new Special_Welfare_Account__c();
               swanew.id=swa.id;
               if(payment.recordtypeid==ReceiptrecordTypeId  || payment.recordtypeid==vendorRefundRTId ){
                   if(SWA.Total_Receipts__c>0)
                       swanew.Total_Receipts__c = SWA.Total_Receipts__c-payment.npe01__Payment_Amount__c;
                       system.debug('**Val of total receipts'+(SWA.Total_Receipts__c-payment.npe01__Payment_Amount__c));
                     if(payment.Payment_Source__c  == '5')
                     {
                          swanew.Child_Support__c= swa.Child_Support__c-payment.npe01__Payment_Amount__c;
                     }
                     else if(payment.Payment_Source__c == '6'){
                            
                          swanew.SSA__c= swa.SSA__c-payment.npe01__Payment_Amount__c;
                     }       
               }
               else if(payment.recordtypeid==vendorInvoiceRTId || payment.recordtypeid==specialwelfarereimburseRTId){ 
                system.debug('payment.recordtypeid'+payment.recordtypeid); 
                paymentIds.add(payment.Payment__c);
                paymentAmount = paymentAmount + payment.npe01__Payment_Amount__c;
                   if(swa.Total_Payments__c >0)     
                       swanew.Total_Payments__c = SWA.Total_Payments__c -payment.npe01__Payment_Amount__c;
                       system.debug('swanew.Total_Payments__c'+swanew.Total_Payments__c); 
                    if(payment.Payment_Source__c == '5'){
                
                        swanew.Child_Support_Payments__c=swa.Child_Support_Payments__c-payment.npe01__Payment_Amount__c;
                        system.debug('swanew.Child_Support_Payments__c='+swanew.Child_Support_Payments__c); 
                    }
                    else if(payment.Payment_Source__c == '6'){
                
                        SWAnew.SSA_SSI_Payments__c=swa.SSA_SSI_Payments__c- payment.npe01__Payment_Amount__c;
                        system.debug('SWAnew.SSA_SSI_Payments__c'+SWAnew.SSA_SSI_Payments__c); 
                    }
              }
              swatoupdate.add(swanew);
               
           }
           
       }
       system.debug('**Swa to update'+ swatoupdate);
       if(swatoupdate.size()>0)
        update swatoupdate;
        system.debug('**paymentIds'+ paymentIds);
        system.debug('**paymentAmount'+ paymentAmount);
       /* List<npe01__OppPayment__c> venderInvoiceList = [Select Id,Prior_Refund1__c from npe01__OppPayment__c where ID In :paymentIds];
        List<npe01__OppPayment__c> tempList = new List<npe01__OppPayment__c>();
        npe01__OppPayment__c temp = new npe01__OppPayment__c();
        for(npe01__OppPayment__c payment : venderInvoiceList){
            temp.Id = payment.Id;
            if(payment.Prior_Refund1__c != null){
                temp.Prior_Refund1__c = payment.Prior_Refund1__c - paymentAmount;
            }
            tempList.add(temp);
            temp = new npe01__OppPayment__c();
        }
        system.debug('**tempList.size()'+ tempList.size());
        if(tempList.size() > 0 ){
            TransactionJournalService.byPassCheckPosted = true;
            update tempList;
            TransactionJournalService.byPassCheckPosted = false;
        } */
   // } 
    
    /**UPDATE GLOBAL VALUE NAME FIELD ***/
    Public static void PopulateGlobalSearchName(set<id> paymentids){
        
       if(trigger.isInsert)
            isNameChange=true;
        if(trigger.isupdate)
            isNameChangeUpdate=true;
                
        list<Payment__c> paymentstoUpdate = new list<Payment__c>();   
        
        list<Payment__c> payments=[select Global_Search_Name__c,Vendor__c,Vendor__r.name,Payment_Amount__c from Payment__c where id in: paymentids];
        system.debug('**VAl of payemnts'+payments);
        for(Payment__c paymentrec : payments){
            Payment__c payment = new Payment__c ();
            payment.id=paymentrec.id;
            string s='';
                if(paymentrec.vendor__c!=null){
                   s=paymentrec.Vendor__r.Name; 
                }
                system.debug('*Val of s after name'+s);
                if(paymentrec.Payment_Amount__c!=null){
                // Update amount text field.
                   // paymentrec.Amount__c = FactoryCls.formatCurr(paymentrec.npe01__Payment_Amount__c);
                   if(s!='')
                                            
                       s=s+' '+ FactoryCls.formatCurr(paymentrec.Payment_Amount__c);
                   else
                       s= FactoryCls.formatCurr(paymentrec.Payment_Amount__c);   
                }
             
                payment.Global_Search_Name__c=s;
                system.debug('**Val of s'+paymentrec.Global_Search_Name__c);
                if(s!=paymentrec.Global_Search_Name__c && trigger.isupdate)
                    paymentstoUpdate.add(payment);
                else
                    paymentstoUpdate.add(payment);    
        }
       
      
        if(paymentstoUpdate.size()>0 )
            update paymentstoUpdate; 
    }
}