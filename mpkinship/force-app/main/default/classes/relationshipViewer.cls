public class relationshipViewer {
	
    @AuraEnabled
    public static List<Contact> getContactWithRelationship(string contId){
        List<Contact> contMap = [Select id, Name,
                                  (Select Id,Type__c,Related_Contact__c,Related_Contact__r.Name from Relationships__r)
                                  FROM Contact
                                  WHERE Id=: contId
                                 ];
        return contMap;
    }
    
}