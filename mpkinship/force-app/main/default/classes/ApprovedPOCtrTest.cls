@isTest
public class ApprovedPOCtrTest {
	@testSetup
    static void testecords(){
        contact con = new contact();
        con.LastName='contactrec';
        con.FirstName='test';
        con.Email='test@contact.com';       
        insert con;
        Campaign parent = new Campaign();
        parent.name = '2021';
        parent.StartDate = Date.Today().addDays(-10);
        parent.EndDate = Date.Today().addDays(10);
        parent.Fiscal_Year_Type__c = 'Local';
        parent.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId();
        insert parent;
        Kinship_Case__c caserec = new Kinship_Case__c();
        caserec.Primary_Contact__c=con.id;
        caserec.DOB__c=system.today().adddays(-100);
        caserec.Race__c='1';
        caserec.Gender_Preference__c='M';
        caserec.Hispanic_Ethnicity__c='1';
        caserec.DSM_V_Indicator_ICD_10_1__c='Yes';
        caserec.Clinical_Medication_Indicator_1__c='Yes';
        caserec.Medicaid_Indicator_1__c='Yes';
        caserec.Referral_Source__c='Family';
        caserec.Autism_Flag_1__c='Yes';
        insert caserec;
        
        id accountrecordtypeid =Schema.SObjectType.account.getRecordTypeInfosByDeveloperName().get('Vendor').getRecordTypeId();
        account accountrec = new account();
        accountrec.Name='test vendor';
        accountrec.Tax_ID__c='333-33-3333';
        accountrec.recordtypeid=accountrecordtypeid;
        accountrec.Entity_Type__c='Individual';
        insert accountrec;
        
        Locality_Account__c la = new Locality_Account__c();
        la.Name='222';
        insert la;
        
        Category__c category = new Category__c();
        category.Name='222';
        category.Locality_Account__c=la.id;
        category.Category_Type__c='CSA';
        category.RecordTypeId= Schema.SObjectType.category__C.getRecordTypeInfosByDeveloperName().get('LEDRS_Category').getRecordTypeId();
        insert category;
        
        Category_Rate__c cr = new Category_Rate__c();
        cr.Category__c = category.id;
        cr.Status__c='Active';
        cr.Price__c=5000;
        cr.Effective_Date__c=system.today();
        cr.RecordTypeId = Schema.SObjectType.Category_Rate__c.getRecordTypeInfosByDeveloperName().get('Other_Rate').getRecordTypeId();
        insert cr;
        
    } 
     public static testmethod void testcreatePO(){
        list<TriggerActivation__c> talist =[select id,name,isactive__C from TriggerActivation__c where name='PurchaseOrder Trigger'];
        list<Kinship_Case__c> cases=[select id from Kinship_Case__c];
        list<account> accounts=[select id,recordtypeid from account where name='test vendor'];
         Campaign parent = new Campaign();
            parent.name = '2021';
            parent.StartDate = Date.Today().addDays(-10);
            parent.EndDate = Date.Today().addDays(10);
            parent.Fiscal_Year_Type__c = 'Local';
            parent.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId();
            insert parent;
        list<Category__c> categories=[select id from Category__c];
         //insert a new product
        Product2 p = new product2(name='x',isactive=true, Unit_Measure__c = '10');
        insert p;
        
        Product2 p2 = new product2(name='y',isactive=true, Unit_Measure__c = '10');
        insert p2;
        
        //define the standart price for the product
        id stdPb = Test.getStandardPricebookId();
        insert new PricebookEntry(pricebook2id = stdPb, product2id = p.id,unitprice=1.0, isActive=true);
        
        Pricebook2 pb = new pricebook2(name='test',Vendor__c=accounts[0].id,IsActive=true,
                                       Effective_Date__c=system.today().addmonths(-2), Accounting_Period__c = parent.id);
        insert pb;
        
        PricebookEntry pbe = new PricebookEntry(pricebook2id=pb.id, product2id=p.id,unitprice=1.0, isActive=true);
        insert pbe;   
        //id opprecType = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Case_Action').getRecordTypeId();
        opportunity opprecord = new opportunity();
        opprecord.CloseDate=date.today();
        opprecord.StageName='Draft';
        opprecord.Name='test opportunity';
        opprecord.Kinship_Case__c=cases[0].id;
        opprecord.Amount=20000;
        opprecord.Category__c =categories[0].id;
        opprecord.AccountId=accounts[0].id;
        
        opprecord.Number_of_Months_Units__c=3;
       // opprecord.RecordTypeId=opprecType;
        insert opprecord;
        system.assertEquals(20000, opprecord.amount);
        ApprovedPOCtr.UpdateState(opprecord.id);
    }
     public static testmethod void testcreatePO1(){
        list<TriggerActivation__c> talist =[select id,name,isactive__C from TriggerActivation__c where name='PurchaseOrder Trigger'];
        list<Kinship_Case__c> cases=[select id from Kinship_Case__c];
        list<account> accounts=[select id,recordtypeid from account where name='test vendor'];
         Campaign parent = new Campaign();
            parent.name = '2021';
            parent.StartDate = Date.Today().addDays(-10);
            parent.EndDate = Date.Today().addDays(10);
            parent.Fiscal_Year_Type__c = 'Local';
            parent.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId();
            insert parent;
        list<Category__c> categories=[select id from Category__c];
         //insert a new product
        Product2 p = new product2(name='x',isactive=true, Unit_Measure__c = '10');
        insert p;
        
        Product2 p2 = new product2(name='y',isactive=true, Unit_Measure__c = '10');
        insert p2;
        
        //define the standart price for the product
        id stdPb = Test.getStandardPricebookId();
        insert new PricebookEntry(pricebook2id = stdPb, product2id = p.id,unitprice=1.0, isActive=true);
        
        Pricebook2 pb = new pricebook2(name='test',Vendor__c=accounts[0].id,IsActive=true,
                                       Effective_Date__c=system.today().addmonths(-2), Accounting_Period__c = parent.id);
        insert pb;
        
        PricebookEntry pbe = new PricebookEntry(pricebook2id=pb.id, product2id=p.id,unitprice=1.0, isActive=true);
        insert pbe;   
        //id opprecType = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Case_Action').getRecordTypeId();
        opportunity opprecord = new opportunity();
        opprecord.CloseDate=date.today();
        opprecord.StageName='Draft';
        opprecord.Name='test opportunity';
        opprecord.Kinship_Case__c=cases[0].id;
        opprecord.Amount=20000;
        opprecord.Category__c =categories[0].id;
        opprecord.AccountId=accounts[0].id;
        
        opprecord.Number_of_Months_Units__c=3;
       // opprecord.RecordTypeId=opprecType;
        insert opprecord;
        system.assertEquals(20000, opprecord.amount);
        ApprovedPOCtr.UpdateState('test');
    }
     public static testmethod void testcreatePO2(){
        list<TriggerActivation__c> talist =[select id,name,isactive__C from TriggerActivation__c where name='PurchaseOrder Trigger'];
        list<Kinship_Case__c> cases=[select id from Kinship_Case__c];
        list<account> accounts=[select id,recordtypeid from account where name='test vendor'];
         Campaign parent = new Campaign();
            parent.name = '2021';
            parent.StartDate = Date.Today().addDays(-10);
            parent.EndDate = Date.Today().addDays(10);
            parent.Fiscal_Year_Type__c = 'Local';
            parent.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId();
            insert parent;
        list<Category__c> categories=[select id from Category__c];
         //insert a new product
        Product2 p = new product2(name='x',isactive=true, Unit_Measure__c = '10');
        insert p;
        
        Product2 p2 = new product2(name='y',isactive=true, Unit_Measure__c = '10');
        insert p2;
        
        //define the standart price for the product
        id stdPb = Test.getStandardPricebookId();
        insert new PricebookEntry(pricebook2id = stdPb, product2id = p.id,unitprice=1.0, isActive=true);
        
        Pricebook2 pb = new pricebook2(name='test',Vendor__c=accounts[0].id,IsActive=true,
                                       Effective_Date__c=system.today().addmonths(-2), Accounting_Period__c = parent.id);
        insert pb;
        
        PricebookEntry pbe = new PricebookEntry(pricebook2id=pb.id, product2id=p.id,unitprice=1.0, isActive=true);
        insert pbe;   
        //id opprecType = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Case_Action').getRecordTypeId();
        opportunity opprecord = new opportunity();
        opprecord.CloseDate=date.today();
        opprecord.StageName='Draft';
        opprecord.Name='test opportunity';
        opprecord.Kinship_Case__c=cases[0].id;
        opprecord.Amount=20000;
        opprecord.Category__c =categories[0].id;
        opprecord.AccountId=accounts[0].id;
        
        opprecord.Number_of_Months_Units__c=3;
       // opprecord.RecordTypeId=opprecType;
        insert opprecord;
        system.assertEquals(20000, opprecord.amount);
        ApprovedPOCtr.UpdateState('');
    }
}