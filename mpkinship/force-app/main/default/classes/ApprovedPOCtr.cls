public class ApprovedPOCtr{
    
    @auraEnabled
    public static RecordWrap UpdateState(string recordId){
        string msg='';
        try{
       // if(recordId!=null && recordId!=''){
            list<opportunity> opplist=[select id,name,stagename from opportunity where id=:recordId];
            if(opplist.size()>0 && opplist[0]!=null && opplist[0].stagename=='Draft'){
                opportunity opp = new opportunity();
                opp.id = opplist[0].id;
                opp.stagename='Approved';
                update opp;
                RecordWrap rw = new RecordWrap();
                rw.message='Record Updated';
                rw.recordId =opp.id;
                return rw;
                
            }  
        //}
        return null;
        }
          catch(Exception e){           
            system.debug('***msg'+e.getMessage());
            //throw all other exception message
            if(!Test.isRunningTest())
                throw new AuraHandledException(e.getMessage());
            else
                return null;
        }
    
    }
    
    public class RecordWrap{
        @auraEnabled
        public string message;
        @auraEnabled
        public string recordId;
    }
}