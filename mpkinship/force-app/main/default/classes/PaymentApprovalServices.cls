public class PaymentApprovalServices {
    public static String CREATED = 'Created';
    public static void updatePaymentStatus(List<Payment_Approval__c> lstNew, List<Payment_Approval__c> lstOld){
        List<String> lstId = new List<String>();
        for(Integer i = 0 ; i < lstNew.size() ; i++) {
            if(String.isNotBlank(lstNew[i].Status__c) && String.isNotBlank(lstOld[i].Status__c) &&
                lstNew[i].Status__c != lstOld[i].Status__c && 
                CREATED.equals(lstOld[i].Status__c)){
                lstId.add(lstNew[i].id);
            }
        }
        System.debug('---PaymentApprovalServices---' + lstId);
        if(lstId.size() > 0) {
            List<Payment_Approval_Step__c> lstPAS = [SELECT Id, Invoice__c, Payment_Approval__c, Payment_Approval__r.Status__c
                                                    FROM Payment_Approval_Step__c
                                                    WHERE Payment_Approval__c IN :lstId];
            System.debug('---PaymentApprovalServices---' + lstPAS);
            if(lstPAS.size() > 0) {
                List<Payment__c> lstPayment = new List<Payment__c>();
                for(Payment_Approval_Step__c pas : lstPAS){
                    Payment__c temp = new Payment__c();
                    temp.id = pas.Invoice__c;
                    temp.Status__c = pas.Payment_Approval__r.Status__c;
                    lstPayment.add(temp);
                }
                System.debug('---PaymentApprovalServices---' + lstPayment.size());
                PaymentTriggerHandler.bypassTrigger = true;
                update lstPayment;
                PaymentTriggerHandler.bypassTrigger = false;
            }
        }
    }
}