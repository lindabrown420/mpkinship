public without sharing class CaseController {
    @AuraEnabled(continuation = true)   
    public static Kinship_Case__c getCaseDetail(string recId){
        try{ 
            Kinship_Case__c cam = [Select Id,Primary_Contact__c,Primary_Contact__r.Name,Kinship_Case_Number__c,contact_Mailing_Street__c,Contact_Mailing_State__c, Race__c,Email_to_vendor__c, DOB__c,Age__c,Gender_Preference__c from Kinship_Case__c where Id =: recId];
            return cam;
        }
        catch(Exception e ){
            system.debug(e.getMessage() + ' e.getMessage() ???');
            return null;
        }
        
    }

    /*@AuraEnabled(continuation = true)   
    public static List<Programs__c> getProgramList(string recId){
        try{ 
            List<Programs__c> prog = [Select Id,Name from Programs__c where Case__c =: recId];
            return prog;
        }
        catch(Exception e ){
            system.debug(e.getMessage() + ' e.getMessage() ???');
            return null;
        }
        
    }*/

    /*@AuraEnabled(continuation = true)   
    public static RelatedList__mdt getRelatedListDetail(string title){
        try{ 
            RelatedList__mdt cam = [Select Id,Additional_Criteria__c,Child_Relationship_Name__c,Field_1_API_Name__c,Field_1_Label__c,Field_1_Lookup__c,Field_1_Type__c,Field_2_API_Name__c,Field_2_Label__c,Field_2_Lookup__c,
									Field_2_Type__c,Field_3_API_Name__c,Field_3_Label__c,Field_3_Lookup__c,Field_3_Type__c,Field_4_API_Name__c,Field_4_Label__c,Field_4_Lookup__c,Field_4_Type__c,Field_5_API_Name__c,
                                    Field_5_Label__c,Field_5_Lookup__c,Field_5_Type__c,Field_6_API_Name__c,Field_6_Label__c,Field_6_Lookup__c,Field_6_Type__c,Field_7_API_Name__c,Field_7_Label__c,Field_7_Lookup__c,
                                    Field_7_Type__c,Is_New_Button__c,Object_API_Name__c,Parent_Field_1_API_Name__c,Parent_Field_2_API_Name__c,Parent_Field_3_API_Name__c,Parent_Field_4_API_Name__c,
                                    Parent_Field_5_API_Name__c,Parent_Field_6_API_Name__c,Parent_Field_7_API_Name__c,Parent_Field_API_Name__c,Plural_Object_API_Name__c,Record_Type__c,Service_Check__c,Title__c from RelatedList__mdt where Title__c =: title];
            return cam;
        }
        catch(Exception e ){
            system.debug(e.getMessage() + ' e.getMessage() ???');
            return null;
        }
        
    }*/
        @AuraEnabled(continuation = true)
    Public static ContentVersion getFileName(ID recordId){
        List<ContentDocumentLink> listCDI = new List<ContentDocumentLink>();
        ContentVersion listcv = new ContentVersion();
        listCDI = [Select Id,ContentDocument.Id,ContentDocument.title,ContentDocument.FileExtension,ContentDocument.Owner.Name,ContentDocument.Owner.Id  From ContentDocumentLink Where LinkedEntityId =: recordId AND (ContentDocument.FileExtension = 'png' OR ContentDocument.FileExtension = 'jpg')];
        System.debug('listCDI'+listCDI);
        Set<Id> cdIds = new Set<Id>();
        for(ContentDocumentLink cdl : listCDI){
            cdIds.add(cdl.ContentDocument.Id);
        }
        if(cdIds.size()>0){
            listcv = [Select Id  From ContentVersion Where ContentDocumentId IN: cdIds order by createdDate desc Limit 1];
            System.debug('listcv'+listcv);
        }
        return listcv;
    }
    @AuraEnabled(continuation = true)
    public static boolean deleteProfilePic(ID recordId){
        
        try {
            
            list<ContentDocument> lstCntDocsToDelete = new list<ContentDocument>();

            for(ContentDocumentLink iterator : [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: recordId AND (ContentDocument.FileExtension = 'png' OR ContentDocument.FileExtension = 'jpg') LIMIT 1]) {
                lstCntDocsToDelete.add(new ContentDocument(Id = iterator.ContentDocumentId));
            }

            if(!lstCntDocsToDelete.isEmpty() && lstCntDocsToDelete != null) {
                delete lstCntDocsToDelete;
                //Database.emptyRecycleBin(lstCntDocsToDelete);
            }    
            return true;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
            //return false;
        }
        
    }
}