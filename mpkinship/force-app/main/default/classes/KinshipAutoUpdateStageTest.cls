@isTest
public class KinshipAutoUpdateStageTest {
    @testsetup static void setup(){
        TriggerActivation__c  ta = new TriggerActivation__c();
        ta.name='PurchaseOrder Trigger';
        ta.isActive__c=false;
        insert ta;
        
        
        TriggerActivation__c  ta1 = new TriggerActivation__c();
        ta1.name='UpdateCSACoordinatorSign';
        ta1.isActive__c=false;
        insert ta1;    
        account accountrec = new account();
        accountrec.Name='test vendor';
        accountrec.Tax_ID__c='333-33-3333';
        accountrec.Entity_Type__c='Individual';
        insert accountrec;
        
        Locality_Account__c la = new Locality_Account__c();
        la.name='33';
        insert la;
        
        id catrecType = Schema.SObjectType.Category__c.getRecordTypeInfosByName().get('LASER Category').getRecordTypeId();
        
        Category__c category = new Category__c();
        category.Name='222';
        category.Category_Type__c='Adoption';
        category.RecordTypeId = catrecType;
        category.Locality_Account__c=la.id;
        insert category;
        
        Campaign parent = new Campaign();
        parent.name = '2021';
        parent.StartDate = Date.Today().addDays(-10);
        parent.EndDate = Date.Today().addDays(10);
        parent.Fiscal_Year_Type__c = 'DSS';
        parent.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId();
        insert parent;
        id recType = Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('LASER_Reporting_Period').getRecordTypeId();
        campaign camp = new campaign();
        camp.name = 'May';
        camp.recordtypeid = recType;
        camp.Begin_Date__c=system.today();
        camp.EndDate=system.today().adddays(2);
        camp.parentId = parent.id;
        insert camp;
    
        id opprecType = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Staff_Operations').getRecordTypeId();   
        opportunity opprecord = new opportunity();
        opprecord.CloseDate=date.today();
        opprecord.StageName='Draft';
        opprecord.Name='test opportunity';
        opprecord.Amount=20000;
        opprecord.AccountId=accountrec.id;
        opprecord.RecordTypeId=opprecType;
        opprecord.Accounting_Period__c = camp.id;
        opprecord.Number_of_Months_Units__c=3; 
        opprecord.Category__c = category.Id;      
        insert opprecord;
       
        id payrecType = Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Staff_Operations').getRecordTypeId();
        Payment__c payment = new Payment__c();
        payment.Opportunity__c = opprecord.id;
        payment.Payment_Amount__c = 2000;
        payment.vendor__c = accountrec.Id;
        payment.RecordTypeId = payrecType;
        payment.Category_lookup__c = category.Id;
        payment.Payment_Method__c = 'Journal Entry';
        payment.Invoice_Stage__c = '';
        insert payment;

        payment.Invoice_Stage__c = '';
        update payment;
    }

    @isTest static void testAutoUpdateBatch(){
        Test.starttest();
        KinshipAutoUpdateStage theBatch = new KinshipAutoUpdateStage();
        Database.executeBatch(theBatch, 200);
        Test.stopTest();
    }
    
    @isTest static void executeSchedule() {  
        Test.startTest();     
        KinshipAutoUpdateStage myClass = new KinshipAutoUpdateStage(); 
        String chron = '0 0 23 * * ?';        
        system.schedule('Test Sched', chron, myClass);
        Test.stopTest();
    }
}