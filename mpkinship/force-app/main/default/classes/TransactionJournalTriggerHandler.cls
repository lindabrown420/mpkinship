public class TransactionJournalTriggerHandler extends TriggerHandler{
	public static Boolean bypassTrigger = false;
    
    /**
     * Handles before delete operations for all records being deleted.
     * Should only be used in BEFORE Trigger context
     */
    public override void beforeDelete() {
        if(PaymentTriggerHandler.bypassTrigger) return;
        List<Transaction_Journal__c> lstTJ = (List<Transaction_Journal__c>)Trigger.old;
        for(Transaction_Journal__c tj : lstTJ) {
            if(String.isNotBlank(tj.Invoice__c) && (tj.Status__c == 'Posted' || tj.Status__c == 'Closed')) {
                tj.addError(Label.ERROR_DELETE_POSTED_TJ);
            }
        }
    }
}