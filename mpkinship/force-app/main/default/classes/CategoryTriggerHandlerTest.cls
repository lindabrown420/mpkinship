@isTest
public class CategoryTriggerHandlerTest {
	@testSetup
    static void testRecords(){
        TriggerActivation__c  ta = new TriggerActivation__c();
        ta.name = 'CategoryTrigger';
        ta.isActive__c = true;
        insert ta;
    }
    
    @isTest
    public static void testInsert(){
        Locality_Account__c la = new Locality_Account__c();
        la.Name='222';
        insert la;
        Test.startTest();
        Category__c category = new Category__c();
        category.Name = '222';
        category.Locality_Account__c = la.id;
        category.Category_Type__c = 'Adoption';
        category.RecordTypeId = Schema.SObjectType.category__C.getRecordTypeInfosByDeveloperName().get('LASER_Category').getRecordTypeId();
        insert category;
        category.Name = '333';
        update category;
        Test.stopTest();
    }
    
    @isTest
    public static void testUpdate(){
        Locality_Account__c la = new Locality_Account__c();
        la.Name='222';
        insert la;
        Cost_Center__c cc = new Cost_Center__c(Name = '12345');
        insert cc;
        Chart_of_Account__c coa = new Chart_of_Account__c();
        coa.Locality_Account__c = la.Id;
        coa.Cost_Center__c = cc.id;
        insert coa;
        Test.startTest();
        Category__c category = new Category__c();
        category.Name = '222';
        category.Locality_Account__c = la.id;
        category.Category_Type__c = 'Adoption';
        category.RecordTypeId = Schema.SObjectType.category__C.getRecordTypeInfosByDeveloperName().get('LASER_Category').getRecordTypeId();
        insert category;
        
        Locality_Account__c la2 = new Locality_Account__c();
        la2.Name = '333';
        insert la2;
        category.Locality_Account__c = la2.id;
        update category;
        Test.stopTest();
    }
}