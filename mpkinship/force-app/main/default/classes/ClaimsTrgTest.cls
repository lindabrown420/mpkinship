@isTest
public class ClaimsTrgTest {
    public static testmethod void testRun(){
        
        TriggerActivation__c  ta = new TriggerActivation__c();
        ta.name='Claims Trigger';
        ta.isActive__c=true;
        insert ta;
        
        contact con = new contact();       
        con.LastName='contactrec';
        con.FirstName='test';
        con.Email='test@contact.com';      
        insert con;
        
        contact con1 = new contact();       
        con1.LastName='testrec';
        con1.FirstName='firsttest';
        con1.Email='test21@gmail.com';      
        insert con1;
        
        account accountrec = new account();
        accountrec.Name='test vendor';
        accountrec.Tax_ID__c='222-22-3333';
        accountrec.Entity_Type__c='Individual';
        insert accountrec;
        
        account accountrec1 = new account();
        accountrec1.Name='test acc';
        accountrec1.Tax_ID__c='111-22-3333';
        accountrec1.Entity_Type__c='Individual';
        insert accountrec1;
        
        Kinship_Case__c caserec = new Kinship_Case__c();
        caserec.Primary_Contact__c=con.id;
        caserec.DOB__c=system.today().adddays(-100);
        caserec.Race__c='1';
        caserec.Gender_Preference__c='M';
        caserec.Hispanic_Ethnicity__c='1';
        caserec.DSM_V_Indicator_ICD_10_1__c='2';
        caserec.Clinical_Medication_Indicator_1__c='2';
        caserec.Medicaid_Indicator_1__c='2';
        caserec.Referral_Source__c='5';
        caserec.Autism_Flag_1__c='2';
        insert caserec;
        
        Kinship_Case__c caserec1 = new Kinship_Case__c();
        caserec1.Primary_Contact__c=con1.id;
        caserec1.DOB__c=system.today().adddays(-100);
        caserec1.Race__c='1';
        caserec1.Gender_Preference__c='M';
        caserec1.Hispanic_Ethnicity__c='1';
        caserec1.DSM_V_Indicator_ICD_10_1__c='2';
        caserec1.Clinical_Medication_Indicator_1__c='2';
        caserec1.Medicaid_Indicator_1__c='2';
        caserec1.Referral_Source__c='5';
        caserec1.Autism_Flag_1__c='2';
        insert caserec1;
        
        Claims__c rec = new Claims__c();
        rec.Name = 'Test 2';
        rec.Claim_Amount__c=200;
        rec.Claim_Type__c='SNAP';
        rec.Payable_to__c= accountrec.id;
        rec.Case__c=caserec.id;
        insert rec;
        rec.Case__c=caserec1.id;
        update rec;
        
        Claims__c rec1 = new Claims__c();
        rec1.Name = 'Test';
        rec1.Claim_Amount__c=201;
        rec1.Amount_Received__c=20;
        rec1.Payable_to__c= accountrec.id;
        rec1.Claim_Type__c='SNAP';
        insert rec1;
        rec1.Payable_to__c= accountrec1.id;
        update rec1;
        
        Claims__c rec2 = new Claims__c();
        rec2.Name = 'Test1';
        rec2.Claim_Amount__c=200;
        rec2.Amount_Received__c=200;
        rec2.Claim_Type__c='SNAP';
        rec2.Payable_to__c= accountrec.id;
        rec2.Case__c=caserec.id;
        insert rec2;
        rec2.Payable_to__c= accountrec1.id;
        rec2.Case__c=caserec1.id;
        update rec2;
    }
}