public class KinshipBudgetController {
    private final static Integer MAX_RESULTS = 5;
    private final static String CAMP = 'Campaign';
    private final static String CENTER = 'Center';

    @AuraEnabled(cacheable=true)
    public static List<LookupSearchResult> kinshipSearchLookup(String searchTerm, List<String> selectedIds, String anOptionalParam) {
        // Prepare query paramters
        String strSearch = '%' + searchTerm + '%';
        searchTerm += '*';

        // Prepare results
        List<LookupSearchResult> results = new List<LookupSearchResult>();
        List<List<SObject>> searchResults;

        if(String.isNotBlank(anOptionalParam)) {
            if(CAMP.equals(anOptionalParam)) {
                searchResults = [FIND :searchTerm IN ALL FIELDS RETURNING
                                Campaign (Id, Name, Fiscal_Year_Type__c WHERE id NOT IN :selectedIds AND RecordType.Name = 'Fiscal Year')
                                LIMIT :MAX_RESULTS];
                String cItem = 'standard:campaign';
                Campaign[] objs = ((List<Campaign>) searchResults[0]);
                for (Campaign o : objs) {
                    results.add(new LookupSearchResult(o.Id, 'Campaign', cItem, o.Name, o.Fiscal_Year_Type__c));
                }
            } else if(CENTER.equals(anOptionalParam)) {
                String cItem = 'standard:account';
                List<Cost_Center__c> objs = [SELECT Id, Name, Cost_Center_Code__c FROM Cost_Center__c 
                                            WHERE id NOT IN :selectedIds AND (Name LIKE :strSearch OR Cost_Center_Code__c LIKE :strSearch)];
                for (Cost_Center__c o : objs) {
                    results.add(new LookupSearchResult(o.Id, 'Cost Center', cItem, o.Name, ''));
                }
            }
        }

        return results;
    }

    @AuraEnabled
    public static BudgetData initData(String strId){
        BudgetData result = new BudgetData();
        result.accountPeriod = '';
        result.rtCSA = Schema.SObjectType.Budgets__c.getRecordTypeInfosByName().get('CSA').getRecordTypeId();
        result.rtDSS = Schema.SObjectType.Budgets__c.getRecordTypeInfosByName().get('DSS').getRecordTypeId();
        result.rtLocal = Schema.SObjectType.Budgets__c.getRecordTypeInfosByName().get('Local').getRecordTypeId();
        result.lstBudgetLine = [SELECT Id, Name, Budget_Line_Description__c FROM LASER_Budget_Line__c WHERE Inactive__c = false];
        result.lstLocality = getLocalityAccount();
        result.lstCSACate = [SELECT Id, Name, Category_Account_Description__c FROM Category__c WHERE Category_Type__c = 'CSA' AND Inactive__c = false];
        if(String.isNotBlank(strId)) {
            result.currentBudget = [SELECT Id, Laser_Budget_Code__c, Category__c, Locality_Account_Code_Name__c,Budget_Code__c, Fiscal_Year__c, RecordTypeId FROM Budgets__c WHERE Id = :strId];
            result.lstCurrentBudget = [SELECT id, Laser_Budget_Code__c, Category__c, Locality_Account_Code_Name__c,Budget_Code__c, Fiscal_Year__c, RecordTypeId,Initial_Budget__c
                                        FROM Budgets__c WHERE Fiscal_Year__c = :result.currentBudget.Fiscal_Year__c];
        }
        return result;
    }

    private static List<LocalityAccount> getLocalityAccount() {
        List<LocalityAccount> result = new List<LocalityAccount>();
        List<Locality_Account__c> lstLA = [SELECT Id,Name, Account_Title__c FROM Locality_Account__c WHERE Inactive__c = false];
        Set<Id> setId = new Set<Id>();
        for(Locality_Account__c la : lstLA) {
            setId.add(la.Id);
        }
        Map<String, String> mapLA2CC = new Map<String, String>();
        for(Chart_of_Account__c coa : [SELECT Id,Locality_Account__c, Cost_Center__c FROM Chart_of_Account__c WHERE Locality_Account__c IN :setId]) {
            mapLA2CC.put(coa.Locality_Account__c, coa.Cost_Center__c);
        }
        for(Locality_Account__c la : lstLA) {
            LocalityAccount tla = new LocalityAccount();
            tla.id = la.Id;
            tla.name = la.Name;
            tla.localityAccountNumber = la.Name;
            tla.accountTitle = la.Account_Title__c;
            tla.costCenter = mapLA2CC.get(la.Id);
            result.add(tla);
        }
        return result;
    }

    @AuraEnabled
    public static BudgetData getCurrentBudgetData(String strId){
        BudgetData result = new BudgetData();
        result.lstCurrentBudget = [SELECT id, Laser_Budget_Code__c, Category__c, Locality_Account_Code_Name__c,Budget_Code__c, Fiscal_Year__c, RecordTypeId,Initial_Budget__c
                                    FROM Budgets__c WHERE Fiscal_Year__c = :strId];
        return result;
    }

    @AuraEnabled
    public static String createBudgets(BudgetData data){
        try {
            String strCode = '';
            /*Map<String, Budgets__c> mapCodeToBudget = new Map<String, Budgets__c>();
            for(Budgets__c b : [SELECT id, Laser_Budget_Code__c, Category__c, Locality_Account_Code_Name__c,Budget_Code__c
                                FROM Budgets__c
                                WHERE Fiscal_Year__c = :data.accountPeriod]) {
                strCode = '';
                if(String.isNotBlank(b.Laser_Budget_Code__c)) strCode = b.Laser_Budget_Code__c;
                else if(String.isNotBlank(b.Category__c)) strCode = b.Category__c;
                else if(String.isNotBlank(b.Locality_Account_Code_Name__c)) strCode = b.Locality_Account_Code_Name__c;
                mapCodeToBudget.put(strCode, b);
            }*/
            List<Budgets__c> lstInsert = new List<Budgets__c>();
            List<Budgets__c> lstUpdate = new List<Budgets__c>();
            Budgets__c tmp;
            for(ChildData cd : data.lstChild){
                strCode = '';
                if(String.isNotBlank(cd.budgetLine)) strCode = cd.budgetLine;
                else if(String.isNotBlank(cd.csaCate)) strCode = cd.csaCate;
                else if(String.isNotBlank(cd.localityCode)) strCode = cd.localityCode;
                /*tmp = mapCodeToBudget.get(strCode);
                if(tmp != null){
                    return 'There is an existing Budget for the Budget Code: ' + tmp.Budget_Code__c + '.';
                }*/
                Budgets__c temp = new Budgets__c();
                temp.Initial_Budget__c = cd.initialBudget;
                temp.recordTypeId = cd.recordTypeId;
                temp.Fiscal_Year__c = data.accountPeriod;
                temp.Laser_Budget_Code__c = String.isBlank(cd.budgetLine)?null:cd.budgetLine;
                temp.Category__c = String.isBlank(cd.csaCate)?null:cd.csaCate;
                temp.Locality_Account_Code_Name__c = String.isBlank(cd.localityCode)?null:cd.localityCode;

                if(String.isBlank(cd.id)) {
                    lstInsert.add(temp);
                } else {
                    temp.Id = cd.id;
                    lstUpdate.add(temp);
                }
            }system.debug('---Quy---' + lstInsert.size());
            if(lstInsert.size() > 0) {
                insert lstInsert;
            }system.debug('---Quy---' + lstUpdate.size());
            if(lstUpdate.size() > 0) {
                update lstUpdate;
            }
            return '';
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    public class BudgetData{
        @AuraEnabled
        public String accountPeriod{get;set;}

        @AuraEnabled
        public List<LASER_Budget_Line__c> lstBudgetLine{get;set;}

        @AuraEnabled
        public List<LocalityAccount> lstLocality{get;set;}

        @AuraEnabled
        public List<Category__c> lstCSACate{get;set;}

        @AuraEnabled
        public List<ChildData> lstChild{get;set;}

        @AuraEnabled
        public String rtCSA{get;set;}

        @AuraEnabled
        public String rtDSS{get;set;}

        @AuraEnabled
        public String rtLocal{get;set;}

        @AuraEnabled
        public List<Budgets__c> lstCurrentBudget{get;set;}

        @AuraEnabled
        public Budgets__c currentBudget{get;set;}
    }

    public class ChildData{
        @AuraEnabled
        public String id{get;set;}

        @AuraEnabled
        public String name{get;set;}

        @AuraEnabled
        public String accountPeriod{get;set;}

        @AuraEnabled
        public Decimal initialBudget{get;set;}

        @AuraEnabled
        public String budgetLine{get;set;}

        @AuraEnabled
        public String localityCode{get;set;}

        @AuraEnabled
        public String csaCate{get;set;}

        @AuraEnabled
        public String recordTypeId{get;set;}

        @AuraEnabled
        public String description{get;set;}
    }

    public class LocalityAccount{
        @AuraEnabled
        public String id{get;set;}

        @AuraEnabled
        public String name{get;set;}

        @AuraEnabled
        public String localityAccountNumber{get;set;}

        @AuraEnabled
        public String accountTitle{get;set;}

        @AuraEnabled
        public String costCenter{get;set;}
    }
}