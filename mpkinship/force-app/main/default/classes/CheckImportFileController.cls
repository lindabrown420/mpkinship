public class CheckImportFileController {
	@AuraEnabled(continuation=true)
    public static void insertCheckList(List<CheckListWrapper> checkListData){
        Kinship_Check__c kinshipCheck = new Kinship_Check__c();
        List<Kinship_Check__c> kinshipCheckList = new List<Kinship_Check__c>();
        List<Account> accList = new List<Account>();
        List<String> localityNumberList = new List<String>();
        for(CheckListWrapper clw:checkListData){
            String localityNumber = '';
            if(clw.vendor != null && clw.vendor != ''){
               List<String> splitList = clw.vendor.split(' ');
               localityNumber = splitList[0];
               localityNumberList.add(localityNumber);
            }
        }
        accList = [Select Id, Locality_Vendor_Number__c from Account where Locality_Vendor_Number__c IN: localityNumberList]; 
        for(CheckListWrapper clw:checkListData){
            kinshipCheck = new Kinship_Check__c();
            String localityNumber = '';
            String accId = '';
            if(clw.vendor != null && clw.vendor != ''){
               List<String> splitList = clw.vendor.split(' ');
               localityNumber = splitList[0];
               if(accList.size()>0){
                    for(Account acc : accList){
                        if(acc.Locality_Vendor_Number__c == localityNumber)
                        kinshipCheck.Vendor__c = acc.Id;
                    }
                }
            }
            /*if(clw.acct != null && clw.acct != ''){
                kinshipCheck.ACCT__c = clw.acct; 
            }
            if(clw.cashAcct != null && clw.cashAcct != ''){
                kinshipCheck.CASH_ACCT__c = clw.cashAcct; 
            }*/
            if(clw.amount != null && clw.amount != ''){
                String str=clw.amount.replace(',','');
                kinshipCheck.Amount__c = Decimal.valueOf(str); 
            }
            if(clw.checkNumber != null && clw.checkNumber != ''){
                kinshipCheck.Check_No__c = clw.checkNumber ; 
            }
            if(clw.dateIssued != null && clw.dateIssued != ''){
                kinshipCheck.Issue_Date__c =date.parse(clw.dateIssued); 
            }
            if(clw.description != null && clw.description != ''){
                kinshipCheck.Description__c = clw.description; 
            }
            kinshipCheckList.add(kinshipCheck);
        }
        if(kinshipCheckList.size() > 0){
            upsert kinshipCheckList Check_No__c;
        }
    }
    public class CheckListWrapper{
        @AuraEnabled
        public string acct {get;set;}
        @AuraEnabled
        public string amount {get;set;}
        @AuraEnabled
        public string cashAcct {get;set;}
        @AuraEnabled
        public string checkNumber {get;set;}
        @AuraEnabled
        public string dateIssued {get;set;}
        @AuraEnabled
        public string description {get;set;}
        @AuraEnabled
        public string vendor {get;set;}
    }
}