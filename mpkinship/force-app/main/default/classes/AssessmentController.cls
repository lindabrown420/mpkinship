public class AssessmentController
{

    @AuraEnabled
    public static boolean showStartButtonMethod(string recId){
        system.debug('showStartButtonMethod');
        Assessment__c asm = queryAssessment(recId);
        system.debug(asm.Is_assessment_completed__c);
        if(asm.recordtype.name == 'VEMAT' && asm.Assessment_Line_Items__r.size() == 0 && (asm.Is_assessment_completed__c == false || asm.Is_assessment_completed__c == null)){
            return true;
        }
        else {
            return false;
        }
        
    }
    
    @AuraEnabled
    public static boolean showTotalPointsButtonMethod(string recId){
        system.debug('showTotalPointsButtonMethod');
        Assessment__c asm = queryAssessment(recId);
        system.debug(asm.Total_Points__c);
        if(asm.recordtype.name == 'VEMAT' && asm.Is_assessment_completed__c == true && (asm.Total_Points__c == 0 || asm.Total_Points__c == null) ){
            return true;
        }
        else {
            return false;
        }
        
    }
    
    @AuraEnabled
    public static boolean updateAssessment(string recId, integer totalPoints){
        Assessment__c asm = queryAssessment(recId);

        asm.Total_Points__c = totalPoints;
        update asm;
        
        return true;
    }
    
    @AuraEnabled(continuation = true)   
    public static Map<String, Map<String, List<VEMAT_Assessment__c>>> getAssessmentSettings(){

        try{
            List<VEMAT_Assessment__c> VEMATAs = [Select Id,Name,
            Category__c,Characteristic__c,Domain__c,Points__c,Row_Number__c
             from VEMAT_Assessment__c
             where Active__c = true
             order by Domain_Lookup__r.Order__c,Row_Number__c];
            
            Map<String, Map<String, List<VEMAT_Assessment__c>>> mainMap = new Map<String, Map<String, List<VEMAT_Assessment__c>>>();
            Map<String, List<VEMAT_Assessment__c>> mapSetngs = new Map<String, List<VEMAT_Assessment__c>>();
            
            for (VEMAT_Assessment__c VEMATA : VEMATAs) {
                if(mainMap.get(VEMATA.Domain__c) == null){
                    mainMap.put(VEMATA.Domain__c, new Map<String, List<VEMAT_Assessment__c>>());
                }
                Map<String, List<VEMAT_Assessment__c>> tempDomainMap = mainMap.get(VEMATA.Domain__c);
                if(tempDomainMap.get(VEMATA.Category__c) == null){
                    tempDomainMap.put(VEMATA.Category__c, new List<VEMAT_Assessment__c>());
                }
                List<VEMAT_Assessment__c> tempCat = tempDomainMap.get(VEMATA.Category__c);

                tempCat.add(VEMATA);

                tempDomainMap.put(VEMATA.Category__c,tempCat);
                mainMap.put(VEMATA.Domain__c,tempDomainMap);

            }

            return mainMap;

        }
        catch(Exception e ){
            system.debug(e.getMessage() + ' e.getMessage() ???');
            return null;
        }
    }


    @AuraEnabled(continuation = true)   
    public static string saveAssessment(List<Assessment_Line_Item__c> assessLine, string recId){
            system.debug(recId);
        try{

            system.debug('assessLine>>>>>' + assessLine);

            if(assessLine.size() > 0)
            {
                insert assessLine;
                Assessment__c asm = [Select Id,Completed_Date__c                              
                             From Assessment__c
                             Where Id=: recId];
                asm.Completed_Date__c = Date.today();
                system.debug(asm);
                update asm;
            }
       
            return 'success';

        }
        catch(Exception e ){
            system.debug(e.getMessage() + ' e.getMessage() ???');
            return null;
        }
    }
    
    /************* For KIN-1260 ***************/
    @AuraEnabled
    public static String getRT_Name(string rtId){
        RecordType rt = [Select id,name from RecordType where id=: rtId];
        return rt.Name;        
    }
    
    @AuraEnabled
    public static Boolean getRefreshPage(string recId){
        Assessment__c asm = [Select id,Refresh_Page__c from Assessment__c where id=: recId];
        if(asm.Refresh_Page__c == true){
            asm.Refresh_Page__c = false;
            update asm;
            return true; 
        }
        else{
            return false;
        }            
    }
    
    public static Assessment__c queryAssessment(string recId){
        Assessment__c asm = [Select Id,recordtype.name,Is_assessment_completed__c,Total_Points__c,
                             (Select id from Assessment_Line_Items__r) 
                             From Assessment__c
                             Where Id=: recId];
        
        return asm;
    }
}