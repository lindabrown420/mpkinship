@isTest
private class LEDRSReportControllerTest {
    @TestSetup
    static void initData(){
        TriggerActivation__c ta = new TriggerActivation__c(Name = 'PaymentTrigger', isActive__c = true);
        insert ta;
        
        FIPS__c fp = new FIPS__c();
        fp.name='test fips';
        insert fp;
        
        Id profileId1 = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User u = new User(FirstName='Test',LastName= 'User',profileid=profileId1 , email='abc123@test.com',
                         username='abc123@test.com.prod',Alias='zbd',LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', EmailEncodingKey='UTF-8',
                          TimeZoneSidKey='America/Los_Angeles');
        Campaign parent = new Campaign(name = 'CSA - 2020', Fiscal_Year_Type__c = 'CSA', RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId(), FIPS__c = fp.id, isActive=true);
        insert parent;
        Campaign parent2 = new Campaign(name = 'DSS - 2020', Fiscal_Year_Type__c = 'DSS', RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId(), FIPS__c = fp.id, isActive=true);
        insert parent2;
        Id campId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('LEDRS Reporting Period').getRecordTypeId();
        Campaign cam = new Campaign(Name= 'Cam', ParentId = parent.id, RecordTypeId = campId, startDate = Date.today().addDays(-10), endDate = Date.today().addDays(10), FIPS__c = fp.id, isActive=true);
        insert cam;
        
        Account acc = new Account(Name = 'test acc', Tax_ID__c = '123-56-7901', Entity_Type__c = 'Individual');
        insert acc;
        
        Locality_Account__c locality = new Locality_Account__c(Name = '12345');
        insert locality;

        Category__c cate = new Category__c(Name = 'test cate', Category_Type__c = 'CSA', Locality_Account__c = locality.id , RecordTypeId = Schema.SObjectType.Category__c.getRecordTypeInfosByName().get('LEDRS Category').getRecordTypeId());
        insert cate;
        
        Kinship_Case__c kcase = new Kinship_Case__c(Primary_Social_Worker__c = u.id);

        Opportunity opp = new Opportunity(Name = 'test Opp', AccountId = acc.id, Category__c = cate.id, CloseDate = Date.today().addDays(10),
                                          StageName='Open', Kinship_Case__c = kcase.id, Parent_Recipient__c = '2');
        insert opp;

        Payment__c payment = new Payment__c(Opportunity__c = opp.id,category_lookup__c=cate.id, 
                                                                Scheduled_Date__c = Date.today(), 
                                                                Status__c = 'Submitted For Approval',
                                                                Invoice_Stage__c = 'Ready to Pay',
                                                               	Written_Off__c = false,
                                                               	Payment_Date__c = Date.today(),
                                                                RecordTypeId = Schema.SObjectType.Payment__c.getRecordTypeInfosByName().get('Vendor Invoice').getRecordTypeId(),
                                                                Payment_Source__c = '1',
                                                                FIPS_Code__c = fp.id);
        insert payment;
        payment.Paid__c = true;
        payment.Amount_Paid__c = 100;
        update payment;
        
        Payment_Line_Item__c pli = new Payment_Line_Item__c(Invoice__c = payment.id);
        insert pli;
        
        Transaction_Journal__c tj = new Transaction_Journal__c();
        tj.Invoice__c = payment.id;
        tj.Child_Campaign__c = cam.id;
        insert tj;
    }

    @isTest
    static void testGetLEDRSData(){
        Campaign c = [SELECT id FROM Campaign WHERE RecordType.Name = 'LEDRS Reporting Period' Limit 1];
        List<String> lstCamp = new List<String>();
        lstCamp.add(c.id);
        Test.startTest();
        LEDRSReportController.getAllAP(c.id);
        List<Object> lstResult = LEDRSReportController.getLEDRSData(lstCamp);
        LEDRSReportController.postedAP(lstCamp);
        LEDRSReportController.checkPostAP(lstCamp);
        Test.stopTest();
        //System.assertEquals(1, lstResult.size(), 'The getLEDRSData method return does not equal 1');
    }
    
    @isTest
    static void testKinshipSearchLookup_WithCamp(){
        Campaign c = [SELECT id FROM Campaign WHERE RecordType.Name = 'LEDRS Reporting Period' Limit 1];
        List<Id> fixedSearchResults= new List<Id>();
        fixedSearchResults.add(c.id);
        Test.setFixedSearchResults(fixedSearchResults);
        Test.startTest();
        List<Object> lstResult = LEDRSReportController.kinshipSearchLookup('ca', new List<String>(), 'Campaign');
        Test.stopTest();
        System.assertEquals(1, lstResult.size(), 'The kinshipSearchLookup method return does not equal 1');
    }

    @isTest
    static void testKinshipSearchLookup_WithCamp2(){
        Campaign c = [SELECT id FROM Campaign WHERE Fiscal_Year_Type__c = 'DSS' Limit 1];
        List<Id> fixedSearchResults= new List<Id>();
        fixedSearchResults.add(c.id);
        Test.setFixedSearchResults(fixedSearchResults);
        Test.startTest();
        List<Object> lstResult = LEDRSReportController.kinshipSearchLookup('DSS', new List<String>(), 'Campaign2');
        Test.stopTest();
        System.assertEquals(1, lstResult.size(), 'The kinshipSearchLookup method return does not equal 1');
    }

    @isTest
    private static void testGetPicklistvalues(){
        Test.startTest();
        Map<String, String> lstResult = LEDRSReportController.getPicklistvalues('Kinship_Check__c', 'Status__c');
        Test.stopTest();

        System.assertNotEquals(null, lstResult.size(), 'The method is return null value');
    }
}