/**
 * Factory class for triggers. 
 * To use, simply call TriggerFactory.createAndExcuteEHandler and
 * pass in the trigger handler class. Exapmles QuoteTriggerHandler.class.
 */
public with sharing class TriggerFactory {
    /**
    * Accepts a Type, e.g. QuoteTriggerHandler.class, and then executes
    * all trigger methods based on the context. If no trigger is found
    * then an exception is thrown. Trigger must implement ITriggerHandler.
    */
    public static void createAndExecuteHandler(Type t) {
        ITriggerHandler handler = getHandler(t);
        if (handler == null) {
            throw new TriggerException('No trigger handler found named: ' + t.getName());
        }

        execute(handler);
    }

    /**
     * Executes the TriggerHandlers methods based on context.
     * If this is called within a non-trigger context then nothing
     * will happen
     */
    @testVisible
    private static void execute(ITriggerHandler handler) {
        if (Trigger.isExecuting) {
            if (Trigger.isBefore) {
                handler.bulkBefore();
                if (Trigger.isInsert) {
                    handler.beforeInsert();
                } else if (Trigger.isUpdate) {
                    handler.beforeUpdate();
                } else if (Trigger.isDelete) {
                    handler.beforeDelete();
                }
            } else {
                handler.bulkAfter();
                if (Trigger.isInsert) {
                    handler.afterInsert();
                } else if (Trigger.isUpdate) {
                    handler.afterUpdate();
                } else if (Trigger.isDelete) {
                    handler.afterDelete();
                }
            }
            handler.andFinally();
        }
    }

    /**
     * Gets an instance of ITrigger. If none found returns null
     */
    @testVisible
    private static ITriggerHandler getHandler(Type t) {
        Object handler = t.newInstance();
        if (!(handler instanceOf ITriggerHandler)) {
            return null;
        }
        return (ITriggerHandler) handler;
    }

    public class TriggerException extends Exception {}
}