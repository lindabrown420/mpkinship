@isTest
public class FAPTMeetingTeamMembersTest {
    /*@testSetup
    static void testRecords(){
        FAPT_Agenda_Team__c fateam = new FAPT_Agenda_Team__c();
        fateam.Name='test team';
        fateam.Description__c='team for testing';
        insert fateam;
        Id contactRecordId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('FAPT_Team_Members').getRecordTypeId();
        contact con = new contact();
        con.lastname='test';
        con.firstname='testing';
        con.FAPT_Agenda_Team__c=fateam.id;
        con.Agency__c='test';
        con.MailingStreet='testing';
        con.MailingCity='test';
        insert con;
          id faptmeetingrecType = Schema.SObjectType.FAPT_Meeting_Agenda__c.getRecordTypeInfosByDeveloperName().get('FAPT_Meeting').getRecordTypeId();
        FAPT_Meeting_Agenda__c faptmeetingagenda = new FAPT_Meeting_Agenda__c();
        faptmeetingagenda.Location_of_Meeting__c='california';
        faptmeetingagenda.Name='fapt meeting';
        faptmeetingagenda.Meeting_Date__c=system.today().adddays(10);
        faptmeetingagenda.Meeting_Start_Time__c = Time.newInstance(1, 2, 3, 4);
        faptmeetingagenda.Team__c=fateam.id;
        faptmeetingagenda.RecordTypeId=faptmeetingrecType;
        insert faptmeetingagenda;
        
    } */
  /*  public static testmethod void FAPTtesting(){
        FAPT_Agenda_Team__c fateam = new FAPT_Agenda_Team__c();
        fateam.Name='test team1';
        fateam.Description__c='team for testing';
        insert fateam;
        system.debug('**VAl of fateam'+fateam);
        Id contactRecordId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Contacts').getRecordTypeId();
        contact con = new contact();
        con.lastname='test1';
        con.firstname='testing';
        con.FAPT_Agenda_Team__c=fateam.id;
        //con.Agency__c='test';
        con.RecordTypeId=contactrecordid;
        con.MailingStreet='testing';
        con.MailingCity='test';
        insert con;
        
        id faptmeetingrecType = Schema.SObjectType.FAPT_Meeting_Agenda__c.getRecordTypeInfosByDeveloperName().get('FAPT_Meeting').getRecordTypeId();
        FAPT_Meeting_Agenda__c faptmeetingagenda = new FAPT_Meeting_Agenda__c();
        faptmeetingagenda.Location_of_Meeting__c='california';
        faptmeetingagenda.Name='fapt meeting';
        faptmeetingagenda.Meeting_Date__c=system.today().adddays(10);
        faptmeetingagenda.Meeting_Start_Time__c = Time.newInstance(1, 2, 3, 4);
        faptmeetingagenda.Team__c=fateam.id;
        faptmeetingagenda.RecordTypeId=faptmeetingrecType;
        insert faptmeetingagenda;
       
        
    }
     public static testmethod void FAPTtestingteamchange(){
       
       FAPT_Agenda_Team__c fateam = new FAPT_Agenda_Team__c();
        fateam.Name='test team1';
        fateam.Description__c='team for testing';
        insert fateam;
        system.debug('**VAl of fateam'+fateam);
        Id contactRecordId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Contacts').getRecordTypeId();
        contact con = new contact();
        con.lastname='test1';
        con.firstname='testing';
        con.FAPT_Agenda_Team__c=fateam.id;
        //con.Agency__c='test';
        con.RecordTypeId=contactrecordid;
        con.MailingStreet='testing';
        con.MailingCity='test';
        insert con;
        FAPT_Agenda_Team__c fateam1 = new FAPT_Agenda_Team__c();
        fateam1.Name='test team2';
        fateam1.Description__c='team for testing';
        insert fateam1;
        Id contactRecordId1 = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Contacts').getRecordTypeId();
        contact con1 = new contact();
        con1.lastname='test2';
        con1.firstname='testing';
        con1.FAPT_Agenda_Team__c=fateam1.id;
        //con1.Agency__c='test';
        con1.RecordTypeId=contactrecordid1;
        con1.MailingStreet='testing';
        con1.MailingCity='test';
        insert con1;
        id faptmeetingrecType = Schema.SObjectType.FAPT_Meeting_Agenda__c.getRecordTypeInfosByDeveloperName().get('FAPT_Meeting').getRecordTypeId();
        FAPT_Meeting_Agenda__c faptmeetingagenda = new FAPT_Meeting_Agenda__c();
        faptmeetingagenda.Location_of_Meeting__c='california';
        faptmeetingagenda.Name='fapt meeting';
        faptmeetingagenda.Meeting_Date__c=system.today().adddays(10);
        faptmeetingagenda.Meeting_Start_Time__c = Time.newInstance(1, 2, 3, 4);
        faptmeetingagenda.Team__c=fateam.id;
        faptmeetingagenda.RecordTypeId=faptmeetingrecType;
        insert faptmeetingagenda;
         id recType = Schema.SObjectType.contact.getRecordTypeInfosByDeveloperName().get('Contacts').getRecordTypeId();
        contact con11 = new contact();
        con11.recordtypeid=recType;
        con11.LastName='contactrec11';
        con11.FirstName='testiing';
        con11.Email='testing@contact.com';
        //con11.Race_of_the_Foster_Parent__c='White';
        //con11.Gender_Preference__c='Female';
        insert con11;
          id caserecType = Schema.SObjectType.Kinship_Case__c.getRecordTypeInfosByDeveloperName().get('Foster_Care_Title_IV_E').getRecordTypeId();
        Kinship_Case__c caserec = new Kinship_Case__c();
        caserec.Primary_Contact__c=con11.id;
        caserec.RecordTypeId=caserecType;
        insert caserec;
         FAPT_Meeting_Agenda_Item__c faptagenda = new FAPT_Meeting_Agenda_Item__c();
        faptagenda.Case_KNP__c = caserec.id;
        faptagenda.FAPT_Meeting_Agenda__c = faptmeetingagenda.id;
        faptagenda.Time__c= Time.newInstance(1, 3, 3, 4);
        insert faptagenda;
         system.debug('**VAl of agenda team'+faptmeetingagenda.team__c);
        faptmeetingagenda.Team__c=fateam1.id;
        update faptmeetingagenda;
         system.debug('**Val of agenda updated team'+faptmeetingagenda.Team__c);
        
    } 
     public static testmethod void FAPTtestingdatechange(){
         FAPT_Agenda_Team__c fateam = new FAPT_Agenda_Team__c();
        fateam.Name='test team1';
        fateam.Description__c='team for testing';
        insert fateam;
        
         id faptmeetingrecType = Schema.SObjectType.FAPT_Meeting_Agenda__c.getRecordTypeInfosByDeveloperName().get('FAPT_Meeting').getRecordTypeId();
        FAPT_Meeting_Agenda__c faptmeetingagenda = new FAPT_Meeting_Agenda__c();
        faptmeetingagenda.Location_of_Meeting__c='california';
        faptmeetingagenda.Name='fapt meeting';
        faptmeetingagenda.Meeting_Date__c=system.today().adddays(10);
        faptmeetingagenda.Meeting_Start_Time__c = Time.newInstance(1, 2, 3, 4);
        faptmeetingagenda.Team__c=fateam.id;
        faptmeetingagenda.RecordTypeId=faptmeetingrecType;
        insert faptmeetingagenda;
                      
        faptmeetingagenda.Meeting_Date__c=system.today().adddays(1);
        update faptmeetingagenda;
        
    } 
    public static testmethod void FAPTtestinglocationchange(){
         FAPT_Agenda_Team__c fateam = new FAPT_Agenda_Team__c();
        fateam.Name='test team1';
        fateam.Description__c='team for testing';
        insert fateam;
         id faptmeetingrecType = Schema.SObjectType.FAPT_Meeting_Agenda__c.getRecordTypeInfosByDeveloperName().get('FAPT_Meeting').getRecordTypeId();
        FAPT_Meeting_Agenda__c faptmeetingagenda = new FAPT_Meeting_Agenda__c();
        faptmeetingagenda.Location_of_Meeting__c='california';
        faptmeetingagenda.Name='fapt meeting';
        faptmeetingagenda.Meeting_Date__c=system.today().adddays(10);
        faptmeetingagenda.Meeting_Start_Time__c = Time.newInstance(1, 2, 3, 4);
        faptmeetingagenda.Team__c=fateam.id;
        faptmeetingagenda.RecordTypeId=faptmeetingrecType;
        insert faptmeetingagenda;
                      
        faptmeetingagenda.Location_of_Meeting__c='Virginia';
        update faptmeetingagenda;
        
    } */
}