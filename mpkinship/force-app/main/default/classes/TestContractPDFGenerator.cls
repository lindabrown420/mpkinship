@isTest
public class TestContractPDFGenerator {
    static testmethod void testmethod1(){
        
        account accountrec = new account();
        accountrec.Name='test vendor';
        accountrec.Tax_ID__c='222-22-3333';
        accountrec.Entity_Type__c='Individual';
        accountrec.Email__c = 'test@gmail.com';
        insert accountrec;
        
        date startdate = Date.Today().addDays(-10);
        date enddate = Date.Today().addDays(10);
        
        Campaign parent = new Campaign();
        parent.name = '2021';
        parent.StartDate = Date.Today().addDays(-10);
        parent.EndDate = Date.Today().addDays(90);
        parent.Fiscal_Year_Type__c = 'Local';
        parent.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId();
        insert parent;
        
        Contract contract02 = new Contract(Email_to_Vendor__c = True,Fiscal_Year__c = parent.id, AccountID = accountrec.Id, Status = 'Draft', StartDate =startdate , ContractTerm = 1);
        insert contract02;
        
        PageReference myVfPage = Page.Contracts;
        Test.setCurrentPage(myVfPage);
        
        // Put Id into the current page Parameters
        ApexPages.currentPage().getParameters().put('id',contract02.Id);
         
        ContractPDFGenerator con = new ContractPDFGenerator();
        ContractPDFGenerator.GenerateCustomPDFAndEmail(contract02.Id);
    }
}