@isTest
private class KinshipBudgetControllerTest {
	@testSetup
    static void initData(){
        Id profileId1 = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User u = new User(FirstName='Test',LastName= 'User',profileid=profileId1 , email='abc123@test.com',
                         username='abc123@test.com.prod',Alias='zbd',LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', EmailEncodingKey='UTF-8',
                          TimeZoneSidKey='America/Los_Angeles');
        
        Account acc = new Account(Name = 'test acc', Tax_ID__c = '123-56-7901', Entity_Type__c = 'Individual');
        insert acc;
        
        Locality_Account__c locality = new Locality_Account__c(Name = '12345');
        insert locality;

        Category__c cate = new Category__c(Name = 'test cate', Category_Type__c = 'CSA', Locality_Account__c = locality.id , RecordTypeId = Schema.SObjectType.Category__c.getRecordTypeInfosByName().get('LEDRS Category').getRecordTypeId());
        insert cate;
        
        FIPS__c fips = new FIPS__c(name = 'FIPS Test', FIPS_Code__c='1');
        insert fips;
        
        Kinship_Case__c kcase = new Kinship_Case__c(Primary_Social_Worker__c = u.id);

        Opportunity opp = new Opportunity(Name = 'test Opp', AccountId = acc.id, Category__c = cate.id, CloseDate = Date.today().addDays(10),
                                          StageName='Open', Kinship_Case__c = kcase.id);
        insert opp;

        Payment__c payment = new Payment__c(Opportunity__c = opp.id, 
                                                                Scheduled_Date__c = Date.today(), 
                                                                Status__c = 'Submitted For Approval',
                                                                Invoice_Stage__c = 'Ready to Pay',
                                                               	Written_Off__c = false,
                                                                Category_lookup__c = cate.id);
        insert payment;
    }

    @isTest
    static void testKinshipSearchLookup_WithCamp(){
        Campaign c = new Campaign(name = 'CSA - 2020', Fiscal_Year_Type__c = 'Local', RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId());
        insert c;
        List<Id> fixedSearchResults= new List<Id>();
        fixedSearchResults.add(c.id);
        Test.setFixedSearchResults(fixedSearchResults);
        Test.startTest();
        List<Object> lstResult = KinshipBudgetController.kinshipSearchLookup('test', new List<String>(), 'Campaign');
        Test.stopTest();
        System.assertEquals(1, lstResult.size(), 'The kinshipSearchLookup method return does not equal 1');
    }
    
    @isTest
    static void testKinshipSearchLookup_WithCostCenter(){
        Cost_Center__c cc = new Cost_Center__c(Name = 'test');
        insert cc;
        List<Id> fixedSearchResults= new List<Id>();
        fixedSearchResults.add(cc.id);
        Test.setFixedSearchResults(fixedSearchResults);
        Test.startTest();
        List<Object> lstResult = KinshipBudgetController.kinshipSearchLookup('test', new List<String>(), 'Center');
        Test.stopTest();
        System.assertEquals(1, lstResult.size(), 'The kinshipSearchLookup method return does not equal 1');
    }
    
    @isTest
    private static void testInitData(){
        Test.startTest();
        Object obj = KinshipBudgetController.initData('');
        Test.stopTest();

        System.assertNotEquals(null, obj, 'The method is return null value');
    }
    
    @isTest
    private static void testCreateBudgets(){
        LASER_Budget_Line__c budgetLine = new LASER_Budget_Line__c(name = '12345', Budget_Line_Description__c='Budget_Line_Description');
        insert budgetLine;
        Locality_Account__c localAcc = new Locality_Account__c(name = '234567', Account_Title__c='Account_Title');
        insert localAcc;
        Category__c cate = [SELECT id FROM Category__c LIMIT 1];
        Campaign c = new Campaign(name = 'CSA - 2020', Fiscal_Year_Type__c = 'Local');
        insert c;
        
        KinshipBudgetController.BudgetData data = (KinshipBudgetController.BudgetData)KinshipBudgetController.initData('');
        data.accountPeriod = c.id;
        data.lstChild = new List<KinshipBudgetController.ChildData>();
        KinshipBudgetController.ChildData child = new KinshipBudgetController.ChildData();
        child.initialBudget = 100;
        child.recordTypeId = data.rtCSA;
        child.accountPeriod = c.id;
        child.budgetLine = budgetLine.id;
        child.csaCate = cate.id;
        child.localityCode = localAcc.id;
        data.lstChild.add(child);
        Test.startTest();
        KinshipBudgetController.createBudgets(data);
        KinshipBudgetController.getCurrentBudgetData(c.id);
        Test.stopTest();
		
        System.assertNotEquals(0, [SELECT id FROM Budgets__c].size(), 'The method does not insert any Budget');
    }
}