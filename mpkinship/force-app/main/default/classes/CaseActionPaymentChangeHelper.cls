public class CaseActionPaymentChangeHelper{

    public static void ChangeStage(List<Payment__c> payments){
        list<Payment__c> paymentsToUpdate = new list<Payment__c >();
        for(Payment__c payment : payments){
            //kin-1483
            if(payment.Scheduled_Date__c!=null && payment.Scheduled_Date__c <=date.today().adddays(15) 
            && payment.Invoice_Stage__c=='Scheduled'){
                Payment__c paymentrec = new Payment__c();
                paymentrec.id = payment.id;
                paymentrec.Invoice_Stage__c='Ready to Pay';
                paymentsToUpdate.add(paymentrec);
                
            }
        }
        
        if(paymentsToUpdate.size()>0)
            update paymentsToUpdate ;
    }
}