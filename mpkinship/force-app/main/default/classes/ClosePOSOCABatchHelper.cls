public class ClosePOSOCABatchHelper{

    public static void closePO(list<opportunity> POs){
        list<opportunity> UpdatedOpplist = new list<opportunity>();
        list<opportunity> opplist=[select id,stagename,(select id from Payment_OppPayment__r) from opportunity where id in: POs];
        for(opportunity opp : opplist){
           opportunity op = new opportunity();
           op.id=opp.id;
           op.stagename='Closed';
           UpdatedOpplist.add(op);
            
        }
        
        if(UpdatedOpplist.size()>0)
            update UpdatedOpplist;
    }

    //OLD WAY COMMENTED
   
   /* public static void closePO(list<opportunity> POs){
        set<id> opportunityids = new set<id> ();
        map<id,integer> opportunityPaidWriteOffPaymentMap = new map<id,integer>();
        for(opportunity opp : POs){
            opportunityids.add(opp.id);
            if(opp.npe01__OppPayment__r.size()>0)
                opportunityPaidWriteOffPaymentMap.put(opp.id,opp.npe01__OppPayment__r.size());    
        }
       system.debug('**VAl of opporutnitypaidwriteoffmap'+ opportunityPaidWriteOffPaymentMap); 
       
       list<opportunity> opplist=[select id,stagename,(Select id from npe01__OppPayment__r) from opportunity where id in: POs];
       map<id,integer> opportunityPaymentMap = new map<id,integer>();
       for (opportunity opp : opplist){
           opportunityPaymentMap.put(opp.id,opp.npe01__OppPayment__r.size());
       } 
       system.debug('**VAl of opppaymentmap'+ opportunityPaymentMap);
       
       list<opportunity> opplisttoUpdate = new list<opportunity>();
       for(id oppid : opportunityPaidWriteOffPaymentMap.keyset()){
           system.debug('**opp val'+ oppid);
           if(opportunityPaymentMap.size()>0 && opportunityPaymentMap.containskey(oppid) && 
               opportunityPaidWriteOffPaymentMap.get(oppid)==opportunityPaymentMap.get(oppid)){
               system.debug('**equal payments if entered'+ opportunityPaymentMap.get(oppid));
               opportunity opp = new opportunity();
               opp.id = oppid;
               opp.stagename ='Closed';
               opplisttoUpdate.add(opp);
           }
       }
       
       if(opplisttoUpdate.size()>0)
           update opplisttoUpdate;
    
    } */


}