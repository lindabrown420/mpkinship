public class CloneRelatedListController {
@AuraEnabled  
    public static List < sObject > fetchRecs( String recId, String sObjName, String parentFldNam, String strCriteria, String selectStr ) {  
          
        String strTitle = ' (';           
        List < sObject > listsObjects = new List < sObject >();  
        RelatedListResult obj = new RelatedListResult();  
        String strSOQL = 'SELECT Id, ';
        if(selectStr != null && selectStr != ''){ strSOQL += selectStr; }
        else{
            strSOQL += ' Name ';    
        }
        strSOQL += ' FROM ' + sObjName + ' WHERE ' + parentFldNam + ' = \'' + recid + '\'';  
        if ( String.isNotBlank( strCriteria ) ) { strSOQL += ' ' + strCriteria;}  
        strSOQL += ' LIMIT 10000';  
        system.debug('**Val of strsoql'+ strSOQL);
        listsObjects = Database.query( strSOQL );
        system.debug('**VAl of sobjects'+listsObjects);    
        return listsObjects;
        /*
        Integer intCount = listsObjects.size();  
        if ( intCount > 3 ) {  
              
            List < sObject > tempListsObjects = new List < sObject >();  
            for ( Integer i = 0; i <3; i++ )  
                tempListsObjects.add( listsObjects.get( i ) );  
              
            obj.listsObject = tempListsObjects;  
            strTitle += '3+';  
              
        } else {  
              
            obj.listsObject = listsObjects;  
            strTitle += String.valueOf( intCount );  
              
        }  
        strTitle += ')';        
        obj.strTitle = strTitle;  
        return obj;  
         */
    }  
      
    public class RelatedListResult {  
          
        @AuraEnabled  
        public String strTitle;  
        @AuraEnabled  
        public List < sObject > listsObject;  
          
    }  
}