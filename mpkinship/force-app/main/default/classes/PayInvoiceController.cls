public class PayInvoiceController {
    
	
    public Map<String, List<KinshipPaymentSyncController.PaymentObj>> mapA{get; set;}
    public Map<String, List<KinshipPaymentSyncController.PaymentObj>> mapC{get; set;}
    public Map<String, List<KinshipPaymentSyncController.PaymentObj>> mapTA{get; set;}
    public Map<String, List<KinshipPaymentSyncController.PaymentObj>> mapTC{get; set;}
    public Decimal grandTotal{get; set;}
    public Map<String, Decimal> totalMap{get; set;}
    public String lstVendor;
    public List<String> listVendor;
    public String lstCategory;
    public List<String> listCategory;
    public String lstOpp;
    public List<String> listOpp;
    public String lstPayment;
    public List<String> listPayment;
    public String caseWorker;
    public List<String> listcaseWorker;
    public String camp;
    public List<String> listcamp;
    public String startDate;
    public Date startDat;
    public String endDate;
    public Date endDat;
    public String status;
    public String rtvalue;
    public List<String> listrtvalue;
    public String ctvalue;
    public List<String> listctvalue;
    public String startAmount;
    public Decimal startAmountt;
    public String endAmount;
    public Decimal endAmountt;
    List<String> monthNames = new List<String>();
    
    public PayInvoiceController() {
        monthNames.add('January');
        monthNames.add('February');
        monthNames.add('March');
        monthNames.add('April');
        monthNames.add('May');
        monthNames.add('June');
        monthNames.add('July');
        monthNames.add('August');
        monthNames.add('September');
        monthNames.add('October');
        monthNames.add('November');
        monthNames.add('December');

        
        lstVendor = ApexPages.currentPage().getParameters().get('lstVendor');
        lstCategory = ApexPages.currentPage().getParameters().get('lstCategory');
        lstOpp = ApexPages.currentPage().getParameters().get('lstOpp');
        lstPayment = ApexPages.currentPage().getParameters().get('lstPayment');
        caseWorker = ApexPages.currentPage().getParameters().get('caseWorker');
        camp = ApexPages.currentPage().getParameters().get('camp');
        startDate = ApexPages.currentPage().getParameters().get('startDate');
        endDate = ApexPages.currentPage().getParameters().get('endDate');
        status = ApexPages.currentPage().getParameters().get('status');
        rtvalue = ApexPages.currentPage().getParameters().get('rtvalue');
        ctvalue = ApexPages.currentPage().getParameters().get('ctvalue');
        startAmount = ApexPages.currentPage().getParameters().get('startAmount');
        endAmount = ApexPages.currentPage().getParameters().get('endAmount');

        if(lstVendor != null && lstVendor != ''){
            listVendor = lstVendor.split(',');
        }
        if(lstCategory != null && lstCategory != ''){
            listCategory = lstCategory.split(',');
        }
        if(lstOpp != null && lstOpp != ''){
            listOpp = lstOpp.split(',');
        }
        if(lstPayment != null && lstPayment != ''){
            listPayment = lstPayment.split(',');
        }
        if(caseWorker != null && caseWorker != ''){
            listcaseWorker = caseWorker.split(',');
        }
        if(camp != null && camp != ''){
            listcamp = camp.split(',');
        }
        if(rtvalue != null && rtvalue != ''){
            listrtvalue = rtvalue.split(',');
        }
        if(ctvalue != null && ctvalue != ''){
            listctvalue = ctvalue.split(',');
        }
        if(startDate != null && startDate != '' && startDate != 'null'){
            startDat = Date.parse(startDate);
        }
        if(endDate != null && endDate != '' && endDate != 'null'){
            endDat = Date.parse(endDate);
        }
        if(startAmount != null && startAmount != '' && startAmount != 'null'){
            startAmountt = Decimal.valueof(startAmount);
        }
        if(endAmount != null && endAmount != '' && startAmount != 'null'){
            endAmountt = Decimal.valueof(endAmount);
        }
        System.debug(endAmount+'endAmount');
        List<KinshipPaymentSyncController.PaymentObj> payment =  KinshipPaymentSyncController.getPaymentData(this.listVendor,this.listCategory,this.listOpp, this.listPayment,this.listcaseWorker, this.listcamp,this.startDat, this.endDat, this.status, this.listrtvalue,this.listctvalue, this.startAmountt, this.endAmountt);
        Map<String, List<KinshipPaymentSyncController.PaymentObj>> mapofpayment = new Map<String, List<KinshipPaymentSyncController.PaymentObj>>();
        Map<String, List<KinshipPaymentSyncController.PaymentObj>> mapofpaymentc = new Map<String, List<KinshipPaymentSyncController.PaymentObj>>();
        Map<String, List<KinshipPaymentSyncController.PaymentObj>> mapofpaymentTA = new Map<String, List<KinshipPaymentSyncController.PaymentObj>>();
        Map<String, List<KinshipPaymentSyncController.PaymentObj>> mapofpaymentTC = new Map<String, List<KinshipPaymentSyncController.PaymentObj>>();
       List<KinshipPaymentSyncController.PaymentObj> staffOperations = new List<KinshipPaymentSyncController.PaymentObj>(); 
       List<KinshipPaymentSyncController.PaymentObj> oneTimeCaseActions = new List<KinshipPaymentSyncController.PaymentObj>(); 
       List<KinshipPaymentSyncController.PaymentObj> ongoingCaseActions = new List<KinshipPaymentSyncController.PaymentObj>(); 
       List<KinshipPaymentSyncController.PaymentObj> purchaseofServicesOrders = new List<KinshipPaymentSyncController.PaymentObj>(); 
       List<KinshipPaymentSyncController.PaymentObj> claimsPayable = new List<KinshipPaymentSyncController.PaymentObj>(); 
       List<KinshipPaymentSyncController.PaymentObj> specialWelfare = new List<KinshipPaymentSyncController.PaymentObj>(); 
       List<KinshipPaymentSyncController.PaymentObj> taxesPayable = new List<KinshipPaymentSyncController.PaymentObj>();     
       List<KinshipPaymentSyncController.PaymentObj> staffOperationss = new List<KinshipPaymentSyncController.PaymentObj>(); 
       List<KinshipPaymentSyncController.PaymentObj> oneTimeCaseActionss = new List<KinshipPaymentSyncController.PaymentObj>(); 
       List<KinshipPaymentSyncController.PaymentObj> ongoingCaseActionss = new List<KinshipPaymentSyncController.PaymentObj>(); 
       List<KinshipPaymentSyncController.PaymentObj> purchaseofServicesOrderss = new List<KinshipPaymentSyncController.PaymentObj>(); 
       List<KinshipPaymentSyncController.PaymentObj> claimsPayables = new List<KinshipPaymentSyncController.PaymentObj>(); 
       List<KinshipPaymentSyncController.PaymentObj> specialWelfares = new List<KinshipPaymentSyncController.PaymentObj>(); 
       List<KinshipPaymentSyncController.PaymentObj> taxesPayables = new List<KinshipPaymentSyncController.PaymentObj>();  
       List<KinshipPaymentSyncController.PaymentObj> taxesPayableA = new List<KinshipPaymentSyncController.PaymentObj>();List<KinshipPaymentSyncController.PaymentObj> taxesPayableC = new List<KinshipPaymentSyncController.PaymentObj>();                                                                                                                                          
        System.debug(payment+'payment');
        Decimal staffOperationsCount = 0;
        Decimal oneTimeCaseActionsCount = 0; 
        Decimal ongoingCaseActionsCount = 0;
        Decimal purchaseofServicesOrdersCount = 0; 
        Decimal claimsPayableCount = 0;
        Decimal specialWelfareCount = 0;
        Decimal taxesPayableCount = 0;
        Decimal staffOperationssCount = 0;
        Decimal oneTimeCaseActionssCount = 0; 
        Decimal ongoingCaseActionssCount = 0;
        Decimal purchaseofServicesOrderssCount = 0; 
        Decimal claimsPayablesCount = 0;
        Decimal specialWelfaresCount = 0;
        Decimal taxesPayablesCount = 0;
        grandTotal = 0;
        totalMap = new Map<String, Decimal>();
        for(integer i = 0 ; i < payment.size() ; i++){
            if(payment[i].status != 'Approved'){
                if(payment[i].fipsName == 'Alleghany Department of Social Services'){
                    if(payment[i].poRecordTypeName == 'Staff & Operations'){
                        staffOperations.add(payment[i]);
                        staffOperationsCount += payment[i].amount;
                        
                    }
                    if(payment[i].poRecordTypeName == 'One time Case Actions'){
                        oneTimeCaseActions.add(payment[i]);
                        oneTimeCaseActionsCount += payment[i].amount;
                    }
                    if(payment[i].poRecordTypeName == 'Ongoing Case Actions'){
                        ongoingCaseActions.add(payment[i]);
                        ongoingCaseActionsCount += payment[i].amount;
                    }
                    if(payment[i].poRecordTypeName == 'Purchase of Services Order'){
                        purchaseofServicesOrders.add(payment[i]);
                        purchaseofServicesOrdersCount += payment[i].amount;
                    }
                    if(payment[i].poRecordTypeName == 'Claims Payable'){
                        claimsPayable.add(payment[i]);
                        claimsPayableCount += payment[i].amount;
                    }
                    if(payment[i].poRecordTypeName == 'Special Welfare'){
                        specialWelfare.add(payment[i]);
                        specialWelfareCount += payment[i].amount;
                    }
                    if(payment[i].localityAccountCode == '61090'){
                        taxesPayable.add(payment[i]);
                        taxesPayableCount += payment[i].amount;
                    }
                }
                else if(payment[i].fipsName == 'Covington Department of Social Services'){
                    if(payment[i].poRecordTypeName == 'Staff & Operations'){
                        staffOperationss.add(payment[i]);
                        staffOperationssCount += payment[i].amount;
                    }
                    if(payment[i].poRecordTypeName == 'One time Case Actions'){
                        oneTimeCaseActionss.add(payment[i]);
                        oneTimeCaseActionssCount += payment[i].amount;
                    }
                    if(payment[i].poRecordTypeName == 'Ongoing Case Actions'){
                        ongoingCaseActionss.add(payment[i]);
                        ongoingCaseActionssCount += payment[i].amount;
                    }
                    if(payment[i].poRecordTypeName == 'Purchase of Services Order'){
                        purchaseofServicesOrderss.add(payment[i]);
                        purchaseofServicesOrderssCount += payment[i].amount;
                    }
                    if(payment[i].poRecordTypeName == 'Claims Payable'){
                        claimsPayables.add(payment[i]);
                        claimsPayablesCount += payment[i].amount;
                    }
                    if(payment[i].poRecordTypeName == 'Special Welfare'){
                        specialWelfares.add(payment[i]);
                        specialWelfaresCount += payment[i].amount;
                    }
                    if(payment[i].poRecordTypeName == '61090'){
                        taxesPayables.add(payment[i]);
                        taxesPayablesCount += payment[i].amount;
                    }
                }
            }
            
        }
        if(staffOperations.size() > 0){
            mapofpayment.put('Staff & Operations',staffOperations);
            totalMap.put('Staff & Operations',staffOperationsCount);
        }
        if(oneTimeCaseActions.size() > 0){
            mapofpayment.put('One time Case Actions',oneTimeCaseActions);
            totalMap.put('One time Case Actions',oneTimeCaseActionsCount);
        }
        if(ongoingCaseActions.size() > 0){
            mapofpayment.put('Ongoing Case Actions',ongoingCaseActions);
            totalMap.put('Ongoing Case Actions',ongoingCaseActionsCount);
        }
        if(purchaseofServicesOrders.size() > 0){
            mapofpayment.put('Purchase of Services Order',purchaseofServicesOrders);
            totalMap.put('Purchase of Services Order',purchaseofServicesOrdersCount);
        }
        if(claimsPayable.size() > 0){
            mapofpayment.put('Claims Payable',claimsPayable);
            totalMap.put('Claims Payable',claimsPayableCount);
        }
        if(specialWelfare.size() > 0){
            mapofpayment.put('Special Welfare',specialWelfare);
            totalMap.put('Special Welfare',specialWelfareCount);
        }
        if(taxesPayable.size() > 0){
            String month ='';
            for(integer j = 0 ; j < taxesPayable.size() ; j++){
                if(j > 0){
                    Integer monthNumber =  taxesPayable[j-1].paymentDate.month();
                    month = monthNames[monthNumber-1];
                    if(taxesPayable[j].paymentDate.month() != taxesPayable[j-1].paymentDate.month()){
                        mapofpaymentTA.put(month,taxesPayableA);
                        taxesPayableA = new List<KinshipPaymentSyncController.PaymentObj>();
                        taxesPayableA.add(taxesPayable[j]);
                    }
                    else{
                        taxesPayableA.add(taxesPayable[j]);
                    }
                }
                else if(j == 0){
                    taxesPayableA.add(taxesPayable[j]);
                }
            }
            mapofpaymentTA.put(month,taxesPayableA);
            mapofpayment.put('Taxes Payable',taxesPayable);
            totalMap.put('Taxes Payable',taxesPayableCount);
        }

        if(staffOperationss.size() > 0){
            mapofpaymentc.put('Staff & Operations',staffOperationss);
            totalMap.put('C Staff & Operations',staffOperationssCount);
        }
        if(oneTimeCaseActionss.size() > 0){
            mapofpaymentc.put('One time Case Actions',oneTimeCaseActionss);
            totalMap.put('C One time Case Actions',oneTimeCaseActionssCount);
        }
        if(ongoingCaseActionss.size() > 0){
            mapofpaymentc.put('Ongoing Case Actions',ongoingCaseActionss);
            totalMap.put('C Ongoing Case Actions',ongoingCaseActionssCount);
        }
        if(purchaseofServicesOrderss.size() > 0){
            mapofpaymentc.put('Purchase of Services Order',purchaseofServicesOrderss);
            totalMap.put('C Purchase of Services Order',purchaseofServicesOrderssCount);
        }
        if(claimsPayables.size() > 0){
            mapofpaymentc.put('Claims Payable',claimsPayables);
            totalMap.put('C Claims Payable',claimsPayablesCount);
        }
        if(specialWelfares.size() > 0){
            mapofpaymentc.put('Special Welfare',specialWelfares);
            totalMap.put('C Special Welfare',specialWelfaresCount);
        }
        if(taxesPayables.size() > 0){
            for(integer j = 0 ; j < taxesPayables.size() ; j++){
                Integer monthNumber =  taxesPayables[j].paymentDate.month();
                String month = monthNames[monthNumber-1];
                if(j > 0){
                    if(taxesPayables[j].paymentDate.month() != taxesPayables[j-1].paymentDate.month()){
                        mapofpaymentTC.put(month,taxesPayableC);
                        taxesPayableC = new List<KinshipPaymentSyncController.PaymentObj>();
                    }
                    else{
                        taxesPayableC.add(taxesPayable[j]);
                    }
                }
                else if(j == 0){
                    taxesPayableC.add(taxesPayable[j]);
                }
                else if(j == taxesPayables.size()){
                    mapofpaymentTC.put(month,taxesPayableC);
                }
                
            }
            mapofpaymentc.put('Taxes Payable',taxesPayables);
            totalMap.put('C Taxes Payable',taxesPayablesCount);
        }
        for(String keyVal: totalMap.keySet()){
            grandTotal += totalMap.get(keyVal);
        }
        mapA = mapofpayment;
        mapC = mapofpaymentc;
        mapTA = mapofpaymentTA;
        mapTC = mapofpaymentTC;
        
    }
    public Integer SizeOfMapA { get { return mapA.size(); } } 
    public Integer SizeOfMapC { get { return mapC.size(); } } 
    public Integer SizeOfMapTA { get { return mapTA.size(); } } 
    public Integer SizeOfMapTC { get { return mapTC.size(); } } 
    
}