global class KinshipTJAutoPostedBatch implements Database.Batchable<sObject>, Database.Stateful, Schedulable{
    global void execute(SchedulableContext SC){
        Database.executeBatch(new KinshipTJAutoPostedBatch(), 200);
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        String query = 'SELECT id, Name, StartDate, EndDate, RecordTypeId, ParentId, Parent.ParentId, Status, FIPS__c, Cate_Type__c ' + 
                        'FROM Campaign ' +
                        'WHERE Auto_Post_Date__c = TODAY ' +
                        'AND RecordType.Name = \'LASER Reporting Period\' ';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Campaign> scope){
        if(!scope.isEmpty()) {
            //autoPostedTJ(scope);
            TransactionJournalService.autoPostedTJ(scope);
        }
    }

    global void finish(Database.BatchableContext BC){
        // get Opp SELECT id, name, RecordType.Name,StageName,Status__c FROM Opportunity
    }

}