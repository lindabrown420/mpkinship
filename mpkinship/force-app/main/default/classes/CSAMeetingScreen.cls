public class CSAMeetingScreen {
    @AuraEnabled(continuation = true)
    Public static CSA_Meeting_Agenda__c getAgendaRecord(ID AgendaId){
        return [Select Id, Name, Location_of_Meeting__c,Meeting_Date__c,Meeting_End_Time2__c,Meeting_Start3_Time__c,
                Meeting_Status__c,Review_Type__c,Send_Notification__c,Team__c From CSA_Meeting_Agenda__c Where Id =: AgendaId];
    } 
    @AuraEnabled(continuation = true)
    Public static List<Team_Member__c> getTeamMembers(ID teamID){
        List<Team_Member__c> tm = new List<Team_Member__c>();
        try{
            tm = [Select Id,Contact__c,Contact_Name__c, Contact_Email__c, CSA_Meeting_Notification__c  from Team_Member__c where CSA_Team__c =: teamID];
            return tm;
        }catch(Exception e){
            return tm;
        }
    }
    
    @AuraEnabled(continuation = true)
    Public static string AddAgendaMember(ID AgendaId, Id ContactId, ID TeamId){
        CSA_Meeting_Members__c CM = new CSA_Meeting_Members__c();
        try{
            
            CM.Contact__c = ContactId;
            CM.CSA_Meeting_Agenda__c = AgendaId;
            insert CM;   
            
            Team_Member__c TM = new Team_Member__c();
            TM.Contact__c = ContactId;
            Tm.CSA_Team__c = TeamId;
            insert TM;
        }catch(Exception e){
            return e.getmessage();
        }
        return 'Member has been created successfully!';  
    }
    @AuraEnabled(continuation = true)
    Public static string AddAgendaMembers(ID AgendaId, List<Id> ContactIds){
        List<CSA_Meeting_Members__c> CMlist = new List<CSA_Meeting_Members__c>();
        //List<Team_Member__c> TMList = new List<Team_Member__c>();
        CSA_Meeting_Members__c CM = new CSA_Meeting_Members__c();
        //Team_Member__c TM = new Team_Member__c();
        String result = '';
        try{
            Delete [Select Id From CSA_Meeting_Members__c Where CSA_Meeting_Agenda__c = :AgendaId];
            for(Id cid: ContactIds){
            CM.Contact__c = cid;
            CM.CSA_Meeting_Agenda__c = AgendaId;
            //TM.Contact__c = cid;
            //TM.CSA_Team__c = TeamId;    
            
            CMlist.add(CM);
            //TMList.add(TM);
            
            CM = new CSA_Meeting_Members__c(); 
            //TM = new Team_Member__c();    
         }
         insert CMlist;
         //insert TMList;
        result =  'CSA Agenda has been saved successfully!'; 
                        
            //insert TM;
        }catch(Exception e){
            return e.getmessage();
        }
        return result;
    }
    @AuraEnabled(continuation = true)
    Public static List<CSA_Meeting_Members__c> getTeamMembersFromAgenda(ID AgendaId){
        return [Select Id, Name,Contact__r.Name, Contact__r.Email,Send_Notification_of_CSA_Meeting__c,Contact_Name__c,Team_Name__c,Contact_Email__c  From CSA_Meeting_Members__c 
                Where CSA_Meeting_Agenda__c =: AgendaId];
    } 
    @AuraEnabled(continuation = true)
    Public static List<CSA_Meeting_Agenda_Item__c> getAgendaItems(ID AgendaId){
        return [Select Id, Name,Agenda_Comments__c, Record_view__c, Case_KNP__c,Review__c,VACMS_Number__c,Case_Name__c,Review_Type__c, Time__c,IFSP__c,Time3__c,IFSP_Name__c From CSA_Meeting_Agenda_Item__c 
                Where CSA_Meeting_Agenda__c =: AgendaId order by Time3__c];
    } 
    @AuraEnabled(continuation = true)
    Public static string deleteitem(ID itemid){
        try{
            CSA_Meeting_Agenda_Item__c r = new CSA_Meeting_Agenda_Item__c(); 
            r.Id = itemid;
            delete r;
            return 'true';
        }catch(exception e){
            return e.getMessage();
        }        
    }
    
    @AuraEnabled(continuation = true)
    Public static string deleteteam(ID teamid){
        try{
            CSA_Meeting_Members__c r = new CSA_Meeting_Members__c(); 
            r.Id = teamid;
            delete r;
            return 'true';
        }catch(exception e){
            return e.getMessage();
        }
        
    }
    
    @AuraEnabled(continuation = true)
    Public static string SendNotification(ID agendaid){
        try{
            CSA_Meeting_Agenda__c r = new CSA_Meeting_Agenda__c(); 
            r.Id = agendaid;
            r.Send_Notification__c = True;            
            update r;
            return 'true';
        }catch(exception e){
            return e.getMessage();
        }        
    }
    
        @AuraEnabled(continuation = true)
    Public static string UpdateCancellation(ID agendaid){
        try{
            CSA_Meeting_Agenda__c r = new CSA_Meeting_Agenda__c(); 
            r.Id = agendaid;
            r.Meeting_Status__c = 'Cancelled';
            update r;
            return 'true';
        }catch(exception e){
            return e.getMessage();
        }        
    }
    
    @AuraEnabled(continuation = true)
    Public static string CheckExstmember(ID ContactId, id TeamId){
         try{
            Team_Member__c ExtTM = [Select id from Team_Member__c where Contact__c = :ContactId
                                   AND CSA_Team__c = :TeamId limit 1];
            system.debug(ExtTM + ' ExtTM???');
         }catch(exception e){
            return 'false';  
        }  
        return 'Member already exist in the team';  
    }
    @AuraEnabled(continuation = true)
    Public static string AddExstmember(ID ContactId, id TeamId, id AgendaId){      
        try{
                Team_Member__c TM = new Team_Member__c();
                TM.Contact__c = ContactId;
                Tm.CSA_Team__c = TeamId;
                insert TM;
            
             CSA_Meeting_Members__c CM = new CSA_Meeting_Members__c();   
             CM.Contact__c = ContactId;
             CM.CSA_Meeting_Agenda__c = AgendaId;
             insert CM; 
            
                return 'true';   
            
        }catch(exception e){
            return e.getMessage();
        }        
    }
    
    @auraEnabled(continuation = true)
    public static boolean CheckMeetingStartTimeForToday(date meetingStartDate, String meetingStartTime){     
        system.debug('**Enterd here in checkmeeting');          
        string timestring= meetingStartTime;
        if(timestring!= null && timestring!='' && meetingStartDate!=null && meetingStartDate==system.today()){
            system.debug('entered inside if');
            //FIND OUT TIME IN USER TIMEZONE
             Datetime now = Datetime.now(); 
            Integer offset = UserInfo.getTimezone().getOffset(now);
            Datetime local = now.addSeconds(offset/1000);
            system.debug('**local'+local);
        
            list<String> splittime = timeString.split(':');
            system.debug('valo f splittime'+splittime);
            time timeval = Time.newInstance(integer.valueof(splittime[0]),integer.valueof(splittime[1]),00,00);
            system.debug('val of timval'+timeval);            
            DateTime DT = DateTime.newInstanceGMT(meetingStartDate,timeval);
            system.debug('**VAl of dt'+DT);
            
            if(local > DT){
                return true;
            }
        }
        return false;
    }
}