@isTest
public class LaserMasterTableControllerTest {
    @testSetup
    static void testRecords(){
        KinshipAdmin__c  lmt = new KinshipAdmin__c ();
        lmt.name = 'Laser Master Table';
        insert lmt;

        ContentVersion contentVersion = new ContentVersion(
        Title = 'Laser Master table',
        PathOnClient = 'Laser.csv',
        VersionData = Blob.valueOf('CFDA,LASER Alias name,Cost Code,Allocated,Cost Code Description,PRGM,Grant,Project,B Line,Exp Type,Federal 1000,State 0100,local 0500,Special Fund,Cost Code,Cases,Children,Adult,Recipientc,Child in Res. Fac.\n0,0001B,00001,SA,Staff & Operations Base Budget,33333,0,90001,855,B,N/A,N/A,N/A,,00001,n/a,n/a,n/a,n/a,n/a'),
        IsMajorVersion = true
        );
        insert contentVersion;    
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];

        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = lmt.id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;
    }

    @isTest
    static void testGetListFile() {
        Test.startTest();
        List<Object> lstResult = LaserMasterTableController.getListFile();
        Test.stopTest();

        System.assertEquals(1, lstResult.size(), 'The method getListFile does not return a record');
    }

    @isTest
    static void testUpdateData() {
        List<ContentVersion>  lstData = [SELECT Id, Title, VersionData, ContentSize, FileType, TagCsv, ContentDocumentId, Public_Date__c
                                            FROM ContentVersion 
                                            Limit 1];
        Test.startTest();
        Boolean result = LaserMasterTableController.updateData(lstData[0].id, Date.today());
        Test.stopTest();

        System.assertEquals(true, result, 'The method updateData does not return true');
    }
}