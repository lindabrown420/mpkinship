public class OpportunityLineItemsTrgHelper{

    public static boolean isRun = false;
    public static boolean IsInsertRun = false;
    public static boolean IsDeleteRun =false;
    public static boolean IsValidate=false;
    public static boolean IsUpdateValidate=false;
    public static boolean IsIVEvalidate=false;
    public static boolean IsIVEvalidateUpdate = false;
    public static boolean Iscatupdated=false;
    public static boolean IsCateogryrun=false;
    
    public static void ValidateIVEForCategory(set<id>opportunitylineitemids,set<id>opportunityIds){
        if(trigger.isInsert)
            IsIVEvalidate=true;
        if(Trigger.isUpdate)
            IsIVEvalidateUpdate=true;
        system.debug('***entered in validateiveforcategory');
        map<id,string> caseFieldOppMap = new map<id,string>(); 
        list<opportunity> opplist=[select id,Kinship_Case__c,Kinship_Case__r.Title_IVE_Eligibility__c from opportunity
                                    where id in: opportunityIds and Kinship_Case__c!=null];
        for(opportunity opp : opplist){
             caseFieldOppMap.put(opp.id,opp.Kinship_Case__r.Title_IVE_Eligibility__c);   
        } 
        
        for(opportunitylineitem opp : [select id,category__c,opportunityid,category__r.category_Type__c from opportunitylineitem where 
                                        id in:opportunitylineitemids] ){
            if(caseFieldOppMap.containskey(opp.opportunityid)){
               string titleIVEvalue= caseFieldOppMap.get(opp.opportunityid);
               system.debug('**Value of title ive'+ titleIVEvalue);
               system.debug('**opplineitem categorytype val'+ opp.category__r.Category_Type__c);
               if(opp.category__c!=null && opp.category__r.Category_Type__c!=null && 
                   (opp.category__r.Category_Type__c=='CSA' || opp.category__r.Category_Type__c=='IVE')){
                   if (titleIVEvalue!='1' && titleIVEvalue!='2'){
                       system.debug('**Should enter in adderror');
                       trigger.newmap.get(opp.id).addError('Please provide Title IV-E eligibility field value to the case related to its Purchase Order.');
                   }    
                   else if(titleIVEvalue=='1' && opp.category__r.Category_Type__c=='IVE'){
                       trigger.newmap.get(opp.id).addError('You cannot use category of type IV-E if its associated case title IV-E eligibility field has no value.');
                   }
               }
            }
        }                           
    }
    
    public static void ValidateCategory(list<opportunitylineitem>opportunitylineitems,set<id>opportunityIds){
        if(trigger.isInsert)
           IsValidate=true; 
        if(trigger.isUpdate)
           IsUpdateValidate=true;    
        list<string> opportunityitems = new list<string>();
        list<opportunitylineitem> opplineitems=[select id,opportunityid,category__C,Product2Id from opportunitylineitem
                                    where opportunityid in: opportunityIds];
        for(opportunitylineitem opitem : opplineitems){
            if(!opportunityitems.contains(opitem.Product2Id+'-'+opitem.category__C))
                opportunityitems.add(opitem.Product2Id+'-'+opitem.category__C);
        } 
        
        for(opportunitylineitem  oppitem :  opportunitylineitems){
            if(opportunityitems.contains(oppitem.Product2Id+'-'+oppitem.category__c)){
                oppitem.addError('You cannot duplicate the category with a service.');
                break;
            }
        }                      
                                    
    }
    
    public static void validateLineItems(set<id> lineitemIds, map<id,opportunitylineitem>opplineItemIDMap){
        if(Trigger.isinsert)
            IsInsertRun = true;
         else if(Trigger.isupdate)
            isRun = true; 
        else if (Trigger.isdelete)
            IsDeleteRun =true;
         system.debug('**Val of isinsert'+IsInsertRun);
            
         list<OpportunityLineItem> opplineitems =[select id,OpportunityId,Opportunity.stagename from OpportunityLineItem
                                                  where id in: lineitemIds and opportunity.stagename !='Draft'];
         for(opportunityLineITem oppitem : opplineitems){
             if(opplineItemIDMap.containskey(oppitem.id)){
                 system.debug('***inside contains');
                 opplineItemIDMap.get(oppitem.id).addError('You cannot make changes once Purchase Order has been approved or awaiting approval.');
             }
         }                                         
                                                  
    }
    // KIN 63
    public static void POServicesCheck(List<OpportunityLineItem> oppLineList, List<Id> oppIdsList){
        Map<Id,Opportunity> mapOpp;
            if(!PurchaseOrderController.IspoScreenRun &&  !PurchaseOrderControllerEdit.IspoScreenRun){
                mapOpp = new Map<Id,Opportunity>([Select Id,name,recordtype.name from Opportunity where Id=:oppIdsList]);
            }
        for(opportunityLineITem oppitem : oppLineList){
            System.debug('OpportunityId'+oppitem.OpportunityId);
            if(oppitem.OpportunityId!=Null && mapOpp.get(oppitem.OpportunityId) !=null){
                if(mapOpp.get(oppitem.OpportunityId).recordtype.name == 'Purchase of Services Order')
                oppitem.addError('Can not delete a Service related to POSO');
            }
        }
    }
    //ends
   
   //FOR CHANGING THE CATEGORY OF INVOICES 
    /* public static void UpdateCategoryInvoices(map<id,opportunitylineitem> opportunitylineItemMap,map<id,id> opplineitemOldCategoryMap,map<id,set<id>> oldcategoryandopplineitemMap){
        IsCateogryrun=true;  
        TransactionJournalService.byPassCheckPosted = true; 
        map<id,category__c> oppitemCatMap = new map<id,category__c>(); 
        list<category__C> category =[select id,Category_Type__c from category__C where id in: oldcategoryandopplineitemMap.keyset()];
        for(Category__C cat : category){
            if(oldcategoryandopplineitemMap.size()>0 && oldcategoryandopplineitemMap.containskey(cat.id) && 
            oldcategoryandopplineitemMap.get(cat.id)!=null){
                for(id oppitem : oldcategoryandopplineitemMap.get(cat.id)){
                    oppitemCatMap.put(oppitem,cat);    
                }     
            }
        } 
        system.debug('**Val of oppitemcatmap'+ oppitemCatMap);     
        list<Payment_Line_Item__c> invoiceItems = [select id,Payment__c,Payment__r.Category_lookup__c,Opportunity_Product__c from Payment_Line_Item__c
                                                    where Opportunity_Product__c in: opportunitylineItemMap.keyset()];
       
        list<npe01__OppPayment__c> paymentsToUpdate = new list<npe01__OppPayment__c>();                                           
        for(Payment_Line_Item__c invoicelineItem : invoiceItems){
            if(opplineitemOldCategoryMap.size() >0 && opplineitemOldCategoryMap.containskey(invoicelineItem.Opportunity_Product__c) && 
                opportunitylineItemMap.size() >0 && opportunitylineItemMap.containskey(invoicelineItem.Opportunity_Product__c) &&
                opportunitylineItemMap.get(invoicelineItem.Opportunity_Product__c)!=null && opplineitemOldCategoryMap.get(invoicelineItem.Opportunity_Product__c)!=null){
                if(invoicelineItem.Payment__r.Category_lookup__c == opplineitemOldCategoryMap.get(invoicelineItem.Opportunity_Product__c)){
                    npe01__OppPayment__c payment = new npe01__OppPayment__c();
                    payment.id = invoicelineItem.Payment__c;
                    payment.Category_lookup__c = opportunitylineItemMap.get(invoicelineItem.Opportunity_Product__c).Category__c;
                    paymentsToUpdate.add(payment);
                }  
            }
        }
        
        list<opportunitylineitem> opplineitemstoUpdate = new list<opportunitylineitem>();
        
        list<opportunitylineitem> oppitems=[select id,category__c,category__r.Category_Type__c,status__c from opportunitylineitem where id in: opportunitylineItemMap.keyset()];
        for(opportunitylineitem oppitem : oppitems){
             opportunitylineitem oppitemupdation = new opportunitylineitem();
            oppitemupdation.id = oppitem.Id;
       
            if(oppitem.category__c!=null && oppitem.category__r.Category_Type__c=='CSA'){
                oppitemupdation.Status__c='Hold';
            }
            else if (oppitem.category__c!=null && oppitem.category__r.Category_Type__c=='IVE' && 
                oppitemCatMap.size()>0 && oppitemCatMap.containskey(oppitem.Id)){
                
                if(oppitemCatMap.get(oppitem.Id).Category_Type__c!='IVE' && oppitemCatMap.get(oppitem.Id).Category_Type__c!='CSA'){                    
                    oppitemupdation.Status__c='Hold';
                }
            }
            else
                oppitemupdation.Status__c='Completed';
            opplineitemstoUpdate.add(oppitemupdation);  
        }
        
        
        if(paymentsToUpdate.size()>0)
            update paymentsToUpdate;
            
        if(opplineitemstoUpdate.size()>0)
            update opplineitemstoUpdate;     
    }*/
    
   

}