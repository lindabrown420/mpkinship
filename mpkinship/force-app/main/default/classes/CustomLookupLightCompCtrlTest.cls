@isTest
public class CustomLookupLightCompCtrlTest {
	public static testmethod void fetchLookUpValuesTest(){
        contact cc = new contact();
        cc.lastname='test contact';
        cc.Email='test@gmail.com';
        insert cc;
        LASER_Cost_Code__c lc = new LASER_Cost_Code__c();
        lc.name='222';
        insert lc;
        CustomLookupLightCompCtrl.fetchLookUpValues('test','Contact');
        campaign camp = new campaign();
        camp.recordtypeid=Schema.SObjectType.campaign.getRecordTypeInfosByDeveloperName().get('Fiscal_Year').getRecordTypeId();
        camp.name='test campaign';
        camp.Type='Local';
        camp.StartDate=date.today();
        camp.EndDate=date.today().adddays(100);
        insert camp;
        CustomLookupLightCompCtrl.fetchLookUpValues('test','Campaign');
        
        product2 pro = new product2();
        pro.name='test';
        pro.Unit_Measure__c='2';
        pro.IsActive=true;
        insert pro;
        CustomLookupLightCompCtrl.fetchLookUpValues('test','Product2');
    }
}