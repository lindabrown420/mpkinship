@isTest
public class AddCategoryControllerTest {
    @isTest
    public static void testInsert(){
        Locality_Account__c la = new Locality_Account__c();
        la.Name='222';
        insert la;
        Cost_Center__c cc = new Cost_Center__c(Name = '12345');
        insert cc;
        Chart_of_Account__c coa = new Chart_of_Account__c();
        coa.Locality_Account__c = la.Id;
        coa.Cost_Center__c = cc.id;
        insert coa;
        Test.startTest();
        List<Chart_of_Account__c> lstReturn = AddCategoryController.getCOA(la.Id);
        Test.stopTest();
        System.assertEquals(1, lstReturn.size(), 'The list return does not equal 1.');
    }
}