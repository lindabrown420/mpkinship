@isTest
public class SWRControllerTest {
    static testmethod void testmethod1(){
        
        TriggerActivation__c  ta = new TriggerActivation__c();
        ta.name='PurchaseOrder Trigger';
        ta.isActive__c=false;
        insert ta;
        
        
         TriggerActivation__c  ta1 = new TriggerActivation__c();
        ta1.name='UpdateCSACoordinatorSign';
        ta1.isActive__c=false;
        insert ta1;  
        
        account accountrec = new account();
        accountrec.Name='test vendor';
        accountrec.Tax_ID__c='333-33-3333';
        accountrec.Entity_Type__c='Individual';
        insert accountrec;
        
        id catrecType = Schema.SObjectType.Category__c.getRecordTypeInfosByName().get('LASER Category').getRecordTypeId();
        
         Locality_Account__c la = new Locality_Account__c();
        la.name='33';
        insert la;
         
        string catType = SObjectType.Category__c.Fields.Category_Type__c.PicklistValues[0].getValue(); 
        
        Category__c category = new Category__c();
        category.Name='222';
        category.Category_Type__c= catType;
        category.RecordTypeId = catrecType;
        category.Locality_Account__c=la.id;
        insert category;
        
         fips__c fip= new fips__c();
        fip.name = 'Alleghany';
        insert fip;
         
        Campaign parent = new Campaign();
        parent.name = '2021';
        parent.StartDate = Date.Today().addDays(-10);
        parent.EndDate = Date.Today().addDays(10);
        parent.Fiscal_Year_Type__c = 'DSS';
        parent.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId();
        insert parent;
        id recType = Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('LASER_Reporting_Period').getRecordTypeId();
        campaign camp = new campaign();
        camp.name = 'May';
        camp.recordtypeid = recType;
        camp.Begin_Date__c=system.today();
        camp.EndDate=system.today().adddays(2);
        camp.parentId = parent.id;
        camp.FIPS__c =  fip.id;
        insert camp;
        
        
        
        
         
        Cost_Center__c cc = new Cost_Center__c();
        cc.Name='test';
        insert cc;
        Chart_of_Account__c ca = new Chart_of_Account__c();
        ca.Locality_Account__c=la.id;
        ca.FIPS__c=fip.id;
        ca.Cost_Center__c=cc.id;
        insert ca;
         
        id opprecType = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Staff_Operations').getRecordTypeId();
        opportunity opprecord = new opportunity();
        opprecord.CloseDate=date.today();
        opprecord.End_Date__c=date.today() + 15;
        opprecord.StageName='Draft';
        opprecord.Name='test opportunity';        
        opprecord.Amount=20000;
        opprecord.AccountId=accountrec.id;
        opprecord.Accounting_Period__c = camp.id;
        opprecord.RecordTypeId=opprecType;
        opprecord.Category__c = category.Id;
        opprecord.Service_End_Date__c = date.today() + 5;
        insert opprecord;
        
       id ROCType = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Report_of_Collection').getRecordTypeId();
        opportunity ROCrecord = new opportunity();
        ROCrecord.CloseDate=date.today() + 2;
        ROCrecord.End_Date__c=date.today() + 20;
        ROCrecord.StageName='Draft';
        ROCrecord.Name='test opportunity';        
        ROCrecord.Amount=20000;
        ROCrecord.AccountId=accountrec.id;
        ROCrecord.Accounting_Period__c = camp.id;
        ROCrecord.RecordTypeId=ROCType;
        ROCrecord.Category__c = category.Id;
        opprecord.Service_End_Date__c = date.today() + 1;
        insert ROCrecord;
        
        id payrecType = Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Vendor_Invoice').getRecordTypeId();
        Payment__c payment = new Payment__c();
        payment.Opportunity__c=opprecord.id;
        payment.Payment_Amount__c=500;
        payment.vendor__c = accountrec.Id;
        payment.Payment_Date__c = System.today();
        payment.RecordTypeId=payrecType;
        payment.Category_lookup__c = category.Id;
        payment.Prior_Refund1__c = 10;
        payment.Refund_Amount__c = 5;
        insert payment;
        
       
        id payrecType1 = Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Special_Welfare_Reimbursement').getRecordTypeId();
        Payment__c payment11 = new Payment__c();
        payment11.Opportunity__c=opprecord.id;
        payment11.Payment_Amount__c=2000;
        payment11.vendor__c = accountrec.Id;
        payment11.Payment_Date__c = System.today();
        payment11.RecordTypeId=payrecType1;
        payment11.Payment__c = payment.Id;
        payment11.Category_lookup__c = category.Id;
        payment11.Refund_Amount__c = 5;
        payment11.Prior_Refund1__c = 10;
        // insert payment11;
        
        id conType = Schema.SObjectType.contact.getRecordTypeInfosByDeveloperName().get('Contacts').getRecordTypeId();
        
        contact con = new contact();
        con.recordtypeid=conType;
        con.LastName='contactrec';
        con.FirstName='test';
        con.Email='test@contact.com';        
        insert con;
        
        Kinship_Case__c caserec = new Kinship_Case__c();
        caserec.Primary_Contact__c=con.id;
        insert caserec;
        
        Special_Welfare_Account__c swa = new Special_Welfare_Account__c();
        swa.Fund_Name__c = 'test';     
        swa.Case_KNP__c = caserec.Id;
        insert swa;
        
        DateTime todaysDate = System.today(); 
        String todaysDateStr = todaysDate.format('yyyy-MM-dd'); 
        
        String accountingPeriod = parent.Id; 
        String endDate = todaysDateStr;
        String startDate = todaysDateStr;
        String recipient = caserec.Id;
        String SWAID = swa.Id;    
        
        List<Payment__c> reimbursements =  new List<Payment__c>();
        reimbursements.add(payment11);
        
        test.startTest();
        SWRController contr = new SWRController();
        SWRController.getVendorInvoiceByFilter(accountingPeriod,endDate,startDate,recipient);
        SWRController.getSWACaseId(SWAID);
        try{
        	SWRController.createReimbursements(reimbursements);    
        }catch(Exception e){}
        
        test.stopTest();
    }
}