public class CSACalendarController {
    @AuraEnabled
    public static List<CSA_Meeting_Agenda__c> getAllAgenda(){
        return [SELECT Id, Name, Location_of_Meeting__c,Meeting_Date__c,Meeting_End_Time2__c,Meeting_Start3_Time__c,
                    Meeting_Status__c,Review_Type__c,Send_Notification__c,Team__c
                    FROM CSA_Meeting_Agenda__c
                    WHERE Meeting_Start3_Time__c != null 
                    AND Meeting_End_Time2__c != null
                    AND Meeting_Status__c != 'Cancelled'
                    AND RecordType.Name = 'CSA Meeting'];
    }

    @AuraEnabled
    public static Boolean checkProfile(){
        List<ObjectPermissions> lstObj = [SELECT Id, ParentId, SobjectType, PermissionsModifyAllRecords, 
                                PermissionsViewAllRecords, PermissionsDelete, 
                                PermissionsEdit, PermissionsRead, PermissionsCreate 
                                FROM ObjectPermissions 
                                WHERE Parent.IsOwnedByProfile = true 
                                AND SObjectType = 'CSA_Meeting_Agenda__c' 
                                AND Parent.Profileid = :Userinfo.getProfileId()];
        if(lstObj != null && lstObj.size() > 0 && lstObj[0].PermissionsCreate) {
            return true;
        } else {
            return false;
        }
    }
}