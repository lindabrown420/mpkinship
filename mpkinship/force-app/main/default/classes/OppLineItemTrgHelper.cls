public class OppLineItemTrgHelper {
    public static void ValidatePOSOscreen(list<opportunitylineitem> opportunitylineitems,set<id> opportunityIds,set<Id> catId){
        Id POSORecTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Purchase of Services Order').getRecordTypeId();
        Map<Id,opportunity> oppmap=new Map<Id,Opportunity>([select id,Kinship_Case__c,Category__c,Category__r.Category_Type__c,Amount from opportunity
                                    where id in: opportunityIds and Kinship_Case__c!=null and recordTypeId=:POSORecTypeId]);
        List<Opportunity> oppList= [select id,Kinship_Case__c,Category__c,Amount from opportunity
                                    where id in: opportunityIds and Kinship_Case__c!=null and recordTypeId=:POSORecTypeId];
        Map<Id, category__c> catmap= new Map<Id,category__c>([Select Id, Category_Type__c from category__c where Id=:catId]);
        System.debug('catmap'+catmap.size());
        List<Id> caseIds= new List<Id>();
        for(Opportunity opp: oppList){
            caseIds.add(opp.Kinship_Case__c);
        }
        Map<Id,List<Special_Welfare_Account__c>> mapCase= new Map<Id,List<Special_Welfare_Account__c>>();
        if(caseIds.size()>0){
            for(Kinship_Case__c cases : [Select Id, name,(Select Id,Name,Date_Closed__c,Current_Balance__c from Special_Welfare_Associations__r) from  Kinship_Case__c where Id=:caseIds]){
                mapCase.put(cases.Id,cases.Special_Welfare_Associations__r);
            }
        }
        list<opportunitylineitem> opplineitems=[select id,opportunityid,category__C,Category__r.Category_Type__c from opportunitylineitem
                                    where opportunityid in: opportunityIds];
        Set<Opportunity> oppToUpdate= new Set<Opportunity>();
        Decimal sumAmount=0;
        System.debug('opportunitylineitems'+opportunitylineitems.size());
        for(opportunitylineitem line:opportunitylineitems){
            sumAmount = sumAmount + line.Subtotal;
            System.debug('cat type'+catmap.get(line.Category__c).Category_Type__c);
            if(catmap.get(line.Category__c).Category_Type__c == 'Special Welfare'){
                System.debug('@@@@'+line.opportunityid);
                if(line.opportunityid!=Null){
                    if(oppmap.get(line.opportunityid)!=null){
                        System.debug('@@@@0'+oppmap.get(line.opportunityid).Kinship_Case__c);
                        if(oppmap.get(line.opportunityid).Kinship_Case__c!=null){
                            
                            System.debug('@@@@1'+(mapCase.get(oppmap.get(line.opportunityid).Kinship_Case__c).size()));
                            if(mapCase.get(oppmap.get(line.opportunityid).Kinship_Case__c).size()>0){
                                if(mapCase.get(oppmap.get(line.opportunityid).Kinship_Case__c)[0].Date_Closed__c <Date.today()){
                                    line.addError('Please select a case with Active Special Welfare Account');
                                } 
                                else{
            
                                    oppmap.get(line.opportunityid).Special_Welfare_Account__c=mapCase.get(oppmap.get(line.opportunityid).Kinship_Case__c)[0].Id;
                                    oppToUpdate.add(oppmap.get(line.opportunityid));
                                    if(mapCase.get(oppmap.get(line.opportunityid).Kinship_Case__c)[0].Id != null && line.Subtotal!=Null && mapCase.get(oppmap.get(line.opportunityid).Kinship_Case__c)[0].Current_Balance__c!=NULL){
                                        System.debug('@@@sum '+ sumAmount);
                                        if( sumAmount > mapCase.get(oppmap.get(line.opportunityid).Kinship_Case__c)[0].Current_Balance__c){
                                            line.addError('Amount should be less than or Equal Current Balance of Special Welfare Account. Current Balance of Special Welfare Account is :'+mapCase.get(oppmap.get(line.opportunityid).Kinship_Case__c)[0].Current_Balance__c);
                                        }
                                    }
                                    
                               }
                            }
                            else{
                                line.addError('Please select a case with Active Special Welfare Account');
                            }
                        }
                        else{
                            System.debug('no case');
                        }
                    }
                    
                }
            }
            
        }
        List<Opportunity> oppUpdate= new List<Opportunity>();
        oppUpdate.addAll(oppToUpdate);
        if(oppUpdate.size()>0){
            try{
                update oppUpdate;
            }
            catch(Exception e){
                System.debug('Exception:'+e);
            }
        }
        
    }
}