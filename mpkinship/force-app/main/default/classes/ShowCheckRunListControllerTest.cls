@isTest
private class ShowCheckRunListControllerTest {
    @TestSetup
    static void initData(){
        Id profileId1 = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User u = new User(FirstName='Test',LastName= 'User',profileid=profileId1 , email='abc123@test.com',
                            username='abc123@test.com.prod',Alias='zbd',LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', EmailEncodingKey='UTF-8',
                            TimeZoneSidKey='America/Los_Angeles');
        
        Account acc = new Account(Name = 'test acc', Tax_ID__c = '123-56-7901', Entity_Type__c = 'Individual');
        insert acc;
        
        Locality_Account__c locality = new Locality_Account__c(Name = '12345');
        insert locality;

        Category__c cate = new Category__c(Name = 'test cate', Category_Type__c = 'CSA', Locality_Account__c = locality.id , RecordTypeId = Schema.SObjectType.Category__c.getRecordTypeInfosByName().get('LEDRS Category').getRecordTypeId());
        insert cate;
        
        Kinship_Case__c kcase = new Kinship_Case__c(Primary_Social_Worker__c = u.id);

        Opportunity opp = new Opportunity(Name = 'test Opp', AccountId = acc.id, Category__c = cate.id, CloseDate = Date.today().addDays(10),
                                            StageName='Open', Kinship_Case__c = kcase.id);
        insert opp;

        Payment__c payment = new Payment__c(Opportunity__c = opp.id, 
                                                                Scheduled_Date__c = Date.today(), 
                                                                Status__c = 'Submitted For Approval',
                                                                Invoice_Stage__c = 'Ready to Pay',
                                                                Written_Off__c = false);
        insert payment;
        
        Check_Run__c checkrun = new Check_Run__c();
        insert checkrun;

        Kinship_Check__c check = new Kinship_Check__c(Amount__c = 400, Check_No__c = '1234', Check_Run__c = checkrun.id);
        insert check;

        payment.Kinship_Check__c = check.id;
        update payment;

        //Loop__Document_Request__c loopDoc = new Loop__Document_Request__c(Loop__Object_Id__c = check.id);
        //insert loopDoc;
    }

    @isTest
    static void testGetCheckList_WithCheckId(){
        Kinship_Check__c check = [SELECT id FROM Kinship_Check__c LIMIT 1];

        Test.startTest();
        List<Object> lstResult = ShowCheckRunListController.getCheckList(check.id, ShowCheckRunListController.CHECK);
        Test.stopTest();
        System.assertEquals(1, lstResult.size(), 'The getCheckList method return does not equal 1');
    }

    @isTest
    static void testGetCheckList_WithPaymentId(){
        Payment__c payment = [SELECT id FROM Payment__c LIMIT 1];

        Test.startTest();
        List<Object> lstResult = ShowCheckRunListController.getCheckList(payment.id, ShowCheckRunListController.PAYMENT);
        Test.stopTest();
        System.assertEquals(1, lstResult.size(), 'The getCheckList method return does not equal 1');
    }

    @isTest
    static void testGetExportData(){
        Check_Run__c check = [SELECT id FROM Check_Run__c LIMIT 1];

        Test.startTest();
        List<Object> lstResult = ShowCheckRunListController.getExportData(check.id);
        Test.stopTest();
        System.assertNotEquals(null, lstResult, 'The getExportData method return does not equal 1');
    }
}