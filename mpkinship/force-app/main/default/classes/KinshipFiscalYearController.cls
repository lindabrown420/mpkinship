public class KinshipFiscalYearController {
    public final static STRING USER = 'User';
    public final static STRING CAMP = 'Campaign';
    public final static STRING FIPS = 'FIPS';
    private final static Integer MAX_RESULTS = 5;

    @AuraEnabled(cacheable=true)
    public static List<LookupSearchResult> kinshipSearchLookup(String searchTerm, List<String> selectedIds, String anOptionalParam) {
        // Prepare query paramters
        searchTerm += '*';

        // Prepare results
        List<LookupSearchResult> results = new List<LookupSearchResult>();
        List<List<SObject>> searchResults;

        if(String.isNotBlank(anOptionalParam)) {
            if(USER.equals(anOptionalParam)) {
                searchResults = [FIND :searchTerm IN ALL FIELDS RETURNING
                                User (Id, Name, Email WHERE id NOT IN :selectedIds)
                                LIMIT :MAX_RESULTS];
                String uItem = 'standard:user';
                User[] users = ((List<User>) searchResults[0]);
                for (User u : users) {
                    results.add(new LookupSearchResult(u.Id, 'User', uItem, u.Name, ''));
                }
            } else if(CAMP.equals(anOptionalParam)) {
                searchResults = [FIND :searchTerm IN ALL FIELDS RETURNING
                                Campaign (Id, Name WHERE id NOT IN :selectedIds AND Fiscal_Year_Type__c = 'Local')
                                LIMIT :MAX_RESULTS];
                String cItem = 'standard:campaign';
                Campaign[] objs = ((List<Campaign>) searchResults[0]);
                for (Campaign o : objs) {
                    results.add(new LookupSearchResult(o.Id, 'Campaign', cItem, o.Name, ''));
                }
            } else if(FIPS.equals(anOptionalParam)) {
                searchResults = [FIND :searchTerm IN ALL FIELDS RETURNING
                                FIPS__c (Id, Name WHERE id NOT IN :selectedIds)
                                LIMIT :MAX_RESULTS];
                String uItem = 'standard:campaign';
                FIPS__c[] objs = ((List<FIPS__c>) searchResults[0]);
                for (FIPS__c o : objs) {
                    results.add(new LookupSearchResult(o.Id, 'FIPS__c', uItem, o.Name, ''));
                }
            }
        }

        return results;
    }

    @AuraEnabled
    public static List<String> getPicklistvalues(String objectName, String field_apiname, String strNone){
        List<String> optionlist = new List<String>();       
        Map<String,Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
        Map<String, Schema.SObjectField> field_map = gd.get(objectName.toLowerCase()).getDescribe().fields.getMap();        
        List<Schema.PicklistEntry> picklistValues = field_map.get(field_apiname).getDescribe().getPickListValues();       
        if(String.isNotBlank(strNone)){
            optionlist.add(strNone);
        }       
        for (Schema.PicklistEntry pv : picklistValues) {
            optionlist.add(pv.getValue());
        }
        return optionlist;
    }

    @AuraEnabled
    public static FiscalYearData initData(){
        FiscalYearData result = new FiscalYearData();
        result.parentCamp = new CampData();
        result.parentCamp.fyType = 'Local';
        result.lstChild = new List<CampData>();
        CampData child = new CampData();
        child.isSendDueDate = false;
        result.lstChild.add(child);
        result.statusoptions = getPicklistvalues('Campaign', 'Status', '');
        result.typeoptions = getPicklistvalues('Campaign', 'Fiscal_Year_Type__c', '');
        result.lstFIPS = [SELECT Id, Name FROM FIPS__c ORDER BY Name];
        result.fips = new List<String>();
        result.lstSchedule = [SELECT Id, Name, Year__c, Month__c, LASER_Close__c, LASER_Re_Open__c,Reporter__c,Reporter__r.Name FROM LASER_Schedule__c];
        return result;
    }

    @AuraEnabled
    public static FiscalYearData saveFiscalYear(FiscalYearData data){
        // check parent FY
        List<Campaign> lstOverlap = [SELECT id, Name, StartDate, EndDate, FIPS__c, FIPS__r.Name
                                    FROM Campaign
                                    WHERE ((StartDate <= :data.parentCamp.startDate AND EndDate >= :data.parentCamp.startDate)
                                    OR (StartDate <= :data.parentCamp.endDate AND EndDate >= :data.parentCamp.endDate))
                                    AND Fiscal_Year_Type__c = :data.parentCamp.fyType
                                    AND FIPS__c IN :data.fips];
        if(lstOverlap.size() > 0) {
            throw new AuraHandledException('This Fiscal Year overlaps with another Fiscal Year: ' + lstOverlap[0].Name + '(' + lstOverlap[0].startDate + '-' + lstOverlap[0].endDate + ' - ' + lstOverlap[0].FIPS__r.Name +  '). Please input again.');
        }
        // look for parent FY
        Map<String, String> mapFIPSToParent = new Map<String, String>();
        if(data.parentCamp.fyType != 'Local') {
            List<Campaign> lstParentLocal = [SELECT id, Name, StartDate, EndDate, FIPS__c, FIPS__r.Name
                                                FROM Campaign
                                                WHERE CALENDAR_YEAR(EndDate) = :data.parentCamp.endDate.year()
                                                AND Fiscal_Year_Type__c = 'Local'];
            Map<Id, String> mapFIPS = new Map<Id, String>();
            for(FIPS__c fips : data.lstFIPS) {
                mapFIPS.put(fips.id, fips.name);
            }
            Set<String> setFIPS = new Set<String>();
            for(Campaign c : lstParentLocal) {
                setFIPS.add(c.FIPS__c);
                mapFIPSToParent.put(c.FIPS__c, c.Id);
            }
            for(String str : data.fips){
                if(!setFIPS.contains(str)) {
                    throw new AuraHandledException('There is a missing Local Fiscal Year for FIPS (' + mapFIPS.get(str) + '). Please create the Parent Local first.');
                }
            }
        }
        try {
            System.debug('---saveFiscalYear---' + data);
            Id fyRecordId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId();
            Id laserRecordId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('LASER Reporting Period').getRecordTypeId();
            Id ledersRecordId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('LEDRS Reporting Period').getRecordTypeId();

            List<Campaign> lstParent = new List<Campaign>();
            for(String str : data.fips) {
                Campaign camp = new Campaign(recordTypeId = fyRecordId, ParentId = data.parentCamp.parentId,
                                                Status = 'Open', Fiscal_Year_Type__c = data.parentCamp.fyType,
                                                StartDate = data.parentCamp.startDate, EndDate = data.parentCamp.endDate,
                                                Name = data.parentCamp.fyType, FIPS__c = str);
                if(data.parentCamp.fyType != 'Local') {
                    camp.ParentId = mapFIPSToParent.get(str);
                }
                lstParent.add(camp);
            }
            insert lstParent;

            if(data.parentCamp.fyType == 'Local') {
                return data;
            }
            Map<String, Campaign> mapParent = new Map<String, Campaign>();
            for(Campaign c : lstParent){
                mapParent.put(c.FIPS__c, c);
            }
            List<Campaign> lstChild = new List<Campaign>();
            System.debug('---saveFiscalYear---' + data.lstChild);
            Campaign temp;
            Campaign parent;
            Map<String, List<Campaign>> mapFIPSToLASER = new Map<String, List<Campaign>>();
            List<Campaign> lstLASER = new List<Campaign>();
            List<Campaign> lstTemp;
            for(CampData camp : data.lstChild) {
                for(String str : data.fips) {
                    parent = mapParent.get(str);
                    temp = new Campaign(ParentId = parent.id, Reporter__c = String.isBlank(camp.reporter)?null:camp.reporter, LASER_Deadline__c = camp.reportDueDate,
                                                StartDate = camp.startDate, EndDate = camp.endDate,Status = data.parentCamp.status,Auto_Post_Days__c = camp.autoPostDays,
                                                Name = parent.id, Send_Due_Date_Reminders_to_Reporter__c = camp.isSendDueDate, FIPS__c = str, Next_LASER_AP__c = null);
                    if(data.parentCamp.fyType == 'DSS') {
                        temp.recordTypeId = laserRecordId;
                        lstLASER.add(temp);
                        lstTemp = mapFIPSToLASER.get(str);
                        if(lstTemp == null) lstTemp = new List<Campaign>();
                        lstTemp.add(temp);
                        mapFIPSToLASER.put(str, lstTemp);
                    } else if(data.parentCamp.fyType == 'CSA') {
                        temp.recordTypeId = ledersRecordId;
                    }
                    lstChild.add(temp);
                }
            }
            if(lstChild.size() > 0) {
                insert lstChild;

                if(lstLASER.size() > 0) {
                    // auto insert Staff & Operations
                    List<Opportunity> lstSO = new List<Opportunity>();
                    for(Campaign c : lstLASER) {
                        Opportunity tSO = new Opportunity();
                        tSO.name = 'SO';
                        tSO.recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Staff & Operations').getRecordTypeId();
                        tSO.CampaignId = mapFIPSToParent.get(c.FIPS__c);//c.Parent.ParentId;
                        tSO.Accounting_Period__c = c.Id;
                        tSO.CloseDate = c.StartDate;
                        tSO.End_Date__c = c.EndDate;
                        tSO.Posting_Deadline__c = c.LASER_Deadline__c;
                        tSO.StageName = 'Open';
                        tSO.FIPS_Code__c = c.FIPS__c;
                        lstSO.add(tSO);
                    }
                    insert lstSO;

                    // update next AP
                    List<Campaign> lstUpdate = new List<Campaign>();
                    List<Campaign> lstPreLASER = new List<Campaign>();
                    for(String str : mapFIPSToLASER.keySet()) {
                        lstLASER = mapFIPSToLASER.get(str);
                        for(Integer i = 0 ; i < lstLASER.size() - 1 ; i++) {
                            lstLASER[i].Next_LASER_AP__c = lstLASER[i + 1].Id;
                            lstUpdate.add(lstLASER[i]);
                        }
                        lstPreLASER.add(lstLASER[0]);
                    }
                    update lstUpdate;

                    // update previous LASER
                    Date d = lstPreLASER[0].StartDate.addDays(-2);
                    System.debug('---saveFiscalYear---' + d);
                    List<Campaign> preCampaigns = [SELECT Id, FIPS__c, StartDate, EndDate
                                                    FROM Campaign
                                                    WHERE StartDate <= :d
                                                    AND EndDate >= :d
                                                    AND RecordTypeId = :laserRecordId];
                    lstUpdate = new List<Campaign>();
                    System.debug('---saveFiscalYear---' + preCampaigns.size());
                    for(Campaign c : lstPreLASER){
                        for(Campaign pre : preCampaigns){
                            if(c.FIPS__c == pre.FIPS__c){
                                pre.Next_LASER_AP__c = c.Id;
                                lstUpdate.add(pre);
                            }
                        }
                    }
                    if(lstUpdate.size() > 0) {
                        update lstUpdate;
                    }
                }
            }
            return data;  
        } catch (Exception e) {
            System.debug('---saveFiscalYear---' + e.getMessage());
            throw new AuraHandledException(e.getMessage());
        }
    }

    public class FiscalYearData{
        @AuraEnabled
        public CampData parentCamp{get;set;}

        @AuraEnabled
        public List<String> statusoptions{get;set;}

        @AuraEnabled
        public List<String> typeoptions{get;set;}

        @AuraEnabled
        public List<CampData> lstChild{get;set;}

        @AuraEnabled
        public List<FIPS__c> lstFIPS{get;set;}

        @AuraEnabled
        public List<String> fips{get;set;}

        @AuraEnabled
        public List<LASER_Schedule__c> lstSchedule{get;set;}
    }

    public class CampData{
        @AuraEnabled
        public String parentId{get;set;}

        @AuraEnabled
        public String reporter{get;set;}

        @AuraEnabled
        public String reporterName{get;set;}

        @AuraEnabled
        public Date reportDueDate{get;set;}

        @AuraEnabled
        public Date startDate{get;set;}

        @AuraEnabled
        public Date endDate{get;set;}

        @AuraEnabled
        public String status{get;set;}

        @AuraEnabled
        public String fyType{get;set;}

        @AuraEnabled
        public Boolean isSendDueDate{get;set;}

        @AuraEnabled
        public String fips{get;set;}

        @AuraEnabled
        public Integer autoPostDays{get;set;}
    }
}