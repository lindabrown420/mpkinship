public class KinshipBudgetEditController {
    @AuraEnabled
    public static BudgetData initData(String strbudget){
        List<Budgets__c> lstBudget = [SELECT Id, Fiscal_Year__c, Fiscal_Year__r.Name FROM Budgets__c WHERE id = :strbudget];
        BudgetData result = new BudgetData();
        if(lstBudget.size() == 1) {
            result.fy = new Campaign(Id = lstBudget[0].Fiscal_Year__c, Name = lstBudget[0].Fiscal_Year__r.Name);
            lstBudget = [SELECT Id, Name, Fiscal_Year__c, Fiscal_Year__r.Name,
                        Total_Expenditures__c, Locality_Account__c, Total_Receipts__c, Budget_Amount__c, Available_Balance__c, 
                        Budget_Balance_Remaining__c, Current_Budget__c, Encumbered__c ,Initial_Budget__c, Category__c, Category__r.Name, 
                        Laser_Budget_Code__c, Laser_Budget_Code__r.Name, Locality_Account_Code_Name__c, Locality_Account_Code_Name__r.Name
                        FROM Budgets__c
                        WHERE Fiscal_Year__c = :lstBudget[0].Fiscal_Year__c];
            result.lstBudget = lstBudget;
            result.totalIB = 0;
            result.totalBT = 0;
            result.totalAvaiB = 0;
            result.totalPayment = 0;
            result.totalReceipt = 0;
            for(Budgets__c budget : lstBudget){
                result.totalIB += getDecimal(budget.Initial_Budget__c);
                result.totalBT += getDecimal(budget.Budget_Amount__c);
                result.totalAvaiB += getDecimal(budget.Available_Balance__c);
                result.totalPayment += getDecimal(budget.Total_Expenditures__c);
                result.totalReceipt += getDecimal(budget.Total_Receipts__c);
            }
        }

        return result;
    }

    private static Decimal getDecimal(Decimal d){
        if(d == null) return 0;
        return d;
    }

    @AuraEnabled
    public static void saveInitialBudgets(BudgetData data){
        if(data != null && data.lstBudget != null && data.lstBudget.size() > 0) {
            update data.lstBudget;
        }
    }

    public class BudgetData{
        @AuraEnabled
        public List<Budgets__c> lstBudget{get;set;}

        @AuraEnabled
        public Campaign fy{get;set;}

        @AuraEnabled
        public Decimal totalIB{get;set;}

        @AuraEnabled
        public Decimal totalBT{get;set;}

        @AuraEnabled
        public Decimal totalAvaiB{get;set;}

        @AuraEnabled
        public Decimal totalPayment{get;set;}

        @AuraEnabled
        public Decimal totalReceipt{get;set;}
    }
}