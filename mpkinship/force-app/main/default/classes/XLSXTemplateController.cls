public class XLSXTemplateController {

    public String xmlHeader {get; set;}
    public List<XLSXGenerator.caseWrapper> textList {get; set;}
     public List<String> caseIds {get; set;}

    public XLSXTemplateController() {
        xmlHeader = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>';
        String caseIdStr = ApexPages.currentPage().getParameters().get('caseIds');
        String startDate = ApexPages.currentPage().getParameters().get('startDate');
        String endDate = ApexPages.currentPage().getParameters().get('endDate');
        caseIds = (List<String>) System.JSON.deserialize(caseIdStr, List<String>.class);
        textList = SPR_Report.getData(caseIds, startDate, endDate);
        system.debug('textList ' + textList);
        
    }

}