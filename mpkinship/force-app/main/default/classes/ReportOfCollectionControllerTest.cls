@isTest
public class ReportOfCollectionControllerTest {

    @isTest static void ReportCollectionTestMethod() {
        
        test.startTest();
        
        Contact con = new Contact();
        
        con.LastName        = 'Contact';
        //con.Relationship__c = 'Primary Case (self)';
        con.Email = 'contact@test.com';
        insert con;
        
        Case c = new Case();
        c.ContactId = con.id;
        
        
        insert c;
        ReportOfCollectionController contr = new ReportOfCollectionController();
        id OppRT = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Report of Collections').getRecordTypeId();
        Opportunity roc = new Opportunity();
        
        //roc.Begin_Date__c      = Date.today();
        //roc.Begin_Receipt__c   = 1000;
        roc.Beginning_Receipt__c = '1';
        roc.Ending_Receipt__c = '20000';
        roc.Name               = 'Test!23';
        roc.StageName          = 'Draft';
        roc.Date_Posted__c     = Date.today();
        roc.End_Date__c        = Date.today().addDays(20);
        roc.Amount             = 1000;
        roc.CloseDate          = Date.today().addDays(7);
         
        
        insert roc;
        
         id invoiceRT = Schema.SObjectType.Payment__c.getRecordTypeInfosByName().get('Receipt').getRecordTypeId();
        
        Payment__c receipt = new Payment__c();
        
        receipt.Payment_Date__c          = DATE.today().addDays(8);
        receipt.Payment_Amount__c    = 1000;
        receipt.Report_Number__c            = '111';
        receipt.Description__c              = 'test';
        //receipt.Pay_Method__c               = 'Cash';
        receipt.Payment_Method__c='Cash';
        receipt.Type_of_Receipt__c          = 'Refund';
        receipt.Opportunity__c       = roc.id;
        receipt.Receipt_Type__c             = 'Administrative';
        receipt.RecordTypeID                = invoiceRT;
        receipt.Payee__c					= 'payee';
        insert receipt;
        
        PageReference myVfPage = Page.Report_Collection;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('id',roc.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(roc);
        
        ReportOfCollectionController controller = new ReportOfCollectionController(sc);
        
        ReportOfCollectionController.PaymentWrapper pw = new ReportOfCollectionController.PaymentWrapper();
        pw.KinshipCaseName = '';
        pw.KinshipCaseCaseNo = pw.KinshipCaseName;
        pw.Payee = pw.KinshipCaseCaseNo;
        pw.LocalityAccountCodes = pw.Payee;
        pw.SpecialWelfareAccountName = pw.LocalityAccountCodes;
        pw.KinshipCaseVACMSNumber = pw.SpecialWelfareAccountName;
    }
}