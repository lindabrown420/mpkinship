@isTest
public class CampaignTriggerTest {
	@testSetup
    static void testRecords(){
        TriggerActivation__c ta = new TriggerActivation__c();
        ta.name='CampaignTrigger';
        ta.isActive__c=true;
        insert ta;
        Campaign parent = new Campaign();
       	parent.name = '2021';
       	parent.StartDate = Date.Today().addDays(-10);
       	parent.EndDate = Date.Today().addDays(10);
       	parent.Fiscal_Year_Type__c = 'DSS';
       	parent.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId();
       	insert parent;
        id recType = Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('LASER_Reporting_Period').getRecordTypeId();
        campaign camp = new campaign();
        camp.recordtypeid = recType;
        camp.Begin_Date__c=system.today();
        camp.EndDate=system.today().adddays(2);
        camp.parentId = parent.id;
        insert camp;
    } 
    
    public static testmethod void test1(){
        list<campaign> camplist=[select id,begin_date__c,enddate,recordtypeid from campaign];
        camplist[0].enddate = system.today().addmonths(24);
        update camplist[0];
    }
    
    public static testmethod void test2(){
        list<campaign> camplist=[select id,begin_date__c,enddate,recordtypeid from campaign];
        Campaign newCamp = new Campaign();
        newCamp.enddate = system.today().addmonths(24);
        newCamp.recordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Events').getRecordTypeId();
        insert newCamp;
        CampaignController.getRT_Name(newCamp.recordTypeId);
    }
    
    public static testmethod void test3(){
        list<campaign> camplist=[select id,begin_date__c,enddate,recordtypeid from campaign];
        Campaign newCamp = new Campaign();
        newCamp.enddate = system.today().addmonths(24);
        newCamp.recordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Tax_Reporting_Period').getRecordTypeId();
        insert newCamp;
    }
    
    
}