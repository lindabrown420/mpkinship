@isTest
private class PaymentTriggerHandlerTest {
    @TestSetup
    static void initData(){
        TriggerActivation__c ta = new TriggerActivation__c(Name = 'PaymentTrigger', isActive__c = true);
        insert ta;
        TriggerActivation__c  ta1 = new TriggerActivation__c();
        ta1.name='PurchaseOrder Trigger';
        ta1.isActive__c=true;
        insert ta1;
        id recType = Schema.SObjectType.contact.getRecordTypeInfosByDeveloperName().get('Contacts').getRecordTypeId();
        contact con = new contact();
        con.recordtypeid=recType;
        con.LastName='contactrec';
        con.FirstName='test';
        con.Email='test@contact.com';        
        insert con;
        
        Kinship_Case__c caserec = new Kinship_Case__c();
        caserec.Primary_Contact__c=con.id;
        insert caserec;
        
        //id vendorrecType = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Vendor').getRecordTypeId();
        account accountrec = new account();
        //accountrec.recordtypeid=vendorrecType;
        accountrec.Name='test vendor';
        accountrec.Tax_ID__c='222-22-3333';
        accountrec.Entity_Type__c='Individual';
        insert accountrec;
        
        Locality_Account__c locality = new Locality_Account__c(Name = '12345');
        insert locality;
        
        Category__c category = new Category__c();
        category.Name='222';
        category.Locality_Account__c = locality.id;
        category.Category_Type__c='Adoption';
        category.RecordTypeId = Schema.SObjectType.Category__c.getRecordTypeInfosByName().get('LASER Category').getRecordTypeId();
        insert category;  
        
        FIPS__c fp = new FIPS__c();
        fp.name='test fips';
        insert fp;
        id recTypeid = Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Fiscal_Year').getRecordTypeId();
        campaign cap = new campaign();
        cap.FIPS__c=fp.id;
        cap.RecordTypeId=recTypeid;
        cap.Fiscal_Year_Type__c='Local';
        cap.name='test';
        cap.Begin_Date__c=system.today().addmonths(-1);
        cap.EndDate=system.today().addyears(1);
        insert cap;
        
    }
    
   @isTest
   static void testInsertPayment(){
       Campaign fy = new Campaign();
       fy.name = '2021';
       fy.StartDate = Date.Today().addDays(-10);
       fy.EndDate = Date.Today().addDays(10);
       fy.Fiscal_Year_Type__c = 'Local';
       fy.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId();
       insert fy;
       
       Campaign parent = new Campaign();
       parent.name = '2021';
       parent.StartDate = Date.Today().addDays(-10);
       parent.EndDate = Date.Today().addDays(10);
       parent.Fiscal_Year_Type__c = 'DSS';
       parent.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId();
       insert parent;
       id recType = Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('LASER_Reporting_Period').getRecordTypeId();
       Campaign c = new Campaign();
       c.name = '2021';
       c.startdate=system.today().adddays(-3);
       c.EndDate=system.today().adddays(20);
       c.RecordTypeId=recType;
       c.ParentId = parent.id;
       insert c;
       list<TriggerActivation__c> talist=[select id,name,isActive__C from TriggerActivation__c where isActive__c=:true 
                                           and name=:'PaymentTrigger'];
       list<TriggerActivation__c> talist1=[select id,name,isActive__C from TriggerActivation__c where isActive__c=:true 
                                           and name=:'PurchaseOrder Trigger'];
       list<Kinship_Case__c> cases=[select id from Kinship_Case__c];
       list<account> accounts=[select id,recordtypeid from account where name='test vendor'];        
       list<Category__c> categories=[select id from Category__c];
       //insert a new product
       Product2 p = new product2(name='x',isactive=true, Unit_Measure__c='1');
       insert p;
       
       Product2 p2 = new product2(name='y',isactive=true, Unit_Measure__c='1');
       insert p2;
       
       //define the standart price for the product
       id stdPb = Test.getStandardPricebookId();
       insert new PricebookEntry(pricebook2id = stdPb, product2id = p.id,unitprice=1.0, isActive=true);
       
       Pricebook2 pb = new pricebook2(name='test',Vendor__c=accounts[0].id,IsActive=true,
                                      Effective_Date__c=system.today().addmonths(-2),Accounting_Period__c = fy.id);
       insert pb;
       
       PricebookEntry pbe = new PricebookEntry(pricebook2id=pb.id, product2id=p.id,unitprice=1.0, isActive=true);
       insert pbe; 
       id recTypeid = Schema.SObjectType.opportunity.getRecordTypeInfosByDeveloperName().get('Purchase_Order').getRecordTypeId();
       opportunity opprecord = new opportunity();
       opprecord.CloseDate=date.today();
       opprecord.StageName='Draft';
       opprecord.Name='test opportunity';
       opprecord.Kinship_Case__c=cases[0].id;
       opprecord.Amount=20000;
       opprecord.RecordTypeId=recTypeid;
       opprecord.AccountId=accounts[0].id;
       opprecord.Number_of_Months_Units__c=3;
       opprecord.Pricebook2Id =pb.id;        
       insert opprecord;
       //NOW CREATING LINE ITEMS
       opportunitylineitem opplineitem = new opportunitylineitem();
       opplineitem.Unit_Measure__c='2';
       opplineitem.Product2Id=p.id;
       opplineitem.PricebookEntryId=pbe.id;
       opplineitem.Quantity=2;
       opplineitem.Category__c=categories[0].id;
       opplineitem.OpportunityId=opprecord.id;
       opplineitem.TotalPrice = 2000;
       insert opplineitem;
       //Create an approval request for the po
       Test.startTest();
       Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitrequest();
       app.setObjectId(opprecord.Id);    
       // Submit the approval request for the po    
       Approval.ProcessResult result = Approval.process(app);    
       // Verify that the results are as expected    
       System.assert(result.isSuccess());
       
       if(result.isSuccess()){
           opprecord.stagename='Approved';           
           update opprecord;
           
           list<Payment__c> payments=[select id,Written_Off__c,Paid__c,Payment_Date__c  from Payment__c
                                                where Opportunity__c=:opprecord.Id];
           system.debug('**VAl of payments'+payments.size());
           payments[0].Paid__c=true;
           payments[0].Payment_Date__c = Date.today();
           payments[0].Invoices_Status__c = 'Posted';
           update payments[0];
           List<Transaction_Journal__c> lstTJ = [SELECT id FROM Transaction_Journal__c];
           System.assertEquals(1, lstTJ.size(), 'The Transaction_Journal__c insert does not equal 1');
       }
   }  
    
    @isTest
   static void testDeletePayment(){
       Campaign parent = new Campaign();
       parent.name = '2021';
       parent.StartDate = Date.Today().addDays(-10);
       parent.EndDate = Date.Today().addDays(10);
       parent.Fiscal_Year_Type__c = 'DSS';
       parent.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId();
       insert parent;
       id recType = Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('LASER_Reporting_Period').getRecordTypeId();
       Campaign c = new Campaign();
       c.name = '2021';
       c.startdate=system.today().adddays(-3);
       c.EndDate=system.today().adddays(20);
       c.RecordTypeId=recType;
       c.ParentId = parent.id;
       insert c;
       list<TriggerActivation__c> talist=[select id,name,isActive__C from TriggerActivation__c where isActive__c=:true 
                                           and name=:'PaymentTrigger'];
       list<TriggerActivation__c> talist1=[select id,name,isActive__C from TriggerActivation__c where isActive__c=:true 
                                           and name=:'PurchaseOrder Trigger'];
       list<Kinship_Case__c> cases=[select id from Kinship_Case__c];
       list<account> accounts=[select id,recordtypeid from account where name='test vendor'];        
       list<Category__c> categories=[select id from Category__c];
       //insert a new product
       Product2 p = new product2(name='x',isactive=true, Unit_Measure__c  = '1');
       insert p;
       
       Product2 p2 = new product2(name='y',isactive=true, Unit_Measure__c  = '1');
       insert p2;
       
       //define the standart price for the product
       id stdPb = Test.getStandardPricebookId();
       insert new PricebookEntry(pricebook2id = stdPb, product2id = p.id,unitprice=1.0, isActive=true);
        list<fips__C> fip=[select id,name from fips__C];
        list<campaign> camp=[select id,fips__c,Begin_Date__c,enddate,Fiscal_Year_Type__c from campaign];
       Pricebook2 pb = new pricebook2(name='test',Vendor__c=accounts[0].id,IsActive=true,
                                      accounting_period__C=camp[0].id);
       insert pb;
       
       PricebookEntry pbe = new PricebookEntry(pricebook2id=pb.id, product2id=p.id,unitprice=1.0, isActive=true);
       insert pbe; 
         id recTypeid = Schema.SObjectType.opportunity.getRecordTypeInfosByDeveloperName().get('Purchase_Order').getRecordTypeId();
       opportunity opprecord = new opportunity();
       opprecord.CloseDate=date.today();
       opprecord.StageName='Draft';
       opprecord.Name='test opportunity';
       opprecord.Kinship_Case__c=cases[0].id;
       opprecord.CampaignId=camp[0].id;
       opprecord.RecordTypeId=recTypeid;
       opprecord.Amount=20000;
       //opprecord.Category__c =categories[0].id;
       opprecord.AccountId=accounts[0].id;
       opprecord.Number_of_Months_Units__c=3;
       opprecord.Pricebook2Id =pb.id;        
       insert opprecord;
       //NOW CREATING LINE ITEMS
       opportunitylineitem opplineitem = new opportunitylineitem();
       opplineitem.Unit_Measure__c='2';
       opplineitem.Product2Id=p.id;
       opplineitem.PricebookEntryId=pbe.id;
       opplineitem.Quantity=2;
       opplineitem.Category__c=categories[0].id;
       opplineitem.OpportunityId=opprecord.id;
       opplineitem.TotalPrice = 2000;
       insert opplineitem;
       //Create an approval request for the po
       Test.startTest();
       Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitrequest();
       app.setObjectId(opprecord.Id);    
       // Submit the approval request for the po    
       Approval.ProcessResult result = Approval.process(app);    
       // Verify that the results are as expected    
       System.assert(result.isSuccess());
       
       if(result.isSuccess()){
           opprecord.stagename='Approved';           
           update opprecord;
           
           list<Payment__c> payments=[select id,Written_Off__c,Paid__c,Payment_Date__c, Kinship_Check__c from Payment__c
                                                where Opportunity__c=:opprecord.Id];
           system.debug('**VAl of payments'+payments.size());
           payments[0].Paid__c=true;
           payments[0].Payment_Date__c = Date.today();
           payments[0].Invoices_Status__c = 'Posted';
           update payments[0];
           
           Kinship_Check__c check = new Kinship_Check__c(Issue_date__c = payments[0].Payment_Date__c, Amount__c = 100);
           insert check;
           payments[0].Payment_Amount__c=99;
           payments[0].Kinship_Check__c = check.Id;
           TransactionJournalService.byPassCheckPosted = true;
           update payments[0];
           TransactionJournalService.byPassCheckPosted = false;
           
           List<Transaction_Journal__c> lstTJ = [SELECT id FROM Transaction_Journal__c];
           System.assertEquals(1, lstTJ.size(), 'The Transaction_Journal__c insert does not equal 1');
           lstTJ[0].Status__c = 'Posted';
           update lstTJ[0];
           try{
               delete payments[0];
           } catch(Exception e){}
           
       }        
   }
}