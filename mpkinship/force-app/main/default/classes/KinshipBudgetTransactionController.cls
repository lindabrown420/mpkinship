public class KinshipBudgetTransactionController {
    private final static Integer MAX_RESULTS = 5;
    public final static String CAMP = 'Campaign';
    public final static String CODE = 'Code';

    @AuraEnabled(cacheable=true)
    public static List<LookupSearchResult> kinshipSearchLookup(String searchTerm, List<String> selectedIds, String anOptionalParam) {
        // Prepare query paramters
        searchTerm += '*';

        // Prepare results
        List<LookupSearchResult> results = new List<LookupSearchResult>();
        List<List<SObject>> searchResults;

        if(String.isNotBlank(anOptionalParam)) {
            if(CAMP.equals(anOptionalParam)) {
                searchResults = [FIND :searchTerm IN ALL FIELDS RETURNING
                                Campaign (Id, Name WHERE id NOT IN :selectedIds AND RecordType.Name = 'Fiscal Year')
                                LIMIT :MAX_RESULTS];
                String cItem = 'standard:campaign';
                Campaign[] objs = ((List<Campaign>) searchResults[0]);
                for (Campaign o : objs) {
                    results.add(new LookupSearchResult(o.Id, 'Campaign', cItem, o.Name, ''));
                }
            } else if(CODE.equals(anOptionalParam)) {
                searchResults = [FIND :searchTerm IN ALL FIELDS RETURNING
                                LASER_Budget_Line__c (Id, Name,Budget_Line_Description__c WHERE id NOT IN :selectedIds AND Inactive__c = false),
                                Locality_Account__c (Id, Name,Account_Title__c WHERE id NOT IN :selectedIds AND Inactive__c = false),
                                Category__c (Id, Name,Category_Account_Description__c WHERE id NOT IN :selectedIds AND Category_Type__c = 'CSA' AND Inactive__c = false)
                                LIMIT :MAX_RESULTS];
                String cItem = 'standard:person_account';
                LASER_Budget_Line__c[] objs1 = ((List<LASER_Budget_Line__c>) searchResults[0]);
                for (LASER_Budget_Line__c o : objs1) {
                    results.add(new LookupSearchResult(o.Id, 'LASER_Budget_Line__c', cItem, o.Name, o.Budget_Line_Description__c));
                }

                cItem = 'standard:poll';
                Locality_Account__c[] objs2 = ((List<Locality_Account__c>) searchResults[1]);
                for (Locality_Account__c o : objs2) {
                    results.add(new LookupSearchResult(o.Id, 'Locality_Account__c', cItem, o.Name, o.Account_Title__c));
                }

                cItem = 'standard:planogram';
                Category__c[] objs3 = ((List<Category__c>) searchResults[2]);
                for (Category__c o : objs3) {
                    results.add(new LookupSearchResult(o.Id, 'Category__c', cItem, o.Name, o.Category_Account_Description__c));
                }
            }
        }

        return results;
    }

    @AuraEnabled
    public static List<BTData> initData(){
        List<BTData> result = new List<BTData>();
        List<Budgets__c> lstBudget = [SELECT id, Name, Budget_Amount__c, Current_Budget__c, Available_Balance__c, 
                                        Fiscal_Year__c, Fiscal_Year__r.Name,Category__c, Category__r.Name, Laser_Budget_Code__c, 
                                        Laser_Budget_Code__r.Name, Locality_Account_Code_Name__c, 
                                        Locality_Account_Code_Name__r.Name
                                        FROM Budgets__c ORDER BY Fiscal_Year__c];
        Map<Id, List<Budgets__c>> mapFY2Budgets = new Map<Id, List<Budgets__c>>();
        for(Budgets__c b : lstBudget) {
            List<Budgets__c> temp = mapFY2Budgets.get(b.Fiscal_Year__c);
            if(temp == null) {
                temp = new List<Budgets__c>();
            }
            temp.add(b);
            mapFY2Budgets.put(b.Fiscal_Year__c, temp);
        }
        for(Id i : mapFY2Budgets.keySet()) {
            BTData data = new BTData();
            data.isShowDelete = false;
            data.isShow = false;
            data.total = 0;
            data.lstChild = new List<BudgetTransactionData>();
            data.lstBudget = mapFY2Budgets.get(i);
            data.accountPeriod = new Campaign(Id = data.lstBudget[0].Fiscal_Year__c, Name = data.lstBudget[0].Fiscal_Year__r.Name);
            result.add(data);
        }
        return result;
    }

    @AuraEnabled
    public static List<Budgets__c> getBudgetsData(String fiscalYear){
        if (String.isBlank(fiscalYear)) return new List<Budgets__c>();
        return [SELECT id, Name, Budget_Amount__c, Current_Budget__c, Available_Balance__c, Fiscal_Year__c,
                Category__c, Category__r.Name, Laser_Budget_Code__c, Laser_Budget_Code__r.Name, Locality_Account_Code_Name__c, Locality_Account_Code_Name__r.Name
                 FROM Budgets__c WHERE Fiscal_Year__c = :fiscalYear];
    }

    @AuraEnabled
    public static void saveBudgetTransactions(List<BTData> data){
        try {
            List<Budget_Transactions__c> lstInsert = new List<Budget_Transactions__c>();
            for(BTData rowData : data) {
                for(BudgetTransactionData btdata : rowData.lstChild) {
                    Budget_Transactions__c temp = new Budget_Transactions__c(Budgets__c = btdata.budgetFrom, Transaction_Type__c = btdata.btType, Transaction_Date__c = Date.today());
                    
                    if(btdata.btType == 'Supplemental Allocation'){
                        temp.Amount__c = btdata.amount;
                        lstInsert.add(temp);
                    } else if(btdata.btType == 'Budget Reduction') {
                        temp.Amount__c = -btdata.amount;
                        lstInsert.add(temp);
                    } else {
                        if(btdata.totalTransferAmount > 0){
                            temp.Amount__c = -btdata.totalTransferAmount;
                            lstInsert.add(temp);
                        }
                        lstInsert.add(new Budget_Transactions__c(Budgets__c = btdata.budgetTo, Transaction_Type__c = btdata.btType, Amount__c = btdata.amount, Transaction_Date__c = Date.today()));
                    }
                    
                }
            }
            if(lstInsert.size() > 0){
                insert lstInsert;
            }
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    public class BTData{
        @AuraEnabled
        public Campaign accountPeriod{get;set;}

        @AuraEnabled
        public List<BudgetTransactionData> lstChild{get;set;}

        @AuraEnabled
        public List<Budgets__c> lstBudget{get;set;}

        @AuraEnabled
        public Boolean isShowDelete{get;set;}

        @AuraEnabled
        public Decimal total{get;set;}

        @AuraEnabled
        public Boolean isShow{get;set;}
    }

    public class BudgetTransactionData{
        @AuraEnabled
        public String code{get;set;}

        @AuraEnabled
        public String description{get;set;} 

        @AuraEnabled
        public String budgetFrom{get;set;}

        @AuraEnabled
        public String code2{get;set;}

        @AuraEnabled
        public String description2{get;set;} 

        @AuraEnabled
        public String budgetTo{get;set;}

        @AuraEnabled
        public String btType{get;set;}

        @AuraEnabled
        public Decimal avaiAmount{get;set;} 

        @AuraEnabled
        public Decimal amount{get;set;}

        @AuraEnabled
        public Decimal totalTransferAmount{get;set;}
    }
}