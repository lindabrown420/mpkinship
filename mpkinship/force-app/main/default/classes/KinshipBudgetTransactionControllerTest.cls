@isTest
private class KinshipBudgetTransactionControllerTest {
	@testSetup
    static void initData(){
        Id profileId1 = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User u = new User(FirstName='Test',LastName= 'User',profileid=profileId1 , email='abc123@test.com',
                         username='abc123@test.com.prod',Alias='zbd',LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', EmailEncodingKey='UTF-8',
                          TimeZoneSidKey='America/Los_Angeles');
        
        Account acc = new Account(Name = 'test acc', Tax_ID__c = '123-56-7901', Entity_Type__c = 'Individual');
        insert acc;

        Locality_Account__c locality = new Locality_Account__c(Name = '12345');
        insert locality;

        Category__c cate = new Category__c(Name = 'test cate', Category_Type__c = 'CSA', Locality_Account__c = locality.id , RecordTypeId = Schema.SObjectType.Category__c.getRecordTypeInfosByName().get('LEDRS Category').getRecordTypeId());
        insert cate;
        
        LASER_Budget_Line__c budgetLine = new LASER_Budget_Line__c(name = '12345', Budget_Line_Description__c='Budget_Line_Description');
        insert budgetLine;
        Locality_Account__c localAcc = new Locality_Account__c(name = '234567', Account_Title__c='Account_Title');
        insert localAcc;
        
        FIPS__c fips = new FIPS__c(name = 'FIPS Test', FIPS_Code__c='1');
        insert fips;
        
        Kinship_Case__c kcase = new Kinship_Case__c(Primary_Social_Worker__c = u.id);

        Opportunity opp = new Opportunity(Name = 'test Opp', AccountId = acc.id, Category__c = cate.id, CloseDate = Date.today().addDays(10),
                                          StageName='Open', Kinship_Case__c = kcase.id);
        insert opp;

        Payment__c payment = new Payment__c(Opportunity__c = opp.id, 
                                                                Scheduled_Date__c = Date.today(), 
                                                                Status__c = 'Submitted For Approval',
                                                                Invoice_Stage__c = 'Ready to Pay',
                                                               	Written_Off__c = false);
        insert payment;
    }

    @isTest
    static void testKinshipSearchLookup_WithCamp(){
        Campaign c = new Campaign(name = 'CSA - 2020', Fiscal_Year_Type__c = 'Local', RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId());
        insert c;
        List<Id> fixedSearchResults= new List<Id>();
        fixedSearchResults.add(c.id);
        Test.setFixedSearchResults(fixedSearchResults);
        Test.startTest();
        List<Object> lstResult = KinshipBudgetTransactionController.kinshipSearchLookup('test', new List<String>(), 'Campaign');
        Test.stopTest();
        System.assertEquals(1, lstResult.size(), 'The kinshipSearchLookup method return does not equal 1');
    }
    
    @isTest
    static void testKinshipSearchLookup_WithCode(){
        Category__c cate = [SELECT id FROM Category__c LIMIT 1];
        LASER_Budget_Line__c bline = [SELECT id FROM LASER_Budget_Line__c LIMIT 1];
        Locality_Account__c accCode = [SELECT id FROM Locality_Account__c LIMIT 1];
        List<Id> fixedSearchResults= new List<Id>();
        fixedSearchResults.add(cate.id);
        fixedSearchResults.add(bline.id);
        fixedSearchResults.add(accCode.id);
        Test.setFixedSearchResults(fixedSearchResults);
        Test.startTest();
        List<Object> lstResult = KinshipBudgetTransactionController.kinshipSearchLookup('234', new List<String>(), 'Code');
        Test.stopTest();
        System.assertEquals(3, lstResult.size(), 'The kinshipSearchLookup method return does not equal 3');
    }
    
    @isTest
    private static void testInitData(){
        Test.startTest();
        Object obj = KinshipBudgetTransactionController.initData();
        Test.stopTest();

        System.assertNotEquals(null, obj, 'The method is return null value');
    }
    
    @isTest
    private static void testSaveBudgetTransactions(){
        Campaign c = new Campaign(name = 'CSA - 2020', Fiscal_Year_Type__c = 'Local', RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId());
        insert c;
        Budgets__c bfrom = new Budgets__c(Fiscal_Year__c = c.id);
        insert bfrom;
        Budgets__c bto = new Budgets__c(Fiscal_Year__c = c.id);
        insert bto;
        List<KinshipBudgetTransactionController.BTData> lstData = KinshipBudgetTransactionController.initData();
        lstData[0].accountPeriod = new Campaign(id = c.id);
        lstData[0].lstChild = new List<KinshipBudgetTransactionController.BudgetTransactionData>();
        KinshipBudgetTransactionController.BudgetTransactionData child = new KinshipBudgetTransactionController.BudgetTransactionData();
        child.btType = 'Supplemental Allocation';
        child.budgetFrom = bfrom.id;
        child.amount = 100;
        lstData[0].lstChild.add(child);
        
        child = new KinshipBudgetTransactionController.BudgetTransactionData();
        child.btType = 'Budget Reduction';
        child.budgetFrom = bfrom.id;
        child.amount = 100;
        lstData[0].lstChild.add(child);
        
        child = new KinshipBudgetTransactionController.BudgetTransactionData();
        child.btType = 'Budget Transfer';
        child.budgetFrom = bfrom.id;
        child.budgetTo = bto.id;
        child.amount = 100;
        child.totalTransferAmount = 100;
        lstData[0].lstChild.add(child);
        
        Test.startTest();
        KinshipBudgetTransactionController.saveBudgetTransactions(lstData);
        Test.stopTest();

        System.assertNotEquals(0, [SELECT id FROM Budget_Transactions__c].size(), 'The method is return null value');
    }

    @isTest
    private static void testGetBudgetsData_WithData(){
        Campaign c = new Campaign(name = 'CSA - 2020', Fiscal_Year_Type__c = 'Local', RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId());
        insert c;

        Budgets__c budget = new Budgets__c(Fiscal_Year__c = c.id);

        Test.startTest();
        List<Budgets__c> lstBudgets = KinshipBudgetTransactionController.getBudgetsData(c.id);
        Test.stopTest();

        System.assertNotEquals(1, lstBudgets.size(), 'The method is return 0 value');
    }

    @isTest
    private static void testGetBudgetsData_WithoutData(){
        Campaign c = new Campaign(name = 'CSA - 2020', Fiscal_Year_Type__c = 'Local', RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId());
        insert c;

        Test.startTest();
        List<Budgets__c> lstBudgets = KinshipBudgetTransactionController.getBudgetsData(c.id);
        Test.stopTest();

        System.assertEquals(0, lstBudgets.size(), 'The method is return a value');
    }
}