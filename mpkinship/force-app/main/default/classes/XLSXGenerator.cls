global class XLSXGenerator {
    @AuraEnabled(continuation = true) 
    public static String generate(List<String> caseId, String startDate, String endDate) {
        // Build XLSX File Content
        PageReference xlsxTemplate = page.XLSXTemplate;
        system.debug('xlsxTemplate ' + xlsxTemplate);
        xlsxTemplate.getParameters().put('caseIds',JSON.serialize(caseId));
        xlsxTemplate.getParameters().put('startDate',startDate);
        xlsxTemplate.getParameters().put('endDate',endDate);
        Blob xlsxContent;
        if (Test.isRunningtest()) {
            xlsxContent = Blob.valueOf('Sample');
        } else {
            xlsxContent = xlsxTemplate.getContent();
        }
        system.debug('xlsxContent ===== ' + xlsxContent);
        
        // Build XLSX File Frame
        StaticResource xlsxTemplateFrame = [SELECT Body FROM StaticResource WHERE Name = 'SPRV3' LIMIT 1];
        Kinship_Case__c kCase = [SELECT id, OASIS_Client_ID__c, Primary_Contact__r.LastName
									FROM Kinship_Case__c WHERE Id IN :caseId LIMIT 1];
        Zippex xlsx = new Zippex(xlsxTemplateFrame.Body);
        // Add the Content to the Frame to complete the File
        
        xlsx.addFile('xl/worksheets/sheet2.xml', xlsxContent, null);
        // Save XLSX File 
        ContentVersion cv = new ContentVersion();
        String title = kCase.Primary_Contact__r.LastName.toUpperCase() + ' ' + kcase.OASIS_Client_ID__c + ' SPR'; // update name here
        cv.Title = title;
        cv.PathOnClient = title + ' - ' + DateTime.now() + '.xlsx';
        cv.VersionData = xlsx.getZipArchive();
        insert cv;
        Id contentDocumentid = [SELECT Id FROM ContentDocument WHERE LatestPublishedVersionId = :cv.Id].Id;
        ContentDocumentLink cdl = new ContentDocumentLink();
        List<ContentDocumentLink> cdlList = new List<ContentDocumentLink>();
        for(Id ids:caseId){
            cdl = new ContentDocumentLink();
            cdl.LinkedEntityId = ids;
            cdl.ContentDocumentId = contentDocumentid;
            cdl.ShareType = 'V';
            cdlList.add(cdl);
        }
        try {
            if(cdlList.size()>0){
                insert cdlList;   
            }
            
        } catch(DMLException e) {
            System.debug(e);
        }
        return URL.getOrgDomainUrl().toExternalForm() + '/sfc/servlet.shepherd/document/download/' + contentDocumentid;
    }
    global class caseWrapper{
        global string ssn {get; set;}
        public string no {get; set;}
        public string OASISClientID {get; set;}
        public string CaseNumber {get; set;}
        public string LASERCostCode{get; set;}
        public string CategoryName1 {get; set;}
        public string ServiceBeginDate {get; set;}
        public string ServiceEndDate {get; set;}
        public string PONumber {get; set;}
        public string FederalReportingPeriod {get; set;}
        public string npe01PaymentDate {get; set;}
        public string CheckNumber {get; set;}
        public string npe01PaymentAmount {get; set;}
        public string Categorylookup {get; set;}
        public string PaymentValidity {get; set;}
        public string Description {get; set;}
        public string VendorName {get; set;}
        public string SPTLabel {get; set;}
        public string AmountinError {get; set;}
        public string AmountofLASERAdjustmentValidated{get; set;}
        public string SPTNameLabel{get; set;}
        public string categoryRateWrap{get; set;}
        public string SPTCatRate{get; set;}
        public string TitleIVEPaymentAmount{get; set;}
        public string CSAPaymentAmount{get; set;}
        public string OtherPaymentAmount{get; set;}
        public string OtherPaymentSource{get; set;}
        public string NoteSUO{get; set;}
        public caseWrapper(){}
}

}