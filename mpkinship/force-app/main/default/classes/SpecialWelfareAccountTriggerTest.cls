@IsTest
public class SpecialWelfareAccountTriggerTest {
    public static testmethod void SpecialWelfareAccountmethod(){
        contact con = new contact();
        con.LastName='contactrec';
        con.FirstName='test';
        con.Email='test@contact.com';        
        insert con;   
        
        Kinship_Case__c caserec = new Kinship_Case__c();
        caserec.Primary_Contact__c=con.id;
        insert caserec;
        
        con = new contact();
        con.LastName='contactrec 2';
        con.FirstName='test 2';
        con.Email='test_2@contact.com';        
        insert con; 
        
        Kinship_Case__c caserec1 = new Kinship_Case__c();
        caserec1.Primary_Contact__c=con.id;
        insert caserec1;
        
        Special_Welfare_Account__c swa = new Special_Welfare_Account__c();
        swa.Fund_Name__c = 'test';     
        swa.Case_KNP__c = caserec.Id;
        swa.Initial_Balance_For_Child_Support_Paymen__c=1000;
        swa.Initial_Balance_For_SSA_SSI_Payments__c=1000;
        insert swa;
        
        TriggerActivation__c  ta = new TriggerActivation__c();
        ta.name='PurchaseOrder Trigger';
        ta.isActive__c=true;
        insert ta;
        TriggerActivation__c  ta2 = new TriggerActivation__c();
        ta2.name='Payment Trigger';
        ta2.isActive__c=true;
        insert ta2;
        
         TriggerActivation__c  ta1 = new TriggerActivation__c();
        ta1.name='UpdateCSACoordinatorSign';
        ta1.isActive__c=true;
        insert ta1;  
        
        account accountrec = new account();
        accountrec.Name='test vendor';
        accountrec.Tax_ID__c='333-33-3333';
        accountrec.Entity_Type__c='Individual';
        insert accountrec;
        
        id catrecType = Schema.SObjectType.Category__c.getRecordTypeInfosByName().get('LASER Category').getRecordTypeId();
        
         Locality_Account__c la = new Locality_Account__c();
        la.name='33';
        insert la;
         
        //string catType = SObjectType.Category__c.Fields.Category_Type__c.PicklistValues[0].getValue(); 
         id catrecTypeid = Schema.SObjectType.category__C.getRecordTypeInfosByDeveloperName().get('LOCAL_Category').getRecordTypeId();
        Category__c category = new Category__c();
        category.Name='222';
        category.Category_Type__c= 'Special Welfare';
        category.RecordTypeId = catrecTypeid;
        //category.Special_Welfare_Reimbursement_Vendor__c=accountrec.id;
        category.Locality_Account__c=la.id;
        insert category;
        
         fips__c fip= new fips__c();
        fip.name = 'Alleghany';
        insert fip;
         
        Campaign parent = new Campaign();
        parent.name = '2021';
        parent.StartDate = Date.Today().addDays(-10);
        parent.EndDate = Date.Today().addDays(10);
        parent.Fiscal_Year_Type__c = 'DSS';
        parent.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId();
        insert parent;
        id recType = Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('LASER_Reporting_Period').getRecordTypeId();
        campaign camp = new campaign();
        camp.name = 'May';
        camp.recordtypeid = recType;
        camp.Begin_Date__c=system.today();
        camp.EndDate=system.today().adddays(2);
        camp.parentId = parent.id;
        camp.FIPS__c =  fip.id;
        insert camp;
        
       
        Cost_Center__c cc = new Cost_Center__c();
        cc.Name='test';
        insert cc;
        Chart_of_Account__c ca = new Chart_of_Account__c();
        ca.Locality_Account__c=la.id;
        ca.FIPS__c=fip.id;
        ca.Cost_Center__c=cc.id;
        insert ca;
         
        id opprecType = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Staff_Operations').getRecordTypeId();
        opportunity opprecord = new opportunity();
        opprecord.CloseDate=date.today();
        opprecord.End_Date__c=date.today() + 15;
        opprecord.StageName='Open';
        opprecord.Name='test opportunity';        
        opprecord.Amount=20000;
        opprecord.AccountId=accountrec.id;
        opprecord.Accounting_Period__c = camp.id;
        opprecord.RecordTypeId=opprecType;
        opprecord.Category__c = category.Id;
        opprecord.Service_End_Date__c = date.today() + 5;
        insert opprecord;
        
       id ROCType = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Report_of_Collection').getRecordTypeId();
        opportunity ROCrecord = new opportunity();
        ROCrecord.CloseDate=date.today() + 2;
        ROCrecord.End_Date__c=date.today() + 20;
        ROCrecord.StageName='Open';
        ROCrecord.Name='test opportunity';        
        ROCrecord.Amount=20000;
        ROCrecord.AccountId=accountrec.id;
        ROCrecord.Accounting_Period__c = camp.id;
        ROCrecord.RecordTypeId=ROCType;        
        ROCrecord.Category__c = category.Id;
        opprecord.Service_End_Date__c = date.today() + 1;
        insert ROCrecord;
        
        id payrecType = Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Vendor_Invoice').getRecordTypeId();
        Payment__c payment = new Payment__c();
        payment.Opportunity__c=opprecord.id;
        payment.Payment_Amount__c=2000;
        payment.vendor__c = accountrec.Id;
        payment.Payment_Date__c = System.today();
        payment.RecordTypeId=payrecType;
        payment.Category_lookup__c = category.Id;
        payment.Prior_Refund1__c = 10;
        payment.Special_Welfare_Account__c = swa.id;
    
        insert payment;
        
       
        id payrecType1 = Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Receipt').getRecordTypeId();
        Payment__c payment11 = new Payment__c();
        payment11.Opportunity__c=opprecord.id;
        payment11.Payment_Amount__c=2000;
        payment11.vendor__c = accountrec.Id;
        payment11.Payment_Date__c = System.today();
        payment11.RecordTypeId=payrecType1;
        payment11.Category_lookup__c = category.Id;
        payment11.Refund_Amount__c = 5;
        payment11.Payment_Source__c='5';
        payment11.Special_Welfare_Account__c = swa.id;
        insert payment11;
        list<Special_Welfare_Account__c> Swalist=[select id,(Select id,payment_source__C from Invoices1__r) from Special_Welfare_Account__c where id=:Swa.id];
        system.debug('**VAl of swalist'+swalist);
        swa.Case_KNP__c = caserec1.Id;
        swa.Account_Closed__c = true;
        update swa;
        
    }
}