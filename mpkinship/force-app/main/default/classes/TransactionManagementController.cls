public class TransactionManagementController {
    private final static Integer MAX_RESULTS = 5;
    public final static STRING USER = 'User';
    public final static STRING CAMP = 'Campaign';
    public final static String STATUS_SA = 'Submitted For Approval';
    public final static String STATUS_CREATE = 'Created';
    public final static String LASER_CERT = 'LASER Certification';
    public final static String LASER_RECON = 'LASER Reconciliation';

    @AuraEnabled
    public static User initData(){
        return [SELECT Id, Name, Title FROM User WHERE id =: userInfo.getUserId()];
    }

    @AuraEnabled(cacheable=true)
    public static LookupSearchResult getDefaultAP(String strId) {
        List<Campaign> lstCams = [SELECT id, name, StartDate, endDate, Status__c, Recon_Approval_Status__c, Status
                                    FROM Campaign
                                    WHERE Id = :strId];
        if(lstCams.size() > 0) {
            return new LookupSearchResult(lstCams[0].Id, 'Campaign', 'standard:campaign', lstCams[0].Name, '');
        }
        return null;
    }

    @AuraEnabled(cacheable=true)
    public static List<LookupSearchResult> kinshipSearchLookup(String searchTerm, List<String> selectedIds, String anOptionalParam) {
        // Prepare query paramters
        searchTerm = '%' + searchTerm + '%';

        // Prepare results
        List<LookupSearchResult> results = new List<LookupSearchResult>();
        List<List<SObject>> searchResults;
        if(String.isNotBlank(anOptionalParam)) {
            if(CAMP.equals(anOptionalParam)) {
                List<Campaign> lstCams = [SELECT id, name, StartDate, endDate, Status__c, Recon_Approval_Status__c, Status
                                    FROM Campaign
                                    WHERE RecordType.Name = 'LASER Reporting Period'
                                    AND Name LIKE :searchTerm
                                    AND id NOT IN :selectedIds
                                    AND IsActive = true ORDER BY StartDate DESC];
                String campaignIcon = 'standard:campaign';
                for(Campaign c : lstCams) {
                    results.add(new LookupSearchResult(c.Id, 'Campaign', campaignIcon, c.Name, 'Status:' + (String.isNotBlank(c.Status)?c.Status:'') + ' - Certification:' + (String.isNotBlank(c.Status__c)?c.Status__c:'') + '-Reconciliation:' + (String.isNotBlank(c.Recon_Approval_Status__c)?c.Recon_Approval_Status__c:'')));
                }
            } else if(USER.equals(anOptionalParam)) {
                searchResults = [FIND :searchTerm IN ALL FIELDS RETURNING
                                User (Id, Name, Email WHERE id NOT IN :selectedIds)
                                LIMIT :MAX_RESULTS];
                String uItem = 'standard:user';
                User[] users = ((List<User>) searchResults[0]);
                for (User u : users) {
                    results.add(new LookupSearchResult(u.Id, 'User', uItem, u.Name, ''));
                }
            }
        }
        return results;
    }

    @AuraEnabled
    public static List<TransJournal> getTransactionByAP(List<String> lstAccPeriod){
        try {
            String query = 'SELECT Id, Name, Child_Campaign__c,Invoice__r.RecordTypeId, Invoice__r.RecordType.Name, ' +
                            'Invoice__c, Invoice__r.Name, Invoice__r.Payment_Amount__c,recordType.Name,  ' +
                            'Status__c, Total_Amount__c, Description__c,  ' +
                            'Credit__c, Debit__c, LASER_Cost_Code__c,Invoice__r.Opportunity__r.Campaign.EndDate,  ' +
                            'LASER_Account_Code__c, FIPS_Code__c, Child_Campaign__r.Status__c,Child_Campaign__r.Name,  ' +
                			'Invoice__r.Category_lookup__r.Locality_Account__c, Invoice__r.Category_lookup__r.Locality_Account__r.Name, ' +
                			'Invoice__r.Category_lookup__r.Locality_Account__r.Account_Title__c ' +
                            'FROM Transaction_Journal__c ' +
                            'WHERE Child_Campaign__r.RecordType.Name = \'LASER Reporting Period\' AND Status__c = \'Posted\' AND RecordType.Name = \'LASER\' ';
            String strWhere = '';
            if(lstAccPeriod.size() > 0) strWhere += ' AND Child_Campaign__c IN :lstAccPeriod ';
            if(String.isNotBlank(strWhere)) query += strWhere;
            query += ' ORDER BY Status__c DESC, CreatedDate DESC LIMIT 10000 ';
            System.debug('--getTransactionByAP--' + query);
            List<TransJournal> results = new List<TransJournal>();
            Set<Id> setId = new Set<Id>();
            Date d;
            for(Transaction_Journal__c tj : Database.query(query)) {
                if(setId.contains(tj.Id)) continue;
                setId.add(tj.Id);
                TransJournal temp = new TransJournal();
                temp.id = tj.Id;
                temp.type = tj.recordType.Name;
                temp.name = tj.name;
                temp.tjUrl = '/' + tj.Id;
                temp.amount = KinshipUtil.getAmountValue(tj.Total_Amount__c);
                temp.description = tj.Description__c;
                if(String.isNotBlank(tj.Invoice__c) && String.isNotBlank(tj.Invoice__r.RecordTypeId)){
                    if(Label.KINSHIP_RECEIPT_ID.contains(tj.Invoice__r.RecordTypeId)){
                        temp.receiptName = tj.Invoice__r.Name;
                        temp.receiptUrl = '/' + tj.Invoice__c;
                        temp.paymentName = '';
                        temp.paymentUrl = '';
                    } else {
                        temp.receiptName = '';
                        temp.receiptUrl = '';
                        temp.paymentName = tj.Invoice__r.Name;
                        temp.paymentUrl = '/' + tj.Invoice__c;
                    }
                }
                temp.status = tj.Status__c;
                temp.credit = KinshipUtil.getAmountValue(tj.Credit__c);
                if(tj.Debit__c != null) {
                    if(tj.Debit__c > 0) {
                        temp.debit = tj.Debit__c;
                    } else {
                        temp.credit = KinshipUtil.getAmountValue(tj.Debit__c);
                    }
                }
                temp.laserAccountCode = tj.LASER_Account_Code__c;
                temp.laserCodeCode = tj.LASER_Cost_Code__c;
                temp.fipsCode = tj.FIPS_Code__c;
                temp.approvalStatus = tj.Child_Campaign__r.Status__c;
                temp.tj = tj;

                results.add(temp);
            }
            return results;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static void updateTransactionStatus(List<String> lstTJ, String status){
        try {
            if(lstTJ == null || lstTJ.size() == 0) return;
            List<Transaction_Journal__c> lstUpdate = new List<Transaction_Journal__c>();
            for(String str : lstTJ) {
                Transaction_Journal__c temp = new Transaction_Journal__c();
                temp.Id = str;
                temp.Status__c = status;
                lstUpdate.add(temp);
            }
            update lstUpdate;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static LASER_Report_TJ__c createLASERCertificationReport(String apId){
        try {
            LASER_Report_TJ__c result = new LASER_Report_TJ__c();
            //if(lstId == null || lstId.size() == 0) return result;
            result.ET_REIM_Admin__c = 0;
            result.ET_REIM_Assist__c = 0;
            result.ET_REIM_PO__c = 0;
            result.ET_REIM_OTHER__c = 0;
            result.ET_NREIM_Admin__c = 0;
            result.ET_NREIM_Assist__c = 0;
            result.ET_NREIM_PO__c = 0;
            result.ET_NREIM_OTHER__c = 0;
            List<Transaction_Journal__c> lstTJ = getTJ(apId);
            Campaign ap = [SELECT id, EndDate, Approved_Rejected_By__c, Approved_Rejected_By__r.Signature__c FROM Campaign WHERE id = :apId];
            result.Report_End_Date__c = ap.EndDate;
            System.debug('---createLASERCertificationReport--' + apId + ' ' + lstTJ.size());
            if(lstTJ.size() == 0) return result;
            Boolean btemp;
            Decimal dTemp;
            for(Transaction_Journal__c tj : lstTJ) {
                btemp = compareString(tj.Fund__c, Label.KINSHIP_FUND);
                if(String.isNotBlank(tj.LASER_Cost_Code__c) && Label.KINSHIP_COSTCODE.contains(tj.LASER_Cost_Code__c)){
                    btemp = false;
                }
                dTemp = getAmount(tj.Debit__c) - getAmount(tj.Credit__c);
                if(compareString(tj.Expenditure_Type__c, Label.KINSHIP_ADMIN)){
                    if(btemp) {
                        result.ET_REIM_Admin__c += dTemp;
                    } else {
                        result.ET_NREIM_Admin__c += dTemp;
                    }
                } else if(compareString(tj.Expenditure_Type__c, Label.KINSHIP_ASSIST)){
                    if(btemp) {
                        result.ET_REIM_Assist__c += dTemp;
                    } else {
                        result.ET_NREIM_Assist__c += dTemp;
                    }
                } else if(compareString(tj.Expenditure_Type__c, Label.KINSHIP_PO)){
                    if(btemp) {
                        result.ET_REIM_PO__c += dTemp;
                    } else {
                        result.ET_NREIM_PO__c += dTemp;
                    }
                }  else {
                    if(btemp) {
                        result.ET_REIM_OTHER__c += dTemp;
                    } else {
                        result.ET_NREIM_OTHER__c += dTemp;
                    }
                }
            }
            result.Approver_Signature__c = ap.Approved_Rejected_By__r.Signature__c;
            result.Accounting_Period__c = ap.id;
            result.Type__c = 'Certification';
            //insert result;
            return result;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static LASER_Report_TJ__c createLASERReconciliationReport(String apId){
        try {
            LASER_Report_TJ__c result = new LASER_Report_TJ__c();
            //if(lstId == null || lstId.size() == 0) return result;
            result.AT_Staff_Exp__c = 0;
            result.AT_OE__c = 0;
            result.AT_MAP__c = 0;
            result.AT_POSE__c = 0;
            result.AT_Other__c = 0;
            result.AT_Total_Staff_Exp__c = 0;
            result.AT_Total_OE__c = 0;
            result.AT_Total_MAP__c = 0;
            result.AT_Total_POSE__c = 0;
            result.AT_Total_Other__c = 0;
            Campaign ap = [SELECT id, EndDate, Approved_Rejected_By__c, Approved_Rejected_By__r.Signature__c FROM Campaign WHERE id = :apId];
            result.Report_End_Date__c = ap.EndDate;
            List<Transaction_Journal__c> lstTJ = getTJ(apId);
            if(lstTJ.size() == 0) return result;
            Boolean btemp;
            Decimal dTemp;
            for(Transaction_Journal__c tj : lstTJ) {
                dTemp = getAmount(tj.Debit__c) - getAmount(tj.Credit__c);
                if(compareString(tj.Expenditure_Type__c, Label.KINSHIP_ADMIN)){
                    if(String.isNotBlank(tj.LASER_Account_Code__c)) {
                        if(tj.LASER_Account_Code__c.startsWithIgnoreCase('51') ||
                            tj.LASER_Account_Code__c.startsWithIgnoreCase('52')) {
                                result.AT_Staff_Exp__c += dTemp;
                        } else if(tj.LASER_Account_Code__c.startsWithIgnoreCase('5')){
                            result.AT_OE__c += dTemp;
                        } else {
                            result.AT_Other__c += dTemp;
                        }
                    }
                } else if(compareString(tj.Expenditure_Type__c, Label.KINSHIP_ASSIST)){
                    result.AT_MAP__c += dTemp;
                } else if(compareString(tj.Expenditure_Type__c, Label.KINSHIP_PO)){
                    result.AT_POSE__c += dTemp;
                }  else {
                    result.AT_Other__c += dTemp;
                }
            }system.debug('---createLASERReconciliationReport---' + ap.id);
            result.Approver_Signature__c = ap.Approved_Rejected_By__r.Signature__c;
            result.Accounting_Period__c = ap.id;
            result.Type__c = 'Reconciliation';
            //insert result;
            return result;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static LASER_Report_TJ__c insertTR(LASER_Report_TJ__c data){
        try {
            insert data;
            return data;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static List<DataExport> getExportData(String apId){
        List<DataExport> result = new List<DataExport>();
        List<Transaction_Journal__c> lstTJ = getTJ(apId);

        for(Transaction_Journal__c tj : lstTJ) {
            DataExport data = new DataExport();
            data.costCode = tj.LASER_Cost_Code__c;
            data.fipsCode = tj.FIPS_Code__c;
            if(!compareString(tj.Fund__c, Label.KINSHIP_FUND) && String.isNotBlank(tj.LASER_Cost_Code__c) && Label.KINSHIP_COSTCODE.contains(tj.LASER_Cost_Code__c)){
                data.fund = '0033';
            } else {
                data.fund = '1111';
            }
            data.accountCode = tj.LASER_Account_Code__c;
            data.expType = tj.Invoice__r.Category_lookup__r.LASER_Cost_Code__r.Laser_Master_Table__r.Exp_Type__c;
            data.programe = tj.PRGM__c;
            data.grant = tj.Grant__c;
            data.project = tj.Project__c;
            data.budgetLine = tj.LASER_Budget_Line__c;
            data.credit = KinshipUtil.getAmountValue(tj.Credit__c);
            if(tj.Debit__c >= 0) {
                data.debit = tj.Debit__c;
            } else {
                data.credit = KinshipUtil.getAmountValue(tj.Debit__c);
            }
            data.dff = tj.DFF__c;//tj.DFF_Type__c;
            data.caseCount = tj.Case_Count__c;
            data.childCount = tj.Invoice__r.Child_Count__c;
            data.adultCount = tj.Invoice__r.Adult_Count__c;
            data.childinRes = tj.Child_In_Residential_Facility__c;
            data.recepientCount = tj.Receipient_Count__c;
            data.serviceDate = tj.Invoice__r.Opportunity__r.CloseDate;
            data.warrantDate = tj.Invoice__r.Payment_Date__c;
            data.adjustmentReason = tj.Invoice__r.Description__c;
            data.caseNumber = tj.Case_Number__c;
            data.clientID = tj.Case_Number__c;
            data.clientName = getString(tj.Invoice__r.Kinship_Case__r.Primary_Contact__r.FirstName) + '_' + getString(tj.Invoice__r.Kinship_Case__r.Primary_Contact__r.LastName);
            //data.clientName = 'Victor_ Opio';
            data.warrantDate = tj.Invoice__r.Payment_Date__c;
            data.receiptNumber = tj.Invoice__r.Report_Number__c;
            result.add(data);
        }

        return result;
    }

    private static List<Transaction_Journal__c> getTJ(String apId) {
        Set<String> setStatus = new Set<String>();
        setStatus.add('Posted');
        //setStatus.add('Closed');
        return [SELECT Id, Name, Invoice__c, Invoice__r.Name, recordType.Name,Expenditure_Type__c, Fund__c,
                        Status__c, Total_Amount__c, Description__c, Credit__c, Debit__c, LASER_Cost_Code__c, LASER_Account_Code__c, FIPS_Code__c,
                        Account_Code_Type__c,PRGM__c,Grant__c,Project__c,LASER_Budget_Line__c,Case_Count__c,
                        Invoice__r.Child_Count__c,Invoice__r.Adult_Count__c,Child_In_Residential_Facility__c,Receipient_Count__c,
                        Invoice__r.RecordType.Name, Invoice__r.Opportunity__r.CloseDate,
                        Invoice__r.Adjustment_Reason__c,Invoice__r.Opportunity__r.Kinship_Case__r.Primary_Contact__r.FirstName,
                        Invoice__r.Opportunity__r.Kinship_Case__r.Primary_Contact__r.LastName,Invoice__r.Payment_Date__c,
                        Invoice__r.Report_Number__c,DFF_Type__c,Case_Number__c,Invoice__r.Kinship_Case__r.Primary_Contact__r.FirstName,
                        Invoice__r.Kinship_Case__r.Primary_Contact__r.LastName,DFF__c,Invoice__r.Description__c,
                		Invoice__r.Category_lookup__r.LASER_Cost_Code__r.Laser_Master_Table__r.Exp_Type__c
                FROM Transaction_Journal__c
                WHERE Child_Campaign__c = :apId
                AND Status__c IN :setStatus];
    }

    private static Boolean compareString(String str, String strLabel) {
        if(String.isNotBlank(str) && strLabel.equals(str)){
            return true;
        }
        return false;
    }

    private static String getString(String str) {
        return String.isNotBlank(str) ? str : '';
    }

    private static Decimal getAmount(Decimal amount) {
        return amount==null?0:amount;
    }

    @AuraEnabled
    public static void submitCampaignForApproval(String camp, String userId, String type, LASER_Report_TJ__c reportTJ){
        try {
            insert reportTJ;

            Payment_Approval__c ap = new Payment_Approval__c();
            ap.Status__c = STATUS_CREATE;
            ap.Approval_User__c = userId;
            ap.RecordTypeId = Schema.SObjectType.Payment_Approval__c.getRecordTypeInfosByName().get('Campaign').getRecordTypeId();
            insert ap;

            // insert Payment_Approval_Step__c
            Payment_Approval_Step__c step = new Payment_Approval_Step__c();
            step.Accounting_Period__c = camp;
            step.Payment_Approval__c = ap.id;
            step.AP_Type__c = type;
            insert step;

            Campaign c = new Campaign();
            c.id = camp;
            if(type == LASER_CERT) {
                c.Status__c = STATUS_SA;
                c.Cert_Requested_By__c = UserInfo.getUserId();
                c.Cert_Report_TJ__c = reportTJ.Id;
                c.Approved_Rejected_By__c = userId;
            } else {
                c.Recon_Approval_Status__c = STATUS_SA;
                c.Recon_Requested_By__c = UserInfo.getUserId();
                c.Recon_Report_TJ__c = reportTJ.Id;
                c.Recon_Approved_Rejected_By__c = userId;
            }
            update c;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static List<CampaignApproval> getApprovalCampignData(){
        try {
            List<CampaignApproval> lstResult = new List<CampaignApproval>();

            List<Payment_Approval_Step__c> lstPAS = [SELECT id, Name, Accounting_Period__c, Accounting_Period__r.Name,CreatedBy.Name, AP_Type__c
                                                    FROM Payment_Approval_Step__c
                                                    WHERE Payment_Approval__r.Status__c = :STATUS_CREATE
                                                    AND ((Accounting_Period__r.Status__c = :'Submitted For Approval' AND AP_Type__c = 'LASER Certification' ) OR 
                                                        (Accounting_Period__r.Recon_Approval_Status__c = :'Submitted For Approval' AND AP_Type__c = 'LASER Reconciliation' ))
                                                    AND Payment_Approval__r.RecordTypeId = :Schema.SObjectType.Payment_Approval__c.getRecordTypeInfosByName().get('Campaign').getRecordTypeId()];

            for(Payment_Approval_Step__c pas : lstPAS) {
                CampaignApproval temp = new CampaignApproval();
                temp.id = pas.Accounting_Period__c + '_' + pas.AP_Type__c;
                temp.url = '/' + pas.Accounting_Period__c;
                temp.name = pas.Accounting_Period__r.Name;
                temp.requestedby = pas.CreatedBy.Name;
                temp.type = pas.AP_Type__c;
                lstResult.add(temp);
            }
            return lstResult;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static List<Campaign> updateCampaignStatus(List<String> lstId, String status, String strNote){
        try {
            List<Campaign> lstCampaign = new List<Campaign>();
            List<String> lstTemp;
            for(String str : lstId){
                lstTemp = str.split('_');
                if(lstTemp.size() == 2){
                    Campaign temp = new Campaign();
                    temp.id = lstTemp[0];
                    if(lstTemp[1] == LASER_CERT) {
                        temp.Status__c = status;
                        temp.Approved_Rejected_By__c = UserInfo.getUserId();
                        temp.Approved_Rejected_Notes__c = strNote;
                    } else {
                        temp.Recon_Approval_Status__c = status;
                        temp.Recon_Approved_Rejected_By__c = UserInfo.getUserId();
                        temp.Recon_Approved_Rejected_Notes__c = strNote;
                    }
                    lstCampaign.add(temp);
                }
            }
            if(lstCampaign.size() > 0){
                update lstCampaign;
            }
            return lstCampaign;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled 
    public static Boolean checkPostAP(List<String> lstId){
        return TransactionJournalService.checkPostedTJ(lstId);
    }

    @AuraEnabled 
    public static void postedAP(List<String> lstId){
        try{
            List<Campaign> lstCamp = [SELECT id, Name, StartDate, EndDate, RecordTypeId, ParentId, Parent.ParentId, Status, FIPS__c, Cate_Type__c
                                        FROM Campaign  
                                        WHERE id IN :lstId];
            TransactionJournalService.autoPostedTJ(lstCamp);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    @AuraEnabled
    public static List<ReportData> getLASERReportData(List<String> lstAccPeriod){
        List<ReportData> lstData = new List<ReportData>();
        List<Payment__c> lstPayment = [SELECT Id, Name,Category_lookup__r.Locality_Account__c,Category_lookup__r.Locality_Account__r.Name,
                                        Payment_Date__c, Payment_Amount__c, Vendor__c, Vendor__r.Name,Kinship_Case__c, Kinship_Case__r.Name,Description__c, Invoice_Stage__c, Invoices_Status__c,
                                        Category_lookup__r.LASER_Account_Code__c,Category_lookup__r.LASER_Account_Code__r.Name, Category_lookup__r.LASER_Cost_Code__c, Category_lookup__r.LASER_Cost_Code__r.Name,
                                       	RecordType.Name, Category_lookup__r.Locality_Account__r.Account_Title__c,Accounting_Period__r.Name, Accounting_Period__c
                                        FROM Payment__c
                                        WHERE Accounting_Period__c IN :lstAccPeriod];
        Set<Id> setId = new Set<Id>();
        for(Payment__c p : lstPayment){
            setId.add(p.id);
        }
        Map<String, Transaction_Journal__c> mapTJ = new Map<String, Transaction_Journal__c>();
        for(Transaction_Journal__c tj : [SELECT Id, Name, Invoice__c FROM Transaction_Journal__c WHERE Invoice__c IN :setId]){
            mapTJ.put(tj.Invoice__c, tj);
        }
        for(Payment__c p : lstPayment){
            ReportData temp = new ReportData();
            temp.payment = p;
            temp.tj = mapTJ.get(p.id);
            lstData.add(temp);
        }system.debug('--Quy--' + lstData.size());
        return lstData;
    }
    
    public class ReportData{
		@AuraEnabled
        public Payment__c payment{get;set;}
        
        @AuraEnabled
        public Transaction_Journal__c tj{get;set;}
    }
    
    public class TransJournal{
        @AuraEnabled
        public string id{get;set;}

        @AuraEnabled
        public string type{get;set;}

        @AuraEnabled
        public string name{get;set;}

        @AuraEnabled
        public string tjUrl{get;set;}

        @AuraEnabled
        public Decimal amount{get;set;}

        @AuraEnabled
        public string description{get;set;}

        @AuraEnabled
        public string status{get;set;}

        @AuraEnabled
        public string receiptName{get;set;}

        @AuraEnabled
        public string receiptUrl{get;set;}

        @AuraEnabled
        public string paymentName{get;set;}

        @AuraEnabled
        public string paymentUrl{get;set;}

        @AuraEnabled
        public Decimal debit{get;set;}

        @AuraEnabled
        public Decimal credit{get;set;}

        @AuraEnabled
        public string laserAccountCode{get;set;}

        @AuraEnabled
        public string laserCodeCode{get;set;}

        @AuraEnabled
        public string fipsCode{get;set;}

        @AuraEnabled
        public string approvalStatus{get;set;}
        
        @AuraEnabled
        public Transaction_Journal__c tj{get;set;}
    }

    public class DataExport{
        @AuraEnabled
        public string costCode{get;set;}

        @AuraEnabled
        public string fipsCode{get;set;}

        @AuraEnabled
        public string fund{get;set;}

        @AuraEnabled
        public string accountCode{get;set;}

        @AuraEnabled
        public string expType{get;set;}

        @AuraEnabled
        public string programe{get;set;}

        @AuraEnabled
        public string grant{get;set;}

        @AuraEnabled
        public string project{get;set;}

        @AuraEnabled
        public string budgetLine{get;set;}

        @AuraEnabled
        public Decimal debit{get;set;}

        @AuraEnabled
        public Decimal credit{get;set;}

        @AuraEnabled
        public string dff{get;set;}

        @AuraEnabled
        public Decimal caseCount{get;set;}

        @AuraEnabled
        public Decimal childCount{get;set;}

        @AuraEnabled
        public Decimal adultCount{get;set;}

        @AuraEnabled
        public Decimal childinRes{get;set;}

        @AuraEnabled
        public Decimal recepientCount{get;set;}

        @AuraEnabled
        public Date serviceDate{get;set;}

        @AuraEnabled
        public string adjustmentReason{get;set;}

        @AuraEnabled
        public string caseNumber{get;set;}

        @AuraEnabled
        public string clientID{get;set;}

        @AuraEnabled
        public string clientName{get;set;}

        @AuraEnabled
        public Date warrantDate{get;set;}

        @AuraEnabled
        public String receiptNumber{get;set;}

        @AuraEnabled
        public string strDff{get;set;}
    }

    public class CampaignApproval{
        @AuraEnabled
        public string id{get;set;}

        @AuraEnabled
        public string name{get;set;}

        @AuraEnabled
        public string url{get;set;}

        @AuraEnabled
        public string requestedby{get;set;}

        @AuraEnabled
        public string type{get;set;}
    }
}