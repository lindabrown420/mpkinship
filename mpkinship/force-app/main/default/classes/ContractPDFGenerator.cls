public class ContractPDFGenerator {
    @AuraEnabled
    public static Contract GenerateCustomPDFAndEmail(Id recordId){
        string result;
        Contract con = [ Select id, Accountid, Account.Name,Title__c,Status, Account.Email__c, Email_to_Vendor__c from Contract where id = :recordId limit 1];
        if(con.Email_to_Vendor__c == True)
        {
            if(con.Account.Email__c == null){	
                //return 'Vendor email does not exist.';
            }
            try{
            PageReference defaultPage = new PageReference('/apex/Contracts'); 
            defaultPage.getParameters().put('id',recordId);
            
            Blob pageData; //variable to hold binary PDF data.
            if(!Test.isRunningTest()){ // for code coverage 
                pageData = defaultPage.getContentAsPDF();
            } else {
                pageData = Blob.valueOf('This is a test.');
            }
            //create attachment
            
            String filename ='Contract ' + DateTime.now().formatLong() + '.pdf';
            String vendorEmail = con.Account.Email__c;
            Attachment att = new Attachment(
                                    ParentId=recordId,
                                    Body=pageData,
                                    Name= filename
                                );
            insert att;
            // Current User information
            String FirstName = Userinfo.getFirstName();
            String LastName  = Userinfo.getLastName();
            String Role  = '';
            Role = con.Title__c;
            String userEmail = Userinfo.getUserEmail();
                
            //create and send email 
            Messaging.EmailFileAttachment mailAtt = new Messaging.EmailFileAttachment();
            mailAtt.setBody(pageData);
            mailAtt.setContentType('application/pdf');
            mailAtt.setFileName(filename);
            string body = 'Hi'+ ' ' + con.account.Name +',' + '<br/>';
            body += '<br/>' + 'Thank you for being a vendor with us.';
            body += 'Your vendor contract is attached for your review and signature. Please contact us at the email below with questions.' + '<br/>';
            body += '<br/>';
			body += '<br/>'+'Thank you,' + '<br/>';
			body +=  FirstName + ' ' + LastName + ' ' + '|' + ' ' + Role + '<br/>';
			body += userEmail;
            Messaging.SingleEmailMessage mess = new Messaging.SingleEmailMessage();
            mess.setSubject('Contract with CSA');
            mess.setToAddresses(new String[]{vendorEmail});
            //mess.setPlainTextBody(body);
            mess.setHtmlBody(body);
            mess.setFileAttachments(new Messaging.EmailFileAttachment[]{mailAtt});
            //Messaging.sendEmail(new Messaging.Email[]{mess},false); 
            //result = 'Email has been sent successfully!!';
                if(con.Status == 'Draft'){
                	con.Status = 'In Approval Process';
                	update con;
                }
            }catch(exception e){
                //return e.getMessage();
            }    
        }else{
          //result = 'Email is disabled for the vendor.';  
        }
        return con;
    }
}