public with sharing class CampaignController {
    @AuraEnabled
    public static String getRT_Name(string rtId){
        RecordType rt = [Select id,name from RecordType where id=: rtId];
        return rt.Name;        
    }
}