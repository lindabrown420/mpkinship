@istest
public class TestMultiSelectLookupController {
    
    static  testmethod void testRecords(){
        account accountrec = new account();
        accountrec.Name='test vendor';
        accountrec.Tax_ID__c='222-22-3333';
        accountrec.Entity_Type__c='Individual';
        insert accountrec;
        
        
        fips__c fip= new fips__c();
        fip.name = 'Alleghany';
        insert fip;
        
         Campaign parent = new Campaign();
        parent.name = 'Local - 2022 - Covington';
        parent.StartDate = Date.Today().addDays(-10);
        parent.EndDate = Date.Today().addDays(90);
        parent.Fiscal_Year_Type__c = 'Local';
        parent.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId();
        insert parent;
        
        Campaign child = new Campaign();
        child.name = 'DSS - 2022 - Covington';
        child.StartDate = Date.Today().addDays(-10);
        child.EndDate = Date.Today().addDays(90);
        child.Fiscal_Year_Type__c = 'DSS';
        child.ParentId = parent.id;
        child.FIPS__c =  fip.id;
        child.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId();
        insert child;
        
        Campaign grandchild = new Campaign();
        grandchild.name = 'LASER - 2022-03-31 - Covington';
        grandchild.StartDate = Date.Today().addDays(-10);
        grandchild.EndDate = Date.Today().addDays(90);
        grandchild.ParentId = child.id;
        grandchild.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('LASER Reporting Period').getRecordTypeId();
        insert grandchild;
        
        id catrecType = Schema.SObjectType.Category__c.getRecordTypeInfosByName().get('LASER Category').getRecordTypeId();
        
        Locality_Account__c la = new Locality_Account__c();
        la.name='33';
        insert la;
        
        Category__c category = new Category__c();
        category.Name='222';
        category.Category_Type__c='IVE';
        category.Locality_Account__c=la.id;
        category.RecordTypeId = catrecType;
        insert category;
        
         Cost_Center__c cc = new Cost_Center__c();
        cc.Name='test';
        insert cc;
        Chart_of_Account__c ca = new Chart_of_Account__c();
        ca.Locality_Account__c=la.id;
        ca.FIPS__c=fip.id;
        ca.Cost_Center__c=cc.id;
        insert ca;
        
        id opprecType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Report of Collections').getRecordTypeId();
        opportunity opprecord = new opportunity();
        opprecord.CloseDate=date.today();
        opprecord.StageName='Draft';
        opprecord.Name='test opportunity';        
        opprecord.Amount=20000;
        opprecord.AccountId=accountrec.id;
        opprecord.RecordTypeId=opprecType;
        opprecord.End_Date__c =date.today() + 15;
        opprecord.Category__c = category.Id;
        insert opprecord;
        
        
        id payrecType = Schema.SObjectType.Payment__c.getRecordTypeInfosByName().get('Vendor Invoice').getRecordTypeId();
        Payment__c payment = new Payment__c();
        payment.Opportunity__c=opprecord.id;
        payment.Payment_Amount__c=2000;
        payment.vendor__c = accountrec.Id;
        payment.RecordTypeId=payrecType;
        payment.Refund_Amount__c = 100;
        payment.Category_lookup__c = category.Id;
        insert payment;
        
        List<Payment__c> records = new List<Payment__c>();
        records.add(payment);
        
        MultiSelectLookupController c = new MultiSelectLookupController();
        
        MultiSelectLookupController.UpdateSobjectList(records, '234',  parent.id, 'check', 'IVE', string.valueof(date.today() + 2), opprecord.id, 'test', '2365454');
        List<String> fieldNames = new List<String>();
        fieldNames.add('StageName');
        
        List<String> fieldNames2 = new List<String>();
        fieldNames2.add('Name');
        string likeString;
        String WhereClause;String WhereClause1 = 'Name = \'a\'' ; String value;String value1 = 'Local - 2022 - Covington';
        List<String> selectedRecId; 
        List<String> costCenterIds = new List<String>(); 
        set<id> categoryids = new set<id>();
        set<id> fipsids = new set<id>();
        List<Payment__c> Invoices = new List<Payment__c>();
        Categoryids.add(category.Id);
        fipsids.add(fip.Id);
        Invoices.add(payment);
        
        List<Id> InvoiceIds = new List<Id>();
        for(Payment__c p: Invoices){
            InvoiceIds.add(p.id);
        }
        
        MultiSelectLookupController.getSobjectListResults( 'Opportunity', fieldNames,  WhereClause1);
        MultiSelectLookupController.getResults('Cost_Center__c', fieldNames2,  value, selectedRecId,  WhereClause, parent.id, likeString, costCenterIds);
        MultiSelectLookupController.getResults('Campaign', fieldNames2,  value1, selectedRecId,  WhereClause, parent.id, likeString, costCenterIds);
        MultiSelectLookupController.getSobjectListResultsCache('Campaign', fieldNames2,  WhereClause1) ;
        MultiSelectLookupController.getResults('Opportunity', fieldNames2,  value1, selectedRecId,  WhereClause, parent.id, likeString, costCenterIds);
        MultiSelectLookupController.getResults('Payment__c', fieldNames2,  value1, selectedRecId,  WhereClause, parent.id, likeString, costCenterIds);
        String recNameTemp; Id recIdTemp;
        MultiSelectLookupController.SObjectResult con = new MultiSelectLookupController.SObjectResult (recNameTemp,recIdTemp, '', '','');
        MultiSelectLookupController.FetchCostcode(Invoices, categoryids, fipsids);
        MultiSelectLookupController.SetPaymentCategoryAndFIPS(InvoiceIds);
        
    }
}