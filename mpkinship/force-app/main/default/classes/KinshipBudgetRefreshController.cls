public class KinshipBudgetRefreshController {
    @AuraEnabled
    public static void RefreshBudget(){
        try {
            List<Campaign> lstFY = [SELECT id, name, parentid, parent.Name, parent.ParentId, parent.Parent.Name
                                    FROM Campaign
                                    WHERE IsActive = true];
            Set<Id> setId = new Set<Id>();
            Map<String, String> mapFY = new Map<String, String>();
            Map<String, String> mapFYLocal = new Map<String, String>();
            for(Campaign c : lstFY) {
                if(String.isNotBlank(c.ParentId)){
                    mapFY.put(c.Id, c.ParentId);
                    setId.add(c.ParentId);
                    setId.add(c.Id);
                }
                if(String.isNotBlank(c.Parent.ParentId)){
                    mapFYLocal.put(c.Id, c.Parent.ParentId);
                }
            }
            List<Budgets__c> lstBudget = [SELECT id, name, Initial_Budget__c, Total_Receipts__c, Total_Expenditures__c, 
                                            Fiscal_Year__c, Fiscal_Year__r.Name, Category__c, Laser_Budget_Code__c, Locality_Account_Code_Name__c
                                            FROM Budgets__c WHERE Fiscal_Year__c IN :setId];
            Map<String, Budgets__c> mapBudget = new Map<String, Budgets__c>();
            for(Budgets__c b : lstBudget) {
                b.Total_Receipts__c = 0;
                b.Total_Expenditures__c = 0;
                if(String.isNotBlank(b.Category__c)) {
                    mapBudget.put(b.Fiscal_Year__c + '-' + b.Category__c, b);
                } else if(String.isNotBlank(b.Locality_Account_Code_Name__c)) {
                    mapBudget.put(b.Fiscal_Year__c + '-' + b.Locality_Account_Code_Name__c, b);
                } else if(String.isNotBlank(b.Laser_Budget_Code__c)) {
                    mapBudget.put(b.Fiscal_Year__c + '-' + b.Laser_Budget_Code__c, b);
                }
            }
            List<Payment__c> lstPayment = [SELECT id, Name, RecordType.Name, Invoice_Stage__c, Category_lookup__c, Category_lookup__r.Name, Category_lookup__r.Locality_Account__c, 
                                                    Category_lookup__r.LASER_Cost_Code__r.Laser_Budget_Line__c, Payment_Amount__c, Accounting_Period__c, Accounting_Period__r.Name
                                                    FROM Payment__c
                                                    WHERE Invoice_Stage__c IN ('Paid', 'Posted', 'Cancelled')
                                                    AND Accounting_Period__c IN :setId];
            String strT, strLocalT;
            Budgets__c b;
            for(Payment__c p : lstPayment) {
                strT = mapFY.get(p.Accounting_Period__c);
                strLocalT = mapFYLocal.get(p.Accounting_Period__c);
                if(String.isNotBlank(strT)) {
                    b = mapBudget.get(strT + '-' + p.Category_lookup__c);
                    if(b != null) {
                        if(Label.KINSHIP_BUDGET_RECEIPT.contains(p.RecordType.Name)) {
                            b.Total_Receipts__c += p.Payment_Amount__c;
                        } else {
                            b.Total_Expenditures__c += p.Payment_Amount__c;
                        }
                    }
                    System.debug('---RefreshBudget---' + b);
                    b = mapBudget.get(strLocalT + '-' + p.Category_lookup__r.Locality_Account__c);
                    if(b != null) {
                        if(Label.KINSHIP_BUDGET_RECEIPT.contains(p.RecordType.Name)) {
                            b.Total_Receipts__c += p.Payment_Amount__c;
                        } else {
                            b.Total_Expenditures__c += p.Payment_Amount__c;
                        }
                    }
                    System.debug('---RefreshBudget2---' + b);
                    b = mapBudget.get(strT + '-' + p.Category_lookup__r.LASER_Cost_Code__r.Laser_Budget_Line__c);
                    if(b != null) {
                        if(Label.KINSHIP_BUDGET_RECEIPT.contains(p.RecordType.Name)) {
                            b.Total_Receipts__c += p.Payment_Amount__c;
                        } else {
                            b.Total_Expenditures__c += p.Payment_Amount__c;
                        }
                    }
                    System.debug('---RefreshBudget3---' + b);
                }
            }
            update lstBudget;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
}