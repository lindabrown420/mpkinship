public class KinshipUnPostPaymentController {

    @AuraEnabled
    public static List<ROCWrapper> getUnpostROC(String stage){
        try {
            String strStage = '%' + stage + '%';
            List<ROCWrapper> lstResult = new List<ROCWrapper>();
            for(Opportunity roc : [SELECT id,name,recordtype.name, StageName, End_Date__c, CloseDate, CampaignId, 
                                    Campaign.Name, Amount, CreatedBy.Name, Posting_Period__c, CreatedDate
                                    FROM Opportunity
                                    WHERE recordtype.name = 'Report of Collections' AND StageName LIKE :strStage
                                  	ORDER BY CreatedDate DESC LIMIT 500]){
                ROCWrapper temp = new ROCWrapper();
                temp.id = roc.id;
                temp.name = roc.name;
                temp.rocurl = '/' + roc.id;
                temp.beginDate = roc.CloseDate;
                temp.endDate = roc.End_Date__c;
                temp.amount = roc.Amount;
                temp.stage = roc.StageName;
                temp.fiscalYear = roc.Campaign.Name;
                temp.fyurl = roc.CampaignId;
                temp.createdBy = roc.CreatedBy.Name;
                temp.postingPeriod = roc.Posting_Period__c;
                temp.createdDate = roc.CreatedDate;
                lstResult.add(temp);
            }
            return lstResult;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static void CompleteAndPostROC(List<String> lstId, String datePosted, String stage){
        try {
            System.debug('---CompleteAndPostROC---' + lstId + ' ' + datePosted + ' ' + stage);
            Id ridFY = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId();
            Id ridLASER = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('LASER Reporting Period').getRecordTypeId();
            Id ridLEDRS = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('LEDRS Reporting Period').getRecordTypeId();
            List<Campaign> lstCam = [SELECT id, StartDate, EndDate, RecordTypeId, ParentId, Parent.ParentId, FIPS__c, Fiscal_Year_Type__c, Posting_Period__c
                                	 FROM Campaign WHERE IsActive = true AND Posting_Period__c = :datePosted ORDER BY recordType.Name DESC];
            System.debug('---CompleteAndPostROC---' + lstCam);
            List<Payment__c> lstPayment = [SELECT id, Invoice_Stage__c, Invoices_Status__c, Accounting_Period__c, Category_Lookup__r.Category_Type__c, FIPS_Code__c
                                                        FROM Payment__c
                                                        WHERE Opportunity__c IN :lstId];
            if(lstCam.size() > 0) {
                Id recordType;
                Id fipsId;
                Id parentId;
                for(Payment__c item : lstPayment) {
                    item.Invoice_Stage__c = 'Posted';
                    item.Invoices_Status__c = 'Posted'
;                    for(Campaign c : lstCam) {
                        recordType = checkFundingSource(item.Category_Lookup__r.Category_Type__c, ridFY, ridLASER, ridLEDRS);
                        System.debug('---CompleteAndPostROC---' + recordType + ' ' + item.FIPS_Code__c);
                        if(recordType == ridFY) {
                            item.Accounting_Period__c = c.Parent.ParentId;
                            break;
                        } else {
                            if(c.recordTypeId == recordType && c.FIPS__c == item.FIPS_Code__c){
                                item.Accounting_Period__c = c.id;
                                break;
                            }
                        }
                    }
                }
                List<Opportunity> lstAuth = new List<Opportunity>();
                for(String str : lstId) {
                    lstAuth.add(new Opportunity(Id = str, StageName = stage, Posting_Period__c = datePosted, CloseDate = lstCam[0].StartDate, End_Date__c = lstCam[0].EndDate));
                }
                system.debug('---CompleteAndPostROC--' + lstPayment);
                system.debug('---CompleteAndPostROC--' + lstAuth);
                update lstPayment;
                update lstAuth;
        	}
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    private static Id checkFundingSource(String strT, Id ridFY, Id ridLASER, Id ridLEDRS) {
        if(strT == 'CSA'){
            return ridLEDRS;
        } else if(strT == 'Special Welfare' || strT == 'Claims Payable'){
            return ridFY;
        } else {
            return ridLASER;
        }
    }

    @AuraEnabled
    public static void MoveROCToNextAP(List<String> lstId){
        try {
            Date d = Date.today();
            d = d.addMonths(1);
            String nextPP = getCurrentPostingPeriod(d);
            
            List<Opportunity> lstUpdate = new List<Opportunity>();
            for(String str : lstId) {
                lstUpdate.add(new Opportunity(Id = str, Posting_Period__c = nextPP));
            }
            update lstUpdate;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    @AuraEnabled
    public static void markCompleteStage(List<String> lstId){
        try{
            List<Payment__c> missingFIPSPayments = [SELECT Id, FIPS_Code__c, Opportunity__r.FIPS_Code__c FROM Payment__c Where FIPS_Code__c = null AND Opportunity__c IN :lstId];
            List<Payment__c> tempPayments = new List<Payment__c>();
            List<Opportunity> lstROC = [Select Id,StageName, FIPS_Code__c from Opportunity where Id IN :lstId];
            for (Payment__c payment : missingFIPSPayments) {
                payment.FIPS_Code__c = payment.Opportunity__r.FIPS_Code__c;
                tempPayments.add(payment);
            }
            if(tempPayments.size() > 0){
                update tempPayments;
            }
            for(Opportunity opp : lstROC) {
                if(opp.StageName != 'Completed'){
                    opp.StageName = 'Completed';
                }
            }
            update lstROC;
        }catch(Exception e){
            throw new AuraHandledException(e.getMessage());
        } 
    }
    
    @AuraEnabled
    public static boolean reOpenROC(List<String> lstROC){
        try{
            system.debug('--reOpenROC---' + lstROC);
            List<opportunity> opplist = [SELECT id,(SELECT id FROM Payment_OppPayment__r WHERE Paid__c=:true) FROM Opportunity WHERE ROC_Id__c IN :lstROC];
            system.debug('--reOpenROC---' + opplist);
            if(opplist.size() > 0){
               for(opportunity opp : opplist){
                   if(opp.Payment_OppPayment__r.size() > 0){
                       return true;
                   }
               }
            }
            List<Opportunity> lstUpdate = new List<Opportunity>();
            for(String str : lstROC) {
                lstUpdate.add(new Opportunity(id = str, StageName = 'Open'));
            }
            system.debug('--reOpenROC---' + lstUpdate);
            update lstUpdate;
            
            List<Opportunity> CaseActions = [select id from Opportunity WHERE ROC_Id__c IN :lstROC];
            system.debug('--reOpenROC---' + CaseActions);
            if(CaseActions.size() > 0)
                delete CaseActions;
            return false;
        }catch(Exception e){
            throw new AuraHandledException(e.getMessage());
        } 
    }

    public static String getCurrentPostingPeriod(Date d){
        switch on d.month() {
            when 1{
                return 'January ' +  d.year();
            }
            when 2{
                return 'February ' +  d.year();
            }
            when 3{
                return 'March ' +  d.year();
            }
            when 4{
                return 'April ' +  d.year();
            }
            when 5{
                return 'May ' +  d.year();
            }
            when 6{
                return 'June ' +  d.year();
            }
            when 7{
                return 'July ' +  d.year();
            }
            when 8{
                return 'August ' +  d.year();
            }
            when 9{
                return 'September ' +  d.year();
            }
            when 10{
                return 'October ' +  d.year();
            }
            when 11{
                return 'November ' +  d.year();
            }
            when 12{
                return 'December ' +  d.year();
            }
            when else {
                return '';
            }
        }
    }

    public class ROCWrapper{
        @AuraEnabled
        public string id{get;set;}

        @AuraEnabled
        public string name{get;set;}

        @AuraEnabled
        public string rocurl{get;set;}

        @AuraEnabled
        public Date beginDate{get;set;}

        @AuraEnabled
        public Date endDate{get;set;}

        @AuraEnabled
        public Decimal amount{get;set;}

        @AuraEnabled
        public string fyUrl{get;set;}

        @AuraEnabled
        public string fiscalYear{get;set;}

        @AuraEnabled
        public string stage{get;set;}

        @AuraEnabled
        public string createdBy{get;set;}

        @AuraEnabled
        public string postingPeriod{get;set;}
        
        @AuraEnabled
        public DateTime createdDate{get;set;}
    }
}