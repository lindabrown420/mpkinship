public class CustomLookupCtr {
    @AuraEnabled
    public static List < sObject > fetchLookUpValues(String searchKeyWord, String ObjectName, string additionalCriteria) {
        String searchKey ='%'+ searchKeyWord + '%';        
        List < sObject > returnList = new List < sObject > ();      
        // Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5  
        if(objectName!='Category__c'){        
            String sQuery =  'select id, Name from ' +ObjectName + ' where Name LIKE: searchKey  order by createdDate DESC limit 5';
            List < sObject > lstOfRecords = Database.query(sQuery);        
            for (sObject obj: lstOfRecords) {
                returnList.add(obj);
            }
        }
        else{
             list<category__c> catlist= new list<Category__c>();
             Id LaserCategoryrecordTypeId = Schema.SObjectType.category__c.getRecordTypeInfosByDeveloperName().get('LASER_Category').getRecordTypeId();                            
             catlist=[select id,Name,recordtype.developername,Category_Account_Description__c,Category_Type__c,inactive__c,RecordTypeId,FIPS_Code__c from category__C where (name like: searchKey or Category_Account_Description__c like: searchKey) 
                            and inactive__c=:False order by createdDate DESC limit 200];    
            for(Category__c cat : catlist){
                system.debug('**VAl of cat'+cat);
                if(cat.recordtypeid==LaserCategoryrecordTypeId && cat.Category_Type__c=='Administrative'){
                    system.debug('**enterd in if');
                }
                else{    
                    system.debug('**entered in else'+cat); 
                    if(cat.Category_Type__c=='CSA'){
                        if(cat.FIPS_Code__c!=null && cat.FIPS_Code__c ==additionalCriteria)
                            returnlist.add(cat);
                    }
                    else                 
                        returnList.add(cat); 
                }    
                      
            }
        }
        
        return returnList;
    }
    
   
    @AuraEnabled
    public static List < sObject > fetchLookUpValuesForCaseAction(String searchKeyWord, String ObjectName,string recordtype,string AdditionalCriteria,list<id> AdditionalListCriteria) {
        String searchKey ='%'+ searchKeyWord + '%';        
        List < sObject > returnList = new List < sObject > ();      
        // Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5  
        if(objectName=='Cost_Center__c'){        
            String sQuery =  'select id, Name from ' +ObjectName + ' where Name LIKE: searchKey and id in: AdditionalListCriteria order by createdDate DESC limit 5';
            List < sObject > lstOfRecords = Database.query(sQuery);        
            for (sObject obj: lstOfRecords) {
                returnList.add(obj);
            }
        }
        else{
             list<category__c> catlist= new list<Category__c>();
             Id LaserCategoryrecordTypeId = Schema.SObjectType.category__c.getRecordTypeInfosByDeveloperName().get('LASER_Category').getRecordTypeId();
             
             catlist=[select id,Name,Category_Account_Description__c,recordtype.developername,Category_Type__c,inactive__c,RecordTypeId,FIPS_Code__c from category__C where (name like: searchKey or Category_Account_Description__c like: searchKey) AND (
                                      Id NOT IN (SELECT Category__c from Category_Rate__c  where Status__c=:'Active')) and inactive__c=:False order by createdDate DESC limit 200];
               
            for(Category__c cat : catlist){
                system.debug('**VAl of cat'+cat);
                if(cat.recordtypeid==LaserCategoryrecordTypeId && cat.Category_Type__c=='Administrative'){
                    system.debug('**enterd in if');
                }
                else{    
                    system.debug('**entered in else'+cat); 
                     if(cat.Category_Type__c=='CSA'){
                        if(cat.FIPS_Code__c!=null && cat.FIPS_Code__c ==AdditionalCriteria)
                            returnlist.add(cat);
                    }
                    else     
                        returnList.add(cat); 
                }    
                      
            }
        }
        
        return returnList;
    }
    
     @AuraEnabled
    public static List < ProductWrap> fetchProductValues(String searchKeyWord, String ObjectName) {
        String searchKey = searchKeyWord + '%';        
        List < sObject > returnList = new List < sObject > (); 
        list<ProductWrap> prowraplist = new list<ProductWrap>();     
        // Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5  
        if(objectName!='Product2'){        
           
        }
        else{
            map<id,product2> productmap = new map<id,product2>();
            list<Product2> productlist=[select id,Name,Unit_Measure__c,Description from Product2 where name like: searchKey order by createdDate DESC limit 5];
            for(product2 product : productlist){
                productmap.put(product.id,product);
            }
            set<id> product2ids = new set<id>();
            list<PricebookEntry> entries =[SELECT Id,Name,Product2.Id,Product2.Name,UnitPrice,Product2.Description  FROM Pricebookentry 
                                                WHERE PriceBook2.IsStandard=true AND Product2.Id IN: productmap.keyset()];
            for(pricebookentry entry : entries){
                ProductWrap prowrap = new ProductWrap();
                if(productmap.containskey(entry.Product2.Id)){
                    product2ids.add(entry.Product2.Id); 
                    prowrap.product =productmap.get(entry.Product2.Id);
                }
                prowrap.UnitPrice= entry.UnitPrice;
                prowraplist.add(prowrap);            
            }
            
            for(id productrecid : productmap.keyset()){
                if(!product2ids.contains(productrecid)){
                    ProductWrap prowrap = new ProductWrap();
                    prowrap.product =  productmap.get(productrecid);
                   // prowrap.UnitPrice=0;  
                    prowraplist.add(prowrap);   
                }
            }                                    
        }
        
        return prowraplist;
    }
    
    public class ProductWrap{
        @auraEnabled
        public product2 product;
        @auraEnabled
        public Decimal UnitPrice;
    
    }

}