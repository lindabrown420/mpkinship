@IsTest
public class CloseCABatchTest {
    public static testmethod void closeCAmethod(){
        List<Opportunity> OppList= new List<Opportunity>();
        Id CARecTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Case Action').getRecordTypeId();
        
        for(Integer i=0 ;i <200;i++)
        {
            Opportunity opprecord = new Opportunity();
            opprecord.Name ='Name'+i;
            opprecord.CloseDate=date.today();
            opprecord.StageName='Open';
            opprecord.Amount=20000;
            opprecord.End_Date__c=Date.today();
            //opprecord.npe01__Do_Not_Automatically_Create_Payment__c=true;
            opprecord.RecordTypeId=CARecTypeId;
            OppList.add(opprecord);
        }
        
        insert OppList;
        Test.startTest();

            CloseCABatch obj = new CloseCABatch();
            DataBase.executeBatch(obj); 
            
        Test.stopTest();
    }
}