public class AddServiceNamesToLaserCodesCtr{

    @auraEnabled
    public static wrapclass getLaserAccountCodes(string recordid){
        list<Category__c> catlist = new list<Category__c>();
        if(recordid!='' && recordid!=null){ 
             catlist =[select id,Service_Name_Code__c,LASER_Account_Code__c,Category_Type__c,LASER_Account_Code__r.name,Service_Name_Code_Value__c,LASER_Cost_Code__c from Category__c where id=:recordid];
             wrapclass wrap = new wrapclass(); 
            if(catlist.size()>0  && catlist[0].LASER_Cost_Code__c!=null){
                
                wrap.category = catlist[0];
                wrap.laseraccountcodeid = catlist[0].LASER_Account_Code__c;
                wrap.laseraccountcodeName= catlist[0].LASER_Account_Code__r.name;
                list<option> laseraccountcodes = new list<option>();  
                list<LASER_Account_Codes__c> laseraccountcodesList =[select id,Name from LASER_Account_Codes__c where Included_for_IVE_Categories__c=:true
                                                                    and Inactive__c=:false];                
                 system.debug('**Val of laseraccountcodeslist'+ laseraccountcodesList);      
                 for( LASER_Account_Codes__c la :  laseraccountcodesList ){
                     option op = new option();
                     op.label = la.name;
                     op.value = la.id;
                     laseraccountcodes.add(op);
                 }
                 
                 if(laseraccountcodes.size()>0)
                     wrap.LaserOptions= laseraccountcodes;       
                 return wrap;    
            }
            else if(catlist.size()>0  && catlist[0].LASER_Cost_Code__c==null)
                wrap.message='Laser Code is null';
                return wrap;
        }
        return null;   
    }


   /* @auraEnabled
    public static wrapclass getServiceName(string recordid){
        list<Category__c> catlist = new list<Category__c>();
        if(recordid!='' && recordid!=null){
            catlist =[select id,Service_Name_Code__c,Service_Name_Code_Value__c,LASER_Cost_Code__c from Category__c where id=:recordid];
            if(catlist.size()>0  && catlist[0].LASER_Cost_Code__c!=null){
                wrapclass wrap = new wrapclass();   
                wrap.category = catlist[0];
                wrap.ServiceNameCode = catlist[0].Service_Name_Code__c;
                wrap.ServiceNameValue = catlist[0].Service_Name_Code_Value__c;
                
                list<option> servicedesirednames = new list<option>();
                //QUERY ON SERVICE NAME CODES
                list<Laser_Codes_and_Service_Names__c> LaserCodesandServiceNames =[select id,LASER_Cost_Code__c,Service_Names_Code__r.name,
                                                Service_Names_Code__r.Service_Name_Code__c from Laser_Codes_and_Service_Names__c        
                                                where LASER_Cost_Code__c=:catlist[0].LASER_Cost_Code__c and Service_Names_Code__c!=null];
                
                for(Laser_Codes_and_Service_Names__c lcsn : LaserCodesandServiceNames){
                     option optionrec = new option();
                     optionrec.label = lcsn.Service_Names_Code__r.name;
                     optionrec.value = lcsn.Service_Names_Code__r.Service_Name_Code__c;
                     servicedesirednames.add(optionrec);
                }
                
                if(servicedesirednames.size()>0)
                    wrap.ServiceNamesOptions = servicedesirednames;
                
                return wrap;    
            }  
            else if(catlist.size()>0 && catlist[0].LASER_Cost_Code__c ==null){
                wrapclass wraprec = new wrapclass();
                wraprec.category= catlist[0];
                wraprec.message='Laser Code is null';
                return wraprec;
            }
        }
        return null;
    } */
    
    @auraEnabled
    public static boolean UpdateLaserAccountCode(String recordid,string laseraccountcode,string laseraccountCodeLabel){
        
        boolean response=false;
        string msg='';
        try{
         
             list<category__c> catlist = new list<category__c>();
             list<category__c> category=[select id,Service_Name_Code__c,LASER_Account_Code__c,LASER_Cost_Code__c from Category__c
                                          where id=:recordid];
             if(Category.size()>0){
                 list<Service_Names_Code__c> serviceNameCodes=new list<Service_Names_Code__c>();
                 if(laseraccountcode!='' && laseraccountcode!=null)
                     serviceNameCodes= [select id from Service_Names_Code__c where LASER_Account_Code__c=:laseraccountcode];
                 category__c categoryrec = new category__c();
                 categoryrec.id = category[0].id;
                if(laseraccountcode!='' && laseraccountcode!=null)
                     categoryrec.LASER_Account_Code__c=laseraccountcode;
                 else{
                     categoryrec.LASER_Account_Code__c=null;
                     categoryrec.Service_Names_Code__c = null;
                 }  
                 if(serviceNameCodes.size()>0)
                     categoryrec.Service_Names_Code__c =serviceNameCodes[0].id;
                 catlist.add(categoryrec);    
             }
              if(catlist.size()>0){
                 update catlist;
                 response=true;
                 return response;
             } 
         }
         catch(DmlException  e){
         
            for (Integer i = 0; i < e.getNumDml(); i++) {
                //Get Validation Rule & Trigger Error Messages
                msg =+ e.getDmlMessage(i) +  '\n' ;
            }
            //throw DML exception message
            if(!Test.isRunningTest())
                throw new AuraHandledException(msg);
            else
                return null;
        }catch(Exception e){
            system.debug('***Exception e'+e.getStackTraceString());
            
            //throw all other exception message
            if(!Test.isRunningTest())
                throw new AuraHandledException(e.getMessage());
            else
                return null;
        }
       return response; 
         
    }
    @auraEnabled
    public static boolean UpdateCategory(string recordid, string servicename, string servicelabel){
         boolean response=false;
         list<category__c> catlist = new list<category__c>();
         list<category__c> category=[select id,Service_Name_Code__c,Service_Name_Code_Value__c,LASER_Cost_Code__c from Category__c
                                      where id=:recordid];
         if(category.size()>0){
             category__c categoryrec = new category__c();
             categoryrec.id = category[0].id;
             categoryrec.Service_Name_Code__c = servicelabel;
             categoryrec.Service_Name_Code_Value__c = servicename;
             catlist.add(categoryrec);    
         }  
         if(catlist.size()>0){
             update catlist;
             response=true;
         } 
         
         return response;                         
    }
    
    public class wrapclass{
        @auraEnabled
        public Category__c category;   
        @auraEnabled
        public string message;
        @auraEnabled
        public string laseraccountcodeid;
        @auraEnabled 
        public string laseraccountcodeName;
        @auraEnabled
        public list<option>  LaserOptions;
    }
    
   
     public class option{
        @auraEnabled
        public string label;
        @auraEnabled
        public string value;
     }
}