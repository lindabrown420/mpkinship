@isTest
public class OpportunityNintexCtrTest {

    @isTest static void OpportunityNintexCtrTestMethod() {
        
        test.startTest();
        Account testAcct = new Account(Name = 'My Test Account', Tax_ID__c = '123-56-7901', Entity_Type__c = 'Individual');
        insert testAcct;

    // Creates first opportunity
    Opportunity oppt = new Opportunity(Name ='New mAWS Deal',
                            AccountID = testAcct.ID,
                            StageName = 'Customer Won',
                            Amount = 3000,
                            CloseDate = System.today()
                            );

   insert oppt;

        
        PageReference myVfPage = Page.Report_Collection;
		Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('id',oppt.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(oppt);
        
    	OpportunityNintexCtr controller = new OpportunityNintexCtr(sc);
        
        OpportunityNintexCtr.getloopparameter();
        test.stoptest();
    }
}