@isTest
public class CaseActionPaymentChangeTest {
    public static testmethod void test1(){
        Id LEDRSRecTypeId = Schema.SObjectType.category__C.getRecordTypeInfosByDeveloperName().get('LEDRS_Category').getRecordTypeId();
         Locality_Account__c la = new Locality_Account__c();
        la.Name='222';
        insert la;
        Category__c category = new Category__c();
        category.Name='222';
        category.Locality_Account__c=la.id;
        category.Category_Type__c='CSA';
        category.RecordTypeId=LEDRSRecTypeId;
        insert category;
         contact con = new contact();
        //con.recordtypeid=recType;
        con.LastName='contactrec';
        con.FirstName='test';
        con.Email='test@contact.com';
        //con.Race_of_the_Foster_Parent__c='White';
        //con.Gender_Preference__c='Male';
        insert con;
        FIPS__c fp = new FIPS__c();
        fp.Is_Default__c=true;
        fp.Name='test fips';
        insert fp;
        campaign camp = new campaign();
        camp.recordtypeid= Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Fiscal_Year').getRecordTypeId();
        camp.Fiscal_Year_Type__c='Local';
        camp.StartDate=system.today().addmonths(-1);
        camp.Name='test camp';
        camp.FIPS__c = fp.id;
        camp.EndDate =system.today().addmonths(12);
        insert camp;
        
        account accountrec = new account();
        accountrec.Name='test vendor';
        accountrec.Tax_ID__c='333-33-3333';
        accountrec.Entity_Type__c='Individual';
        insert accountrec;
        
        Kinship_Case__c caserec = new Kinship_Case__c();
        caserec.Name='test';
        caserec.Primary_Contact__c=con.id;
        caserec.Hispanic_Ethnicity__c='1';
        caserec.DSM_V_Indicator_ICD_10_1__c='1';
        caserec.Gender_Preference__c='M';
        caserec.Race__c='1';
        caserec.Autism_Flag_1__c='2';
        caserec.DSM_V_Indicator_ICD_10_1__c='2';
        caserec.Clinical_Medication_Indicator_1__c='2';
        caserec.Medicaid_Indicator_1__c='2';
        caserec.Referral_Source__c='1';
        caserec.DOB__c=date.today().addYears(-10);
        insert caserec;
         Id CARecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Case_Actions').getRecordTypeId();
        opportunity opprec = new opportunity();
        opprec.RecordTypeId=CARecordTypeId;
        opprec.AccountId=accountrec.id;
        opprec.Kinship_Case__c=caserec.id;
        opprec.Name='test';
        opprec.Parent_Recipient__c='1';
        opprec.CampaignId=camp.id;
        opprec.Category__c=category.id;
        opprec.amount=2000;
        opprec.Type_of_Case_Action__c='One time';
        opprec.CloseDate=date.today().adddays(1);
        opprec.StageName='Draft';
        opprec.End_Date__c=date.today().addmonths(3);
        opprec.Number_of_Months_Units__c=4;
        insert opprec;
        Paymentrecordhelper.isOpportunity=false;
         Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitrequest();
        app.setObjectId(opprec.Id);    
         // Submit the approval request for the po    
        Approval.ProcessResult result = Approval.process(app);    
        // Verify that the results are as expected    
        System.assert(result.isSuccess());
      
        if(result.isSuccess()){

            opprec.stagename='Approved';
            update opprec;

        }  
        opprec.stagename='Open';
        update opprec;
        list<opportunity> opp=[select id,Number_of_Months_Units__c,(Select id from Payment_OppPayment__r),Amount,stagename,recordtypeid,recordtype.developername,Invoice_Stage__c
                              from opportunity where id=:opprec.id];
        system.debug('***VAl ofopp'+opp);
        payment__C pay = new payment__c();
        pay.opportunity__C=opprec.id;
        pay.Scheduled_Date__c=date.today().adddays(1);
        pay.Invoice_Stage__c='Scheduled';
        pay.Paid__c=false;
        pay.Written_Off__c=false;
        insert pay;
        	
        Test.startTest();
            CaseActionPaymentChange obj = new CaseActionPaymentChange();
            DataBase.executeBatch(obj); 
            
        Test.stopTest();
    } 
}