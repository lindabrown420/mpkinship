public with sharing class caseAssignmentController {
    @AuraEnabled
    public static Integer checkDuplicate(String employeeId, String caseId, String role){
        List<Case_Assignment__c> caList = [Select id,Case__c
                                           FROM Case_Assignment__c
                                           WHERE Employee__c =: employeeId
                                           AND Case__c =: caseId
                                           AND Role__c =: role
                                           AND End_Date__c = null];

        return caList.size();
    }

    /*@AuraEnabled
    public static List<Case_Assignment__c> getOwnerHistory(String caseId){
        List<Case_Assignment__c> caList = [Select id,Employee__c,End_Date__c,Begin_Date__c,Employee__r.Name
                                           FROM Case_Assignment__c
                                           WHERE Case__c =: caseId
                                           AND Role__c = 'Case Owner'
                                           Order by Begin_Date__c desc];

        return caList;
    }*/
}