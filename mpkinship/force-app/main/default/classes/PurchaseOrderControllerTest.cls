@isTest
public class PurchaseOrderControllerTest {
     @testSetup
    static void testecords(){
        contact con = new contact();
        con.LastName='contactrec';
        con.FirstName='test';
        con.Email='test@contact.com';       
        insert con;
        Campaign parent = new Campaign();
        parent.name = '2021';
        parent.StartDate = Date.Today().addDays(-10);
        parent.EndDate = Date.Today().addDays(10);
        parent.Fiscal_Year_Type__c = 'Local';
        parent.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId();
        insert parent;
        Kinship_Case__c caserec = new Kinship_Case__c();
        caserec.Primary_Contact__c=con.id;
        caserec.DOB__c=system.today().adddays(-100);
        caserec.Race__c='1';
        caserec.Gender_Preference__c='M';
        caserec.Hispanic_Ethnicity__c='1';
        caserec.DSM_V_Indicator_ICD_10_1__c='1';
        caserec.Clinical_Medication_Indicator_1__c='1';
        caserec.Medicaid_Indicator_1__c='1';
        caserec.Referral_Source__c='1';
        caserec.Autism_Flag_1__c='1';
        insert caserec;
        
        id accountrecordtypeid =Schema.SObjectType.account.getRecordTypeInfosByDeveloperName().get('Vendor').getRecordTypeId();
        account accountrec = new account();
        accountrec.Name='test vendor';
        accountrec.Tax_ID__c='333-33-3333';
        accountrec.recordtypeid=accountrecordtypeid;
        accountrec.Entity_Type__c='Individual';
        insert accountrec;
        
        Locality_Account__c la = new Locality_Account__c();
        la.Name='222';
        insert la;
        
        Category__c category = new Category__c();
        category.Name='222';
        category.Locality_Account__c=la.id;
        category.Category_Type__c='CSA';
        category.RecordTypeId= Schema.SObjectType.category__C.getRecordTypeInfosByDeveloperName().get('LEDRS_Category').getRecordTypeId();
        insert category;
        
        Category_Rate__c cr = new Category_Rate__c();
        cr.Category__c = category.id;
        cr.Status__c='Active';
        cr.Price__c=5000;
        cr.Effective_Date__c=system.today();
        cr.RecordTypeId = Schema.SObjectType.Category_Rate__c.getRecordTypeInfosByDeveloperName().get('Other_Rate').getRecordTypeId();
        insert cr;
        
    } 
    public static testmethod void test1(){
        id recordid =  Schema.SObjectType.opportunity.getRecordTypeInfosByDeveloperName().get('Purchase_Order').getRecordTypeId();
       system.debug('****recordid'+recordid);
        PurchaseOrderController.getRecordType(recordid);
         contact con1 = new contact();
        con1.LastName='contactrec two three';
        con1.FirstName='test';
        con1.Email='test@contact.com';       
        insert con1;
         Kinship_Case__c caserec = new Kinship_Case__c();
        caserec.Primary_Contact__c=con1.id;
        caserec.DOB__c=system.today().adddays(-100);
        caserec.Race__c='1';
        caserec.Gender_Preference__c='M';
        caserec.Hispanic_Ethnicity__c='1';
        caserec.DSM_V_Indicator_ICD_10_1__c='1';
        caserec.Clinical_Medication_Indicator_1__c='1';
        caserec.Medicaid_Indicator_1__c='1';
        caserec.Referral_Source__c='1';
        caserec.Autism_Flag_1__c='1';
        insert caserec;
         id accountrecordtypeid =Schema.SObjectType.account.getRecordTypeInfosByDeveloperName().get('Vendor').getRecordTypeId();
        account accrec = new account();
        accrec.Name='testing vendor22';
        accrec.Tax_ID__c='63-33-323';
        accrec.recordtypeid=accountrecordtypeid;
        insert accrec;
         string casewrapper='{"contactFirstName" :"test","contactLastName":"testing","DOB":"2021-06-14","HispanicEthnicity":"2","Gender":"F","Race":"1","OasisClinetId":"null","DSM":"No","Medical":"No","Referal":"","Clinical":"No","Autism":"No","titleIVE":""}';
          PurchaseOrderController.UpdateCase(caserec.id,casewrapper,accrec.id);
         // PurchaseOrderController.UpdateCase(caserec.id,casewrapper);
        
    }
    
    public static testmethod void testFetchInitialValues(){
         Profile p = [SELECT Id FROM Profile WHERE Name='Case Worker']; 
        User u = new User(Alias = 'standt', Email='standarduser112@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser232@testorg.com');

        System.runAs(u) {  
            FIPS__c fp = new FIPS__c();
            fp.Is_Default__c=true;
            fp.Name='test fips';
            insert fp;
            campaign camp = new campaign();
            camp.recordtypeid= Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Fiscal_Year').getRecordTypeId();
            camp.Fiscal_Year_Type__c='Local';
            camp.StartDate=system.today().addmonths(-1);
            camp.Name='test camp';
            camp.FIPS__c = fp.id;
            camp.EndDate =system.today().addmonths(12);
            insert camp;
            PurchaseOrderController.FetchInitialValues();
            purchaseOrderController.FetchBeginDate(camp.id);
        }    
    }
    
    public static testmethod void test2(){      
         
            FIPS__c fp = new FIPS__c();
            fp.Is_Default__c=true;
            fp.Name='test fips';
            insert fp;
            campaign camp = new campaign();
            camp.recordtypeid= Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Fiscal_Year').getRecordTypeId();
            camp.Fiscal_Year_Type__c='Local';
            camp.StartDate=system.today().addmonths(-1);
            camp.Name='test camp';
            camp.FIPS__c = fp.id;
            camp.EndDate =system.today().addmonths(12);
            insert camp;
            list<Account> acclist=[select id,name,recordtypeid from account];
            pricebook2 pricebookrec = new pricebook2();
            pricebookrec.name='test name';
            pricebookrec.Accounting_Period__c =camp.id;
            pricebookrec.vendor__c=acclist[0].id;
            insert pricebookrec;
            PurchaseOrderController.getPricebook(camp.id, acclist[0].id);
       
    }
    public static testmethod void test3(){      
            FIPS__c fp = new FIPS__c();
            fp.Is_Default__c=true;
            fp.Name='test fips';
            insert fp;
            campaign camp = new campaign();
            camp.recordtypeid= Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Fiscal_Year').getRecordTypeId();
            camp.Fiscal_Year_Type__c='Local';
            camp.StartDate=system.today().addmonths(-1);
            camp.Name='test camp';
            camp.FIPS__c = fp.id;
            camp.EndDate =system.today().addmonths(12);
            insert camp;
            list<Account> acclist=[select id,name,recordtypeid from account where name='test vendor'];
           
        Product2 p = new product2(name='x',isactive=true, Unit_Measure__c = '10');
        insert p;
        
        Product2 p2 = new product2(name='y',isactive=true, Unit_Measure__c = '10');
        insert p2;
        
        //define the standart price for the product
        id stdPb = Test.getStandardPricebookId();
        insert new PricebookEntry(pricebook2id = stdPb, product2id = p.id,unitprice=1.0, isActive=true);
        
        Pricebook2 pb = new pricebook2(name='test',Vendor__c=acclist[0].id,IsActive=true, Accounting_Period__c = camp.id);
        insert pb;
        
        PricebookEntry pbe = new PricebookEntry(pricebook2id=pb.id, product2id=p.id,unitprice=1.0, isActive=true);
        insert pbe;   
        PurchaseOrderController.getPricebook(camp.id, acclist[0].id);
       
    }
   
    public static testmethod void test4(){
        FIPS__c fp = new FIPS__c();
        fp.Is_Default__c=true;
        fp.Name='test fips';
        insert fp;
        campaign camp = new campaign();
        camp.recordtypeid= Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Fiscal_Year').getRecordTypeId();
        camp.Fiscal_Year_Type__c='Local';
        camp.StartDate=system.today().addmonths(-1);
        camp.Name='test camp';
        camp.FIPS__c = fp.id;
        camp.EndDate =system.today().addmonths(12);
        insert camp;
        list<Account> acclist=[select id,name,recordtypeid from account where name='test vendor'];
           
        Product2 p = new product2(name='x',isactive=true, Unit_Measure__c = '10');
        insert p;
        
        Product2 p2 = new product2(name='y',isactive=true, Unit_Measure__c = '10');
        insert p2;
        
        //define the standart price for the product
        id stdPb = Test.getStandardPricebookId();
        insert new PricebookEntry(pricebook2id = stdPb, product2id = p.id,unitprice=1.0, isActive=true);
        
        Pricebook2 pb = new pricebook2(name='test',Vendor__c=acclist[0].id,IsActive=true, Accounting_Period__c = camp.id);
        insert pb;
        
        PricebookEntry pbe = new PricebookEntry(pricebook2id=pb.id, product2id=p.id,unitprice=1.0, isActive=true);
        insert pbe;   
        opportunity opp1 = new opportunity();
        opp1.Kinship_Case__c= [select id,name from kinship_case__C].id;
        opp1.accountid=acclist[0].id;
        opp1.CampaignId=camp.id;
        opp1.closedate=system.today();
        opp1.End_Date__c=system.today().adddays(6);
        opp1.pricebook2id = pb.id;
        list<kinship_Case__c> cases=[select id,name from kinship_case__C];
        list<Category__c> category=[select id,name,recordtypeid from category__c];
        id cc = category[0].id;
        PurchaseOrderController.getCase(cases[0].id,acclist[0].id);  
       // PurchaseOrderController.getCase(cases[0].id);     
        string oppstring = '{"Opp":{"Kinship_Case__c":"'+cases[0].Id+'","CampaignId":"'+camp.Id+'","accountid":"'+acclist[0].Id+'","closedate":"2021-06-14","End_Date__c":"2021-10-14"},"pricebookid":"'+pb.id+'","oppproduct":[{"Product2Id":"'+p.id+'","unitLabel":"At Cost","unitMeasure":"11","UnitPrice":"200","quantity":"2","pricebookentryid":"'+pbe.id+'","categoryid":"'+cc+'","recipient":"1","SPTNameselected":"2","SPTselected":"1","MandateSelected":"4","SPTselectedlabel":"test","SPTNameselectedlabel":"test sname","MandateSelectedlabel":"test mandate","Description":"test"}]}';
        PurchaseOrderController.CreatePO('',oppstring,'System Administrator',true,false,'',false);
        PurchaseOrderController.CreatePOwithNote('',new ContentNote(title='test') ,oppstring,'System Administrator',true,false,'',false);
      
    }
    public static testmethod void test5(){
        FIPS__c fp = new FIPS__c();
        fp.Is_Default__c=true;
        fp.Name='test fips';
        insert fp;
        campaign camp = new campaign();
        camp.recordtypeid= Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Fiscal_Year').getRecordTypeId();
        camp.Fiscal_Year_Type__c='Local';
        camp.StartDate=system.today().addmonths(-1);
        camp.Name='test camp';
        camp.FIPS__c = fp.id;
        camp.EndDate =system.today().addmonths(12);
        insert camp;
        list<Account> acclist=[select id,name,recordtypeid from account where name='test vendor'];
           
        Product2 p = new product2(name='x',isactive=true, Unit_Measure__c = '10');
        insert p;
        
        Product2 p2 = new product2(name='y',isactive=true, Unit_Measure__c = '10');
        insert p2;
        
        //define the standart price for the product
        id stdPb = Test.getStandardPricebookId();
        insert new PricebookEntry(pricebook2id = stdPb, product2id = p.id,unitprice=1.0, isActive=true);
        
        Pricebook2 pb = new pricebook2(name='test',Vendor__c=acclist[0].id,IsActive=true, Accounting_Period__c = camp.id);
        insert pb;
        
        PricebookEntry pbe = new PricebookEntry(pricebook2id=pb.id, product2id=p.id,unitprice=1.0, isActive=true);
        insert pbe;   
        opportunity opp1 = new opportunity();
        opp1.Kinship_Case__c= [select id,name from kinship_case__C].id;
        opp1.accountid=acclist[0].id;
        opp1.CampaignId=camp.id;
        
        opp1.pricebook2id = pb.id;
        list<kinship_Case__c> cases=[select id,name from kinship_case__C];
        list<Category__c> category=[select id,name,recordtypeid from category__c];
        id cc = category[0].id;
               
        string oppstring = '{"Opp":{"Kinship_Case__c":"'+cases[0].Id+'","CampaignId":"'+camp.Id+'","accountid":"'+acclist[0].Id+'","closedate":"2021-06-14","End_Date__c":"2021-10-14"},"pricebookid":"'+pb.id+'","oppproduct":[{"Product2Id":"'+p.id+'","unitLabel":"At Cost","unitMeasure":"11","UnitPrice":"200","quantity":"2","pricebookentryid":"'+pbe.id+'","categoryid":""}]}';
        PurchaseOrderController.CreatePO('1122aa',oppstring,'System Administrator',true,false,'',false);
        PurchaseOrderController.CreatePOwithNote('1122aa',new ContentNote(title='test') ,oppstring,'System Administrator',true,false,'',false);

      
    }
    public static testmethod void test6(){
        FIPS__c fp = new FIPS__c();
        fp.Is_Default__c=true;
        fp.Name='test fips';
        insert fp;
        campaign camp = new campaign();
        camp.recordtypeid= Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Fiscal_Year').getRecordTypeId();
        camp.Fiscal_Year_Type__c='Local';
        camp.StartDate=system.today().addmonths(-1);
        camp.Name='test camp';
        camp.FIPS__c = fp.id;
        camp.EndDate =system.today().addmonths(12);
        insert camp;
        list<Account> acclist=[select id,name,recordtypeid from account];
           
        Product2 p = new product2(name='x',isactive=true, Unit_Measure__c = '10');
        insert p;
        
        Product2 p2 = new product2(name='y',isactive=true, Unit_Measure__c = '10');
        insert p2;
        
        //define the standart price for the product
        id stdPb = Test.getStandardPricebookId();
        insert new PricebookEntry(pricebook2id = stdPb, product2id = p.id,unitprice=1.0, isActive=true);
        
        Pricebook2 pb = new pricebook2(name='test',Vendor__c=acclist[0].id,IsActive=true, Accounting_Period__c = camp.id);
        insert pb;
        
        PricebookEntry pbe = new PricebookEntry(pricebook2id=pb.id, product2id=p.id,unitprice=1.0, isActive=true);
        insert pbe;   
        opportunity opp1 = new opportunity();
        opp1.Kinship_Case__c= [select id,name from kinship_case__C].id;
        opp1.accountid=acclist[0].id;
        opp1.CampaignId=camp.id;
        
        opp1.pricebook2id = pb.id;
        list<kinship_Case__c> cases=[select id,name from kinship_case__C];
        list<Category__c> category=[select id,name,recordtypeid from category__c];
        id cc = category[0].id;
               
        string oppstring = '{"Opp":{"Kinship_Case__c":"'+cases[0].Id+'","CampaignId":"'+camp.Id+'","accountid":"null","closedate":"2021-06-14","End_Date__c":"2021-10-14"},"pricebookid":"'+pb.id+'","oppproduct":[{"Product2Id":"'+p.id+'","unitLabel":"At Cost","unitMeasure":"11","UnitPrice":"200","quantity":"2","pricebookentryid":"'+pbe.id+'","categoryid":"'+cc+'"}]}';
        PurchaseOrderController.CreatePO('',oppstring,'System Administrator',true,false,'',false);
        PurchaseOrderController.CreatePOwithNote('',new ContentNote(title='test') ,oppstring,'System Administrator',true,false,'',false);
      
    }
     public static testmethod void test10(){
        FIPS__c fp = new FIPS__c();
        fp.Is_Default__c=true;
        fp.Name='test fips';
        insert fp;
        campaign camp = new campaign();
        camp.recordtypeid= Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Fiscal_Year').getRecordTypeId();
        camp.Fiscal_Year_Type__c='Local';
        camp.StartDate=system.today().addmonths(-1);
        camp.Name='test camp';
        camp.FIPS__c = fp.id;
        camp.EndDate =system.today().addmonths(12);
        insert camp;
        list<Account> acclist=[select id,name,recordtypeid from account where name='test vendor'];
           
        Product2 p = new product2(name='x',isactive=true, Unit_Measure__c = '10');
        insert p;
        
        Product2 p2 = new product2(name='y',isactive=true, Unit_Measure__c = '10');
        insert p2;
        
        //define the standart price for the product
        id stdPb = Test.getStandardPricebookId();
        insert new PricebookEntry(pricebook2id = stdPb, product2id = p.id,unitprice=1.0, isActive=true);
        
        Pricebook2 pb = new pricebook2(name='test',Vendor__c=acclist[0].id,IsActive=true, Accounting_Period__c = camp.id);
        insert pb;
        
        PricebookEntry pbe = new PricebookEntry(pricebook2id=pb.id, product2id=p.id,unitprice=1.0, isActive=true);
        insert pbe;   
        opportunity opp1 = new opportunity();
        opp1.Kinship_Case__c= [select id,name from kinship_case__C].id;
        opp1.accountid=acclist[0].id;
        opp1.CampaignId=camp.id;
        opp1.closedate=system.today();
        opp1.End_Date__c=system.today().adddays(6);
        opp1.pricebook2id = pb.id;
        list<kinship_Case__c> cases=[select id,name from kinship_case__C];
        list<Category__c> category=[select id,name,recordtypeid from category__c];
        id cc = category[0].id;
        PurchaseOrderController.getCase(cases[0].id,acclist[0].id);       
        //PurchaseOrderController.getCase(cases[0].id);  
        string oppstring = '{"Opp":{"Kinship_Case__c":"'+cases[0].Id+'","CampaignId":"'+camp.Id+'","accountid":"'+acclist[0].Id+'","closedate":"2021-06-14","End_Date__c":"2021-10-14"},"pricebookid":"","oppproduct":[{"IsNewService":"true",ProductSelectedId":"'+p.id+'","unitLabel":"At Cost","unitMeasure":"11","ProductSelectedUnitPrice":"200","UnitPrice":"400",quantity":"2","categoryid":"'+cc+'","recipient":"1","ServiceName":"testing","ServiceDescription":"","SPTNameselected":"2","SPTselected":"1","MandateSelected":"4","SPTselectedlabel":"test","SPTNameselectedlabel":"test sname","MandateSelectedlabel":"test mandate","Description":"test"}]}';     
         PurchaseOrderController.CreatePO('',oppstring,'System Administrator',true,false,'',false);
        PurchaseOrderController.CreatePOwithNote('',new ContentNote(title='test') ,oppstring,'System Administrator',true,false,'',false);
      
    }

     public static testmethod void test11(){
        FIPS__c fp = new FIPS__c();
        fp.Is_Default__c=true;
        fp.Name='test fips';
        insert fp;
        campaign camp = new campaign();
        camp.recordtypeid= Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Fiscal_Year').getRecordTypeId();
        camp.Fiscal_Year_Type__c='Local';
        camp.StartDate=system.today().addmonths(-1);
        camp.Name='test camp';
        camp.FIPS__c = fp.id;
        camp.EndDate =system.today().addmonths(12);
        insert camp;
        list<Account> acclist=[select id,name,recordtypeid from account where name='test vendor'];
           
        Product2 p = new product2(name='x',isactive=true, Unit_Measure__c = '10');
        insert p;
        
        Product2 p2 = new product2(name='y',isactive=true, Unit_Measure__c = '10');
        insert p2;
        
        //define the standart price for the product
        id stdPb = Test.getStandardPricebookId();
         //system.debug('**VAl of stdpb'+stdPb);
         Pricebook2 standardPricebook = new Pricebook2(Id = Test.getStandardPricebookId(),Vendor__c=acclist[0].id,IsActive = true,Accounting_Period__c=camp.id);

        //Execute an update DML on the Pricebook2 record, to make IsStandard to true

        Update standardPricebook;
         list<pricebook2> pricebookstd =[select id,isstandard from pricebook2 where id=:standardPricebook.id];
         system.debug('***Stdpricbeook'+pricebookstd);
        insert new PricebookEntry(pricebook2id = stdPb, product2id = p.id,unitprice=1.0, isActive=true);
        
        Pricebook2 pb = new pricebook2(name='test',Vendor__c=acclist[0].id,IsActive=true, Accounting_Period__c = camp.id);
        insert pb;
        
        PricebookEntry pbe = new PricebookEntry(pricebook2id=pb.id, product2id=p.id,unitprice=1.0, isActive=true);
        insert pbe;   
        opportunity opp1 = new opportunity();
        opp1.Kinship_Case__c= [select id,name from kinship_case__C].id;
        opp1.accountid=acclist[0].id;
        opp1.CampaignId=camp.id;
        opp1.closedate=system.today();
        opp1.End_Date__c=system.today().adddays(6);
        opp1.pricebook2id = pb.id;
        list<kinship_Case__c> cases=[select id,name from kinship_case__C];
        list<Category__c> category=[select id,name,recordtypeid from category__c];
        id cc = category[0].id;
        PurchaseOrderController.getCase(cases[0].id,acclist[0].id); 
        //PurchaseOrderController.getCase(cases[0].id); 
         boolean istrue=true;
         
         
        string oppstring = '{"Opp":{"Kinship_Case__c":"'+cases[0].Id+'","CampaignId":"'+camp.Id+'","accountid":"'+acclist[0].Id+'","closedate":"2021-06-14","End_Date__c":"2021-10-14"},"oppproduct":[{"IsNewService":true,"unitLabel":"At Cost","unitMeasure":"11","ProductSelectedUnitPrice":"200","UnitPrice":"400","quantity":"2","categoryid":"'+cc+'","recipient":"1","ServiceName":"testing","ServiceDescription":"","SPTNameselected":"2","SPTselected":"1","MandateSelected":"4","SPTselectedlabel":"test","SPTNameselectedlabel":"test sname","MandateSelectedlabel":"test mandate","Description":"test"}]}';     
         PurchaseOrderController.CreatePO('',oppstring,'System Administrator',true,false,'',false);
        PurchaseOrderController.CreatePOwithNote('',new ContentNote(title='test') ,oppstring,'System Administrator',true,false,'',false);
      
    }
    
    
     public static testmethod void test12(){
        FIPS__c fp = new FIPS__c();
        fp.Is_Default__c=true;
        fp.Name='test fips';
        insert fp;
        campaign camp = new campaign();
        camp.recordtypeid= Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Fiscal_Year').getRecordTypeId();
        camp.Fiscal_Year_Type__c='Local';
        camp.StartDate=system.today().addmonths(-1);
        camp.Name='test camp';
        camp.FIPS__c = fp.id;
        camp.EndDate =system.today().addmonths(12);
        insert camp;
        list<Account> acclist=[select id,name,recordtypeid from account where name='test vendor'];
           
        Product2 p = new product2(name='x',isactive=true, Unit_Measure__c = '10');
        insert p;
        
        Product2 p2 = new product2(name='y',isactive=true, Unit_Measure__c = '10');
        insert p2;
        
        //define the standart price for the product
        id stdPb = Test.getStandardPricebookId();
         //system.debug('**VAl of stdpb'+stdPb);
         Pricebook2 standardPricebook = new Pricebook2(Id = Test.getStandardPricebookId(),Vendor__c=acclist[0].id,IsActive = true,Accounting_Period__c=camp.id);

        //Execute an update DML on the Pricebook2 record, to make IsStandard to true

        Update standardPricebook;
         list<pricebook2> pricebookstd =[select id,isstandard from pricebook2 where id=:standardPricebook.id];
         system.debug('***Stdpricbeook'+pricebookstd);
        insert new PricebookEntry(pricebook2id = stdPb, product2id = p.id,unitprice=1.0, isActive=true);
        
        Pricebook2 pb = new pricebook2(name='test',Vendor__c=acclist[0].id,IsActive=true, Accounting_Period__c = camp.id);
        insert pb;
        
        PricebookEntry pbe = new PricebookEntry(pricebook2id=pb.id, product2id=p.id,unitprice=1.0, isActive=true);
        insert pbe;   
        opportunity opp1 = new opportunity();
        opp1.Kinship_Case__c= [select id,name from kinship_case__C].id;
        opp1.accountid=acclist[0].id;
        opp1.CampaignId=camp.id;
        opp1.closedate=system.today();
        opp1.End_Date__c=system.today().adddays(6);
        opp1.pricebook2id = pb.id;
        list<kinship_Case__c> cases=[select id,name from kinship_case__C];
        list<Category__c> category=[select id,name,recordtypeid from category__c];
        id cc = category[0].id;
        PurchaseOrderController.getCase(cases[0].id,acclist[0].id); 
         //PurchaseOrderController.getCase(cases[0].id);
         boolean istrue=true;
         
         
        string oppstring = '{"Opp":{"Kinship_Case__c":"'+cases[0].Id+'","CampaignId":"'+camp.Id+'","accountid":"'+acclist[0].Id+'","closedate":"2021-06-14","End_Date__c":"2021-10-14"},"oppproduct":[{"IsNewService":true,"unitLabel":"At Cost","unitMeasure":"11","ProductSelectedId":"'+p2.id +'","UnitPrice":"400","quantity":"2","categoryid":"'+cc+'","recipient":"1","ServiceName":"testing","ServiceDescription":"","SPTNameselected":"2","SPTselected":"1","MandateSelected":"4","SPTselectedlabel":"test","SPTNameselectedlabel":"test sname","MandateSelectedlabel":"test mandate","Description":"test"}]}';     
         PurchaseOrderController.CreatePO('',oppstring,'System Administrator',true,false,'',false);
        PurchaseOrderController.CreatePOwithNote('',new ContentNote(title='test') ,oppstring,'System Administrator',true,false,'',false);
      
    }
    public static testmethod void test13(){
        FIPS__c fp = new FIPS__c();
        fp.Is_Default__c=true;
        fp.Name='test fips';
        insert fp;
        campaign camp = new campaign();
        camp.recordtypeid= Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Fiscal_Year').getRecordTypeId();
        camp.Fiscal_Year_Type__c='Local';
        camp.StartDate=system.today().addmonths(-1);
        camp.Name='test camp';
        camp.FIPS__c = fp.id;
        camp.EndDate =system.today().addmonths(12);
        insert camp;
        list<Account> acclist=[select id,name,recordtypeid from account where name='test vendor'];
           
        Product2 p = new product2(name='x',isactive=true, Unit_Measure__c = '10');
        insert p;
        
        Product2 p2 = new product2(name='y',isactive=true, Unit_Measure__c = '10');
        insert p2;
        
        //define the standart price for the product
        id stdPb = Test.getStandardPricebookId();
         //system.debug('**VAl of stdpb'+stdPb);
         Pricebook2 standardPricebook = new Pricebook2(Id = Test.getStandardPricebookId(),Vendor__c=acclist[0].id,IsActive = true,Accounting_Period__c=camp.id);

        //Execute an update DML on the Pricebook2 record, to make IsStandard to true

        Update standardPricebook;
         list<pricebook2> pricebookstd =[select id,isstandard from pricebook2 where id=:standardPricebook.id];
         system.debug('***Stdpricbeook'+pricebookstd);
        insert new PricebookEntry(pricebook2id = stdPb, product2id = p.id,unitprice=1.0, isActive=true);
        
        Pricebook2 pb = new pricebook2(name='test',Vendor__c=acclist[0].id,IsActive=true, Accounting_Period__c = camp.id);
        insert pb;
        
        PricebookEntry pbe = new PricebookEntry(pricebook2id=pb.id, product2id=p.id,unitprice=1.0, isActive=true);
        insert pbe;   
        opportunity opp1 = new opportunity();
        opp1.Kinship_Case__c= [select id,name from kinship_case__C].id;
        opp1.accountid=acclist[0].id;
        opp1.CampaignId=camp.id;
        opp1.closedate=system.today();
        opp1.End_Date__c=system.today().adddays(6);
        opp1.pricebook2id = pb.id;
        list<kinship_Case__c> cases=[select id,name from kinship_case__C];
        list<Category__c> category=[select id,name,recordtypeid from category__c];
        id cc = category[0].id;
        PurchaseOrderController.getCase(cases[0].id,acclist[0].id); 
       //PurchaseOrderController.getCase(cases[0].id);
         boolean istrue=true;
         
         
        string oppstring = '{"Opp":{"Kinship_Case__c":"'+cases[0].Id+'","CampaignId":"'+camp.Id+'","accountid":"'+acclist[0].Id+'","closedate":"2021-06-14","End_Date__c":"2021-10-14"},"pricebookid":"'+pb.id+'","oppproduct":[{"IsNewService":true,"unitLabel":"At Cost","unitMeasure":"11","ProductSelectedId":"'+p2.id +'","UnitPrice":"400","quantity":"2","categoryid":"'+cc+'","recipient":"1","ServiceName":"testing","ServiceDescription":"","SPTNameselected":"2","SPTselected":"1","MandateSelected":"4","SPTselectedlabel":"test","SPTNameselectedlabel":"test sname","MandateSelectedlabel":"test mandate","Description":"test"}]}';       
         PurchaseOrderController.CreatePO('',oppstring,'System Administrator',true,false,'',false);
      
    }
     
}