//FOR CHANING THE UNPAID CASE ACTION PAYMENTS STAGE TO READY TO PAY FROM SCHEDULED IF IT IS WITHIN 30 DAYS RANGE FROM TODAY.
public class CaseActionPaymentChange implements Database.Batchable<sObject> {

      public Database.QueryLocator start(Database.BatchableContext BC) {
          string stageval='Open';
          date todaydate = date.today();
          //KIN-1483
          date thirtyDaysDate =  date.today().adddays(15);
          string InvoiceStage='Scheduled';
          boolean isPaid=false;
          Boolean isWriteOff = false;
          Id CARecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Case_Actions').getRecordTypeId();
          
          string query = 'SELECT Id,Invoice_Stage__c,Opportunity__c,Opportunity__r.StageName,Scheduled_Date__c from Payment__c where Opportunity__r.StageName=:stageval and Paid__c=:isPaid and Written_Off__c=:isWriteOff and Scheduled_Date__c >=: todaydate and Scheduled_Date__c <: thirtyDaysDate and Invoice_Stage__c=:InvoiceStage and Opportunity__r.recordtypeid=:CARecordTypeId ORDER BY Opportunity__c'; 
          return Database.getQueryLocator(query);
      }
      
     public void execute(Database.BatchableContext BC, List<Payment__c > payments) {
        system.debug('**VAl fo payments'+payments);
        if(payments.size()>0)
            CaseActionPaymentChangeHelper.ChangeStage(payments);   
             
    } 
    
    public void finish(Database.BatchableContext BC) {
        // execute any post-processing operations
    }  

}