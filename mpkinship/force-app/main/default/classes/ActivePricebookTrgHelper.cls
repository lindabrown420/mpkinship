public class ActivePricebookTrgHelper{
    public static boolean isRun=false;
    public static boolean isRunUpdate = false;
    
    public static void CheckDuplicatePricebook(list<pricebook2> pricebooks,set<id> vendorids ,set<id> AccountingPeriodIds){
        if(trigger.isInsert)
            isRun=true;
        else if(trigger.isUpdate)
            isRunUpdate=true;     
        set<string> VendorAccountingStrings = new set<String>();
        list<Pricebook2> activepricebooks=[select id,IsActive,Accounting_Period__c,Vendor__c from pricebook2 where
                        Vendor__c in:vendorids and Accounting_Period__c in:AccountingPeriodIds and isactive=:True];
        for(pricebook2 pricebook : activepricebooks){
            if(!VendorAccountingStrings.contains(pricebook.Vendor__c+'-'+pricebook.Accounting_Period__c))
                VendorAccountingStrings.add(pricebook.Vendor__c+'-'+pricebook.Accounting_Period__c);      
        }  
        
        for(pricebook2 pricebookrec : pricebooks){
            if(VendorAccountingStrings.size()>0 && VendorAccountingStrings.contains(pricebookrec.Vendor__c+'-'+pricebookrec.Accounting_Period__c)){
                pricebookrec.addError('There is already an active pricebook for this vendor and Fiscal Year. Please inactivate the current active pricebook before creating a new one.');
            }
        }              
        
    }
}