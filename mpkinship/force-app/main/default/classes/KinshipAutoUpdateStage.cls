global class KinshipAutoUpdateStage implements Database.Batchable<sObject>,Database.Stateful, Schedulable{
    public String queryStr;
    public Integer batchSize;
    global void execute(SchedulableContext sc){
        KinshipAutoUpdateStage theBatch = new KinshipAutoUpdateStage();
        Database.executeBatch(theBatch, batchSize);        
    }

    global KinshipAutoUpdateStage() {
            queryStr = 'SELECT id, name, Payment_Method__c, RecordType.Name, Invoice_Stage__c '+
                        'FROM Payment__c '+
                        'WHERE CreatedDate >= LAST_N_DAYS:30 AND RecordType.Name = \'Staff & Operations\' AND Payment_Method__c = \'Journal Entry\' AND Invoice_Stage__c = \'\' ';
            batchSize = 200;
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(queryStr);
    }

    global void execute(Database.BatchableContext BC, List<Payment__c> scope) {
        for(Payment__c p : scope) {
            p.Invoice_Stage__c = 'Pending';
        }
        update scope;
    }

    global void finish(Database.BatchableContext BC) { 
    }
}