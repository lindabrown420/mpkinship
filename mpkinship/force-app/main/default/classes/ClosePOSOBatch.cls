/**CLOSING POSO AND CASE ACTION BOTH IF END DATE IS TODAY DATE AND THERE IS NO OPEN INVOICE **/

public class ClosePOSOBatch implements Database.Batchable<SObject>{
  
    public Database.QueryLocator start(Database.BatchableContext bc){       
        Id POSORecTypeId =Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Purchase_Order').getRecordTypeId();
        Id CaseActionRecTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Case_Actions').getRecordTypeId();
        Date todaydate=date.Today();
       /*boolean isPaid=true;
        boolean isWriteoff=true;
        string query = 'Select Id, Name,StageName,(select id,npe01__Paid__c,npe01__Written_Off__c from npe01__OppPayment__r where npe01__Paid__c=:isPaid or npe01__Written_Off__c=:isWriteoff) from Opportunity Where StageName=\'Open\' AND End_Date__c=:todaydate AND (recordTypeId=:POSORecTypeId OR recordTypeId=:CaseActionRecTypeId)'; */
        
        string query = 'Select Id, Name,StageName,(select id,Paid__c,Written_Off__c from Payment_OppPayment__r) from Opportunity Where StageName=\'Open\' AND End_Date__c=:todaydate AND Action_Selected__c=\'Change End Date\' AND (recordTypeId=:POSORecTypeId OR recordTypeId=:CaseActionRecTypeId)';
        return Database.getQueryLocator(query);
    }
    public void execute(Database.BatchableContext BC, List<Opportunity> POs) {
        system.debug('**LIST of posos'+ POs);
        ClosePOSOCABatchHelper.closePO(POs);
        
    } 
    public void finish(Database.BatchableContext BC) {
        // execute any post-processing operations
    }
}