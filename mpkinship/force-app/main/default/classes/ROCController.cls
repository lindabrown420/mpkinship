public inherited sharing class ROCController {
    public ROCController() {

    }  
    
    @AuraEnabled(continuation = true)       
    public static List<Payment__c> getNextReceiptNumber(string rocId){
        List<Payment__c> oppPayment =  [Select id,Report_Number__c,CreatedDate from Payment__c where (recordType.name = 'Receipt' or recordType.name = 'Vendor Refund') and Opportunity__c=: rocId order by Report_Number__c  desc limit 1];  
        return oppPayment;
        
    } 

    @AuraEnabled(continuation = true)       
    public static Deposits__c getStatusValue(string dId){
        Deposits__c deposit =  [Select id,Status__c from Deposits__c where Id=: dId];  
        return deposit;       
    } 

    @AuraEnabled(continuation = true)
    public static string getNextRecord(string ROCDate, Integer isNext){
        try {
            DateTime dt = (DateTime)JSON.deserialize('"'+ROCDate+'"', DateTime.class);/*
            Opportunity ROCId = new Opportunity();
            ROCId = [Select CreatedDate From Opportunity Where CreatedDate > :dt And RecordType.name = 'Report of Collections' Order by CreatedDate Limit 1];
            /*if(isNext == 1){
                ROCId = [Select Id From Opportunity Where CreatedDate > :ROCDate And RecordType.name = 'Report of Collections' Order by CreatedDate Limit 1];
            }else{
                ROCId = [Select Id From Opportunity Where CreatedDate < :ROCDate And RecordType.name = 'Report of Collections' Order by CreatedDate Limit 1];
            }*/
            
            return string.valueOf(dt);
            //return string.valueOf(ROCDate);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    

        @AuraEnabled(continuation = true)       
    public static Category__c getCategoryName(string paymentId){
        Category__c cat =  [Select Id,Category_Type__c from Category__c where Id =: paymentId];  
        return cat;
        
    } 
    
    @AuraEnabled(continuation = true)       
    public static Id getrecordTypeId(){
        Id RecordTypeId = Schema.SObjectType.Payment__c.getRecordTypeInfosByName().get('Vendor Refund').getRecordTypeId() ;  
        return RecordTypeId;
        
    }  
    
    @AuraEnabled(continuation = true)       
    public static Category__c checkFundingSource(string catId){
        try{ 
            Category__c cat = [Select Id,Category_Type__c from Category__c where Id =: catId];
            return cat;
        }
        catch(Exception e ){
            system.debug(e.getMessage() + ' e.getMessage() ???');
            return null;
        }
        
    }    
        
    @AuraEnabled(continuation = true)   
    public static Campaign getDatefromAccountingPeriod(string recordId){
        try{ 
            Campaign cam = [Select Id,StartDate,EndDate,FIPS__c from Campaign where Id =: recordId];
            return cam;
        }
        catch(Exception e ){
            system.debug(e.getMessage() + ' e.getMessage() ???');
            return null;
        }
        
    }
    @AuraEnabled(continuation = true)   
    public static string deleteROC(string ROCID){
        try{
            Opportunity opp =[Select Id from Opportunity where Id =: ROCID limit 1];
            
            List<Payment__c> oppPayments = [Select Id  from Payment__c where Opportunity__c = :opp.Id AND (RecordType.Name = 'Receipt' OR RecordType.Name = 'Vendor Refund')];
            Set<Id> oppPaymentIds = new Set<Id>();
            Set<Id> contentDocumentIds = new Set<Id>();
            List<ContentDocument> cdList = new  List<ContentDocument>();
            List<ContentDocumentLink> cdlList = new List<ContentDocumentLink>();
            List<ContentDocumentLink> cdlListForCheck = new List<ContentDocumentLink>();
            Boolean checkCDL = false;
            for(Payment__c oppy : oppPayments){
                oppPaymentIds.add(oppy.Id);
            }
            if(oppPaymentIds.size()>0){
                cdlList = [Select Id,ContentDocumentId from ContentDocumentLink where LinkedEntityId IN: oppPaymentIds];
                if(cdlList.size()>0){
                    for(ContentDocumentLink cdl : cdlList){
                        contentDocumentIds.add(cdl.ContentDocumentId);
                    }
                    cdlListForCheck = [Select Id,ContentDocumentId from ContentDocumentLink where ContentDocumentId IN: contentDocumentIds];
                    if(cdlList.size() == cdlListForCheck.size()){
                        cdList = [Select Id from ContentDocument where Id IN: contentDocumentIds];
                    }
                    else{
                        checkCDL = true;
                    }
                }
            }
            
            if(oppPayments.size()>0){
              delete oppPayments;  
            }
            delete opp;

            if(cdList.size()>0){
                delete cdList;  
            }
            if(checkCDL){
                //delete cdlList;
            }
            //   if(cdlList.size()>0){
            //     delete cdlList;  
            //   }
           

            return 'Done';
            
        }
        catch(Exception e ){
            system.debug(e.getMessage() + ' e.getMessage() ???');
            return e.getMessage();
            
        }
        
    }
    @AuraEnabled(continuation = true)   
    public static string cloneROC(string ROCID){
        string recordid;
        
        try{
            /*Payment__c Receipts = [Select Id,Fund__c,Opportunity__c, Name, Recordtype.Name,Payer__c,Category_lookup__r.RecordType.Name, Invoices_Status__c, Report_Number__c, Record_view__c, Kinship_Check__c, Check_Number__c, Case_Name__c, Payment_Date__c, Kinship_Case__c,Kinship_Case__r.Name,Kinship_Case__r.Case_No__c, Description__c,Payment__r.Name, Payment__c,
                                        Payer_Contact__c,FIPS_Code__c,Contact__c,Type_of_Receipt__c,Payment_Amount__c, Pay_Method__c, Receipt_Type__c,Accounting_Period__c,Accounting_Period__r.Name, Recipient__c,Vendor__c,Vendor__r.Name,Claims__r.Name, Special_Welfare_Account__c,Special_Welfare_Account__r.Name,  Check_Reference_Number__c from Payment__c where Id = :ROCID AND (RecordType.Name = 'Receipt') limit 1 ]; */
            Payment__c Receipts = [Select Id,Fund__c,Opportunity__c,Payment_Source__c,Adjustment_Amount__c, Name, Recordtype.Name,Payer__c,Category_lookup__r.RecordType.Name, Invoices_Status__c, Report_Number__c, Record_view__c, Kinship_Check__c, Check_Number__c, Case_Name__c, Payment_Date__c, Kinship_Case__c,Kinship_Case__r.Name,Kinship_Case__r.Case_No__c, Description__c,Payment__r.Name, Payment__c,
                                        Payer_Contact__c,FIPS_Code__c,Contact__c,Type_of_Receipt__c,Payment_Amount__c, Payment_Method__c, Receipt_Type__c,Accounting_Period__c,Accounting_Period__r.Name, Recipient__c,Vendor__c,Vendor__r.Name,Claims__r.Name, Special_Welfare_Account__c,Special_Welfare_Account__r.Name,  Check_Reference_Number__c from Payment__c where Id = :ROCID AND (RecordType.Name = 'Receipt') limit 1 ];
            System.debug('Receipts>>>>>>>>'+Receipts);
            Payment__c payments = Receipts.clone(false,true);
            payments.Opportunity__c = Receipts.Opportunity__c;
            insert payments;
            List<ContentDocumentLink> insertcdlList = new List<ContentDocumentLink>();
            ContentDocumentLink cdl= new ContentDocumentLink();
            List<ContentDocumentLink> cdlList = [Select Id,ContentDocumentId from ContentDocumentLink where LinkedEntityId =: ROCID];
            System.debug('cdlList'+cdlList);
            for(ContentDocumentLink c: cdlList){
                cdl= new ContentDocumentLink();
               cdl.ContentDocumentId = c.ContentDocumentId;
               cdl.LinkedEntityId = payments.Id;
               insertcdlList.add(cdl);
            }   
            if(insertcdlList.size()>0){
                insert insertcdlList;
            }
            recordid =  '';
        }
        catch(Exception e ){
            system.debug(e.getMessage() + ' e.getMessage() ???');
            return e.getMessage();
            
        }
        return recordid;
    }
    @AuraEnabled(continuation = true)   
    public static campaign getdefaultFiscalYear(){
        system.debug('***Enterd in fetchintialvalues');
        string accountingPeriodId;
        Integer currentyear = system.today().year();
        list<FIPS__c> fips =[select id,FIPS_Code__c,RAM_Email__c from FIPS__c where Is_Default__c=:true limit 1];
        Id campRecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Fiscal_Year').getRecordTypeId();
        if(fips.size()>0){
          campaign ParentFiscalYear =[select id,Name,StartDate,EndDate from campaign where Fiscal_Year_Type__c=:'Local' and
                                            IsActive = true and recordtypeid=:campRecordTypeId and
                                            FIPS__c=:fips[0].id limit 1];
            
             return ParentFiscalYear;
         
        }   
        return null;   
    
    }
    
    @AuraEnabled(continuation = true)    
    public static Payment__c getReceiptInfo(string recpId){
         try{
            // Opportunity opp =[Select Id from Opportunity where Id =: rocId limit 1];
            /* Payment__c AllReceiptInfo = [Select id,Report_Number__c,RecordType.Name,Receipt_Type__c,Payment_Amount__c,Pay_Method__c,Payment_Method__c,FIPS_Code__c,Accounting_Period__c,Category_lookup__c,Category_lookup__r.Category_Type__c,Category_lookup__r.Name,Kinship_Case__c,Kinship_Case__r.Name,Claims__c,Recipient__c,Payment_Source__c,Adjustment_Amount__c,(select Id,Adjustment_Amount__c from Invoices__r where RecordType.Name = 'Adjustments'),
            Claims__r.Name, Special_Welfare_Account__c, Special_Welfare_Account__r.Name, Vendor__c,Vendor__r.Name,Payer_Contact__c,Payer_Contact__r.Name from Payment__c where Id = :recpId AND RecordType.Name IN ('Receipt','Vendor Refund', 'Special Welfare Reimbursement') AND RecordType.Name != 'Adjustments']; */
            Payment__c AllReceiptInfo = [Select id,Report_Number__c,RecordType.Name,Receipt_Type__c,Payment_Amount__c,Payment_Method__c,FIPS_Code__c,Accounting_Period__c,Category_lookup__c,Category_lookup__r.Category_Type__c,Category_lookup__r.Name,Kinship_Case__c,Kinship_Case__r.Name,Claims__c,Recipient__c,Payment_Source__c,Adjustment_Amount__c,(select Id,Adjustment_Amount__c from Invoices__r where RecordType.Name = 'Adjustments'),
            Claims__r.Name, Special_Welfare_Account__c, Special_Welfare_Account__r.Name, Vendor__c,Vendor__r.Name,Payer_Contact__c,Payer_Contact__r.Name from Payment__c where Id = :recpId AND RecordType.Name IN ('Receipt','Vendor Refund', 'Special Welfare Reimbursement') AND RecordType.Name != 'Adjustments']; 
             return AllReceiptInfo; 
            
        }catch(Exception e){}
        return null;
    }
    @AuraEnabled(continuation = true)    
    public static Category__c GetSWCategory(){
         try{
            Category__c SWACategory = [Select id,Name,Category_Type__c from Category__c where Category_Type__c = 'Special Welfare' limit 1];
             return SWACategory; 
            
        }catch(Exception e){}
        return null;
    }
    @AuraEnabled(continuation = true)
    public static string updateFipsCodeinInvoices(string invoiceid){
         List<String> listInvoiceid = new List<String>();
        List<Payment__c> InvoiceList = new List<Payment__c>();
        if(invoiceid.contains(',')){
            listInvoiceid = invoiceid.split(',');
            try{
                List<Payment__c> Invoice = [Select id, Opportunity__r.Campaign.FIPS__c  from Payment__c where id IN: listInvoiceid];
                for(Payment__c opp : Invoice){
                    opp.FIPS_Code__c = opp.Opportunity__r.Campaign.FIPS__c;
                    InvoiceList.add(opp);
                }
                
                update InvoiceList;
                
            }catch(Exception e){}
        }
        
        set<id> categoryids = new set<id>();
        set<id> fipsids = new set<id>();
        
        try{
            Payment__c Invoice = [Select id, Opportunity__r.Campaign.FIPS__c, Category_lookup__c  from Payment__c where id=: invoiceid];
            
            Invoice.FIPS_Code__c = Invoice.Opportunity__r.Campaign.FIPS__c;
            
            categoryids.add(Invoice.Category_lookup__c);
            fipsids.add(Invoice.FIPS_Code__c);
            
            id costcenterid = FetchCostcode(Invoice, categoryids, fipsids);
            if(costcenterid !=null){
                Invoice.Cost_Center__c = costcenterid;
            }
            
            update Invoice;
            
        }catch(Exception e){}
        return null;
    }
    
       /**POPULATE COSTCODE**/
   public static string FetchCostcode(Payment__c payment,set<id> categoryids,set<id> fipsids){   
       Id CostCenterId;
              list<Category__c> categories=[select id,Locality_Account__c from category__C where id in: categoryids and Locality_Account__c!=null];
       system.debug(categories + ' categories in  FetchCostcode ???? ' );
       
       map<id,list<category__C>> LocalityCategoryMap = new map<id,list<category__C>>();
       for(Category__C category : categories){
           if(LocalityCategoryMap.containskey(category.Locality_Account__c)){
              LocalityCategoryMap.get(category.Locality_Account__c).add(category);   
           }
           else
              LocalityCategoryMap.put(category.Locality_Account__c,new list<category__C>{category});
       }
       
       system.debug(LocalityCategoryMap + ' LocalityCategoryMap in  FetchCostcode ????  ');
       system.debug(LocalityCategoryMap.keyset() + ' LocalityCategoryMap.keyset() in  FetchCostcode ????  ');
       system.debug(fipsids + 'fipsids ');
       
       map<string,id> CatFipsWithCostCodeMap = new map<String,id>();
       list<Chart_of_Account__c> charts=[select id,Locality_Account__c,Cost_Center__c,FIPS__c from Chart_of_Account__c where 
                                         Locality_Account__c in:LocalityCategoryMap.keyset() and  FIPS__c in: fipsids and Cost_Center__c!=null];
       system.debug(charts + ' charts in  FetchCostcode ????  ');
       
       for(Chart_of_Account__c chart : charts){
            
            if(LocalityCategoryMap.size()>0 && LocalityCategoryMap.containskey(chart.Locality_Account__c)){               
                for(category__c categoryrec : LocalityCategoryMap.get(chart.Locality_Account__c)){                    
                    if(!CatFipsWithCostCodeMap.containskey(categoryrec.id+'-'+chart.FIPS__c ))
                        CatFipsWithCostCodeMap.put(categoryrec.id+'-'+chart.FIPS__c,chart.Cost_Center__c);
                }
            }
        
        } 
         system.debug(CatFipsWithCostCodeMap + ' CatFipsWithCostCodeMap in  FetchCostcode ????  ');
            
            if(CatFipsWithCostCodeMap.size()>0 && CatFipsWithCostCodeMap.containskey(payment.Category_lookup__c+'-'+payment.FIPS_Code__c)){                
            CostCenterId = CatFipsWithCostCodeMap.get(payment.Category_lookup__c+'-'+payment.FIPS_Code__c);
            system.debug( CostCenterId+ '  CostCenterId in  FetchCostcode ????  ');
            return CostCenterId;
        } 
       return null;
   }
    
    
    @AuraEnabled(continuation = true)
    public static decimal getReceiptsSumbyID(List<String> ReceiptsIDs){
        List<Payment__c> Receipts = [Select id,Payment_Amount__c from Payment__c where id IN: ReceiptsIDs];
        Decimal Sum = 0.0;
        for(Payment__c pay: Receipts ){
            Sum += pay.Payment_Amount__c;
        }
        return Sum;
    }
    @AuraEnabled(continuation = true)
    public static string UpdateReceiptsbyVendorRefund(string VendorRefundId, List<String> ReceiptsIDs){
        List<Payment__c> Receipts = [Select id from Payment__c where id IN: ReceiptsIDs];
        List<Payment__c> UpdateReceipts = new List<Payment__c>();
        for(Payment__c pay: Receipts ){
             UpdateReceipts.add(pay);
        }
        if(UpdateReceipts.size() > 0){
            update UpdateReceipts;
        }
        return 'Done';
    }
    @AuraEnabled(continuation = true)
    Public static List<Payment__c> getChecksbyOpportunityId(ID ROCID){
        return [Select Id, Name, Report_Number__c, Check_Number__c,Kinship_Check__c, Case_Name__c, Payment_Date__c, Kinship_Case__c,Kinship_Case__r.Name,Kinship_Case__r.Case_No__c, Description__c, 
        Type_of_Receipt__c,Payment_Amount__c,Receipt_Type__c, Special_Welfare_Account__c From Payment__c 
               //Where Opportunity__c =: ROCID
               //AND Kinship_Check__c  <> null AND Recordtype.Name = 'Receipt'
             Where Kinship_Check__c  <> null ];
    }
    
    @AuraEnabled(continuation = true)
    Public static List<Payment__c> getPaymentsbyCheckID(ID ROCID, ID Checkid){
        return [Select Id, Name,Category_Name__c, Opporunity_Name__c,Payment_Method__c, Amount_Paid__c, Check_Number__c, Kinship_Check__c,Category_lookup__r.Category_Type__c, 
                Case_Name__c, Payment_Date__c, Kinship_Case__c,Kinship_Case__r.Name,Kinship_Case__r.Case_No__c, 
                Description__c, Balance_Remaining__c, Refund_Amount__c, Prior_Refund1__c, Vendor__c, Vendor_Name__c,
                Type_of_Receipt__c,Payment_Amount__c,Invoice_Stage__c, Special_Welfare_Account__c,Opportunity__r.Campaign.FIPS__c
                From Payment__c 
                Where  Kinship_Check__c  = :Checkid AND Recordtype.Name = 'Vendor Invoice'];
    }
    
    @AuraEnabled(continuation = true)
    Public static List<Payment__c> getReceiptsFromROC(ID ROCID){
        return [Select Id, Name, Recordtype.Name,Payer__c,Cost_Center__c,Cost_Center__r.Name,Category_lookup__r.Name,Category_lookup__r.RecordType.Name,Category_lookup__r.Category_Type__c, Invoices_Status__c, Report_Number__c, Record_view__c, Kinship_Check__c, Check_Number__c, Case_Name__c, Payment_Date__c, Kinship_Case__c,Kinship_Case__r.Name,Kinship_Case__r.Case_No__c, Description__c,Payment__r.Name, Payment__c,
        Type_of_Receipt__c,Payment_Amount__c,Receipt_Type__c,Accounting_Period__c,Accounting_Period__r.Name, Recipient__c,Vendor__c,Vendor__r.Name,Claims__r.Name, Special_Welfare_Account__c,Special_Welfare_Account__r.Name,Check_Reference_Number__c From Payment__c Where Opportunity__c =: ROCID
               AND (Recordtype.Name = 'Receipt') order by Recordtype.Name, Report_Number__c,CreatedDate];
    }
    @AuraEnabled(continuation = true)
    Public static List<Payment__c> getAdjustmentsFromROC(ID ROCID){
        return [Select Id, Name, Recordtype.Name,Payer__c,Cost_Center__c,Cost_Center__r.Name,Category_lookup__r.Name,Category_lookup__r.RecordType.Name,Category_lookup__r.Category_Type__c, Invoices_Status__c, Report_Number__c, Record_view__c, Kinship_Check__c, Check_Number__c, Case_Name__c, Payment_Date__c, Kinship_Case__c,Kinship_Case__r.Name,Kinship_Case__r.Case_No__c, Description__c,Payment__r.Name, Payment__c,
        Type_of_Receipt__c,Payment_Amount__c,Receipt_Type__c,Accounting_Period__c,Accounting_Period__r.Name, Recipient__c,Vendor__c,Vendor__r.Name,Claims__r.Name, Special_Welfare_Account__c,Special_Welfare_Account__r.Name,Check_Reference_Number__c From Payment__c Where Opportunity__c =: ROCID
               AND (Recordtype.Name = 'Adjustments') order by Recordtype.Name, Report_Number__c,CreatedDate];
    }
    @AuraEnabled(continuation = true)
    Public static List<Payment__c> getvRefundFromROC(ID ROCID){
        return [Select Id, Name, Recordtype.Name,Payer__c,Cost_Center__c,Cost_Center__r.Name,Category_lookup__r.Name,Category_lookup__r.RecordType.Name,Category_lookup__r.Category_Type__c, Invoices_Status__c, Report_Number__c, Record_view__c, Kinship_Check__c, Check_Number__c, Case_Name__c, Payment_Date__c, Kinship_Case__c,Kinship_Case__r.Name,Kinship_Case__r.Case_No__c, Description__c,Payment__r.Name, Payment__c,
        Type_of_Receipt__c,Payment_Amount__c,Receipt_Type__c,Accounting_Period__c,Accounting_Period__r.Name, Recipient__c,Vendor__c,Vendor__r.Name,Claims__r.Name, Special_Welfare_Account__c,Special_Welfare_Account__r.Name,Check_Reference_Number__c From Payment__c Where Opportunity__c =: ROCID
               AND (Recordtype.Name = 'Vendor Refund') order by Recordtype.Name, Report_Number__c,CreatedDate];
    } 
     @AuraEnabled(continuation = true)
    Public static List<Payment__c> getSWRFromROC(ID ROCID){
        return [Select Id, Name, Recordtype.Name,Payer__c,Cost_Center__c,Cost_Center__r.Name,Category_lookup__r.Name,Category_lookup__r.RecordType.Name,Category_lookup__r.Category_Type__c, Invoices_Status__c, Report_Number__c, Record_view__c, Kinship_Check__c, Check_Number__c, Case_Name__c, Payment_Date__c, Kinship_Case__c,Kinship_Case__r.Name,Kinship_Case__r.Case_No__c, Description__c,Payment__r.Name, Payment__c,
        Type_of_Receipt__c,Payment_Amount__c,Receipt_Type__c,Accounting_Period__c,Accounting_Period__r.Name, Recipient__c,Vendor__c,Vendor__r.Name,Claims__r.Name, Special_Welfare_Account__c,Special_Welfare_Account__r.Name,Check_Reference_Number__c From Payment__c Where Opportunity__c =: ROCID
               AND (Recordtype.Name = 'Special Welfare Reimbursement') order by Recordtype.Name, Report_Number__c,CreatedDate];
    } 
    @AuraEnabled(continuation = true)
    Public static List<Payment__c> getVRefundsFromROC(ID ROCID, ID RCPTID){
        return [Select Id, Name, Report_Number__c, Invoices_Status__c, Case_Name__c, Payment_Date__c, Kinship_Case__c,Kinship_Case__r.Name,Kinship_Case__r.Case_No__c, Description__c, 
        Type_of_Receipt__c,Payment_Amount__c, Receipt_Type__c, Special_Welfare_Account__c From Payment__c Where Opportunity__c =: ROCID
               AND Recordtype.Name = 'Vendor Refund'];
    } 
    
    @AuraEnabled(continuation = true)
    Public static Payment__c getReceiptRecord(ID RecieptID){
       /* return [Select Id, Name, Report_Number__c,Category_lookup__c,Pay_Method__c,Invoice_Stage__c, Invoices_Status__c, Kinship_Check__c, Case_Name__c, Payment_Date__c, Kinship_Case__c,Kinship_Case__r.Name,Kinship_Case__r.Case_No__c, Description__c,Claims__c,Payment_Source__c,
        Receipt_Type__c,Payment_Amount__c, Special_Welfare_Account__c From Payment__c 
                Where id =: RecieptID]; */
        return [Select Id, Name, Report_Number__c,Category_lookup__c,Payment_Method__c,Invoice_Stage__c, Invoices_Status__c, Kinship_Check__c, Case_Name__c, Payment_Date__c, Kinship_Case__c,Kinship_Case__r.Name,Kinship_Case__r.Case_No__c, Description__c,Claims__c,Payment_Source__c,
        Receipt_Type__c, Payment_Amount__c, Special_Welfare_Account__c From Payment__c 
                Where id =: RecieptID];        
    } 
    
    @AuraEnabled(continuation = true)
    Public static Opportunity getROCRecord(ID ROCID){
        return [Select Id, Name, Posting_Period__c,CreatedDate,CreatedById, CreatedBy.Name,FIPS_Code__c, Campaign.Name,Campaign.IsActive,Campaign.Status, Total_Receipts__c, CloseDate,Beginning_Receipt__c,End_Date__c,Ending_Receipt__c,StageName, Amount, Accounting_Period__c, Accounting_Period__r.Status, CampaignId From Opportunity Where Id =: ROCID];
    } 
    @AuraEnabled(continuation = true)
    Public static string deleteReceipt(ID receiptID){
        //try{

            List<Payment__c> payment = [Select Kinship_Check__r.Check_Run__c from Payment__c Where id =: receiptID];
            
             List<Id> accountIds = new List<Id>();
             for (Payment__c s : payment) {
                 accountIds.add(s.Kinship_Check__r.Check_Run__c);
                
             } 
             List<Check_Run__c> checkRun = [Select id from Check_Run__c Where id IN:accountIds ];
            
             system.debug( accountIds+ '  Account Ids????  ');
            
             
            delete checkRun;
            List<Payment__c> pay = [Select Kinship_Check__c from Payment__c Where id =: receiptID];
            List<Id> accountId = new List<Id>();
             for (Payment__c s : pay) {
                 accountId.add(s.Kinship_Check__c);
                
             } 
             List<Kinship_Check__c> check = [Select id from Kinship_Check__c Where id IN:accountId ];
             delete check;

            Set<Id> contentDocumentIds = new Set<Id>();
            List<ContentDocument> cdList = new  List<ContentDocument>();
            List<ContentDocumentLink> cdlList = new List<ContentDocumentLink>();
            List<ContentDocumentLink> cdlListForCheck = new List<ContentDocumentLink>();
            Boolean checkCDL = false;
            
            try{

            
            cdlList = [Select Id,ContentDocumentId from ContentDocumentLink where LinkedEntityId =: receiptID];
                if(cdlList.size()>0){
                    for(ContentDocumentLink cdl : cdlList){
                        contentDocumentIds.add(cdl.ContentDocumentId);
                    }
                    cdlListForCheck = [Select Id,ContentDocumentId from ContentDocumentLink where ContentDocumentId IN: contentDocumentIds];
                    if(cdlList.size() == cdlListForCheck.size()){
                        cdList = [Select Id from ContentDocument where Id IN: contentDocumentIds];
                    }
                    else{
                        checkCDL = true;
                    }
                }
                if(cdList.size()>0){
                    delete cdList;  
                  }
          
            }catch(exception e){
                return e.getMessage();
            }

            Payment__c r = new Payment__c(); 
            r.Id = receiptID;
            system.debug( r+ '  Receipt????  ');
            delete r;

           
            return 'true';
      //  }catch(exception e){
         //   return e.getMessage();
       // }
        
    }
    
    @AuraEnabled(continuation = true)
    Public static boolean isAmountsEqual(ID ROCID){
        Opportunity ROC = [Select Id, Total_Receipts__c, Amount from Opportunity where Id =: ROCID];
        return ROC.Total_Receipts__c== ROC.Amount;
    }

    @AuraEnabled(continuation = true)
    Public static String markCompleteStage(ID ROCID){
        List<Payment__c> missingFIPSPayments = [SELECT Id, FIPS_Code__c FROM Payment__c Where FIPS_Code__c = null];
        List<Payment__c> tempPayments = new List<Payment__c>();
        Opportunity opp = [Select Id,StageName, FIPS_Code__c from Opportunity where Id =: ROCID LIMIT 1];
        for (Payment__c payment : missingFIPSPayments) {
            payment.FIPS_Code__c = opp.FIPS_Code__c;
            tempPayments.add(payment);
        }
        if(tempPayments.size() > 0){
            update tempPayments;
        }
            if(opp.StageName != 'Completed'){
                Opportunity ROC = new Opportunity();
                ROC.id = ROCID;
                ROC.StageName = 'Completed';
                try{
                    update ROC;
                    return 'true';
                }catch(Exception e){
                    return 'false';
                } 
            }
            else{
                return 'Completed';
            }
    
        
      
        
    }

    @AuraEnabled(continuation = true)
    Public static boolean markOpenStage(ID ROCID){
        Opportunity ROC = new Opportunity();
        ROC.id = ROCID;
        ROC.StageName = 'Open';
        try{
            update ROC;
            //ADDING CODE TO DELETE CASEACTIONS ASSOCIATED WITH THIS REOPENED ROCID
            list<opportunity> CaseActions=[select id from opportunity where ROC_Id__c=:ROCID];
            if(CaseActions.size()>0)
                delete CaseActions;
            system.debug('print line 384');    
            return false;
        }catch(Exception e){
            system.debug('**VAl of exception'+ e);
            return true;
        }
    }
    
    @AuraEnabled(continuation = true)
    Public static Category__c getCategoryType(String categoryId ){
          Category__c cat;
               cat = [Select Id,Category_Type__c,RecordType.Name from Category__c where Id =: categoryId AND Inactive__c <> true ];
            return cat;
       
        
    }
    @AuraEnabled(continuation = true)
    Public static boolean isReceiptNumberEsixt(String rNumber, string ROCID, string receiptid){
        try{
            List<Payment__c> Receipts;
            if(receiptid !=null){
               Receipts = [Select Id from Payment__c where Report_Number__c =: rNumber 
                                AND Opportunity__c =: ROCID 
                                AND Recordtype.Name = 'Receipt' 
                                AND Id != :receiptid];
            }else{
               Receipts = [Select Id from Payment__c where Report_Number__c =: rNumber 
                                                   AND Opportunity__c =: ROCID AND Recordtype.Name = 'Receipt'];
            }
            return Receipts.size() > 0;
                
        }catch(exception e){
            return false;
        }
        
        
    }
    @AuraEnabled(continuation = true)
    public static string checkReceiptRange(Integer startRange,Integer endRange, ID ROCID){
       /* try {
            List<Payment__c> tempReceiptList = new List<Payment__c>();
            tempReceiptList = [Select Id from Payment__c where Opportunity__c =: ROCID AND Recordtype.Name = 'Receipt' AND (Receipt_Number_Fr__c < :startRange OR Receipt_Number_Fr__c > :endRange) ];
            if(tempReceiptList.size() > 0){
                return 'Not Allowed';
            }else{
                return 'Allowed';
            }
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }*/
        return null;
    }
    
    public class ReceiptClass{
        @AuraEnabled public string Id = '';
        @AuraEnabled public string ReportNumber = '';
        @AuraEnabled public string kcase = '';
        @AuraEnabled public string kvendor = '';
        @AuraEnabled public Date DateOfReceipt = null;
        @AuraEnabled public string ReceiptType= '';
        @AuraEnabled public Decimal Amount = 0.0;
        @AuraEnabled public string Description = '';
        @AuraEnabled public List<RefundClass> VendorRefunds;
        public ReceiptClass(){
            VendorRefunds =  new List<RefundClass>();
        }        
    }
     public class RefundClass{
        @AuraEnabled public string Id = '';
        @AuraEnabled public string Name = '';
        @AuraEnabled public string kcase = '';
        @AuraEnabled public string kvendor = '';
        @AuraEnabled public Date DateOfReceipt = null;
        @AuraEnabled public string ReceiptType= '';
        @AuraEnabled public Decimal Amount = 0.0;
        @AuraEnabled public string Description = '';
        public RefundClass(){}        
    }

    @AuraEnabled(continuation = true)
    Public static List<ReceiptClass> getReceipts(ID ROCID){
        system.debug('Called ');
        List<Payment__c> Receipts =  [Select 
                                                Id, Vendor__r.Name, Name, Report_Number__c, Case_Name__c, Payment_Date__c, Kinship_Case__c,Kinship_Case__r.Name,
                                                Kinship_Case__r.Case_No__c, Description__c, Recipient__c,
                                                Payment_Amount__c,Receipt_Type__c,Special_Welfare_Account__c From Payment__c 
                                                Where Opportunity__c =: ROCID AND Recordtype.Name = 'Receipt'];
        List<Id> RECIDS = new List<Id>();
        for(Payment__c REC: Receipts){
            RECIDS.add(REC.id);    
        }
        List<Payment__c> VRefunds =  [Select 
                                                Id, Vendor__r.Name, Name, Report_Number__c, Case_Name__c, Payment_Date__c, Kinship_Case__c,Kinship_Case__r.Name,
                                                Kinship_Case__r.Case_No__c, Description__c, 
                                                Payment_Amount__c,Receipt_Type__c, Special_Welfare_Account__c From Payment__c 
                                                Where Opportunity__c =: ROCID AND Recordtype.Name = 'Vendor Refund'
                                               ];
        system.debug('Receipts '+ Receipts.size());
        system.debug('VRefunds '+ VRefunds.size());
        Map<ID, List<RefundClass>> reundMap = new Map<ID, List<RefundClass>>();
        RefundClass Refund = new RefundClass();
        for(Payment__c vr: VRefunds){
            List<RefundClass> tempList = reundMap.get(vr.id); 
            if(tempList == null){
                tempList = new List<RefundClass>(); 
            }
            Refund.Id = vr.Id;
            Refund.Name = vr.Name;
            Refund.kcase = vr.Case_Name__c;
            Refund.kvendor = vr.Vendor__r.Name;
            Refund.DateOfReceipt = vr.Payment_Date__c;
            Refund.ReceiptType = vr.Receipt_Type__c;
            Refund.Amount = vr.Payment_Amount__c;
            Refund.Description = vr.Description__c; 
            tempList.add(Refund);
            reundMap.put(vr.id, tempList);
            Refund = new RefundClass();    
            
        }
        List<ReceiptClass> ReceiptList = new List<ReceiptClass>();
        ReceiptClass RC = new ReceiptClass();
        system.debug('reundMap '+ reundMap.size());
        List<RefundClass> RefundList = new List<RefundClass>();
        
            for(Payment__c REC: Receipts){
                RC.Id = REC.Id;
                RC.ReportNumber = REC.Report_Number__c;
                RC.kcase = REC.Case_Name__c;
                RC.kvendor = REC.Vendor__r.Name;
                RC.DateOfReceipt = REC.Payment_Date__c;
                RC.ReceiptType = REC.Receipt_Type__c;
                RC.Amount = REC.Payment_Amount__c;
                RC.Description = REC.Description__c;
                
                RC.VendorRefunds = reundMap.get(REC.ID);
                
                ReceiptList.add(RC);
                RC = new ReceiptClass();                
        }
        return ReceiptList;
    } 
    
    
    //CHECK FOR PAID CASEACTION
    @AuraEnabled(continuation = true)
    public static boolean checkPaidCaseAction(Id ROCId){
        system.debug('**Val of rocid'+ ROCId);
        list<opportunity> opplist=[select id,(Select id from Payment_OppPayment__r where Paid__c=:true) from opportunity where
                                    ROC_Id__c=:ROCId];
        if(opplist.size() >0){
           for(opportunity opp : opplist){
               if(opp.Payment_OppPayment__r.size()>0){
                   return true;
               }
                   
           }
           return markOpenStage(ROCId);
        }
        system.debug('entered in false');
        return markOpenStage(ROCId);        
    }
    @AuraEnabled(continuation = true)
    public static List<Chart_of_Account__c> getCostCenter(Id categoryId){
        Category__c cat = new Category__c();
        List<Chart_of_Account__c> cfa = new List<Chart_of_Account__c>();
        cat = [Select id,Locality_Account__c from Category__c where Id =: categoryId ];
        if(cat.Locality_Account__c != null){
            cfa = [Select id,S_O_Default__c, Cost_Center__c,Cost_Center__r.Name,fips__c from Chart_of_Account__c where Locality_Account__c =: cat.Locality_Account__c]; 
        }
        return cfa;
    }
    @AuraEnabled(continuation = true)
    public static Boolean checkPayable(Id claimId){
        Boolean payableCheck = false;
        Claims__c claim = new Claims__c();
        claim = [Select id,Payable_to__c from Claims__c where Id =: claimId ];
        if(claim.Payable_to__c == null){
            payableCheck = true;
        }
        return payableCheck;
    }
    @AuraEnabled(continuation = true)
    public static Claims__c getClaim(Id contactId){
        Boolean payableCheck = false;
        List<Kinship_Case__c> caseList = new List<Kinship_Case__c>();
        Claims__c claim = new Claims__c();
        List<Claims__c> claimList = new List<Claims__c>();
        caseList = [Select Id from Kinship_Case__c where Primary_Contact__c =: contactId];
        claimList = [Select id from Claims__c where Case__c IN: caseList];
        if(claimList.size() == 1){
            for(Claims__c c:claimList){
                claim = c;
            } 
        }
        return claim;
    }
    @AuraEnabled(continuation = true)
    public static SObject getCaseId(Id recordId, String objectName){
        Claims__c claim = new Claims__c();
        Special_Welfare_Account__c swa = new Special_Welfare_Account__c();
        if(objectName.contains('SWA')){
            swa = [Select Case_KNP__c from Special_Welfare_Account__c where Id =: recordId];
            return swa;
        }
        else if(objectName.contains('Claim')){
            claim = [Select Case__c from Claims__c where Id =: recordId];
            return claim;
        }
        else{
            return null;
        }
    }

    @AuraEnabled(continuation = true)
    Public static Claims__c getClaimType(Id recordId){
        
      Claims__c  claimType = [Select Claim_Type__c from Claims__c where Id =: recordId];        
        
        return claimType;
    }
    
}