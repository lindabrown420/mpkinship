@isTest
public class ClosePOSOCATest {
	
	public static testmethod void closeCAPOSOTestmethod(){
         TriggerActivation__c  ta = new TriggerActivation__c ();
        ta.name='PaymentTrigger';
        ta.isActive__c=true;
        insert ta;
        TriggerActivation__c  ta1 = new TriggerActivation__c();
        ta1.name='PurchaseOrder Trigger';
        ta1.isActive__c=true;
        insert ta1;
         Id LEDRSRecTypeId = Schema.SObjectType.category__C.getRecordTypeInfosByDeveloperName().get('LEDRS_Category').getRecordTypeId();
         Locality_Account__c la = new Locality_Account__c();
        la.Name='222';
        insert la;
        Category__c category = new Category__c();
        category.Name='222';
        category.Locality_Account__c=la.id;
        category.Category_Type__c='CSA';
        category.RecordTypeId=LEDRSRecTypeId;
        insert category;
        Category__c category1 = new Category__c();
        category1.Name='222';
        category1.Locality_Account__c=la.id;
        category1.Inactive__c=false;
        category1.Category_Type__c='CSA';
        category1.RecordTypeId=LEDRSRecTypeId;
        insert category1;
        Category__c category2 = new Category__c();
        category2.Name='222';
        category2.Locality_Account__c=la.id;
        category2.Category_Type__c='CSA';
        category2.RecordTypeId=LEDRSRecTypeId;
        insert category2;
        Service_Placement_Types__c spt = new Service_Placement_Types__c();
        spt.name='test spt';
        spt.Code__c='1';
        insert spt;
        Category_Rate__c cr = new Category_Rate__c();
        cr.Category__c = category.id;
        cr.Status__c='Active';
        cr.Price__c=5000;
        cr.Effective_Date__c=system.today();
        cr.Min_Age__c=5;
        cr.Max_Age__c=15;
        cr.Prorate_First_Payment__c=true;
        cr.Prorate_final_payment__c=true;
        cr.Service_Placement_Type__c=spt.Id;
        cr.RecordTypeId = Schema.SObjectType.Category_Rate__c.getRecordTypeInfosByDeveloperName().get('Service_Names').getRecordTypeId(); 
        insert cr;
        Category_Rate__c cr1 = new Category_Rate__c();
        cr1.Category__c = category1.id;
        cr1.Status__c='Active';
        cr1.Price__c=5000;
        cr1.Effective_Date__c=system.today();
        cr1.Min_Age__c=2;
        cr1.Max_Age__c=4;
        cr1.Prorate_First_Payment__c=true;
        cr1.Prorate_final_payment__c=true;
        cr1.RecordTypeId = Schema.SObjectType.Category_Rate__c.getRecordTypeInfosByDeveloperName().get('Service_Names').getRecordTypeId(); 
        insert cr1;
        Category_Rate__c cr2 = new Category_Rate__c();
        cr2.Category__c = category2.id;
        cr2.Status__c='Active';
        cr2.Price__c=5000;
        cr2.Effective_Date__c=system.today();
        //cr2.Min_Age__c=2;
       // cr2.Max_Age__c=4;
        cr2.Prorate_First_Payment__c=true;
        cr2.Prorate_final_payment__c=true;
        cr2.RecordTypeId = Schema.SObjectType.Category_Rate__c.getRecordTypeInfosByDeveloperName().get('Other_Rate').getRecordTypeId(); 
        insert cr2;
        
        Mandate_Type__c mt = new Mandate_Type__c();
        mt.Name='test mandate';
        mt.Mandate_Code__c='22';
        insert mt;
        CategoryAndSPT__c cas = new CategoryAndSPT__c();
        cas.Category__c=category.id;
        cas.Service_Placement_Type__c=spt.id;
        insert cas;
        Mandate_Type_And_Category__c mtc = new Mandate_Type_And_Category__c();
        mtc.Category__c =category.id;
        mtc.Mandate_Type__c=mt.id;
        insert mtc;
        
        id vendorrecType = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Vendor').getRecordTypeId();
        account accountrec = new account();
        accountrec.recordtypeid=vendorrecType;
        accountrec.Name='test vendor';
        accountrec.Tax_ID__c='222-22-3333';
        accountrec.Entity_Type__c='Individual';
        insert accountrec;
         contact con = new contact();
         con.accountid=accountrec.id;
        //con.recordtypeid=recType;
        con.LastName='contactrec11';
        con.FirstName='test';
        con.Email='test233@contact.com';        
        insert con;
        FIPS__c fp = new FIPS__c();
        fp.Is_Default__c=true;
        fp.Name='test fips';
        insert fp;
        campaign camp = new campaign();
        camp.recordtypeid= Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Fiscal_Year').getRecordTypeId();
        camp.Fiscal_Year_Type__c='Local';
        camp.StartDate=system.today().addmonths(-1);
        camp.Name='test camp';
        camp.FIPS__c = fp.id;
        camp.EndDate =system.today().addmonths(12);
        insert camp;
        
        
        Kinship_Case__c caserec = new Kinship_Case__c();
        caserec.Name='test';
        caserec.Primary_Contact__c=con.id;
        caserec.Hispanic_Ethnicity__c='1';
        caserec.DSM_V_Indicator_ICD_10_1__c='1';
        caserec.Gender_Preference__c='M';
        caserec.Race__c='1';
        caserec.Autism_Flag_1__c='1';
        caserec.DSM_V_Indicator_ICD_10_1__c='1';
        caserec.Clinical_Medication_Indicator_1__c='1';
        caserec.Medicaid_Indicator_1__c='1';
        caserec.Referral_Source__c='1';
        caserec.DOB__c=date.today().addYears(-10);
        insert caserec;
        Kinship_Case__c caserec1 = new Kinship_Case__c();
        caserec1.Name='test case1';
        caserec1.Primary_Contact__c=con.id;
        caserec1.Hispanic_Ethnicity__c='1';
        caserec1.DSM_V_Indicator_ICD_10_1__c='1';
        caserec1.Gender_Preference__c='F';
        caserec1.Race__c='2';
        caserec1.Autism_Flag_1__c='2';
        caserec1.DSM_V_Indicator_ICD_10_1__c='2';
        caserec1.Clinical_Medication_Indicator_1__c='1';
        caserec1.Medicaid_Indicator_1__c='2';
        caserec1.Referral_Source__c='2';
        caserec1.InActive__c=true;
        insert caserec1;
        Service_Names_Code__c snc = new Service_Names_Code__c();
        snc.name='snc test';
        snc.Service_Name_Code__c ='2';
        insert snc;
        Service_Name_and_Service_Type__c snt = new Service_Name_and_Service_Type__c();
        snt.Service_Placement_Type__c =spt.id;
        snt.Service_Names_Code__c=snc.id;
        insert snt;
       
        
        Special_Welfare_Account__c swa = new Special_Welfare_Account__c();
        swa.Initial_Balance__c=10000;
        swa.Account_Closed__c=false;
        swa.Child_Support__c=2000;
        swa.Case_KNP__c=caserec.id;
        swa.SSA__c=2000;
        insert swa;
        Id carecTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Case_Actions').getRecordTypeId();
        
        opportunity opprecord1 = new opportunity();
        opprecord1.CloseDate=date.today();
        opprecord1.End_Date__c=date.today().addmonths(6);
        opprecord1.StageName='Draft';
        opprecord1.Type_of_Case_Action__c='One time';
        opprecord1.Name='test opportunity';
        opprecord1.Amount=200;
        opprecord1.Kinship_Case__c=caserec.id;
        opprecord1.recordtypeid=carecTypeId;
        opprecord1.Parent_Recipient__c='1';
        opprecord1.Category__c =category.id;
        opprecord1.AccountId=accountrec.id;
        opprecord1.Number_of_Months_Units__c=3;
        opprecord1.Mandate_Type_Code__c='';
        opprecord1.MandateType1__c='';
        opprecord1.Service_Description_Other__c='';
        opprecord1.Service_Name_Code_Value__c='';
        opprecord1.Service_Name_Code1__c='';
        opprecord1.Service_Placement_Type_Code__c='';
        opprecord1.Service_Placement_Type1__c='';

        insert opprecord1;
         Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitrequest();
        app.setObjectId(opprecord1.Id);    
         // Submit the approval request for the po    
        Approval.ProcessResult result = Approval.process(app);    
        // Verify that the results are as expected    
        System.assert(result.isSuccess());
           if(result.isSuccess()){
               test.starttest();
            opprecord1.stagename='Approved';
            update opprecord1;
               test.stopTest();
        }   
        list<opportunity> opplist=[select id,stagename,(select id,paid__c,written_off__C from Payment_OppPayment__r) from opportunity where id=:opprecord1.id];
        opplist[0].stagename='Open';
        update opplist[0];
        system.debug('***Val of opplist 191'+opplist[0].Payment_OppPayment__r);
        list<Payment__c> payments=[select id,Paid__c,Written_Off__c,Invoice_Stage__c,Scheduled_Date__c from Payment__c where Opportunity__c=:opprecord1.id];
    	system.debug('**payments in 191'+payments.size());
        ClosePOSOCA.FetchInvoices(opprecord1.id);
        opplist[0].Action_Selected__c='Await Final Invoice';
        update opplist[0];
        ClosePOSOCA.FetchInitialValues(opprecord1.id);
        string ids = '["'+payments[0].id+'"]';
     
        ClosePOSOCA.SaveRecords(opprecord1.id,'Await Final Invoice',string.valueof(date.today().addmonths(2)),ids);
    }    
    
    public testmethod static void test1(){
    
        id ContactrecType = Schema.SObjectType.contact.getRecordTypeInfosByDeveloperName().get('Contacts').getRecordTypeId();
        contact con = new contact();
        con.recordtypeid=ContactrecType;
        con.LastName='contactrec';
        con.FirstName='test';
        con.Email='test@contact.com';        
        insert con;
        
        Kinship_Case__c caserec = new Kinship_Case__c();
        caserec.Primary_Contact__c=con.id;
        insert caserec;
        
        //id vendorrecType = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Vendor').getRecordTypeId();
        account accountrec = new account();
        //accountrec.recordtypeid=vendorrecType;
        accountrec.Name='test vendor';
        accountrec.Tax_ID__c='222-22-3333';
        accountrec.Entity_Type__c='Individual';
        insert accountrec;
        
         Locality_Account__c la = new Locality_Account__c();
        la.Name='222';
        insert la;
        
        Category__c category = new Category__c();
        category.Name='222';
        category.Locality_Account__c =la.id;
        category.Category_Type__c='Adoption';
        category.RecordTypeId= Schema.SObjectType.category__C.getRecordTypeInfosByDeveloperName().get('LASER_Category').getRecordTypeId();
        insert category;  
        
        TriggerActivation__c  ta = new TriggerActivation__c ();
        ta.name='PaymentTrigger';
        ta.isActive__c=true;
        insert ta;
        TriggerActivation__c  ta1 = new TriggerActivation__c();
        ta1.name='PurchaseOrder Trigger';
        ta1.isActive__c=true;
        insert ta1;
        
        FIPS__c fp = new FIPS__c();
        fp.name='test fips';
        insert fp;
        id recTypeid = Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Fiscal_Year').getRecordTypeId();
        campaign cap = new campaign();
        cap.FIPS__c=fp.id;
        cap.RecordTypeId=recTypeid;
        cap.Fiscal_Year_Type__c='Local';
        cap.name='test';
        cap.Begin_Date__c=system.today().addmonths(-1);
        cap.EndDate=system.today().addyears(1);
        insert cap;
    
    	 list<TriggerActivation__c> talist=[select id,name,isActive__C from TriggerActivation__c where isActive__c=:true 
                                           and name=:'PaymentTrigger'];
        list<TriggerActivation__c> talist1=[select id,name,isActive__C from TriggerActivation__c where isActive__c=:true 
                                            and name=:'PurchaseOrder Trigger'];
        list<Kinship_Case__c> cases=[select id from Kinship_Case__c];
        list<account> accounts=[select id,recordtypeid from account where name='test vendor'];        
        list<Category__c> categories=[select id from Category__c];
          //insert a new product
        Product2 p = new product2(name='x',isactive=true,Unit_Measure__c='1');
        insert p;
        
        Product2 p2 = new product2(name='y',isactive=true,Unit_Measure__c='1');
        insert p2;
        list<fips__C> fip=[select id,name from fips__C];
        list<campaign> camp=[select id,fips__c,Begin_Date__c,enddate,Fiscal_Year_Type__c from campaign];
        //define the standart price for the product
        id stdPb = Test.getStandardPricebookId();
        insert new PricebookEntry(pricebook2id = stdPb, product2id = p.id,unitprice=1.0, isActive=true);
        
        Pricebook2 pb = new pricebook2(name='test',Vendor__c=accounts[0].id,IsActive=true,
                                      accounting_period__c=camp[0].id);
        insert pb;
        
        PricebookEntry pbe = new PricebookEntry(pricebook2id=pb.id, product2id=p.id,unitprice=1000.0, isActive=true);
        insert pbe; 
        id recType = Schema.SObjectType.opportunity.getRecordTypeInfosByDeveloperName().get('Purchase_Order').getRecordTypeId();
       test.startTest();
        opportunity opprecord = new opportunity();
        opprecord.CloseDate=date.today();
        opprecord.StageName='Draft';
        opprecord.Name='test opportunity';
        opprecord.RecordTypeId=recType;
        opprecord.Kinship_Case__c=cases[0].id;
        opprecord.CampaignId=camp[0].id;
        opprecord.Amount=2000;
        opprecord.Category__c =categories[0].id;
        opprecord.AccountId=accounts[0].id;
        opprecord.Number_of_Months_Units__c=1;
        opprecord.Pricebook2Id =pb.id;        
        insert opprecord;
        //NOW CREATING LINE ITEMS
        opportunitylineitem opplineitem = new opportunitylineitem();
        opplineitem.Unit_Measure__c='1';
        opplineitem.Product2Id=p.id;
        opplineitem.PricebookEntryId=pbe.id;
        opplineitem.Quantity=2;
        opplineitem.Category__c=categories[0].id;
        opplineitem.OpportunityId=opprecord.id;
        opplineitem.TotalPrice = 2000;
        insert opplineitem;
        //Create an approval request for the po
    
        Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitrequest();
        app.setObjectId(opprecord.Id);    
     
        Approval.ProcessResult result = Approval.process(app);    

        System.assert(result.isSuccess());
      
        if(result.isSuccess()){
            opprecord.stagename='Approved';
            update opprecord;
         test.stopTest();  
        } 
        opportunity opp = new opportunity();
        opp.id=opprecord.id;
        opp.stagename='Open';
        update opp;
            list<Payment__c> payments=[select id,Written_Off__c,Amount_Paid__c,Paid__c,recordtypeid,recordtype.name,recordtype.developername,
                                                 (Select id,unit_price__c from invoice_Line_Items__r) from Payment__c
                                                    where Opportunity__c=:opp.Id];
        list<id> payemntids = new list<id>();
        payemntids.add(payments[0].id);
          system.debug('lineitems 340'+payments[0]); 
        //["a033R000000wvTyQAI"]
        string ids = '["'+payments[0].id+'"]';
     
        ClosePOSOCA.SaveRecords(opprecord.id,'Close Immediately','2021-12-26',ids);
    }
     public testmethod static void test2(){
    
        id ContactrecType = Schema.SObjectType.contact.getRecordTypeInfosByDeveloperName().get('Contacts').getRecordTypeId();
        contact con = new contact();
        con.recordtypeid=ContactrecType;
        con.LastName='contactrec';
        con.FirstName='test';
        con.Email='test@contact.com';        
        insert con;
        
        Kinship_Case__c caserec = new Kinship_Case__c();
        caserec.Primary_Contact__c=con.id;
        insert caserec;
        
        //id vendorrecType = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Vendor').getRecordTypeId();
        account accountrec = new account();
        //accountrec.recordtypeid=vendorrecType;
        accountrec.Name='test vendor';
        accountrec.Tax_ID__c='222-22-3333';
        accountrec.Entity_Type__c='Individual';
        insert accountrec;
        
         Locality_Account__c la = new Locality_Account__c();
        la.Name='222';
        insert la;
        
        Category__c category = new Category__c();
        category.Name='222';
        category.Locality_Account__c =la.id;
        category.Category_Type__c='Adoption';
        category.RecordTypeId= Schema.SObjectType.category__C.getRecordTypeInfosByDeveloperName().get('LASER_Category').getRecordTypeId();
        insert category;  
        
        TriggerActivation__c  ta = new TriggerActivation__c ();
        ta.name='PaymentTrigger';
        ta.isActive__c=true;
        insert ta;
        TriggerActivation__c  ta1 = new TriggerActivation__c();
        ta1.name='PurchaseOrder Trigger';
        ta1.isActive__c=true;
        insert ta1;
        
        FIPS__c fp = new FIPS__c();
        fp.name='test fips';
        insert fp;
        id recTypeid = Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Fiscal_Year').getRecordTypeId();
        campaign cap = new campaign();
        cap.FIPS__c=fp.id;
        cap.RecordTypeId=recTypeid;
        cap.Fiscal_Year_Type__c='Local';
        cap.name='test';
        cap.Begin_Date__c=system.today().addmonths(-1);
        cap.EndDate=system.today().addyears(1);
        insert cap;
    
    	 list<TriggerActivation__c> talist=[select id,name,isActive__C from TriggerActivation__c where isActive__c=:true 
                                           and name=:'PaymentTrigger'];
        list<TriggerActivation__c> talist1=[select id,name,isActive__C from TriggerActivation__c where isActive__c=:true 
                                            and name=:'PurchaseOrder Trigger'];
        list<Kinship_Case__c> cases=[select id from Kinship_Case__c];
        list<account> accounts=[select id,recordtypeid from account where name='test vendor'];        
        list<Category__c> categories=[select id from Category__c];
          //insert a new product
        Product2 p = new product2(name='x',isactive=true,Unit_Measure__c='1');
        insert p;
        
        Product2 p2 = new product2(name='y',isactive=true,Unit_Measure__c='1');
        insert p2;
        list<fips__C> fip=[select id,name from fips__C];
        list<campaign> camp=[select id,fips__c,Begin_Date__c,enddate,Fiscal_Year_Type__c from campaign];
        //define the standart price for the product
        id stdPb = Test.getStandardPricebookId();
        insert new PricebookEntry(pricebook2id = stdPb, product2id = p.id,unitprice=1.0, isActive=true);
        
        Pricebook2 pb = new pricebook2(name='test',Vendor__c=accounts[0].id,IsActive=true,
                                      accounting_period__c=camp[0].id);
        insert pb;
        
        PricebookEntry pbe = new PricebookEntry(pricebook2id=pb.id, product2id=p.id,unitprice=1000.0, isActive=true);
        insert pbe; 
        id recType = Schema.SObjectType.opportunity.getRecordTypeInfosByDeveloperName().get('Purchase_Order').getRecordTypeId();
       test.startTest();
        opportunity opprecord = new opportunity();
        opprecord.CloseDate=date.today();
        opprecord.StageName='Draft';
        opprecord.Name='test opportunity';
        opprecord.RecordTypeId=recType;
        opprecord.Kinship_Case__c=cases[0].id;
        opprecord.CampaignId=camp[0].id;
        opprecord.Amount=2000;
        opprecord.Category__c =categories[0].id;
        opprecord.AccountId=accounts[0].id;
        opprecord.Number_of_Months_Units__c=1;
        opprecord.Pricebook2Id =pb.id;        
        insert opprecord;
        //NOW CREATING LINE ITEMS
        opportunitylineitem opplineitem = new opportunitylineitem();
        opplineitem.Unit_Measure__c='1';
        opplineitem.Product2Id=p.id;
        opplineitem.PricebookEntryId=pbe.id;
        opplineitem.Quantity=2;
        opplineitem.Category__c=categories[0].id;
        opplineitem.OpportunityId=opprecord.id;
        opplineitem.TotalPrice = 2000;
        insert opplineitem;
        //Create an approval request for the po
    
        Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitrequest();
        app.setObjectId(opprecord.Id);    
     
        Approval.ProcessResult result = Approval.process(app);    

        System.assert(result.isSuccess());
      
        if(result.isSuccess()){
            opprecord.stagename='Approved';
            update opprecord;
         test.stopTest();  
        }   
            list<Payment__c> payments=[select id,Written_Off__c,Amount_Paid__c,Paid__c,
                                                 (Select id,unit_price__c from invoice_Line_Items__r) from Payment__c
                                                    where Opportunity__c=:opprecord.Id];
        list<id> payemntids = new list<id>();
        payemntids.add(payments[0].id);
          system.debug('lineitems'+payments[0].invoice_Line_Items__r[0].unit_price__C); 
        //["a033R000000wvTyQAI"]
        string ids = '["'+payments[0].id+'"]';
     
        ClosePOSOCA.SaveRecords(opprecord.id,'Await Final Invoice','2021-12-26',ids);
    }
    
    public testmethod static void test3(){
    
        id ContactrecType = Schema.SObjectType.contact.getRecordTypeInfosByDeveloperName().get('Contacts').getRecordTypeId();
        contact con = new contact();
        con.recordtypeid=ContactrecType;
        con.LastName='contactrec';
        con.FirstName='test';
        con.Email='test@contact.com';        
        insert con;
        
        Kinship_Case__c caserec = new Kinship_Case__c();
        caserec.Primary_Contact__c=con.id;
        insert caserec;
        
        //id vendorrecType = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Vendor').getRecordTypeId();
        account accountrec = new account();
        //accountrec.recordtypeid=vendorrecType;
        accountrec.Name='test vendor';
        accountrec.Tax_ID__c='222-22-3333';
        accountrec.Entity_Type__c='Individual';
        insert accountrec;
        
         Locality_Account__c la = new Locality_Account__c();
        la.Name='222';
        insert la;
        
        Category__c category = new Category__c();
        category.Name='222';
        category.Locality_Account__c =la.id;
        category.Category_Type__c='Adoption';
        category.RecordTypeId= Schema.SObjectType.category__C.getRecordTypeInfosByDeveloperName().get('LASER_Category').getRecordTypeId();
        insert category;  
        
        TriggerActivation__c  ta = new TriggerActivation__c ();
        ta.name='PaymentTrigger';
        ta.isActive__c=true;
        insert ta;
        TriggerActivation__c  ta1 = new TriggerActivation__c();
        ta1.name='PurchaseOrder Trigger';
        ta1.isActive__c=true;
        insert ta1;
        
        FIPS__c fp = new FIPS__c();
        fp.name='test fips';
        insert fp;
        id recTypeid = Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Fiscal_Year').getRecordTypeId();
        campaign cap = new campaign();
        cap.FIPS__c=fp.id;
        cap.RecordTypeId=recTypeid;
        cap.Fiscal_Year_Type__c='Local';
        cap.name='test';
        cap.Begin_Date__c=system.today().addmonths(-1);
        cap.EndDate=system.today().addyears(1);
        insert cap;
    
    	 list<TriggerActivation__c> talist=[select id,name,isActive__C from TriggerActivation__c where isActive__c=:true 
                                           and name=:'PaymentTrigger'];
        list<TriggerActivation__c> talist1=[select id,name,isActive__C from TriggerActivation__c where isActive__c=:true 
                                            and name=:'PurchaseOrder Trigger'];
        list<Kinship_Case__c> cases=[select id from Kinship_Case__c];
        list<account> accounts=[select id,recordtypeid from account where name='test vendor'];        
        list<Category__c> categories=[select id from Category__c];
          //insert a new product
        Product2 p = new product2(name='x',isactive=true,Unit_Measure__c='1');
        insert p;
        
        Product2 p2 = new product2(name='y',isactive=true,Unit_Measure__c='1');
        insert p2;
        list<fips__C> fip=[select id,name from fips__C];
        list<campaign> camp=[select id,fips__c,Begin_Date__c,enddate,Fiscal_Year_Type__c from campaign];
        //define the standart price for the product
        id stdPb = Test.getStandardPricebookId();
        insert new PricebookEntry(pricebook2id = stdPb, product2id = p.id,unitprice=1.0, isActive=true);
        
        Pricebook2 pb = new pricebook2(name='test',Vendor__c=accounts[0].id,IsActive=true,
                                      accounting_period__c=camp[0].id);
        insert pb;
        
        PricebookEntry pbe = new PricebookEntry(pricebook2id=pb.id, product2id=p.id,unitprice=1000.0, isActive=true);
        insert pbe; 
        id recType = Schema.SObjectType.opportunity.getRecordTypeInfosByDeveloperName().get('Purchase_Order').getRecordTypeId();
       test.startTest();
        opportunity opprecord = new opportunity();
        opprecord.CloseDate=date.today();
        opprecord.StageName='Draft';
        opprecord.Name='test opportunity';
        opprecord.RecordTypeId=recType;
        opprecord.Kinship_Case__c=cases[0].id;
        opprecord.CampaignId=camp[0].id;
        opprecord.Amount=2000;
        opprecord.Category__c =categories[0].id;
        opprecord.AccountId=accounts[0].id;
        opprecord.Number_of_Months_Units__c=1;
        opprecord.Pricebook2Id =pb.id;        
        insert opprecord;
        //NOW CREATING LINE ITEMS
        opportunitylineitem opplineitem = new opportunitylineitem();
        opplineitem.Unit_Measure__c='1';
        opplineitem.Product2Id=p.id;
        opplineitem.PricebookEntryId=pbe.id;
        opplineitem.Quantity=2;
        opplineitem.Category__c=categories[0].id;
        opplineitem.OpportunityId=opprecord.id;
        opplineitem.TotalPrice = 2000;
        insert opplineitem;
        //Create an approval request for the po
    
        Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitrequest();
        app.setObjectId(opprecord.Id);    
     
        Approval.ProcessResult result = Approval.process(app);    

        System.assert(result.isSuccess());
      
        if(result.isSuccess()){
            opprecord.stagename='Approved';
            update opprecord;
         test.stopTest();  
        }   
            list<Payment__c> payments=[select id,Written_Off__c,Amount_Paid__c,Paid__c,
                                                 (Select id,unit_price__c from invoice_Line_Items__r) from Payment__c
                                                    where Opportunity__c=:opprecord.Id];
        list<id> payemntids = new list<id>();
        payemntids.add(payments[0].id);
          system.debug('lineitems'+payments[0].invoice_Line_Items__r[0].unit_price__C); 
        //["a033R000000wvTyQAI"]
        string ids = '["'+payments[0].id+'"]';
     
        ClosePOSOCA.SaveRecords(opprecord.id,'Change End Date','2020-12-26',ids);
    }
    
      public testmethod static void test4(){
    
        id ContactrecType = Schema.SObjectType.contact.getRecordTypeInfosByDeveloperName().get('Contacts').getRecordTypeId();
        contact con = new contact();
        con.recordtypeid=ContactrecType;
        con.LastName='contactrec';
        con.FirstName='test';
        con.Email='test@contact.com';        
        insert con;
        
        Kinship_Case__c caserec = new Kinship_Case__c();
        caserec.Primary_Contact__c=con.id;
        insert caserec;
        
        //id vendorrecType = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Vendor').getRecordTypeId();
        account accountrec = new account();
        //accountrec.recordtypeid=vendorrecType;
        accountrec.Name='test vendor';
        accountrec.Tax_ID__c='222-22-3333';
        accountrec.Entity_Type__c='Individual';
        insert accountrec;
        
         Locality_Account__c la = new Locality_Account__c();
        la.Name='222';
        insert la;
        
        Category__c category = new Category__c();
        category.Name='222';
        category.Locality_Account__c =la.id;
        category.Category_Type__c='Adoption';
        category.RecordTypeId= Schema.SObjectType.category__C.getRecordTypeInfosByDeveloperName().get('LASER_Category').getRecordTypeId();
        insert category;  
        
        TriggerActivation__c  ta = new TriggerActivation__c ();
        ta.name='PaymentTrigger';
        ta.isActive__c=true;
        insert ta;
        TriggerActivation__c  ta1 = new TriggerActivation__c();
        ta1.name='PurchaseOrder Trigger';
        ta1.isActive__c=true;
        insert ta1;
        
        FIPS__c fp = new FIPS__c();
        fp.name='test fips';
        insert fp;
        id recTypeid = Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Fiscal_Year').getRecordTypeId();
        campaign cap = new campaign();
        cap.FIPS__c=fp.id;
        cap.RecordTypeId=recTypeid;
        cap.Fiscal_Year_Type__c='Local';
        cap.name='test';
        cap.Begin_Date__c=system.today().addmonths(-1);
        cap.EndDate=system.today().addyears(1);
        insert cap;
    
    	 list<TriggerActivation__c> talist=[select id,name,isActive__C from TriggerActivation__c where isActive__c=:true 
                                           and name=:'PaymentTrigger'];
        list<TriggerActivation__c> talist1=[select id,name,isActive__C from TriggerActivation__c where isActive__c=:true 
                                            and name=:'PurchaseOrder Trigger'];
        list<Kinship_Case__c> cases=[select id from Kinship_Case__c];
        list<account> accounts=[select id,recordtypeid from account where name='test vendor'];        
        list<Category__c> categories=[select id from Category__c];
          //insert a new product
        Product2 p = new product2(name='x',isactive=true,Unit_Measure__c='1');
        insert p;
        
        Product2 p2 = new product2(name='y',isactive=true,Unit_Measure__c='1');
        insert p2;
        list<fips__C> fip=[select id,name from fips__C];
        list<campaign> camp=[select id,fips__c,Begin_Date__c,enddate,Fiscal_Year_Type__c from campaign];
        //define the standart price for the product
        id stdPb = Test.getStandardPricebookId();
        insert new PricebookEntry(pricebook2id = stdPb, product2id = p.id,unitprice=1.0, isActive=true);
        
        Pricebook2 pb = new pricebook2(name='test',Vendor__c=accounts[0].id,IsActive=true,
                                      accounting_period__c=camp[0].id);
        insert pb;
        
        PricebookEntry pbe = new PricebookEntry(pricebook2id=pb.id, product2id=p.id,unitprice=1000.0, isActive=true);
        insert pbe; 
        id recType = Schema.SObjectType.opportunity.getRecordTypeInfosByDeveloperName().get('Purchase_Order').getRecordTypeId();
       test.startTest();
        opportunity opprecord = new opportunity();
        opprecord.CloseDate=date.today();
        opprecord.StageName='Draft';
        opprecord.Name='test opportunity';
        opprecord.RecordTypeId=recType;
        opprecord.Kinship_Case__c=cases[0].id;
        opprecord.CampaignId=camp[0].id;
        opprecord.Amount=2000;
        opprecord.Category__c =categories[0].id;
        opprecord.AccountId=accounts[0].id;
        opprecord.Number_of_Months_Units__c=1;
        opprecord.Pricebook2Id =pb.id;        
        insert opprecord;
        //NOW CREATING LINE ITEMS
        opportunitylineitem opplineitem = new opportunitylineitem();
        opplineitem.Unit_Measure__c='1';
        opplineitem.Product2Id=p.id;
        opplineitem.PricebookEntryId=pbe.id;
        opplineitem.Quantity=2;
        opplineitem.Category__c=categories[0].id;
        opplineitem.OpportunityId=opprecord.id;
        opplineitem.TotalPrice = 2000;
        insert opplineitem;
        //Create an approval request for the po
    
        Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitrequest();
        app.setObjectId(opprecord.Id);    
     
        Approval.ProcessResult result = Approval.process(app);    

        System.assert(result.isSuccess());
      
        if(result.isSuccess()){
            opprecord.stagename='Approved';
            update opprecord;
         test.stopTest();  
        }   
        list<Payment__c> payments=[select id,Written_Off__c,Amount_Paid__c,Paid__c,
                                                 (Select id,unit_price__c from invoice_Line_Items__r) from Payment__c
                                                    where Opportunity__c=:opprecord.Id];
        list<id> payemntids = new list<id>();
        payemntids.add(payments[0].id);
          system.debug('lineitems'+payments[0].invoice_Line_Items__r[0].unit_price__C); 
        string ids = '["'+payments[0].id+'"]';
     
        ClosePOSOCA.SaveRecords(opprecord.id,'Change End Date',string.valueof(date.today().addmonths(2)),ids);
    }
    
    public testmethod static void testEndDateClose(){
    
        id ContactrecType = Schema.SObjectType.contact.getRecordTypeInfosByDeveloperName().get('Contacts').getRecordTypeId();
        contact con = new contact();
        con.recordtypeid=ContactrecType;
        con.LastName='contactrec';
        con.FirstName='test';
        con.Email='test@contact.com';        
        insert con;
        
        Kinship_Case__c caserec = new Kinship_Case__c();
        caserec.Primary_Contact__c=con.id;
        insert caserec;
        
        //id vendorrecType = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Vendor').getRecordTypeId();
        account accountrec = new account();
        //accountrec.recordtypeid=vendorrecType;
        accountrec.Name='test vendor';
        accountrec.Tax_ID__c='222-22-3333';
        accountrec.Entity_Type__c='Individual';
        insert accountrec;
        
         Locality_Account__c la = new Locality_Account__c();
        la.Name='222';
        insert la;
        
        Category__c category = new Category__c();
        category.Name='222';
        category.Locality_Account__c =la.id;
        category.Category_Type__c='Adoption';
        category.RecordTypeId= Schema.SObjectType.category__C.getRecordTypeInfosByDeveloperName().get('LASER_Category').getRecordTypeId();
        insert category;  
        
        TriggerActivation__c  ta = new TriggerActivation__c ();
        ta.name='PaymentTrigger';
        ta.isActive__c=true;
        insert ta;
        TriggerActivation__c  ta1 = new TriggerActivation__c();
        ta1.name='PurchaseOrder Trigger';
        ta1.isActive__c=true;
        insert ta1;
        
        FIPS__c fp = new FIPS__c();
        fp.name='test fips';
        insert fp;
        id recTypeid = Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Fiscal_Year').getRecordTypeId();
        campaign cap = new campaign();
        cap.FIPS__c=fp.id;
        cap.RecordTypeId=recTypeid;
        cap.Fiscal_Year_Type__c='Local';
        cap.name='test';
        cap.Begin_Date__c=system.today().addmonths(-1);
        cap.EndDate=system.today().addyears(1);
        insert cap;
    
    	 list<TriggerActivation__c> talist=[select id,name,isActive__C from TriggerActivation__c where isActive__c=:true 
                                           and name=:'PaymentTrigger'];
        list<TriggerActivation__c> talist1=[select id,name,isActive__C from TriggerActivation__c where isActive__c=:true 
                                            and name=:'PurchaseOrder Trigger'];
        list<Kinship_Case__c> cases=[select id from Kinship_Case__c];
        list<account> accounts=[select id,recordtypeid from account where name='test vendor'];        
        list<Category__c> categories=[select id from Category__c];
          //insert a new product
        Product2 p = new product2(name='x',isactive=true,Unit_Measure__c='1');
        insert p;
        
        Product2 p2 = new product2(name='y',isactive=true,Unit_Measure__c='1');
        insert p2;
        list<fips__C> fip=[select id,name from fips__C];
        list<campaign> camp=[select id,fips__c,Begin_Date__c,enddate,Fiscal_Year_Type__c from campaign];
        //define the standart price for the product
        id stdPb = Test.getStandardPricebookId();
        insert new PricebookEntry(pricebook2id = stdPb, product2id = p.id,unitprice=1.0, isActive=true);
        
        Pricebook2 pb = new pricebook2(name='test',Vendor__c=accounts[0].id,IsActive=true,
                                      accounting_period__c=camp[0].id);
        insert pb;
        
        PricebookEntry pbe = new PricebookEntry(pricebook2id=pb.id, product2id=p.id,unitprice=1000.0, isActive=true);
        insert pbe; 
        id recType = Schema.SObjectType.opportunity.getRecordTypeInfosByDeveloperName().get('Purchase_Order').getRecordTypeId();
       test.startTest();
        opportunity opprecord = new opportunity();
        opprecord.CloseDate=date.today();
        opprecord.StageName='Draft';
        opprecord.Name='test opportunity';
        opprecord.RecordTypeId=recType;
        opprecord.Kinship_Case__c=cases[0].id;
        opprecord.CampaignId=camp[0].id;
        opprecord.Amount=2000;
        opprecord.Category__c =categories[0].id;
        opprecord.AccountId=accounts[0].id;
        opprecord.Number_of_Months_Units__c=1;
        opprecord.Pricebook2Id =pb.id;        
        insert opprecord;
        //NOW CREATING LINE ITEMS
        opportunitylineitem opplineitem = new opportunitylineitem();
        opplineitem.Unit_Measure__c='1';
        opplineitem.Product2Id=p.id;
        opplineitem.PricebookEntryId=pbe.id;
        opplineitem.Quantity=2;
        opplineitem.Category__c=categories[0].id;
        opplineitem.OpportunityId=opprecord.id;
        opplineitem.TotalPrice = 2000;
        insert opplineitem;
        //Create an approval request for the po
    
        Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitrequest();
        app.setObjectId(opprecord.Id);    
     
        Approval.ProcessResult result = Approval.process(app);    

        System.assert(result.isSuccess());
      
        if(result.isSuccess()){
            opprecord.stagename='Approved';
            update opprecord;
         test.stopTest();  
        }   
        list<Payment__c> payments=[select id,Written_Off__c,Amount_Paid__c,Paid__c,
                                                 (Select id,unit_price__c from invoice_Line_Items__r) from Payment__c
                                                    where Opportunity__c=:opprecord.Id];
        list<id> payemntids = new list<id>();
        payemntids.add(payments[0].id);
          system.debug('lineitems'+payments[0].invoice_Line_Items__r[0].unit_price__C); 
        string ids = '["'+payments[0].id+'"]';
     
        ClosePOSOCA.SaveRecords(opprecord.id,'Change End Date',string.valueof(date.today().addmonths(-10)),ids);
    }
}