global class CategoryRateChangeBatchScheduler implements Schedulable{
	global void execute(SchedulableContext ctx) {
        CategoryRateChangeBatch sampleBatchObj=new CategoryRateChangeBatch();
        database.executebatch(sampleBatchObj);
    }
}