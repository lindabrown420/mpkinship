public class ContactTriggerHandler  extends TriggerHandler{
	public static Boolean bypassTrigger = false;
    
    public override void afterUpdate() {
        if(ContactTriggerHandler.bypassTrigger) return;
        List<Contact> lstNew = (List<Contact>)Trigger.new;
        List<Contact> lstOld = (List<Contact>)Trigger.old;
        Map<String, Contact> mapContact = new Map<String, Contact>();
        Set<Id> setId = new Set<Id>();
        for(Integer i = 0 ; i < lstNew.size() ; i++) {
            if(lstNew[i].FirstName != lstOld[i].FirstName || lstNew[i].LastName != lstOld[i].LastName) {
                mapContact.put(lstNew[i].Id, lstNew[i]);
                setId.add(lstNew[i].Id);
            }
        }
        if(setId.size() > 0) {
            List<Kinship_Case__c> lstCase = [SELECT id, Name, Primary_Contact__c FROM Kinship_Case__c WHERE Primary_Contact__c IN :setId];
            Contact temp;
            for(Kinship_Case__c c : lstCase) {
                temp = mapContact.get(c.Primary_Contact__c);
                if(temp != null) {
                    c.Name = temp.Name;
                }
            }
            update lstCase;
        }
    }
}