@isTest
public class PrintNotesCtrTest {
    public static testmethod void PrintNotesTest(){
        id recType = Schema.SObjectType.contact.getRecordTypeInfosByDeveloperName().get('Contacts').getRecordTypeId();
        contact con = new contact();
        con.recordtypeid=recType;
        con.LastName='contactrec';
        con.FirstName='test';
        con.Email='test@contact.com';
        //con.Race_of_the_Foster_Parent__c='White';
        //con.Gender_Preference__c='Male';
        insert con;
        
        Kinship_Case__c caserec = new Kinship_Case__c();
        caserec.Primary_Contact__c=con.id;
        insert caserec;
        Kinship_Case_Notes__c casenote = new Kinship_Case_Notes__c();
        casenote.Kinship_Case__c=caserec.id;
        casenote.Description__c='test note';
        casenote.Name='adding new note';
        insert casenote;
        ApexPages.StandardController sc = new ApexPages.StandardController(caserec);
        ApexPages.CurrentPage().getParameters().put('id',caserec.id);
        PrintNotesCtr printnoteconrec = new PrintNotesCtr(sc);
        system.assertequals('test',con.firstname);
    }
}