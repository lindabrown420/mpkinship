public class ChartOfAccountTriggerHandler extends TriggerHandler{
	public static Boolean bypassTrigger = false;
    
    public override void afterInsert() {
        if(ChartOfAccountTriggerHandler.bypassTrigger) return;
        List<Chart_of_Account__c> lstNew = (List<Chart_of_Account__c>)Trigger.new;
        Set<Id> setLA = new Set<Id>();
        for(Chart_of_Account__c coa : lstNew) {
            setLA.add(coa.Locality_Account__c);
        }
        List<Category__c> lstCate = [SELECT id, Status__c, Inactive__c FROM Category__c WHERE Locality_Account__c IN :setLA];
        for(Category__c ca : lstCate) {
            ca.Status__c = 'Active';
            ca.Inactive__c = false;
        }
        update lstCate;
    }
    
    public override void beforeUpdate() {
    	if(ChartOfAccountTriggerHandler.bypassTrigger) return;
        List<Chart_of_Account__c> lstNew = (List<Chart_of_Account__c>)Trigger.new;
        Set<Id> setFips = new Set<Id>();
        Set<Id> setCostCenters = new Set<Id>();
        
        for(Chart_of_Account__c coa : lstNew) {
            if(coa.S_O_Default__c == true){
				setFips.add(coa.FIPS__c);
            	setCostCenters.add(coa.Cost_Center__c);
                
            }
        }
        try{
            List<Chart_of_Account__c> lstChart = [Select id from Chart_of_Account__c
                                                  Where FIPS__c IN: setFips AND S_O_Default__c = true
                                                 ];
            if(lstChart.size() > 0){
                for(Chart_of_Account__c CA: lstNew){
                    CA.addError('There is already default one cost center exists for this FIPS');
                }
            } 
        }Catch(Exception e){
            
        }
    }
    
    public override void beforeInsert() {
    	if(ChartOfAccountTriggerHandler.bypassTrigger) return;
        List<Chart_of_Account__c> lstNew = (List<Chart_of_Account__c>)Trigger.new;
        Set<Id> setFips = new Set<Id>();
        Set<Id> setCostCenters = new Set<Id>();
        
        for(Chart_of_Account__c coa : lstNew) {
           if(coa.S_O_Default__c == true){
				setFips.add(coa.FIPS__c);
            	setCostCenters.add(coa.Cost_Center__c);
                
            }
        }
        try{
            List<Chart_of_Account__c> lstChart = [Select id from Chart_of_Account__c
                                                  Where FIPS__c IN: setFips AND S_O_Default__c = true
                                                  ];
            if(lstChart.size() > 0){
                for(Chart_of_Account__c CA: lstNew){
                    CA.addError('There is already default one cost center exists for this FIPS');
                }
            } 
        }Catch(Exception e){
            
        }
    }
}