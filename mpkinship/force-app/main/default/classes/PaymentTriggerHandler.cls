public class PaymentTriggerHandler extends TriggerHandler{
    public static Boolean bypassTrigger = false;
    public static Boolean bypassBTrigger = false;
    public static Boolean bypassOppUpdate = false;
    public static Boolean bypassTJPosted = false;
    /**
     * Called prior to BEFORE trigger. Use to cache any data required
     * input maps prior to execution of the BEFORE trigger
     */
    public override void bulkBefore() {}

    /**
     * Called prior to AFTER trigger. Use to cache any data required
     * input maps prior to execution of the AFTER trigger
     */
    public override void bulkAfter() {}

    /**
     * Handles before insert operations for all records being inserted.
     * Should only be used in BEFORE Trigger context
     */
    public override void beforeInsert() {
        if(PaymentTriggerHandler.bypassTrigger) return;
        TransactionJournalService.autoFillFYOnInvoice(Trigger.new, null);
    }

    /**
     * Handles after insert operations for all records being inserted.
     * Should only be used in AFTER Trigger context
     */
    public override void afterInsert() {
        //COMMENTED AS TJ WILL BE CREATED ON UPDATE OF PAYMENT
       /* if(PaymentTriggerHandler.bypassTrigger) return;
        TransactionJournalService.createTJBaseOnPayment(Trigger.new);  */
        if(PaymentTriggerHandler.bypassTrigger) return;
        TransactionJournalService.createTJBaseOnPayment(Trigger.new, null);
    }

    /**
     * Handles before update operations for all records being updated.
     * Should only be used in BEFORE Trigger context
     */
    public override void beforeUpdate() {
        if(PaymentTriggerHandler.bypassTrigger) return;
        TransactionJournalService.autoFillFYOnInvoice(Trigger.new, Trigger.old);
        if(!PaymentTriggerHandler.bypassTJPosted) {
            TransactionJournalService.checkPostedAndClosedTJ(Trigger.new);
        }
    }

    /**
     * Handles after update operations for all records being updated.
     * Should only be used in AFTER Trigger context
     */
    public override void afterUpdate() {
        if(PaymentTriggerHandler.bypassTrigger) return;
        //TransactionJournalService.UpdateTJBaseOnPayment(Trigger.new);
        //ADDED BY BHAVNA
        TransactionJournalService.createTJBaseOnPayment(Trigger.new, Trigger.old);
        //TransactionJournalService.updateAPTBaseOnInvoice(Trigger.new, Trigger.old);
        
        // KIN-1210
        List<Payment__c> lstNew = (List<Payment__c>)Trigger.new;
        List<Payment__c> lstOld = (List<Payment__c>)Trigger.old;
        List<Payment__c> lstAmountChanged = new List<Payment__c>();
        Set<String> setId = new Set<String>();
        Map<Id, Decimal> mapOldAmount = new Map<Id, Decimal>();
        for(Integer i = 0 ; i < lstNew.size() ; i++) {
            if(lstNew[i].Payment_Amount__c != lstOld[i].Payment_Amount__c && String.isNotBlank(lstNew[i].Kinship_Check__c)) {
                if(lstNew[i].Invoice_Stage__c != 'Cancelled') {
                    lstAmountChanged.add(lstNew[i]);
                    setId.add(lstNew[i].Kinship_Check__c);
                    mapOldAmount.put(lstNew[i].Id, lstOld[i].Payment_Amount__c);
                } else {
                    lstNew[i].addError(Label.ERROR_CHANGE_AMOUNT);
                }
            }
        }
        System.debug('--afterUpdate--' + lstAmountChanged);
        if(lstAmountChanged.size() > 0) {
            List<Kinship_Check__c> lstCheck = [SELECT id, Amount__c, Check_Run__c, Name, Name__c FROM Kinship_Check__c WHERE Id IN :setId];
            Map<String, Kinship_Check__c> mapIdToCheck = new Map<String, Kinship_Check__c>();
            setId = new Set<String>();
            for(Kinship_Check__c check : lstCheck) {
                mapIdToCheck.put(check.id, check);
                setId.add(check.Check_Run__c);
            }
            Map<String, Check_Run__c> mapIdToCheckRun = new Map<String, Check_Run__c>();
            for(Check_Run__c cr : [SELECT id, Check_Run_Total__c FROM Check_Run__c WHERE Id IN :setId]) {
                mapIdToCheckRun.put(cr.id, cr);
            }
            List<Kinship_Check__c> lstCheckUpdate = new List<Kinship_Check__c>();
            List<Check_Run__c> lstCheckRunUpdate = new List<Check_Run__c>();
            setId = new Set<String>();
            Decimal diff = 0;
            for(Payment__c payment : lstAmountChanged) {
                Kinship_Check__c check = mapIdToCheck.get(payment.Kinship_Check__c);
                if(check != null) {
                    diff = payment.Payment_Amount__c - check.Amount__c;
                    check.Amount__c += (payment.Payment_Amount__c - mapOldAmount.get(payment.id));
                    check.Name = check.Name__c + ' ' + check.Amount__c;
                    lstCheckUpdate.add(check);
                    Check_Run__c checkRun = mapIdToCheckRun.get(check.Check_Run__c);
                    if(checkRun != null) {
                        checkRun.Check_Run_Total__c += diff;
                        if(!setId.contains(checkRun.Id)) {
                            lstCheckRunUpdate.add(checkRun);
                        }
                        setId.add(checkRun.Id);
                    }
                }
            }
            if(lstCheckUpdate.size() > 0){
                update lstCheckUpdate;
            }
            if(lstCheckRunUpdate.size() > 0) {
                update lstCheckRunUpdate;
            }
        }
    }

    /**
     * Handles before delete operations for all records being deleted.
     * Should only be used in BEFORE Trigger context
     */
    public override void beforeDelete() {
        if(PaymentTriggerHandler.bypassTrigger) return;
        if(!PaymentTriggerHandler.bypassTJPosted) {
            TransactionJournalService.checkPostedAndClosedTJ(Trigger.old);

            // can't delete if there is a Check link to it
            List<Payment__c> lstPayment = (List<Payment__c>)Trigger.old;
            for(Payment__c payment : lstPayment) {
                if(String.isNotBlank(payment.Kinship_Check__c)) {
                    payment.addError(Label.ERROR_DELETE_PAYMENT_WITH_CHECK);
                }
            }
        }
    }

    /**
     * Handles after delete operations for all records being deleted.
     * Should only be used in AFTER Trigger context
     */
    public override void afterDelete() {}

    /**
     * Call once all records have been processed by the trigger handler.
     * Use this to execute all final operations like DML of other records.
     */
    public override void andFinally() {
        //upsert or insert records
        //this.insertHistory();
    }
}