public class DepositSlipController {
    public id collId;
   
    public Decimal cash{get; set;}
    public Decimal MO{get; set;}
    public Decimal check{get; set;}
    public Decimal TotalAmount{get; set;}
    public String createdby{get; set;}
    public String lastmodified{get; set;}
    public Decimal payTotal {get; set;}
    public List<Opportunity> oppLists{get; set;}
    public List<Payment__c> receiptsLists{get; set;}
    public Deposits__c deposit{get; set;}
    public Date depositDate{get; set;}
    public DepositSlipController(){}
    
    
    public Decimal TotalAllSWA {get; set;}
     public Decimal TotalNOTSWA {get; set;}
    public DepositSlipController(ApexPages.StandardController stdController) {
        cash        = 0.00;
        check       = 0.00;
        MO          = 0.00;
        TotalAmount = 0.00;
        
        
        payTotal = 0.00;
        this.collId = ApexPages.currentPage().getParameters().get('id');
        
        try{  
            deposit  = [Select Id,createdby.Name,lastmodifiedby.Name,Deposit_Date__c from Deposits__c where Id =: this.collId]; 
            createdby = deposit.createdby.Name;
            lastmodified = deposit.lastmodifiedby.Name;
            depositDate = deposit.Deposit_Date__c;
            oppLists = [Select Id from Opportunity where deposit__c =: this.collId];
            /*receiptsLists = [select id,name,Payment_Date__c,Pay_Method__c,Payment_Amount__c from Payment__c where Opportunity__c IN: oppLists AND 
                            (Payment__c.RecordType.Name = 'Receipt' OR Payment__c.RecordType.Name = 'Vendor Refund' OR Payment__c.RecordType.Name = 'Special Welfare Reimbursement')
                            order by Payment_Date__c]; */
            receiptsLists = [select id,name,Payment_Date__c,Payment_Method__c,Payment_Amount__c from Payment__c where Opportunity__c IN: oppLists AND 
                            (Payment__c.RecordType.Name = 'Receipt' OR Payment__c.RecordType.Name = 'Vendor Refund' OR Payment__c.RecordType.Name = 'Special Welfare Reimbursement')
                            order by Payment_Date__c];                
            System.debug(receiptsLists +'receiptsList ');
            for(Payment__c rc: receiptsLists) {
                System.debug(rc.Payment_Method__c+' rc.Payment_Method__c');
                 System.debug(rc.Payment_Amount__c +' rc.Payment_Amount__c ');
                //if(rc.Pay_Method__c == 'Cash') {
                if(rc.Payment_Method__c== 'Cash') {
                    if(rc.Payment_Amount__c != null)
                        cash += rc.Payment_Amount__c;
                }
               // else if(rc.Pay_Method__c == 'Check')
                else if(rc.Payment_Method__c== 'Check')
                {
                    if(rc.Payment_Amount__c != null)
                        check += rc.Payment_Amount__c;
                }
                //else if(rc.Pay_Method__c == 'Money Order')
                else if(rc.Payment_Method__c== 'Money Order')
                {
                    if(rc.Payment_Amount__c != null)
                        MO += rc.Payment_Amount__c;
                }
                payTotal = cash + check + MO; 
                System.debug(payTotal +' payTotal ');
                System.debug(cash +' cash ');
            }
           
            
        }catch(Exception e){
           system.debug('***Exception e'+e);
        }
    }
}