public class FIPSTriggerHandler  extends TriggerHandler{
    public static Boolean bypassTrigger = false;
    /**
     * Called prior to BEFORE trigger. Use to cache any data required
     * input maps prior to execution of the BEFORE trigger
     */
    public override void bulkBefore() {}

    /**
     * Called prior to AFTER trigger. Use to cache any data required
     * input maps prior to execution of the AFTER trigger
     */
    public override void bulkAfter() {}

    /**
     * Handles before insert operations for all records being inserted.
     * Should only be used in BEFORE Trigger context
     */
    public override void beforeInsert() {
    }

    /**
     * Handles after insert operations for all records being inserted.
     * Should only be used in AFTER Trigger context
     */
    public override void afterInsert() {
    }

    /**
     * Handles before update operations for all records being updated.
     * Should only be used in BEFORE Trigger context
     */
    public override void beforeUpdate() {
    }

    /**
     * Handles after update operations for all records being updated.
     * Should only be used in AFTER Trigger context
     */
    public override void afterUpdate() {
        if(FIPSTriggerHandler.bypassTrigger) return;
        List<FIPS__c> lstNew = (List<FIPS__c>)Trigger.new;
        List<FIPS__c> lstOld = (List<FIPS__c>)Trigger.old;
        Set<String> setId =  new Set<String>();
        for(Integer i = 0 ; i < lstNew.size() ; i++) {
            if(lstNew[i].Locality__c != lstOld[i].Locality__c){
                setId.add(lstNew[i].id);
            }
        }
        if(setId.size() > 0) {
            List<Campaign> lstCam = [SELECT id, Name, FY_Name__c FROM Campaign WHERE FIPS__c IN :setId];
            for(Campaign c : lstCam) {
                c.Name = c.FY_Name__c;
            }
            if(lstCam.size() > 0) {
                update lstCam;
            }
        }
    }

    /**
     * Handles before delete operations for all records being deleted.
     * Should only be used in BEFORE Trigger context
     */
    public override void beforeDelete() {
    }

    /**
     * Handles after delete operations for all records being deleted.
     * Should only be used in AFTER Trigger context
     */
    public override void afterDelete() {}

    /**
     * Call once all records have been processed by the trigger handler.
     * Use this to execute all final operations like DML of other records.
     */
    public override void andFinally() {
        //upsert or insert records
        //this.insertHistory();
    }
}