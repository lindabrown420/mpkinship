public class KinshipUtil {
    public static List<sObject> cloneObjects(List<sObject> sObjects,
                                        Schema.SObjectType objectType){
        List<Id> sObjectIds = new List<Id>{};
        List<String> sObjectFields = new List<String>{};
        List<sObject> clonedSObjects = new List<sObject>{};

        if(objectType != null){
            sObjectFields.addAll(
            objectType.getDescribe().fields.getMap().keySet());
        }

        if (sObjects != null && 
            !sObjects.isEmpty() && 
            !sObjectFields.isEmpty()){

            for (sObject objectInstance: sObjects){
            sObjectIds.add(objectInstance.Id);
            }

            String allSObjectFieldsQuery = 'SELECT ' + sObjectFields.get(0); 

            for (Integer i=1 ; i < sObjectFields.size() ; i++){
            allSObjectFieldsQuery += ', ' + sObjectFields.get(i);
            }

            allSObjectFieldsQuery += ' FROM ' + 
                                    objectType.getDescribe().getName() + 
                                    ' WHERE ID IN (\'' + sObjectIds.get(0) + 
                                    '\'';

            for (Integer i=1 ; i < sObjectIds.size() ; i++){
            allSObjectFieldsQuery += ', \'' + sObjectIds.get(i) + '\'';
            }

            allSObjectFieldsQuery += ')';

            try{
            
                for (SObject sObjectFromDatabase:
                        Database.query(allSObjectFieldsQuery)){
                    clonedSObjects.add(sObjectFromDatabase.clone(false,true));  
                }

            } catch (exception e){
            }
        }    
        return clonedSObjects;
    }
    
    public static String getMaxLength(String str, Integer maxlength) {
        if (!String.isBlank(str) && str.length() > maxlength) {
            return str.substring(0, maxlength);
        } else {
            return str;
        }
    }
    
    public static Decimal getAmountValue(Decimal d) {
        if(d < 0) {
            return -d;
        }
        return d;
    }
}