public class SPR_Report {
    public static List<SPRReportingPeriod__c> lstRP{
        get{
           if (lstRP == null)
            {
                lstRP = SPRReportingPeriod__c.getall().values();
            }
            return lstRP; 
        }
        private set;
    }
    public static List<XLSXGenerator.caseWrapper> getData(List<String> caseId, String startDate, String endDate){
        List<String> lstStage = new List<String>();
        lstStage.add('Paid');
        lstStage.add('Posted');
        lstStage.add('Cancelled');
        List<Payment__c> pay = new List<Payment__c>();
        if((startDate != null && startDate != '') && (endDate != null && endDate != '')){
            pay = [Select id,Kinship_Case__c,Kinship_Case__r.ssn__c,Kinship_Case__r.OASIS_Client_ID__c,Category_Name__c,Service_Begin_Date__c,Service_End_Date__c,Category_lookup__r.LASER_Cost_Code__r.Name,
                   PO_Number__c,Payment_Date__c	,Check_Number__c,Payment_Amount__c,Category_lookup__c,Category_lookup__r.Category_Type__c,Category_lookup__r.Name,Description__c,Vendor__r.Name,
                   Category_lookup__r.Service_Names_Code__r.SPR_Service_Description__r.Name,Category_lookup__r.RecordType.Name,Kinship_Case__r.oasis_number_clone__c,
                   Name,Opportunity__r.Legacy_Category_Code__c,Category_lookup__r.LASER_Cost_Code__r.Cost_Code_Title__c
                   from Payment__c where Kinship_Case__c IN :caseId AND Invoice_Stage__c IN :lstStage And (Payment_Date__c>=:Date.parse(startDate) AND Payment_Date__c	 <=: Date.parse(endDate))];  
        }
        else if(startDate != null && startDate != ''){
            pay = [Select id,Kinship_Case__c,Kinship_Case__r.ssn__c,Kinship_Case__r.OASIS_Client_ID__c,Category_Name__c,Service_Begin_Date__c,Service_End_Date__c,Category_lookup__r.LASER_Cost_Code__r.Name,
                   PO_Number__c,Payment_Date__c	,Check_Number__c,Payment_Amount__c,Category_lookup__c,Category_lookup__r.Category_Type__c,Category_lookup__r.Name,Description__c,Vendor__r.Name,
                   Category_lookup__r.Service_Names_Code__r.SPR_Service_Description__r.Name,Category_lookup__r.RecordType.Name,Kinship_Case__r.oasis_number_clone__c,
                   Name,Opportunity__r.Legacy_Category_Code__c,Category_lookup__r.LASER_Cost_Code__r.Cost_Code_Title__c
                   from Payment__c where Kinship_Case__c IN :caseId AND Invoice_Stage__c IN :lstStage And (Payment_Date__c	 >=:Date.parse(startDate))];  
        }
        else if(endDate != null && endDate != ''){
            pay = [Select id,Kinship_Case__c,Kinship_Case__r.ssn__c,Kinship_Case__r.OASIS_Client_ID__c,Category_Name__c,Service_Begin_Date__c,Service_End_Date__c,Category_lookup__r.LASER_Cost_Code__r.Name,
                   PO_Number__c,Payment_Date__c	,Check_Number__c,Payment_Amount__c,Category_lookup__c,Category_lookup__r.Category_Type__c,Category_lookup__r.Name,Description__c,Vendor__r.Name,
                   Category_lookup__r.Service_Names_Code__r.SPR_Service_Description__r.Name,Category_lookup__r.RecordType.Name,Kinship_Case__r.oasis_number_clone__c,
                   Name,Opportunity__r.Legacy_Category_Code__c,Category_lookup__r.LASER_Cost_Code__r.Cost_Code_Title__c
                   from Payment__c where Kinship_Case__c IN :caseId AND Invoice_Stage__c IN :lstStage And (Payment_Date__c	 <=: Date.parse(endDate))];  
        }
        else{
            pay = [Select id,Kinship_Case__c,Kinship_Case__r.ssn__c,Kinship_Case__r.OASIS_Client_ID__c,Category_Name__c,Service_Begin_Date__c,Service_End_Date__c,Category_lookup__r.LASER_Cost_Code__r.Name,
                   PO_Number__c,Payment_Date__c	,Check_Number__c,Payment_Amount__c,Category_lookup__c,Category_lookup__r.Category_Type__c,Category_lookup__r.Name,Description__c,Vendor__r.Name,
                   Category_lookup__r.Service_Names_Code__r.SPR_Service_Description__r.Name,Category_lookup__r.RecordType.Name,Kinship_Case__r.oasis_number_clone__c,
                   Name,Opportunity__r.Legacy_Category_Code__c,Category_lookup__r.LASER_Cost_Code__r.Cost_Code_Title__c
                   from Payment__c where Kinship_Case__c IN :caseId AND Invoice_Stage__c IN :lstStage];  
        }
        System.debug('pay '+pay);
        XLSXGenerator.caseWrapper cw = new XLSXGenerator.caseWrapper();
        List<XLSXGenerator.caseWrapper> cwList = new List<XLSXGenerator.caseWrapper>();
        Integer i = 1;
        for(Payment__c payment:pay){
            cw = new XLSXGenerator.caseWrapper();
            cw.no = String.valueOf(i);
            //cw.ssn;
            cw.CaseNumber = String.valueOf(payment.Kinship_Case__r.oasis_number_clone__c);
            if(cw.CaseNumber != null){
                cw.CaseNumber = '　' + cw.CaseNumber;
                //cw.ssn  = replaceSpecialCharacter(cw.ssn);
            }
            cw.OASISClientID = String.valueOf(payment.Kinship_Case__r.OASIS_Client_ID__c);
            if(cw.OASISClientID != null){
                //cw.OASISClientID  = replaceSpecialCharacter(cw.OASISClientID);
            }
            cw.ServiceBeginDate = formatDate(payment.Service_Begin_Date__c);
            if(cw.ServiceBeginDate != null){
                //cw.ServiceBeginDate  = replaceSpecialCharacter(cw.ServiceBeginDate);
            }
            cw.ServiceEndDate = formatDate(payment.Service_End_Date__c);
            if(cw.ServiceEndDate != null){
                // cw.ServiceEndDate  = replaceSpecialCharacter(cw.ServiceEndDate);
            }
            cw.PONumber = String.valueOf(payment.Name) + '/' + String.valueOf(payment.PO_Number__c);
            if(cw.PONumber  != null){
                //cw.PONumber  = replaceSpecialCharacter(cw.PONumber);
            }
            cw.npe01PaymentDate = formatDate(payment.Payment_Date__c	);
            if(cw.npe01PaymentDate  != null){
                // cw.npe01PaymentDate  = replaceSpecialCharacter(cw.npe01PaymentDate);
            }
            cw.CheckNumber = String.valueOf(payment.Check_Number__c);
            System.debug('---cwCheckNumber--'+payment.Check_Number__c);
            if(cw.CheckNumber != null){
                cw.CheckNumber = '　' + cw.CheckNumber;
                //cw.CheckNumber  = replaceSpecialCharacter(cw.CheckNumber);
            }
            cw.Categorylookup = String.valueOf(payment.Category_lookup__c);
            if(cw.Categorylookup  != null){
                // cw.Categorylookup = replaceSpecialCharacter(cw.Categorylookup);
            }
            cw.Description = String.valueOf(payment.Description__c);
            if(cw.Description != null){
                cw.Description = replaceSpecialCharacter(cw.Description);
            }
            cw.VendorName = String.valueOf(payment.Vendor__r.Name);
            if(cw.VendorName != null){
                cw.VendorName  = replaceSpecialCharacter(cw.VendorName);
            }
            
            //cw.CategoryName1 = String.valueOf(payment.Category_lookup__r.Name);
            cw.CategoryName1 = String.valueOf(payment.Category_lookup__r.Service_Names_Code__r.SPR_Service_Description__r.Name);
            if(String.isBlank(cw.CategoryName1)){
                String codeTitle = payment.Category_lookup__r.LASER_Cost_Code__r.Cost_Code_Title__c;
                if(String.isNotBlank(codeTitle)) {
                    if(codeTitle.toLowerCase().contains('basic maintenance')) {
                    	cw.CategoryName1 = 'Maintenance Payment';
                    } else if(codeTitle.toLowerCase().contains('enhanced maintenance')) {
                    	cw.CategoryName1 = 'Enhanced Maintenance for Additional Daily Supervision';
                    } else {
                        cw.CategoryName1 = 'Other: Describe in Notes Column';
                    }
                }  else {
                    cw.CategoryName1 = 'Other: Describe in Notes Column';
                }
            }
            cw.LASERCostCode = String.valueOf(payment.Category_lookup__r.LASER_Cost_Code__r.Name);
            if(cw.LASERCostCode  != null){
                // cw.LASERCostCode = replaceSpecialCharacter(cw.LASERCostCode);
            }
            if(String.isBlank(payment.Category_lookup__c)) {
                cw.LASERCostCode = payment.Opportunity__r.Legacy_Category_Code__c;
            }
            if(payment.Category_lookup__r.RecordType.Name == 'LEDRS Category') {
                cw.LASERCostCode = 'CSA';
                cw.CategoryName1 = '';
            } else if(payment.Category_lookup__r.RecordType.Name == 'LOCAL Category') {
                cw.LASERCostCode = 'OTHER';
                cw.CategoryName1 = 'Other: Describe in Notes Column';
            }
            
            cw.AmountofLASERAdjustmentValidated = '';
            
            cw.FederalReportingPeriod = getReportingPeriod(payment.Payment_Date__c	, lstRP);
            cw.PaymentValidity = '';
            cw.NoteSUO = '';
            if(payment.Category_lookup__r.Category_Type__c == 'IVE') {
                cw.TitleIVEPaymentAmount = String.valueOf(payment.Payment_Amount__c	);
            } else if(payment.Category_lookup__r.Category_Type__c == 'CSA'){
                cw.CSAPaymentAmount = String.valueOf(payment.Payment_Amount__c);
            }
            if(payment.Category_lookup__r.Category_Type__c != 'IVE' && payment.Category_lookup__r.Category_Type__c != 'CSA'){
                cw.OtherPaymentAmount =String.valueOf(payment.Payment_Amount__c);
                cw.OtherPaymentSource = '';//String.valueOf(payment.Payment_Amount__c);
            }
            else{
                cw.OtherPaymentAmount = '';
                cw.OtherPaymentSource = '';
            }
            
            cw.AmountinError = '';
            cwList.add(cw);
            
            i++;
            System.debug('---cwCheckNumber--'+cw.CheckNumber);
            System.debug('---cw--'+cw);
            System.debug('---i--'+i);
        }
        System.debug('---cwList'+cwList);
        return cwList;
        
        //XLSXGenerator.generate(cwList);
    }
    public static String getReportingPeriod(Date paymentDate, List<SPRReportingPeriod__c> lstRP) {
        for(SPRReportingPeriod__c rp : lstRP) {
            if(rp.StartDate__c <= paymentDate && rp.EndDate__c >= paymentDate) {
                return rp.Name;
            }
        }
        return '';
    }
    public static String formatDate(Date paymentDate){
        if(paymentDate != null) {
            String month = String.valueOf(paymentDate.month());
            if(month.length() == 1) {
                month = '0' + month;
            }
            String day = String.valueOf(paymentDate.day());
            if(day.length() == 1) {
                day = '0' + day;
            }
            return month + '/' + day + '/' + String.valueOf(paymentDate.year()).right(2);
        }
        return '';
    }
    public static String replaceSpecialCharacter(String s){
        s = s.replace('&', '');
        //s = s.replace('-', ' ');
        /*s = s.replace(':', '');
        s = s.replace(';', '');
        s = s.replace('-', '');
        s = s.replace('|', '');
        */
        /*s = s.replace('<', '<');
        s = s.replace('>', '>');
        s = s.replace('"', '"');
        s =s.replace('¢', '¢');
        s = s.replace('£', '£');
        s =s.replace('¥', '¥');
        s = s.replace('€', '€');
        s = s.replace('©','©');
        s = s.replace('®', '®');
        s = s.replace('|', ' ');
        s = s.replace(';', ' ');
        s = s.replace(':', ' ');*/
        return s;
    }
    /*public class caseWrapper{
        public string ssn;
        public string OASISClientID;
        public string LASERCostCode;
        public string CategoryName;
        public string ServiceBeginDate;
        public string ServiceEndDate;
        public string PONumber;
        public string FederalReportingPeriod;
        public string npe01PaymentDate;
        public string CheckNumber;
        public string npe01PaymentAmount;
        public string Categorylookup;
        public string PaymentValidity;
        public string Description;
        public string VendorName;
        public string SPTLabel;
        public string AmountinError;
        public string AmountofLASERAdjustmentValidated;
        public string SPTNameLabel;
        public string categoryRateWrap;
        public string SPTCatRate;
        public string TitleIVEPaymentAmount;
        public string CSAPaymentAmount;
        public string OtherPaymentAmount;
        public string OtherPaymentSource;
        
        
        
     }*/
}