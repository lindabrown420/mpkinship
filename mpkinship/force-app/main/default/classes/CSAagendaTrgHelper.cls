/**********
Helper class for CSAagendaTrg trigger
*********/
public class CSAagendaTrgHelper{
    public static boolean isRun=false;
    public static boolean IsSendRun=false;
    
    //SEND CANCELLED EMAIL
    public static void SendCancelledEmail(map<id,CSA_Meeting_Agenda__c> csaAgendaMap){
        isRun=true;
        map<id,list<CSA_Meeting_Members__c>> csagendaMembersMap = new map<id,list<CSA_Meeting_Members__c>>();
        csagendaMembersMap= getAgendamap(csaAgendaMap);
       
       List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
       for(id agendaid : csaAgendaMap.keyset()){
           if(csagendaMembersMap.containskey(agendaid)){
               for(CSA_Meeting_Members__c cm : csagendaMembersMap.get(agendaid)){
                   Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();  
                    message.toAddresses = new String[] {cm.contact__r.email};  
                    string htmlbody=getCancelledEmailBody(cm.contact__r.name,cm.CSA_Meeting_Agenda__r.Meeting_Date__c,cm.CSA_Meeting_Agenda__r.Location_of_Meeting__c,
                                            cm.CSA_Meeting_Agenda__r.Meeting_Start3_Time__c ); 
                    message.setHtmlBody(htmlbody);
                    message.setSubject('CSA Team Meeting--Cancelled');
                    allmsg.add(message);  
               }
           }
       }
        try{    
            Messaging.SendEmailResult[] results = Messaging.sendEmail(allmsg,false);
 
         }
         catch(Exception e){
             system.debug('**Exception'+e);
         }
    }   
       
       
      //SEND CONFIRMED NOTIFICATION
      public static void SendNotificationEmail(map<id,CSA_Meeting_Agenda__c> csaAgendaConfirmMap){
          IsSendRun=true;
          map<id,list<CSA_Meeting_Members__c>> csagendaMembersMap = new map<id,list<CSA_Meeting_Members__c>>();
          csagendaMembersMap= getAgendamap(csaAgendaConfirmMap);
          List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
          for(id agendaid : csaAgendaConfirmMap.keyset()){
              if(csagendaMembersMap.containskey(agendaid)){
                  for(CSA_Meeting_Members__c cm : csagendaMembersMap.get(agendaid)){
                      Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();  
                      message.toAddresses = new String[] {cm.contact__r.email};  
                      string htmlbody=getConfirmedEmailBody(cm.contact__r.name,cm.CSA_Meeting_Agenda__r.Meeting_Date__c,cm.CSA_Meeting_Agenda__r.Location_of_Meeting__c,
                                                              cm.CSA_Meeting_Agenda__r.Meeting_Start3_Time__c); 
                      message.setHtmlBody(htmlbody);
                      message.setSubject('CSA Team Meeting');
                      allmsg.add(message);  
                  }
              }
         }
         try{    
            Messaging.SendEmailResult[] results = Messaging.sendEmail(allmsg,false);
        
        }
        catch(Exception e){
            system.debug('**Exception'+e);
        }
    }
      
      //GETTING AGENDA AND MEMBERS MAP
      public static map<id,list<CSA_Meeting_Members__c>> getAgendamap(map<id,CSA_Meeting_Agenda__c> csaAgendaMap){
           map<id,list<CSA_Meeting_Members__c>> agendaMap = new map<id,list<CSA_Meeting_Members__c>>();
          list<CSA_Meeting_Members__c> csameetingmembers=[select id,Contact__c,contact__r.name,CSA_Meeting_Agenda__c,contact__r.email,
                                    CSA_Meeting_Agenda__r.Location_of_Meeting__c,CSA_Meeting_Agenda__r.Meeting_Date__c,
                                    CSA_Meeting_Agenda__r.Meeting_Start3_Time__c from CSA_Meeting_Members__c
                                    where CSA_Meeting_Agenda__c IN: csaAgendaMap.keyset() and contact__r.email!=:null];
        for(CSA_Meeting_Members__c cm : csameetingmembers){
            if(agendaMap.containskey(cm.CSA_Meeting_Agenda__c))
                agendaMap.get(cm.CSA_Meeting_Agenda__c).add(cm);
            else
                agendaMap.put(cm.CSA_Meeting_Agenda__c, new list<CSA_Meeting_Members__c>{cm});                                                   
        } 
        return agendaMap;
      
    }       
    
     //CANCELLED EMAIL BODY
        public static string getCancelledEmailBody(string name,date meetingdate, string meetinglocation,string meetingtime){
            //DATE CONVERSION
            string dateStr='';
            if(meetingdate!=null)
                dateStr = meetingdate.day() + '/' + meetingdate.month() + '/' + meetingdate.year();
            list<string> timeval= new list<string>();
            String strConvertedDate='';
            if(meetingtime!=null){
               timeval= meetingtime.split(':'); 
            }
            if(timeval.size()>0){
                time t= Time.newInstance(Integer.valueOf(timeval[0]), Integer.valueOf(timeval[1]), 0, 0);
                DateTime dt = DateTime.newInstance(meetingdate, t);
                Time myGmtTime = dt.timeGMT();
                System.debug('GMT Time'+dt.timeGMT());
                DateTime dtt = DateTime.newInstanceGmt(meetingdate, myGmtTime);
                strConvertedDate = dtt.format('MM/dd/yyyy HH:mm:ss a', 'America/Los_Angeles');
                System.debug('Converted time'+strConvertedDate); 
            }
             string body='';
             body = '<html><body> Hi <b>'+ name+',</b><br/> '; 
             body+='The Scheduled team meeting on ';
             if(meetingtime!=null && strConvertedDate!=''){
                  body+= strConvertedDate+' PST';    
             }
             else{
                 body+= dateStr;
             }
             if(meetinglocation!=null && meetinglocation!='')
                 body+=' at '+meetinglocation+' has been cancelled now.</body> </html>'; 
             else
                 body+=' has been cancelled now.</body> </html>';
             return body;  
        }
      
      //CONFIRMED EMAIL BODY
      public static string getConfirmedEmailBody(string name,date meetingdate, string meetinglocation, string meetingtime){  
          //DATE CONVERSION
            string dateStr='';
            if(meetingdate!=null)
                dateStr = meetingdate.day() + '/' + meetingdate.month() + '/' + meetingdate.year();
            list<string> timeval= new list<string>();
            String strConvertedDate='';
            if(meetingtime!=null){
               timeval= meetingtime.split(':'); 
            }
          if(timeval.size()>0){
            time t= Time.newInstance(Integer.valueOf(timeval[0]), Integer.valueOf(timeval[1]), 0, 0);
            DateTime dt = DateTime.newInstance(meetingdate, t);
            Time myGmtTime = dt.timeGMT();
            System.debug('GMT Time'+dt.timeGMT());
            DateTime dtt = DateTime.newInstanceGmt(meetingdate, myGmtTime);
            strConvertedDate = dtt.format('MM/dd/yyyy HH:mm:ss a', 'America/Los_Angeles');
            System.debug('Converted time'+strConvertedDate); 
         }          
         string body='';
             body = '<html><body> Hi <b>'+ name+',</b><br/> '; 
             body+='You are invited to attend the meeting on ';
             if(meetingtime!=null && strConvertedDate!=''){
                  body+= strConvertedDate+' PST';    
             }
             else{
                 body+= dateStr;
             }
             if(meetinglocation!=null && meetinglocation!='')
                 body+=' at '+meetinglocation+'.</body> </html>'; 
             else
                 body+='.</body> </html>';
                  
         return body;  
      }
                             
   
}