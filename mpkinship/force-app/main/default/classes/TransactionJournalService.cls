public class TransactionJournalService {
    public static Boolean byPassCheckPosted = false;
    public static Boolean byPassCampaignUpdate = false;
    public static final String POSTED = 'Posted';
    public static final String CLOSED = 'Closed';
    public static final String CANCELLED = 'Cancelled';
    public static final String UNCANCELLED = 'Uncancelled';
    public static final String PAID = 'Paid';
    public static final String PENDING = 'Pending';
  
    public static void createTJBaseOnPayment(List<Payment__c> lstPayment, List<Payment__c> lstOld) {
        List<Transaction_Journal__c> lstTJ = new List<Transaction_Journal__c>();
        String cateLEDRS = Schema.SObjectType.Category__c.getRecordTypeInfosByName().get('LEDRS Category').getRecordTypeId();
        String recordTypeIdLASER = Schema.SObjectType.Transaction_Journal__c.getRecordTypeInfosByName().get('LASER').getRecordTypeId();
        String recordTypeIdLEDRS = Schema.SObjectType.Transaction_Journal__c.getRecordTypeInfosByName().get('LEDRS').getRecordTypeId();
        String recordTypeIdOther = Schema.SObjectType.Transaction_Journal__c.getRecordTypeInfosByName().get('Other').getRecordTypeId();
        List<Payment__c> payments = new list<Payment__c>();
        Set<Id> setId = new Set<Id>();
        if(lstOld != null) {
            for(Integer i = 0 ; i < lstPayment.size() ; i++) {
                if(lstPayment[i].Invoices_Status__c != lstOld[i].Invoices_Status__c && lstPayment[i].Invoices_Status__c == POSTED){
                    payments.add(lstPayment[i]);
                    setId.add(lstPayment[i].id);
                }
            }
        } else {
            for(Integer i = 0 ; i < lstPayment.size() ; i++) {
                if(lstPayment[i].Invoices_Status__c == POSTED){
                    payments.add(lstPayment[i]);
                    setId.add(lstPayment[i].id);
                }
            }
        }
        if(payments.size() > 0){
            Map<String, String> mapPaymentToLA = new Map<String, String>();
            Set<id> setLA = new Set<Id>();
            for(Payment__c p : [SELECT id, Category_lookup__r.Locality_Account__c FROM Payment__c WHERE Id IN :setId]){
                mapPaymentToLA.put(p.id, p.Category_lookup__r.Locality_Account__c);
                setLA.add(p.Category_lookup__r.Locality_Account__c);
            }
            Map<String, Chart_of_Account__c> mapCOA = new Map<String, Chart_of_Account__c>();
            for(Chart_of_Account__c p : [SELECT Id, Name, Cost_Center__c, Locality_Account__c, FIPS__c FROM Chart_of_Account__c WHERE Locality_Account__c IN :setLA]){
                mapCOA.put(p.Locality_Account__c + ':' + p.FIPS__c, p);
            }
            System.debug('---createTJBaseOnPayment--' + payments.size());
            Map<Id, Id> mapPaymentToCate = new Map<Id, Id>();
            Chart_of_Account__c coatemp;
            for(Payment__c p : Payments) {
                Transaction_Journal__c tj = new Transaction_Journal__c();
                tj.RecordTypeId = recordTypeIdLASER;
                tj.Invoice__c = p.id;
                if(Label.KINSHIP_RECEIPT_ID.contains(p.RecordTypeId) || p.Payment_Amount__c < 0) {
                    tj.Credit__c = KinshipUtil.getAmountValue(p.Payment_Amount__c);
                } else {
                    tj.Debit__c = p.Payment_Amount__c;
                }
                tj.Status__c = POSTED;
                tj.Transaction_Description__c = p.Other_Transaction_Description__c;
                tj.Child_Campaign__c = p.Accounting_Period__c;
                tj.Vendor_Name__c = p.Vendor__c;
                coatemp = mapCOA.get(mapPaymentToLA.get(p.id) + ':' + p.FIPS_Code__c);
                if(coatemp != null) {
                    tj.Chart_of_Account__c = coatemp.id;
                }
                lstTJ.add(tj);
                mapPaymentToCate.put(p.Id, p.Category_lookup__c);
            }
            if(lstTJ.size() > 0) {
                List<Category__c> lstCate = [SELECT id, RecordTypeId,Locality_Account__c, Category_Type__c  FROM Category__c WHERE Id IN :mapPaymentToCate.values()];
                Map<Id, Category__c> mapCateToId = new Map<Id, Category__c>();
                for(Category__c cate : lstCate) {
                    mapCateToId.put(cate.Id, cate);
                }
                Category__c cTemp;
                for(Transaction_Journal__c tj : lstTJ){
                    Id temp = mapPaymentToCate.get(tj.Invoice__c);
                    System.debug('---createTJBaseOnPayment--' + temp);
                    if(temp != null){
                        cTemp = mapCateToId.get(temp);
                        System.debug('---createTJBaseOnPayment--' + cTemp);
                        if(cTemp != null) {
                            if(String.isNotBlank(cTemp.Category_Type__c) && Label.KINSHIP_TJ_OTHER.contains(cTemp.Category_Type__c)) {
                                tj.recordTypeId = recordTypeIdOther;
                            } else if(cTemp.RecordTypeId == cateLEDRS){
                                tj.recordTypeId = recordTypeIdLEDRS;
                            }
                            tj.Locality_Code__c = cTemp.Locality_Account__c;
                        }
                    }
                }
                insert lstTJ;
            }
        }
    }  

    public static void checkPostedAndClosedTJ(List<sObject> lstObj) {
        if(TransactionJournalService.byPassCheckPosted) return;
        //ADDED FOR KIN-1445
        String MyProfileName ='';
        List<Profile> profilenames= [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
        if(profilenames.size()>0 && profilenames[0].name!=null)
            MyProfileName=profilenames[0].Name;
        
        Set<String> setId = new Set<String>();
        for(sObject obj : lstObj) {
            setId.add((String)obj.get('Id'));
        }
        Map<String, Transaction_Journal__c> mapTJ = new Map<String, Transaction_Journal__c>();
        for(Transaction_Journal__c tj : [SELECT id, Invoice__c
                                                FROM Transaction_Journal__c
                                                WHERE Status__c IN ('Posted','Closed')
                                                AND  Invoice__c IN :setId]){
            if(String.isNotBlank(tj.Invoice__c)) {
                mapTJ.put(tj.Invoice__c, tj);
            }
        }
        for(sObject obj : lstObj) {
            if(mapTJ.get((String)obj.get('Id')) != null && MyProfileName!='System Administrator') {
                obj.addError(Label.TJ_Posted_or_Closed_Error_Message);
            }
        }
    }

    public static void insertAccountPeriod(List<Campaign> lstNew) {
        Id recordId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('LASER Reporting Period').getRecordTypeId();
        List<Campaign> lstChanged = new List<Campaign>();
        for(Integer i = 0 ; i < lstNew.size() ; i++){
            if(lstNew[i].recordTypeId == recordId) {
                lstChanged.add(lstNew[i]);
            }
        }
        if(lstChanged.size() == 0) return;
        Date startDate = Date.today();
        Date endDate = Date.today();
        for(Campaign c : lstChanged) {
            if(c.startDate < startDate) startDate = c.startDate;
            if(c.endDate > endDate) endDate = c.endDate;
        }
    }

    public static void updateAccountPeriod(List<Campaign> lstNew, List<Campaign> lstOld) {
        if(TransactionJournalService.byPassCampaignUpdate) return;
        Set<Id> setOpen = new Set<Id>();
        Set<Id> setPosted = new Set<Id>();
        for(Integer i = 0 ; i < lstNew.size() ; i++){
            if(lstNew[i].Status != lstOld[i].Status) {
                if(lstNew[i].Status == 'Open'){
                    setOpen.add(lstNew[i].Id);
                } else if (lstNew[i].Status == 'Posted'){
                    setPosted.add(lstNew[i].Id);
                }
            }
        }
        if(setOpen.size() > 0) {
            // delete all TJ
            List<Transaction_Journal__c> lstDelete = [SELECT id, Invoice__c
                                                        FROM Transaction_Journal__c
                                                        WHERE Child_Campaign__c IN :setOpen];
            if(lstDelete.size() > 0){
                List<Payment__c> lstInvoice = new List<Payment__c>();
                for(Transaction_Journal__c tj : lstDelete){
                    lstInvoice.add(new Payment__c(Id = tj.Invoice__c, Invoices_Status__c = ''));
                }
                delete lstDelete;
                update lstInvoice;
            }
        }
        if(setPosted.size() > 0) {
            System.debug('--updateAccountPeriod--' + setPosted);
            List<Campaign> lstCamp = [SELECT id, Name, StartDate, EndDate, RecordTypeId, ParentId, Parent.ParentId, Status, FIPS__c, Cate_Type__c
                                        FROM Campaign  
                                        WHERE id IN :setPosted];
            autoPostedTJ(lstCamp);
        }
    }

    public static Boolean checkPostedTJ(List<Id> setId) {
        List<Campaign> lstCamp = [SELECT id, Name, StartDate, EndDate, RecordTypeId, ParentId, Parent.ParentId, Status, FIPS__c, Cate_Type__c
                                    FROM Campaign  
                                    WHERE id IN :setId];

        Map<Integer, String> mapMonthToString = new Map<Integer, String>();
        mapMonthToString.put(1, 'January');
        mapMonthToString.put(2, 'February');
        mapMonthToString.put(3, 'March');
        mapMonthToString.put(4, 'April');
        mapMonthToString.put(5, 'May');
        mapMonthToString.put(6, 'June');
        mapMonthToString.put(7, 'July');
        mapMonthToString.put(8, 'August');
        mapMonthToString.put(9, 'September');
        mapMonthToString.put(10, 'October');
        mapMonthToString.put(11, 'November');
        mapMonthToString.put(12, 'December');

        String strCondition = getPostCondition(lstCamp);
        System.debug('---autoPostedTJ---' + strCondition);
        String query = 'SELECT id, Invoices_Status__c, Kinship_Check__c, Payment_Method__c,Opportunity__c, ' +
                        'Accounting_Period__c, Accounting_Period__r.StartDate, Accounting_Period__r.EndDate, RecordType.Name,Payment_Date__c, ' +
                        'Accounting_Period__r.FIPS__c, Accounting_Period__r.Next_LASER_AP__c, Invoice_Stage__c,Paid__c,Opportunity__r.Posting_Period__c ' +
                        'FROM Payment__c ' +
                        'WHERE Invoices_Status__c <> \'Posted\' AND Accounting_Period__c IN :setId AND Invoice_Stage__c IN (\'Pending\', \'Paid\', \'Cancelled\') ';
        System.debug('---autoPostedTJ---' + query);
        List<Payment__c> lstPayment = Database.query(query);
        List<Payment__c> lstPaymentMoved = [SELECT id, Invoices_Status__c, Kinship_Check__c,Payment_Method__c,Opportunity__c,
                                                        Accounting_Period__c, Accounting_Period__r.StartDate, Accounting_Period__r.EndDate, RecordType.Name,Payment_Date__c,
                                                        Accounting_Period__r.FIPS__c, Accounting_Period__r.Next_LASER_AP__c, Invoice_Stage__c,Paid__c,Opportunity__r.Posting_Period__c
                                                        FROM Payment__c
                                                        WHERE RecordType.Name = 'Staff & Operations' AND Invoices_Status__c != 'Posted' AND Opportunity__r.Accounting_Period__c IN :setId AND Invoice_Stage__c = 'Ready to Pay'];
        if(lstPayment.size() > 0 || lstPaymentMoved.size() > 0){
            return true;
        }
        return false;
    }

    public static void autoPostedTJ(List<Campaign> lstCamp) {
        Set<Id> setParentId = new Set<Id>();
        Set<Id> setId = new Set<Id>();
        for(Campaign c : lstCamp) {
            setParentId.add(c.Parent.ParentId);
            setId.add(c.Id);
        }
        PaymentTriggerHandler.bypassTJPosted = true;
        PaymentTriggerHandler.bypassBTrigger = true;

        Map<Integer, String> mapMonthToString = new Map<Integer, String>();
        mapMonthToString.put(1, 'January');
        mapMonthToString.put(2, 'February');
        mapMonthToString.put(3, 'March');
        mapMonthToString.put(4, 'April');
        mapMonthToString.put(5, 'May');
        mapMonthToString.put(6, 'June');
        mapMonthToString.put(7, 'July');
        mapMonthToString.put(8, 'August');
        mapMonthToString.put(9, 'September');
        mapMonthToString.put(10, 'October');
        mapMonthToString.put(11, 'November');
        mapMonthToString.put(12, 'December');

        String strCondition = getPostCondition(lstCamp);
        System.debug('---autoPostedTJ---' + strCondition);
        String query = 'SELECT id, Invoices_Status__c, Kinship_Check__c, Payment_Method__c,Opportunity__c, Opportunity__r.RecordType.Name, ' +
                        'Accounting_Period__c, Accounting_Period__r.StartDate, Accounting_Period__r.EndDate, RecordType.Name,Payment_Date__c, ' +
                        'Accounting_Period__r.FIPS__c, Accounting_Period__r.Next_LASER_AP__c, Invoice_Stage__c,Paid__c,Opportunity__r.Posting_Period__c ' +
                        'FROM Payment__c ' +
                        'WHERE Invoices_Status__c <> \'Posted\' AND Accounting_Period__c IN :setId AND Invoice_Stage__c IN (\'Pending\', \'Paid\', \'Cancelled\') ';
        System.debug('---autoPostedTJ---' + query);
        List<Payment__c> lstPayment = Database.query(query);
        List<Payment__c> lstPaymentMoved = [SELECT id, Invoices_Status__c, Kinship_Check__c, Payment_Method__c,Opportunity__c,
                                                        Accounting_Period__c, Accounting_Period__r.StartDate, Accounting_Period__r.EndDate, RecordType.Name,Payment_Date__c,
                                                        Accounting_Period__r.FIPS__c, Accounting_Period__r.Next_LASER_AP__c, Invoice_Stage__c,Paid__c,Opportunity__r.Posting_Period__c
                                                        FROM Payment__c
                                                        WHERE RecordType.Name = 'Staff & Operations' AND Invoices_Status__c != 'Posted' AND Opportunity__r.Accounting_Period__c IN :setId AND Invoice_Stage__c = 'Ready to Pay'];
        if(lstPayment.size() > 0 || lstPaymentMoved.size() > 0){
            List<Payment__c> lstPaymentUpdate = new List<Payment__c>();
            List<Opportunity> lstPOUpdate = new List<Opportunity>();
            Set<Id> setOppId = new Set<Id>();

            for(Payment__c payment : lstPayment) {
                payment.Invoices_Status__c = 'Posted';
                if(payment.Invoice_Stage__c == 'Pending') {
                    payment.Invoice_Stage__c = 'Posted';
                }
                lstPaymentUpdate.add(payment);
                if(payment.RecordType.Name == 'Staff & Operations') {
                	if(!setOppId.contains(payment.Opportunity__c)) {
                        lstPOUpdate.add(new Opportunity(Id = payment.Opportunity__c, StageName = 'Closed'));
                    }
                    setOppId.add(payment.Opportunity__c);
                } else if(payment.Opportunity__r.RecordType.Name == 'Report of Collections') {
                    if(!setOppId.contains(payment.Opportunity__c)) {
                        lstPOUpdate.add(new Opportunity(Id = payment.Opportunity__c, StageName = 'Posted'));
                    }
                    setOppId.add(payment.Opportunity__c);
                }     
            }
            System.debug('---lstPaymentUpdate SO---' + lstPaymentUpdate);
            if(lstPaymentUpdate.size() > 0) {
                update lstPaymentUpdate;
            }
            if(lstPOUpdate.size() > 0) {
                update lstPOUpdate;
            }
            // moved to the next LASER Period
            if(lstPaymentMoved.size() > 0){
                Set<Id> setNextAP = new Set<Id>();
                sObject originalSObject = (sObject) lstPaymentMoved[0];
                List<Payment__c> lstSOCreated = (List<Payment__c>)KinshipUtil.cloneObjects(lstPaymentMoved, originalSObject.getsObjectType());
                for(Integer i = 0 ; i < lstSOCreated.size(); i++){
                    lstSOCreated[i].Accounting_Period__c = lstPaymentMoved[i].Accounting_Period__r.Next_LASER_AP__c;
                    setNextAP.add(lstSOCreated[i].Accounting_Period__c);
                }
                system.debug('---KinshipTJAutoPostedBatch--' + setNextAP);
                Map<Id, Id> mapNextAP = new Map<Id, Id>();
                for(Opportunity opp : [SELECT id, Accounting_Period__c FROM Opportunity WHERE RecordType.Name = 'Staff & Operations' AND Accounting_Period__c IN :setNextAP]){
                    mapNextAP.put(opp.Accounting_Period__c, opp.id);
                }
                system.debug('---KinshipTJAutoPostedBatch--' + mapNextAP);
                for(Payment__c payment : lstSOCreated){
                    payment.Opportunity__c = mapNextAP.get(payment.Accounting_Period__c);
                }
                insert lstSOCreated;
                delete lstPaymentMoved;
            }

            for(Campaign c : lstCamp){
                c.Status = 'Posted';
            }
            TransactionJournalService.byPassCampaignUpdate = true;
            update lstCamp;
            TransactionJournalService.byPassCampaignUpdate = false;
        }
        PaymentTriggerHandler.bypassTJPosted = false;
        PaymentTriggerHandler.bypassBTrigger = false;
    }

    private static String getPostCondition(List<Campaign> lstCamp) {
        String result = '';
        for(Campaign c : lstCamp) {
            result += 'OR (Payment_Date__c >= ' + getDate(c.startDate) + ' AND Payment_Date__c <= ' + getDate(c.endDate) + ' AND  FIPS_Code__c = \'' + c.FIPS__c + '\' AND Category_lookup__r.RecordType.Name = \'' + c.Cate_Type__c + '\' ) ';
        }
        return result.replaceFirst('OR', '');
    }

    @testVisible
    private static String getRefundPostCondition(List<Campaign> lstCamp, Map<Integer, String> mapMonthToString) {
        String months = '';
        String fips = '';
        String cate = '';
        for(Campaign c : lstCamp) {
            months += ',\'' + mapMonthToString.get(c.endDate.month()) + ' ' + c.EndDate.year() + '\'';
            fips = ',\'' + c.FIPS__c + '\'';
            cate = ',\'' + c.Cate_Type__c + '\'';
        }
        String result = '';
        result += ' Opportunity__r.Posting_Period__c IN (' + months.replaceFirst(',', '') + ') AND  FIPS_Code__c IN (' + fips.replaceFirst(',', '') + ') AND Category_lookup__r.RecordType.Name IN (' + cate.replaceFirst(',', '') + ') ';
        return result;
    }

    private static String getDate(Date d) {
        return d.year() + '-' + String.valueOf(d.month()).leftPad(2, '0') + '-' + String.valueOf(d.day()).leftPad(2, '0');
    }

    @testVisible
    private static Campaign getAP(List<Campaign> lstAP, Payment__c p) {
        for(Campaign c : lstAP) {
            if(c.StartDate <= p.Payment_Date__c && p.Payment_Date__c <= c.EndDate) {
                return c;
            }
        }
        return null;
    }
	 @TestVisible
    private static Map<Id, Id> getBudgetMap(List<Campaign> lstCamp){
        Set<Id> setId = new Set<Id>();
        for(Campaign c : lstCamp){
            setId.add(c.id);
        }
        Map<Id, Id> mapReturn = new Map<Id, Id>();
        for(Budgets__c b : [SELECT id, Fiscal_Year__c FROM Budgets__c WHERE Fiscal_Year__c IN :setId]) {
            mapReturn.put(b.Fiscal_Year__c, b.id);
        }
        return mapReturn;
    }

    @TestVisible
    private static Id checkFundingSource(String strT, Id ridFY, Id ridLASER, Id ridLEDRS) {
        if(strT == 'CSA'){
            return ridLEDRS;
        } else if(strT == 'Special Welfare' || strT == 'Claims Payable'){
            return ridFY;
        } else {
            return ridLASER;
        }
    }

    public static void autoFillFYOnInvoice(List<Payment__c> lstNew, List<Payment__c> lstOld) {
        Id ridFY = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId();
        Id ridLASER = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('LASER Reporting Period').getRecordTypeId();
        Id ridLEDRS = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('LEDRS Reporting Period').getRecordTypeId();
        Id rtSO = Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Staff_Operations').getRecordTypeId();
        Id rtVI = Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Vendor_Invoice').getRecordTypeId();
        Id rtSWR = Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Special_Welfare_Reimbursement').getRecordTypeId();
        Id rtVR = Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Vendor_Refund').getRecordTypeId();
        Id rtR = Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Receipt').getRecordTypeId();
        Id rtADJ = Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Adjustments').getRecordTypeId();
        Date startDate;
        Date endDate;
        Set<Id> setId = new Set<Id>();
        Map<Id, Id> mapInv2Cate = new Map<Id, Id>();
        Set<Id> setOpp = new Set<Id>();
        List<Payment__c> lstSOPayment = new List<Payment__c>();
        Set<Id> setSOOpp = new Set<Id>();
        Map<Id, Id> mapFIPS = new Map<Id, Id>();

        Set<Id> setTCCateId = new Set<Id>();
        List<Payment__c> lstTCPayment = new List<Payment__c>();
        if(lstOld == null) {
            for(Payment__c item : lstNew) {
                if(Label.KINSHIP_SO_RT.equals(item.recordTypeId)) {
                    lstSOPayment.add(item);
                    setSOOpp.add(item.Opportunity__c);
                }
                if((item.recordTypeId == rtVI && (item.Invoice_Stage__c == CANCELLED || item.Invoice_Stage__c == PAID || item.Invoice_Stage__c == PENDING)) ||
                   ((item.recordTypeId == rtVR || item.recordTypeId == rtSWR || item.recordTypeId == rtR) && item.Invoice_Stage__c == PENDING) ||
                  	((item.recordTypeId == rtADJ) && item.Invoice_Stage__c == CANCELLED)) {
                       setId.add(item.id);
                       if(String.isNotBlank(item.Category_lookup__c)) {
                           mapInv2Cate.put(item.id, item.Category_lookup__c);
                       }
                       setOpp.add(item.Opportunity__c);
                       mapFIPS.put(item.id, item.FIPS_Code__c);
                       if(startDate == null || item.Payment_Date__c < startDate) startDate = item.Payment_Date__c;
                       if(endDate == null || item.Payment_Date__c > endDate) endDate = item.Payment_Date__c;
                }
                // transaction Code
                if((item.Invoice_Stage__c == CANCELLED || item.Invoice_Stage__c == UNCANCELLED) && (item.RecordTypeId == rtSO || item.RecordTypeId == rtVI)){
                    setTCCateId.add(item.Category_lookup__c);
                    lstTCPayment.add(item);
                }
            }
        } else {
            for(Integer i = 0 ; i < lstNew.size() ; i++) {
                if(Label.KINSHIP_SO_RT.equals(lstNew[i].recordTypeId)) {
                    lstSOPayment.add(lstNew[i]);
                    setSOOpp.add(lstNew[i].Opportunity__c);
                }
                if((((lstNew[i].recordTypeId == rtVI && (lstNew[i].Invoice_Stage__c == CANCELLED || lstNew[i].Invoice_Stage__c == PAID || lstNew[i].Invoice_Stage__c == PENDING) && lstNew[i].Invoice_Stage__c != lstOld[i].Invoice_Stage__c) ||
                   ((lstNew[i].recordTypeId == rtVR || lstNew[i].recordTypeId == rtSWR || lstNew[i].recordTypeId == rtR) && lstNew[i].Invoice_Stage__c == PENDING) || 
                   ((lstNew[i].recordTypeId == rtADJ) && lstNew[i].Invoice_Stage__c == CANCELLED)) && lstNew[i].Invoice_Stage__c != lstOld[i].Invoice_Stage__c) ||
                   ((lstNew[i].recordTypeId == rtVR || lstNew[i].recordTypeId == rtSWR || lstNew[i].recordTypeId == rtR) && lstNew[i].FIPS_Code__c != lstOld[i].FIPS_Code__c)
                  ) {
                       setId.add(lstNew[i].id);
                       if(String.isNotBlank(lstNew[i].Category_lookup__c)) {
                           mapInv2Cate.put(lstNew[i].id, lstNew[i].Category_lookup__c);
                       }
                       setOpp.add(lstNew[i].Opportunity__c);
                       mapFIPS.put(lstNew[i].id, lstNew[i].FIPS_Code__c);
                       if(startDate == null || lstNew[i].Payment_Date__c < startDate) startDate = lstNew[i].Payment_Date__c;
                       if(endDate == null || lstNew[i].Payment_Date__c > endDate) endDate = lstNew[i].Payment_Date__c;
                }

                // transaction Code
                if((lstNew[i].Invoice_Stage__c == CANCELLED || lstNew[i].Invoice_Stage__c == UNCANCELLED) && (lstNew[i].RecordTypeId == rtSO || lstNew[i].RecordTypeId == rtVI)){
                    setTCCateId.add(lstNew[i].Category_lookup__c);
                    lstTCPayment.add(lstNew[i]);
                }
            }
        }
        // assign for SO
        System.debug('---autoFillFYOnInvoice SO---' + lstSOPayment.size());
        if(lstSOPayment.size() > 0) {
            Map<Id, Opportunity> mapSOOpp = new Map<Id, Opportunity>();
            for(Opportunity opp : [SELECT id, Accounting_Period__c, Accounting_Period__r.EndDate FROM Opportunity WHERE id IN :setSOOpp]){
                mapSOOpp.put(opp.Id, opp);
            }
            for(Payment__c payment : lstSOPayment) {
                Opportunity opp = mapSOOpp.get(payment.Opportunity__c);
                if(opp != null) {
                    payment.Accounting_Period__c = opp.Accounting_Period__c;
                    if(payment.Scheduled_Date__c == null && payment.Payment_Method__c == 'Check') {
                        payment.Scheduled_Date__c = opp.Accounting_Period__r.EndDate;
                    }
                }
            }
        }

        // transaction Code
        System.debug('---autoFillFYOnInvoice lstTCPayment---' + lstTCPayment.size());
        if(lstTCPayment.size() > 0) {
            Map<Id, String> mapTCCate2Type = new Map<Id, String>();
            for(Category__c c : [SELECT id, Category_Type__c FROM Category__c WHERE id IN :setTCCateId]){
                mapTCCate2Type.put(c.id, c.Category_Type__c);
            }
            String str;
            for(Payment__c payment : lstTCPayment) {
                str = mapTCCate2Type.get(payment.Category_lookup__c);
                if(str == 'CSA' || str == 'IVE') {
                    if(payment.Invoice_Stage__c == CANCELLED) {
                        payment.Payment_Source__c = '2';
                        System.debug('---autoFillFYOnInvoice CANCELLED---' + payment);
                    } else if(payment.Invoice_Stage__c == UNCANCELLED){
                        payment.Payment_Source__c = '12';
                        System.debug('---autoFillFYOnInvoice UNCANCELLED---' + payment);
                    }
                }
            }
        }
        
        System.debug('---autoFillFYOnInvoice---' + setId.size());
        if(setId.size() == 0) return;
        Map<Id, String> mapCate2Type = new Map<Id, String>();
        for(Category__c c : [SELECT id, Category_Type__c FROM Category__c WHERE id IN :mapInv2Cate.values()]){
            mapCate2Type.put(c.id, c.Category_Type__c);
        }
        Map<Id, String> mapType = new Map<Id, String>();
        for(Id item : setId) {
            Id itemp = mapInv2Cate.get(item);
            if(String.isNotBlank(itemp)) {
                mapType.put(item, mapCate2Type.get(itemp));
            }
        }
        Map<Id, Opportunity> mapIdToOpp = new Map<Id, Opportunity>();
        Set<String> lstPP = new Set<String>();
        for(Opportunity opp : [SELECT id, Name, Posting_Period__c, CampaignId FROM Opportunity WHERE ID IN :setOpp]){
            if(String.isNotBlank(opp.Posting_Period__c)) lstPP.add(opp.Posting_Period__c);
            mapIdToOpp.put(opp.id, opp);
        }

        List<Campaign> lstCam = [SELECT id, StartDate, EndDate, RecordTypeId, ParentId, Parent.ParentId, FIPS__c, Fiscal_Year_Type__c, Posting_Period__c
                                FROM Campaign WHERE IsActive = true 
                                AND ((startDate <= :startDate
                                AND endDate >= :endDate) OR Posting_Period__c IN :lstPP )];
        System.debug('---autoFillFYOnInvoice---' + lstCam);
        if(lstCam.size() > 0) {
            Id recordType;
            Id fipsId;
            Id parentId;
            Opportunity oppTemp;
            for(Payment__c item : lstNew) {
                //fipsId = mapFIPS.get(item.Opportunity__c);
                //parentId = mapParentAP.get(item.Opportunity__c);
                for(Campaign c : lstCam) {
                    if(setId.contains(item.Id)) {
                        recordType = checkFundingSource(mapType.get(item.id), ridFY, ridLASER, ridLEDRS);
                        System.debug('---autoFillFYOnInvoice---' + recordType + ' ' + item.FIPS_Code__c);
                        if(recordType == ridFY) {
                            oppTemp = mapIdToOpp.get(item.Opportunity__c);
                            if(oppTemp != null) {
                            	item.Accounting_Period__c = oppTemp.CampaignId;
                            }
                        } else {
                            if(item.RecordTypeId == rtR || item.RecordTypeId == rtVR || item.RecordTypeId == rtSWR) {
                                oppTemp = mapIdToOpp.get(item.Opportunity__c);
                                if(oppTemp != null && c.recordTypeId == recordType && oppTemp.Posting_Period__c == c.Posting_Period__c && c.FIPS__c == item.FIPS_Code__c){
                                    item.Accounting_Period__c = c.id;
                                    break;
                                }
                            } else {
                                if(c.recordTypeId == recordType && item.Payment_Date__c >= c.startDate && item.Payment_Date__c <= c.endDate &&
                                    c.FIPS__c == item.FIPS_Code__c){
                                        item.Accounting_Period__c = c.id;
                                        break;
                                }
                            }
                        	//((c.recordTypeId == ridFY && c.Fiscal_Year_Type__c == 'Local' ) || parentId == c.Parent.ParentId)
                        }
                    }
                }
            }
        }
    }
}