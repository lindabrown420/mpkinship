@isTest
public class KinshipCaseTriggerTest {
    public static testmethod void test1(){
   
        TriggerActivation__c  ta = new TriggerActivation__c();
        ta.name='KinshipCaseTrigger';
        ta.isActive__c=true;
        insert ta;
     
        contact con = new contact();       
        con.LastName='contactrec';
        con.FirstName='test';
        con.Email='test@contact.com';      
        insert con;
        
        contact con1 = new contact();       
        con1.LastName='test1';
        con1.FirstName='last1';
        con1.Email='test1@contact.com';      
        insert con1;
        
        Kinship_Case__c caserec = new Kinship_Case__c();
        caserec.Primary_Contact__c=con.id;
        caserec.DOB__c=system.today().adddays(-100);
        caserec.Race__c='1';
        caserec.Gender_Preference__c='M';
        caserec.Hispanic_Ethnicity__c='1';
        caserec.DSM_V_Indicator_ICD_10_1__c='2';
        caserec.Clinical_Medication_Indicator_1__c='2';
        caserec.Medicaid_Indicator_1__c='2';
        caserec.Referral_Source__c='5';
        caserec.Autism_Flag_1__c='2';
        insert caserec;
        
        
        account accountrec = new account();
        accountrec.Name='test vendor';
        accountrec.Tax_ID__c='222-22-3333';
        accountrec.Entity_Type__c='Individual';
        insert accountrec;
        
        Claims__c rec = new Claims__c();
        rec.Name = 'Test 2';
        rec.Claim_Amount__c=200;
        rec.Claim_Type__c='SNAP';
        rec.Case__c=caserec.Id;
        rec.Payable_to__c=accountrec.Id;
        insert rec;
        
        caserec.Primary_Contact__c=con1.id;
        update caserec;
        //rec.Case__c=caserec1.Id;
        //update rec;
    }
}