@isTest
private class KinshipPaymentSyncControllerTest {
    @TestSetup
    static void initData(){
        Id profileId1 = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User u = new User(FirstName='Test',LastName= 'User',profileid=profileId1 , email='abc123@test.com',
                         username='abc123@test.com.prod',Alias='zbd',LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', EmailEncodingKey='UTF-8',
                          TimeZoneSidKey='America/Los_Angeles');
        
        Account acc = new Account(Name = 'test acc', Tax_ID__c = '123-56-7901', Entity_Type__c = 'Individual');
        insert acc;

        Locality_Account__c locality = new Locality_Account__c(Name = '12345');
        insert locality;

        Category__c cate = new Category__c(Name = 'test cate', Category_Type__c = 'CSA', Locality_Account__c = locality.id , RecordTypeId = Schema.SObjectType.Category__c.getRecordTypeInfosByName().get('LEDRS Category').getRecordTypeId());
        insert cate;
        
        Kinship_Case__c kcase = new Kinship_Case__c(Primary_Social_Worker__c = u.id);

        Opportunity opp = new Opportunity(Name = 'test Opp', AccountId = acc.id, Category__c = cate.id, CloseDate = Date.today().addDays(10),
                                          StageName='Open', Kinship_Case__c = kcase.id, RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Purchase of Services Order').getRecordTypeId());
        insert opp;

        Payment__c payment = new Payment__c(Opportunity__c = opp.id, 
                                            Scheduled_Date__c = Date.today(), 
                                            Status__c = 'Submitted For Approval',
                                            Invoice_Stage__c = 'Ready to Pay',
                                            Written_Off__c = false,
                                            Payment_Method__c = 'Check',
                                            Category_lookup__c = cate.id,
                                            Payment_Amount__c = 100,
                                           	RecordTypeId = Schema.SObjectType.Payment__c.getRecordTypeInfosByName().get('Vendor Invoice').getRecordTypeId());
        insert payment;
        
        Cost_Center__c cc = new Cost_Center__c(Name = 'Cost Center', Cost_Center_Code__c = '12345');
        insert cc;
        
        FIPS__c fips = new FIPS__c(name = 'FIPS');
        insert fips;
    }

    @isTest
    static void testKinshipSearchLookup_WithAcc(){
        List<Account> lstData = [select id, name from Account];
        List<Id> fixedSearchResults= new List<Id>();
        fixedSearchResults.add(lstData[0].id);
        Test.setFixedSearchResults(fixedSearchResults);
        Test.startTest();
        List<Object> lstResult = KinshipPaymentSyncController.kinshipSearchLookup('test', new List<String>(), 'Account');
        Test.stopTest();
        System.assertEquals(1, lstResult.size(), 'The kinshipSearchLookup method return does not equal 1');
    }
    
    @isTest
    static void testKinshipSearchLookup_WithCamp(){
        Campaign c = new Campaign(name = 'CSA - 2020', Fiscal_Year_Type__c = 'Local', RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId());
        insert c;
        List<Id> fixedSearchResults= new List<Id>();
        fixedSearchResults.add(c.id);
        Test.setFixedSearchResults(fixedSearchResults);
        Test.startTest();
        List<Object> lstResult = KinshipPaymentSyncController.kinshipSearchLookup('test', new List<String>(), 'Campaign');
        Test.stopTest();
        System.assertEquals(1, lstResult.size(), 'The kinshipSearchLookup method return does not equal 1');
    }

    @isTest
    static void testKinshipSearchLookup_WithCate(){
        List<Category__c> lstData = [select id, name from Category__c];
        List<Id> fixedSearchResults= new List<Id>();
        fixedSearchResults.add(lstData[0].id);
        Test.setFixedSearchResults(fixedSearchResults);
        Test.startTest();
        List<Object> lstResult = KinshipPaymentSyncController.kinshipSearchLookup('cate', new List<String>(), 'Category__c');
        Test.stopTest();
        System.assertEquals(1, lstResult.size(), 'The kinshipSearchLookup method return does not equal 1');
    }

    @isTest
    static void testKinshipSearchLookup_WithOpp(){
        List<Opportunity> lstData = [select id, name from Opportunity];
        List<Id> fixedSearchResults= new List<Id>();
        fixedSearchResults.add(lstData[0].id);
        Test.setFixedSearchResults(fixedSearchResults);
        Test.startTest();
        List<Object> lstResult = KinshipPaymentSyncController.kinshipSearchLookup('opp', new List<String>(), 'Opportunity');
        Test.stopTest();
        System.assertEquals(1, lstResult.size(), 'The kinshipSearchLookup method return does not equal 1');
    }
    
    @isTest
    static void testKinshipSearchLookup_WithUser(){
        List<User> lstData = [select id, name from User];
        List<Id> fixedSearchResults= new List<Id>();
        fixedSearchResults.add(lstData[0].id);
        Test.setFixedSearchResults(fixedSearchResults);
        Test.startTest();
        List<Object> lstResult = KinshipPaymentSyncController.kinshipSearchLookup('user', new List<String>(), 'User');
        Test.stopTest();
        System.assertEquals(1, lstResult.size(), 'The kinshipSearchLookup method return does not equal 1');
    }
    
    @isTest
    static void testKinshipSearchLookup_WithEmployee(){
        Employee__c emp = new Employee__c(name = 'Test');
        insert emp;
        List<Id> fixedSearchResults= new List<Id>();
        fixedSearchResults.add(emp.id);
        Test.setFixedSearchResults(fixedSearchResults);
        Test.startTest();
        List<Object> lstResult = KinshipPaymentSyncController.kinshipSearchLookup('Employee', new List<String>(), 'Employee');
        Test.stopTest();
        System.assertEquals(1, lstResult.size(), 'The kinshipSearchLookup method return does not equal 1');
    }
    
    @isTest
    static void testKinshipSearchLookup_WithPayment(){
        List<Payment__c> lstData = [select id, name from Payment__c];
        List<Id> fixedSearchResults= new List<Id>();
        fixedSearchResults.add(lstData[0].id);
        Test.setFixedSearchResults(fixedSearchResults);
        Test.startTest();
        List<Object> lstResult = KinshipPaymentSyncController.kinshipSearchLookup('payment', new List<String>(), 'Payment');
        Test.stopTest();
        System.assertEquals(1, lstResult.size(), 'The kinshipSearchLookup method return does not equal 1');
    }
    
    @isTest
    static void testKinshipSearchLookup_WithCC(){
        List<Cost_Center__c> lstData = [SELECT Id, Name, Cost_Center_Code__c FROM Cost_Center__c];
        List<Id> fixedSearchResults= new List<Id>();
        fixedSearchResults.add(lstData[0].id);
        Test.setFixedSearchResults(fixedSearchResults);
        Test.startTest();
        List<Object> lstResult = KinshipPaymentSyncController.kinshipSearchLookup('CostCenter', new List<String>(), 'CostCenter');
        Test.stopTest();
        System.assertEquals(1, lstResult.size(), 'The kinshipSearchLookup method return does not equal 1');
    }
    
    @isTest
    static void testKinshipSearchLookup_WithFIPS(){
        List<FIPS__c> lstData = [SELECT Id, Name FROM FIPS__c];
        List<Id> fixedSearchResults= new List<Id>();
        fixedSearchResults.add(lstData[0].id);
        Test.setFixedSearchResults(fixedSearchResults);
        Test.startTest();
        List<Object> lstResult = KinshipPaymentSyncController.kinshipSearchLookup('FIPS', new List<String>(), 'FIPS');
        Test.stopTest();
        System.assertEquals(1, lstResult.size(), 'The kinshipSearchLookup method return does not equal 1');
    }

    @isTest
    static void testGetPaymentData(){
        List<Payment__c> lstPayment = [SELECT Id, Name, Payment_Amount__c, Category_lookup__c, Scheduled_Date__c,
                                                Opportunity__c, Opportunity__r.AccountId, Opportunity__r.Account.Name,
                                                Opportunity__r.Memo__c, Opportunity__r.Name, Status__c, Opportunity__r.Category__c,
                                                Opportunity__r.Kinship_Case__r.Primary_Social_Worker__c
                                                FROM Payment__c
                                                LIMIT 1];
        List<String> lstVendor = new List<String>();
        lstVendor.add(lstPayment[0].Opportunity__r.AccountId);
        List<String> lstCategory = new List<String>();
        lstCategory.add(lstPayment[0].Opportunity__r.Category__c);
        List<String> lstOpp = new List<String>();
        lstOpp.add(lstPayment[0].Opportunity__c);
        List<String> lstP = new List<String>();
        lstP.add(lstPayment[0].id);
        List<String> lstCaseWorker = new List<String>();
        lstCaseWorker.add(lstPayment[0].Opportunity__r.Kinship_Case__r.Primary_Social_Worker__c);
        Date startDate = Date.today().addDays(-2);
        Date endDate = Date.today().addDays(2);
        List<String> lstAP = new List<String>();
        lstAP.add(Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Staff & Operations').getRecordTypeId());
        lstAP.add(Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Purchase of Services Order').getRecordTypeId());
        lstAP.add(Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Case Action').getRecordTypeId());
        List<String> lstCT = new List<String>();
        lstCT.add(Schema.SObjectType.Category__c.getRecordTypeInfosByName().get('LASER Category').getRecordTypeId());
        lstCT.add(Schema.SObjectType.Category__c.getRecordTypeInfosByName().get('LEDRS Category').getRecordTypeId());
        lstCT.add(Schema.SObjectType.Category__c.getRecordTypeInfosByName().get('LOCAL Category').getRecordTypeId());
        Test.startTest();
        List<Object> lstResult = KinshipPaymentSyncController.getPaymentData(lstVendor, lstCategory, lstOpp, lstP, lstCaseWorker, new List<String>(), startDate, endDate, 'Submitted For Approval', lstAP, lstCT, 0, 1000);
        Test.stopTest();
        System.assertEquals(1, lstResult.size(), 'The getPaymentData method return does not equal 1');
    }

    @isTest
    static void testSubmitPaymentForApproval(){
        List<Payment__c> lstPayment = [SELECT Id, Name, Payment_Amount__c, Category_lookup__c, Scheduled_Date__c,
                                                Opportunity__c, Opportunity__r.AccountId, Opportunity__r.Account.Name,
                                                Opportunity__r.Memo__c, Opportunity__r.Name, Status__c, Opportunity__r.Category__c,
                                                Opportunity__r.Kinship_Case__r.Primary_Social_Worker__c
                                                FROM Payment__c
                                                LIMIT 1];
        List<String> lstVendor = new List<String>();
        lstVendor.add(lstPayment[0].Opportunity__r.AccountId);
        List<String> lstCategory = new List<String>();
        lstCategory.add(lstPayment[0].Opportunity__r.Category__c);
        List<String> lstOpp = new List<String>();
        lstOpp.add(lstPayment[0].Opportunity__c);
        List<String> lstP = new List<String>();
        lstP.add(lstPayment[0].id);
        List<String> lstCaseWorker = new List<String>();
        lstCaseWorker.add(lstPayment[0].Opportunity__r.Kinship_Case__r.Primary_Social_Worker__c);
        Date startDate = Date.today().addDays(-2);
        Date endDate = Date.today().addDays(2);
        List<KinshipPaymentSyncController.PaymentObj> lstData = KinshipPaymentSyncController.getPaymentData(lstVendor, lstCategory, lstOpp, lstP, lstCaseWorker, new List<String>(), startDate, endDate, 'Submitted For Approval', new List<String>(), new List<String>(), 0, 1000);
        User u = [SELECT id FROM User limit 1];
        Test.startTest();
        KinshipPaymentSyncController.submitPaymentForApproval(lstData, u.id);
        KinshipPaymentSyncController.updateCostCenterOnPayment(lstData);
        Test.stopTest();
        System.assertEquals(1, [SELECT id FROM Payment_Approval__c].size(), 'The List Payment_Approval__c return does not equal 1');
        System.assertEquals(1, [SELECT id FROM Payment_Approval_Step__c].size(), 'The List Payment_Approval_Step__c return does not equal 1');
    }

    @isTest
    static void testSyncPaymentToCheck(){
        List<Payment__c> lstPayment = [SELECT Id, Name, Payment_Amount__c, Category_lookup__c, Scheduled_Date__c,
                                                Opportunity__c, Opportunity__r.AccountId, Opportunity__r.Account.Name,
                                                Opportunity__r.Memo__c, Opportunity__r.Name, Status__c
                                                FROM Payment__c
                                                LIMIT 1];
        List<String> lstId = new List<String>();
        lstId.add(lstPayment[0].Id);
        
        Test.startTest();
        List<Kinship_Check__c> lstResult = KinshipPaymentSyncController.syncPaymentToCheck(lstId);
        Test.stopTest();
        System.assertEquals(1, lstResult.size(), 'The syncPaymentToCheck Method return does not equal 1');
    }

    @isTest
    private static void testGetPicklistvalues(){
        Test.startTest();
        List<String> lstResult = KinshipPaymentSyncController.getPicklistvalues('Kinship_Check__c', 'Status__c', '--none--');
        Test.stopTest();

        System.assertNotEquals(null, lstResult, 'The method does not return 5 expected status on the picklist');
    }
    
    @isTest
    private static void testGetApprovalPaymentData(){
        List<Payment__c> lstPayment = [SELECT Id, Name, Payment_Amount__c, Category_lookup__c, Scheduled_Date__c,
                                                Opportunity__c, Opportunity__r.AccountId, Opportunity__r.Account.Name,
                                                Opportunity__r.Memo__c, Opportunity__r.Name, Status__c, Opportunity__r.Category__c,
                                                Opportunity__r.Kinship_Case__r.Primary_Social_Worker__c
                                                FROM Payment__c
                                                LIMIT 1];
        List<String> lstVendor = new List<String>();
        lstVendor.add(lstPayment[0].Opportunity__r.AccountId);
        List<String> lstCategory = new List<String>();
        lstCategory.add(lstPayment[0].Opportunity__r.Category__c);
        List<String> lstOpp = new List<String>();
        lstOpp.add(lstPayment[0].Opportunity__c);
        List<String> lstP = new List<String>();
        lstP.add(lstPayment[0].id);
        List<String> lstCaseWorker = new List<String>();
        lstCaseWorker.add(lstPayment[0].Opportunity__r.Kinship_Case__r.Primary_Social_Worker__c);
        Date startDate = Date.today().addDays(-2);
        Date endDate = Date.today().addDays(2);
        List<KinshipPaymentSyncController.PaymentObj> lstData = KinshipPaymentSyncController.getPaymentData(lstVendor, lstCategory, lstOpp, lstP, lstCaseWorker, new List<String>(), startDate, endDate, 'Submitted For Approval', new List<String>(), new List<String>(), 0, 1000);
        User u = [SELECT id FROM User limit 1];
        KinshipPaymentSyncController.submitPaymentForApproval(lstData, u.id);
        
        Test.startTest();
        List<Object> lstResult = KinshipPaymentSyncController.getApprovalPaymentData();
        KinshipPaymentSyncController.updatePaymentStatus(lstP, 'Approved', 'Approved');
        Test.stopTest();

        System.assertEquals(1, lstResult.size(), 'The method does not return 1 result');
    }
    
    @isTest
    private static void testSavePaymentToCheck(){
        List<Payment__c> lstPayment = [SELECT Id, Name, Payment_Amount__c, Category_lookup__c, Scheduled_Date__c,
                                                Opportunity__c, Opportunity__r.AccountId, Opportunity__r.Account.Name,
                                                Opportunity__r.Memo__c, Opportunity__r.Name, Status__c, Opportunity__r.Category__c,
                                                Opportunity__r.Kinship_Case__r.Primary_Social_Worker__c
                                                FROM Payment__c
                                                LIMIT 1];
        List<KinshipPaymentSyncController.checkObj> lstCheck = new List<KinshipPaymentSyncController.checkObj>();
        KinshipPaymentSyncController.checkObj temp = new KinshipPaymentSyncController.checkObj();
        List<KinshipPaymentSyncController.PaymentObj> lstObj = new List<KinshipPaymentSyncController.PaymentObj>();
        KinshipPaymentSyncController.PaymentObj tempPayment = new KinshipPaymentSyncController.PaymentObj();
        tempPayment.id = lstPayment[0].id;
        lstObj.add(tempPayment);
        temp.lstPayment = lstObj;
        temp.vendorId = lstPayment[0].Opportunity__r.AccountId;
        temp.amount = 1000;
        lstCheck.add(temp);
        
        Test.startTest();
        List<Kinship_Check__c> lstResult = KinshipPaymentSyncController.savePaymentToCheck(lstCheck, Date.today());
        Test.stopTest();

        System.assertEquals(1, lstResult.size(), 'The method savePaymentToCheck does not return 1 result');
    }
    
    @isTest
    private static void testUpdateCheckNo(){
        Kinship_Check__c temp = new Kinship_Check__c();
        insert temp;
        List<String> lstId = new List<String>();
        lstId.add(temp.id);
        
        Test.startTest();
        List<Kinship_Check__c> lstResult = KinshipPaymentSyncController.updateCheckNo(lstId, 1);
        Test.stopTest();

        System.assertEquals(1, lstResult.size(), 'The method updateCheckNo does not return 1 result');
    }

    @isTest
    private static void testupdateVendorNumber(){
        Account vendor = [SELECT id FROM Account LIMIT 1];
        KinshipPaymentSyncController.PaymentObj obj = new KinshipPaymentSyncController.PaymentObj();
        obj.vendorId = vendor.Id;
        obj.vendorNumber = '1234';
        List<KinshipPaymentSyncController.PaymentObj> lstData = new List<KinshipPaymentSyncController.PaymentObj>();
        lstData.add(obj);
        
        Test.startTest();
        KinshipPaymentSyncController.updateVendorNumber(lstData);
        Test.stopTest();
	}
    
    @isTest
    private static void testinsertCheckFile(){
        List<Cost_Center__c> lstCC = [SELECT id FROM Cost_Center__c];
        
        Test.startTest();
        KinshipPaymentSyncController.insertCheckFile('test', 'CheckFile.TXT', lstCC[0].id);
        Test.stopTest();
	}
    
    @isTest
    private static void testUpdateFIPS(){
        List<Payment__c> lstPayment = [SELECT Id, Name FROM Payment__c LIMIT 1];
        List<String> lstId = new List<String>();
        for(Payment__c p : lstPayment) {
            lstId.add(p.id);
        }
        FIPS__c fips = [SELECT Id FROM FIPS__c LIMIT 1];
        
        Test.startTest();
        KinshipPaymentSyncController.updateFIPS(lstId, fips.id);
        Test.stopTest();
    }
}