public class WriteOffPaymentCtr{

    @auraEnabled
    public static Payment__c getInvoiceType(string recordid){
        list<Payment__c> invoice =[select id,Invoice_Type__c from  Payment__c where id=:recordid];
        if(invoice.size()>0 && invoice[0].Invoice_Type__c!=null)
            return invoice[0];
        return null;    
    }
    @auraEnabled
    public static ResponseWrap WriteOffInvoice(string recordid){
     string msg='';
      try{
           list<Payment__c> invoice =[select id,Invoice_Stage__c,Written_Off__c from  Payment__c where id=:recordid];
           list<Payment__c> invoiceToUpdate = new list<Payment__c>();
           if(invoice.size()>0 && invoice[0].Written_Off__c==false){
               Payment__c invoicerec = new Payment__c();
               invoicerec.id = invoice[0].id;
               invoicerec.Written_Off__c =true;
               invoicerec.Invoice_Stage__c='Write off';
               invoiceToUpdate.add(invoicerec);
           }
           
           if(invoiceToUpdate.size()>0){
               update invoiceToUpdate;
                ResponseWrap wrap = new ResponseWrap();
                wrap.msg='Record updated';
                wrap.paymentrec= invoiceToUpdate[0];
                return wrap;
            }  
          return null;  
        } //try ends here           
          catch(Exception e){
            system.debug('***Exception e'+e.getStackTraceString());
            system.debug('***msg'+e.getMessage());
            //throw all other exception message
            if(!Test.isRunningTest())
                throw new AuraHandledException(e.getMessage());
            else
                return null;
        }
                                       
    }
    
    public class ResponseWrap{
        @auraEnabled
        public string msg;
        @auraEnabled
        public Payment__c paymentrec;
    }

    
}