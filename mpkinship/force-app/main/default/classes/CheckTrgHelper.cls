public class CheckTrgHelper{

    public static boolean IsCancelledRun=false;
    public static boolean IsResissued=false;
    
    //FOR CANCELLED STATUS
    public static void CancelledCheckUpdates(set<id> checkIds){
        system.debug('**Entedd in cancelled method');
        TransactionJournalService.byPassCheckPosted = true;
        IsCancelledRun=true;
        Savepoint sp1 = Database.setSavepoint(); 
        try{  
            //ORIGINAL INVOICES TO UPDATE
            list<Payment__c> payments = new list<Payment__c>();
            //ADJUSTMENT RECORDS TO CREATE
            list<Payment__c> adjustmentsToCreate = new list<Payment__c>();
            //GETTING PO IDS
            set<id> POIDs = new set<id>();
            
             Id AdjustmentsRecordTypeId =Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Adjustments').getRecordTypeId(); 
             id vendorinvoiceRecordTypeid = Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Vendor_Invoice').getRecordTypeId(); 
            list<Payment__c> checkPayments=[select id,Cancelled__c,Payment_Source__c,Amount_Paid__c,Invoice_Number__c,Invoice_Stage__c,recordtypeid,Invoice_Type__c,Kinship_Case__c,Paid__c,Written_Off__c,Payment__c,Global_Search_Name__c,
                                                    Vendor__c,Prior_Refund1__c,Employee_Rate__c,FICA_Withholding__c,Cost_Center__c,Payment_Method__c,Scheduled_Date__c,Service_Begin_Date__c,Service_End_Date__c,
                                                    Fund__c,Category_lookup__c,Category_lookup__r.category_type__c,FIPS_Code__c,Payment_Date__c,Payment_Amount__c, Opportunity__c,
                                                    Opportunity__r.stagename,Kinship_Check__c,Kinship_Check__r.Voided_Date__c,(select id,Invoice__c,LineItem_Index__c,Remaining_Amount__c,Max_Quantity_Authorized__c,Unit_Price__c,
                                                    Remaining_Quantity__c,Quantity_Billed__c,Opportunity_Product__c,Previously_Billed__c,Max_Hours_Authorized__c,LineItem_Amount_Billed__c from Invoice_Line_Items__r) from
                                                    Payment__c where Kinship_Check__c in:checkIds and Invoice_Stage__c=:'Paid' and Paid__c=:true];
           system.debug('**VAl of checkpayments'+ checkPayments.size());
           
           //FOR CREATING NEGATIVE CASEACTIONS FOR COMPANION PAYROLL CATEGORY OF INVOICES
           list<Payment__c> paymentCompanionPayroll = new list<Payment__c>();
           set<id> PaymentCompanionIds = new set<id>();
           for(Payment__c payment : checkPayments){
               if(payment.Category_lookup__r.category_type__c=='Companion Payroll' && payment.Employee_Rate__c!=null &&
                   payment.FICA_Withholding__c >0){
                   paymentCompanionPayroll.add(payment);
                   PaymentCompanionIds.add(payment.id);
               }
           }
           CaseActionsForCompanionPayrollCategory(paymentCompanionPayroll,PaymentCompanionIds);
           
          // Change the status of the related Invoice from Paid to Cancelled. Keep Paid as True. Change Initial Amount/Encumbered Amount to 0 and creating adjustment
           map<id,Payment__c> paymentAdjustmentmap = new map<id,Payment__c>();
           
            for(Payment__c payment : checkPayments){
                
                POIDs.add(payment.Opportunity__c);
                Payment__c  adjustmentpayment = payment.clone(false, true, false, false);  
                adjustmentpayment.Invoice_Stage__c='Cancelled';
                adjustmentpayment.Cancelled__c=false;
                //ADDED AS PER KIN-1502
                adjustmentpayment.payment__c=payment.id;
                if(payment.Payment_Amount__c!=null)
                    adjustmentpayment.Payment_Amount__c = -payment.Payment_Amount__c;
                adjustmentpayment.recordtypeid= AdjustmentsRecordTypeId;
                if(payment.Amount_Paid__c!=null)
                    adjustmentpayment.Amount_Paid__c= -payment.Amount_Paid__c;
                if(payment.Kinship_Check__r.Voided_Date__c!=null)
                    adjustmentpayment.Payment_Date__c=payment.Kinship_Check__r.Voided_Date__c;               
                adjustmentsToCreate.add(adjustmentpayment);
                
                if(payment.Invoice_Type__c=='POSO' && payment.recordtypeid==vendorinvoiceRecordTypeid)
                   paymentAdjustmentmap.put(payment.id,adjustmentpayment);
                //CHANGE DONE TO ORIGINAL PAYMENT
                Payment__c paymentrec = new Payment__c();
                paymentrec.id=payment.id;
              /*  if(payment.Invoice_Type__c=='POSO' && payment.recordtypeid==vendorinvoiceRecordTypeid){                      
                    paymentrec.Invoice_Stage__c='Cancelled';
                    paymentrec.Amount_Paid__c=0;
                }
                else if(payment.Invoice_Type__c=='Case Action' && payment.recordtypeid==vendorinvoiceRecordTypeid) */
                paymentrec.Cancelled__c=true;    
                payments.add(paymentrec);
                system.debug('**Val of payments'+paymentrec);
              
            } 
            
            system.debug('**VAl of adjustmenttocreate'+adjustmentsToCreate.size());
            system.debug('**Val of payments'+ payments);  
                                         
            //CREATE NEW INVOICE RECORDS
            set<id> POTypePaymentIds = new set<id>();
            set<id> updatedopportunitiesids = new set<id>();
            list<opportunity> opplisttoUpdate = new list<opportunity>();
            
            map<Payment__c,Payment__c> OldNewPaymentMap = new map<Payment__c,Payment__c>();
            for(Payment__c  paymentrec : checkPayments){
                Payment__c  paymentClone = paymentrec.clone(false, true, false, false);  
                paymentClone.Payment_Amount__c=null;
                paymentClone.Cancelled__c=false;
                //ADDED AS PER KIN-1502
                paymentClone.payment__c=paymentrec.payment__c;
                //CHECK LATERFOR READY TO PAY FOR CASEACTION 
                if(paymentrec.recordtypeid==vendorinvoiceRecordTypeid ){
                    if(paymentrec.Invoice_Type__c=='POSO'){
                        paymentClone.Invoice_Stage__c='Scheduled';
                        paymentClone.Payment_Amount__c=null;
                    }    
                    else{
                        paymentClone.Payment_Amount__c = paymentrec.Payment_Amount__c;
                        //kin-1483
                        //COMMENTED FOR KIN-1538
                        if( paymentrec.Scheduled_Date__c < date.today().adddays(15))
                            paymentClone.Invoice_Stage__c='Ready to Pay';
                        else
                            paymentClone.Invoice_Stage__c='Scheduled';
                         //paymentClone.Invoice_Stage__c='Cancelled';    
                    }
                }
                else{
                    paymentClone.Invoice_Stage__c='Ready to Pay';
                    paymentClone.Payment_Amount__c=paymentrec.Payment_Amount__c;
                }    
                paymentClone.Paid__c=false;
                paymentClone.Written_Off__c=false;
                paymentClone.Kinship_Check__c=null;
                paymentClone.Payment_Date__c=null;
                paymentClone.Payment_Method__c = paymentrec.Payment_Method__c;
                OldNewPaymentMap.put(paymentrec,paymentClone);
                if(paymentrec.recordtypeid==vendorinvoiceRecordTypeid && paymentrec.Invoice_Type__c=='POSO')
                    POTypePaymentIds.add(paymentrec.id);
                
                //ADDING PO TO STAGE CHANGE
                if(paymentrec.Opportunity__r.stagename=='Closed' && !updatedopportunitiesids.contains(paymentrec.Opportunity__c)){
                    opportunity opp = new opportunity();
                    opp.id= paymentrec.Opportunity__c;
                    opp.stagename='Open';
                    updatedopportunitiesids.add(opp.id);
                    opplisttoUpdate.add(opp);
                }
            }
                 
           system.debug('**VAl of oldnewpaymentmap'+OldNewPaymentMap); 
           system.debug('**VAl of oldnewpaymentmap size'+OldNewPaymentMap.size());
            map<id,decimal> opportuniytlineitemQuantitybilledMap = new map<id,decimal>();
            list<AggregateResult> aggregatelist=[select Opportunity_Product__c,sum(Quantity_Billed__c)qb from Payment_Line_Item__c 
                                                where invoice__c not in:POTypePaymentIds and invoice__r.Opportunity__c in:POIDs and 
                                                invoice__r.recordtypeid=:vendorinvoiceRecordTypeid and invoice__r.invoice_Stage__C!='Cancelled' and invoice__r.cancelled__c=false group by Opportunity_Product__c];
            system.debug('**Aggreagate list size'+aggregatelist.size()); 
            for(AggregateResult ag : aggregatelist){
                id opportuntiylineitemid = (id)ag.get('Opportunity_Product__c');
                decimal quantitybilled = (Decimal) ag.get('qb');
                if(quantitybilled==null)
                    quantitybilled=0;
                opportuniytlineitemQuantitybilledMap.put(opportuntiylineitemid,quantitybilled);
            }  
            system.debug('**quantitybilled lineitem map size'+opportuniytlineitemQuantitybilledMap);
            
            //CREATE DUPLICATE PAYMENT RECORDS FIRST
            if(OldNewPaymentMap.size()>0)
                insert OldNewPaymentMap.values();
            system.debug('***oldnewpaymentmap values'+ oldnewpaymentmap.values());    
            system.debug('***till line 96');     
            //CLONING PAYMENT LINEITEMS 
            list<Payment_Line_Item__c > newPaymentLineitemsToCreate = new list<Payment_Line_Item__c>();
            system.debug('**map val'+ opportuniytlineitemQuantitybilledMap);
           // if(opportuniytlineitemQuantitybilledMap.size()>0){
                for(Payment__c payment : checkPayments){
                    system.debug('**VAl of payment'+ payment);
                    for(Payment_Line_Item__c item : payment.Invoice_Line_Items__r){
                        system.debug('**VAl of item'+item);
                        if(OldNewPaymentMap.size()>0 && OldNewPaymentMap.containskey(payment)){
                            Payment_Line_Item__c paymentlineitemClone = item.clone(false, true, false, false); 
                            paymentlineitemClone.invoice__c=OldNewPaymentMap.get(payment).id;
                           // if(opportuniytlineitemQuantitybilledMap.size()>0 && opportuniytlineitemQuantitybilledMap.containskey(item.Opportunity_Product__c)){
                                    system.debug('**Val of opplineitemquantitybilledmap'+opportuniytlineitemQuantitybilledMap.containskey(item.Opportunity_Product__c));
                                    if(opportuniytlineitemQuantitybilledMap.size()> 0  && opportuniytlineitemQuantitybilledMap.containskey(item.Opportunity_Product__c)){
                                        paymentlineitemClone.Previously_Billed__c = opportuniytlineitemQuantitybilledMap.get(item.Opportunity_Product__c);
                                    }
                                    else
                                        paymentlineitemClone.Previously_Billed__c = 0;
                                    paymentlineitemClone.Quantity_Billed__c=null;
                                    paymentlineitemClone.LineItem_Amount_Billed__c =null;
                                    paymentlineitemClone.Remaining_Quantity__c=item.Max_Quantity_Authorized__c - paymentlineitemClone.Previously_Billed__c;
                                    paymentlineitemClone.Remaining_Amount__c=((item.Max_Quantity_Authorized__c  - paymentlineitemClone.Previously_Billed__c) * item.Unit_Price__c).setscale(2);
                                   system.debug('**paymentlineitemclone'+ paymentlineitemClone);
                                    newPaymentLineitemsToCreate.add(paymentlineitemClone);
                                
                            //}
                        }   
                         
                    }
                } 
          //  }  //IF OF CHECKING THE SIZE THAT PAYMENT MAP HAS VALUE.                               
            system.debug('**Amy new paymentlineitemto create'+newPaymentLineitemsToCreate.size() );
            //CODE FOR UPDATING EXISTING LINE ITEMS WITH REMAINING QUANTITY AND PREVIOUSLY BILLED
           list<Payment_Line_Item__c> oldLineItemsToUpdate = new list<Payment_Line_Item__c>();
            if(newPaymentLineitemsToCreate.size()>0){
                list<Payment_Line_Item__c> remainingPaymentItems =[select id,name,Previously_Billed__c,Remaining_Amount__c,LineItem_Amount_Billed__c,Quantity_Billed__c,Unit_Price__c,Unit__c,Remaining_Quantity__c,
                                                      Product__c,Unit_Measure__c,Max_Quantity_Authorized__c,Opportunity_Product__c from Payment_Line_Item__c where invoice__c not in:POTypePaymentIds and
                                                      invoice__r.Opportunity__c in:POIDs and invoice__r.invoice_stage__c!='Cancelled'  and invoice__r.cancelled__c=false and invoice__r.recordtypeid=:vendorinvoiceRecordTypeid ];

                                                       //payment__r.npe01__Opportunity__c in:POIDs and payment__r.invoice_stage__c='Cancelled' and payment__r.recordtypeid=:vendorinvoiceRecordTypeid ];
                system.debug('**Val of remaingpaymentitems'+remainingPaymentItems );
                for(Payment_Line_Item__c pitem : remainingPaymentItems){
                    if(opportuniytlineitemQuantitybilledMap.size() > 0 && opportuniytlineitemQuantitybilledMap.containskey(pitem.Opportunity_Product__c)){
                            system.debug('**old payment val'+ pitem);
                            Payment_Line_Item__c pitemupdate = new Payment_Line_Item__c();
                            pitemupdate.id = pitem.id;
                            pitemupdate.Previously_Billed__c= opportuniytlineitemQuantitybilledMap.get(pitem.Opportunity_Product__c);
                            pitemupdate.Remaining_Quantity__c= (pitem.Max_Quantity_Authorized__c  - pitemupdate.Previously_Billed__c).setscale(2);
                            pitemupdate.Remaining_Amount__c = ((pitem.Max_Quantity_Authorized__c  - pitemupdate.Previously_Billed__c) * pitem.Unit_Price__c).setscale(2); 
                            system.debug('**Val of pitemupdate'+pitemupdate);
                            oldLineItemsToUpdate.add(pitemupdate);  
                    }
                }                                     
            }
           system.debug('**oldl ineitemstoupdate'+ oldLineItemsToUpdate.size()); 
            //CREATE ADJUSTMENTS 
            if(adjustmentsToCreate.size()>0)
                insert adjustmentsToCreate;
                
            //CREATE INOVIE LINE ITEMS FOR ADJUSTMENTS 
            list<Payment_Line_Item__c> adjustmentlineitemsToCreate = new list<Payment_Line_Item__c>();
             list<Payment_Line_Item__c> PaymentItems =[select id,name,Previously_Billed__c,Remaining_Amount__c,LineItem_Amount_Billed__c,Quantity_Billed__c,Unit_Price__c,Unit__c,Remaining_Quantity__c,
                                                      Product__c,Unit_Measure__c,Max_Quantity_Authorized__c,Opportunity_Product__c,invoice__c from Payment_Line_Item__c where invoice__c in: paymentAdjustmentmap.keyset()];
             for(Payment_Line_Item__c paylineitem : PaymentItems){
                 if(paymentAdjustmentmap.size() >0 && paymentAdjustmentmap.containskey(paylineitem.invoice__c)){
                      Payment_Line_Item__c adjustmentpaymentlineitem = paylineitem.clone(false, true, false, false);
                      adjustmentpaymentlineitem.invoice__c= paymentAdjustmentmap.get(paylineitem.invoice__c).id;
                      adjustmentlineitemsToCreate.add(adjustmentpaymentlineitem);
                  }     
             }        
              
            if(adjustmentlineitemsToCreate.size()>0)
                insert adjustmentlineitemsToCreate;     
            if( newPaymentLineitemsToCreate.size()>0)
                insert newPaymentLineitemsToCreate;   
           if(oldLineItemsToUpdate.size()>0)
               update oldLineItemsToUpdate; 
            //UPDATE THE ORIGINAL PAYMENT AT LAST 
            system.debug('**Val of payments'+ payments);
            if(payments.size()>0)
                update payments;  
            if(opplisttoUpdate.size()>0)
                update opplisttoUpdate;     
           system.debug('till 153');
           system.debug('val of sp'+ sp1);  
           TransactionJournalService.byPassCheckPosted = false;        
       } //TRY BLOCK ENDS HERE 
       catch(DmlException  e){
            Database.rollback(sp1);
            system.debug('**Exception of dml of cancelled check'+e.getStackTraceString());
        }catch(Exception e){
            system.debug('***Exception e of cancelled check'+e.getStackTraceString());
            system.debug('**Exception of e for cancelled check val'+e);
            Database.rollback(sp1);
            
        }                                         
    }
    
    //REISSUED CHECK METHOD
    public static void ReissuedCheckUpdates(set<id> reissueIds){
        system.debug('**insert inside reissued');
        TransactionJournalService.byPassCheckPosted = true;
        Savepoint sp = Database.setSavepoint();        
        IsResissued=true;
        try{
            //ORIGINAL INVOICES TO UPDATE
            list<Payment__c> payments = new list<Payment__c>();
            //ADJUSTMENT RECORDS TO CREATE
             //GETTING PO IDS
            set<id> POIDs = new set<id>();
            list<Payment__c> adjustmentsToCreate = new list<Payment__c>();
            Id AdjustmentsRecordTypeId =Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Adjustments').getRecordTypeId(); 
             id vendorinvoiceRecordTypeid = Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Vendor_Invoice').getRecordTypeId(); 
            
             list<Payment__c> checkPayments=[select id,Amount_Paid__c,Payment_Source__c,Invoice_Number__c,Invoice_Stage__c,recordtypeid,Invoice_Type__c,Kinship_Case__c,Paid__c,Written_Off__c,payment__c,Cancelled__c,Global_Search_Name__c,
                                                    Vendor__c,Prior_Refund1__c,Employee_Rate__c,FICA_Withholding__c,Cost_Center__c,Payment_Method__c,Scheduled_Date__c,Service_Begin_Date__c,Service_End_Date__c,Fund__c,Category_lookup__c,FIPS_Code__c,Payment_Date__c,Payment_Amount__c,
                                                    Opportunity__c,Opportunity__r.stagename,Kinship_Check__c,Kinship_Check__r.Voided_Date__c,(select id,invoice__c,LineItem_Index__c,Remaining_Amount__c,Max_Quantity_Authorized__c,Unit_Price__c,
                                                    Remaining_Quantity__c,Quantity_Billed__c,Opportunity_Product__c,Previously_Billed__c,Max_Hours_Authorized__c,LineItem_Amount_Billed__c from Invoice_Line_Items__r) from
                                                    Payment__c where Kinship_Check__c in: reissueIds and Invoice_Stage__c=:'Paid' and Paid__c=:true];
            system.debug('**VAl of reissued checkpayments'+ checkpayments);
             // Change the status of the related Invoice from Paid to Cancelled. Keep Paid as True. Change Initial Amount/Encumbered Amount to 0 and creating adjustment
             map<id,Payment__c> paymentAdjustmentmap = new map<id,Payment__c>();
            
            for(Payment__c payment : checkPayments){
                POIDs.add(payment.Opportunity__c);
                Payment__c  adjustmentpayment = payment.clone(false, true, false, false);  
                adjustmentpayment.Invoice_Stage__c='Cancelled';
                 adjustmentpayment.Cancelled__c=false;
                //ADDED AS PER KIN-1502
                adjustmentpayment.payment__c=payment.id;
                if(payment.Payment_Amount__c!=null)
                    adjustmentpayment.Payment_Amount__c = -payment.Payment_Amount__c;
                adjustmentpayment.recordtypeid= AdjustmentsRecordTypeId;
                if(payment.Amount_Paid__c!=null)
                    adjustmentpayment.Amount_Paid__c= -payment.Amount_Paid__c;
                if(payment.Kinship_Check__r.Voided_Date__c!=null)
                    adjustmentpayment.Payment_Date__c=payment.Kinship_Check__r.Voided_Date__c;               
                adjustmentsToCreate.add(adjustmentpayment);
                
                 if(payment.Invoice_Type__c=='POSO' && payment.recordtypeid==vendorinvoiceRecordTypeid)
                   paymentAdjustmentmap.put(payment.id,adjustmentpayment);
                   
                //CHANGE DONE TO ORIGINAL PAYMENT
                Payment__c paymentrec = new Payment__c();
                paymentrec.id=payment.id;
                /*if(payment.Invoice_Type__c=='POSO' && payment.recordtypeid==vendorinvoiceRecordTypeid){                      
                    paymentrec.Invoice_Stage__c='Cancelled';
                    paymentrec.Amount_Paid__c=0;
                }
                else if(payment.Invoice_Type__c=='Case Action' && payment.recordtypeid==vendorinvoiceRecordTypeid) */
                paymentrec.Cancelled__c=true; 
                
                payments.add(paymentrec);
                
            }  
            system.debug('**REissued adjustment record size'+adjustmentsToCreate.size());
            system.debug('**payemntssize'+payments.size());
            
           
             //CREATE NEW INVOICE RECORDS
            set<id> POTypePaymentIds = new set<id>();
            set<id> updatedopportunitiesids = new set<id>();
            list<opportunity> opplisttoUpdate = new list<opportunity>();
            
            map<Payment__c,Payment__c> OldNewPaymentMap = new map<Payment__c,Payment__c>();
            for(Payment__c  paymentrec : checkPayments){
                Payment__c  paymentClone = paymentrec.clone(false, true, false, false);  
                paymentClone.Cancelled__c=false;
                 //ADDED AS PER KIN-1502
                paymentClone.payment__c=paymentrec.payment__c;
                //STAGE PART IS COMMENTED HERE FOR KIN-1538
                paymentClone.Invoice_Stage__c='Ready to Pay';
              /* if(paymentrec.recordtypeid==vendorinvoiceRecordTypeid && paymentrec.Invoice_Type__c=='Case Action')
                   paymentClone.Invoice_Stage__c='Cancelled';
                else
                    paymentClone.Invoice_Stage__c='Ready to Pay'; */   
                paymentClone.Paid__c=false;
                paymentClone.Written_Off__c=false;
                paymentClone.Kinship_Check__c=null;
                paymentClone.Payment_Date__c=null;
                paymentClone.Payment_Amount__c=paymentrec.Payment_Amount__c;
                paymentClone.Payment_Method__c = paymentrec.Payment_Method__c;
                OldNewPaymentMap.put(paymentrec,paymentClone);
                if(paymentrec.recordtypeid==vendorinvoiceRecordTypeid && paymentrec.Invoice_Type__c=='POSO'){                    
                    POTypePaymentIds.add(paymentrec.id);
                    
                }
                
                        
                 //ADDING PO TO STAGE CHANGE
                if(paymentrec.Opportunity__r.stagename=='Closed' && !updatedopportunitiesids.contains(paymentrec.Opportunity__c)){
                    opportunity opp = new opportunity();
                    opp.id= paymentrec.Opportunity__c;
                    opp.stagename='Open';
                    updatedopportunitiesids.add(opp.id);
                    opplisttoUpdate.add(opp);
                }    
                  
            }   
            
            //CREATE DUPLICATE PAYMENT RECORDS FIRST
            if(OldNewPaymentMap.size()>0)
                insert OldNewPaymentMap.values();
            
            system.debug('**val of oldnewpaymentmap reissued'+ OldNewPaymentMap); 
            system.debug('**val of oldnewpaymentmap reissued size'+ OldNewPaymentMap.size());    
            //NOW CLONING PAYMENTLINEITEMS can be commented because we are using deep clone.
            list<Payment_Line_Item__c> lineItemsClone = new list<Payment_Line_Item__c>();
            if(POTypePaymentIds.size()>0){
                for(Payment__c payment : checkPayments){
                    for(Payment_Line_Item__c pitem : payment.Invoice_Line_Items__r){
                        if(OldNewPaymentMap.size()>0 && OldNewPaymentMap.containskey(payment)){
                            Payment_Line_Item__c clonepitem = pitem.clone(false, true, false, false);
                            clonepitem.invoice__c= OldNewPaymentMap.get(payment).id;  
                            lineItemsClone.add(clonepitem);
                        }
                    }
                } 
            }   
            system.debug('**lineitemsclone reissued'+lineItemsClone.size());
           
             //CREATE ADJUSTMENTS 
           if(adjustmentsToCreate.size()>0)
               insert adjustmentsToCreate; 
                
            //CREATE INOVIE LINE ITEMS FOR ADJUSTMENTS 
            list<Payment_Line_Item__c> adjustmentlineitemsToCreate = new list<Payment_Line_Item__c>();
             list<Payment_Line_Item__c> PaymentItems =[select id,name,Previously_Billed__c,Remaining_Amount__c,LineItem_Amount_Billed__c,Quantity_Billed__c,Unit_Price__c,Unit__c,Remaining_Quantity__c,
                                                      Product__c,Unit_Measure__c,Max_Quantity_Authorized__c,Opportunity_Product__c,invoice__C from Payment_Line_Item__c where invoice__c in: paymentAdjustmentmap.keyset()];
             for(Payment_Line_Item__c paylineitem : PaymentItems){
                 if(paymentAdjustmentmap.size() >0 && paymentAdjustmentmap.containskey(paylineitem.invoice__c)){
                      Payment_Line_Item__c adjustmentpaymentlineitem = paylineitem.clone(false, true, false, false);
                      adjustmentpaymentlineitem.invoice__c= paymentAdjustmentmap.get(paylineitem.invoice__c).id;
                      adjustmentlineitemsToCreate.add(adjustmentpaymentlineitem);
                  }     
             }   
             
                
           if(adjustmentlineitemsToCreate.size()>0)
                insert adjustmentlineitemsToCreate;  
                       
           if(lineItemsClone.size()>0)
               insert lineItemsClone;  
             //UPDATE THE ORIGINAL PAYMENT AT LAST 
            if(payments.size()>0)
                update payments; 
            if(opplisttoUpdate.size()>0)
                update opplisttoUpdate;      
            system.debug('reissued till 259');    
            TransactionJournalService.byPassCheckPosted = false;    
                     
       } //TRY BLOCK ENDS
       catch(DmlException  e){
            Database.rollback(sp);
            system.debug('**Exception of dml'+e.getStackTraceString());
        }catch(Exception e){
            system.debug('***Exception e'+e.getStackTraceString());
            Database.rollback(sp);
            
        }                                       
        
    }
    
    //CASEACTIONS FOR NEGATIVE PAYMENTS
    public static void CaseActionsForCompanionPayrollCategory(list<Payment__c> payments,set<id> paymentids){
        system.debug('**VAl of payments 419'+payments);
        system.debug('**Val of payment ids'+paymentids);
        list<Payment__c> Childpayments = [select id,FICA_Payment_Id__c,Opportunity__c from Payment__c where FICA_Payment_Id__c In:paymentids];
        system.debug('**VAl of childpayments'+childpayments);
        set<id> caseactionIds= new set<id>();
        map<id,id> caseActionPaymentIdMap = new map<id,id>();
        
        for(Payment__c payment : Childpayments){
            caseactionIds.add(payment.Opportunity__c);
            caseActionPaymentIdMap.put(payment.Opportunity__c,payment.FICA_Payment_Id__c);
        }
        
        list<opportunity> opportunities=[select id,name,closedate,amount,accountid,recordtypeid,Type_of_Case_Action__c,End_Date__c,category__c,
                    Number_of_Months_Units__c,Kinship_Case__c,campaignid,Fund__c,description,stagename,FIPS_Code__c,Cost_Center__c from
                    opportunity where id in: caseactionIds];
        list<opportunity> caseActionsToCreate = new list<opportunity>(); 
        map<id,opportunity> oldNewCaseActionsMap = new map<id,opportunity>();
                   
        for(opportunity opp : opportunities){
             opportunity caseactionNegative= opp.clone(false, true, false, false); 
             caseactionNegative.amount = -(opp.amount); 
             caseActionsToCreate.add(caseactionNegative); 
             oldNewCaseActionsMap.put(opp.id,caseactionNegative);  
        }  
        
        if(caseActionsToCreate.size()>0)
            insert caseActionsToCreate;
         
         system.debug('**VAl of oldNewCaseActionsMap'+ oldNewCaseActionsMap);
         map<id,id> NewCaseActionmap = new map<id,id>();
         for(id opp : oldNewCaseActionsMap.keyset()){
             if(oldNewCaseActionsMap.containskey(opp))
                 NewCaseActionmap.put(oldNewCaseActionsMap.get(opp).id,opp);    
         }
        system.debug('**VAl of newcaseacitonmap'+  NewCaseActionmap);  
        //NOW CREATE CHILD PAYMENTS
       list<opportunity> createdCaseActions =[select id,name,closedate,amount,recordtypeid,Type_of_Case_Action__c,End_Date__c,category__c,accountid,
                    Number_of_Months_Units__c,Kinship_Case__c,campaignid,Fund__c,description,stagename,FIPS_Code__c,Cost_Center__c from
                    opportunity where id in: NewCaseActionmap.keyset()];
          system.debug('****Createdcaseactions'+ createdCaseActions );             
       list<Payment__c> paymentsOfCaseActions = new list<Payment__c>();             
       Id AdjustmentsRecordTypeId =Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Adjustments').getRecordTypeId(); 
       //Id vendorRecordTypeId= Schema.SObjectType.npe01__OppPayment__c.getRecordTypeInfosByDeveloperName().get('Vendor_Invoice').getRecordTypeId(); 
       for(opportunity opp : createdCaseActions){
             Payment__c payment = new Payment__c();
                 //changed again from kin 1538
                payment.recordtypeid = AdjustmentsRecordTypeId;
                payment.Opportunity__c= opp.id;
                payment.FIPS_Code__c=opp.FIPS_Code__c;
                payment.Vendor__c=opp.accountid;
                payment.Kinship_Case__c=opp.Kinship_Case__c;
                payment.Service_Begin_Date__c=opp.closedate;
                payment.Service_End_Date__c=opp.End_Date__c;
                payment.Scheduled_Date__c=opp.closedate;
                payment.Amount_Paid__c=opp.amount;
                payment.Payment_Method__c='Check';
                payment.Payment_Amount__c=opp.amount;
                if(opp.Cost_Center__c!=null)
                    payment.Cost_Center__c= opp.Cost_Center__c;
                 if(opp.Category__c !=null)
                    payment.Category_lookup__c=opp.Category__c ;     
                payment.Description__c=opp.description;
                payment.Fund__c=opp.Fund__c;
                //COMMENTED AS PER USER STORY KIN-1538               
                //payment.Invoice_Stage__c='Ready to Pay'; 
                payment.Invoice_Stage__c='Cancelled'; 
                if(NewCaseActionmap.size() >0 && NewCaseActionmap.containskey(opp.id)){
                    id oldcaseactionid = NewCaseActionmap.get(opp.id);
                    if(caseActionPaymentIdMap.size()>0 && caseActionPaymentIdMap.containskey(oldcaseactionid)){
                         payment.FICA_Payment_Id__c=caseActionPaymentIdMap.get(oldcaseactionid);    
                    }
                
                } 
                paymentsOfCaseActions.add(payment);    
       
       }  
       
       if(paymentsOfCaseActions.size()>0)
           insert paymentsOfCaseActions;           
        
    }

}