@isTest
private class KinshipOpportunityUpdateControllerTest {
    @TestSetup
    static void initData(){
        Id profileId1 = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User u = new User(FirstName='Test',LastName= 'User',profileid=profileId1 , email='abc123@test.com',
                         username='abc123@test.com.prod',Alias='zbd',LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', EmailEncodingKey='UTF-8',
                          TimeZoneSidKey='America/Los_Angeles');
        
        Account acc = new Account(Name = 'test acc', Tax_ID__c = '123-56-7901', Entity_Type__c = 'Individual');
        insert acc;

        Locality_Account__c locality = new Locality_Account__c(Name = '12345');
        insert locality;

        Category__c cate = new Category__c(Name = 'test cate', Category_Type__c = 'CSA', Locality_Account__c = locality.id , RecordTypeId = Schema.SObjectType.Category__c.getRecordTypeInfosByName().get('LEDRS Category').getRecordTypeId());
        insert cate;
        
        Kinship_Case__c kcase = new Kinship_Case__c(Primary_Social_Worker__c = u.id);
        
        FIPS__c fips1 = new FIPS__c(Name = 'Covington', Locality__c = 'Covington');
        insert fips1;
        
        FIPS__c fips2 = new FIPS__c(Name = 'Alleghany', Locality__c = 'Alleghany');
        insert fips2;
        
        Campaign Pc = new Campaign(name = 'Local - 2020', Fiscal_Year_Type__c = 'Local', RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId());
        insert Pc;
        
        Campaign parentC = new Campaign(name = 'DSS - 2020', Fiscal_Year_Type__c = 'DSS',ParentId = Pc.id, RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Fiscal Year').getRecordTypeId());
        insert parentC;
        
        Campaign c = new Campaign(name = 'LASER - 2020 - Alleghany', ParentId = parentC.id,FIPS__c = fips1.id, RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('LASER Reporting Period').getRecordTypeId());
        insert c;
        
        c = new Campaign(name = 'LASER - 2020 - Covington', ParentId = parentC.id,FIPS__c = fips2.id, RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('LASER Reporting Period').getRecordTypeId());
        insert c;
        

        Opportunity opp = new Opportunity(Name = 'test Opp', AccountId = acc.id, Category__c = cate.id, CloseDate = Date.today().addDays(10),
                                          StageName='Open', Kinship_Case__c = kcase.id, FIPS_Code__c = fips1.id,CampaignId = Pc.Id,
                                          RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Purchase of Services Order').getRecordTypeId());
        insert opp;

        Payment__c payment = new Payment__c(Opportunity__c = opp.id, 
                                                                Scheduled_Date__c = Date.today(), 
                                                                Status__c = 'Submitted For Approval',
                                                                Invoice_Stage__c = 'Ready to Pay',
                                                                Written_Off__c = false,
                                                                Payment_Method__c = 'Check',
                                                                Category_lookup__c = cate.id,
                                                                Payment_Amount__c = 100,
                                                                FIPS_Code__c = fips1.id);
        insert payment;
        
        Cost_Center__c cc = new Cost_Center__c(Name = 'Cost Center', Cost_Center_Code__c = '12345');
        insert cc;
    }
    
    @isTest
    static void testKinshipSearchLookup_WithOpp(){
        List<Opportunity> lstData = [select id, name from Opportunity];
        List<Id> fixedSearchResults= new List<Id>();
        fixedSearchResults.add(lstData[0].id);
        Test.setFixedSearchResults(fixedSearchResults);
        Test.startTest();
        List<Object> lstResult = KinshipOpportunityUpdateController.kinshipSearchLookup('opp', new List<String>(), 'PO');
        Test.stopTest();
        System.assertEquals(1, lstResult.size(), 'The kinshipSearchLookup method return does not equal 1');
    }
    
    @isTest
    static void testKinshipSearchLookup_WithPayment(){
        List<Payment__c> lstData = [select id, name from Payment__c];
        List<Id> fixedSearchResults= new List<Id>();
        fixedSearchResults.add(lstData[0].id);
        Test.setFixedSearchResults(fixedSearchResults);
        Test.startTest();
        List<Object> lstResult = KinshipOpportunityUpdateController.kinshipSearchLookup('Payment', new List<String>(), 'Payment');
        Test.stopTest();
        System.assertEquals(1, lstResult.size(), 'The kinshipSearchLookup method return does not equal 1');
    }
    
    @isTest
    static void testKinshipSearchLookup_WithFIPS(){
        List<FIPS__c> lstData = [select id, name from FIPS__c];
        List<Id> fixedSearchResults= new List<Id>();
        fixedSearchResults.add(lstData[0].id);
        Test.setFixedSearchResults(fixedSearchResults);
        Test.startTest();
        List<Object> lstResult = KinshipOpportunityUpdateController.kinshipSearchLookup('FIPS', new List<String>(), 'FIPS');
        Test.stopTest();
        System.assertEquals(1, lstResult.size(), 'The kinshipSearchLookup method return does not equal 1');
    }
    
    @isTest
    static void testKinshipSearchLookup_WithCC(){
        List<Cost_Center__c> lstData = [select id, name from Cost_Center__c];
        List<Id> fixedSearchResults= new List<Id>();
        fixedSearchResults.add(lstData[0].id);
        Test.setFixedSearchResults(fixedSearchResults);
        Test.startTest();
        List<Object> lstResult = KinshipOpportunityUpdateController.kinshipSearchLookup('Cost', new List<String>(), 'CC');
        Test.stopTest();
        System.assertEquals(1, lstResult.size(), 'The kinshipSearchLookup method return does not equal 1');
    }
    
    @isTest
    static void testGetOpportunitis(){
        List<Payment__c> lstData = [SELECT id, name, Opportunity__c
                                                FROM Payment__c];
        List<String> lstPO = new List<String>();
        List<String> lstPayment = new List<String>();
        for(Payment__c p : lstData) {
            lstPO.add(p.Opportunity__c);
            lstPayment.add(p.id);
        }
        Test.startTest();
        List<Object> lstResult = KinshipOpportunityUpdateController.getOpportunitis(lstPO, lstPayment);
        Test.stopTest();
    }
    
    @isTest
    static void testUpdateFIPSonPOSO(){
        List<Payment__c> lstData = [SELECT id, name, Opportunity__c
                                                FROM Payment__c];
        List<String> lstPO = new List<String>();
        List<String> lstPayment = new List<String>();
        for(Payment__c p : lstData) {
            lstPO.add(p.Opportunity__c);
            lstPayment.add(p.id);
        }
        List<KinshipOpportunityUpdateController.OppWrapper> lstResult = KinshipOpportunityUpdateController.getOpportunitis(lstPO, lstPayment);
        String fips;
        for(FIPS__c f : [SELECT Id, Name FROM FIPS__c WHERE Name = 'Covington']) {
            fips = f.id;
        }
        String ccId;
        for(Cost_Center__c cc : [SELECT Id, Name FROM Cost_Center__c]) {
            ccId = cc.id;
        }
        Test.startTest();
        KinshipOpportunityUpdateController.updateFIPSonPOSO(fips, ccId, lstResult);
        Test.stopTest();
    }
}