import { LightningElement, api } from 'lwc';

export default class CaseNewForm extends LightningElement {
    @api recordId;
    @api fromOtherComponent = false;
    showLoader = false;
    handleSuccess(event) {
        console.log('handleSuccess');
        console.log(event.detail.id);
        this.showLoader = false;
        if(this.fromOtherComponent == false){
            window.location.href = '/' + event.detail.id;
        }
        else{
            console.log(event.detail.id);
            //console.log(event.detail.Name);
            const passEventr = new CustomEvent('newcase', {  
                detail: { newCaseId: event.detail.id}  
            });  
            this.dispatchEvent(passEventr);
        }
        
    }
    handleError(){
        console.log('handleError');
        this.showLoader = false;
    }
    handleSubmit(){
        console.log('handleSubmit');
        this.showLoader = true;
    }
    closeModal() {
        window.location.href = '/lightning/o/Kinship_Case__c/home'
    }
}