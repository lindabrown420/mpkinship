import { LightningElement, api, wire, track } from 'lwc';


export default class Chatterfeed extends LightningElement {
    @track isFixClass = 'slds-hide';
    @track sfdcBaseURL = window.location.origin;
    @api recId = '';
    @api
    openChatter(opid) {
        this.recId = opid;
        console.log('This.recId ' + this.recId);
        this.clickFix();
    }

    get fullUrl() {
        console.log('sfdcBaseURL ???', this.sfdcBaseURL);
        return this.sfdcBaseURL + `/apex/chatterfeed?RecordId=${this.recId}`;
    }
    clickFix() {
        this.isFixClass = 'fix-to-left';
    }
    clickClose() {
        this.isFixClass = 'slds-hide';
    }
}