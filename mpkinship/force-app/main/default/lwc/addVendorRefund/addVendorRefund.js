import { LightningElement, api, track, wire } from 'lwc';
import Date_of_Receipt from '@salesforce/schema/Payment__c.Date_of_Receipt__c';
import Case from '@salesforce/schema/Payment__c.Kinship_Case__c';
import Description from '@salesforce/schema/Payment__c.Description__c';
import RecordTypeId from '@salesforce/schema/Payment__c.RecordTypeId';
import Receipt_Type from '@salesforce/schema/Payment__c.Receipt_Type__c';
//import Pay_Method from '@salesforce/schema/Payment__c.Pay_Method__c';
import Pay_Method from '@salesforce/schema/Payment__c.Payment_Method__c';
import Amount from '@salesforce/schema/Payment__c.Payment_Amount__c';
import Category from '@salesforce/schema/Payment__c.Category_lookup__c';
import adjustAmount from '@salesforce/schema/Payment__c.Adjustment_Amount__c';
import Claims from '@salesforce/schema/Payment__c.Claims__c';
import Contact from '@salesforce/schema/Payment__c.Contact__c';
import Bank_Statement from '@salesforce/schema/Payment__c.Bank_Statement__c';
import Fund from '@salesforce/schema/Payment__c.Fund__c';
import Accounting_Period from '@salesforce/schema/Payment__c.Accounting_Period__c';
import Vendor from '@salesforce/schema/Payment__c.Vendor__c';
import CASE_NAME from '@salesforce/schema/Payment__c.Case_Name__c';

import Special_Welfare_Account from '@salesforce/schema/Payment__c.Special_Welfare_Account__c';
import { hideSpinner, showSpinner } from 'c/util';
import Report_of_Collections from '@salesforce/schema/Payment__c.Opportunity__c'
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import payment from '@salesforce/schema/Payment__c';
import getReceiptRecord from "@salesforce/apexContinuation/ROCController.getReceiptRecord";
import getVRefundsFromROC from "@salesforce/apexContinuation/ROCController.getVRefundsFromROC";
import UpdateReceiptsbyVendorRefund  from "@salesforce/apexContinuation/ROCController.UpdateReceiptsbyVendorRefund";
import getReceiptsSumbyID from "@salesforce/apexContinuation/ROCController.getReceiptsSumbyID";


const columns = [

    { label: 'Name', fieldName: 'Name', fixedWidth: 200 },
    { label: 'Date of Receipt', fieldName: 'Date_of_Receipt__c', fixedWidth: 200 },
    { label: 'Case', fieldName: 'Case_Name__c', fixedWidth: 240 },
    { label: 'Description', fieldName: 'Description__c', fixedWidth: 240 },
    { label: 'Amount', fieldName: 'Payment_Amount__c', fixedWidth: 180 },
    { label: 'Receipt Type', fieldName: 'Receipt_Type__c', fixedWidth: 200 },
];

export default class AddVendorRefund extends LightningElement {
    // The record page provides recordId and objectApiName
    @api recordId;
    @api objectApiName;
    @track showLoader = false;
    @api isUpdate;
    @api opportunityid;
    @api selectedVFund;
    @api selectedReceipt;
    @api selectedcheckrows;
    @track relatetReceipt;
    @track isRecordCreated = false;
    @track kcase = '';
    @track recnumber = '';
    @track kvendor = '';
    @track kcontact = '';
    @track payMethod = '';
    @track kfund;
    @track des = '';
    @track swaccount;
    @track receiptType = '';
    @track doreceipt = '';
    @track paymentAmount = '';
    @track kclaims;
    @track catLoop = '';
    @track accountPeriod;
    @track KinshipCheck = '';
    
    @api closedate;
    @api beginningreceipt;
    @api enddate;
    @api endingreceipt;
    columns = columns;
    recordTypeIdStr
    fields = [Special_Welfare_Account, Receipt_Type, Case, Date_of_Receipt, Vendor, Amount, Contact, Claims, Pay_Method, Category,
        Fund, Accounting_Period, Description, CASE_NAME
    ];
    getVRefundsFromROC() {
        console.log(this.selectedReceipt + ' this.selectedReceipt???');
        getVRefundsFromROC({ ROCID: this.opportunityid, RCPTID: this.selectedReceipt }).then((result) => {
                if (result) {
                    console.log(this.result + ' this.result???');
                    this.relatetReceipt = result;
                } else {
                    this.relatetReceipt = [];
                }
            })
            .catch((error) => {
                //console.log(error);
            });
    }
    @wire(getObjectInfo, { objectApiName: payment })
    getObjectdata1({ error, data }) {
        if (data) {
            console.log('data.defaultRecordTypeId - Test ' + data.defaultRecordTypeId);
            console.log('data.defaultRecordTypeId ', data.recordTypeInfos);
            for (var key in data.recordTypeInfos) {
                if (data.recordTypeInfos[key].name == "Vendor Refund") {
                    this.recordTypeIdStr = data.recordTypeInfos[key].recordTypeId;
                }
            }
            // perform your custom logic here
        } else if (error) {
            // perform your logic related to error 
        }
    };
    
    connectedCallback() {
        console.log(this.selectedReceipt + ' this.selectedReceipt???');
        console.log(this.opportunityid + ' this.opportunityid???');
        if (this.selectedReceipt != null && this.selectedReceipt != '') {
            //this.selectedVFund = this.selectedReceipt;
            getReceiptRecord({ RecieptID: this.selectedReceipt }).then((result) => {
                    if (result) {
                        this.KinshipCheck = result.Kinship_Check__c;
                        this.kcase = result.Kinship_Case__c;
                        this.kvendor = result.Vendor__c;
                        this.kcontact = result.Contact__c;
                        //this.payMethod = result.Pay_Method__c;
                        this.payMethod = result.Payment_Method__c;
                        this.kfund = result.Fund__c;
                        this.des = result.Description__c;
                        this.swaccount = result.Special_Welfare_Account__c;
                        this.receiptType = result.Receipt_Type__c;
                        this.doreceipt = result.Date_of_Receipt__c;
                        this.paymentAmount = result.Payment_Amount__c;
                        this.kclaims = result.Claims__c;
                        this.catLoop = result.Category_lookup__c;
                        this.accountPeriod = result.Accounting_Period__c;
                        this.recnumber = result.Report_Number__c;
                    }
                })
                .catch((error) => {
                    //console.log(error);
                });
                console.log(this.selectedcheckrows + 'this.selectedcheckrows ???/');
                getReceiptsSumbyID({ ReceiptsIDs: this.selectedcheckrows }).then((result) => {
                    if (result) {
                        console.log(result + ' result <><>');
                        this.paymentAmount = result;
                    }
                })
                .catch((error) => {
                    //console.log(error);
                });
        }
        if (this.opportunityid != null && this.opportunityid != '') {
            this.is = true;
            this.getVRefundsFromROC();
        }

    }

    UpdateReceiptsbyVendorRefund(){
        console.log(this.selectedcheckrows + ' this.selectedcheckrows???');
        console.log(this.selectedVFund + ' this.selectedVFund???');
        
        UpdateReceiptsbyVendorRefund({ VendorRefundId: this.selectedVFund , ReceiptsIDs: this.selectedcheckrows }).then((result) => {
                if (result) {
                    this.isRecordCreated = true;
                } else {
                    
                }
            })
            .catch((error) => {
                //console.log(error);
            });
    }
    handleSubmit(event) {
        showSpinner(this);
        //this.showLoader = true;
        event.preventDefault(); // stop the form from submitting
        //const fields = event.detail.fields;
        //fields.Opportunity__c = this.opportunityid; // modify a field
        //fields.RecordTypeId = this.recordTypeIdStr; //'012180000009TyEAAU';
        //fields.Receipt__c = this.selectedReceipt;
        //this.template.querySelector('lightning-record-form').submit(fields);
    }
    handleSuccess(event) {
        event.preventDefault(); // stop the form from submitting
        this.selectedVFund = event.detail.id;
        this.isRecordCreated = true;
        this.UpdateReceiptsbyVendorRefund();
        //this.showLoader = false;
        hideSpinner(this);
        //fields.LastName = 'My Custom Last Name'; // modify a field  
    }
    handleReset(event) {
        const inputFields = this.template.querySelectorAll(
            'lightning-input-field'
        );
        if (inputFields) {
            inputFields.forEach(field => {
                field.reset();
            });
        }
    }
    errorHandler(e) {
        console.log('s');
        this.template.querySelector(".slds-notify_container.slds-is-relative");
    }
    @track isShowToast = false;
    @track toastType = '';
    @track title = '';
    @track message = '';
    @track toastIcon = 'utility:error';
    toastManager(toastType, title, message) {
        this.toastType = "slds-notify slds-notify_toast ";
        if (toastType == 'error') {
            this.toastType += 'slds-theme_error';
        } else if (toastType == 'success') {
            this.toastType += 'slds-theme_success';
        } else if (toastType == 'info') {
            this.toastType += 'slds-theme_info';
            this.toastIcon = 'utility:info';
        }
        this.title = title;
        this.message = message;
        this.isShowToast = true;
        setTimeout(function() {
            this.isShowToast = false;
        }.bind(this), 10000);
    }
    closeToast(event) {
        this.isShowToast = false;
        return false;
    }

    @api
    setIsUpdate(isUpdate) {
        this.isUpdate = isUpdate;

    }
    vendorCancel() {
        console.log('vendor refund >>>>');
        this.dispatchEvent(new CustomEvent('closerefundcancel'));
    }

}