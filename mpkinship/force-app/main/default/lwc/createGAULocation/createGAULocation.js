import { LightningElement, api, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
//import Name from '@salesforce/schema/npsp__Allocation__c.Name';
//import Amount from '@salesforce/schema/npsp__Allocation__c.npsp__Amount__c';
//import General_Accounting_Unit from '@salesforce/schema/npsp__Allocation__c.npsp__General_Accounting_Unit__c';
//import LASER_Cost_Code from '@salesforce/schema/npsp__Allocation__c.LASER_Cost_Code__c';
//import Payment from '@salesforce/schema/npsp__Allocation__c.npsp__Payment__c';
//import Percent from '@salesforce/schema/npsp__Allocation__c.npsp__Percent__c';
//import Opportunity from '@salesforce/schema/npsp__Allocation__c.npsp__Opportunity__c';
//import Recurring_Donation from '@salesforce/schema/npsp__Allocation__c.npsp__Recurring_Donation__c';

import getGAULocation from "@salesforce/apexContinuation/GAULocationController.getGAULocation";

const columns = [
    { label: 'Name', fieldName: 'Name' },
    { label: 'General Accounting Unit', fieldName: 'GUA_Name__c' },
   // { label: 'Amount', fieldName: 'npsp__Amount__c' },
   // { label: 'Percent', fieldName: 'npsp__Percent__c' },
];


export default class CreateGAULocation extends LightningElement {
    @api recordId;
    @api receiptid;
    @api opportunityid;
    @api objectApiName;

    @track sfdcBaseURL = window.location.origin;
    @track isShowToast = false;
    @track toastType = '';
    @track title = '';
    @track message = '';
    @track toastIcon = 'utility:error';
    @track GAULocation = [];
    locationColumns = columns;


    //manageURL = this.sfdcBaseURL + '/apex/npsp__ALLO_ManageAllocations?id=' + this.receiptid;
    fields = [General_Accounting_Unit, Amount, Percent, ];
    toastManager(toastType, title, message) {
        this.toastType = "slds-notify slds-notify_toast ";
        if (toastType == 'error') {
            this.toastType += 'slds-theme_error';
        } else if (toastType == 'success') {
            this.toastType += 'slds-theme_success';
        } else if (toastType == 'info') {
            this.toastType += 'slds-theme_info';
            this.toastIcon = 'utility:info';
        }
        this.title = title;
        this.message = message;
        this.isShowToast = true;
        setTimeout(function() {
            this.isShowToast = false;
        }.bind(this), 10000);
    }
    closeToast(event) {
        this.isShowToast = false;
        return false;
    }
    @api
    setopportunityid(opportunityid) {
        this.opportunityid = opportunityid;
    }
    @api
    setreceiptid(receiptid) {
        this.receiptid = receiptid;
    }
    errorHandler(e) {
        console.log('s');
        this.template.querySelector(".slds-notify_container.slds-is-relative");
    }
    handleSubmit(event) {
        const fields = event.detail.fields;
        //fields.npsp__Payment__c = this.receiptid;
        fields.npsp__Opportunity__c = this.opportunityid;
        this.template.querySelector('lightning-record-form').submit(fields);
    }
    allocationCancel() {
        //this.dispatchEvent(new CustomEvent('closeallocation'));
        this.showEdit = false;
    }

    handleSuccess(event) {
        const fields = event.detail.fields;
        this.recordId = event.detail.id;
        console.log(this.recordId, ' this.recordId ???');
        this.showEdit = false;
        this.getGAULocation();
        setTimeout(function() {


        }.bind(this), 1000);

    }
    @track manageShow = false;
    handleManage(event) {
        this.manageShow = true;
        console.log(this.receiptid, ' this.receiptid????');
        console.log(this.sfdcBaseURL, ' this.sfdcBaseURL????');
        //this.manageURL = this.sfdcBaseURL + '/apex/npsp__ALLO_ManageAllocations?id=' + this.receiptid;
        console.log(this.manageURL, ' this.manageURL????');
    }
    handleManageClose(event) {
        this.manageShow = false;

    }
    handleCancel(event) {
        this.showEdit = false;
    }
    @track showEdit = false;
    handleShowEdit() {
        this.showEdit = true;
    }
    @api
    getGAULocation() {
        getGAULocation({ RecpID: this.opportunityid }).then((result) => {
                if (result) {
                    this.GAULocation = result;
                } else {
                    this.GAULocation = [];
                }
            })
            .catch((error) => {
                //console.log(error);
            });
    }

}