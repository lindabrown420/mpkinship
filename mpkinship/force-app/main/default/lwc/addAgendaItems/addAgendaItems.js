import { LightningElement, api, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import CSA_Meeting_Agenda__c from '@salesforce/schema/CSA_Meeting_Agenda_Item__c.CSA_Meeting_Agenda__c';
import Review from '@salesforce/schema/CSA_Meeting_Agenda_Item__c.Review__c';
import Time from '@salesforce/schema/CSA_Meeting_Agenda_Item__c.Time3__c';
import Case from '@salesforce/schema/CSA_Meeting_Agenda_Item__c.Case_KNP__c';
import Agenda_Comments from '@salesforce/schema/CSA_Meeting_Agenda_Item__c.Agenda_Comments__c';
import IFSP from '@salesforce/schema/CSA_Meeting_Agenda_Item__c.IFSP__c';
export default class AddAgendaItems extends LightningElement {
    @api recordId;
    @api agentId;
    @api objectApiName;
    @track isShowToast = false;
    @track toastType = '';
    @track title = '';
    @track message = '';
    @track toastIcon = 'utility:error';
    @track selectedItem;
    @api isUpdate;
    fields = [Review, Case, Time, Agenda_Comments];

    toastManager(toastType, title, message) {
        this.toastType = "slds-notify slds-notify_toast ";
        if (toastType == 'error') {
            this.toastType += 'slds-theme_error';
        } else if (toastType == 'success') {
            this.toastType += 'slds-theme_success';
        } else if (toastType == 'info') {
            this.toastType += 'slds-theme_info';
            this.toastIcon = 'utility:info';
        }
        this.title = title;
        this.message = message;
        this.isShowToast = true;
        setTimeout(function() {
            this.isShowToast = false;
        }.bind(this), 10000);
    }
    closeToast(event) {
        this.isShowToast = false;
        return false;
    }
    @api
    setIsUpdate(isUpdate) {
        this.isUpdate = isUpdate;
    }
    @api
    setAgentid(agentid) {
        this.agentId = agentid;
    }
    errorHandler(e) {
        console.log('s');
        this.template.querySelector(".slds-notify_container.slds-is-relative");
    }
    handleSubmit(event) {
        console.log(this.agentId + ' Agent id');
        const fields = event.detail.fields;
        fields.CSA_Meeting_Agenda__c = this.agentId;
        console.log(fields, ' fields');
        this.template.querySelector('lightning-record-form').submit(fields);
        setTimeout(function() {
            //this.closeToast();
        }.bind(this), 2000);
    }
    agendaItemCancel() {
        this.dispatchEvent(new CustomEvent('closeaddagenda'));
    }
    handleSuccess(event) {

        this.selectedItem = event.detail.id;
        this.dispatchEvent(new CustomEvent('closeaddagenda'));
    }
}