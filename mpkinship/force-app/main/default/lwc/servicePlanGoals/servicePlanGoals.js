import { LightningElement,api } from 'lwc';

import getGoalRecords from '@salesforce/apex/ServicePlanGoals.getGoalRecords';
export default class ServicePlanGoals extends LightningElement {
    @api recordId;
    //caseId;
    goalRecords = [];
    newGoalCheck = false;
    @api isDisabled = false;

    connectedCallback(){
        console.log('connectedCallback servicePlanGoal');
        this.fetchRecords();
        
    }
    
    fetchRecords(){
        console.log('fetchRecords'); 
        this.newGoalCheck = false; 
        //this.goalRecords = [];      
        getGoalRecords({
            recId: this.recordId
        })
        .then(result => {  
            this.goalRecords = result;
            console.log(this.goalRecords);            
        })
        .catch(error => {
           console.log(error);
        });
    }

    newGoalClicked(){
        console.log('newGoalClicked');
        if(this.newGoalCheck == true){
            this.newGoalCheck = false;
        }
        else{
            this.newGoalCheck = true;
        }
        //this.fetchRecords();
    }

}