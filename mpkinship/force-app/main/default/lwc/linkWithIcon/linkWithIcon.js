import { LightningElement,api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

export default class LinkWithIcon extends NavigationMixin (LightningElement) {
    @api linkData;
    handleNavigate(){
        
        if(this.linkData.type=='Navigation'){
            if(this.linkData.navigationType == 'standard__objectPage'){
                if(this.linkData.recordTypeId){
                    this[NavigationMixin.Navigate]({
                        type: this.linkData.navigationType,
                        attributes: {
                            objectApiName: this.linkData.objectApiName,
                            actionName: this.linkData.actionName,
                        },
                        state: {
                            recordTypeId: this.linkData.recordTypeId
                        }
                    });
                }
                else if(this.linkData.filterName){
                    this[NavigationMixin.Navigate]({
                        type: this.linkData.navigationType,
                        attributes: {
                            objectApiName: this.linkData.objectApiName,
                            actionName: this.linkData.actionName,
                        },
                        state: {
                            filterName: this.linkData.filterName
                        }
                    });
                }
                else{
                    this[NavigationMixin.Navigate]({
                        type: this.linkData.navigationType,
                        attributes: {
                            objectApiName: this.linkData.objectApiName,
                            actionName: this.linkData.actionName,
                        },
                    });
                }
            }
            else if(this.linkData.navigationType == 'standard__recordPage'){
                this[NavigationMixin.Navigate]({
                    type: this.linkData.navigationType,
                    attributes: {
                        recordId:this.linkData.recordId,
                        objectApiName: this.linkData.objectApiName,
                        actionName: this.linkData.actionName,
                    }
                });
            }
            else if(this.linkData.navigationType == 'standard__recordRelationshipPage'){
                this[NavigationMixin.Navigate]({
                    type: this.linkData.navigationType,
                    attributes: {
                        recordId:this.linkData.recordId,
                        objectApiName: this.linkData.objectApiName,
                        actionName: this.linkData.actionName,
                        relationshipApiName:this.linkData.relationshipApiName,
                    },
                });
            }
            else if(this.linkData.navigationType == 'standard__navItemPage'){
                this[NavigationMixin.Navigate]({
                    type: this.linkData.navigationType,
                    attributes: {
                        apiName:this.linkData.apiName
                    },
                });
            }
            else if(this.linkData.navigationType == 'standard__webPage'){
                this[NavigationMixin.Navigate]({
                    type: this.linkData.navigationType,
                    attributes: {
                        url:this.linkData.url
                    },
                });
            }
        }
        else if(this.linkData.type=='URL'){
            window.open(this.linkData.url);
        }
    }
}