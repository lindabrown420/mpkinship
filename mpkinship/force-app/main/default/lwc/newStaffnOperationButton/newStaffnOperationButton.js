import { LightningElement } from 'lwc';

export default class NewStaffnOperationButton extends LightningElement {
    openNew(event){
        window.location = '/lightning/n/New_Invoice';
    }
}