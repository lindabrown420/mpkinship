import { LightningElement, track } from 'lwc';
import GetSectionDetail from '@salesforce/apexContinuation/UISettingController.getSectionDetail';
export default class HomeDashboard extends LightningElement {
  @track sectionContainerData;
  @track columnSize;
    connectedCallback(){
        GetSectionDetail({})
        .then((result) => {
            result.forEach(function(key) {
                if(key.size =='1'){
                    var size = '3';
                    key.columnSize = 'slds-col slds-size_'+size+'-of-3';
                }
                else{
                    var size = '1';
                    key.columnSize = 'slds-col slds-size_'+size+'-of-3';
                }
            
            });
            this.sectionContainerData = result;
        })
        .catch((error) =>{

        });
        /*this.sectionContainerData1s = [
        {
            Id : 1,
            heading : 'Create a',
            link : [
                {
                    isEmpty: false,
                    iconName: 'standard:opportunity',
                    text:'Purchase Order',
                    isIcon: true,
                    type:'Navigation',
                    navigationType:'standard__objectPage',
                    objectApiName:'Opportunity',
                    actionName: 'new',
                    recordTypeId:'012180000009L3RAAU',
                    url:'',
                    apiName:'',
                    recordId:'',
                    relationshipApiName:''
                },
                {
                    isEmpty: false,
                    iconName: 'standard:case',
                    text:'Case Action',
                    isIcon: true,
                    type:'Navigation',
                    navigationType:'standard__objectPage',
                    objectApiName:'Opportunity',
                    actionName: 'new',
                    recordTypeId:'012180000009KeXAAU'
                },
                {
                    isEmpty: true,
                    iconName: '',
                    text:'',
                    isIcon: false,
                    type:'',
                    navigationType:'',
                    objectApiName:'',
                    actionName: '',
                    recordTypeId:'',
                    url:''
                },
                {
                    isEmpty: true,
                    iconName: '',
                    text:'',
                    isIcon: false,
                    type:'',
                    navigationType:'',
                    objectApiName:'',
                    actionName: '',
                    recordTypeId:'',
                    url:''
                },
                {
                    isEmpty: true,
                    iconName: '',
                    text:'',
                    isIcon: false,
                    type:'',
                    navigationType:'',
                    objectApiName:'',
                    actionName: '',
                    recordTypeId:'',
                    url:''
                }
            ], 
        },
        {
            Id : 2,
            heading : 'Navigate to',
            link : [
                {
                    iconName: 'standard:account',
                    text:'Vendor',
                    isIcon: true,
                    type:'Navigation',
                    navigationType:'standard__objectPage',
                    objectApiName:'Account',
                    actionName: 'home',
                    recordTypeId:'',
                    url:''
                },
                {
                    iconName: 'standard:case',
                    text:'Case',
                    isIcon: true,
                    type:'Navigation',
                    navigationType:'standard__objectPage',
                    objectApiName:'Case',
                    actionName: 'home',
                    recordTypeId:'',
                    url:''
                },
                {
                    iconName: 'standard:contact',
                    text:'Purchase Order',
                    isIcon: true,
                    type:'Navigation',
                    navigationType:'standard__objectPage',
                    objectApiName:'Opportunity',
                    actionName: 'home',
                    recordTypeId:'',
                    url:''
                },
                {
                    iconName: 'standard:avatar',
                    text:'Employees',
                    isIcon: true,
                    type:'Navigation',
                    navigationType:'standard__objectPage',
                    objectApiName:'Employee__c',
                    actionName: 'home',
                    recordTypeId:'',
                    url:''
                },
                {
                    isEmpty: true,
                    iconName: '',
                    text:'',
                    isIcon: false,
                    type:'',
                    navigationType:'',
                    objectApiName:'',
                    actionName: '',
                    recordTypeId:'',
                    url:''
                },
            ],
        },
        {
            Id : 3,
            heading : 'Month End Close',
            link : [
                {
                    isEmpty: false,
                    iconName: 'standard:account',
                    text:'LASER',
                    isIcon: true,
                    type:'url',
                    url:'/lightning/n/LASER_Month_End_Close',
                    navigationType:'',
                    objectApiName:'',
                    actionName: '',
                    recordTypeId:'',

                },
                {
                    isEmpty: false,
                    iconName: 'standard:lead',
                    text:'LEDRS',
                    isIcon: true,
                    type:'url',
                    url:'/lightning/n/LEDRS_Month_End_Close',
                    navigationType:'',
                    objectApiName:'',
                    actionName: '',
                    recordTypeId:'',
                },
                {
                    isEmpty: false,
                    iconName: 'standard:report',
                    text:'Reports',
                    isIcon: true,
                    type:'Navigation',
                    navigationType:'standard__objectPage',
                    objectApiName:'Report',
                    actionName: 'home',
                    recordTypeId:'',
                    url:''
                },
                {
                    isEmpty: true,
                    iconName: '',
                    text:'', 
                    isIcon: false,
                    type:'',
                    navigationType:'',
                    objectApiName:'',
                    actionName: '',
                    recordTypeId:'',
                    url:''
                },
                {
                    isEmpty: true,
                    iconName: '',
                    text:'', 
                    isIcon: false,
                    type:'',
                    navigationType:'',
                    objectApiName:'',
                    actionName: '',
                    recordTypeId:'',
                    url:''
                },
            ], 
        },
        {
            Id : 4,
            heading : 'Checks',
            link : [
                {
                    isEmpty: false,
                    iconName: 'standard:approval',
                    text:'Start Check Run',
                    isIcon: true,
                    type:'url',
                    url:'/lightning/n/Pay_Invoices',
                    navigationType:'',
                    objectApiName:'',
                    actionName: '',
                    recordTypeId:'',
                },
                {
                    isEmpty: false,
                    iconName: 'standard:case',
                    text:'Cancel and Reissue',
                    isIcon: true,
                    type:'url',
                    url:'/lightning/n/Post_Check_List',
                    navigationType:'',
                    objectApiName:'',
                    actionName: '',
                    recordTypeId:'',
                },
                {
                    isEmpty: false,
                    iconName: 'standard:apps',
                    text:'Assign Check Numbers',
                    isIcon: true,
                    type:'Navigation',
                    navigationType:'standard__objectPage',
                    objectApiName:'Kinship_Check__c',
                    actionName: 'list',
                    recordTypeId:'',
                    url:''
                },
                {
                    isEmpty: false,
                    iconName: 'standard:account',
                    text:'LASER',
                    isIcon: true,
                    type:'url',
                    url:'/lightning/n/LASER_Month_End_Close',
                    navigationType:'',
                    objectApiName:'',
                    actionName: '',
                    recordTypeId:'',
                },
                {
                    isEmpty: true,
                    iconName: '',
                    text:'', 
                    isIcon: false,
                    type:'',
                    navigationType:'',
                    objectApiName:'',
                    actionName: '',
                    recordTypeId:'',
                },
            ],
        },
        {
            Id : 5,
            heading : 'Receivables',
            link : [
                {
                    isEmpty: false,
                    iconName: 'standard:note',
                    text:'Claims',
                    isIcon: true,
                    type:'',
                    navigationType:'',
                    objectApiName:'',
                    actionName: '',
                    recordTypeId:'',
                    url:''
                },
                {
                    isEmpty: false,
                    iconName: 'standard:customers',
                    text:'Report of Collections',
                    isIcon: true,
                    type:'',
                    navigationType:'',
                    objectApiName:'',
                    actionName: '',
                    recordTypeId:'',
                    url:''
                },
                {
                    iconName: 'standard:report',
                    text:'Reports',
                    isIcon: true,
                    type:'Navigation',
                    navigationType:'standard__objectPage',
                    objectApiName:'Report',
                    actionName: 'home',
                    isEmpty: false,
                    recordTypeId:'',
                    url:''
                },
                {
                    isEmpty: true,
                    iconName: '',
                    text:'', 
                    isIcon: false,
                    type:'',
                    navigationType:'',
                    objectApiName:'',
                    actionName: '',
                    recordTypeId:'',
                    url:''
                },
                {
                    isEmpty: true,
                    iconName: '',
                    text:'', 
                    isIcon: false,
                    type:'',
                    navigationType:'',
                    objectApiName:'',
                    actionName: '',
                    recordTypeId:'',
                    url:''
                },
            ], 
        },
        {
            Id : 6,
            heading : 'Staff & Operations',
            link : [
                {
                    isEmpty: false,
                    iconName: 'standard:avatar',
                    text:'Administrative Payments',
                    isIcon: true,
                    type:'',
                    navigationType:'',
                    objectApiName:'',
                    actionName: '',
                    recordTypeId:'',
                    url:''
                },
                {
                    isEmpty: true,
                    iconName: '',
                    text:'', 
                    isIcon: false,
                    type:'',
                    navigationType:'',
                    objectApiName:'',
                    actionName: '',
                    recordTypeId:'',
                    url:''
                },
                {
                    isEmpty: true,
                    iconName: '',
                    text:'', 
                    isIcon: false,
                    type:'',
                    navigationType:'',
                    objectApiName:'',
                    actionName: '',
                    recordTypeId:'',
                    url:''
                },
                {
                    isEmpty: true,
                    iconName: '',
                    text:'', 
                    isIcon: false,
                    type:'',
                    navigationType:'',
                    objectApiName:'',
                    actionName: '',
                    recordTypeId:'',
                    url:''
                },
                {
                    isEmpty: true,
                    iconName: '',
                    text:'', 
                    isIcon: false,
                    type:'',
                    navigationType:'',
                    objectApiName:'',
                    actionName: '',
                    recordTypeId:'',
                    url:''
                },
            ],
        },
        {
            Id : 7,
            heading : 'Budgets',
            link : [
                {
                    isEmpty: false,
                    iconName: 'standard:all',
                    text:'Budget Transfer',
                    isIcon: true,
                    type:'Navigation',
                    navigationType:'standard__objectPage',
                    objectApiName:'Budget_Transactions__c',
                    actionName: 'list',
                    recordTypeId:'',
                    url:''
                    
                },
                {
                    isEmpty: false,
                    iconName: 'standard:article',
                    text:'Locality Budget',
                    isIcon: true,
                    type:'Navigation',
                    navigationType:'standard__objectPage',
                    objectApiName:'Budget_Transactions__c',
                    actionName: 'list',
                    recordTypeId:'',
                    url:''
                    
                },
                {
                    isEmpty: false,
                    iconName: 'standard:assignment',
                    text:'State Budget',
                    isIcon: true,
                    type:'Navigation',
                    navigationType:'standard__objectPage',
                    objectApiName:'Budget_Transactions__c',
                    actionName: 'list',
                    recordTypeId:'',
                    url:''
                    
                },
                {
                    isEmpty: false,
                    iconName: 'standard:snippet',
                    text:'CSA Budget',
                    isIcon: true,
                    type:'Navigation',
                    navigationType:'standard__objectPage',
                    objectApiName:'Budget_Transactions__c',
                    actionName: 'list',
                    recordTypeId:'',
                    url:''
                    
                },
                {
                    isEmpty: true,
                    iconName: '',
                    text:'', 
                    isIcon: false,
                    type:'',
                    navigationType:'',
                    objectApiName:'',
                    actionName: '',
                    recordTypeId:'',
                    url:''
                }
            ], 
        },
        {
            Id : 8,
            heading : 'Accounting',
            link : [
                {
                    isEmpty: false,
                    iconName: 'standard:opportunity',
                    text:'Adjustments',
                    isIcon: true,
                    type:'',
                    navigationType:'',
                    objectApiName:'',
                    actionName: '',
                    recordTypeId:'',
                    url:''
                },
                {
                    isEmpty: false,
                    iconName: 'standard:opportunity',
                    text:'Non-Reimbursable Transactions',
                    isIcon: true,
                    type:'',
                    navigationType:'',
                    objectApiName:'',
                    actionName: '',
                    recordTypeId:'',
                    url:''
                },
                {
                    isEmpty: false,
                    iconName: 'standard:opportunity',
                    text:'Transaction Journal',
                    isIcon: true,
                    type:'Navigation',
                    navigationType:'standard__objectPage',
                    objectApiName:'Transaction_Journal__c',
                    actionName: 'list',
                    recordTypeId:'',
                    url:''
                    
                },
                {
                    isEmpty: false,
                    iconName: 'standard:opportunity',
                    text:'General Ledger',
                    isIcon: true,
                    type:'',
                    navigationType:'',
                    objectApiName:'',
                    actionName: '',
                    recordTypeId:'',
                    url:''
                },
                {
                    isEmpty: false,
                    iconName: 'standard:opportunity',
                    text:'Reports',
                    isIcon: true,
                    type:'',
                    navigationType:'',
                    objectApiName:'',
                    actionName: '',
                    recordTypeId:'',
                    url:''
                }
                
            ], 
        },
    ];*/
  }

}