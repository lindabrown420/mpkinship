import { LightningElement, api, track, wire } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

import Report_Number from '@salesforce/schema/Payment__c.Report_Number__c';
import Date_of_Receipt from '@salesforce/schema/Payment__c.Payment_Date__c';
import Case from '@salesforce/schema/Payment__c.Kinship_Case__c';
import Description from '@salesforce/schema/Payment__c.Description__c';
import RecordTypeId from '@salesforce/schema/Payment__c.RecordTypeId';
import Receipt_Type from '@salesforce/schema/Payment__c.Receipt_Type__c';
//import Pay_Method from '@salesforce/schema/npe01__OppPayment__c.Pay_Method__c';
import Pay_Method from '@salesforce/schema/Payment__c.Payment_Method__c';
import Amount from '@salesforce/schema/Payment__c.Payment_Amount__c';
import adjustAmount from '@salesforce/schema/Payment__c.Adjustment_Amount__c';
import Category from '@salesforce/schema/Payment__c.Category_lookup__c';
import Claims from '@salesforce/schema/Payment__c.Claims__c';
import Contact from '@salesforce/schema/Payment__c.Contact__c';
import Bank_Statement from '@salesforce/schema/Payment__c.Bank_Statement__c';
import Fund from '@salesforce/schema/Payment__c.Fund__c';
import Accounting_Period from '@salesforce/schema/Payment__c.Accounting_Period__c';
import Vendor from '@salesforce/schema/Payment__c.Vendor__c';
import CASE_NAME from '@salesforce/schema/Payment__c.Case_Name__c'; 
import Special_Welfare_Account from '@salesforce/schema/Payment__c.Special_Welfare_Account__c';
import KinshipCheck from '@salesforce/schema/Payment__c.Kinship_Check__c';
import GetSWCategory from "@salesforce/apexContinuation/ROCController.GetSWCategory";
import getReceiptRecord from "@salesforce/apexContinuation/ROCController.getReceiptRecord";
import Check_Number from '@salesforce/schema/Payment__c.Check_Reference_Number__c';
import { updateRecord } from 'lightning/uiRecordApi';
import { hideSpinner, showSpinner } from 'c/util';
import Report_of_Collections from '@salesforce/schema/Payment__c.Opportunity__c'
import isReceiptNumberEsixt from "@salesforce/apexContinuation/ROCController.isReceiptNumberEsixt";
import getCategoryType from "@salesforce/apexContinuation/ROCController.getCategoryType";
import updateFipsCodeinInvoices from "@salesforce/apexContinuation/ROCController.updateFipsCodeinInvoices";
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import payment from '@salesforce/schema/Payment__c';
import Pay_stage from '@salesforce/schema/Payment__c.Invoice_Stage__c';
import getCostCenter from "@salesforce/apexContinuation/ROCController.getCostCenter";
import checkPayable from "@salesforce/apexContinuation/ROCController.checkPayable";
import getClaim from "@salesforce/apexContinuation/ROCController.getClaim";
import getCaseId from "@salesforce/apexContinuation/ROCController.getCaseId";
import getClaimType from "@salesforce/apexContinuation/ROCController.getClaimType";


import { ShowToastEvent } from 'lightning/platformShowToastEvent';
export default class AddReceipt extends NavigationMixin (LightningElement){
    // The record page provides recordId and objectApiName
    @api receiptNumber;
    @api recordId;
    @api objectApiName;
    @track showLoader = false;
    @track isView = false;
    @api isUpdate;
    @api reportCollectionId;
    @api selectedReceipt;
    @api childFy;
    @api rcordtype;
    @api rcordname;
    @api receipttype;
    @api closedate;
    @api balanceamount;
    @api beginningreceipt;
    @api enddate;
    @api endingreceipt;
    @api selectedrecipient;
    @api selectedpayer;
    @api catrecname;
    @api catname;
    @api selectedPaymentAmount;
    @api selectedPaymentMethod;
    @api campaignId;
    @api campaignName;
    @api parentId;
    @track equal = false;
    @track addAmount = false;
    @track subtractAmount = false;
    @track disableFiscalYear = false;
    @track disableCostCenter = false;
    @track parentFiscalYear = '';
    @track SWAcategoryId;
    @track receiptAmount;
    @track SWAcategoryName = null;
    @track Categorydisable = false;
    @track fundValue = '';
    @api fipsCode;
    @api catName = '';
    @api catId='';
    @api costId = '';
    @api costCenterName = '';
    @api payStage = 'Pending';
    createVendorShow = true;
    createContactShow = true;
    createCaseShow = true;
    createSWAShow = true;
    createClaimShow = true;
    value = 'Case'; 
    valueTranscationCode = '';	
    reportNumberValue= '';
    showCase = true;
    showClaim = false;
    showSpecialWelfareAccount = false;
    naCatType = false;

    recordTypeIdStr
    fields = [Report_Number, Receipt_Type, Case, Date_of_Receipt, Vendor, Amount,adjustAmount, Contact, Claims, Pay_Method, Category,
        Fund, Accounting_Period, Description, CASE_NAME, Special_Welfare_Account,KinshipCheck,Check_Number
    ];

    
    get options() {
        return [
            //{ label: 'Vendor', value: 'Vendor' },
            { label: 'Case', value: 'Case' },
            //{ label: 'Contact', value: 'Contact' },
            { label: 'Claim', value: 'Claim' },
            { label: 'Special Welfare Account', value: 'SpecialWelfareAccount' },
            { label: 'Not Applicable', value: 'Not Applicable' },
        ];
    }
    get payeroptions() {	
        return [	
            { label: 'Vendor', value: 'Vendor' },	
            //{ label: 'Case', value: 'Case' },	
            { label: 'Contact', value: 'Contact' }	
        ];	
    }

    get optionFund() {
        return [
            { label: 'Reimbursable', value: 'Reimbursable' },
            { label: 'Non-Reimbursable', value: 'Non-Reimbursable' },
        ];
    }

    dateChange(event)
    {
        this.closedate = event.target.value;
    }
    GetSWCategory(){
        console.log( 'GET SWA Category Id ????');
        GetSWCategory().then((result) => {
            if (result) {
                console.log('result ', result);
                this.SWAcategoryId = result.Id;
                this.SWAcategoryName = result.Name;
                this.receiptTypeValue = result.Category_Type__c;
                this.disableFiscalYear = true;
                let newsObject = [{ 'recId' : this.SWAcategoryId ,'recName' : this.SWAcategoryName}];
                this.parentFisaclYearName = JSON.stringify(newsObject);
                setTimeout(function () {
                    this.template.querySelector("c-sat-multi-select-lookup").setData();
                }.bind(this), 1000);
                this.getCostCenters(this.SWAcategoryId); 
            }
        })
        .catch((error) => {
            console.log(error);
        });
    }
    getReceiptRecord(){
        console.log( 'GET SWA Category Id ????', this.selectedReceipt);
        console.log( 'GET SWA Category Id ????', this.reportNumberValue);
        getReceiptRecord({RecieptID: this.selectedReceipt}).then((result) => {
            if (result) {
                console.log('result ', result);
                this.SWAcategoryId = result.Category_lookup__c;
                console.log('CategoryLookup????????????????????',this.SWAcategoryId)
                if(result.Claims__c)
                this.claimsValue = result.Claims__c;
                this.createClaimShow = false;
                if(result.Special_Welfare_Account__c)
                this.specialWelfareAccountValue = result.Special_Welfare_Account__c;
                this.createSWAShow = false;
                if(result.Payment_Source__c){
                    this.valueTranscationCode = result.Payment_Source__c;
                    this.paymentSourceValue = result.Payment_Source__c;
                }
                if(result.Report_Number__c){
                    this.reportNumberValue = result.Report_Number__c;
                }
                
            }
            console.log( 'GET SWA Category Id ????', this.reportNumberValue);
            console.log('CategoryLookup????????????????????',this.SWAcategoryId)
        })
        .catch((error) => {
            console.log(error);
        });
    }
    
    handleLoadForm(event){
        const fields = event.detail.fields;
        //this.value = fields.Recipient__c;
    }
    choseCase=true;
    perviousReceipient = '';
    handleRecipientChange(event) {
        if(this.value != null && this.value != ''){
            this.perviousReceipient = this.value;
        }
        this.value = event.detail.value;
        if(this.value != 'Not Applicable'){
            this.naCatType = false;
            this.isCatLaser = false;
            this.SWAcategoryId = null;
            this.SWAcategoryName = null;
            this.disableFiscalYear = false;
            this.parentFisaclYearName = '';
        
            setTimeout(function () {
                this.template.querySelector("c-sat-multi-select-lookup").setData();
            }.bind(this), 1000);
            this.showCostLookup = false;
            this.parentCostCenterName = '';
            this.showRefundSource = false;
            if(this.value =='Claim'){
                this.catWhereStr = "  AND Inactive__c = false AND Category_Type__c = 'Claims Payable'";
                this.SWAcategoryId = null;
                console.log('In IF Claim');
                //this.makeDisable();
            }
            else{
                console.log('this.value AFTER??????????', this.value);
                this.catWhereStr = "  AND Inactive__c = false";
            }
            if(this.choseCase){
                console.log('In Chosecase');
                this.setFieldVisibility()
            }
            else{
                if(this.value =='Case'){
                    this.toastManager('error', 'Error', 'You cannot select Case as recipient with this Category.');
                    // setTimeout(function(){
                    //     this.value='Claim';
                    // }.bind(this),100);
                }
                else{
                    this.setFieldVisibility();
                }
            }
        }
        else{
            if(this.naCatType){
                this.toastManager('warning', 'Warning', "You cannot select 'Not Applicable' as recipient with this category.");
                setTimeout(function(){
                    this.value = this.perviousReceipient;
                }.bind(this),100);
                
            }
            else{
                this.choseCase = true;
                this.isCatLaser = false;
                this.SWAcategoryId = null;
                this.SWAcategoryName = null;
                this.disableFiscalYear = false;
                this.parentFisaclYearName = '';
                this.showCase = false;
                this.showSpecialWelfareAccount = false;
                this.showClaim = false;
                this.catWhereStr = "  AND Inactive__c = false AND (Category_Type__c <>'CSA' AND Category_Type__c <> 'IVE')";
                setTimeout(function () {
                    this.template.querySelector("c-sat-multi-select-lookup").setData();
                }.bind(this), 1000);
                this.showCostLookup = false;
                this.parentCostCenterName = '';
                this.showRefundSource = false;
            }
            
        }
        
    }
    @track vendorPayer = true;	
    @track casePayer = false;	
    @track contactPayer = false;
    @track payerValue='Vendor';	
    handlepayerChange(event) {	
        this.payerValue = event.detail.value;	
        if(this.payerValue == 'Vendor'){	
            this.vendorPayer = true;	
            this.casePayer = false;	
            this.contactPayer = false;	
        }	
        else if(this.payerValue == 'Case'){	
            this.vendorPayer = false;	
            this.casePayer = true;	
            this.contactPayer = false;	
        }	
        else if(this.payerValue == 'Contact'){	
            this.vendorPayer = false;	
            this.casePayer = false;	
            this.contactPayer = true;	
        }
    }
    setFieldVisibility(){
        this.Categorydisable = true;

        if(this.SWAcategoryId != null || this.SWAcategoryId != '')
        {
            if(this.update == false){
                this.SWAcategoryId = null;
                this.SWAcategoryName = null;
                this.disableFiscalYear = false;
                this.parentFisaclYearName = '';
                this.choseCase = true;
                setTimeout(function () {
                    this.template.querySelector("c-sat-multi-select-lookup").setData();
                }.bind(this), 1000);
                this.showCostLookup = false;
                this.parentCostCenterName = '';
                this.showRefundSource = false;
            }
            
        }
        
            this.claimRecordId = null;
            this.claimsValue = '';
            this.payableToCheck = false;
        

        // if(this.SWAcategoryName != null){
        //     console.log('I am in SWAcategoryName',this.showSpecialWelfareAccount);
        //     this.SWAcategoryId = null;
        //     this.SWAcategoryName = null;
        //     this.disableFiscalYear = false;
          
        //     this.parentFisaclYearName = '';
        //     setTimeout(function () {
        //         this.template.querySelector("c-sat-multi-select-lookup").setData();
        //     }.bind(this), 1000);
        //     this.showCostLookup = false;
        //     this.parentCostCenterName = '';
        // }
        if(this.value =='Case'){
            this.Categorydisable=false;
            this.showSpecialWelfareAccount = false;
            this.createCaseShow = true;
            this.showCase = true;
            this.showClaim = false;
            this.showClaimBalance = false;
            this.showSpecialWelfareAccount = false;
            this.specialWelfareAccountValue = '';
            this.createSWAShow = true;
            this.showSWRBalance = false;
            this.claimsValue = '';
            this.createClaimShow = true;
            if(!this.showRefundSourc){
                this.showRefundSource = false;
                this.showOtherTranscationText = false;
            }
            console.log('I am in case',this.showSpecialWelfareAccount);
        }
        else if(this.value =='Claim'){
            this.catWhereStr = "  AND Inactive__c = false AND Category_Type__c = 'Claims Payable'";
            this.value ='Claim'
            console.log('Category Type??????', this.Category_Type__c);
            if(!this.firstLoad){
                this.caseValue = '';
            }
            this.firstLoad = false;
            this.showCase = false;
            this.createCaseShow = true;

            this.showClaim = true;
            
            this.specialWelfareAccountValue = '';
            this.showSpecialWelfareAccount = false;
            this.createSWAShow = true;
            this.showSWRBalance = false;
            if(this.isUpdate)
            this.showClaimBalance = true;
            if(!this.showRefundSourc){
                this.showRefundSource = false;
                this.showOtherTranscationText = false;
            }
            console.log('I am in claim',this.showSpecialWelfareAccount);
            
        }
        else if(this.value =='SpecialWelfareAccount'){
            console.log('I am in SpecialWelfareAccount');
            this.showRefundSource = true;
            this.optionsTranscationCode = [];	
            this.optionsTranscationCode = [{label: 'Child Support Collections through DCSE', value: '5' },{ label: 'Payments made on behalf of the child (ex: SSA, SSI, VA benefits, …) – Special Welfare Refunds', value: '6'  }];	
            //this.optionsTranscationCode.push({ label: 'Payments made on behalf of the child (ex: SSA, SSI, VA benefits, …) – Special Welfare Refunds', value: '6'  });	
            this.showClaimBalance = false;
            if(!this.firstLoad){
                this.caseValue = '';
            }
            this.firstLoad = false;
            this.showCase = false;
            this.createCaseShow = true;
            this.claimsValue = '';
            this.createClaimShow = true;
            this.showClaim = false;
            this.showSpecialWelfareAccount = true;
            this.showRefundSourc = false;
            // if(!this.isUpdate){
                this.GetSWCategory();
           //}
            
        }
        else if(this.value =='Not Applicable'){
                this.showCase = false;
                this.showSpecialWelfareAccount = false;
                this.showClaim = false;
                this.catWhereStr = "  AND Inactive__c = false AND (Category_Type__c <>'CSA' AND Category_Type__c <> 'IVE')";
                
        }
    }
    parentFisaclYearName;
    parentCostCenterName;
    catWhereStr;
    optionsTranscationCode = [];
    firstLoad = false;
  
    connectedCallback() {
        console.log('this.balanceamount>>>>>>>>>>>>', this.balanceamount);
        this.receiptAmount = this.balanceamount;
        this.payMethod = this.selectedPaymentMethod;
        this.catWhereStr = "  AND Inactive__c = false";
        this.value = 'Case';
        this.payerValue='Vendor';
        this.Categorydisable = false;
        this.vendorPayer = true;	
        this.casePayer = false;	
        this.contactPayer = false;
        
        this.showCase = true;
        
        this.showClaim = false;
        this.showClaimBalance = false;
        this.showSpecialWelfareAccount = false;
        this.showSWRBalance = false;
        this.showRefundSource = false;
        this.showOtherTranscationText = false;
        console.log(this.selectedReceipt + ' ???? this.selectedReceipt?????');
        console.log(Category + ' ???? Categoryloook?????');
        this.receiptTypeValue = this.catname;
        var cat = this.catrecname;
        if(cat != null && cat != ''){
            if(this.catrecname.includes('LASER')){
                this.isCatLaser = true;
                //this.isCatLaser = true;
            }
            else{
                this.isCatLaser = false;
            }
        }
        
        this.showRefundSource = false;
        this.showOtherTranscationText = false;
        this.disabledCase = false;
        if(this.selectedrecipient != null && this.selectedrecipient != ''){
            if(this.selectedrecipient.includes('Special Welfare Account')){
                this.value = 'SpecialWelfareAccount';
            }
            else{
                this.value = this.selectedrecipient;
            }
           
        }
        this.payerValue = this.selectedpayer;
        if(this.payerValue == 'Vendor'){	
            this.vendorPayer = true;	
            this.casePayer = false;	
            this.contactPayer = false;	
        }	
        else if(this.payerValue == 'Case'){	
            this.vendorPayer = false;	
            this.casePayer = true;	
            this.contactPayer = false;	
        }	
        else if(this.payerValue == 'Contact'){	
            this.vendorPayer = false;	
            this.casePayer = false;	
            this.contactPayer = true;	
        }
        else {
            this.payerValue='Vendor';
            this.vendorPayer = true;	
            this.casePayer = false;	
            this.contactPayer = false;	
        }
        this.getReceiptRecord();
        if(this.catId != '' && this.catName != ''){
            let newsObject = [{ 'recId' : this.catId ,'recName' : this.catName }];
            this.SWAcategoryId = this.catId;
            this.parentFisaclYearName = JSON.stringify(newsObject);
            console.log('CategoryLookup????????????????????catId',this.SWAcategoryId);
            getCostCenter({ categoryId: this.catId}).then((result) => {
                if (result) {
                    this.costCenterIds = [];
                    if(result.length >= 1){
                        result.forEach(ele => {
                            this.costCenterIds.push(ele.Cost_Center__c);
                        });
                    }
                }
                
            })
            .catch((error) => {
                //console.log(error);
                
            });
        }
        /*else
            {
                    this.showCostLookup = true;
            }*/
        if(this.costId != '' && this.costCenterName != ''){
            this.showCostLookup = true;
            let newsObject = [{ 'recId' : this.costId  ,'recName' : this.costCenterName }];
            this.costCernterId = this.costId ;
            this.parentCostCenterName = JSON.stringify(newsObject);
            this.disableCostCenter = true;
            
        }
        else{
            if(this.isUpdate == true){
                if(this.catId != null && this.catId != ''){
                    this.getCostCenters(this.catId);
                }
                
            } 
        }
        //if(this.rcordname != 'Vendor Refund'){
            
            if(this.receipttype =='CSA' || this.receipttype =='Special Welfare' || this.receipttype == 'IVE' ){
                this.showRefundSource = true;
                this.showRefundSourc = true;
                if(this.receipttype =='CSA'){	
                    this.optionsTranscationCode = [];	
                    this.optionsTranscationCode =[{ label: 'Parental Co-Payment', value: '4' },{ label: 'Other-Credits', value: '11' }];	
                    //this.optionsTranscationCode.push({ label: 'Other-Credits', value: '11' });
                    console.log('CategoryLookup????????????????????CSA',this.SWAcategoryId)	
                }	
                else if(this.receipttype =='Special Welfare'){	
                    this.optionsTranscationCode = [];	
                    this.optionsTranscationCode = [{label: 'Child Support Collections through DCSE', value: '5' },{ label: 'Payments made on behalf of the child (ex: SSA, SSI, VA benefits, …) – Special Welfare Refunds', value: '6'  }];	
                    //this.optionsTranscationCode.push({ label: 'Payments made on behalf of the child (ex: SSA, SSI, VA benefits, …) – Special Welfare Refunds', value: '6'  });	
                    console.log('CategoryLookup????????????????????SW',this.SWAcategoryId);
                }	
                else{	
                    this.optionsTranscationCode = [];	
                    this.optionsTranscationCode =[{ label: 'Parental Co-Payment', value: '4' },{ label: 'Other-Credits', value: '11' }];
                    //    this.optionsTranscationCode=[{ label: 'Parental Co-Payment', value: '4' },{label: 'Child Support Collections through DCSE', value: '5' },{ label: 'Payments made on behalf of the child (ex: SSA, SSI, VA benefits, …) – Special Welfare Refunds', value: '6'  },{ label: 'Other-Credits', value: '11' }];	
                    /*this.optionsTranscationCode.push({label: 'Child Support Collections through DCSE', value: '5' });	
                    this.optionsTranscationCode.push({ label: 'Payments made on behalf of the child (ex: SSA, SSI, VA benefits, …) – Special Welfare Refunds', value: '6'  });	
                    this.optionsTranscationCode.push({ label: 'Other-Credits', value: '11' });	
                */console.log('CategoryLookup????????????????????else',this.SWAcategoryId);
                    }
                    console.log('CategoryLookup????????????????????1',this.SWAcategoryId);	
            }
            else{
                this.showRefundSource = false;
                this.showRefundSourc = false;
                this.showOtherTranscationText = false;
            }
            if(this.receipttype =='Special Welfare'){
                this.value = 'SpecialWelfareAccount';
                this.showSpecialWelfareAccount = true;
                this.showCase = false;
                
                
                this.showClaim = false;
                this.showClaimBalance = false;
                //this.disabledCase = true;
                this.Kinship_Case__c = null;
                //this.disabledCase = true;
            }
            else{
                this.disabledCase = false;
            }
        //}
        if(this.isUpdate){
            this.firstLoad = true;
            if(this.parentFisaclYearName != null && this.parentFisaclYearName != '')
            this.disableFiscalYear = true;
            this.setFieldVisibility();
        }
        else{
            this.value = 'Case';
            this.payerValue='Vendor';
            this.Categorydisable = false;
            this.vendorPayer = true;	
            this.casePayer = false;	
            this.contactPayer = false;
            
            this.showCase = true;
            
            this.showClaim = false;
            this.showClaimBalance = false;
            this.showSpecialWelfareAccount = false;
            this.showSWRBalance = false;
            this.showRefundSource = false;
            this.showOtherTranscationText = false;
            this.SWAcategoryId = '';
            this.costCernterId = '';
            this.isCatLaser = false;
        }
        
        
    }
    @wire(getObjectInfo, { objectApiName: payment })
    getObjectdata({ error, data }) {
        if (data) {
            console.log('data.defaultRecordTypeId ' + data.defaultRecordTypeId);
            console.log('data.defaultRecordTypeId ', data.recordTypeInfos);
            for (var key in data.recordTypeInfos) {
                if (data.recordTypeInfos[key].name == "Receipt") {
                    this.recordTypeIdStr = data.recordTypeInfos[key].recordTypeId;
                }
            }
            // perform your custom logic here
        } else if (error) {
            // perform your logic related to error 
        }
    };
    @track paymentAmount;
    @track fields;
    @track payMethod = 'Check';
    handleChange(event) {
        this.valuePay = event.detail.value;
        this.payMethod = this.valuePay;
   //     console.log('PaymentMethod???????***', this.valuePay);
   //     if (this.valuePay == 'Cash') {
   //         this.payStage = 'Pending';
   //     }
   //     else {
   //         this.payStage = 'Pending';
   //     }
   //     console.log('payStagevalue???????', this.payStage);
   }
    
    
    
    handleSubmit(event) {
        showSpinner(this);
        //this.showLoader = true;
        event.preventDefault(); // stop the form from submitting
        const fields = event.detail.fields;
        this.fields = event.detail.fields;
        
        fields.Accounting_Period__c = this.childFy;
        fields.Opportunity__c = this.reportCollectionId; // modify a field

        fields.Invoice_Stage__c = this.payStage;
       
        console.log('Paystage??????????????????', fields.Invoice_Stage__c);
        this.balanceamount = this.receiptAmount;

        console.log('balanceamount on submit??????????????????', this.balanceamount);

        if(this.recordId == null || this.recordId == ''){
            fields.RecordTypeId = this.recordTypeIdStr; //'012180000009TyEAAU';
        }

         console.log('Receipt Number??????????????????', this.receiptNumber);

        if (this.receiptNumber == null || this.receiptNumber == '') {
            console.log('Receipt Number??????????????????', this.receiptNumber);
            this.toastManager('error', 'Error', 'Receipt Number is required.');
            event.preventDefault();
            hideSpinner(this);
            return true;
        }
        
            
        if (this.value == null || this.value == '') {
            this.toastManager('error', 'Error', 'Recipient is required.');
            event.preventDefault();
            hideSpinner(this);
            return true;
        }
        else{
            if(this.value == 'Claim'){
                this.specialWelfareAccountValue = null;
                fields.Special_Welfare_Account__c= null;
                if(this.payableToCheck){
                    this.updateClaim = true; 
                    this.claimRecordId = this.claimsValue;
                    event.preventDefault();
                    hideSpinner(this);
                    this.title = 'Claim';
                    this.objectName = 'Claims__c';
                    this.fieldName = ['Claim_Type__c','Claim_Amount__c', 'Case__c','Payable_to__c', 'Claim_Notes__c'];
                    this.showCreateNew = true;
                    this.titleOfNew = 'Create New Claims';
                    this.toastManager('info', 'Info', 'Payable To is required.');
                    return true;
                }
            }
        }
        if (this.payerValue == null || this.payerValue == '') {
            this.toastManager('error', 'Error', 'Payer is required.');
            event.preventDefault();
            hideSpinner(this);
            return true;
        }
        /*if (this.parentFiscalYear == null || this.parentFiscalYear == '') {
            this.toastManager('error', 'Error', 'Accounting Period is required.');
            event.preventDefault();
            hideSpinner(this);
            return true;
        }
        else{
            fields.Accounting_Period__c = this.parentFiscalYear;
        }*/
        if(this.showRefundSource){
            if (this.paymentSourceValue == null || this.paymentSourceValue == '') {
                this.toastManager('error', 'Error', 'Transcation Code is required.');
                event.preventDefault();
                hideSpinner(this);
                return true;
            
            }
        }
        if(this.showOtherTranscationText){
            if (fields.Other_Transaction_Description__c == null || fields.Other_Transaction_Description__c == '') {
                this.toastManager('error', 'Error', 'Other Transaction Description is required.');
                event.preventDefault();
                hideSpinner(this);
                return true;
            
            }
        }
        if (fields.Payment_Date__c == null) {
            this.toastManager('error', 'Error', 'Date of Receipt is required.');
            event.preventDefault();
            hideSpinner(this);
            return true;
        }

        if (fields.Payment_Amount__c == null || fields.Payment_Amount__c == '') {
            this.toastManager('error', 'Error', 'Payment Amount is required.');
            event.preventDefault();
            hideSpinner(this);
            return true;
        }
        else{
            if(!this.addAmount){
                this.paymentAmount = parseInt(fields.Payment_Amount__c);
            }
            
        }
        if (this.value != null && this.value != '') {
            if(this.value == 'Vendor'){
                if (fields.Vendor__c == null || fields.Vendor__c == '') {
                    this.toastManager('error', 'Error', 'Vendor is Required');
                    event.preventDefault();
                    hideSpinner(this);
                    return true;
                }
            }
            if(this.value == 'Case'){
                if (fields.Kinship_Case__c == null || fields.Kinship_Case__c == '') {
                    this.toastManager('error', 'Error', 'Case is Required');
                    event.preventDefault();
                    hideSpinner(this);
                    return true;
                }
            }
            if(this.value == 'Contact'){
                if (fields.Contact__c == null || fields.Contact__c == '') {
                    this.toastManager('error', 'Error', 'Contact is Required');
                    event.preventDefault();
                    hideSpinner(this);
                    return true;
                }
            }
            if(this.value == 'Claim'){
                if (fields.Claims__c == null || fields.Claims__c == '') {
                    this.toastManager('error', 'Error', 'Claim is Required');
                    event.preventDefault();
                    hideSpinner(this);
                    return true;
                }
            }
            if(this.value == 'SpecialWelfareAccount'){
                fields.Recipient__c = 'Special Welfare Account';
                if (fields.Special_Welfare_Account__c == null || fields.Special_Welfare_Account__c == '') {
                    this.toastManager('error', 'Error', 'Special Welfare Account is Required');
                    event.preventDefault();
                    hideSpinner(this);
                    return true;
                }
            }
        }
        if (this.categoryType != null && this.categoryType == 'CSA' && this.payerValue != 'Vendor') {
            this.toastManager('error', 'Error', 'Please select vendor as payer for CSA category.');
            event.preventDefault();
            hideSpinner(this);
            return true;
        }
        if (this.categoryType != null && this.categoryType == 'IVE') {
            console.log('categogryType??????',this.categoryType);
            this.toastManager('error', 'Error', 'IVE categories are not allowed for receipts.');
            event.preventDefault();
            hideSpinner(this);
            return true;
        }
        if (this.SWAcategoryId != null && this.SWAcategoryId == '') {
            this.toastManager('error', 'Error', 'Category is required.');
            event.preventDefault();
            hideSpinner(this);
            return true;
        }
        if (!this.showCostLookup) {
            this.toastManager('error', 'Error', 'There is no Cost Center associated with this Category.');
            event.preventDefault();
            hideSpinner(this);
            return true;
        }
        if (this.costCernterId != null && this.costCernterId == '') {
            this.toastManager('error', 'Error', 'Cost Center is required.');
            event.preventDefault();
            hideSpinner(this);
            return true;
        }
        /*if(!this.disabledCase){
            if ((fields.Kinship_Case__c == null && fields.Claims__c == null && fields.Contact__c == null && fields.Vendor__c == null && fields.Special_Welfare_Account__c == null)
                || (fields.Kinship_Case__c == '' && fields.Claims__c == '' && fields.Contact__c == '' && fields.Vendor__c == '')&& fields.Special_Welfare_Account__c == '') {
                this.toastManager('error', 'Error', 'One of the following fields should have a value: Case, Contact, Claim, Vendor,Special Welfare Account');
                event.preventDefault();
                hideSpinner(this);
                return true;
            }
        }*/
        
        console.log(this.selectedReceipt + ' ???? this.selectedReceipt?????');
        //if (this.selectedReceipt == null || this.selectedReceipt == '') {
        if (this.rcordname == "Receipt" ) {
            
            isReceiptNumberEsixt({ rNumber: fields.Report_Number__c, ROCID: this.reportCollectionId , receiptid: this.selectedReceipt}).then((result) => {
                    console.log(result + ' ???? result?????');
                     /*if (result) {

                        this.toastManager('error', 'Duplicate Receipt Number', 'Receipt Number must be unique.');
                        hideSpinner(this);
                    } else */{
                        this.template.querySelector('lightning-record-edit-form').submit(fields);
                        setTimeout(function() {
                            this.closeToast();
                            hideSpinner(this);
                        }.bind(this), 2000);

                    }
                })
                .catch((error) => {
                    console.log(error);
                });
        } else {
            this.template.querySelector('lightning-record-edit-form').submit(fields);

            showSpinner(this);
        }


    }
    
    
    
    HandlSubmit(event) {
        const fields = event.detail.fields;
        this.fields = event.detail.fields;

        if(this.objectName == 'Claims__c'){
            if(fields.Payable_to__c  == null && fields.Case__c  == null) 
            {
                
                this.toastManager('error', 'Error', 'Payable to OR Case field is required.');
                event.preventDefault();
                hideSpinner(this);
                return true;
            }
            if(fields.Payable_to__c  == null){
                this.payableToCheck = true;
            }
            else{
                this.payableToCheck = false;
            }
            if(fields.Claim_Type__c == null || fields.Claim_Type__c == '')
            {
                this.toastManager('error', 'Error', 'Please select Claim Type.');
                event.preventDefault();
                hideSpinner(this);
                return true;
            }
    
            // if(fields.Claim_Amount__c == null || fields.Claim_Amount__c == '')
            // {
            //     this.toastManager('error', 'Error', 'Please enter Amount');
            //     event.preventDefault();
            //     hideSpinner(this);
            //     return true;
            // }
    
            // if(fields.Claim_Begin_Date__c == null || fields.Claim_Begin_Date__c == '')
            // {
            //     this.toastManager('error', 'Error', 'Please select Begin Date.');
            //     event.preventDefault();
            //     hideSpinner(this);
            //     return true;
            // }
    
            // if(fields.Claim_End_Date__c == null || fields.Claim_End_Date__c == '')
            // {
            //     this.toastManager('error', 'Error', 'Please select End Date.');
            //     event.preventDefault();
            //     hideSpinner(this);
            //     return true;
            // }
    
        }
    }
    showEditHandle(event){
        this.isView = false;
    }
    handleSuccess(event) {
        if(this.addAmount){
            this.balanceamount = this.balanceamount + this.paymentAmount;
        }
        if( this.subtractAmount){
            this.balanceamount = this.balanceamount - this.paymentAmount;
        }
        /*if(this.paymentAmount<this.balanceamount){
            this.balanceamount = this.balanceamount - this.paymentAmount;
        }
        else{
            this.balanceamount = 0;
        }*/
        
        this.selectedReceipt = event.detail.id;
        updateFipsCodeinInvoices({ invoiceid: this.selectedReceipt}).then((result) => {
            console.log(result + ' ???? result?????');
        })
        .catch((error) => {
            console.log(error);
        });
        const inputFields = this.template.querySelectorAll(
            'lightning-input-field'
        );
        if (inputFields) {
            inputFields.forEach(field => {
                if(field.fieldName == 'Report_Number__c'){      
                    this.receiptNumber = parseInt(this.receiptNumber)+1;
                    this.receiptNumber = this.receiptNumber.toString(); 
                    console.log('ELSE Condition receipt number>>>>>', this.receiptNumber);
                    
                }
                else if(field.value != this.closedate){
                    field.value  = null;
                }
                
                 
            });
        }
   
        this.disableCostCenter = false;
        this.disableFiscalYear = false;
        this.showCostLookup = false;
        this.parentFisaclYearName ='';
        this.selectedReceipt = null;
        if(!this.isNext ){
            const selectedEvent = new CustomEvent("closereceiptcancel", {
                detail:this.isNext 

            });
            this.dispatchEvent(selectedEvent);
            
        }
        this.value = 'Case';
        this.payerValue = 'Vendor';
        this.Categorydisable = false;
        this.vendorPayer = true;	
        this.casePayer = false;	
        this.contactPayer = false;
        
        this.showCase = true;
        
        this.showClaim = false;
        this.showClaimBalance = false;
        this.showSpecialWelfareAccount = false;
        this.showSWRBalance = false;
        this.showRefundSource = false;	
        this.showOtherTranscationText = false;
        this.isCatLaser = false;
        this.SWAcategoryId = '';
        const selectedEvent = new CustomEvent("balanceamountchange", {
            detail:this.balanceamount 
            
        });
        this.dispatchEvent(selectedEvent);
        
        //this.showLoader = false;
       
        //this.isView = true;
        hideSpinner(this);
        //fields.LastName = 'My Custom Last Name'; // modify a field  
    }
    newReceiptForm(event){
        this.template.querySelector('lightning-record-edit-form').submit(this.fields);
        
        //const selectedEvent = new CustomEvent("closereceiptcancel", {
        //    detail: this.isNext
        //});
        //this.dispatchEvent(selectedEvent);
        this.selectedReceipt = '';
        this.isView = false;
    }
    handleReset(event) {
        const inputFields = this.template.querySelectorAll(
            'lightning-input-field'
        );
        if (inputFields) {
            inputFields.forEach(field => {
                field.reset();
            });
        }
    }
    errorHandler(e) {
        console.log('s');
        this.template.querySelector(".slds-notify_container.slds-is-relative");
        hideSpinner(this);
    }

    @track isShowToast = false;
    @track toastType = '';
    @track title = '';
    @track message = '';
    @track toastIcon = 'utility:error';
    titleoftoast;
    toastManager(toastType, title, message) {
        this.toastType = "slds-notify slds-notify_toast ";
        if (toastType == 'error') {
            this.toastType += 'slds-theme_error';
        } else if (toastType == 'success') {
            this.toastType += 'slds-theme_success';
        } else if (toastType == 'info') {
            this.toastType += 'slds-theme_info';
            this.toastIcon = 'utility:info';
        }
        else if (toastType == 'warning') {
            this.toastType += 'slds-theme_warning';
            this.toastIcon = 'utility:warning';
        }
        this.titleoftoast = title;
        this.message = message;
        this.isShowToast = true;
        setTimeout(function() {
            this.isShowToast = false;
        }.bind(this), 10000);
    }
    closeToast(event) {
        this.isShowToast = false;
        return false;
    }

    @api
    setIsUpdate(isUpdate) {
        this.isUpdate = isUpdate;

    }
    @track isNext = true; 
    saveOrNextHandle(event){

        
        var flag = event.target.dataset.isnext;        
        if(flag == "0"){
            this.isNext = false;
            
        }
        if(flag == "1"){
            this.isNext = true;
            this.selectedReceipt = null;
        }

        
    }
    invoicecancel() {
        this.isNext = false;
        const selectedEvent = new CustomEvent("closereceipt", {
            detail: this.isNext
        });
        this.dispatchEvent(selectedEvent);
    }
    @track receiptTypeValue = '';
    @track showRefundSource = false;
    @track showRefundSourc = false;
    @track disabledCase = false;
    @track isCatLaser = false; 
    @track categoryType = ''; 
    
    handleChangeCategory(event){
        var categoryId = event.target.value;
        if(categoryId != null && categoryId !=''){
            getCategoryType({ categoryId: categoryId}).then((result) => {
                 if (result) {
                     var catRecName = '';
                    this.receiptTypeValue = result.Category_Type__c;
                    this.categoryType = result.Category_Type__c;
                    catRecName = result.RecordType.Name;
                    if(catRecName.includes('LASER')){
                        this.isCatLaser = true;
                      
                        //this.isCatLaser = true;

                    }
                    else{
                        this.isCatLaser = false;
                    }
                    this.SWAcategoryId = result.Id;
                    //if(this.rcordname != 'Vendor Refund'){
                        if(result.Category_Type__c =='CSA' || result.Category_Type__c =='Special Welfare' || this.receipttype == 'IVE'){
                            this.showRefundSource = true;
                            this.showRefundSourc = true;
                            if(this.receipttype =='CSA'){	
                                this.optionsTranscationCode = [];	
                                this.optionsTranscationCode =[{ label: 'Parental Co-Payment', value: '4' },{ label: 'Other-Credits', value: '11' }];	
                                //this.optionsTranscationCode.push({ label: 'Other-Credits', value: '11' });	
                            }	
                            else if(this.receipttype =='Special Welfare'){	
                                this.optionsTranscationCode = [];	
                                this.optionsTranscationCode = [{label: 'Child Support Collections through DCSE', value: '5' },{ label: 'Payments made on behalf of the child (ex: SSA, SSI, VA benefits, …) – Special Welfare Refunds', value: '6'  }];	
                                //this.optionsTranscationCode.push({ label: 'Payments made on behalf of the child (ex: SSA, SSI, VA benefits, …) – Special Welfare Refunds', value: '6'  });	
                            }	
                            else{	
                                this.optionsTranscationCode = [];	
                                this.optionsTranscationCode =[{ label: 'Parental Co-Payment', value: '4' },{ label: 'Other-Credits', value: '11' }];	
                                
                                /*this.optionsTranscationCode.push({label: 'Child Support Collections through DCSE', value: '5' });	
                                this.optionsTranscationCode.push({ label: 'Payments made on behalf of the child (ex: SSA, SSI, VA benefits, …) – Special Welfare Refunds', value: '6'  });	
                                this.optionsTranscationCode.push({ label: 'Other-Credits', value: '11' });	
                            */}	
                        }
                        else{
                            this.showRefundSource = false;
                            this.showRefundSourc = false;
                            this.showOtherTranscationText = false;
                        }
                        if(result.Category_Type__c =='Special Welfare'){
                            this.value = 'SpecialWelfareAccount';
                            this.showSpecialWelfareAccount = true;
                            this.showCase = false;
                            
                            
                            this.showClaim = false;
                            this.showClaimBalance = false;
                            //this.disabledCase = true;
                            this.Kinship_Case__c = null;
                            this.showRefundSourc = false;
                            this.SWAcategoryName='';
                        }
                        else{
                            this.disabledCase = false;
                        }
                    //}
                    
                } else {
                   this.receiptTypeValue = '';
    
                }
            })
            .catch((error) => {
                //console.log(error);
            });
        }
       
    }
    handleChangePaymentAmount(event){
        var paymentAmount = event.target.value;
        this.receiptAmount = 0;
        this.receiptAmount = this.balanceamount + +paymentAmount;

        if(this.selectedPaymentAmount != ''){
            if(paymentAmount == this.selectedPaymentAmount){
                this.equal = true;
                this.addAmount = false;
                this.subtractAmount = false;
            }
            if(paymentAmount > this.selectedPaymentAmount){
                this.equal = false;
                this.addAmount = false;
                this.subtractAmount = true;
            }
            if(paymentAmount < this.selectedPaymentAmount){
                this.equal = false;
                this.addAmount = true;
                this.subtractAmount = false;
                this.paymentAmount =this.selectedPaymentAmount - paymentAmount;
            
        }
        }

        
    }

    selectedRecords(event) {
        this.catWhereStr = "  AND Inactive__c = false";
        let selectedRecords = JSON.parse(event.detail);
      
        if (selectedRecords && selectedRecords.object === "Category__c") {
            if(selectedRecords.records.length > 0){
                this.SWAcategoryId= selectedRecords.records[0].recId;
                getCategoryType({ categoryId: this.SWAcategoryId}).then((result) => {
                    if (result) {
                        var catRecName = '';
                       this.receiptTypeValue = result.Category_Type__c;
                       this.categoryType = result.Category_Type__c;
                       catRecName = result.RecordType.Name;
                       if(catRecName.includes('LASER')){
                        this.isCatLaser = true;
                       
                        //this.isCatLaser = true;
                       }
                       else{
                           this.isCatLaser = false;
                       }
                       this.SWAcategoryId = result.Id;
                       //if(this.rcordname != 'Vendor Refund'){
                           if(result.Category_Type__c =='CSA' || result.Category_Type__c =='Special Welfare' || result.Category_Type__c == 'IVE'){	
                                this.showRefundSource = true;	
                                if(result.Category_Type__c =='CSA'){	
                                 this.optionsTranscationCode = [];	
                                 this.optionsTranscationCode =[{ label: 'Parental Co-Payment', value: '4' },{ label: 'Other-Credits', value: '11' }];	
                                }	
                                else if(result.Category_Type__c =='Special Welfare'){	
                                    this.optionsTranscationCode = [];	
                                    this.optionsTranscationCode = [{label: 'Child Support Collections through DCSE', value: '5' },{ label: 'Payments made on behalf of the child (ex: SSA, SSI, VA benefits, …) – Special Welfare Refunds', value: '6'  }];	
                                }	
                                else{
                                    this.isCatLaser = true;	
                                    this.fundValue = 'Reimbursable';
                                    this.optionsTranscationCode = [];	
                                    this.optionsTranscationCode =[{ label: 'Parental Co-Payment', value: '4' },{ label: 'Other-Credits', value: '11' }];	
                                    
                                }
                    
                                this.showRefundSourc = true;
                                
                           }
                           
                           else{
                               this.showRefundSource = false;
                               this.showRefundSourc = false;
                               this.showOtherTranscationText = false;
                           }
                           if(result.Category_Type__c =='Special Welfare'){
                               this.value = 'SpecialWelfareAccount';
                               this.showSpecialWelfareAccount = true;
                               this.showCase = false;
                               
                               
                               this.showClaim = false;
                               this.showClaimBalance = false;
                               //this.disabledCase = true;
                               this.Kinship_Case__c = null;
                               this.showRefundSourc = false;
                               this.SWAcategoryName='';
                           }
                           else{
                               this.disabledCase = false;
                           }
                           if(result.Category_Type__c =='Claims Payable'){
                            this.catWhereStr = "  AND Inactive__c = false AND Category_Type__c = 'Claims Payable'";
                            this.value ='Claim'
                            this.choseCase=false;
                            
                            this.showCase = false;
                            
                            this.showClaim = true;
                            this.showSpecialWelfareAccount = false;
                            this.showSWRBalance = false;
                            if(!this.showRefundSourc){
                                this.showRefundSource = false;
                                this.showOtherTranscationText = false;
                            }
                            }
                            if(result.Category_Type__c =='CSA' || result.Category_Type__c == 'IVE'){
                                this.naCatType = true;
                            }
                            else{
                                this.naCatType = false;
                            }
                       //}
                       
                   } else {
                      this.receiptTypeValue = '';
                      
       
                   }
               })
               .catch((error) => {
                   //console.log(error);
               });
                this.getCostCenters(this.SWAcategoryId); 
                this.disableFiscalYear = true;
            }else{
                this.disableFiscalYear = false;
                this.showCostLookup = false;
                this.showRefundSource = false;
                this.SWAcategoryId= '';
                this.value ='Case'
                        this.receiptTypeValue = '';
                        this.naCatType = false;
                        this.showCase = true;
                        
                        this.showClaim = false;
                        this.showClaimBalance = false;
                        this.showSpecialWelfareAccount = false;
                        this.showSWRBalance = false;
                        if(!this.showRefundSourc){
                            this.showRefundSource = false;
                            this.showOtherTranscationText = false;
                        }
            }
        }
        if (selectedRecords && selectedRecords.object === "Cost_Center__c") {
            if(selectedRecords.records.length > 0){
                this.costCernterId= selectedRecords.records[0].recId;
                this.disableCostCenter = true;
            }else{
                this.disableCostCenter = false;
                this.costCernterId= '';
            }
        }
        
    }
    costWhereStr;
    costCenterIds = [];
    showCostLookup = false;
    costCernterId;
    getCostCenters(categoryId){
        getCostCenter({ categoryId: categoryId}).then((result) => {
            if (result) {
                this.costCenterIds = [];
                if(result.length > 1){
                    this.costCernterId = '';
                    this.parentCostCenterName = '';
                    this.disableCostCenter = false;
                    result.forEach(ele => {
                        if(this.fipsCode == ele.FIPS__c){
                            let newsObject = [{ 'recId' : ele.Cost_Center__c ,'recName' : ele.Cost_Center__r.Name }];
                            this.costCernterId = ele.Cost_Center__c;
                            this.parentCostCenterName = JSON.stringify(newsObject);
                            this.disableCostCenter = true;
                        }
                        else{
                            let newsObject = [{ 'recId' : ele.Cost_Center__c ,'recName' : ele.Cost_Center__r.Name }];
                            this.costCernterId = ele.Cost_Center__c;
                            this.parentCostCenterName = JSON.stringify(newsObject);
                            this.disableCostCenter = true;
                        }
                        this.costCenterIds.push(ele.Cost_Center__c);
                    });
                    this.showCostLookup = true;
                }
                else if(result.length == 1){
                    result.forEach(ele => {
                        if(ele.Cost_Center__c){
                            let newsObject = [{ 'recId' : ele.Cost_Center__c ,'recName' : ele.Cost_Center__r.Name }];
                            this.costCernterId = ele.Cost_Center__c;
                            this.parentCostCenterName = JSON.stringify(newsObject);
                            this.disableCostCenter = true;
                            this.costCenterIds.push(ele.Cost_Center__c);
                        }
                    });
                    this.showCostLookup = true;
                }
                else{
                    this.showCostLookup = false;
                    this.toastManager('error', 'Error', 'There is no Cost Center associated with this Category.');
                }
               
                
            }
            else{
                this.showCostLookup = false;
                this.toastManager('error', 'Error', 'There is no Cost Center associated with  this Category.');
            }
       })
       .catch((error) => {
           //console.log(error);
       });
    }
    objectName;
    showCreateNew = false;
    fieldName;
    titleOfNew;
    title;
    vendorValue;
    payerCaseValue;
    payerContactValue;
    vendorValue;
    claimsValue;
    specialWelfareAccountValue;
    reportNumberValue;
    contactValue;
    @api caseValue;
    showClaimBalance = false;
    showSWRBalance = false;
    payableToCheck = false;
    updateClaim = false;
    handleCaseChange(event){
        this.caseValue = event.detail.value[0];
        if(this.caseValue == undefined){
            this.createCaseShow = true;
        }
        else{
            this.createCaseShow = false;
        }
    }
    handleSpecialWelfareChange(event){
        this.specialWelfareAccountValue = event.detail.value[0];
        if(this.specialWelfareAccountValue == undefined){
            this.createSWAShow = true;
        }
        else{
            this.createSWAShow = false;
            this.getCaseId(this.specialWelfareAccountValue,'SWA');
        }
        
        this.showSWRBalance = true;
    }
    handleClaimsChange(event){
        this.claimsValue = event.detail.value[0];
        console.log('Claimvalue handleClaimsChange????',this.claimsValue);
        if(this.claimsValue == undefined){
            this.createClaimShow = true;
        }
        else{
            this.createClaimShow = false;
            this.checkPayable(this.claimsValue);
            this.getCaseId(this.claimsValue,'Claim');
            this.getClaimType(this.claimsValue);
        }
        this.showClaimBalance = true;
    }
    handleVendorChange(event){
        this.vendorValue = event.detail.value[0];
        if(this.vendorValue == undefined){
            this.createVendorShow = true;
        }
        else{
            this.createVendorShow = false;
        }
    }
    handleContactChange(event){
        this.payerCaseValue = event.detail.value[0];
        if(this.payerCaseValue == undefined){
            this.createContactShow = true;
        }
        else{
            this.createContactShow = false;
            getClaim({ contactId: this.payerCaseValue}).then((result) => {
                if (result) {      
                    this.claimsValue = result.Id;
                    if(this.claimsValue)
                    {
                        this.createClaimShow = false;
                        this.showClaimBalance = true;
                        this.showClaim = true;
                        this.showCase = false;
                        this.showSpecialWelfareAccount = false;
                        this.value = 'Claim';
                        //this.catWhereStr = "  AND Inactive__c = false AND Category_Type__c = 'Claims Payable'";
                        this.checkPayable(this.claimsValue);   
                    }
                    else{
                        this.claimsValue = '';
                        this.payableToCheck = false; 
                    }  
                }
           })
           .catch((error) => {
               //console.log(error);
           });
        }
    }
    checkPayable(claimId){
        checkPayable({ claimId: claimId}).then((result) => {
            if (result) {      
                this.payableToCheck = true;         
            }
            else{
                this.payableToCheck = false; 
            }
        })
        .catch((error) => {
            //console.log(error);
        });
    }
    getCaseId(recordId,object){
        getCaseId({ recordId:recordId,
                    objectName:object}).then((result) => {
            if (result) {      
                if(object == 'SWA'){
                    if(result.Case_KNP__c){
                        this.caseValue = result.Case_KNP__c;
                    }
                    
                } 
                else if(object == 'Claim'){
                    if(result.Case__c){
                        this.caseValue = result.Case__c;
                    }
                    
                }       
            }
        })
        .catch((error) => {
            //console.log(error);
        });
    }

    getClaimType(recordId){
        getClaimType({recordId:recordId}).then((result)=>{
            if(result)
            {
                if(result.Claim_Type__c == 'ADMINISTRATIVE')
                    {
                        this.catWhereStr = "  AND Inactive__c = false AND Category_Type__c =" + "'" + result.Claim_Type__c + "'" ;
                        }
                else if(result.Claim_Type__c == 'PARENTAL COPAYMENTS')
                    {
                        this.catWhereStr = "  AND Inactive__c = false AND Category_Type__c = 'CSA'" ;
                    }
                else if(result.Claim_Type__c == 'Claims Payable')
                    {
                        this.catWhereStr = "  AND Inactive__c = false AND Category_Type__c = 'Claims Payable'" ;
                    }
            }
            

        })

    }

    createVendor(){
        this.title = 'Vendor';
        this.objectName = 'Account';
        this.fieldName = ['Name','Email__c','Tax_ID__c','Locality_Vendor_Number__c','BillingAddress'];
        this.showCreateNew = true;
        this.titleOfNew = 'Create New Vendor';

    }
    createPayerCase(){
        this.title = 'PayerCase';
        this.objectName = 'Kinship_Case__c';
        this.fieldName = ['Name','Primary_Contact__c','Gender_Preference__c','DOB__c'];
        this.showCreateNew = true;
        this.titleOfNew = 'Create New Case';
    }
    createPayerContact(){
        this.title = 'PayerContact';
        this.objectName = 'Contact';
        this.fieldName = ['Title','Name','Email','Phone'];
        this.showCreateNew = true;
        this.titleOfNew = 'Create New Contact';
    }
    /*createVendor(){
        this.title = 'Vendor';
        this.objectName = 'Account';
        this.fieldName = ['Name','Email__c','Fax','Phone'];
        this.showCreateNew = true;
        this.titleOfNew = 'Create New Vendor';
    }*/
    createCase(){
        this.title = 'Case';
        this.objectName = 'Kinship_Case__c';
        this.fieldName = ['Name','Primary_Contact__c','Gender_Preference__c','DOB__c'];
        this.showCreateNew = true;
        this.titleOfNew = 'Create New Case';
    }
    createContact(){
        this.title = 'Contact';
        this.objectName = 'Contact';
        this.fieldName = ['Title','Name','Email','Phone'];;
        this.showCreateNew = true;
        this.titleOfNew = 'Create New Contact';
    }
    createSpecialWelfareAccount(){
        this.title = 'SpecialWelfareAccount';
        this.objectName = 'Special_Welfare_Account__c';
        this.fieldName = ['Case_KNP__c','Initial_Balance__c','Account_Description__c'];
        this.showCreateNew = true;
        this.titleOfNew = 'Create New Special Welfare Account';
    }
    createClaim(){
        this.title = 'Claim';
        this.objectName = 'Claims__c';
        this.fieldName = ['Claim_Type__c','Claim_Amount__c', 'Case__c','Payable_to__c', 'Claim_Notes__c'];
        this.showCreateNew = true;
        this.titleOfNew = 'Create New Claims';
    
    }
    closeCreateNewModal(){
        this.showCreateNew = false;
    }
    createNewCancel(){
        this.showCreateNew = false;
    }
    handleCreateSuccess(event){
        if(this.title == 'PayerVendor'){
            this.vendorValue = event.detail.id;
            this.createVendorShow = false;
            console.log('vendorValue Id????????????',this.vendorValue);

            this.payerCaseValue='';
            this.createContactShow = true;
            this.payerContactValue='';
        }
        else if(this.title == 'PayerCase'){
            this.payerCaseValue = event.detail.id;
            this.createContactShow = false;
            console.log('payerCaseValue Id????????????',this.payerCaseValue);

            this.vendorValue='';
            this.createVendorShow = true;
            this.payerContactValue='';
        }
        else if(this.title == 'PayerContact'){
            this.payerContactValue = event.detail.id;
            console.log('payerContactValue Id????????????',this.payerContactValue);

            this.payerCaseValue='';
            this.createContactShow = true;
            this.vendorValue='';
            this.createVendorShow = true;
        }
        else if(this.title == 'Case'){
            this.disabledCase = false;
            this.caseValue = event.detail.id;
            this.createCaseShow = false;
            console.log('caseValue Id????????????',this.caseValue);
            this.showClaimBalance = false;
            this.contactValue ='';
            this.specialWelfareAccountValue='';
            this.createSWAShow = true;
            this.claimsValue='';
            this.createClaimShow = true;
            this.vendorValue='';
            this.createVendorShow = true;
        }
        else if(this.title == 'Contact'){
            this.contactValue = event.detail.id;
            console.log('contactValue Id????????????',this.contactValue);
            this.showClaimBalance = false;
            this.caseValue = '';
            this.createCaseShow = true;
            this.specialWelfareAccountValue='';
            this.createSWAShow = true;
            this.claimsValue='';
            this.createClaimShow = true;
            this.vendorValue='';
            this.createVendorShow = true;
        }
        else if(this.title == 'SpecialWelfareAccount'){
            this.specialWelfareAccountValue = event.detail.id;
            this.createSWAShow = true;
            this.showSWRBalance = true;
            this.showClaimBalance = false;
            this.caseValue = '';
            this.createCaseShow = true;
            this.contactValue ='';
            this.claimsValue='';
            this.createClaimShow = true;
            this.vendorValue='';
            this.createVendorShow = true;
        }
        else if(this.title == 'Claim'){
            if(this.updateClaim){

            }
            else{
                this.claimsValue = event.detail.id;
                this.createClaimShow = false;
                this.showClaimBalance = true;
                console.log('Claim Id????????????',this.claimsValue);
                this.caseValue = '';
                this.createCaseShow = true;
                this.contactValue ='';
                console.log('specialWelfareAccountValue??????????',this.specialWelfareAccountValue);
                this.specialWelfareAccountValue='';
                this.createSWAShow = true;
                this.vendorValue='';
                this.createVendorShow = true;
            }
        }
        else if(this.title == 'Vendor'){
            this.vendorValue = event.detail.id;
            this.createVendorShow = false;
            this.caseValue = '';
            this.createCaseShow = true;
            this.contactValue ='';
            this.specialWelfareAccountValue='';
            this.createSWAShow = true;
            this.claimsValue='';
            this.createClaimShow = true;
        }
        this.showCreateNew = false;
    }
    showOtherTranscationText = false;
    paymentSourceValue = '';
    handleFundChange(event)
    {
        this.fundValue = event.detail.value;
        console.log('this.fundValue???????', this.fundValue);

    }
    handlePaymentSourceChange(event){
        console.log(event);
        this.paymentSourceValue = event.detail.value;
        if(event.detail.value == '11'){
            this.toastManager('info', 'Info', 'Special Welfare Accounting Procedures: If this receipt for a Child Support or SSA/SSI deposit, change this category to Special Welfare to first record the deposit in the child’s Special Welfare Account. Next, to reimburse CSA or IV-E expenditures using Child Support or SSA/SSI deposit funds that were deposited into a child’s special welfare account, close this screen and select the Special Welfare Reimbursement option. See section 3.5 of the Finance Guidelines Manual for additional information.');
            this.showOtherTranscationText = true;
        }
        else{
            this.showOtherTranscationText = false;
        }
    }
    
}