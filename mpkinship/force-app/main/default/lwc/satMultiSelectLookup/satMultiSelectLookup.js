import { LightningElement,api,track } from 'lwc';
import getResults from '@salesforce/apex/MultiSelectLookupController.getResults';

export default class LwcMultiLookup extends LightningElement {
    @api objectname = 'Account';
    @api fieldnames = ['Name'];
    @api label;
    @api placeholder;
    @api perentselect;
    @api disabled = false
    @api required;
    @api variant;
    @api styl;
    @api iconname = 'action:new_account'
    @api LoadingText = false;
    @api whereStr = '';
    @api whereRecstr = '';
    @api parentId; 
    @api likeString;
    @track searchRecords = [];
    @track csasearchRecords = [];
    @track selectedRecords = [];
    @track txtclassname = 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click';
    @track messageFlag = false;
    @api fipsCode;
    @api costCenterIds;
    @api showCostLookup;
    @api disableCostCenter;

    connectedCallback() {
        
        console.log('catWhereStr1111???', this.whereStr);
        if (this.perentselect && this.perentselect != "") {
            this.selectedRecords = JSON.parse(this.perentselect);
            console.log('selectedRecords sat??', this.selectedRecords);

        } else {
            this.selectedRecords = [];
        }
        if(this.showCostLookup && !this.disableCostCenter){
            this.searchField();
        }
        
    }
    
    @api
    setData() {
        if (this.perentselect && this.perentselect !== "") {
            this.selectedRecords = JSON.parse(this.perentselect);
            console.log('selectedRecords setData??', this.selectedRecords);
        } else {
            this.selectedRecords = [];
        }
    }
 
    



    searchField(event) {
        let currentText = ''
        if(event){
            currentText = event.target.value;
        }
        else{
            currentText = '';
        }
        let selectRecId = [];
        for(let i = 0; i < this.selectedRecords.length; i++){
            selectRecId.push(this.selectedRecords[i].recId);
        }
        console.log('selectRecId????', this.selectRecId);
        this.LoadingText = true;
        console.log(this.fieldnames);
        getResults({ ObjectName: this.objectname, fieldNames: this.fieldnames, value: currentText, selectedRecId : selectRecId, whereStr: this.whereStr,parentId: this.parentId,likeString: this.likeString,costCenterIds : this.costCenterIds })
        .then(result => {
            console.log('In getResults');
            this.csasearchRecords = [];
            result.forEach(element => {
                console.log('Result in getResults???',result);
                if(element.thirdName == 'CSA'){
                    if(element.forthName== this.fipsCode){
                        this.csasearchRecords.push(element);
                    }
                }
                else{
                    this.csasearchRecords.push(element);
                }
            });
            this.searchRecords= this.csasearchRecords;
            console.log(this.csasearchRecords);
            this.LoadingText = false;
            
            this.txtclassname =  result.length > 0 ? 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click slds-is-open' : 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click';
            if(currentText.length > 0 && (result.length == 0 || this.csasearchRecords.length ==0)) {
                this.messageFlag = true;
            }
            else {
                this.messageFlag = false;
            }

            if(this.selectRecordId != null && this.selectRecordId.length > 0) {
                this.iconFlag = false;
                this.clearIconFlag = true;
            }
            else {
                this.iconFlag = true;
                this.clearIconFlag = false;
            }
        })
        .catch(error => {
            console.log('-------error-------------'+error);
            console.log(error);
        });
        
    }
    
   setSelectedRecord(event) {
       console.log("setSelect");
        var recId = event.currentTarget.dataset.id;
        var selectName = event.currentTarget.dataset.name;
        let newsObject = { 'recId' : recId ,'recName' : selectName };
        this.selectedRecords.push(newsObject);
        this.txtclassname =  'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click';
		this.template.querySelectorAll('lightning-input').forEach(each => {
            each.value = '';
        });
        let selRecords = {
            object: this.objectname,
            records: this.selectedRecords
        }
        const selectedEvent = new CustomEvent('selected', { detail: JSON.stringify(selRecords) });
        this.dispatchEvent(selectedEvent);
    }

    removeRecord (event){
        let selectRecId = [];
        for(let i = 0; i < this.selectedRecords.length; i++){
            if(event.detail.name !== this.selectedRecords[i].recId)
                selectRecId.push(this.selectedRecords[i]);
        }
        this.selectedRecords = [...selectRecId];
        let selRecords = {
            object: this.objectname,
            records: this.selectedRecords
        }
        const selectedEvent = new CustomEvent('selected', { detail: JSON.stringify(selRecords) });
        this.dispatchEvent(selectedEvent);
    }
}