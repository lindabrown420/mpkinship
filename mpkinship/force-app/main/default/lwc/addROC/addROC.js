import { LightningElement, api, track, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent'
import { NavigationMixin } from 'lightning/navigation';

import FiscalYear_FIELD from '@salesforce/schema/Opportunity.CampaignId';
import ChildFiscalYear_FIELD from '@salesforce/schema/Opportunity.Fiscal_Year_fr__c';
import RecordTypeId from '@salesforce/schema/Payment__c.RecordTypeId';
import begindate_FIELD from '@salesforce/schema/Opportunity.CloseDate';
import bgrept_FIELD from '@salesforce/schema/Opportunity.Beginning_Receipt__c';
import Endrept_FIELD from '@salesforce/schema/Opportunity.Ending_Receipt__c';
import Enddate_FIELD from '@salesforce/schema/Opportunity.End_Date__c';
import Refernce_FIELD from '@salesforce/schema/Opportunity.Reference__c';
import TotalAmount_FIELD from '@salesforce/schema/Opportunity.Amount';
import Stage_FIELD from '@salesforce/schema/Opportunity.StageName';
import RECEIPT_AMOUNT_FIELD from '@salesforce/schema/Opportunity.Total_Receipts__c';
import FIPSCode from '@salesforce/schema/Opportunity.FIPS_Code__c';
import Pay_stage from '@salesforce/schema/Payment__c.Invoice_Stage__c';
import getReceiptsFromROC from "@salesforce/apexContinuation/ROCController.getReceiptsFromROC";
import getAdjustmentsFromROC from "@salesforce/apexContinuation/ROCController.getAdjustmentsFromROC";
import getvRefundFromROC from "@salesforce/apexContinuation/ROCController.getvRefundFromROC";
import getNextRecord from "@salesforce/apexContinuation/ROCController.getNextRecord";
import getSWRFromROC from "@salesforce/apexContinuation/ROCController.getSWRFromROC";
import isAmountsEqual from "@salesforce/apexContinuation/ROCController.isAmountsEqual";
import markCompleteStage from "@salesforce/apexContinuation/ROCController.markCompleteStage";
import getROCRecord from "@salesforce/apexContinuation/ROCController.getROCRecord";
import deleteReceipt from "@salesforce/apexContinuation/ROCController.deleteReceipt";   
import markOpenStage from "@salesforce/apexContinuation/ROCController.markOpenStage";
import checkReceiptRange from "@salesforce/apexContinuation/ROCController.checkReceiptRange";
import getReceipts from "@salesforce/apexContinuation/ROCController.getReceipts";
import getChecksbyOpportunityId from "@salesforce/apexContinuation/ROCController.getChecksbyOpportunityId";
import getPaymentsbyCheckID from "@salesforce/apexContinuation/ROCController.getPaymentsbyCheckID";
import getSobjectListResults from "@salesforce/apexContinuation/MultiSelectLookupController.getSobjectListResults";
import UpdateSobjectList from "@salesforce/apexContinuation/MultiSelectLookupController.UpdateSobjectList";
import getCategoryType from "@salesforce/apexContinuation/ROCController.getCategoryType";
import uploadFile from '@salesforce/apexContinuation/StaffAndOperationsController.uploadFile';
import cloneROC from '@salesforce/apexContinuation/ROCController.cloneROC';
import deleteROC from '@salesforce/apexContinuation/ROCController.deleteROC';
import getFileName from '@salesforce/apexContinuation/StaffAndOperationsController.getFileName';
import getDatefromAccountingPeriod from '@salesforce/apexContinuation/ROCController.getDatefromAccountingPeriod';
import payment from '@salesforce/schema/Payment__c';
import checkFundingSource from '@salesforce/apexContinuation/ROCController.checkFundingSource';
import getrecordTypeId from '@salesforce/apexContinuation/ROCController.getrecordTypeId';
import getCategoryName from '@salesforce/apexContinuation/ROCController.getCategoryName';
import checkPaidCaseAction from '@salesforce/apexContinuation/ROCController.checkPaidCaseAction';
import ACCOUNTING_PERIOD_OBJECT from '@salesforce/schema/Campaign';
import getNextReceiptNumber from "@salesforce/apexContinuation/ROCController.getNextReceiptNumber";
import { hideSpinner, showSpinner } from 'c/util';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import Opportunity from '@salesforce/schema/Opportunity';
import id from '@salesforce/user/Id';
const fileColumns = [
    {  
        label: "Title",  
        fieldName: "titleLink",  
        type: "url",  
        typeAttributes: { label: { fieldName: "title" }, tooltip:"title", target: "_blank" }  
    },
    {  
        label: "Owner",  
        fieldName: "ownerLink",  
        type: "url",  
        typeAttributes: { label: { fieldName: "owner" }, tooltip:"owner", target: "_blank" }  
    },
];


const colum = [
    { label: 'Receipt Number', fieldName: 'Report_Number__c', fixedWidth: 160 },
    
    { label: 'Date of Receipt', fieldName: 'Payment_Date__c', fixedWidth: 160,
        type: "date-local",
        typeAttributes: {
            month: "numeric",
            day: "numeric",
            year: "numeric"
        }
    },
    { label: 'Recipient', fieldName: 'Recipient__c', fixedWidth: 180 },
    {
        label: "Recipient Name",   
        fieldName: "recipient_url",
        type: "url",
        typeAttributes: {
            label: { fieldName: "RecipientName" },
            target: "_blank",
            tooltip: { fieldName: "RecipientName" }
        }
    },
   /* { label: 'Payment', fieldName: 'paymentName', fixedWidth: 120 },*/
    /*{
        label: "Vendor Invoice",   
        fieldName: "payment_url",
        type: "url",
        typeAttributes: {
            label: { fieldName: "paymentName" },
            target: "_blank",
            tooltip: { fieldName: "paymentName" }
        }
    }
    ,*/
    /*{
        label: "Vendor Refund",   
        fieldName: "vendor_url",
        type: "url",
        typeAttributes: {
            label: { fieldName: "Vendor_Refund__c" },
            target: "_blank",
            tooltip: { fieldName: "Vendor_Refund__c" }
        }
    },*/
    { label: 'Description', fieldName: 'Description__c', fixedWidth: 180 },
    { label: 'Check Number', fieldName: 'Check_Reference_Number__c'},
    { label: 'Amount', fieldName: 'Payment_Amount__c', type: 'currency', fixedWidth: 120 },
    { label: 'Funding Source', fieldName: 'Receipt_Type__c', fixedWidth: 136 },
    { label: 'Record Type', fieldName: 'RecordtypeName', fixedWidth: 120 },
    {
        label: "",
        type: "button-icon",
        typeAttributes: {
            iconName: "utility:edit",
            //label: 'Note',
            name: "editReceipt",
            title: "Edit Receipt",
            disabled: { fieldName: "isEdit" },
            value: { fieldName: "Id" }
        },
        fixedWidth: 50
    },
    {
        label: "",
        type: "button-icon",
        typeAttributes: {
            iconName: "utility:delete",
            //label: 'Note',
            name: "deleteRec",
            title: "Delete Receipt",
            disabled: false,
            value: { fieldName: "Id" }
        },
        fixedWidth: 50
    }, 
    {
        label: "",
        type: "button-icon",
        typeAttributes: {
            iconName: "utility:save",
            //label: 'Note',
            name: "clonebtn",
            title: "Clone Receipt",
            disabled: { fieldName: "isClone" },
            value: { fieldName: "Id" }
        },
        fixedWidth: 50
    },
    {
        label: "",
        type: "button-icon",
        typeAttributes: {
            iconName: "utility:preview",
            //label: 'Note',
            name: "viewdetails",
            title: "View details",
            disabled: false,
            value: { fieldName: "Id" }
        },
        fixedWidth: 50
    },
    {
        label: "",
        type: "button-icon",
        typeAttributes: {
            iconName: "utility:file",
            //label: 'Note',
            name: "attachFile",
            title: "Attach File",
            disabled: { fieldName: "isAttach" },
            value: { fieldName: "Id" }
        },
        fixedWidth: 50
    },
];

const columns = [
    { label: 'Receipt Number', fieldName: 'Report_Number__c', fixedWidth: 160 },
    
    { label: 'Date of Receipt', fieldName: 'Payment_Date__c', fixedWidth: 160,
        type: "date-local",
        typeAttributes: {
            month: "numeric",
            day: "numeric",
            year: "numeric"
        }
    },
    { label: 'Recipient', fieldName: 'Recipient__c', fixedWidth: 180 },
    {
        label: "Recipient Name",   
        fieldName: "recipient_url",
        type: "url",
        typeAttributes: {
            label: { fieldName: "RecipientName" },
            target: "_blank",
            tooltip: { fieldName: "RecipientName" }
        }
    },
   /* { label: 'Payment', fieldName: 'paymentName', fixedWidth: 120 },*/
    /*{
        label: "Vendor Invoice",   
        fieldName: "payment_url",
        type: "url",
        typeAttributes: {
            label: { fieldName: "paymentName" },
            target: "_blank",
            tooltip: { fieldName: "paymentName" }
        }
    }
    ,*/
    /*{
        label: "Vendor Refund",   
        fieldName: "vendor_url",
        type: "url",
        typeAttributes: {
            label: { fieldName: "Vendor_Refund__c" },
            target: "_blank",
            tooltip: { fieldName: "Vendor_Refund__c" }
        }
    },*/
    { label: 'Description', fieldName: 'Description__c', fixedWidth: 180 },
    { label: 'Check Number', fieldName: 'Check_Number__c'},
    { label: 'Amount', fieldName: 'Payment_Amount__c', type: 'currency', fixedWidth: 120 },
    { label: 'Funding Source', fieldName: 'Receipt_Type__c', fixedWidth: 136 },
    { label: 'Record Type', fieldName: 'RecordtypeName', fixedWidth: 120 },
    {
        label: "",
        type: "button-icon",
        typeAttributes: {
            iconName: "utility:edit",
            //label: 'Note',
            name: "editReceipt",
            title: "Edit Receipt",
            disabled: { fieldName: "isEdit" },
            value: { fieldName: "Id" }
        },
        fixedWidth: 50
    },
    {
        label: "",
        type: "button-icon",
        typeAttributes: {
            iconName: "utility:delete",
            //label: 'Note',
            name: "deleteRec",
            title: "Delete Receipt",
            disabled: false,
            value: { fieldName: "Id" }
        },
        fixedWidth: 50
    }, 
    {
        label: "",
        type: "button-icon",
        typeAttributes: {
            iconName: "utility:save",
            //label: 'Note',
            name: "clonebtn",
            title: "Clone Receipt",
            disabled: { fieldName: "isClone" },
            value: { fieldName: "Id" }
        },
        fixedWidth: 50
    },
    {
        label: "",
        type: "button-icon",
        typeAttributes: {
            iconName: "utility:preview",
            //label: 'Note',
            name: "viewdetails",
            title: "View details",
            disabled: false,
            value: { fieldName: "Id" }
        },
        fixedWidth: 50
    },
    {
        label: "",
        type: "button-icon",
        typeAttributes: {
            iconName: "utility:file",
            //label: 'Note',
            name: "attachFile",
            title: "Attach File",
            disabled: { fieldName: "isAttach" },
            value: { fieldName: "Id" }
        },
        fixedWidth: 50
    },
];

const Adjcolumn = [
    { label: 'Receipt Number', fieldName: 'Report_Number__c', fixedWidth: 160 },
    
    { label: 'Date of Receipt', fieldName: 'Payment_Date__c', fixedWidth: 160,
        type: "date-local",
        typeAttributes: {
            month: "numeric",
            day: "numeric",
            year: "numeric"
        }
    },
    { label: 'Recipient', fieldName: 'Recipient__c', fixedWidth: 180 },
    {
        label: "Recipient Name",   
        fieldName: "recipient_url",
        type: "url",
        typeAttributes: {
            label: { fieldName: "RecipientName" },
            target: "_blank",
            tooltip: { fieldName: "RecipientName" }
        }
    },
    { label: 'Description', fieldName: 'Description__c', fixedWidth: 180 },
    //{ label: 'Check Number', fieldName: 'npe01__Check_Reference_Number__c'},
    { label: 'Amount', fieldName: 'Payment_Amount__c', type: 'currency', fixedWidth: 120 },
    { label: 'Funding Source', fieldName: 'Receipt_Type__c', fixedWidth: 136 },
    { label: 'Record Type', fieldName: 'RecordtypeName', fixedWidth: 120 },
    {
        label: "",
        type: "button-icon",
        typeAttributes: {
            iconName: "utility:edit",
            //label: 'Note',
            name: "editReceipt",
            title: "Edit Receipt",
            disabled: true,
            value: { fieldName: "Id" }
        },
        fixedWidth: 50
    },
    {
        label: "",
        type: "button-icon",
        typeAttributes: {
            iconName: "utility:delete",
            //label: 'Note',
            name: "deleteRec",
            title: "Delete Receipt",
            disabled: false,
            value: { fieldName: "Id" }
        },
        fixedWidth: 50
    }, 
    {
        label: "",
        type: "button-icon",
        typeAttributes: {
            iconName: "utility:save",
            //label: 'Note',
            name: "clonebtn",
            title: "Clone Receipt",
            disabled: { fieldName: "isClone" },
            value: { fieldName: "Id" }
        },
        fixedWidth: 50
    },
    {
        label: "",
        type: "button-icon",
        typeAttributes: {
            iconName: "utility:preview",
            //label: 'Note',
            name: "viewdetails",
            title: "View details",
            disabled: false,
            value: { fieldName: "Id" }
        },
        fixedWidth: 50
    },
    {
        label: "",
        type: "button-icon",
        typeAttributes: {
            iconName: "utility:file",
            //label: 'Note',
            name: "attachFile",
            title: "Attach File",
            disabled: false,
            value: { fieldName: "Id" }
        },
        fixedWidth: 50
    },
];
const column = [
    { label: 'Receipt Number', fieldName: 'Report_Number__c', fixedWidth: 160 },
    
    { label: 'Date of Receipt', fieldName: 'Payment_Date__c', fixedWidth: 160,
        type: "date-local",
        typeAttributes: {
            month: "numeric",
            day: "numeric",
            year: "numeric"
        }
    },
    { label: 'Recipient', fieldName: 'Recipient__c', fixedWidth: 180 },
    {
        label: "Recipient Name",   
        fieldName: "recipient_url",
        type: "url",
        typeAttributes: {
            label: { fieldName: "RecipientName" },
            target: "_blank",
            tooltip: { fieldName: "RecipientName" }
        }
    },
   /* { label: 'Payment', fieldName: 'paymentName', fixedWidth: 120 },*/
    /*{
        label: "Vendor Invoice",   
        fieldName: "payment_url",
        type: "url",
        typeAttributes: {
            label: { fieldName: "paymentName" },
            target: "_blank",
            tooltip: { fieldName: "paymentName" }
        }
    }
    ,*/
    /*{
        label: "Vendor Refund",   
        fieldName: "vendor_url",
        type: "url",
        typeAttributes: {
            label: { fieldName: "Vendor_Refund__c" },
            target: "_blank",
            tooltip: { fieldName: "Vendor_Refund__c" }
        }
    },*/
    { label: 'Description', fieldName: 'Description__c', fixedWidth: 180 },
    //{ label: 'Check Number', fieldName: 'npe01__Check_Reference_Number__c'},
    { label: 'Amount', fieldName: 'Payment_Amount__c', type: 'currency', fixedWidth: 120 },
    { label: 'Funding Source', fieldName: 'Receipt_Type__c', fixedWidth: 136 },
    { label: 'Record Type', fieldName: 'RecordtypeName', fixedWidth: 120 },
    {
        label: "",
        type: "button-icon",
        typeAttributes: {
            iconName: "utility:edit",
            //label: 'Note',
            name: "editReceipt",
            title: "Edit Receipt",
            disabled: { fieldName: "isEdit" },
            value: { fieldName: "Id" }
        },
        fixedWidth: 50
    },
    {
        label: "",
        type: "button-icon",
        typeAttributes: {
            iconName: "utility:delete",
            //label: 'Note',
            name: "deleteRec",
            title: "Delete Receipt",
            disabled: false,
            value: { fieldName: "Id" }
        },
        fixedWidth: 50
    }, 
    {
        label: "",
        type: "button-icon",
        typeAttributes: {
            iconName: "utility:save",
            //label: 'Note',
            name: "clonebtn",
            title: "Clone Receipt",
            disabled: { fieldName: "isClone" },
            value: { fieldName: "Id" }
        },
        fixedWidth: 50
    },
    {
        label: "",
        type: "button-icon",
        typeAttributes: {
            iconName: "utility:preview",
            //label: 'Note',
            name: "viewdetails",
            title: "View details",
            disabled: false,
            value: { fieldName: "Id" }
        },
        fixedWidth: 50
    },
    {
        label: "",
        type: "button-icon",
        typeAttributes: {
            iconName: "utility:file",
            //label: 'Note',
            name: "attachFile",
            title: "Attach File",
            disabled: { fieldName: "isAttach" },
            value: { fieldName: "Id" }
        },
        fixedWidth: 50
    },
];


const Reccolumns = [
    
    { label: 'Vendor', fieldName: 'Vendor_Name__c' ,fixedWidth: 160},
    { label: 'Check #', fieldName: 'Check_Number__c',fixedWidth: 110},
    { label: 'Payment Date', fieldName: 'Payment_Date__c',fixedWidth: 126 },
    { label: 'Category', fieldName: 'Category_Name__c' ,fixedWidth: 96,},    
    { label: 'Case', fieldName: 'Case_Name__c' ,fixedWidth: 160},
    { label: 'Payment Amount', fieldName: 'Payment_Amount__c', type: 'currency', fixedWidth: 150},
    { label: 'Prior Refund', fieldName: 'Prior_Refund1__c' ,type: 'currency', fixedWidth: 120},
    { label: 'Balance Remaining', fieldName: 'Balance_Remaining__c',type: 'currency', fixedWidth: 156 },
    { label: 'Refund Amount', fieldName: 'Refund_Amount__c',fixedWidth: 145,type: 'currency', editable: {fieldName: 'isEditAmount'}, cellAttributes :{class:'slds-cell-edit slds-is-edited'} },
    { label: 'Refund Category', fieldName: 'categoryName',fixedWidth: 145 },
    {
        label: "",
        type: "button-icon",
        typeAttributes: {
            iconName: "utility:edit",
            //label: 'Note',
            name: "editVendorRefund",
            title: "Edit Vendor Refund",
            disabled: { fieldName: "isCategoryNull" },
            value: { fieldName: "Id" }
        },
        fixedWidth: 50
    },
];

export default class RecordFormEditExample extends NavigationMixin(LightningElement) {
    // The record page provides recordId and objectApiName
    @api recordId;
    @api objectApiName;
    @track sfdcBaseURL = window.location.origin;
    colum = colum;
    columns = columns;
    Adjcolumn = Adjcolumn;
    column = column;
    Reccolumns = Reccolumns;
    fileColumns = fileColumns;
    @track stageLabel = 'Open';
    @track payerValue = 'Vendor';
    @track nameROC = '';
    @track isRecordCreated = false;
    @track isRecepit = false;
    @track isAddRecepit = true;
    @track displayType = 'edit';
    @track reportId;
    @track ROCStage;
    @track isDelete = true;
    @track isReopen = false;
    @track isModalOpen = false;
    @track isGAUModalOpen = false;
    @track relatetReceipt;
    @track relatetSWR;
    @track relatetAdjustment;
    @track selectedPaymentMethod = '';
    @track relatetvRefund;
    @track selectedReceipt;
    @track differ='';
    @track toastIcon = 'utility:error';
    @track AllRelatetReceipt;
    @track rectYPEvalue = 'Receipt';
    @track childFiscalYearId = '';
    @track disableCheck = false;
    @track disableVendor = false;
    @track disableCase = false;
    @track disableFiscalYear = false;
    @track vendorId = '';
    @track caseId = '';
    @track parentFiscalYear = '';
    @track sparentFiscalYear = '';
    @track vparentFiscalYear = '';
    @track closeDate = '';
    @track beginningReceipt = 'N/A';
    @track endDate = '';
    @track reportCollection='';
    @track markCompleteDisabled = true;
    @track receiptName='';
    @track endingReceipt = 'N/A';
    @track balanceAmount='';
    @track totalReceipts='';
    @track amount=0;
    @track parentFisaclYearName='';
    @track CampaignName ='';
    @track CampaignId = '';
    @track status = '';
    @track accounting_period_status = '';
    @track chequeId = '';
    @track receiptVendorOption = true;
    @track receiptOption = false;
    @track SWROption = false;
    @track adjustmentOption = false;
    @track vendorOption = false;
    @track payStage ;
    @track showCheckReceipts = false;
    @api isUpdate;
    @track CheckReceiptList = false;
    @track formtitle  = 'Add Receipt / Refund';
    isModalClose = false;
    @track refundDescription = '';
    whereStr ='';
    isView = true;

    recordTypeIdStr;
    recordTypeIdForPayment;
    get RECTYPEoptions() {
        return [
            { label: 'Receipt', value: 'Receipt' },
            { label: 'Vendor Refund', value: 'Vendor Refund' },
            { label: 'Special Welfare Reimbursement', value: 'Special Welfare Reimbursement' },
            { label: 'Adjustments', value: 'Receipt_Adjustments' },
        ];
    }
    @track checkvalue = '';
    @track checkoptions = [];
    get checkoptionsFunc() {
        return [
            { label: 'New', value: 'new' },
            { label: 'In Progress', value: 'inProgress' },
            { label: 'Finished', value: 'finished' },
        ];
    }
    postingPeriodValue = '';


    fields = [begindate_FIELD, Enddate_FIELD, bgrept_FIELD, Endrept_FIELD, FiscalYear_FIELD, Refernce_FIELD, TotalAmount_FIELD, /*FIPSCode,  Stage_FIELD,*/ RECEIPT_AMOUNT_FIELD];
    @track manageURL = this.sfdcBaseURL + '/apex/npsp__ALLO_ManageAllocations?id=';
    @track rocURL = this.sfdcBaseURL + '/apex/Report_Collection?id=';
    @track AllCheckReceipts = [];
    VRRecorTypeId;
    @wire(getObjectInfo, { objectApiName: payment })
    getObjectdata1({ error, data }) {
        if (data) {
            console.log('data.defaultRecordTypeId ' + data.defaultRecordTypeId);
            console.log('data.defaultRecordTypeId ', data.recordTypeInfos);
            for (var key in data.recordTypeInfos) {
                if (data.recordTypeInfos[key].name == "Vendor Refund") {
                    this.VRRecorTypeId = data.recordTypeInfos[key].recordTypeId;
                }
            }
            // perform your custom logic here
        } else if (error) {
            // perform your logic related to error 
        }
    };
    
    @wire(getObjectInfo, { objectApiName: Opportunity })
    getObjectdata({ error, data }) {
        if (data) {
            console.log('data.defaultRecordTypeId ' + data.defaultRecordTypeId);
            console.log('data.defaultRecordTypeId ', data.recordTypeInfos);
            for (var key in data.recordTypeInfos) {
                if (data.recordTypeInfos[key].name == "Report of Collections") {
                    this.recordTypeIdStr = data.recordTypeInfos[key].recordTypeId;
                }
            }
            console.log('recordTypeIdStr ', this.recordTypeIdStr);
            // perform your custom logic here
        } else if (error) {
            // perform your logic related to error 
        }
    };
    @wire(getObjectInfo, { objectApiName: ACCOUNTING_PERIOD_OBJECT })
    Function({ error, data }) {
        if (data) {
            console.log('Data ', data);
            for (var key in data.recordTypeInfos) {
                console.log(data.recordTypeInfos[key].name + ' data.recordTypeInfos[key].name???');

                if (data.recordTypeInfos[key].name == "Fiscal Year") {

                    this.apRTId = data.recordTypeInfos[key].recordTypeId;
                    this.whereStr = " AND RecordTypeId = '" + this.apRTId + "' AND IsActive = true ";
                    console.log('WhereStr>>>>>>>>>', this.whereStr);
                }
            }
            // perform your custom logic here
        } else if (error) {
            // perform your logic related to error 
        }
    };

    dateChange(event)
    {
        this.closedate = event.target.value;
    }

    connectedCallback() {
        console.log(this.rocURL + ' this.rocURL???');
        
        if (this.recordId != null && this.recordId != '') {
            this.isRecordCreated = true;
            this.reportId = this.recordId;
            this.displayType = 'view';
            this.isView = true;
           
            getROCRecord({ ROCID: this.recordId }).then((result) => {
                    if (result) {
                        this.ROCCreatedDate = result.CreatedDate;
                        this.reportCollection = result.Total_Receipts__c;
                        this.receiptName = result.Amount;

                        console.log('Total Rec?????????????', result.Total_Receipts__c)
                        if(result.Total_Receipts__c < result.Amount){
                            console.log('Total Rec?????????????', result.Total_Receipts__c)
                            this.balanceAmount = result.Total_Receipts__c;
                        }
                        else if(result.Total_Receipts__c == 0)
                        {
                            this.balanceAmount = 0;
                        }
                        console.log('Basement?????????????', this.balanceAmount);
                        this.totalReceipts = result.Total_Receipts__c;

                        if(result.FIPS_Code__c){
                            this.fipsCode = result.FIPS_Code__c;
                        }
                        if(result.Posting_Period__c){
                            this.postingPeriod = result.Posting_Period__c;
                        }
                        this.postingPeriod;
                      
                        let newsObject = [{ 'recId' : result.CampaignId ,'recName' : result.Campaign.Name }];
                        this.parentFiscalYear = result.CampaignId;
                        this.sparentFiscalYear = result.CampaignId;
    
                        this.parentFisaclYearName = JSON.stringify(newsObject);
                        this.accounting_period_status = result.Campaign.Status;
                        //console.log('Accounting period Status????',this.accounting_period_status);
                        this.closeDate = result.CloseDate;
                    
                        if(result.Accounting_Period__r){
                            this.status = result.Accounting_Period__r.Status;
                        }
                        if(result.Beginning_Receipt__c){
                            this.beginningReceipt = result.Beginning_Receipt__c;
                            this.recnumber = this.Report_Number__c;
                        }
                        else{
                            this.beginningReceipt = 'N/A';
                            this.recnumber = null;
                        }
                        if(result.Report_Number__c)
                        {
                            console.log('ReportNumber???????',result.Report_Number__c);
                            this.recnumber = this.Report_Number__c;
                        }
                        else{
                            this.recnumber = this.Report_Number__c;
                        }
                        if(result.Ending_Receipt__c){
                            this.endingReceipt = result.Ending_Receipt__c;
                        }
                        else{
                            this.endingReceipt = 'N/A';
                        }
                        this.endDate = result.End_Date__c;
                       

                        console.log('totalReceipt?????????????',this.totalReceipts);
                        console.log('amount???????????',this.receiptName);
                        this.stageLabel = result.StageName;
                        this.nameROC = result.Name;
                        console.log('this.nameROC>>>>>' + this.nameROC);
                        console.log('this.stageLabel>>>>>' + this.stageLabel);
                        if (this.stageLabel == 'Completed') {
                            console.log('In Complete>>>>>>>>>>>>>>>');
                            this.ROCStage = true;
                            this.isReopen = true;
                            this.isDelete = false;
                            this.displayType = 'readonly';
                            this.isAddRecepit = false;
                            this.markCompleteDisabled = false;
                        } else if (this.stageLabel == 'Posted') {
                            console.log('In Posted>>>>>>>>>>>>>>>');
                            this.ROCStage = true;
                            this.isDelete = false;
                            this.displayType = 'readonly';
                            this.isAddRecepit = false;
                            
                            this.markCompleteDisabled = false;
                        } else {
                            console.log('In Else>>>>>>>>>>>>>>>');
                            this.ROCStage = false;
                            this.markCompleteDisabled = true;
                        }
                        console.log('this.ROCStage>>>>>' + this.ROCStage);


                    }
                })
                .catch((error) => {
                    //console.log(error);
                });
                
            //this.stageLabel = getFieldValue(this.ROC.data, Stage_FIELD);
            this.getReceiptNumber();
            this.getReceiptsFromROC();
            this.getAdjustmentsFromROC();
            this.getvRefundFromROC();
            this.getSWRFromROC();
            this.getReceipts();
            this.getPaymentsbyCheckID();
            this.getChecksbyOpportunityId();
        
        }else{
            this.isView = false;
        }
    }
    getReceiptNumber(){
        
        getNextReceiptNumber({ rocId: this.recordId}).then((result) => {
            if (result) {
                if(result.length > 0){
                    result.forEach(ele => {
                       
                        this.recnumber = parseInt(ele.Report_Number__c) + 1;
                        this.recnumber = this.recnumber.toString();
                        console('Inget receipt?????????????????', ele.Report_Number__c);
                         
                    });
                    
                }
                else
                {
                    this.recnumber = this.beginningReceipt;
                }
            }
       })
       .catch((error) => {
           //console.log(error);
       });
    }
    getPaymentsbyCheckID() {
        console.log(this.checkvalue + 'checkvalue ????')
        getPaymentsbyCheckID({ ROCID: this.recordId, Checkid: this.checkvalue }).then((result) => {
                if (result) {
                    console.log(this.result + ' this.result getPaymentsbyCheckID???');
                    console.log('result ', result);
                    if(result.length > 0){
                        result.forEach(element => {
                            var isDecimal = (element.Payment_Amount__c - Math.floor(element.Payment_Amount__c)) !== 0; 
                            if (isDecimal)
                                element.Payment_Amount__c = parseFloat(element.Payment_Amount__c).toFixed(2);
                        

                        });
                        this.AllCheckReceipts = result;
                        this.showCheckReceipts = true;
                    }else {
                        this.AllCheckReceipts = [];
                        this.showCheckReceipts = false;
                    }
                    
                } else {
                    this.AllCheckReceipts = [];
                    this.showCheckReceipts = false;
                }
            })
            .catch((error) => {
                //console.log(error);
            });
    }
    getChecksbyOpportunityId() {
        console.log(this.recordId + ' this.recordId???');
        getChecksbyOpportunityId({ ROCID: this.recordId }).then((result) => {
                if (result) {
                    console.log(this.result + ' this.result getChecksbyOpportunityId???');
                    console.log('result ', result);
                    
                    //this.checkoptions
                    this.checkoptions = [];
                    result.forEach(e => { 
                        console.log(e.Check_Number__c + 'e.Check_Number__c ()');
                        this.checkoptions.push({ label: e.Check_Number__c, value: e.Kinship_Check__c });
                    });

                } else {

                }
                this.refreshROC();
            })
            .catch((error) => {
                //console.log(error);
            });
    }
    checkhandleChange(event) {
        this.checkvalue = event.detail.value;
        this.getPaymentsbyCheckID();
        console.log(this.checkvalue + ' check value in checkhandleChange ??')
    }
    openNewROC(event){
        window.open('/lightning/n/Create_Report_of_Collections');
    }
    getReceipts() {
        console.log(this.recordId + ' this.recordId???');
        getReceipts({ ROCID: this.recordId }).then((result) => {
                if (result) {
                    console.log(this.result + ' this.result getReceipts???');
                    console.log('result ', result);
                    result.forEach(e => { 
                        this.checkoptions.push({ label: e.Check_Number__c, value: e.Kinship_Check__c });
                    });
                    this.AllRelatetReceipt = result;
                } else {
                    this.AllRelatetReceipt = [];
                }
            })
            .catch((error) => {
                //console.log(error);
            });
    }
   
    handleSubmit(event) {
        event.preventDefault(); // stop the form from submitting
        const fields = event.detail.fields;
                this.showLoader = true;
        fields.Name = 'ROC';
        
        fields.npe01__Do_Not_Automatically_Create_Payment__c = true;
        if (fields.End_Date__c == null) {
            fields.End_Date__c = '';
            // this.toastManager('error', 'Error', 'End Date is required.');
            // event.preventDefault();
            // this.showLoader = false;
            // return true;
        }
        else{
            this.endDate = fields.End_Date__c ;
        }
        
        /*if (fields.CampaignId == null || fields.CampaignId == '') {
            this.toastManager('error', 'Error', 'Parent Fiscal Year is required.');
            event.preventDefault();
            this.showLoader = false;
            return true;
        }*/
        console.log('fiscal?????????', this.parentFiscalYear);
        // if (this.parentFiscalYear == null || this.parentFiscalYear == '') {
        //     this.toastManager('error', 'Error', 'Fiscal Year is required.');
        //     event.preventDefault();
        //     this.showLoader = false;
        //     return true;
        // }
        //else{
            fields.CampaignId = this.parentFiscalYear;
            this.sparentFiscalYear = this.parentFiscalYear;
        //}
        // if(this.postingPeriodValue == ''){
        //     this.toastManager('error', 'Error', 'Posting Period is required.');
        //     event.preventDefault();
        //     this.showLoader = false;
        //     return true;
        // }
        //else{
            fields.Posting_Period__c = this.postingPeriodValue;
        //}
        if (fields.Beginning_Receipt__c == null  || fields.Beginning_Receipt__c == '') {
            this.beginningReceipt = 'N/A';
            fields.Beginning_Receipt__c = '';
            /*this.toastManager('error', 'Error', 'Begin Receipt # is required.');
            event.preventDefault();
            this.showLoader = false;
            return;*/
        }
        else{
            this.beginningReceipt = fields.Beginning_Receipt__c;
        }
       if (fields.Ending_Receipt__c == null  || fields.Ending_Receipt__c == '') {
        this.endingReceipt = 'N/A';
        fields.Ending_Receipt__c = '';
          /*  this.toastManager('error', 'Error', 'End Receipt # is required.');
            event.preventDefault();
            this.showLoader = false;
            return;*/
        }
        else{
            this.endingReceipt = fields.Ending_Receipt__c;
        } 
        if (fields.CloseDate != null  && fields.CloseDate != '') {
            this.closeDate = fields.CloseDate; 
        }     
        if (fields.Amount == null || fields.Amount == '') {
            this.toastManager('error', 'Error', 'Amount is required.');
            event.preventDefault();
            this.showLoader = false;
            return;
        }

        fields.RecordTypeId = this.recordTypeIdStr;
        if (this.reportId == null) {
            fields.StageName = 'Open';
        }
        console.log(fields.StageName + ' fields.StageName????');
        this.ROCStage = fields.StageName;
        if (this.ROCStage == "Completed" || this.ROCStage == "Posted") {
            this.ROCStage = true;
        } else {
            this.ROCStage = false;
        }
        console.log('this.ROCStage>>>>>' + this.ROCStage);
        //fields.LastName = 'My Custom Last Name'; // modify a field
        this.isUpdate = false;
        this.template.querySelector('lightning-record-edit-form').submit(fields);
        /*
        if (this.recordId) {s
           // this.checkReceiptRangeHandle(fields);
        } else {
          
        }*/
        if(this.childFiscalYearId == null) {
            this.childFiscalYearId = fields.Fiscal_Year_fr__c;
        }
        if(this.totalReceipts < fields.Amount){
            this.balanceAmount = this.totalReceipts;
        }
        else{
            this.balanceAmount = 0;
        }
        

    }
    balanceAmountChange(event){
        var balanceAmount = event.detail;
        console.log('In balanceAmountChange target>>>>>>>>>>>>>>>', balanceAmount);
        this.balanceAmount = balanceAmount;
        console.log('In balanceAmountChange>>>>>>>>>>>>>>>', this.balanceAmount);
    }
    checkReceiptRangeHandle(fields) {

        checkReceiptRange({ startRange: fields.Beginning_Receipt__c, endRange: fields.Ending_Receipt__c, ROCID: this.recordId }).then((result) => {
                if (result == 'Allowed') {
                    console.log('checkReceiptRange in if condition');
                    this.template.querySelector('lightning-record-edit-form').submit(fields);
                } else {
                    console.log('checkReceiptRange in else condition');
                    this.toastManager('error', 'Receipt range is not applicabale as there are already some receipts added within this range.', '');
                }
                console.log("checkReceiptRange");
            })
            .catch((error) => {
                console.log(error);
            });
    }
    handleSuccess(event) {
        //event.preventDefault(); // stop the form from submitting
        this.reportId = event.detail.id;
        this.recordId = this.reportId;
        this.isRecordCreated = true;
        //this.displayType = 'readonly';
        this.isView = true;
        this.showLoader = false;
        //fields.LastName = 'My Custom Last Name'; // modify a field  
    }
    handleerror(event){
        this.showLoader = false;
        var errorMessage = event.detail.detail;
        if(errorMessage.includes('Begin/End dates should be within Accounting Period Start/End dates')){
            this.toastManager('error', 'Error', 'Begin/End dates should be within Fiscal Year Start/End dates');
        }
        else{
            // this.toastManager('', errorMessage, '');
        }
        console.log('POSTED ERROR 1>>>>>>>>>', errorMessage);
    }
    handleClick() {
        //this.receiptVendorOption = true;
        console.log('accounting_period_status', this.accounting_period_status);
        if(this.accounting_period_status != 'Posted')
        {
        
        this.isRecepit = true;
        const tempId = this.recordId;
        this.selectedReceipt = '';
        this.pageViewHander(1);
        
        //this.recordId = null;
        this.isModalOpen = true;
        this.isUpdate = false;
        this.getReceiptNumber();
        //this.recnumber = null;
        this.chknumber = null;
        this.vparentFiscalYear = null;
        this.receiptType  = null;
        this.doreceipt = this.closeDate;
        //this.payMethod  = null;
        this.refundDescription = null;
        this.selectedReceiptRTID = '';
        this.selectedReceiptRTName = '';
        this.selectedReceiptType = '';
        /*this.selectedReceipt = null;
        setTimeout(function() {
            //this.recordId = tempId;
            this.template.querySelector("c-add-receipt").setIsUpdate(true);
        }.bind(this), 1000);*/
    }
    else{
        this.toastManager('info', 'Accounting Period is Posted, Receipt Cannot be created.', '');
    }
    }
    pageViewHander(pageNo){
        this.getReceiptNumber();
        this.receiptVendorOption = false;
        this.CheckReceiptList = false;
        this.CreateNewPayment = false;
        this.categoryRequired = false;
        this.categoryisIVE = false;
        this.categoryisnotIVE = false;
        this.receiptOption = false;
        this.SWROption = false;
        this.adjustmentOption = false;
        this.vendorOption = false;
        this.checkvalue = ''; 
        this.fromAmount = '';
        this.paymentNumber = '';
        this.disableCat = false;
        this.caseId = '';
        this.kinshipCaseName = '';
        this.accountName = '';
        this.kinshipCheckName = '';
        this.catRecName = '';
        this.catName = '';
        this.categoryName = '';
        this.catId = '';
        this.costCenterId  = '';
        this.costCenterName = '';
        if(pageNo == 1){
            
            this.receiptVendorOption = true;
            this.rectYPEvalue = "Receipt";
            
        }else if(pageNo == 2){
            //this.recnumber= '';
            this.chknumber= '';
            this.vparentFiscalYear = '';
            this.disableFiscalYear = false;
            this.doreceipt= this.closeDate;
            this.payMethod = 'Check';
            this.refundDescription = '';
            this.AllCheckReceipts = [];
            this.showCheckReceipts = false;
            this.disableVendor = false;
            this.disableCheck = false;
            this.disableCase = false;
            this.CheckReceiptList = true;
        
        }else if(pageNo == 3){
            this.receiptOption = true;
            this.selectedPaymentAmount = '';
            this.selectedPaymentMethod = '';
            this.CampaignId = '';
            this.CampaignName = ''; 
            this.formtitle = 'Add Receipt';
        }else if(pageNo == 4){
            this.vendorOption = true;
            this.formtitle = 'Add Vendor Refund';
        }else if(pageNo == 5){
            this.SWROption = true;
        }else if(pageNo == 6)
        {
            this.adjustmentOption = true;
            this.formtitle = 'Add Adjustment';
        }
        this.payMethod = '';
    }
    Back1(){
        this.formtitle  = 'Add Receipt / Refund';
        this.pageViewHander(1);
    }
    
    typeOptionChange(event){
        this.rectYPEvalue = event.target.value;
    }
    
    NextOptions() {
        // to close modal set isModalOpen tarck value as false
        if(this.status != "Posted")
         {   
             
            if(this.rectYPEvalue == "Receipt"){
                this.formtitle = 'Add Receipt';    
                this.pageViewHander(3);
            
            }
        }
        if(this.status == "Posted")
        {
            this.toastManager('info', 'Receipt status is posted , Receipts can not be modified.', '');
        }
        if(this.rectYPEvalue == "Vendor Refund"){
            this.pageViewHander(2);
            this.formtitle = 'Add Vendor Refund';
        }
        if(this.rectYPEvalue == "Special Welfare Reimbursement"){
            this.pageViewHander(5);
            this.formtitle = 'Add Special Welfare Reimbursement';
        }
        if(this.rectYPEvalue == "Receipt_Adjustments")
        {
            this.pageViewHander(6);
            this.formtitle = 'Adjustments';
        }
            
    }
    NextOptions2(){
        this.pageViewHander(4);

    }
    markOpenStageHandle(event) {
        checkPaidCaseAction({ ROCId: this.reportId }).then((result) => {
            if (result == true) {
                const event = new ShowToastEvent({
                    title: 'Error',
                    variant: 'error',
                    message: 'Cant reopen it as it has paid case action.',
                });
                this.dispatchEvent(event);
            } else if(result == false) {
                
                this.stageLabel = 'Open';
                this.dispatchEvent(event);
                this.toastManager('success', 'The Report of Collection has been opened.', '');
                this.refreshROC();
                this.ROCStage = false;
                console.log('this.ROCStage>>>>>' + this.ROCStage);
                this.getReceiptsFromROC();
                this.getvRefundFromROC();
                this.getSWRFromROC();
                this.getAdjustmentsFromROC();
                this.displayType = 'view';
                this.markCompleteDisabled = true;
                this.isAddRecepit = true;
            }
        })
        .catch((error) => {
            //console.log(error);
        });

        
        /*markOpenStage({ ROCID: this.reportId }).then((result) => {
                if (result) {
                    const event = new ShowToastEvent({
                        title: 'Record updated',
                        variant: 'success',
                        message: '',
                    });
                    this.stageLabel = 'Open';
                    //this.dispatchEvent(event);
                    this.toastManager('success', 'The Report of Collection has been opened.', '');
                    this.ROCStage = false;
                    this.getReceiptsFromROC();
                    this.displayType = 'view';
                    this.isAddRecepit = true;
                } else {

                }
            })
            .catch((error) => {
                //console.log(error);
            });*/

    }
    
    dateChange(event)
    {
        this.closedate = event.target.value;
    }
    isCloseModal(event){
        console.log('')
        this.isModalClose = true;
        var isNext = event.detail;
        if(isNext){
            this.Back1();
        }else{
            this.closeModal();     
        }
    }
    closeModal(event)  {
            if(this.receiptOption && this.isModalClose ){
                getROCRecord({ ROCID: this.recordId }).then((result) => {
                    if (result) {
                        this.receiptName = result.Amount;
                        this.totalReceipts = result.Total_Receipts__c;
                        this.differ = this.totalReceipts - this.receiptName ;
                        if(this.receiptName >= this.totalReceipts)
                        {
                            this.isModalOpen = false;
                            this.isModalClose = false;
                            this.toastManager('success', 'Receipt has been created successfully');
                            
                        }
                        else
                        {
                            this.isModalOpen = false;
                            this.isModalClose = false;
                            this.toastManager('warning', 'Warning','Total Receipt: '+ this.totalReceipts + ' - ' + ' Total Report of Collection: '+ this.receiptName + ' = ' + this.differ );
                            
                            
                        }
                        
                    }
                })
            }
            else if(this.SWROption && this.isModalClose){
                this.toastManager('success', 'Special Welfare Reimbursement has been created successfully');
                this.isModalClose = false;
            }
            setTimeout(function(){
                    this.isModalOpen = false;
                    this.disableVendor = false;
                    this.showCreateNewButton = false;
                    this.disableCheck = false;
                    this.disableCase = false;
                    this.getReceiptsFromROC();
                    this.getvRefundFromROC();
                    this.getSWRFromROC();
                    this.getAdjustmentsFromROC();
                    this.AllCheckReceipts = [];       
                    this.getChecksbyOpportunityId();
                    for(var i=1 ; i<5; i++){
                    setTimeout(function(){
                    this.refreshROC();
                    }.bind(this),i*1000);

                    }
                    this.formtitle  = 'Add Receipt / Refund';
                    this.rectYPEvalue = "Receipt";
                }.bind(this),2000);
        
    }
    closeAllocationModal() {
        // to close modal set isGAUModalOpen tarck value as false
        this.isGAUModalOpen = false;
    }
    submitDetails() {
        // to close modal set isModalOpen tarck value as false
        //Add your code to call apex method or do some processing
        this.isModalOpen = false;
        this.getReceiptsFromROC();
        this.getvRefundFromROC();
        this.getSWRFromROC();
        this.getAdjustmentsFromROC();
    }

    handleSectionToggle(event) {
        const openSections = event.detail.openSections;
    }
   
    refundDescriptionHandle(event){
        this.refundDescription = event.detail.value;
    }
    getReceiptsFromROC() {
        getReceiptsFromROC({ ROCID: this.reportId }).then((result) => {
                if (result) {
                    this.assignValue(result,'Receipt');
                    /*result.forEach(ele => {
                        if(ele.Recipient__c == 'Vendor'){
                            if(ele.Vendor__r){
                                ele.RecipientName = ele.Vendor__r.Name;
                                ele.recipient_url = '/' + ele.Vendor__c;
                            }
                            
                        }
                        else if(ele.Recipient__c == 'Case'){
                            if(ele.Kinship_Case__r){
                                ele.RecipientName = ele.Kinship_Case__r.Name;
                                ele.recipient_url = '/' + ele.Kinship_Case__c;
                            }
                            
                        }
                        else if(ele.Recipient__c == 'Claim'){
                            if( ele.Claims__r){
                                ele.RecipientName = ele.Claims__r.Name;
                                ele.recipient_url = '/' + ele.Claims__c;
                            }
                            
                        }
                        else if(ele.Recipient__c == 'Special Welfare Account'){
                            if(ele.Special_Welfare_Account__r){
                                ele.RecipientName = ele.Special_Welfare_Account__r.Name;
                                ele.recipient_url = '/' + ele.Special_Welfare_Account__c;
                            }
                            
                        }
                        ele.RecordtypeName = ele.RecordType.Name;
                        if(ele.RecordtypeName == 'Special Welfare Reimbursement' || ele.RecordtypeName == 'Vendor Refund'){
                            ele.isEdit = true;
                        }else{
                            ele.isEdit = false;
                        }
                        
                        if(ele.RecordtypeName == "Receipt"){
                            if(this.stageLabel == "Completed"){
                                ele.isClone = true;
                            }
                            else{
                                ele.isClone = false;
                            }
                             
                        }else{
                            ele.isClone = true;
                        }
                        if(this.stageLabel == "Completed"){
                            ele.isAttach = true; 
                        }else{
                            ele.isAttach = false;
                        }
                        console.log('Payment???????????', ele.Payment__c);
                        if(ele.Payment__c != undefined && ele.Payment__r){
                            ele.paymentName = ele.Payment__r.Name;
                        }else{
                            ele.paymentName = ' ';
                        }
                        if(ele.Payment__c != undefined && ele.Payment__c != ''){
                            ele.payment_url = '/' + ele.Payment__c;
                        } else{
                            ele.payment_url = '#';
                            ele.Payment__c = ' ';
                            
                        }
                        if(ele.Id != undefined && ele.Id != ''){
                            ele.receipt_url = '/' + ele.Id;
                        } else{
                            ele.receipt_url = '#';                         
                        }
                        if(ele.Invoices_Status__c == 'Posted'){
                            this.ROCStage = false;
                            console.log('this.ROCStage>>>>>' + this.ROCStage);
                        }
                    });
                    this.relatetReceipt = result;*/
                } else {
                    this.relatetReceipt = [];
                    
                }
                console.log('this.relatetReceipt',this.relatetReceipt)
            })
            .catch((error) => {
                //console.log(error);
            });
    }
    getAdjustmentsFromROC() {
        getAdjustmentsFromROC({ ROCID: this.reportId }).then((result) => {
                if (result) {
                    this.assignValue(result,'Adjustments');
                    
                } else {
                    
                    this.relatetAdjustment = [];
                }
                console.log('this.Adjustments',this.relatetAdjustment)
            })
            .catch((error) => {
                //console.log(error);
            });
    }
    getvRefundFromROC() {
        getvRefundFromROC({ ROCID: this.reportId }).then((result) => {
                if (result) {
                    this.assignValue(result,'vRefund');
                    
                } else {
                    
                    this.relatetvRefund = [];
                }
                console.log('this.relatetvRefund',this.relatetvRefund)
            })
            .catch((error) => {
                //console.log(error);
            });
    }

    getSWRFromROC() {
        getSWRFromROC({ ROCID: this.reportId }).then((result) => {
                if (result) {
                    this.assignValue(result,'SWR');
                   
                } else {
                    this.relatetSWR = [];
                }
                console.log('this.relatetReceipt',this.relatetReceipt)
            })
            .catch((error) => {
                //console.log(error);
            });
    }
    
    assignValue(result,check){
        result.forEach(ele => {
            if(ele.Recipient__c == 'Vendor'){
                if(ele.Vendor__r){
                    ele.RecipientName = ele.Vendor__r.Name;
                    ele.recipient_url = '/' + ele.Vendor__c;
                }
                
            }
            else if(ele.Recipient__c == 'Case'){
                if(ele.Kinship_Case__r){
                    ele.RecipientName = ele.Kinship_Case__r.Name;
                    ele.recipient_url = '/' + ele.Kinship_Case__c;
                }
                
            }
            else if(ele.Recipient__c == 'Claim'){
                if( ele.Claims__r){
                    ele.RecipientName = ele.Claims__r.Name;
                    ele.recipient_url = '/' + ele.Claims__c;
                }
                
            }
            else if(ele.Recipient__c == 'Special Welfare Account'){
                if(ele.Special_Welfare_Account__r){
                    ele.RecipientName = ele.Special_Welfare_Account__r.Name;
                    ele.recipient_url = '/' + ele.Special_Welfare_Account__c;
                }
                
            }
            ele.RecordtypeName = ele.RecordType.Name;
            if(ele.RecordtypeName == 'Special Welfare Reimbursement' || ele.RecordtypeName == 'Vendor Refund'){
                ele.isEdit = true;
            }else{
                ele.isEdit = false;
            }
            
            if(ele.RecordtypeName == "Receipt"){
                if(this.stageLabel == "Completed"){
                    ele.isClone = true;
                }
                else{
                    ele.isClone = false;
                }
                 
            }else{
                ele.isClone = true;
            }
            if(this.stageLabel == "Completed"){
                ele.isAttach = true; 
            }else{
                ele.isAttach = false;
            }
            console.log('Payment???????????', ele.Payment__c);
            if(ele.Payment__c != undefined && ele.Payment__r){
                ele.paymentName = ele.Payment__r.Name;
            }else{
                ele.paymentName = ' ';
            }
            if(ele.Payment__c != undefined && ele.Payment__c != ''){
                ele.payment_url = '/' + ele.Payment__c;
            } else{
                ele.payment_url = '#';
                ele.Payment__c = ' ';
                
            }
            if(ele.Id != undefined && ele.Id != ''){
                ele.receipt_url = '/' + ele.Id;
            } else{
                ele.receipt_url = '#';                         
            }
            if(ele.Invoices_Status__c == 'Posted'){
                this.ROCStage = true;
                console.log('this.ROCStage>>>>>' + this.ROCStage);
            }
        });
        if(check == 'Receipt'){
            this.relatetReceipt = result;
        }
        else if(check == 'SWR') {
            this.relatetSWR = result;
        }
        else if(check == 'vRefund')
        {
            this.relatetvRefund = result;
        }
        else if(check == 'Adjustments')
        {
            this.relatetAdjustment = result;
        }
        
    }
    printROC(event) {
        // if (this.isAddRecepit) {
        //     this.isAmountsEqual(function(iThis) {
        //         markCompleteStage({ ROCID: iThis.reportId }).then((result) => {
        //                 if (result) {
        //                     const event = new ShowToastEvent({
        //                         title: 'Record updated',
        //                         variant: 'success',
        //                         message: '',
        //                     });
        //                     iThis.stageLabel = 'Completed';
        //                     //iThis.dispatchEvent(event);
        //                     iThis.toastManager('success', 'The Report of Collection has been completed.', '');
        //                     iThis.rocURL = iThis.sfdcBaseURL + '/apex/Report_Collection?id=' + iThis.reportId;
        //                     console.log(iThis.rocURL + ' iThis.rocURL???');
        //                     iThis.openPrint();
        //                     iThis.ROCStage = true;
        //                     iThis.displayType = 'readonly';
        //                     iThis.isAddRecepit = false;
        //                     iThis.getReceiptsFromROC();
        //                 } else {

        //                 }
        //             })
        //             .catch((error) => {
        //                 //console.log(error);
        //             });

        //     });
        // } else {
        //     this.rocURL = this.sfdcBaseURL + '/apex/Report_Collection?id=' + this.reportId;
        //     console.log(this.rocURL + ' this.rocURL???');
        //     this.openPrint(); differ= this.totalReceipts - this.receiptName

        // }
        if(this.receiptName >= this.totalReceipts)
        {
        this.rocURL = this.sfdcBaseURL + '/apex/Report_Collection?id=' + this.reportId;
        console.log(this.rocURL + ' this.rocURL???');
        this.openPrint();
        }
        else
        {
            this.toastManager('error','Total Amounts are not equal', 'Please make sure the total amount should match with receipts amount');
        }
    }
    markCompleteStage(event) {
        this.isAmountsEqual(function(iThis) {
            if(iThis.parentFiscalYear == '' || iThis.parentFiscalYear == null)
            {
              iThis.toastManager('error', 'Fiscal Year is required.', 'Fiscal Year is required.');
                return;
            }
            else if(iThis.postingPeriod   == '' || iThis.postingPeriod  == null)
            {
                console.log('iThis.postingPeriodValue',iThis.postingPeriodValue);

                console.log('iThis.postingPeriod',iThis.postingPeriod);

                iThis.toastManager('error', 'Posting Period is required.', 'Posting Period is required click on edit icon to add Posting Period.');
                return;
            }
            else
            {
                markCompleteStage({ ROCID: iThis.reportId }).then((result) => {
                    if (result == 'true') {
                                const event = new ShowToastEvent({
                                    title: 'Record updated',
                                    variant: 'success',
                                    message: '',
                                });
                                
                                    iThis.stageLabel = 'Completed';
                                    //iThis.dispatchEvent(event);
                                    iThis.toastManager('success', 'The Report of Collection has been completed.', '');
                                    iThis.refreshROC();
                                    iThis.ROCStage = true;
                                    iThis.isReopen = true;
                                    iThis.displayType = 'readonly';
                                    iThis.isAddRecepit = false;
                                    iThis.markCompleteDisabled = false;
                                    iThis.getReceiptsFromROC();
                                
                            } 
                    else if(result == 'Completed') {
                                iThis.toastManager('info', 'The Report of Collection is already completed.', ''); 
                            }
                        
                    else
                    {
                        this.toastManager('error','Error', 'Total Report of Collection is not equal to Total Receipt');
                    }
                            console.log('this.ROCStage>>>>>' + this.ROCStage);
                    })
        
                     .catch((error) => {
                         //console.log(error);
                     });
                    }
            });
    

    }
    isAmountsEqual(callBack) {
        isAmountsEqual({ ROCID: this.reportId }).then((result) => {
            console.log()
                if (result) {
                    callBack(this);

                } else {
                    const event = new ShowToastEvent({
                        title: 'Total Amounts are not equal',
                        variant: 'error',
                        message: 'Please make sure the total amount should match with receipts amount',
                    });
                    //this.dispatchEvent(event);
                    this.toastManager('error', 'Total Amounts are not equal', 'Please make sure the total amount should match with receipts amount');
                }
            })
            .catch((error) => {
                //console.log(error);
            });

    }
    
    @track selectedReceiptRTID = '';
    @track selectedReceiptRTName = '';
    @track selectedReceiptType = '';
    @track selectedRecipient = '';
    @track selectedPayer = '';
    @track catRecName = '';
    @track catName = '';
    @track selectedcostCenterName = '';
    @track selectedcostCenterId = '';
    @track selectedcategoryName = '';
    @track selectedcatName = ''; 
    swaId = '';
    claimId  = '';
    catId='';
    costCenterId ='';
    costCenterName ='';
    caseValue = '';
    handleRowAction(event) {
        const action = event.detail.action;

        const row = event.detail.row;
        switch (action.name) {
            case "editReceipt":
                this.SWROption = false;
                if(row.RecordtypeName == 'Vendor Refund'){
                    this.toastManager('info', 'Vendor Refunds can not be modified.', '');
                    break;
                }

                if(row.Invoices_Status__c == 'Posted'){
                    this.toastManager('info', 'Invoice status is posted , Receipts can not be modified.', '');
                    break;
                }
                if (this.isAddRecepit == false) {
                    this.toastManager('info', 'The status is completed , Receipts can not be modified.', '');
                    return false;
                }
                
                this.catId = row.Category_lookup__c;
                console.log('CategoryID?????????????', this.catId);
                this.selectedReceipt = row.Id;
                this.selectedReceiptRTID = row.RecordType.Id;
                this.selectedReceiptRTName = row.RecordType.Name;
                this.selectedReceiptType = row.Receipt_Type__c; 
                this.selectedRecipient = row.Recipient__c;
                this.selectedPayer = row.Payer__c;
                console.log('SelectedPayer?????????????', this.selectedPayer);
                this.recnumber = row.Report_Number__c;
                this.caseValue = row.Kinship_Case__c;
                if(row.Special_Welfare_Account__c)
                this.swaId = row.Special_Welfare_Account__c;
                if(row.Claims__c)
                this.claimId = row.Claims__c;
                if(row.Category_lookup__c){
                    this.catRecName = row.Category_lookup__r.RecordType.Name;
                    this.catName = row.Category_lookup__r.Category_Type__c;
                    this.categoryName = row.Category_lookup__r.Name;
                    this.catId = row.Category_lookup__c;
                    //console.log('Category?????????????', this.categoryName);
                    //console.log('CatName', this.catName);
                }
                
                else{
                    this.catRecName = '';
                    this.catName = '';
                    this.categoryName = '';
                    this.catId = '';
                }
                if(row.Cost_Center__c){
                    this.costCenterId = row.Cost_Center__c;
                    this.costCenterName = row.Cost_Center__r.Name;
                    console.log('costCenterName?????????', this.costCenterName);
                }
                else{
                    this.costCenterId  = '';
                    this.costCenterName = '';
                }
                
                this.selectedPaymentAmount = row.Payment_Amount__c;
                this.selectedPaymentMethod = row.Payment_Method__c;
                if(row.Accounting_Period__c && row.Accounting_Period__c != ''){
                    this.CampaignId = row.Accounting_Period__c;
                    this.CampaignName = row.Accounting_Period__r.Name;     
                }
                console.log('catRecName>>>>>>>>', this.catRecName);
                console.log('Category>>>>>>>>', this.categoryName);
                console.log('CatName>>>>>>>', this.catName);   
                console.log('costCenterName>>>>>>>', this.costCenterName);
                this.selectedcategoryName = this.categoryName;
                this.selectedcatName = this.catName;
                this.selectedcostCenterId = this.costCenterId;
                this.selectedcostCenterName = this.costCenterName;
                this.isRecepit = true;
                this.isModalOpen = true;
                this.isUpdate = true;
                this.receiptOption  = true;
                this.adjustmentOption = false;
                this.receiptVendorOption = false;
                this.vendorOption = false;
                this.VRefundShow = false;
                this.CheckReceiptList = false;
                this.CreateNewPayment =false;
                this.categoryRequired = false;
                this.categoryisIVE = false;
                this.categoryisnotIVE = false;
                this.formtitle = 'Edit Receipt';
                break;
            case "deleteRec":
                if(row.Invoices_Status__c == 'Posted'){
                    this.toastManager('info', 'Invoice status is posted , Receipts can not be deleted.', '');
                    break;
                }
                if (this.isAddRecepit == false) {
                    this.toastManager('info', 'The status is completed , Receipts can not be deleted.', '');
                    return false;
                }
                
                this.selectedReceipt = row.Id;
                this.deleteConfirmation();
                this.getReceiptsFromROC();
                this.getvRefundFromROC();
                this.getSWRFromROC();
                this.getAdjustmentsFromROC();
                break;
            case "viewdetails":
                this.selectedReceipt = row.Id;  
                window.open("/" + row.Id);
                break;  
            case "gauallocaion":
                this.selectedReceipt = row.Id;
                const rType = row.Receipt_Type__c;
                if(rType == 'Administrative'){
                    //this.isGAUModalOpen = true;
                    this.manageShow = true;
                    this.manageURL = this.sfdcBaseURL + '/apex/npsp__ALLO_ManageAllocations?id=' + this.selectedReceipt;
                    
                }else{
                     this.toastManager('info', 'GAU Allocation is not applicable for this Receipt Type', '');
                     break;
                }
                /*setTimeout(function() {
                    this.template.querySelector("c-create-g-a-u-location").getGAULocation();
                }.bind(this), 1000);*/
                break;
            case "vendorrefund":
                this.selectedReceipt = row.Id;
                this.VRefundShow = true;
                break;    
            case "attachFile":
                this.selectedReceipt = row.Id;
                this.getFileNames(this.selectedReceipt);
                this.showFileUpload = true;
                this.clonebox = false;
                break; 
            case "clonebtn":      
                this.clonebox = true; 
                this.selectedReceipt = row.Id;
        }
    }
    fileData = [];
    getFileNames(selectedReceipt){
        getFileName({ InvoiceId: selectedReceipt }).then((result) => {
            if (result.length>0) {
                this.showFile = false;
                this.fileData = result;
                var a = '';
                if(result.length>1){
                    result.forEach(function(key) {
                        result.Title = key.ContentDocument.Title;
                        result.Owner = key.ContentDocument.Owner.Name;
                        /*if(a == ''){
                            a = key.ContentDocument.Title;
                        }
                        else{
                            a = a + ', '+key.ContentDocument.Title;
                        }*/
                        
                    });
                    //this.fileName = a;
                }
                else{
                    result.Title = result[0].ContentDocument.Title;
                    result.Owner = result[0].ContentDocument.Owner.Name;
                    //this.fileName = result[0].ContentDocument.Title;
                }
               
            }
        })
            .catch((error) => {
                this.isSelectedRecord = false;
                this.parentfiscalyearid = null;
                //console.log(error);
            });
    }
    handleCloseModal(){
        this.showFileUpload = false;
        this.addFile = false;
        this.uploadIds ='';
        this.uploadedFiles =[];
        this.fileName= [];
        this.fileData =[];
    }
    handleAddFile(){
        this.showFileUpload = false;
        uploadFile({ recordId: this.selectedReceipt,uploadIds:this.uploadIds }).then((result) => {
            console.log(result + ' ???? result?????');
        })
        .catch((error) => {
            console.log(error);
        });
        this.addFile = false;
        this.uploadIds ='';
        this.uploadedFiles =[];
        this.fileName= [];
        this.fileData = [];
    }
    @track uploadIds ='';
    @track uploadedFiles =[];
    @track fileName= [];
    handleUploadFinished(event) {
        const uploadedFile = event.detail.files;
        this.uploadedFiles.push(event.detail.files);
        for(let i = 0; i < uploadedFile.length; i++) {
            if(this.uploadIds == ''){
                this.uploadIds=uploadedFile[i].contentVersionId;
            }
            else{
                this.uploadIds=this.uploadIds+','+uploadedFile[i].contentVersionId;
            }
            this.fileName.push(uploadedFile[i].name);
        }
        this.addFile = true;
    }
    @track addFile = false;
    @track showFileUpload = false;
    @track VRefundShow = false;
    closeVRefundModal() {
        // to close modal set isGAUModalOpen tarck value as false
        this.VRefundShow = false;

    }
    
    @track manageShow = false;
    handleManage(event) {
        this.manageShow = true;
        this.manageURL = this.sfdcBaseURL + '/apex/npsp__ALLO_ManageAllocations?id=' + this.selectedReceipt;
        console.log(this.manageURL, ' this.manageURL????');
    }
    handleManageClose(event) {
        this.manageShow = false;

    }

    @track showROCDeleteConfirm = false;
    deleteROCConfirmation() {
        this.showROCDeleteConfirm = true;
    }
    // showROCDeleteConfirm(event) {
    //     this.showROCDeleteConfirm = false;
    // }

    deleteROCHandle(event) {
    
        deleteROC({ROCID: this.reportId}).then(result => {
            if(result == 'Done') {
                this.toastManager('success', 'ROC has been deleted successfully', '');
                this.navigateToObjectHome();
                this.closesROCDeleteConfirm();
            }
            // else{
            //     this.closesROCDeleteConfirm();
            //     if(result.includes('This record is related to a Check Run and can no longer be deleted')){
            //         this.toastManager('error', 'Error', 'This record is related to a Check Run and can no longer be deleted.');
            //     }
            //     else{
            //         this.toastManager('error', 'Error', result);
            //     }
            // }
        }).catch(error => {
            
        });
    }

    closesROCDeleteConfirm(event) {
        this.showROCDeleteConfirm = false;
    }

    @track showDeleteConfirm = false;
    deleteConfirmation() {
        this.showDeleteConfirm = true;
    }

    deleteReceiptHandle(event) {
        deleteReceipt({ receiptID: this.selectedReceipt }).then((result) => {
                if (result) {
                    console.log(result + ' result??????');
                    if(result.includes('Transaction journal already posted or Closed.')){
                        this.toastManager('error', 'Transaction journal already posted or Closed.', 'You cant update or delete this invoice because the Transaction journal already posted or Closed.');
                        this.closesDeleteConfirm(null);
                        return;   
                    }
                     if(result.includes('This record is related to a Check Run and can no longer be deleted')){
                         this.toastManager('error', 'Error', 'This record is related to a Check Run and can no longer be deleted.');
                         this.closesDeleteConfirm(null);
                         return;   
                     }
                    this.getReceiptsFromROC();
                    this.getvRefundFromROC();
                    this.getSWRFromROC();
                    this.getAdjustmentsFromROC();
                    this.toastManager('success', 'The receipt has been deleted.', '');
                    this.closesDeleteConfirm(null);
                    this.refreshROC();
                }
            })
            .catch((error) => {
                //console.log(error);
            });
    }
    closesDeleteConfirm(event) {
        this.showDeleteConfirm = false;
    }
    SaveandContinue(event) {
        window.open("/" + this.reportId, '_blank');

    }

    @api
    refreshROC() {
        this.template.querySelector('lightning-record-edit-form').submit();
    }
    @track isShowToast = false;
    @track toastType = '';
    @track title = '';
    @track message = '';
    toastManager(toastType, title, message) {
        this.toastType = "slds-notify slds-notify_toast ";
        if (toastType == 'error') {
            this.toastType += 'slds-theme_error';
            this.toastIcon = 'utility:error';
        } else if (toastType == 'success') {
            this.toastType += 'slds-theme_success';
            this.toastIcon = 'utility:success';
        } else if (toastType == 'info') {
            this.toastType += 'slds-theme_info';
            this.toastIcon = 'utility:info';
        }
        this.title = title;
        this.message = message;
        this.isShowToast = true;
        setTimeout(function() {
            this.isShowToast = false;
        }.bind(this), 10000);
    }
    closeToast(event) {
        this.isShowToast = false;
        return false;
    }

    @track isPrintModalOpen = false;
    openPrint() {
        this.isPrintModalOpen = true;
    }
    closePrint(event) {
        this.isPrintModalOpen = false;
    }
    @track showLoader = false;
    handleReset(event) {
        event.preventDefault();
        //this.showLoader = true;
        //window.location = "/lightning/o/Report_of_Collection__c/home";
        this.navigateToObjectHome();
    }
    navigateToObjectHome() {
        // Navigate to the Account home page
        this[NavigationMixin.Navigate]({
            type: 'standard__navItemPage',
            attributes: {
                apiName: 'Report_Of_Collections'
            }
            /*type: 'standard__objectPage',
            attributes: {
                objectApiName: 'Opportunity',
                actionName: 'home',
            },*/
        });
    }
    ROCCreatedDate;
    handleNextRecord(event){
        console.log(this.ROCCreatedDate);
        getNextRecord({ ROCDate: Date.parse(this.ROCCreatedDate), isNext: 1 }).then((result) => {
            console.log(result);
            if (result) {
                window.location = '/' + result;
            }
        })
        .catch((error) => {
            console.log(error);
        });
    }
    handlePrevious(event){
        getNextRecord({ ROCDate: Date.parse(this.ROCCreatedDate), isNext: 0 }).then((result) => {
            if (result) {
                window.location = '/' + result;
            }
        })
        .catch((error) => {
            console.log(error);
        });
        
    }
    
    @track showCreateNewButton = false;
    getPaymentsByCheckVendor(){
        if(this.checkvalue != '' || this.vendorId != '' || (this.toAmount != '' && this.toAmount != null) || (this.fromAmount != '' && this.fromAmount != null) || (this.paymentNumber != '' && this.paymentNumber != null) || this.caseId != '' ){
            let whereHandle = ''
            if(this.checkvalue != ''){
                whereHandle = " AND Kinship_Check__c  = '"+ this.checkvalue + "'";  
            }
            if(this.vendorId != ''){
                whereHandle += ' And ';
                whereHandle += "Vendor__c = '" + 
                this.vendorId + "' ";
            }
            if(this.toAmount != '' && this.toAmount != null){
                whereHandle += ' And ';
                whereHandle += "Payment_Amount__c <= " + 
                parseFloat(this.toAmount);
            }
            if(this.fromAmount != '' && this.fromAmount != null){
                whereHandle += ' And ';
                whereHandle += "Payment_Amount__c = " + 
                parseFloat(this.fromAmount);
            }
            if( this.paymentNumber != '' && this.paymentNumber != null){
                whereHandle += ' And ';
                whereHandle += "Invoice_Number__c like '%" +
                this.paymentNumber + "%' ";
            }
            if(this.caseId != ''){
                whereHandle += ' And ';
                whereHandle += "Kinship_Case__c = '" + 
                this.caseId + "' ";
            }
            console.log('whereHandle??????????????', whereHandle);
            getSobjectListResults({ObjectName: "Payment__c", fieldNames: [ "Name","Category_Name__c","Category_lookup__r.Category_Type__c","Category_lookup__r.FIPS_Code__c", "Opporunity_Name__c","Payment_Method__c", "Amount_Paid__c", "Check_Number__c", "Kinship_Check__c","Opportunity__r.Campaign.FIPS__c", 
                "Case_Name__c", "Payment_Date__c", "Kinship_Case__c","Kinship_Case__r.Name","Kinship_Case__r.Case_No__c", 
                "Description__c", "Balance_Remaining__c", "Refund_Amount__c", "Prior_Refund1__c", "Vendor__c", "Vendor_Name__c","Legacy_Category_Code__c",
                "Type_of_Receipt__c","Payment_Amount__c","Invoice_Stage__c", "Special_Welfare_Account__c", "Category_lookup__c", "Contact__c"],        
              
               WhereClause: " ( Recordtype.Name = 'Vendor Invoice' OR Recordtype.Name = 'Staff & Operations') AND Balance_Remaining__c > 0   AND Invoice_Stage__c = 'Paid' AND Invoice_Stage__c != 'Cancelled' AND 	Paid__c = true AND  Kinship_Check__r.Status__c <> 'Cancelled'" + whereHandle  
    
    
            }).then(result => {
                if(result){
                    console.log('Result11111111122222222222', result);
                    this.showCheckReceipts = true;
                    var i = 0;
                    if(result.length > 0){
                        result.forEach(element => {
                            if(element.Category_lookup__c){
                                if(element.Category_lookup__r.Category_Type__c == 'CSA'){
                                    if(element.Category_lookup__r.FIPS_Code__c != this.fipsCode){
                                        console.log('result123',result);
                                        result.splice(i, 1);
                                    }
                                }
                            }
                        });
                        result.forEach(element => {
                            var isDecimal = (element.Payment_Amount__c - Math.floor(element.Payment_Amount__c)) !== 0; 
                            if (isDecimal){
                                element.Payment_Amount__c = parseFloat(element.Payment_Amount__c).toFixed(2);
                            }
                            
                            if(element.Category_lookup__c == null || element.Category_lookup__c == ''){
                                element.isCategoryNull = false;
                                element.isEditAmount = false;
                                element.Category_Name__c = '';
                            }else{
                                element.isCategoryNull = true;
                                element.isEditAmount = true; 
                            }
                            element.index =  i ;
                            element.categoryId ='';
                            element.categoryName = '';
                            element.categoryType = '';
                             
                            i++
                        });
                        this.AllCheckReceipts = [...result];
                        this.showCreateNewButton = false;
                    }
                    else{
                        this.AllCheckReceipts = [...result];
                        this.showCreateNewButton = true;
                        this.showCheckReceipts = false;
                        //this.disableCheck = false;
                        //this.disableVendor = false;
                        //this.disableCase = false;
                        //this.checkvalue = '';
                        //this.toAmount = ''; 
                        //this.fromAmount = '';
                        //this.paymentNumber = ''; 
                        //this.caseId = '';
                    }
                    
                    
                }else{
                    this.AllCheckReceipts = [];
                    this.showCheckReceipts = false;
                    this.showCreateNewButton = true;

                }
            }).catch(error => {
                this.AllCheckReceipts = [];
                this.showCheckReceipts = false;
                this.showCreateNewButton = true;
            });
        }
        
        
        
    }
    selectOptions = '';
    categoryValue;
    selectedRecords(event) {

        let selectedRecords = JSON.parse(event.detail);
        
        if (selectedRecords && selectedRecords.object === "Kinship_Check__c") {
            if(selectedRecords.records.length > 0){
                this.checkvalue = selectedRecords.records[0].recId;
                //this.getPaymentsbyCheckID();
                this.newskinshipCheckNameObject = [{ 'recId' : selectedRecords.records[0].recId ,'recName' : selectedRecords.records[0].recName }];
               // 
                
                this.disableCheck = true;
            }else{
                this.disableCheck = false;
                this.checkvalue = '';
                this.AllCheckReceipts = [];
                
            }
            
        
        }
        if (selectedRecords && selectedRecords.object === "Account") {
            if(selectedRecords.records.length > 0){
                this.vendorId = selectedRecords.records[0].recId;
                //this.getPaymentsbyCheckID();
                this.newsaccountNameObject = [{ 'recId' : selectedRecords.records[0].recId ,'recName' : selectedRecords.records[0].recName  }];
                this.disableVendor = true;
            }else{
                this.disableVendor = false;
                this.vendorId = '';
                this.AllCheckReceipts = [];
            }
            
        
        }
        if (selectedRecords && selectedRecords.object === "Kinship_Case__c") {
            if(selectedRecords.records.length > 0){
                this.caseId = selectedRecords.records[0].recId;
                //this.getPaymentsbyCheckID();
                this.newskinshipCaseNameObject = [{ 'recId' : selectedRecords.records[0].recId ,'recName' : selectedRecords.records[0].recName }];
                //
                this.disableCase = true;
            }else{
                this.disableCase = false;
                this.caseId = '';
                this.AllCheckReceipts = [];
            }
            
        
        }
        
        if (selectedRecords && selectedRecords.object === "Campaign") {
            if(selectedRecords.records.length > 0){
                this.parentFiscalYear = selectedRecords.records[0].recId;
                console.log('parentFiscalYear>>>>', selectedRecords.records[0].recId);
                this.vparentFiscalYear = selectedRecords.records[0].recId;
                console.log('vparentFiscalYear>>>>', selectedRecords.records[0].recId);


                //this.getPaymentsbyCheckID();
                this.getDatefromAccountingPeriod();
                this.disableFiscalYear = true;
            }else{
                this.disableFiscalYear = false;
                this.parentFiscalYear = '';
                this.vparentFiscalYear = '';
            }
            
        
        }
        if(selectedRecords.object != "Campaign"/*this.vendorId != '' || this.checkvalue != '' || this.caseId != ''*/){
            this.getPaymentsByCheckVendor();
        }
        
    }
    toAmount;
    fromAmount;
    handleChangeToAmount(event){
        this.toAmount = event.detail.value;
    }
    handleKeyToAmount(event){
        if(event.which == 13){
            //if(this.toAmount  != '' && this.toAmount  != null){
                this.getPaymentsByCheckVendor();
            //}
        }
    }
    handleBlurToAmount(event){
        this.toAmount = event.target.value;
        //if(this.toAmount  != '' && this.toAmount  != null){
            this.getPaymentsByCheckVendor();
        //}
    }
    handleChangeFromAmount(event){
        this.fromAmount = event.detail.value;
    }
    handleKeyFromAmount(event){
        if(event.which == 13){
            //if(this.fromAmount  != '' && this.fromAmount  != null){
                this.getPaymentsByCheckVendor();
            //}
        }
    }
    handleBlurFromAmount(event){
        this.fromAmount = event.target.value;
        //if(this.fromAmount  != '' && this.fromAmount  != null){
            this.getPaymentsByCheckVendor();
        //}
    }
    paymentNumber;
    handleChangePayment(event){
        this.paymentNumber = event.detail.value;
    }
    handleKeyPayment(event){
        if(event.which == 13){
            //if(this.paymentNumber  != '' && this.paymentNumber  != null){
                this.getPaymentsByCheckVendor();
            //}
        }
    }
    handleBlurPayment(event){
        this.paymentNumber = event.target.value;
        //if(this.paymentNumber  != '' && this.paymentNumber  != null){
            this.getPaymentsByCheckVendor();
        //}
    }
    fipsCode;
    getDatefromAccountingPeriod(){
        this.selectOptions = [];
        getDatefromAccountingPeriod({recordId: this.parentFiscalYear }).then(result => {
             console.log(result + 'getDatefromAccountingPeriod');
             if(result){
                 var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];;
                 var date = new Date();
                 var startdate = new Date(result.StartDate);
                 var enddate = new Date(result.EndDate);
                 var numberOfMonths = (enddate.getFullYear() - startdate.getFullYear()) * 12;
                 
                 //numberOfMonths -= startdate.getMonth();
                 //numberOfMonths += enddate.getMonth();
                 if(numberOfMonths == 0){
                     for(var i = startdate.getMonth(); i<= enddate.getMonth(); i++){
                         const option = {
                             label: months[i] + ' ' + startdate.getFullYear(),
                             value: months[i] + ' ' + startdate.getFullYear()
                         };
                         this.selectOptions = [ ...this.selectOptions, option ];
                     }
                 }
                 else{
                     for(var i = startdate.getMonth(); i<= 11; i++){
                         const option = {
                             label: months[i] + ' ' + startdate.getFullYear(),
                             value: months[i] + ' ' + startdate.getFullYear()
                         };
                         this.selectOptions = [ ...this.selectOptions, option ];
                     }
                     for(var i = 0; i<= enddate.getMonth(); i++){
                         const option = {
                             label: months[i] + ' ' + enddate.getFullYear(),
                             value: months[i] + ' ' + enddate.getFullYear()
                         };
                         this.selectOptions = [ ...this.selectOptions, option ];
                     }
                 }
                 if(this.postingPeriod == ''){
                    this.postingPeriodValue = months[date.getMonth()] + ' ' + date.getFullYear();
                    this.postingPeriod =  this.postingPeriodValue;
                 }
                 else{
                    this.postingPeriodValue = this.postingPeriod;
                 }
                 this.fipsCode = result.FIPS__c;  
                 this.postingPeriod =  this.postingPeriodValue; 
          }
             
         }).catch(error => {
             
         });
    }
    UpdateSobjectList(){
        
        console.log(this.reportId + ' this.reportId in UpdateSobjectList?????');
        UpdateSobjectList({records: this.saveDraftValues, recnumber: this.recnumber , vparentFiscalYear:this.vparentFiscalYear ,payMethod: this.payMethod, receiptType: this.receiptType, doreceipt: this.doreceipt, OppId: this.reportId, Description: this.refundDescription,
        chknumber: this.chknumber}).then(result => {
           console.log(result + ' result in UpdateSobjectList ???');
            if(result == "Done"){
                this.showLoader = false; 
                this.toastManager('success', 'Refunds have been made successfully!.', '');
                this.closeModal();
            }else if(result == 'Insert failed. First exception on row 0; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, Total receipts amount should not be greater than report of collection amount.: [Payment_Amount__c]') {
                this.showLoader = false; 
                this.toastManager('error', 'Total receipts amount should not be greater than report of collection amount', '');
                
            }else if(result == 'Insert failed. First exception on row 0; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, Receipt Number should be within Receipt Range: [Report_Number__c]'){
                this.showLoader = false; 
                this.toastManager('error', 'Receipt Number should be within Receipt Range.', '');
            }else if(result == 'Insert failed. First exception on row 0; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, Receipt Date should be within Date Range.: [Date_of_Receipt__c]' || result == 'Invalid date:'){
                this.showLoader = false; 
                this.toastManager('error', 'Receipt Date should be within Date Range.', '');
            }
            else{
                this.showLoader = false; 
                this.toastManager('error', result, '');
                
            }
            
        }).catch(error => {
            
        });
    }
    reportNumberChange(event){
        this.recnumber = event.detail.value;
    }
    chkChange(event){
        this.chknumber = event.detail.value;
    }
    receiptTypeChange(event){
        this.receiptType = event.detail.value;
    }
    receiptMethodChange(event){
        this.value = event.detail.value;
        this.payMethod = this.value;  
        console.log('PaymentMethod???????',this.payMethod);
        if(this.payMethod == 'Cash')
        {
            this.payStage = 'Paid';
        }
        else
        {
            this.payStage = 'Pending';
        }
        console.log('payStagevalue???????',this.payStage);
    }
    receiptDateChange(event){
        this.doreceipt = event.detail.value;
    }
    @track changeAmount;
    @track cellAmount;
    handleChangeCell(event){
        this.changeAmount = event.target.draftValues;
        this.cellAmount = this.changeAmount;
    }

    @track saveDraftValues;
    @track saveCategoryValues;
    categoryRequired = false;
    handleSave(event) {
        if(this.showLoader){
            return true;
        }
        
        this.showLoader = true; 
         var updatedRecords = event.detail.draftValues;
         this.saveDraftValues = [];
         this.saveCategoryValues = [];
        if(this.recnumber  == null || this.recnumber == ''){
            this.showLoader = false; 
            this.toastManager('error',  'Receipt Number is required.'); return null;
        }/*else if(this.receiptType  == null){
            this.toastManager('error',  'Receipt Type is required.'); return null;
        }*/else if(this.doreceipt == null || this.doreceipt == '' ){
            this.showLoader = false; 
            this.toastManager('error',  'Payment date is required.'); return null;
        }else if(this.payMethod  == null || this.payMethod == ''){
            this.showLoader = false; 
            this.toastManager('error', 'Payment Method is required.'); return null;
        }
        
        //console.log('PayStage????', this.payStage);
        /*
        if (this.vparentFiscalYear == null || this.vparentFiscalYear == '') {
            this.showLoader = false; 
            this.toastManager('error', 'Error', 'Fiscal Year is required.');
            return null;
        }*/
         //if(this.selectedCheckRows.length > 0){
                var isEmpty = false;
                var numberOfEmpty = 0;
                var isBalanceGreater = false;
                var balanceamount = 0;
                var categoryRequired = false;
                updatedRecords.forEach(e => {
                    e.Invoice_Stage__c = this.payStage;
                    console.log('Invoice_Stage??????????????????', e.Invoice_Stage__c);

                    var obj = this.selectedCheckRows.find(o => o === e.Id);
                    let obj1 = this.AllCheckReceipts.find(o => o.Id === e.Id);
                    if(e.Refund_Amount__c != null && e.Refund_Amount__c != ""){
                        numberOfEmpty += 1;
                    } if(obj1.Balance_Remaining__c < parseFloat(e.Refund_Amount__c)){
                        isBalanceGreater =  true;
                    }
                    else{
                        if(balanceamount == 0){
                            balanceamount = this.balanceAmount - parseFloat(e.Refund_Amount__c);
                        }
                        else{
                            balanceamount = balanceamount - parseFloat(e.Refund_Amount__c);
                        }
                    }
                    if(obj1){
                        //console.log('Category_lookup__r.Category_Type__c',obj1.Category_lookup__r.Category_Type__c);
                        e.Name = obj1.Name;
                        e.Legacy_Category_Code__c = obj1.Legacy_Category_Code__c;
                        e.Prior_Refund1__c = obj1.Prior_Refund1__c;
                        if(obj1.Category_lookup__c){
                            e.Category_lookup__c = obj1.Category_lookup__c;
                            e.Receipt_Type__c = obj1.Category_lookup__r.Category_Type__c ;
                            
                        }
                        /*else{
                            if(obj1.categoryId != ''){
                                e.Category_lookup__c =obj1.categoryId;
                                e.Category_Type__c = obj1.categoryType;
                            }
                            else{
                                e.Receipt_Type__c = '' ;
                                e.Category_lookup__c='';
                            }
                            
                        }*/
                        e.Kinship_Check__c = obj1.Kinship_Check__c;
                        e.Kinship_Case__c = obj1.Kinship_Case__c;
                        e.Vendor__c = obj1.Vendor__c;
                        e.Contact__c = obj1.Contact__c;
                        e.Description__c = obj1.Description__c;                      
                        e.Opportunity__c = obj1.Opportunity__r.Campaign.FIPS__c;
                    }
                    //if(obj){
                        if(e.Refund_Amount__c != null && e.Refund_Amount__c != ""){
                            if(e.Category_lookup__c != null && e.Category_lookup__c != ''){
                                this.saveDraftValues.push(e);
                            }
                            else{
                                if(obj1.categoryId != ''){
                                    e.Category_lookup__c =obj1.categoryId;
                                    e.Category_Type__c = obj1.categoryType;
                                    this.saveDraftValues.push(e);
                                }
                                else{
                                    categoryRequired = true;
                                }
                                this.saveCategoryValues.push(e);
                            }
                            
                        }
                        
                    //}
                });
                
                if(numberOfEmpty == 0){
                    this.showLoader = false; 
                    this.toastManager('error', 'Refund Amount should not be empty.'); return null;
                }else if(isBalanceGreater){
                    this.showLoader = false; 
                    this.toastManager('error', 'Refund Amount should not be greater than the remaining balance.'); return null;
                }else if(categoryRequired){
                    this.showLoader = false; 
                    this.toastManager('error', 'Category is missing with the payment(s). please choose a category from Refund Category column.'); return null;
                }
                else{
                    this.UpdateSobjectList();
                    this.balanceAmount = balanceamount; 
                }
                
            
         /*}
         else{
            this.toastManager('info', 'Please select payment before saving.', ''); 
         }*/
         
        
    }
    @track doreceipt = '';
    @track receiptType = '';
    @track recnumber = '';
    @track chknumber = '';
    @track payMethod = '' ;
    @track selectedCheckRows = [];
    getSelectedRows(event) {
        const selectedRows = event.detail.selectedRows;
        this.selectedCheckRows = [];
        // Display that fieldName of the selected rows
        for (let i = 0; i < selectedRows.length; i++){
            this.selectedReceipt = selectedRows[i].Id;
            this.selectedCheckRows.push( selectedRows[i].Id);
        }
        console.log('This.selectedCheckRows ' , this.selectedCheckRows);
            
    }

    showEditHandle(event){
        this.isView = false;    
        if(this.parentFiscalYear != null && this.parentFiscalYear != ''){
            this.disableFiscalYear = true;
            this.getDatefromAccountingPeriod();
        }
    }

    handleBeginningReceiptChange(event) {
        console.log('Beginning date');
        this.bgrept_FIELD = event.target.value;
    }

    handleEndingReceiptChange(event) {
        console.log('Ending date');
        this.Ending_Receipt__c = event.target.value;
    }
    @track clonebox = false;
    cloneROC(){
        this.clonebox = true;   
    }
    cloneCencel(){
        this.clonebox = false;
    }
    cloneCreate(){
        
        cloneROC({ROCID: this.selectedReceipt}).then(result => {
            if(result == 'Insert failed. First exception on row 0; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, Total receipts amount should not be greater than report of collection amount.: [Payment_Amount__c]') {
                this.showLoader = false; 
                this.toastManager('error', 'Clone has not been created', 'Total receipts amount should not be greater than report of collection amount');
                
            }
            else if(result == ''){
                this.toastManager('success', 'Clone has been created successfully', '');
            }
            this.clonebox = false;
            this.getReceiptsFromROC();
            this.getvRefundFromROC();
            this.getSWRFromROC();
            this.getAdjustmentsFromROC();
            this.getChecksbyOpportunityId();
            for(var i=1 ; i<5; i++){
            setTimeout(function(){
            this.refreshROC();
            }.bind(this),i*1000);

            }
            //this.rocURL = this.sfdcBaseURL+"/lightning/r/Opportunity/" +result+"/view";
            //window.location= this.rocURL;
     }).catch(error => {
           
    });

    }
    errorHandler(event) {
        console.log('s');
        this.template.querySelector(".slds-notify_container.slds-is-relative");
        this.showLoader = false;
        var errorMessage = event.detail.detail;
        if(errorMessage.includes('Receipt Date should be within Date Range')){
            this.toastManager('error', errorMessage, '');
        }
        else{
            this.toastManager('error', errorMessage, '');
        }
        console.log('POSTED ERROR 2>>>>>>>',errorMessage);
    }
    postingPeriod = ''
    handlePostingPeriodChange(event){
        this.postingPeriodValue = event.detail.value;
        this.postingPeriod = event.detail.value;
    }
    
    CreateNewPayment = false;
    createNewPayment(event){
        
        getrecordTypeId().then(data => {
            if(data){
                this.recordTypeIdForPayment = data;
                this.CreateNewPayment = true;
                this.CheckReceiptList = false; 
                this.categoryRequired = false;
            }
            
        }).catch(error => {
            
        });
        
    }
    BacktoCheckReceipt(event){

        this.CreateNewPayment = false;
        this.CheckReceiptList = false; 
        this.categoryRequired = false;
        this.isNext = false;
        this.categoryisIVE = false;
        this.categoryisnotIVE = false;
        this.categoryRequired =false;
        this.modalClass = "slds-modal slds-fade-in-open";
        if(this.newskinshipCaseNameObject){
            this.kinshipCaseName = JSON.stringify(this.newskinshipCaseNameObject);
        }
        if(this.newsaccountNameObject){
            this.accountName = JSON.stringify(this.newsaccountNameObject);
        }
        if(this.newskinshipCheckNameObject){
            this.kinshipCheckName = JSON.stringify(this.newskinshipCheckNameObject);
        }
        
        
    }
    categoryisIVE = false;
    categoryisnotIVE = false;
    categoryChange(event){
        if(event.detail.value[0]){
            var catId = event.detail.value[0];
            if(catId){
                checkFundingSource({catId: catId}).then(result => {
                    if(result.Category_Type__c == 'IVE') {
                        this.categoryisIVE = true;
                        this.categoryisnotIVE = false;
                    }
                    else{
                        this.categoryisnotIVE = true;
                        this.categoryisIVE = false;
                    }
                }).catch(error => {
                    
                });
            }
        }
        
        
    }
    @track isNext = true; 
    oppId;
    saveOrNextHandle(event){
        this.oppId = this.recordId;
        var flag = event.target.dataset.isnext;        
        if(flag == "0"){
            this.isNext = false;
        }
        if(flag == "1"){
            this.isNext = true;
        }
    }
    handleSuccessPayment(event) {
        if(this.isNext){
            const inputFields = this.template.querySelectorAll(
                '.paymentField'
            );
            if (inputFields) {
                inputFields.forEach(field => {
                    field.value = '';
                });
            }
            this.CreateNewPayment = false;
            this.CheckReceiptList = false; 
            this.categoryRequired = false;
                setTimeout(function(){
                    this.CreateNewPayment = true;
                }.bind(this),500);
    
                
        }else{
            this.CreateNewPayment = false;
            this.categoryRequired = false;
            this.CheckReceiptList = true;  
            this.categoryisIVE = false;
            this.categoryisnotIVE = false;  
            this.closeModal();
        }
    }
    handleSubmitPayment(event){
            /*event.preventDefault(); // stop the form from submitting
            const fields = event.detail.fields;
            this.fields = event.detail.fields;
            this.template.querySelectorAll('lightning-record-edit-form').forEach(element => {
                element.submit();
            });*/
    } 
    categoryValue;
    disableCat =false;
    catWhereStr = "  AND Inactive__c = false AND Category_Type__c <> 'IVE'";
    selectedCategory(event) {
        let selectedRecords = JSON.parse(event.detail);
        
        if (selectedRecords && selectedRecords.object === "Category__c") {
            if(selectedRecords.records.length > 0){
                this.categoryValue = selectedRecords.records[0].recId;
                this.categoryName = selectedRecords.records[0].recName;
                this.disableCat = true;
                getCategoryName({catId: this.categoryValue}).then(result => {
                    if(result){
                        this.categoryType = result.Category_Type__c;
                    }
                }).catch(error => {
                   
                });
                //this.getPaymentsbyCheckID();
                
                
            }else{
                this.disableCat = false;
                this.checkvalue = '';
                this.AllCheckReceipts = [];
                
            }
            
        
        }
    }
    index;
    modalClass='slds-modal slds-fade-in-open';
    handleRowActionRec(event) {
        const action = event.detail.action;

        const row = event.detail.row;
        switch (action.name) {
            case "editVendorRefund":
                this.vendorRefundId = row.Id;
                this.disableCat = false;
                this.categoryRequired =true;
                this.CheckReceiptList = false; 
                this.modalClass = "slds-modal";
                this.index = row.index;
                break;
        }
    }
    kinshipCaseName;
    accountName;
    kinshipCheckName;
    newskinshipCaseNameObject='';
    newskinshipCheckNameObject='';
    newsaccountNameObject='';
    
    handleSuccessCat(event) {
        var paymentId = event.detail.id;
        
        
    }
    saveCategory(event){
                this.AllCheckReceipts[this.index].categoryId = this.categoryValue;
                this.AllCheckReceipts[this.index].categoryName = this.categoryName;
                this.AllCheckReceipts[this.index].categoryType = this.categoryType;
                this.AllCheckReceipts[this.index].isEditAmount = true;
                this.AllCheckReceipts = [...this.AllCheckReceipts];

                this.categoryRequired = false;
                this.CheckReceiptList = true; 
                this.modalClass = "slds-modal slds-fade-in-open";
                this.kinshipCaseName = JSON.stringify(this.newskinshipCaseNameObject);
                this.accountName = JSON.stringify(this.newsaccountNameObject);
                this.kinshipCheckName = JSON.stringify(this.newskinshipCheckNameObject);
    }
}