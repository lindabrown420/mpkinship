import { LightningElement } from 'lwc';
import insertCheckList from "@salesforce/apexContinuation/CheckImportFileController.insertCheckList";
export default class CheckImportFile extends LightningElement {
    get acceptedFormats() {
        return ['.rpt'];
    }
    handleFilesChange(event){
        this.parseExcelFile1(event.target.files[0]);
    }
    parseExcelFile1(inputElement) {
        var checkNumberData = [];
        var tempData = {};
        
        var file = inputElement;
        var fr=new FileReader();
        fr.onload=function(){
          var lines = fr.result.split('\n');
          for (var i = 0; i < lines.length; i++) {
              if(lines[i].includes('PAGE NUMBER')){
                  i = i+7;
              }  
              else if(!lines[i].includes('TOTAL CHECK') && !lines[i].includes('TOTAL FUND') && !lines[i].includes('TOTAL REPORT')){
                  var totalCheck = false;
                  var whileCheck = true;
                    if(lines[i].length == 126){
                        if(lines[i][1] != ''){
                            var checkNumber = '';
                            var duplicate = '';
                            for(var j = 1; j < 14; j++){
                                checkNumber = checkNumber + lines[i][j];
                            }
                            if(lines[i+1][1] != ''){
                                for(var j = 1; j < 14; j++){
                                    duplicate = duplicate + lines[i+1][j];
                                }
                            }
                            if(checkNumber == duplicate){
                                while (whileCheck) {
                                    if(lines[i].includes('TOTAL CHECK')){
                                        totalCheck = true;
                                        whileCheck = false;
                                    }
                                    i++;
                                }
                                i = i-2;
                                tempData.checkNumber = checkNumber.replace(/\s+/g,' ').trim();
                            }
                            else{
                                tempData.checkNumber = checkNumber.replace(/\s+/g,' ').trim();
                                totalCheck = false;
                            }
                            
                        }
                        if(lines[i][14] != ''){
                            var cashAcct = '';
                            for(var j = 14; j < 26; j++){
                                cashAcct = cashAcct + lines[i][j];
                            }
                            
                            tempData.cashAcct = cashAcct.replace(/\s+/g,' ').trim();
                        }
                        if(lines[i][26] != ''){
                            var dateIssued = '';
                            for(var j = 26; j < 38; j++){
                                dateIssued = dateIssued + lines[i][j];
                            }
                            
                            tempData.dateIssued = dateIssued.replace(/\s+/g,' ').trim();
                        }
                        if(lines[i][38] != ''){
                            var vendor = '';
                            for(var j = 38; j < 74; j++){
                                vendor = vendor + lines[i][j];
                            }
                            
                            tempData.vendor = vendor.replace(/\s+/g,' ').trim();
                        }
                        if(lines[i][74] != ''){
                            var acct = '';
                            for(var j = 74; j < 83; j++){
                                acct = acct + lines[i][j];
                            }
                            
                            tempData.acct = acct.replace(/\s+/g,' ').trim();
                        }
                        if(lines[i][83] != ''){
                            var description = '';
                            for(var j = 83; j < 108; j++){
                                description = description + lines[i][j];
                            }
                            
                            tempData.description = description.replace(/\s+/g,' ').trim();
                        }
                        if(lines[i][108] != ''){
                            var amount = '';
                            if(totalCheck){
                                for(var j = 108; j < 126; j++){
                                    amount = amount + lines[i+1][j];
                                }
                            }
                            else{
                                for(var j = 108; j < 126; j++){
                                    amount = amount + lines[i][j];
                                }
                            }
                            tempData.amount = amount.replace(/\s+/g,' ').trim();
                        }
                    }
                  
                  
              }
              if((tempData.checkNumber && tempData.checkNumber != '') || (tempData.cashAcct && tempData.cashAcct != '') || (tempData.dateIssued && tempData.dateIssued != '') || (tempData.vendor && tempData.vendor != '') || (tempData.acct && tempData.acct != '') || (tempData.description && tempData.description != '') || (tempData.amount && tempData.amount != ''))
              checkNumberData.push(tempData);
              tempData = {};
          };
          
          if(checkNumberData.length > 0){
            console.log(checkNumberData);
            insertCheckList({ checkListData: checkNumberData }).then((result) => {
              
              })
              .catch((error) => {
                  //console.log(error);
              });
          }
          
        }
        fr.readAsText(file);
      }
}