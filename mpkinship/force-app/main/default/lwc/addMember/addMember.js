import { LightningElement, api, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import Contact from '@salesforce/schema/CSA_Meeting_Members__c.Contact__c';
import CSA_Meeting_Agenda from '@salesforce/schema/CSA_Meeting_Members__c.CSA_Meeting_Agenda__c';
import AddExstmember from "@salesforce/apexContinuation/CSAMeetingScreen.AddExstmember";
import CheckExstmember from "@salesforce/apexContinuation/CSAMeetingScreen.CheckExstmember";
export default class AddMember extends LightningElement {
    @api recordId;
    @api agentid;
    @api teamid;
    @api objectApiName;
    @track isShowToast = false;
    @track toastType = '';
    @track title = '';
    @track message = '';
    @track toastIcon = 'utility:error';


    fields = [Contact];
    toastManager(toastType, title, message) {
        this.toastType = "slds-notify slds-notify_toast ";
        if (toastType == 'error') {
            this.toastType += 'slds-theme_error';
        } else if (toastType == 'success') {
            this.toastType += 'slds-theme_success';
        } else if (toastType == 'info') {
            this.toastType += 'slds-theme_info';
            this.toastIcon = 'utility:info';
        }
        this.title = title;
        this.message = message;
        this.isShowToast = true;
        setTimeout(function() {
            this.isShowToast = false;
        }.bind(this), 10000);
    }
    closeToast(event) {
        this.isShowToast = false;
        return false;
    }
    @api
    setAgentId(agentid) {
        this.agentid = agentid;
    }
    @api
    setTeamId(teamid) {
        this.teamid = teamid;
    }
    errorHandler(e) {
        console.log('s');
        this.template.querySelector(".slds-notify_container.slds-is-relative");
    }
    handleSubmit(event) {
        const fields = event.detail.fields;
        if (fields.Contact__c) {
            this.ContactId = fields.Contact__c;
        } else {
            this.toastManager('error', 'Error', 'Please select contact first', '');
            event.preventDefault(); // stop the form from submitting
            return false;
        }
        CheckExstmember({ ContactId: this.ContactId, TeamId: this.teamid }).then((result) => {
                console.log('result', result);
                if (result == 'Member already exist in the team') {
                    this.toastManager('error', 'Error', result);

                    setTimeout(function() {
                        this.closeToast();

                    }.bind(this), 2000);
                    event.preventDefault(); // stop the form from submitting
                    return false;
                } else {
                    this.template.querySelector('lightning-record-form').submit(fields);
                }
            })
            .catch((error) => {
                //console.log(error);
            });

        event.preventDefault(); // stop the form from submitting
        return false;
    }
    addMemberCancel() {
        this.dispatchEvent(new CustomEvent('closeaddteammember'));
    }
    handleChange(event) {

        }
        //(ID ContactId, id TeamId)
    @track ContactId;
    handleSuccess(event) {
        const fields = event.detail.fields;
        this.recordId = event.detail.id;
        console.log(this.recordId, ' this.recordId ???');
        console.log(this.teamid, ' this.teamid ???');
        console.log('fields.Contact', fields.Contact__c.value);
        this.ContactId = fields.Contact__c.value;
        console.log(this.ContactId, ' this.ContactId ???');

        setTimeout(function() {

            console.log(' call AddExstmember');
            AddExstmember({ ContactId: this.ContactId, TeamId: this.teamid, AgendaId: this.agentid }).then((result) => {
                    console.log('result', result);
                    if (result == 'true') {
                        this.toastManager('success', 'Record Created', 'Member has been added successfully!');
                        setTimeout(function() {
                            this.closeToast();
                        }.bind(this), 2000);

                    } else {
                        if (result == 'Insert failed. First exception on row 0; first error: REQUIRED_FIELD_MISSING, Required fields are missing: [Contact]: [Contact]') {
                            this.toastManager('error', 'Error', 'Please select contact first', '');
                        } else {
                            this.toastManager('error', 'Error', result);
                        }

                        setTimeout(function() {
                            this.closeToast();
                        }.bind(this), 2000);

                    }
                    this.dispatchEvent(new CustomEvent('closeaddteammember'));
                })
                .catch((error) => {
                    //console.log(error);
                });
        }.bind(this), 2000);
        
    }
}