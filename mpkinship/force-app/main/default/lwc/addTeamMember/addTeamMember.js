import { LightningElement, api, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import FirstName from '@salesforce/schema/Contact.FirstName';
import LastName from '@salesforce/schema/Contact.LastName';
import Email from '@salesforce/schema/Contact.Email';
import Phone from '@salesforce/schema/Contact.Phone';
import Mobile from '@salesforce/schema/Contact.MobilePhone';
import MailingAddress from '@salesforce/schema/Contact.MailingAddress';
import OtherAddress from '@salesforce/schema/Contact.OtherAddress';
import Title from '@salesforce/schema/Contact.Title';
import AddAgendaMember from "@salesforce/apexContinuation/CSAMeetingScreen.AddAgendaMember";
export default class AddTeamMember extends LightningElement {
    @api recordId;
    @api agentid;
    @api teamid;
    @api objectApiName;
    @track isShowToast = false;
    //@track agentid;
    @track toastType = '';
    @track title = '';
    @track message = '';
    @track toastIcon = 'utility:error';
    @api isUpdate;
    fields = [FirstName, LastName, Email, Title, Phone, Mobile, MailingAddress, OtherAddress];

    toastManager(toastType, title, message) {
        this.toastType = "slds-notify slds-notify_toast ";
        if (toastType == 'error') {
            this.toastType += 'slds-theme_error';
        } else if (toastType == 'success') {
            this.toastType += 'slds-theme_success';
        } else if (toastType == 'info') {
            this.toastType += 'slds-theme_info';
            this.toastIcon = 'utility:info';
        }
        this.title = title;
        this.message = message;
        this.isShowToast = true;
        setTimeout(function() {
            this.isShowToast = false;
        }.bind(this), 10000);
    }
    closeToast(event) {
        this.isShowToast = false;
        return false;
    }
    @api
    setIsUpdate(isUpdate) {
        this.isUpdate = isUpdate;
    }
    @api
    setAgentId(agentid) {
        this.agentid = agentid;
    }
    @api
    setTeamId(teamid) {
        this.teamid = teamid;
    }

    errorHandler(e) {
        console.log('s');
        this.template.querySelector(".slds-notify_container.slds-is-relative");
    }
    handleSubmit(event) {
        const fields = event.detail.fields;
        console.log(fields, ' fields');
        this.template.querySelector('lightning-record-form').submit(fields);

    }
    addTeamMemberCancel() {
        this.dispatchEvent(new CustomEvent('closeaddteammember'));
    }
    handleSuccess(event) {
        this.recordId = event.detail.id;
        setTimeout(function() {
            console.log(' call AddAgendaMember');
            AddAgendaMember({ AgendaId: this.agentid, ContactId: this.recordId, TeamId: this.teamid }).then((result) => {
                    console.log('result', result);
                    if (result) {
                        this.toastManager('success', 'Record Created', result);
                        setTimeout(function() {
                            this.closeToast();
                        }.bind(this), 2000);

                    } else {
                        this.toastManager('error', 'Error', result);
                        setTimeout(function() {
                            this.closeToast();
                        }.bind(this), 2000);

                    }
                    this.dispatchEvent(new CustomEvent('closeaddteammember'));
                })
                .catch((error) => {
                    //console.log(error);
                });
                
        }.bind(this), 2000);
        
    }
}