import { LightningElement,api } from 'lwc';
import getStatusValue from "@salesforce/apexContinuation/ROCController.getStatusValue";

export default class AddDepositDate extends LightningElement {
    @api recordId;
    isShowToast = false;
    toastType = '';
    title = '';
    message = '';
    statusValue = 'Deposited';
    showField;
    connectedCallback() {
        getStatusValue({ dId: this.recordId}).then((result) => {
            if (result) {
                if(result.Status__c == 'Draft' || result.Status__c == 'In Progress'){
                    this.showField = false;
                }
                else{
                    this.showField = true;
                }
            }
       })
       .catch((error) => {
           //console.log(error);
       });
    }

    handleSubmit(event) {
    }
    handleerror(event){
        this.showLoader = false;
        var errorMessage = event.detail.detail;
        this.toastManager('error', 'Error', errorMessage);
    }
    handleSuccess(event) {
        this.toastManager('success', 'Success', 'Record Updated Successfully');
        let pubsubCmp = this.template.querySelector('.first-event');
        pubsubCmp.timeout = 1000;
        pubsubCmp.publish('RESCHEDULE');
    }
    toastManager(toastType, title, message) {
        this.toastType = "slds-notify slds-notify_toast ";
        if (toastType == 'error') {
            this.toastType += 'slds-theme_error';
            this.toastIcon = 'utility:error';
        } else if (toastType == 'success') {
            this.toastType += 'slds-theme_success';
            this.toastIcon = 'utility:success';
        } else if (toastType == 'info') {
            this.toastType += 'slds-theme_info';
            this.toastIcon = 'utility:info';
        }
        this.title = title;
        this.message = message;
        this.isShowToast = true;
        setTimeout(function() {
            this.isShowToast = false;
        }.bind(this), 10000);
    }
    closeToast(event) {
        this.isShowToast = false;
        return false;
    }
}