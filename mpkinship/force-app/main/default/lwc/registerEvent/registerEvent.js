import { LightningElement,api,wire } from 'lwc';
import pubsub from 'c/one_pubsub';
import { CurrentPageReference } from 'lightning/navigation';

export default class RegisterEvent extends LightningElement {
    @api name = '';
    @api namespace = '';
    @api timeout = 0;
    @api pagereference;
    currentPageReference = '';
    @wire(CurrentPageReference)
    setCurrentPageReference(currentPageReference) {
        if( this.pagereference ){
            this.currentPageReference = currentPageReference;
        }
    }
    @api
    publish(data){
        if( this.name ){
            pubsub.fire( pubsub.buildEventName(this.currentPageReference, this.namespace, this.name) , data, this.timeout);
        }
        else{
            console.error('One PubSub: Name must be defined.');
        }
    }
    @api
    publishToNamespace(data){
        if( this.name ){
            pubsub.fireWithNamespace( pubsub.buildEventName(this.currentPageReference, this.namespace, '') , data, this.timeout);
        }
        else{
            console.error('One PubSub: Name must be defined.');
        }
    }
}