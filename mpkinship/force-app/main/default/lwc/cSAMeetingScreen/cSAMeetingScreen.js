import { LightningElement, api, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent'
import Name from '@salesforce/schema/CSA_Meeting_Agenda__c.Name';
import Location_of_Meeting from '@salesforce/schema/CSA_Meeting_Agenda__c.Location_of_Meeting__c';
import Meeting_Date from '@salesforce/schema/CSA_Meeting_Agenda__c.Meeting_Date__c';
import Team__c from '@salesforce/schema/CSA_Meeting_Agenda__c.Team__c';
import Meeting_Start_Time from '@salesforce/schema/CSA_Meeting_Agenda__c.Meeting_Start3_Time__c';
import Meeting_End_Time from '@salesforce/schema/CSA_Meeting_Agenda__c.Meeting_End_Time2__c';
import Meeting_Status from '@salesforce/schema/CSA_Meeting_Agenda__c.Meeting_Status__c';
import Review_Type from '@salesforce/schema/CSA_Meeting_Agenda__c.Review_Type__c';
import getTeamMembers from "@salesforce/apexContinuation/CSAMeetingScreen.getTeamMembers";
import AddAgendaMembers from "@salesforce/apexContinuation/CSAMeetingScreen.AddAgendaMembers";
import getTeamMembersFromAgenda from "@salesforce/apexContinuation/CSAMeetingScreen.getTeamMembersFromAgenda";
import getAgendaItems from "@salesforce/apexContinuation/CSAMeetingScreen.getAgendaItems";
import deleteitem from "@salesforce/apexContinuation/CSAMeetingScreen.deleteitem";
import deleteteam from "@salesforce/apexContinuation/CSAMeetingScreen.deleteteam";
import SendNotification from "@salesforce/apexContinuation/CSAMeetingScreen.SendNotification";
import UpdateCancellation from "@salesforce/apexContinuation/CSAMeetingScreen.UpdateCancellation";
import getAgendaRecord from "@salesforce/apexContinuation/CSAMeetingScreen.getAgendaRecord";
import CheckMeetingStartTimeForToday from "@salesforce/apexContinuation/CSAMeetingScreen.CheckMeetingStartTimeForToday";

const columns = [
    { label: 'Name', fieldName: 'Contact_Name__c' },
    { label: 'Email', fieldName: 'Contact_Email__c' },
];

const Teamcolumns = [
    /*{ label: 'Name', fieldName: 'Name' },*/
    { label: 'Team Name', fieldName: 'Team_Name__c' },
    { label: 'Contact Name', fieldName: 'Contact_Name__c' },
    { label: 'Contact Email', fieldName: 'Contact_Email__c' },
    {
        label: "",
        type: "button-icon",
        typeAttributes: {
            iconName: "utility:delete",
            name: "deleteteam",
            title: "Remove",
            disabled: false,
            value: { fieldName: "Id" }
        },
        fixedWidth: 40
    },
];
const Itemcolumns = [
    /*{ label: 'Name', fieldName: 'Name' },*/
    {
        label: "Name",
        fieldName: "Record_view__c",
        type: "url",
        sortable: true,
        typeAttributes: {
            label: { fieldName: "Name" },
            target: "_blank",
            tooltip: { fieldName: "Name" }
        }
    },
    { label: 'Case', fieldName: 'Case_Name__c' },
    { label: 'VACMS #', fieldName: 'VACMS_Number__c' },
    { label: 'Review', fieldName: 'Review__c' },
    { label: 'Review Type', fieldName: 'Review_Type__c' },
    { label: 'Agenda Comments', fieldName: 'Agenda_Comments__c' },
    { label: 'Time', fieldName: 'Time3__c', sortable: true },
    { label: 'Service Plan', fieldName: 'IFSP_Name__c'},
    {
        label: "",
        type: "button-icon",
        typeAttributes: {
            iconName: "utility:edit",
            //label: 'Note',
            name: "edititem",
            title: "Edit Item",
            disabled: false,
            value: { fieldName: "Id" }
        },
        fixedWidth: 40
    },

    {
        label: "",
        type: "button-icon",
        typeAttributes: {
            iconName: "utility:delete",
            name: "deleteitem",
            title: "Delete Item",
            disabled: false,
            value: { fieldName: "Id" }
        },
        fixedWidth: 40
    }
];
export default class CSAMeetingScreen extends NavigationMixin(LightningElement) {
    columns = columns;
    @track defaultDate = '8:00:00.000';
    
    @track Teamcolumns = Teamcolumns;
    @track Itemcolumns = Itemcolumns;
    @track memberList = [];
    @api recordId;
    @track agentid;
    @api objectApiName;
    @track displayType = 'edit';
    @track team;
    @track teamMembers = false;
    //@track recordId;
    @track isRecordCreated = false;
    @track teamMemabersList = [];
    @track relatedTeams = [];
    @track relatedItems = [];
    @track isModalOpen = false;
    @track isTeamModalOpen = false;
    @track isExtTeamModalOpen = false;
    @track selecteditem;
    @track selectedteam;
    @track stageLabel = 'Pending';
    @track meetingDate;
    @track meetingST;
    @api starttime = '8:00:00.000';
    @api endtime = '';
    @api eventdate;
    fields = [Name, Meeting_Start_Time, Meeting_Date, Meeting_End_Time, Team__c, Meeting_Status, Location_of_Meeting, Review_Type];
    @api iscsacalendar = false;

    sortDirection = 'asc';

    connectedCallback() {
        console.log(this.recordId, ' this.recordId>>>')
        if (this.recordId != null && this.recordId != '') {
            this.isRecordCreated = true;
            this.displayType = 'view';
            getAgendaRecord({ AgendaId: this.recordId }).then((result) => {
                    if (result) {
                        this.agentid = this.recordId;
                        this.stageLabel = result.Meeting_Status__c;
                        this.team = result.Team__c;
                        if (this.stageLabel == 'Cancelled') {
                            this.Cancelbuttondisable = true;
                            this.isMeetingCancel = true;
                        }
                        this.getAgendaItems();
                        this.getTeamMembersFromAgenda();
                        this.getAllTeamMembers();
                    }
                })
                .catch((error) => {
                    //console.log(error);
                });
        }
    }
    
    handleSubmit(event) {
        event.preventDefault(); 
        const fields = event.detail.fields;
        this.meetingDate = fields.Meeting_Date__c;
        this.meetingST = fields.Meeting_Start3_Time__c;
        if (this.memberList.length == 0) {
            this.toastManager('warning', 'Please select at least one team member', '');
            event.preventDefault();
            return;
        }

        if(!this.isRecordCreated){
            CheckMeetingStartTimeForToday({ meetingStartDate: this.meetingDate, meetingStartTime: this.meetingST }).then((result) => {
                console.log('CheckMeetingStartTimeForToday', result);
                if (result) {
                    this.toastManager('info', 'Invalid meeting time.', '');
                } 
                else{
                    this.template.querySelector('.agendaForm1').submit(fields);
                }
            })
            .catch((error) => {
                //console.log(error);
            });
        }
        else{
            this.template.querySelector('lightning-record-edit-form').submit(fields);
            this.contactIsAdd = true;
        }
    }
    edistAgentHandle() {
        this.isRecordCreated = false;
        this.eixstingMembers = false;
        getAgendaRecord({ AgendaId: this.recordId }).then((result) => {
            if (result) {
                this.agentid = this.recordId;
                this.starttime = result.Meeting_Start3_Time__c;
                this.endtime = result.Meeting_End_Time2__c;
            }
        })
        .catch((error) => {
            //console.log(error);
        });
    }
    handleSuccess(event) {
        this.recordId = event.detail.id;
        this.agentid = this.recordId;
        this.isRecordCreated = true;

        AddAgendaMembers({ AgendaId: this.recordId, ContactIds: this.memberList }).then((result) => {
                console.log('result', result);
                if (result) {
                    this.displayType = 'readonly';
                    this.toastManager('success', 'Record Created', result);
                    setTimeout(function() {
                        this.closeToast();
                    }.bind(this), 2000);
                    this.eixstingMembers = false;

                } else {
                    this.toastManager('info', 'Info', result);
                    setTimeout(function() {
                        this.closeToast();
                    }.bind(this), 2000);

                }
                this.relatedTeams = [];
                this.getTeamMembersFromAgenda();
            })
            .catch((error) => {
                //console.log(error);
            });

    }
    closeModal() {
        // to close modal set isModalOpen tarck value as false
        this.isModalOpen = false;
        this.getAgendaItems();
        this.getTeamMembersFromAgenda();
    }
    closeModal2() {
        // to close modal set isModalOpen tarck value as false
        
        this.isTeamModalOpen = false;
        setTimeout(function(){
            this.getTeamMembersFromAgenda();
            this.getAllTeamMembers();
        }.bind(this), 1000);
        
    }
    @track contactIsAdd = false;
    closeModal3() {
        // to close modal set isModalOpen tarck value as false
        this.contactIsAdd = true;
        this.isExtTeamModalOpen = false;
        this.getTeamMembersFromAgenda();
    }
    getTeamMembersFromAgenda() {
        this.relatedTeams = [];
        console.log('start  team method');
        console.log('this.agentid in getTeamMembersFromAgenda >>>' + this.agentid);
        getTeamMembersFromAgenda({ AgendaId: this.agentid }).then((result) => {
                if (result) {
                    console.log('result  team method', result);
                    this.relatedTeams = result;
                    this.memberList = [];
                    for (let i = 0; i < result.length; i++) {
                        //alert("You selected: " + selectedRows[i].Contact_Name__c + " id: " + selectedRows[i].Contact__c);
                        this.memberList.push(result[i].Contact__c);
                        this.listOfMember.push(result[i].Contact__c);
                    }
                } else {
                    this.relatedTeams = [];
                }
            })
            .catch((error) => {
                console.log(error);
            });
    }
    getAgendaItems() {
        this.relatedItems = [];
        console.log('start  getAgendaItems method');
        console.log('this.agentid in getAgendaItems >>>' + this.agentid);
        getAgendaItems({ AgendaId: this.agentid }).then((result) => {
                if (result) {
                    console.log('result  getAgendaItems method>>>>>', result);
                    this.relatedItems = result;
                    console.log('IFsp>>>>', result.IFSP_Name__c);
                } else {
                    this.relatedItems = [];
                }
            })
            .catch((error) => {
                console.log(error);
            });

    }
    @track listOfMember = [];
    getAllTeamMembers() {
        this.teamMemabersList = [];
        console.log('getAllTeamMembers');
        console.log('this.agentid in getAllTeamMembers >>>' + this.team);
        getTeamMembers({ teamID: this.team }).then((result) => {
                if (result) {
                    this.teamMembers = true;
                    this.teamMemabersList = result;
                    console.log('result of memberList???', this.memberList);
                    console.log('this.listOfMember???', this.listOfMember);
                    this.listOfMember = this.memberList;
                } else {

                }
            })
            .catch((error) => {
                //console.log(error);
            });
    }
    handleitemRowAction(event) {
        const action = event.detail.action;

        const row = event.detail.row;
        if (this.isMeetingCancel == true) {
            this.toastManager('info', 'The status is cancelled , items can not be modified or deleted.', '');
            return false;
        }
        switch (action.name) {
            case "edititem":
                this.selecteditem = row.Id;
                this.isModalOpen = true;
                this.isUpdate = true;
                break;
            case "deleteitem":
                this.selecteditem = row.Id;
                this.deleteConfirmation();

        }
    }
    @track showDeleteConfirm = false;
    @track showteamDeleteConfirm = false;
    deleteteamConfirmation() {
        this.showteamDeleteConfirm = true;
    }
    deleteConfirmation() {
        this.showDeleteConfirm = true;
    }
    deleteitemHandle(event) {
        deleteitem({ itemid: this.selecteditem }).then((result) => {
                if (result) {
                    this.getAgendaItems();
                    console.log('show toast delete button');
                    this.toastManager('success', 'The item has been deleted.', '');
                    console.log('call close method');
                    this.closesDeleteConfirm(null);
                }
            })
            .catch((error) => {
                //console.log(error);
            });
    }
    handleteamRowAction(event) {
        const action = event.detail.action;
        const row = event.detail.row;
        if (this.isMeetingCancel == true) {
            this.toastManager('info', 'The status is cancelled , members can not be deleted.', '');
            return false;
        }
        switch (action.name) {
            case "deleteteam":
                this.selectedteam = row.Id;
                this.deleteteamConfirmation();

        }
    }
    deleteteamHandle(event) {
        deleteteam({ teamid: this.selectedteam }).then((result) => {
                if (result) {
                    this.getTeamMembersFromAgenda();
                    this.getAllTeamMembers();
                    console.log('show toast delete button');
                    this.toastManager('success', 'The member has been removed.', '');
                    console.log('call close method');
                    this.closeTeamDeleteConfirm(null);
                }
            })
            .catch((error) => {
                //console.log(error);
            });
    }
    closeTeamDeleteConfirm(event) {
        console.log('close confirm box');
        this.showteamDeleteConfirm = false;
    }
    closesDeleteConfirm(event) {
        console.log('close confirm box');
        this.showDeleteConfirm = false;
    }
    handleChange(event) {
        console.log('field change');
        console.log('event.detail', event.detail);
        console.log('event.detail.value', event.detail.value);
        this.team = event.detail.value[0];
        console.log('this.team >>>', this.team);
        getTeamMembers({ teamID: this.team }).then((result) => {
                if (result) {
                    this.teamMembers = true;
                    this.teamMemabersList = result;
                    console.log(this.teamMembers);
                    console.log(this.teamMemabersList);
                } else {

                }
            })
            .catch((error) => {
                //console.log(error);
            });
    }
    navigateToObjectHome() {
        // Navigate to the Account home page
        this[NavigationMixin.Navigate]({
            type: 'standard__objectPage',
            attributes: {
                objectApiName: 'CSA_Meeting_Agenda__c',
                actionName: 'home',
            },
        });
    }
    handleReset(event) {
        console.log('---' + this.iscsacalendar);
        if(this.iscsacalendar) {
            const csacancel = new CustomEvent('csacancel', {});
            this.dispatchEvent(csacancel);
        } else {
            this.navigateToObjectHome();
        }
    }
    handleUnSelectMember(event) {

    }
    handleSelectMember(event) {
        const selectedRows = event.detail.selectedRows;
        // Display that fieldName of the selected rows
        this.memberList = [];
        for (let i = 0; i < selectedRows.length; i++) {
            //alert("You selected: " + selectedRows[i].Contact_Name__c + " id: " + selectedRows[i].Contact__c);
            this.memberList.push(selectedRows[i].Contact__c);
            this.listOfMember.push(selectedRows[i].Contact__c);
        }
        console.log('result of memberList', this.memberList);
    }

    @track isShowToast = false;
    @track toastType = '';
    @track title = '';
    @track message = '';
    @track toastIcon = 'utility:error';
    toastManager(toastType, title, message) {
        if(toastType.toUpperCase() == 'ERROR'){
            toastType = 'info';
        }
        this.toastType = "slds-notify slds-notify_toast ";
        if (toastType == 'error') {
            this.toastType += 'slds-theme_error';
            this.toastIcon = 'utility:error';
        } else if (toastType == 'success') {
            this.toastType += 'slds-theme_success';
            this.toastIcon = 'utility:success';
        } else if (toastType == 'info') {
            this.toastType += 'slds-theme_info';
            this.toastIcon = 'utility:info';
        }
        this.title = title;
        this.message = message;
        this.isShowToast = true;
        setTimeout(function() {
            this.isShowToast = false;
        }.bind(this), 10000);
    }
    closeToast(event) {
        this.isShowToast = false;
        return false;
    }
    handleClickItem() {
        console.log('button click');
        this.isModalOpen = true;
        this.selecteditem = '';
        console.log('this.agentid >>>>', this.agentid);
        console.log('this.selecteditem >>>>', this.selecteditem);
        setTimeout(function() {
            this.template.querySelector("c-add-agenda-items").setIsUpdate(true);
            this.template.querySelector("c-add-agenda-items").setAgentid(this.agentid);

        }.bind(this), 1000);
    }

    handleMemberClickItem() {
        console.log('Member button click ');
        this.isTeamModalOpen = true;
        console.log('this.agentid >>>>', this.agentid);
        console.log('this.team >>>>', this.team);
        setTimeout(function() {
            this.template.querySelector("c-add-team-member").setIsUpdate(true);
            this.template.querySelector("c-add-team-member").setAgentId(this.agentid);
            this.template.querySelector("c-add-team-member").setTeamId(this.team);
        }.bind(this), 1000);
    }
    handleExistingMemberClickItem() {
        console.log('Ext Member button click ');
        this.isExtTeamModalOpen = true;
        console.log('this.agentid >>>>', this.agentid);
        console.log('this.team >>>>', this.team);
        setTimeout(function() {
            this.template.querySelector("c-add-member").setAgentId(this.agentid);
            this.template.querySelector("c-add-member").setTeamId(this.team);
        }.bind(this), 1000);
    }
    @track Emailbuttondisable = false;
    @track Cancelbuttondisable = false;
    handleSendEmailClick() {
        console.log('button click send email');
        SendNotification({ agendaid: this.agentid }).then((result) => {
                if (result) {
                    this.toastManager('success', 'Email has been sent to all team members.', '');
                    Emailbuttondisable = True;
                }
            })
            .catch((error) => {
                //console.log(error);
            });
    }
    @track showCancelConfirm = false;
    @track CloseCancelConfirm = false;
    @track isMeetingCancel = false;
    closeCancelConfirm(event) {
        console.log('close confirm box');
        this.showCancelConfirm = false;
    }
    CancelMeetingClick() {
        this.showCancelConfirm = true;
    }
    handleCancelMeetingClick() {
        console.log('cancel button click');
        this.showCancelConfirm = false;
        UpdateCancellation({ agendaid: this.agentid }).then((result) => {
                console.log('result ????? ', result);
                if (result) {
                    this.toastManager('success', 'Meeting has been cancelled.', '');
                    this.Cancelbuttondisable = true;
                    this.isMeetingCancel = true;
                }
            })
            .catch((error) => {
                //console.log(error);
            });
    }
    handleChatter() {
        setTimeout(
            function() {
                this.template.querySelectorAll("c-chatterfeed").forEach((element) => {
                    element.openChatter(this.agentid);
                });
            }.bind(this),
            1000
        );
    }

    // Used to sort the 'Age' column
    sortBy(field, reverse, primer) {
        const key = primer ?

            function(x) {
                return primer(x[field]);
            } :
            function(x) {
                return x[field];
            };

        return function(a, b) {
            a = key(a);
            b = key(b);
            return reverse * ((a > b) - (b > a));
        };
    }

    onHandleSort(event) {
        const { fieldName: sortedBy, sortDirection } = event.detail;
        const cloneData = [...this.relatedItems];

        cloneData.sort(this.sortBy(sortedBy, sortDirection === 'asc' ? 1 : -1));
        this.relatedItems = cloneData;
        this.sortDirection = sortDirection;
        this.sortedBy = sortedBy;
    }
    @track eixstingMembers = false;
    handleAddExistingMemberClickItem(){
        if(this.contactIsAdd){
            this.getTeamMembersFromAgenda();
            this.getAllTeamMembers();
            this.contactIsAdd = false;
        }
        
        this.eixstingMembers = true;

    }
    handleCloseModal(){
        this.eixstingMembers = false;
        this.contactIsAdd = true;
    }
}