import { LightningElement,api,track } from 'lwc';
import FactoryCss from '@salesforce/resourceUrl/FactoryCss';
import getCaseDetail from '@salesforce/apexContinuation/CaseController.getCaseDetail';
//import getRelatedListDetail from '@salesforce/apexContinuation/CaseController.getRelatedListDetail';
//import fetchRecs from '@salesforce/apexContinuation/CloneRelatedListController.fetchRecs';
import { hideSpinner, showSpinner } from 'c/util';
import { loadStyle } from 'lightning/platformResourceLoader';
import uploadFile from '@salesforce/apexContinuation/StaffAndOperationsController.uploadFile';
import getFileName from '@salesforce/apexContinuation/CaseController.getFileName';
import deleteProfilePic from '@salesforce/apexContinuation/CaseController.deleteProfilePic';
import generate from '@salesforce/apexContinuation/XLSXGenerator.generate';
//import getGoalRecords from '@salesforce/apex/ServicePlanGoals.getGoalRecords';
//import getProgramList from '@salesforce/apexContinuation/CaseController.getProgramList';
import Avatar from '@salesforce/resourceUrl/Avatar'

import { NavigationMixin } from 'lightning/navigation';
//import Age from '@salesforce/schema/Kinship_Case__c.Age__c';

const Reccolumns = [
];
export default class CaseLayout extends NavigationMixin(LightningElement) {
    
    imageUrl = Avatar

    iscase = true;
    selectStr;
    Reccolumns = Reccolumns;
    allData=[];
    @api recordId;
    goalRecords = [];
    newGoalCheck = false;
    contactName;
    Email_to_vendor__c;
    contactAddress;
    contactNumber;
    childAge;
    childBOB;
    childGender;
    childRace;
    program='/lightning/r/Programs__c/a2T3R00000000FpUAI/view';
    goal = '/lightning/r/Goals__c/a2U3R00000000cmUAA/view'
    chatterURL = window.location.origin +'/lightning/page/chatter';
    taskURL = window.location.origin +'/lightning/o/Task/home';
    noteURL = window.location.origin +'/lightning/o/ContentNote/home';
    contactURL = window.location.origin +'/lightning/o/Relationship__c/home';
    moreURL = '#';
    Field_1_Lookup__c;
    Field_2_Lookup__c;
    Field_3_Lookup__c;
    Field_4_Lookup__c;
    Field_5_Lookup__c;
    Field_6_Lookup__c;
    Field_1_API_Name__c;
    Field_2_API_Name__c;
    Field_3_API_Name__c;
    Field_4_API_Name__c;
    Field_5_API_Name__c;
    Field_6_API_Name__c;
    Parent_Field_2_API_Name__c;
    Parent_Field_3_API_Name__c;
    Parent_Field_4_API_Name__c;
    Parent_Field_5_API_Name__c;
    Parent_Field_6_API_Name__c;
    parentFieldName;
    recId = '';
    conId = '';
    showViewForm = true;
    IsNewButton = false;
    programList = [];
    get acceptedFormats() {
        return ['.jpg', '.png'];
    }
    renderedCallback() {
        
        Promise.all([
            loadStyle( this, FactoryCss )
            ]).then(() => {
                console.log( 'Files loaded' );
            })
            .catch(error => {
                console.log( error.body.message );
        });
        
        
            
            
        
    }
    connectedCallback(){
        console.log('connectedCallback>>>>>');
        console.log('this.recordId>>>>>',this.recordId);
        getCaseDetail({recId: this.recordId }).then(result => {
            console.log('result>>>>>',result);
            if(result){
                if(result.Primary_Contact__c){
                    this.conId = result.Primary_Contact__c;
                   //this.contactURL = '/'+result.Primary_Contact__c; 
                   this.contactName = result.Primary_Contact__r.Name;
                   this.childAge = result.Age__c;
                   this.childBOB = result.DOB__c;
                   this.splitDOB();
                   this.childGender = result.Gender_Preference__c;
                   this.childRace = result.Race__c;
                    if(result.Race__c == 1)
                        {
                            this.childRace = "White";
                        }
                    else if(result.Race__c == 2)
                        {
                            this.childRace = "African-American or black";   
                        }
                    else if(result.Race__c == 3)
                        {
                            this.childRace = "Asian";
                        }
                    else if(result.Race__c == 4)
                        {
                            this.childRace = "American Indian or Alaska Native";
                        }
                    else if(result.Race__c == 5)
                        {
                            this.childRace = "Native Hawaiian or Other Pacific Islander	";
                        }
                    else if(result.Race__c == 6)
                        {
                            this.childRace = "Unable to determine";
                        }
                    else if(result.Race__c == 7)
                        {
                            this.childRace = "Bi-racial";
                        }
                        
                }
                if(result.contact_Mailing_Street__c  && result.Contact_Mailing_State__c){
                   this.contactAddress = result.contact_Mailing_Street__c + ', '+ result.Contact_Mailing_State__c;
                }
                else if(result.contact_Mailing_Street__c){
                   this.contactAddress = result.contact_Mailing_Street__c;
                }
                else if( result.Contact_Mailing_State__c){
                   this.contactAddress = result.Contact_Mailing_State__c;
                }
                if(result.Kinship_Case_Number__c){
                   this.contactNumber = result.Kinship_Case_Number__c;
                }
                this.selectOptions =[];
                
                 
            }
            
        }).catch(error => {
            console.log('error>>>>>',error);
        });


        /*getProgramList({recId: this.recordId }).then(result => {
            console.log('result>>>>>',result);
            if(result){
                result.forEach(each => {
                    each.programId = '/lightning/r/Programs__c/'+each.Id+'/view'
                });
                this.programList = result;
            }
            
        }).catch(error => {
            console.log('error>>>>>',error);
        });*/

        // getRelatedListDetail({title: 'ServicePlan'}).then(result => {
        //     if(result){
        //         this.recId = this.recordId;
        //         this.createTableColumns(result);
        //         this.createSelectStr(result);
        //         }
            
        // }).catch(error => {
        // });
        getFileName({recordId: this.recordId }).then(result => {
            if(result== null || result== '')
            {
                this.imageUrl = Avatar;
                this.isImg = false;
            }
            else if(result){
                if(result.Id){
                    this.isImg = true;
                    this.imageUrl = '/sfc/servlet.shepherd/version/download/'+result.Id;
                } else {
                    this.isImg = false;
                    this.imageUrl = Avatar;
                }
                
            }
            
        }).catch(error => {
            
        });
        
    }
    isImg = false;
    splitDOB(){
        var a = this.childBOB.split('-')
        this.childBOB = a[1]+"-"+a[2]+"-"+a[0];
    }

    newProgramModal = false;
    openProgamModal(){
        this.newProgramModal = true;
    }
    closePModal(){
        this.newProgramModal = false;
    }
    
    newGoalModal = false;
    handleViewGoal(){
        this.newGoalModal = true;
        this.newGoalClicked();
    
    }

    closeGModal(){
        this.newGoalModal = false;
        
    }

    showCaseField = false;
    handleViewCase(){
        this.showCaseField = true;
    }

    editModal(){
        this.showViewForm = false;
    }
    closeModal(){
        this.showCaseField = false;
        this.showViewForm = true;
    }
    handleSuccess(){
        this.showCaseField = false;
        this.showViewForm = true;
    }
    handlePostClick(){
        window.open(this.chatterURL);
    }
    handleTaskClick(){
        window.open(this.taskURL);
    }
    handleNoteClick(){
        window.open(this.noteURL);
    }
    handleContactClick(){
        window.open(this.contactURL);
    }
    handleMorelick(){
        window.open(this.moreURL);
    }


   

    createSelectStr(result) {
        if(result.Parent_Field_API_Name__c)
        this.parentFieldName = result.Parent_Field_API_Name__c;

        if(result.Title__c){
            this.Title = result.Title__c;
        }

        console.log('Title?????????????----', this.Title);

        if(result.Additional_Criteria__c){
            this.criteria = result.Additional_Criteria__c;
        }
        else{
            this.criteria = '';
        }
        if(result.Object_API_Name__c)
        this.sObjName = result.Object_API_Name__c;

        console.log('ObjectAPI?????????????----', this.sObjName);
        var selectStr = '';
        var f1 = result.Field_1_API_Name__c;
        this.Field_1_API_Name__c = result.Field_1_API_Name__c;
        if(f1 && f1 != ''){
            selectStr += f1;
        }
        var f2 = result.Field_2_API_Name__c;
        this.Field_2_API_Name__c = result.Field_2_API_Name__c;
        var isLookup2 = result.Field_2_Lookup__c;
        this.Field_2_Lookup__c = result.Field_2_Lookup__c;
        var ParentfieldAPI2 = result.Parent_Field_2_API_Name__c;
        this.Parent_Field_2_API_Name__c = result.Parent_Field_2_API_Name__c;
        if(f2 && f2 != ''){
            if(selectStr != ''){
                selectStr += ', ';
                
            }
            selectStr += f2;
            if(isLookup2!=undefined && isLookup2!='' && ParentfieldAPI2!=undefined && ParentfieldAPI2!=''){
            	if(selectStr != ''){
                    selectStr += ', ';
                    selectStr +=ParentfieldAPI2;
            	}    
            }
        }
        var f3 = result.Field_3_API_Name__c;
        this.Field_3_API_Name__c = result.Field_3_API_Name__c;
        var isLookup3 = result.Field_3_Lookup__c;
        this.Field_3_Lookup__c = result.Field_3_Lookup__c;
        var ParentfieldAPI3 = result.Parent_Field_3_API_Name__c;
        this.Parent_Field_3_API_Name__c = result.Parent_Field_3_API_Name__c;
        if(f3 && f3 != ''){
            if(selectStr != ''){
                selectStr += ', ';
            }
            selectStr += f3;
            if(isLookup3!=undefined && isLookup3!='' && ParentfieldAPI3!=undefined && ParentfieldAPI3!=''){
            	if(selectStr != ''){
                    selectStr += ', ';
                    selectStr +=ParentfieldAPI3;
            	}    
            }
        }
        var f4 = result.Field_4_API_Name__c;
        this.Field_4_API_Name__c = result.Field_4_API_Name__c;
        var isLookup4 = result.Field_4_Lookup__c;
        this.Field_4_Lookup__c = result.Field_4_Lookup__c;
        var ParentfieldAPI4 = result.Parent_Field_4_API_Name__c;
        this.Parent_Field_4_API_Name__c = result.Parent_Field_4_API_Name__c;
        if(f4 && f4 != ''){
            if(selectStr != ''){
                selectStr += ', ';
            }
            selectStr += f4;
            if(isLookup4!=undefined && isLookup4!='' && ParentfieldAPI4!=undefined && ParentfieldAPI4!=''){
            	if(selectStr != ''){
                    selectStr += ', ';
                    selectStr +=ParentfieldAPI4;
            	}    
            }
        }
        var f5 = result.Field_5_API_Name__c;
        this.Field_5_API_Name__c = result.Field_5_API_Name__c;
        var isLookup5 = result.Field_5_Lookup__c;
        this.Field_5_Lookup__c = result.Field_5_Lookup__c;
        var ParentfieldAPI5 = result.Parent_Field_5_API_Name__c;
        this.Parent_Field_5_API_Name__c = result.Parent_Field_5_API_Name__c;
        if(f5 && f5 != ''){
            if(selectStr != ''){
                selectStr += ', ';
            }
            selectStr += f5;
            if(isLookup5!=undefined && isLookup5!='' && ParentfieldAPI5!=undefined && ParentfieldAPI5!=''){
            	if(selectStr != ''){
                    selectStr += ', ';
                    selectStr +=ParentfieldAPI5;
            	}    
            }
        }

        var f6 = result.Field_6_API_Name__c;
        this.Field_6_API_Name__c = result.Field_6_API_Name__c;
        var isLookup6 = result.Field_6_Lookup__c;
        this.Field_6_Lookup__c = result.Field_6_Lookup__c;
        var ParentfieldAPI6 = result.Parent_Field_6_API_Name__c;
        this.Parent_Field_6_API_Name__c = result.Parent_Field_6_API_Name__c;
        if(f6 && f6 != ''){
            if(selectStr != ''){
                selectStr += ', ';
            }
            selectStr += f6;
            if(isLookup6!=undefined && isLookup6!='' && ParentfieldAPI6!=undefined && ParentfieldAPI6!=''){
            	if(selectStr != ''){
                    selectStr += ', ';
                    selectStr +=ParentfieldAPI6;
            	}    
            }
        }
        this.selectStr = selectStr;
        this.getDataTableDetail();
    }
    createTableColumns(result) {
		var colums = [];
        if(result.Field_1_API_Name__c == 'ContentDocumentId'){
            var f1 = 'Title';
            var f1Label = 'Subject';
            colums.push({ label: f1Label, fieldName: 'recordURL', type: 'url', 
                            typeAttributes: { label: { fieldName: f1 }, tooltip:{ fieldName: f1 }, target: '_blank' } });
                

            var f2 = 'TextPreview';
            var f2Label = 'Description';
            colums.push({ label: f2Label, fieldName: f2, type: type2});
            
            
        }
        else{
            if(result.Field_1_API_Name__c && result.Field_1_Label__c ){
                var f1 = result.Field_1_API_Name__c;
                var f1Label = result.Field_1_Label__c;
                
                if(f1 && f1 != ''){
                    colums.push({ label: f1Label, fieldName: 'recordURL', type: 'url', 
                                 typeAttributes: { label: { fieldName: f1 }, tooltip:{ fieldName: f1 }, target: '_blank' } });
                    
                }
            }
            if(result.Field_2_API_Name__c && result.Field_2_Label__c ){
                var f2 = result.Field_2_API_Name__c;
                var f2Label = result.Field_2_Label__c;
                var type2 = result.Field_2_Type__c;
                if(f2 && f2 != ''){
                    if(result.Field_2_Lookup__c!=undefined && result.Field_2_Lookup__c==true && 
                        result.Parent_Field_2_API_Name__c!=undefined && result.Parent_Field_2_API_Name__c!=''){
                           colums.push({ label: f2Label, fieldName: 'f2url', type: 'url', 
                         typeAttributes: { label: { fieldName: 'f2Name' }, tooltip:{ fieldName: f2 }, target: '_blank' } });     
                    }
                    else
                        colums.push({ label: f2Label, fieldName: f2, type: type2});
                    
                }
            }
            if(result.Field_3_API_Name__c && result.Field_3_Label__c ){
                var f3 = result.Field_3_API_Name__c;
                var f3Label = result.Field_3_Label__c;
                var type3 = result.Field_3_Type__c;
                if(f3 && f3 != ''){
                    if(result.Field_3_Lookup__c!=undefined && result.Field_3_Lookup__c==true && 
                        result.Parent_Field_3_API_Name__c!=undefined && result.Parent_Field_3_API_Name__c!=''){
                           colums.push({ label: f3Label, fieldName: 'f3url', type: 'url', 
                         typeAttributes: { label: { fieldName: 'f3Name' }, tooltip:{ fieldName: f3 }, target: '_blank'  }  });     
                    }
                    else
                        colums.push({ label: f3Label, fieldName: f3, type: type3 });
                    
                }
            }
            if(result.Field_4_API_Name__c && result.Field_4_Label__c ){var f4 =result.Field_4_API_Name__c;
                var f4Label = result.Field_4_Label__c;
                var type4 = result.Field_4_Type__c;
                if(f4 && f4 != ''){
                    if(result.Field_4_Lookup__c!=undefined && result.Field_4_Lookup__c==true && 
                        result.Parent_Field_4_API_Name__c!=undefined && result.Parent_Field_4_API_Name__c!=''){
                           colums.push({ label: f4Label, fieldName: 'f4url', type: 'url', 
                         typeAttributes: { label: { fieldName: 'f4Name' }, tooltip:{ fieldName: f4 }, target: '_blank' }  });     
                    }
                    else
                        colums.push({ label: f4Label, fieldName: f4, type: type4 });
                    
                }
            }
            if(result.Field_5_API_Name__c && result.Field_5_Label__c ){
                var f5 = result.Field_5_API_Name__c;
                var f5Label = result.Field_5_Label__c;
                var type5 = result.Field_5_Type__c;
                if(f5 && f5 != ''){
                    if(result.Field_5_Lookup__c!=undefined && result.Field_5_Lookup__c==true && 
                        result.Parent_Field_5_API_Name__c!=undefined && result.Parent_Field_5_API_Name__c!=''){
                        colums.push({ label: f5Label, fieldName: 'f5url', type: 'url', 
                        typeAttributes: { label: { fieldName: 'f5Name' }, tooltip:{ fieldName: f5 }, target: '_blank' }  });     
                    }
                    else
                        colums.push({ label: f5Label, fieldName: f5, type: type5 });
                    
                }
            }
            if(result.Field_6_API_Name__c && result.Field_6_Label__c ){
                var f6 = result.Field_6_API_Name__c;
                var f6Label = result.Field_6_Label__c;
                var type6 = result.Field_6_Type__c;
                if(f6 && f6 != ''){
                    if(result.Field_6_Lookup__c!=undefined && result.Field_6_Lookup__c==true && 
                        result.Parent_Field_6_API_Name__c!=undefined && result.Parent_Field_6_API_Name__c!=''){
                           colums.push({ label: f6Label, fieldName: 'f6url', type: 'url', 
                         typeAttributes: { label: { fieldName:'f6Name' }, tooltip:{ fieldName: f6 }, target: '_blank' }  });     
                    }
                    else
                        colums.push({ label: f6Label, fieldName: f6, type: type6 });
                    
                }
            }
        }
        
        if(result.Is_New_Button__c == true){
            this.IsNewButton = true;
        }
        else{
            this.IsNewButton = false;
        }
        this.Reccolumns = colums;
        
	}
    
    // getDataTableDetail(){
    //     fetchRecs({recId: this.recId,  
    //     sObjName: this.sObjName,  
    //     parentFldNam: this.parentFieldName,  
    //     strCriteria: this.criteria,
    //     selectStr: this.selectStr}).then(res => {
    //         if(res){
    //             if(res!=null && res!=undefined && res.length > 10)
    //                 this.IsShowAll = true;
    //             else
    //                 this.IsShowAll = false;
    //             for(var i = 0; i < res.length; i++){
    //                 res[i].recordURL = '/'+res[i].Id; 
    //                 if(this.Field_2_Lookup__c!=undefined && this.Field_2_Lookup__c==true &&
    //                     this.Parent_Field_2_API_Name__c!=undefined ){
    //                     if(this.Parent_Field_2_API_Name__c == 'RecordTypeId'){
    //                         var parentid =this.Parent_Field_2_API_Name__c;
    //                         res[i].f2url = '/lightning/setup/ObjectManager/01I3d0000008h80/RecordTypes/'+res[i][parentid]+'/view';
    //                     }    
    //                     else{
    //                         var parentid =this.Parent_Field_2_API_Name__c;
    //                         if(res[i][parentid]){
    //                             res[i].f2url ='/'+res[i][parentid];
    //                         }
    //                         else{
    //                             res[i].f2url = '';
    //                         }
    //                     }
                            
                        
    //                     var parentName =this.Field_2_API_Name__c;
    //                     parentName = parentName.split(".");
                       
    //                     if(parentName.length == 2){
    //                         if(res[i][parentName[0]][parentName[1]]){
    //                             res[i].f2Name =res[i][parentName[0]][parentName[1]];
    //                         }
    //                         else{
    //                             res[i].f2Name = '';
    //                         }
    //                     }
                           
    //                 }
    //                 if(this.Field_3_Lookup__c!=undefined && this.Field_3_Lookup__c==true &&
    //                     this.Parent_Field_3_API_Name__c!=undefined ){
    //                     var parentid =this.Parent_Field_3_API_Name__c;
    //                     var parentName =this.Field_3_API_Name__c;
    //                     parentName = parentName.split(".");
    //                     if(res[i][parentid]){
    //                         res[i].f3url ='/'+res[i][parentid];
    //                     }
    //                     else{
    //                         res[i].f3url = '';
    //                     }
    //                     if(parentName.length == 2){
    //                         if(res[i][parentName[0]][parentName[1]]){
    //                             res[i].f3Name =res[i][parentName[0]][parentName[1]];
    //                         }
    //                         else{
    //                             res[i].f3Name = '';
    //                         }
    //                     }
                           
    //                 }
    //                 if(this.Field_4_Lookup__c!=undefined && this.Field_4_Lookup__c==true &&
    //                     this.Parent_Field_4_API_Name__c!=undefined ){
    //                     var parentid =this.Parent_Field_4_API_Name__c;
    //                     var parentName =this.Field_4_API_Name__c;
    //                     parentName = parentName.split(".");
    //                     if(res[i][parentid]){
    //                         res[i].f4url ='/'+res[i][parentid];
    //                     }
    //                     else{
    //                         res[i].f4url = '';
    //                     }
    //                     if(parentName.length == 2){
    //                         if(res[i][parentName[0]][parentName[1]]){
    //                             res[i].f4Name =res[i][parentName[0]][parentName[1]];
    //                         }
    //                         else{
    //                             res[i].f4Name = '';
    //                         }
    //                     }
                            
    //                 }
    //                 if(this.Field_5_Lookup__c!=undefined && this.Field_5_Lookup__c==true &&
    //                     this.Parent_Field_5_API_Name__c!=undefined ){
    //                     var parentid =this.Parent_Field_5_API_Name__c;
    //                     var parentName =this.Field_5_API_Name__c;
    //                     parentName = parentName.split(".");
    //                     if(res[i][parentid]){
    //                         res[i].f5url ='/'+res[i][parentid];
    //                     }
    //                     else{
    //                         res[i].f5url = '';
    //                     }
    //                     if(parentName.length == 2){
    //                         if(res[i][parentName[0]][parentName[1]]){
    //                             res[i].f5Name =res[i][parentName[0]][parentName[1]];
    //                         }
    //                         else{
    //                             res[i].f5Name = '';
    //                         }
    //                     }
                           
    //                 }
    //                 if(this.Field_6_Lookup__c!=undefined && this.Field_6_Lookup__c==true &&
    //                     this.Parent_Field_6_API_Name__c!=undefined ){
    //                     var parentid =this.Parent_Field_6_API_Name__c;
    //                     var parentName =this.Field_6_API_Name__c;
    //                     parentName = parentName.split(".");
    //                     if(res[i][parentid]){
    //                         res[i].f6url ='/'+res[i][parentid];
    //                     }
    //                     else{
    //                         res[i].f6url = '';
    //                     }
    //                     if(parentName.length == 2){
    //                         if(res[i][parentName[0]][parentName[1]]){
    //                             res[i].f6Name =res[i][parentName[0]][parentName[1]];
    //                         }
    //                         else{
    //                             res[i].f6Name = '';
    //                         }
    //                     }
                       
    //                 }
                    
    //             }
    //             var responselist=[];
    //             if(res.length > 10){
    //                 for(var i=0 ; i < res.length ;i++){
    //                     if(i<10)
    //                     	responselist.push(res[i]);
    //                     else
    //                         break;
    //                 }
    //             }
    //             if(res.length <=10)    
    //             this.allData = res;
    //             else if( res.length >10 && responselist.length >0)
    //                 this.allData = responselist;
    //         }
    //         hideSpinner(this);
            
    //     }).catch(error => {
    //     });
        
    // }
    // handleActive(event){
    //     showSpinner(this);
    //     var tabName = event.target.value;
    //     getRelatedListDetail({title: tabName}).then(result => {
    //         if(result){
    //             //if(tabName == 'Health' ||  tabName == 'Support' || tabName == 'Medication' ||  tabName == 'Surgeries' || tabName == 'Diagnosis'  ){
    //                 //this.recId = this.conId;
    //             //}
    //             //else{
    //                 this.recId = this.recordId;
    //             //}
                
    //             this.createTableColumns(result);
    //             this.createSelectStr(result);
    //             }
    //         else{
    //             this.Reccolumns = [];
    //             this.allData = [];
    //         }
    //     }).catch(error => {
    //     });
    // }
    showFileUpload = false;
    handleOnImgClick(){
        this.showFileUpload = true;
    }
    handleClosModal(){
        this.showFileUpload = false;
    }
    uploadedFiles=[];
    fileName=[];
    //imageUrl = '/sfc/servlet.shepherd/version/download/0683R000000368oQAA';
    addFile = false;
   
    handleUploadFinished(event) {
        const uploadedFile = event.detail.files;
        this.uploadedFiles.push(event.detail.files);
        for(let i = 0; i < uploadedFile.length; i++) {
            if(this.uploadIds == ''){
                this.uploadIds=uploadedFile[i].contentVersionId;
            }
            else{
                this.uploadIds=this.uploadIds+','+uploadedFile[i].contentVersionId;
            }
            this.fileName.push(uploadedFile[i].name);
        }
        this.addFile = true;
        this.handleClosModal();
    }
    handleAddFile(){
        this.showFileUpload = false;
        uploadFile({ recordId: this.recordId,uploadIds:this.uploadIds }).then((result) => {
            getFileName({recordId: this.recordId }).then(result => {
                if(result== null || result== '')
            {
                this.imageUrl = Avatar;
                this.isImg = false;
            }
                else if(result){
                    if(result.Id){
                        this.isImg = true;
                        this.imageUrl = '/sfc/servlet.shepherd/version/download/'+result.Id;
                    } else {
                        this.isImg = false;
                        this.imageUrl = Avatar;
                    }
                }
                
            }).catch(error => {
                
            });
        })
        .catch((error) => {
            console.log(error);
        });
        this.addFile = false;
        this.uploadIds ='';
        this.uploadedFiles =[];
        this.fileName= [];
        this.fileData = [];
    }

    navigateToRefferal() {
        this[NavigationMixin.Navigate]({
            type: 'standard__objectPage',
            attributes: {
                objectApiName: 'Case',
                actionName: 'new'
            },
        });
    }
    closeErrorAlert(){
        this.isError = false;
            
    }
    errorMsg = '';
    isError = false;
    deleteProfilePic(){
        if (confirm('Do you want to delete profile picture?') == false) {
            return false;
          } 
        deleteProfilePic({recordId: this.recordId }).then(result => {
            if(result== null || result== '')
            {
                this.isImg = false;
            }
            else if(result){
                this.isImg = true;
            }
            this.errorMsg = '';
            this.isError = false;
            
        }).catch(error => {
            console.log(error);
            this.errorMsg = 'You are not able to delete the image due to INSUFFICIENT ACCESS. Please contact admin.';
            this.isError = true;
        });
    }
    navigateToEngmnt() {
        this[NavigationMixin.Navigate]({
            type: 'standard__objectPage',
            attributes: {
                objectApiName: 'Intake__c',
                actionName: 'new'
            },
        });
    }

    
    navigateToService() {
        var int = 1;
        this[NavigationMixin.Navigate]({
            type: 'standard__objectPage',
            attributes: {
                objectApiName: 'IFSP__c',
                actionName: 'new'
            },
            state: {
                useRecordTypeCheck: int,
                
            }
        });
    }


    showUpload = false;
    handleUpload()
    {
        this.showUpload = true;
    }

    closeImg()
    {
        showUpload = false;
    }

    handleCloseModal(){
        this.showUpload = false;
        this.addFile = false;
        this.uploadIds ='';
        this.uploadedFiles =[];
        this.fileName= [];
        this.fileData =[];
    }
    createNew(){
        var int = 1;
        /*if(this.criteria.includes('Diagnosis')){
            int = 1;
        }
        else if(this.criteria.includes('Medications')){
            int = 1;
        }
        else{
            int = 3;
        }*/

        if(this.sObjName == 'IFSP__c' && this.Title == 'ServicePlan'){
            this[NavigationMixin.Navigate]({
                type: 'standard__objectPage',
                attributes: {
                    objectApiName: this.sObjName,
                    actionName: 'new'
                },
                state: {
                    useRecordTypeCheck: int
                }
            });
        }

        else if(this.sObjName == 'Health__c'){
            this[NavigationMixin.Navigate]({
                type: 'standard__objectPage',
                attributes: {
                    objectApiName: this.sObjName,
                    actionName: 'new'
                },
                state: {
                    useRecordTypeCheck: int
                }
            });
        }
        else if(this.sObjName == 'Agreements__c'){
            this[NavigationMixin.Navigate]({
                type: 'standard__objectPage',
                attributes: {
                    objectApiName: this.sObjName,
                    actionName: 'new'
                },
                state: {
                    useRecordTypeCheck: int
                }
            });
        }

        else if(this.sObjName == 'ContentDocumentLink' && this.Title == 'Notes'){
            this[NavigationMixin.Navigate]({
                type: 'standard__objectPage',
                attributes: {
                    objectApiName: 'ContentNote',
                    actionName: 'new'
                },
                state: {
                    useRecordTypeCheck: int
                }
            });
        }

        else if(this.sObjName == 'ContentDocumentLink' && this.Title == 'Files'){
            this.handleUpload();
        }

        else if(this.sObjName == 'Task__c'){

            this[NavigationMixin.Navigate]({
                type: 'standard__recordPage',
                attributes: {
                    objectApiName: 'Task',
                    actionName: 'home'
                },
                state: {
                    useRecordTypeCheck: int
                }
            });
        }


        
        else{
            this[NavigationMixin.Navigate]({
                type: 'standard__objectPage',
                attributes: {
                    objectApiName: this.sObjName,
                    actionName: 'new'
                },
            });
           
            
        }
        
    }

    handleUploadFinished(event) {
        const uploadedFile = event.detail.files;
        this.uploadedFiles.push(event.detail.files);
        for(let i = 0; i < uploadedFile.length; i++) {
            if(this.uploadIds == ''){
                this.uploadIds=uploadedFile[i].contentVersionId;
            }
            else{
                this.uploadIds=this.uploadIds+','+uploadedFile[i].contentVersionId;
            }
            this.fileName.push(uploadedFile[i].name);
        }
        this.addFile = true;
        this.handleAddFile();
    }

    generateExcelSheet(){
        var recId = [];
        recId.push(this.recordId);
        generate({
            recId: recId
        })
        .then(result => {
            console.log('result:');
        })
    }


    /*connectedCallback(){
        console.log('connectedCallback servicePlanGoal');
        this.fetchRecords();
        
    }*/
    
    fetchRecords(){
        console.log('fetchRecords'); 
        this.newGoalCheck = false; 
        //this.goalRecords = [];      
        // getGoalRecords({
        //     recId: this.recordId
        // })
        // .then(result => {  
        //     this.goalRecords = result;
        //     console.log(this.goalRecords);            
        // })
        // .catch(error => {
        //    console.log(error);
        // });
    }

    newGoalClicked(){
        console.log('newGoalClicked');
        if(this.newGoalCheck == true){
            this.newGoalCheck = false;
            this.newGoalModal = false;
        }
        else{
            this.newGoalCheck = true;
        }
        //this.fetchRecords();
    }

    startAssessments(){
        this[NavigationMixin.Navigate]({
            "type": "standard__component",
            "attributes": {
                //Here customLabelExampleAura is name of lightning aura component
                //This aura component should implement lightning:isUrlAddressable
                "componentName": "c__startAssessment"
            }
        });
    }
}