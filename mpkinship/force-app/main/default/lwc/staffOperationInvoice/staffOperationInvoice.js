import { LightningElement, api, track, wire } from 'lwc';

import Stage from '@salesforce/schema/Payment__c.Invoice_Stage__c';
import InvoiceNumber from '@salesforce/schema/Payment__c.Invoice_Number__c';
import Vendor from '@salesforce/schema/Payment__c.Vendor__c';
import Description from '@salesforce/schema/Payment__c.Description__c';
import Category from '@salesforce/schema/Payment__c.Category_lookup__c';
import Payment_Method from '@salesforce/schema/Payment__c.Payment_Method__c';
import Amount from '@salesforce/schema/Payment__c.Payment_Amount__c';
import Accounting_Period from '@salesforce/schema/Payment__c.Accounting_Period__c';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import payment from '@salesforce/schema/Payment__c';
import { hideSpinner, showSpinner } from 'c/util';
import getInvoiceId from '@salesforce/apexContinuation/StaffAndOperationsController.getInvoiceId';
import getFIPS from '@salesforce/apexContinuation/StaffAndOperationsController.getFIPS';
import insertStaffAndOperation from '@salesforce/apexContinuation/StaffAndOperationsController.insertStaffAndOperation';
import uploadFile from '@salesforce/apexContinuation/StaffAndOperationsController.uploadFile';
import getFileName from '@salesforce/apexContinuation/StaffAndOperationsController.getFileName';
import getCostCenter from "@salesforce/apexContinuation/ROCController.getCostCenter";

import updateFipsCodeinInvoices from "@salesforce/apexContinuation/ROCController.updateFipsCodeinInvoices";
import lookUp from '@salesforce/apex/Lookup.search';
import { getRecordUi, getRecordNotifyChange, getFieldDisplayValue } from 'lightning/uiRecordApi';
const columns = [
    {
        label: "Title",
        fieldName: "titleLink",
        type: "url",
        typeAttributes: { label: { fieldName: "title" }, tooltip: "title", target: "_blank" }
    },
    {
        label: "Owner",
        fieldName: "ownerLink",
        type: "url",
        typeAttributes: { label: { fieldName: "owner" }, tooltip: "owner", target: "_blank" }
    },
];
export default class StaffOperationInvoice extends LightningElement {
    columns = columns;
    @api recordId;
    @api objectApiName;
    @track showLoader = false;
    @api isUpdate;
    @api opportunityid;
    @api parentfiscalyearid;
    @api selectedInvoice;
    @track selectedRecordId;
    @track fiscalyearId;
    @track accountingPeriod;
    @track paymentMethod = 'Check';
    @track isView = false;
    @track fund = '';
    recordTypeIdStr
    fields = [InvoiceNumber, Vendor, Amount, Payment_Method, Category, Stage, Description];
    @wire(getRecordUi, { recordIds: '$selectedInvoice', layoutTypes: 'Full', modes: 'View' })
    accountRecordUi({ error, data }) {
        if (data) {
            console.log(data, 'data')
        }
    }
    @wire(getObjectInfo, { objectApiName: payment })
    getObjectdata({ error, data }) {
        if (data) {

            console.log('data.defaultRecordTypeId ' + data.defaultRecordTypeId);
            console.log('data.defaultRecordTypeId ', data.recordTypeInfos);
            for (var key in data.recordTypeInfos) {
                if (data.recordTypeInfos[key].name == "Staff & Operations") {
                    this.recordTypeIdStr = data.recordTypeInfos[key].recordTypeId;
                }
            }
            // perform your custom logic here
        } else if (error) {
            // perform your logic related to error 
        }
    };

    catWhereStr = " AND (RecordType.Name = 'LOCAL Category' OR (RecordType.Name = 'LASER Category' AND Category_Type__c = 'Administrative')) AND Inactive__c = false";

    // catWhereStr = " AND Category_Type__c = 'Administrative' AND Inactive__c = false";
    disableCat = false;
    selectedCate = '';
    costCernterId = '';
    disableCostCenter = false;
    selectedRecords(event) {
        var ind = parseInt(event.target.dataset.id);
        let selectedRecords = JSON.parse(event.detail);
        //this.selectedCate = '';
        if (selectedRecords && selectedRecords.object === "Category__c") {
            if (selectedRecords.records.length > 0) {
                this.itemList[ind].selectedRecords = '';
                this.itemList[ind].selectedRecords = selectedRecords.records[0].recId;
                
                this.selectedCate  = '';
                for (var i = 0; i < this.itemList.length; i++) {
                    if (this.selectedCate == '') {
                        if(this.itemList[i].selectedRecords)
                        this.selectedCate = this.itemList[i].selectedRecords;
                    }
                    else {
                        if(this.itemList[i].selectedRecords)
                        this.selectedCate = this.selectedCate + ',' + this.itemList[i].selectedRecords;
                    }
                }
                for (var i = 0; i < this.itemList.length; i++) {
                    if (ind == this.itemList[i].id) {
                        this.itemList[i].disableCat = true;
                    }
                }
                //this.itemList[ind].disableCat = true;
                //this.disableCat = true;
                if (this.inEdit) {
                    this.disableCat = true;
                }
                this.getCostCenters(selectedRecords.records[0].recId,ind);
                //this.handleSelectYear();
            } else {
                for (var i = 0; i < this.itemList.length; i++) {
                    if (ind == this.itemList[i].id) {
                        this.itemList[i].disableCat = false;
                        this.itemList[i].showCostLookup = false;
                        this.itemList[i].selectedRecords = '';
                        this.selectedCate = '';
                    }
                }
                for (var i = 0; i < this.itemList.length; i++) {
                    if (this.selectedCate == '') {
                        if(this.itemList[i].selectedRecords && this.itemList[i].selectedRecords !='')
                        this.selectedCate = this.itemList[i].selectedRecords;
                    }
                    else {
                        if(this.itemList[i].selectedRecords && this.itemList[i].selectedRecords !='')
                        this.selectedCate = this.selectedCate + ',' + this.itemList[i].selectedRecords;
                    }
                }
                //this.itemList[ind].disableCat = false;
                //this.disableCat = false;
                //this.selectedCate = '';

            }
        }
         if (selectedRecords && selectedRecords.object === "Cost_Center__c") {
             if (selectedRecords.records.length > 0) {
                 this.itemList[ind].costCernterId = '';
                 this.itemList[ind].costCernterId = selectedRecords.records[0].recId;
                 this.costCernterId = '';
                 for (var i = 0; i < this.itemList.length; i++) {
                     if (this.costCernterId == '') {
                         if(this.itemList[i].costCernterId)
                         this.costCernterId = this.itemList[i].costCernterId;
                     }
                     else {
                         if(this.itemList[i].costCernterId)
                         this.costCernterId = this.costCernterId + ',' + this.itemList[i].costCernterId;
                     }
                 }
                 for (var i = 0; i < this.itemList.length; i++) {
                     if (ind == this.itemList[i].id) {
                         this.itemList[i].disableCostCenter = true;
                     }
                 }
                 if (this.inEdit) {
                     this.disableCostCenter = true;
                 }
             } else {
                 for (var i = 0; i < this.itemList.length; i++) {
                     if (ind == this.itemList[i].id) {
                         this.itemList[i].disableCostCenter = false;
                         this.itemList[i].costCernterId = '';
                         this.costCernterId = '';
                     }
                 }
                 for (var i = 0; i < this.itemList.length; i++) {
                     if (this.costCernterId == '') {
                         if(this.itemList[i].costCernterId && this.itemList[i].costCernterId !='')
                         this.costCernterId = this.itemList[i].costCernterId;
                     }
                     else {
                         if(this.itemList[i].costCernterId && this.itemList[i].costCernterId !='')
                         this.costCernterId = this.costCernterId + ',' + this.itemList[i].costCernterId;
                     }
                 }
                 //this.costCernterId = '';

             }
         }
    }
     getCostCenters(categoryId,ind) {
         getCostCenter({ categoryId: categoryId }).then((result) => {
             if (result) {
                 if (result.length > 1) {
                    
                     for (var i = 0; i < this.itemList.length; i++) {
                         if (ind == this.itemList[i].id) {
                             this.itemList[i].parentCostCenterName = '';
                             this.itemList[i].disableCostCenter = false;
                             this.itemList[i].costCenterIds = [];
                             result.forEach(ele => {
                                 this.itemList[i].costCenterIds.push(ele.Cost_Center__c);
                                 if(this.fipsCode == ele.FIPS__c){
                                     if(ele.S_O_Default__c == true)
                                     {
                                            let newsObject = [{ 'recId' : ele.Cost_Center__c ,'recName' : ele.Cost_Center__r.Name }];
                                            this.itemList[ind].parentCostCenterName = JSON.stringify(newsObject);
                                            this.itemList[ind].disableCostCenter  = true;
                                            for (var j = 0; j < this.itemList.length; j++) {
                                                if (this.costCernterId == '') {
                                                    if(this.itemList[j].costCenterIds)
                                                    this.costCernterId = this.itemList[j].costCenterIds[0];
                                                }
                                                else {
                                                    if(this.itemList[j].costCernterId)
                                                    this.costCernterId = this.costCernterId + ',' + this.itemList[j].costCenterIds[0];
                                                }
                                            }
                                        }
                                     }
                             });
                             this.itemList[i].showCostLookup = true;
                         }
                     }
                 }
                 else if (result.length == 1) {
                     result.forEach(ele => {
                         if (ele.Cost_Center__c) {
                             this.costCernterId = '';
                             this.itemList[ind].disableCostCenter = true;
                             this.itemList[ind].costCenterIds = [];
                             this.itemList[ind].costCenterIds.push(ele.Cost_Center__c);
                             this.itemList[ind].showCostLookup = true;
                             let newsObject = [{ 'recId': ele.Cost_Center__c, 'recName': ele.Cost_Center__r.Name }];
                            this.itemList[ind].parentCostCenterName = JSON.stringify(newsObject);
                             for (var i = 0; i < this.itemList.length; i++) {
                                 if (this.costCernterId == '') {
                                     if(this.itemList[i].costCenterIds)
                                     this.costCernterId = this.itemList[i].costCenterIds[0];
                                 }
                                 else {
                                     if(this.itemList[i].costCernterId)
                                     this.costCernterId = this.costCernterId + ',' + this.itemList[i].costCenterIds[0];
                                 }
                             }
                            
                             if (this.inEdit) {
                                 this.disableCostCenter = true;
                             }
                         }
                     });
                 }
                 else {
                     for (var i = 0; i < this.itemList.length; i++) {
                         if (ind == this.itemList[i].id) {
                             this.itemList[i].showCostLookup = false;
                         }
                     }
                     this.toastManager('error', 'Error', 'There is no Cost Center associated with this Category.');
                 }


             }
             else {
                 for (var i = 0; i < this.itemList.length; i++) {
                     if (ind == this.itemList[i].id) {
                         this.itemList[i].showCostLookup = false;
                     }
                 }
                 this.toastManager('error', 'Error', 'There is no Cost Center associated with this Category.');
             }
         })
             .catch((error) => {
                 //console.log(error);
             });
     }
    connectedCallback() {
        this.showAddDelete = true;
        console.log(this.opportunityid + ' this.selectedInvoice???');

        getInvoiceId({ InvoiceId: this.selectedInvoice }).then((result) => {
            console.log(result + ' this.getInvoiceId???');
            console.log(result.Fund__c + ' this.result.fund__c???');
            console.log(result.Category_lookup__c + ' this.result.Category_lookup__c???');

            if (result) {
                this.fund = result.Fund__c;
                this.selectedCate = result.Category_lookup__c;
            } else {
                this.fund = 'Reimbursable';
            }
            console.log(this.selectedCate + ' this.selectedCate ???');

        })
        .catch((error) => {
            this.fund = 'Reimbursable';
        });
        getFIPS({ parentfiscalyearid: this.parentfiscalyearid }).then((result) => {
            if (result) {
                this.fipsCode = result.FIPS__c;
            }
        })
        .catch((error) => {
        });
    }

    handleSelection(event) {
        this.fiscalyearId = event.detail;
        console.log("the selected record id is" + event.detail);
    }
    showEditHandle(event) {
        this.isView = false;
    }
    allnumeric(inputtxt) {
        var numbers = /^[0-9]+$/;
        if (inputtxt.match(numbers)) {
            return true;
        }
        else {
            return false;
        }
    }


    handleSubmit(event) {
        event.preventDefault(); // stop the form from submitting
        showSpinner(this);
        const fields = event.detail.fields;
        /*fields.Opportunity__c = this.opportunityid; // modify a field
        if(this.recordId == null || this.recordId == ''){
            fields.RecordTypeId = this.recordTypeIdStr; //'012180000009TyEAAU';
        }
        
        fields.fund__c = this.fund;
        /*if(fields.Invoice_Number__c != null){
            if(!this.allnumeric(fields.Invoice_Number__c)){
                this.toastManager('error', 'Error', 'Invoice number should be numeric.');
                event.preventDefault();
                hideSpinner(this);
                return false;
            }
        }/
        
        var singleInsert = false;
        if (this.selectedCate == null || this.selectedCate == '') {
            this.toastManager('error', 'Error', 'Category is required.');
            event.preventDefault();
            hideSpinner(this);
            return true;
        }
        else{
            if(this.selectedCate.includes(',')){
                singleInsert = false;
            }
            else{
                singleInsert = true;
            }
        }
        if (fields.Vendor__c == null) {
            this.toastManager('error', 'Error', 'Vendor is required.');
            event.preventDefault();
            hideSpinner(this);
            return true;
        }
        if (fields.Payment_Amount__c == null || fields.Payment_Amount__c == '') {
            this.toastManager('error', 'Error', 'Payment Amount is required.');
            event.preventDefault();
            hideSpinner(this);
            return true;
        }

        console.log('fields.Payment_Method__c  ', fields.Payment_Method__c);
        if (fields.Payment_Method__c == null) {
            this.toastManager('error', 'Error', 'Payment method is required.');
            event.preventDefault();
            hideSpinner(this);
            return true;
        }
        if (fields.Payment_Method__c == 'Journal Entry') {
            var today = new Date();
            fields.Payment_Date__c = today;
            fields.Invoice_Stage__c = 'Paid';
        }*/
        var isVal = true;
        
        this.template.querySelectorAll('lightning-input-field').forEach(element => {
            if (element.fieldName == 'Opportunity__c') {
                element.value = this.opportunityid;
            }
            if (element.fieldName == 'fund__c') {
                element.value = this.fund;
            }
            if (element.fieldName == 'Invoice_Number__c') {
                element.value = this.invoiceNumber;
            }
            if (element.fieldName == 'Description__c') {
                element.value = this.description;
            }
            if (element.fieldName == 'Scheduled_Date__c') {
                element.value = this.scheduledDate;
            }
            if (element.fieldName == 'Payment_Date__c') {
                if (this.showPD) {
                    if (this.paymentDate == null || this.paymentDate == '') {
                        this.toastManager('error', 'Error', 'Payment Date is required.');
                        event.preventDefault();
                        hideSpinner(this);
                        isVal = false;
                        return true;
                    }
                    else {
                        element.value = this.paymentDate;
                    }
                }

            }
            if (element.fieldName == 'Category_lookup__c') {
                console.log('Category???????',this.selectedCate);
                if (this.selectedCate == null || this.selectedCate == '') {
                    this.toastManager('error', 'Error', 'Category is required.');
                    event.preventDefault();
                    hideSpinner(this);
                    isVal = false;
                    return true;
                }
                 else {
                     for (var i = 0; i < this.itemList.length; i++) {
                         if(!this.itemList[i].showCostLookup){
                             this.toastManager('error', 'Error', 'There is no Cost Center associated with Category.');
                             event.preventDefault();
                             hideSpinner(this);
                             isVal = false;
                             return true;
                         } 
                     }
                     if(isVal){
                         if (this.selectedCate.includes(',')) {
                             const myArr = this.selectedCate.split(",");
                             element.value = myArr[0];
                             var a = this.selectedCate.indexOf(",");
                             a = a + 1;
                             this.selectedCate = this.selectedCate.substr(a, this.selectedCate.length);
                         }
                         else {
                             element.value = this.selectedCate;
                             //if(this.showAddDelete){
                                // this.selectedCate = '';
                             //}
                            
                         }
                     }
                    
                 }
                
            }

             if (element.fieldName == 'Cost_Center__c') 
             {   console.log('costcenterId???????',this.costCernterId);
                 if (this.costCernterId == null || this.costCernterId == '') {
                     this.toastManager('error', 'Error', 'Cost Center is required.');
                     event.preventDefault();
                     hideSpinner(this);
                     isVal = false;
                     return true;
                 }
                 else {
                     if(isVal){
                         if (this.costCernterId.includes(',')) {
                             const myArr = this.costCernterId.split(",");
                             element.value = myArr[0];
                             var a = this.costCernterId.indexOf(",");
                             a = a + 1;
                             this.costCernterId = this.costCernterId.substr(a, this.costCernterId.length);
                         }
                         else {
                             element.value = this.costCernterId;
                             //if(this.showAddDelete){
                             //    this.costCernterId = '';
                             //}
                            
                         }
                     }
                 }
             }
            if (element.fieldName == 'Vendor__c') {
                console.log('Vendor???????',this.vendor);
                if (this.vendor == null || this.vendor == '') {
                    this.toastManager('error', 'Error', 'Vendor is required.');
                    event.preventDefault();
                    hideSpinner(this);
                    isVal = false;
                    return true;
                }
                else {
                    if (this.vendor[0] == '0') {
                        element.value = this.vendor;
                    }
                    else {
                        element.value = this.vendor[0];
                    }

                }
            }
            if (element.fieldName == 'Payment_Amount__c') {
                if (element.value == null || element.value == '') {
                    this.toastManager('error', 'Error', 'Payment Amount is required.');
                    event.preventDefault();
                    hideSpinner(this);
                    isVal = false;
                    return true;
                }
            }
            if (element.fieldName == 'Payment_Method__c') {
                if (this.paymentMethod == null || this.paymentMethod == '') {
                    this.toastManager('error', 'Error', 'Payment method is required.');
                    event.preventDefault();
                    hideSpinner(this);
                    isVal = false;
                    return true;
                }
                else {
                    element.value = this.paymentMethod;
                }
            }
            var JournalEntry = false;
            if (element.fieldName == 'Payment_Method__c') {
                if (element.value == 'Journal Entry' || element.value == 'Credit Card') {
                    var today = new Date();
                    JournalEntry = true;
                    //element.Payment_Date__c = today;
                    element.Invoice_Stage__c = 'Pending';
                }
            }

            if (JournalEntry && element.fieldName == 'Invoice_Stage__c') {
                element.value == 'Pending';
            }
            
        });
        if (!this.showAddDelete) {
            fields.Id = this.selectedInvoice;
            fields.Opportunity__c = this.opportunityid;
            fields.fund__c = this.fund;
            fields.Invoice_Number__c = this.invoiceNumber;
            fields.Description__c = this.Description__c;
            fields.Scheduled_Date__c = this.scheduledDate;
            if (this.selectedCate == null || this.selectedCate == '') {
                this.toastManager('error', 'Error', 'Category is required.');
                event.preventDefault();
                hideSpinner(this);
                isVal = false;
                return true;
            }
             else {
                 fields.Category_lookup__c = this.selectedCate;
                 for (var i = 0; i < this.itemList.length; i++) {
                     if(!this.itemList[i].showCostLookup){
                         this.toastManager('error', 'Error', 'There is no Cost Center associated with Category.');
                         event.preventDefault();
                         hideSpinner(this);
                         isVal = false;
                         return true;
                     } 
                 }
             }
             if (this.costCernterId == null || this.costCernterId == '') {
                 this.toastManager('error', 'Error', 'Cost Center is required.');
                 event.preventDefault();
                 hideSpinner(this);
                 isVal = false;
                 return true;
             }
             else {
                 fields.Cost_Center__c = this.costCernterId;
             }
            if (this.showPD) {
                if (this.paymentDate == null || this.paymentDate == '') {
                    this.toastManager('error', 'Error', 'Payment Date is required.');
                    event.preventDefault();
                    hideSpinner(this);
                    isVal = false;
                    return true;
                }
                else {
                    fields.Payment_Date__c = this.paymentDate;

                }
            }
            if (this.vendor == null || this.vendor == '') {
                this.toastManager('error', 'Error', 'Vendor is required.');
                event.preventDefault();
                hideSpinner(this);
                isVal = false;
                return true;
            }
            else {
                if (this.vendor[0] == '0') {
                    fields.Vendor__c = this.vendor;
                }
                else {
                    fields.Vendor__c = this.vendor[0];
                }

            }

            if (fields.Payment_Amount__c == null || fields.Payment_Amount__c == '') {
                this.toastManager('error', 'Error', 'Payment Amount is required.');
                event.preventDefault();
                hideSpinner(this);
                isVal = false;
                return true;
            }

            if (this.paymentMethod == null || this.paymentMethod == '') {
                this.toastManager('error', 'Error', 'Payment method is required.');
                event.preventDefault();
                hideSpinner(this);
                isVal = false;
                return true;
            }
            else {
                fields.Payment_Method__c = this.paymentMethod;
            }

            var JournalEntry = false;
            if (fields.Payment_Method__c == 'Journal Entry' || fields.Payment_Method__c == 'Credit Card') {
                var today = new Date();
                JournalEntry = true;
                fields.Invoice_Stage__c = 'Pending';
            }
            if (isVal) {
                this.template.querySelector('lightning-record-edit-form').submit(fields);
            }
        }
            
        
        //if(this.showAddDelete)
        {
            if (isVal) {
                this.template.querySelectorAll('lightning-record-edit-form').forEach(element => {
                    element.submit();
                });
            }
        }



        /*if(singleInsert){
            this.template.querySelector('lightning-record-edit-form').submit(fields);
        }
        else{
            this.insertStaffAndOperations(fields.Invoice_Number__c,fields.Vendor__c,fields.Payment_Method__c,this.parentfiscalyearid,fields.Payment_Amount__c,fields.Invoice_Stage__c,fields.Fund__c,this.selectedCate,this.recordTypeIdStr,this.recordId,fields.Description__c,fields.Opportunity__c);
        }*/


    }
    @track description;
    @track paymentDate;
    @track scheduledDate;
    @track paymentAmount;
    @track invoiceNumber;
    @track vendor;
    
    insertStaffAndOperations(invoiceNumber, vendor, paymentMethod, parentfiscalyearid, paymentAmount, invoiceStage, fund, selectedCate, recordTypeIdStr, recordId, description, opportunity) {
        insertStaffAndOperation({ uploadIds: this.uploadIds, invoiceNumber: invoiceNumber, vendor: vendor, paymentMethod: paymentMethod, parentfiscalyearid: parentfiscalyearid, paymentAmount: paymentAmount, invoiceStage: invoiceStage, fund: fund, selectedCate: selectedCate, recordTypeIdStr: recordTypeIdStr, recordId: recordId, description: description, opportunity: opportunity }).then(result => {
            console.log(result + ' result in UpdateSobjectList ???');
            if (result) {
                this.disableCat = false;
                this.selectedCate = '';
                setTimeout(function () {
                    this.template.querySelector("c-sat-multi-select-lookup").setData();
                }.bind(this), 1000);
                result.forEach(function (key) {
                    console.log('this.selectedInvoice', this.selectedInvoice);
                    if (this.selectedInvoice != undefined && this.selectedInvoice != null && this.selectedInvoice != '') {
                        this.selectedInvoice = this.selectedInvoice + ',' + key.Id;
                    }
                    else {
                        this.selectedInvoice = key.Id;
                    }
                }.bind(this));

                /*updateFipsCodeinInvoices({ invoiceid: this.selectedInvoice }).then((result) => {
                    console.log(result + ' ???? result?????');
                })
                    .catch((error) => {
                        console.log(error);
                    });*/
                this.selectedInvoice = null;
                const inputFields = this.template.querySelectorAll(
                    'lightning-input-field'
                );
                this.description = '';
                this.paymentAmount = '';
                this.invoiceNumber = '';
                this.vendor = '';
                this.paymentMethod = '';
                hideSpinner(this);
                if (!this.isNext) {
                    this.dispatchEvent(new CustomEvent('closeinvoicecancel', {
                        detail: this.isNext
                    }));
                }
                this.showLoader = false;
                this.toastManager('success', 'Invoices have been made successfully!.', '');
            }

        }).catch(error => {
            this.toastManager('error', error, '');
            hideSpinner(this);
        });
    }
    @track stage = '';
    @track showPD = false;
    paymentMethodChange(event) {
        const temp = event.detail.value;
        if (temp == 'Check') {
            this.showPD = false;
            this.stage = 'Ready to Pay';
        } else if (temp == 'Journal Entry') {
            this.showPD = true;
            this.stage = 'Pending';
        } else if (temp == 'Credit Card') {
            this.showPD = true;
            this.stage = 'Pending';
        }
        this.paymentMethod = event.detail.value;

    }
    invoiceNumberChange(event) {
        this.invoiceNumber = event.detail.value;
    }
    vendorChange(event) {
        this.vendor = event.detail.value;
    }
    descriptionChange(event) {
        this.description = event.detail.value;
    }
    paymentDateChange(event) {
        this.paymentDate = event.detail.value;
    }
    scheduledDateChange(event) {
        this.scheduledDate = event.detail.value;
    }

    handleSuccess(event) {
        this.selectedInvoice = event.detail.id;
        uploadFile({ recordId: this.selectedInvoice, uploadIds: this.uploadIds }).then((result) => {
            console.log(result + ' ???? result?????');
        })
            .catch((error) => {
                console.log(error);
            });
        this.disableCat = false;
        this.selectedCate = '';
        this.costCernterId = '';
        setTimeout(function () {
            this.template.querySelector("c-sat-multi-select-lookup").setData();
        }.bind(this), 1000);


        /*updateFipsCodeinInvoices({ invoiceid: this.selectedInvoice }).then((result) => {
            console.log(result + ' ???? result?????');
        })
            .catch((error) => {
                console.log(error);
            });*/
        //this.isView = true;
        this.selectedInvoice = null;
        const inputFields = this.template.querySelectorAll(
            'lightning-input-field'
        );
        if (inputFields) {
            inputFields.forEach(field => {
                if (field.value != 'Reimbursable' && field.value != 'Non-Reimbursable') {
                    field.value = null;
                }

            });
        }
        this.template.querySelectorAll('lightning-record-edit-form').forEach(element => {
            if (element.value != 'Reimbursable' && element.value != 'Non-Reimbursable') {
                element.value = null;
            }
        });
        for (var i = 0; i < this.itemList.length; i++) {
            this.itemList[i].disableCat = false;
            this.itemList[i].disableCostCenter = false;
            this.itemList[i].showCostLookup = false;
        }
        for (var i = this.itemList.length; i >= 2; i--) {
            this.itemList = this.itemList.filter(function (element) {
                return parseInt(element.id) !== parseInt(event.target.accessKey);
            });
        }
        
        this.uploadIds = '';
        this.fileName = [];
        hideSpinner(this);
        if (!this.isNext) {
            this.dispatchEvent(new CustomEvent('closeinvoicecancel', {
                detail: this.isNext
            }));
        }


    }
    handleReset(event) {
        // const inputFields = this.template.querySelectorAll(
        //     'lightning-input-field'
        // );
        // if (inputFields) {
        //     inputFields.forEach(field => {
        //         field.reset();
        //     });
        // }
        this.isView = true;
    }
    isError = false;
    errorHandler(event) {
        if (event.detail.output.fieldErrors.Category_lookup__c) {
            this.toastManager('error', 'Category is Inactive', event.detail.output.fieldErrors.Category_lookup__c[0].message);
        }
        else if (event.detail.output.fieldErrors.Payment_Date__c) {
            this.toastManager('error', 'Error', event.detail.output.fieldErrors.Payment_Date__c[0].message);
        }
        hideSpinner(this);
        console.log('s');
        this.showAddDelete = true;
        this.isError = true;
    }

    @track isShowToast = false;
    @track toastType = '';
    @track title = '';
    @track message = '';
    @track toastIcon = 'utility:error';
    toastManager(toastType, title, message) {
        this.toastType = "slds-notify slds-notify_toast ";
        if (toastType == 'error') {
            this.toastType += 'slds-theme_error';
        } else if (toastType == 'success') {
            this.toastType += 'slds-theme_success';
        } else if (toastType == 'info') {
            this.toastType += 'slds-theme_info';
            this.toastIcon = 'utility:info';
        }
        this.title = title;
        this.message = message;
        this.isShowToast = true;
        setTimeout(function () {
            this.isShowToast = false;
        }.bind(this), 10000);
    }
    closeToast(event) {
        this.isShowToast = false;
        return false;
    }
    @track isSelectedRecord = false;
    @track selectedRecordName = false;
    @track selectedCategoryStr = '';
    @track inEdit = false;
    @track showAddDelete = true;
    @track fileData = [];
    @track showFile = true;
    @api
    setIsUpdate(isUpdate) {

        this.isUpdate = isUpdate;
        this.stage = 'Ready to Pay';
        if (isUpdate != null && isUpdate != '') {
            this.itemList[0].disableCat = true;
            this.showAddDelete = false;
            this.isView = false;
            getInvoiceId({ InvoiceId: this.isUpdate }).then((result) => {
                if (result) {
                    this.parentfiscalyearid = result.PO_Parent_Fiscal_Year__c;
                    this.selectedRecordName = result.Fiscal_Year_Name__c;
                    this.description = result.Description__c;
                    this.paymentDate = result.Payment_Date__c;
                    this.scheduledDate = result.Scheduled_Date__c;
                    this.invoiceNumber = result.Invoice_Number__c;
                    this.vendor = result.Vendor__c;
                    this.paymentMethod = result.Payment_Method__c;
                    if (this.paymentMethod == 'Check') {
                        this.showPD = false;
                    } else if (this.paymentMethod == 'Journal Entry' || this.paymentMethod == 'Credit Card') {
                        this.showPD = true;
                    }
                    this.isSelectedRecord = true;
                    this.stage = result.Invoice_Stage__c;
                    this.selectedCate = result.Category_lookup__c;
                    if(result.Category_lookup__c){
                         this.selectedCategoryStr = JSON.stringify([{ 'recId': result.Category_lookup__c, 'recName': result.Category_lookup__r.Name }]);
                         getCostCenter({ categoryId: result.Category_lookup__c}).then((result1) => {
                             if (result1) {
                                 this.itemList[0].costCenterIds = [];
                                 if(result1.length >= 1){
                                     result1.forEach(ele => {
                                         this.itemList[0].costCenterIds.push(ele.Cost_Center__c);
                                     });
                                     console.log(this.itemList[0].costCenterIds,'this.itemList[0].costCenterIds');
                                 }
                                 console.log(this.itemList[0].costCenterIds,'this.itemList[0].costCenterIds');
                             }
                         })
                         .catch((error) => {
                             console.log(error,'error');
                         });
                    }
                    console.log('rrrrresult.Cost_Center__c');
                    console.log(result.Cost_Center__c,'result.Cost_Center__c');
                     if(result.Cost_Center__c){
                         let newsObject = [{ 'recId': result.Cost_Center__c, 'recName': result.Cost_Center__r.Name }];
                         this.itemList[0].parentCostCenterName = JSON.stringify(newsObject);
                         this.itemList[0].showCostLookup = true;
                         this.itemList[0].disableCostCenter = true;
                         this.costCernterId = result.Cost_Center__c ;
                     }
                     else{
                         this.itemList[0].showCostLookup = false;
                     }
                    setTimeout(function () {
                        const staffOp = this.template.querySelectorAll(
                            'c-sat-multi-select-lookup'
                        );
                        if (staffOp) {
                            staffOp.forEach(op => {
                                op.setData();
                                this.disableCat = true;
                                this.inEdit = true;
                                //op.setIsUpdate(null);
                            });
                        }
                    }.bind(this), 1000);
                } else {
                    this.parentfiscalyearid = null;
                    this.isSelectedRecord = false;
                }
            })
                .catch((error) => {
                    this.isSelectedRecord = false;
                    this.parentfiscalyearid = null;
                    //console.log(error);
                });
            getFileName({ InvoiceId: this.isUpdate }).then((result) => {
                if (result.length > 0) {
                    this.showFile = false;
                    this.fileData = result;
                    var a = '';
                    if (result.length > 1) {
                        result.forEach(function (key) {
                            result.Title = key.ContentDocument.Title;
                            result.Owner = key.ContentDocument.Owner.Name;
                            /*if(a == ''){
                                a = key.ContentDocument.Title;
                            }
                            else{
                                a = a + ', '+key.ContentDocument.Title;
                            }*/

                        });
                        //this.fileName = a;
                    }
                    else {
                        result.Title = result[0].ContentDocument.Title;
                        result.Owner = result[0].ContentDocument.Owner.Name;
                        //this.fileName = result[0].ContentDocument.Title;
                    }

                }
            })
                .catch((error) => {
                    this.isSelectedRecord = false;
                    this.parentfiscalyearid = null;
                    //console.log(error);
                });
        } else {
            this.isSelectedRecord = false;
            this.selectedInvoice = '';
            //this.parentfiscalyearid = null;
            this.selectedRecordName = '';
            this.isView = false;
        }

    }

    invoicecancel() {
        this.isNext = undefined;
        this.dispatchEvent(new CustomEvent('closeinvoicecancel', {
            detail: this.isNext
        }));
    }
    @track isNext = true;
    saveOrNextHandle(event) {

        var flag = event.target.dataset.isnext;
        if (flag == "0") {
            this.isNext = false;
        }
        if (flag == "1") {
            this.isNext = true;
            this.selectedInvoice = null;
        }
        if (this.showAddDelete) {
            this.handleSubmit(event);
        }

    }

    get acceptedFormats() {
        return ['.pdf', '.png', '.jpg', '.jpeg', '.doc', '.xls', '.csv', '.xlsx'];
    }
    @track uploadIds = '';
    @track uploadedFiles = [];
    @track fileName = [];
    handleUploadFinished(event) {
        // Get the list of uploaded files
        //this.fileName = '';
        const uploadedFile = event.detail.files;
        this.uploadedFiles.push(event.detail.files);
        for (let i = 0; i < uploadedFile.length; i++) {
            if (this.uploadIds == '') {
                this.uploadIds = uploadedFile[i].contentVersionId;
                //this.fileName = uploadedFile[i].name;
            }
            else {
                this.uploadIds = this.uploadIds + ',' + uploadedFile[i].contentVersionId;
                //this.fileName = this.fileName + ', '+uploadedFile[i].name;
            }
            this.fileName.push(uploadedFile[i].name);
        }
        /*for(var i = 0 ; i< this.uploadedFiles.length ;i++){
            if(this.uploadedFiles.length>1){
                for(let j = 0; j < uploadedFile.length; j++){
                    if(this.fileName == ''){
                        this.fileName = this.uploadedFiles[i][j].name;
                    }
                    else{
                        this.fileName = this.fileName + ', '+this.uploadedFiles[i][j].name;
                    }
                }
                
                
            }
            else{
                this.fileName = this.uploadedFiles[i][0].name;
            }
        }*/

    }
    keyIndex = 0;
    @track itemList = [
        {
            id: 0,
            amount: 0
        }
    ];
    get getTotalAmountFun() {
        var total = 0;
        this.itemList.forEach(e => {
            total += e.amount;
        });
        return total;
    }
    addRow() {
        ++this.keyIndex;
        var newItem = [{ id: this.keyIndex, amount: 0 }];
        this.itemList = this.itemList.concat(newItem);
        //this.hideDeleteButton= true;
    }
    hideDeleteButton = false;
    removeRow(event) {
        if (this.itemList.length >= 2) {
            this.itemList = this.itemList.filter(function (element) {
                return parseInt(element.id) !== parseInt(event.target.accessKey);
            });
            this.keyIndex--;
            //this.hideDeleteButton= false;
        }
    }
    @track showTotalAmount = false;
    paymentAmountChange(event) {
        this.showTotalAmount = true;
        var ind = parseInt(event.target.dataset.id);
        if (event.detail.value && event.detail.value != '') {
            for (var i = 0; i < this.itemList.length; i++) {
                if (ind == this.itemList[i].id) {
                    this.itemList[i].amount = parseFloat(event.detail.value);
                }
            }

        } else {
            for (var i = 0; i < this.itemList.length; i++) {
                if (ind == this.itemList[i].id) {
                    this.itemList[i].amount = 0;
                }
            }
        }

    }
}