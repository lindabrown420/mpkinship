import { LightningElement, api, track, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent'
import { NavigationMixin } from 'lightning/navigation';
import Opportunity from '@salesforce/schema/Opportunity';
import ACCOUNTING_PERIOD_OBJECT from '@salesforce/schema/Campaign';
import LOCALE from '@salesforce/i18n/locale';
import CURRENCY from '@salesforce/i18n/currency';
import begindate_FIELD from '@salesforce/schema/Opportunity.CloseDate';
import BatchNumber from '@salesforce/schema/Opportunity.Voucher_Number__c';
import checkStub from '@salesforce/schema/Payment__c.Check_Stub__c';
import VendorName from '@salesforce/schema/Opportunity.AccountId';
import Stage from '@salesforce/schema/Opportunity.StageName';
import localalityCode from '@salesforce/schema/Payment__c.Locality_Account_Codes__c';
//import ParentFiscalYear from '@salesforce/schema/Opportunity.CampaignId';Accounting_Period__c
import ParentFiscalYear from '@salesforce/schema/Opportunity.Accounting_Period__c';
import getInvoices from '@salesforce/apexContinuation/StaffAndOperationsController.getInvoices';
import getInvoicesList from '@salesforce/apexContinuation/StaffAndOperationsController.getInvoicesList';

import getPaymentInvoices from '@salesforce/apexContinuation/StaffAndOperationsController.getPaymentInvoices';
import getSobjectListResults from '@salesforce/apexContinuation/MultiSelectLookupController.getSobjectListResults';


import getOpportunityRecord from '@salesforce/apexContinuation/StaffAndOperationsController.getOpportunityRecord';
import getOpportunityRecordByComp from '@salesforce/apexContinuation/StaffAndOperationsController.getOpportunityRecordByComp';
import deleteInvoice from "@salesforce/apexContinuation/StaffAndOperationsController.deleteInvoice";
//import isGUAAllocationCreated from "@salesforce/apexContinuation/StaffAndOperationsController.isGUAAllocationCreated";
import { getRecordUi, getRecordNotifyChange } from 'lightning/uiRecordApi';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import RecordTypeId from '@salesforce/schema/Opportunity.RecordTypeId';


const columns = [

    {
        label: "Invoice Name",
        fieldName: "Record_view__c",
        type: "url",
        sortable: true,
        typeAttributes: {
            label: { fieldName: "Name" },
            target: "_blank",
            tooltip: { fieldName: "Name" }
        }
    },
    { label: 'Invoice Number', fieldName: 'Invoice_Number__c', fixedWidth: 180 },
    { label: 'Vendor', fieldName: 'Vendor_Name__c', fixedWidth: 180 },
    { label: 'Description', fieldName: 'Description__c', fixedWidth: 180, type: 'text', wrapText: true },
    { label: 'Category', fieldName: 'Category_Name__c', fixedWidth: 180 },
    { label: 'Payment Method', fieldName: 'Payment_Method__c', fixedWidth: 180 },
    { label: 'Fiscal Year', fieldName: 'Fiscal_Year_Name__c', fixedWidth: 180 },
    { label: 'Stage', fieldName: 'Invoice_Stage__c', fixedWidth: 120 },
    { label: 'Amount', fieldName: 'Payment_Amount__c', fixedWidth: 120 },
    {
        label: "",
        type: "button-icon",
        typeAttributes: {
            iconName: "utility:edit",
            //label: 'Note',
            name: "editInvoice",
            title: "Edit Invoice",
            disabled: false,
            value: { fieldName: "Id" }
        },
        fixedWidth: 50
    },
    {
        label: "",
        type: "button-icon",
        typeAttributes: {
            iconName: "utility:delete",
            //label: 'Note',
            name: "deleteInvoice",
            title: "Delete Invoice",
            disabled: false,
            value: { fieldName: "Id" }
        },
        fixedWidth: 50
    },
    {
        label: "",
        type: "button-icon",
        typeAttributes: {
            iconName: "standard:custom",
            //label: 'Note',
            name: "gauallocaion",
            title: "Manage Payment Allocations",
            disabled: false,
            value: { fieldName: "Id" }
        },
        fixedWidth: 40
    },

]
const allColumns = [

    { label: 'General Accounting Units', fieldName: 'GAUName' },
    { label: 'Amount', fieldName: 'Amount' },

]

const column = [{ label: "Voucher Number", type: "text", fieldName: "invoicenumber", typeAttributes: {}, cellAttributes: { alignment: "left" } },
{ label: "Accounting Period Name ", type: "text", fieldName: "accountingperiodname", typeAttributes: {}, cellAttributes: { alignment: "left" } },
{ typeAttributes: {}, cellAttributes: { alignment: "center" }, label: "Status", type: "text", fieldName: "status" },
{ typeAttributes: {}, cellAttributes: { alignment: "center" }, label: "Total Invoices", type: "currency", fieldName: "totalinvoices" },
{ typeAttributes: {}, cellAttributes: { alignment: "center" }, label: "Total Adjustments", fieldName: "totaladjustments", type: "currency" },
{ typeAttributes: {}, cellAttributes: { alignment: "center" }, label: "Total Amount", type: "currency", fieldName: "totalamount" },
{
    typeAttributes: {
        rowActions: [{ label: "Add Invoices\u00A0", value: "value", $position: 0 }, { label: "Add Adjustments", value: "value", $position: 1 },
        { label: "Post Accounting Period", value: "value", $position: 2 }], menuAlignment: "right"
    }, cellAttributes: {}, label: "Actions", type: "action", fieldName: "actions"
}];



export default class AddStaffAndOperations extends LightningElement {

    @api recordId;
    @api objectApiName;
    @track parentfiscalyearid;
    @track sfdcBaseURL = window.location.origin;
    @track isRecordCreated = false;
    @track displayType = 'edit';
    @track toastIcon = 'utility:error';
    @track rectYPEvalue = 'Staff & Operations';
    @track isAddRecepit = true;
    @track relatedtInvoices = [];
    @track selectedinvoice;
    @track isModalOpen = false;
    @track isModalOpenA = false;
    @track disableAP = false;
    @track formtitle = 'Add Invoice';







    whereStr = '';
    apRTId = '';
    fields = [BatchNumber, begindate_FIELD, ParentFiscalYear, Stage, checkStub];
    columns = columns;
    allColumns = allColumns;
    recordTypeIdStr
    @track manageURL = this.sfdcBaseURL + '/apex/npsp__ALLO_ManageAllocations?id=';
    activeSections = ['A'];

    @wire(getObjectInfo, { objectApiName: Opportunity })
    getObjectdata({ error, data }) {
        if (data) {

            //console.log('data.defaultRecordTypeId ' + data.defaultRecordTypeId);
            //console.log('data.defaultRecordTypeId ', data.recordTypeInfos);
            for (var key in data.recordTypeInfos) {
                console.log(data.recordTypeInfos[key].name + ' data.recordTypeInfos[key].name???');

                if (data.recordTypeInfos[key].name == "Staff & Operations") {

                    this.recordTypeIdStr = data.recordTypeInfos[key].recordTypeId;
                    console.log(this.recordTypeIdStr + ' this.recordTypeIdStr???');
                }
            }
            // perform your custom logic here
        } else if (error) {
            // perform your logic related to error 
        }
    };
    @wire(getObjectInfo, { objectApiName: ACCOUNTING_PERIOD_OBJECT })
    Function({ error, data }) {
        if (data) {
            console.log('Data ', data);
            for (var key in data.recordTypeInfos) {
                console.log(data.recordTypeInfos[key].name + ' data.recordTypeInfos[key].name???');

                if (data.recordTypeInfos[key].name == "LASER Reporting Period") {

                    this.apRTId = data.recordTypeInfos[key].recordTypeId;
                    this.whereStr = " AND RecordTypeId = '" + this.apRTId + "' ";
                }
            }
            // perform your custom logic here
        } else if (error) {
            // perform your logic related to error 
        }
    };
    sum = 0;
    @track disablebutton;
    @track isEdit = true;
    @track apWithVouchers;
    connectedCallback() {
        console.log(this.recordId + ' this.recordId???');
        getSobjectListResults({ ObjectName: 'Campaign', fieldNames: ['Name', 'FIPS__c', 'FIPS__r.Name', 'Status', 'StartDate', 'EndDate', 'IsActive', "(Select Id,Total_Amount__c,Total_Adjustment__c, Name,Accounting_Period__r.Name,Accounting_Period__c,Accounting_Period__r.Current_Status__c, StageName, Accounting_Period__r.IsActive From Purchase_Orders__r Where RecordType.Name = 'Staff & Operations' AND Accounting_Period__r.parent.IsActive = true AND Accounting_Period__r.parent.parent.IsActive = true Order by Name)"], WhereClause: "Name Like '%LASER%'" }).then(result => {
            if (result) {
                var data = result;
                var tempIndexList = [];
                var dataList = [];
                var counter = -1;
                var fipsMap = [];
                result.forEach(each => {
                    if (fipsMap[each.FIPS__c] == undefined) {
                        counter++;
                        fipsMap[each.FIPS__c] = counter;
                        //tempIndexList[each.FIPS__c] = [];
                        tempIndexList[counter] = {};
                        tempIndexList[counter].FIPSName = each.FIPS__r.Name;
                        tempIndexList[counter].FIPSId = each.FIPS__r.Id;
                        tempIndexList[counter].vouchers = [];
                    }
                    if (each.Purchase_Orders__r) {
                        var tempFips = each.FIPS__c;
                        each.Purchase_Orders__r.forEach(PO => {
                            PO.APName = PO.Accounting_Period__r.Name;
                            PO.sum = PO.Total_Amount__c + PO.Total_Adjustment__c;
                            if (PO.sum <= 0) {
                                PO.showPeriod = true;
                            }
                            else {
                                PO.showPeriod = false;
                            }
                            tempIndexList[fipsMap[tempFips]].vouchers.push(PO);
                        });
                    }
                    //console.log('invoice amount????????????',PO.Total_Amount__c);
                    /*if(tempIndexList[each.FIPS__c]){
                        if(each.Purchase_Orders__r){
                            each.Purchase_Orders__r.forEach(PO => {
                                tempIndexList[each.FIPS__c].vouchers.push(PO);        
                            })
                        }
                    }
                    else{
                        tempIndexList[each.FIPS__c] = [each.FIPS__r.Name];
                        tempIndexList[each.FIPS__c].FIPSName = each.FIPS__r.Name;
                        tempIndexList[each.FIPS__c].FIPSId = each.FIPS__r.Id; 
                        tempIndexList[each.FIPS__c].vouchers = [];
                        dataList[tempIndexList[each.FIPS__c]] = [];
                            if(each.Purchase_Orders__r){
                                each.Purchase_Orders__r.forEach(PO => {
                                    tempIndexList[each.FIPS__c].vouchers.push(PO);        
                                })
                            }
                            dataList[tempIndexList[each.FIPS__c]].push(tempIndexList[each.FIPS__c]);
                    }*/


                });
                tempIndexList.forEach(e => {
                    console.log('e ', e);
                });

                data.map(item => {
                    if (item.Purchase_Orders__r) {
                        item.isVouchers = true;
                        item.Name = item.Name + " (" + item.Purchase_Orders__r.length + ")";
                        console.log('Name????????????', item.Name);
                        console.log('NameId????????????', item.Id);

                        item.Purchase_Orders__r.map(po => {
                            po.hrefP = '/lightning/n/LASER_Month_End_Close?c__apid=' + po.Accounting_Period__c;
                            console.log('hrefA?????', po.hrefP)
                            /*if(po.StageName == 'Open'){
                                po.href = "/" + po.Id;
                            }else{
                                po.href = "#";
                            }*/
                            po.href = "/" + po.Id;

                            console.log('Id????????????', po.href);
                            if (po.StageName == 'Open') {
                                po.isClosed = false;
                            }
                            else {
                                po.isClosed = true;
                            }
                            if (po.Accounting_Period__r.Current_Status__c == 'Current') {
                                po.isCurrent = true;
                            }
                            else {
                                po.isCurrent = false;
                            }
                        });
                    } else {
                        item.isVouchers = false;
                        item.Name = item.Name + " (0)";
                    }

                    return item;


                });
                tempIndexList = this.sort_by_key(tempIndexList, 'FIPSName');
                
                tempIndexList.forEach(ele => {
                    if(ele.vouchers){
                        ele.vouchers = this.sort_by_key(ele.vouchers, 'Name');
                    }
                    
                });
                this.apWithVouchers = tempIndexList;
                console.log('AP with Staff and Operation ', this.apWithVouchers);


            }
        }).catch(error => {
            console.log(error);
        });

        if (this.recordId != null && this.recordId != '') {
            this.displayType = "view";
            this.isRecordCreated = true;
            this.isAddRecepit = true;

            getOpportunityRecord({ OppId: this.recordId }).then((result) => {
                if (result) {
                    console.log(result.StageName + 'result.StageName ???');
                    this.parentfiscalyearid = result.Accounting_Period__c;
                    if (result.StageName == 'Closed' || result.StageName == 'Posted') {
                        this.disablebutton = 'disabled';
                    }
                    if (result.StageName == 'Closed')
                    {
                        this.isEdit = false;
                    }
                }
            })
                .catch((error) => {
                    //console.log(error);
                });

            this.getInvoices();
            //this.getInvoicesList();
            this.getPaymentInvoices();
        }

    }

    sort_by_key(array, key) {
        return array.sort(function (a, b) {
            var x = a[key]; var y = b[key];
            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
        });
    }

    @track total;
    @track subTotal;
    getPaymentInvoices() {
        var sum = 0;
        var subSum;
        var totalSum = 0;
        getPaymentInvoices({ OppId: this.recordId }).then(result => {
            console.log('NewPaymentresult????',result);
            if (result) {
                 result.forEach(e => {

                     if(e.payments)
                     {  sum = 0;
                         e.payments.forEach(pay => {
                            if(pay.Amount )
                            {
                                sum += pay.Amount
                                pay.sum = sum;
                                subSum = pay.sum;
                                
                                console.log('Subsumamounts??????',pay.sum);
                                
                            }
                            // else
                            // {
                            //     pay.sum = 0;
                            //     subSum = pay.sum;
                            // }
                                                        
                            if (pay.Stage == 'Paid') {
                             pay.isPaid = true;
                             } else {
                                 pay.isPaid = false;
                             }
                             if (pay.Stage == 'Pending') {
                                 pay.isPending = true;
                             }
                             else {
                                 pay.isPending = false;
                             }
                             if (pay.RecordtypeName == 'Adjustments') {
                                 pay.isAdjustment = true;
                             }
                             else {
                                 pay.isAdjustment = false;
                             }
                             
                             
                            
                             

                         }); 
                         
                         console.log('subSum??????',subSum);
                         if(subSum)
                         {
                            e.subTotal = subSum;
                         }
                         else
                         {  
                            subSum = 0 
                            e.subTotal = subSum;
                         }
                         console.log('e.subToltal??????',e.subTotal);
                         e.formattedNumber = new Intl.NumberFormat(LOCALE, {
                            style: 'currency',
                            currency: CURRENCY,
                            currencyDisplay: 'symbol'
                        }).format(e.subTotal);
                         e.Title = e.localityaccountName + ' ' + '('+ 'Total Amount: '+e.formattedNumber + ')';
                         totalSum += subSum;
                        
                        
                     }                       
                    
                     
                 });
                 
                 this.total = totalSum;
                console.log('Total??????????',this.total);
                this.relatedtInvoices = result;
               
                console.log('IfrelatedtInvoices??????',this.relatedtInvoices);
            } else {
                this.relatedtInvoices = [];
                
            }

        }).catch(error => {

        });
    }

    // getInvoicesList() {
    //     getInvoicesList({ OppId: this.recordId }).then(result => {
    //         //console.log('getInvoicesresult????',result);
    //         if (result) {
    //             result.forEach(e => {
                    
    //                 if (e.Stage == 'Paid') {
    //                     e.isPaid = true;
    //                 } else {
    //                     e.isPaid = false;
    //                 }
    //                 if (e.Stage == 'Pending') {
    //                     e.isPending = true;
    //                 }
    //                 else {
    //                     e.isPending = false;
    //                 }
    //                 if (e.RecordtypeName == 'Adjustments') {
    //                     e.isAdjustment = true;
    //                 }
    //                 else {
    //                     e.isAdjustment = false;
    //                 }
    //             });
    //             //this.relatedtInvoices = result;
    //             //console.log('IfrelatedtInvoices??????',this.relatedtInvoices );
    //         } else {
    //             //this.relatedtInvoices = [];
    //         }

    //     }).catch(error => {

    //     });
    // }
    selectedAP(event) {
        let selectedRecords = JSON.parse(event.detail);

        if (selectedRecords && selectedRecords.object === "Campaign") {
            if (selectedRecords.records.length > 0) {
                this.CampaignId = selectedRecords.records[0].recId;
                this.disableAP = true;
                this.handleSelectYear();
            } else {
                this.disableAP = false;
                this.CampaignId = '';

            }
        }
    }


    getInvoices() {
        //this.getInvoicesList();
        this.getPaymentInvoices();
        /*getInvoices({ OppId: this.recordId }).then((result) => {
            if (result) {
                console.log(this.result + ' this.result???');
                //this.relatedtInvoices = result;
            } else {
                //this.relatedtInvoices = [];
            }

        })
            .catch((error) => {
                //console.log(error);
            });*/
    }

    handleSubmit(event) {
        console.log(this.recordTypeIdStr + ' this.recordTypeIdStr???');

        var fields = event.detail.fields;
        console.log(fields + ' fields???');
        fields.RecordTypeId = this.recordTypeIdStr;
        //this.parentfiscalyearid = fields.CampaignId;
        this.parentfiscalyearid = fields.Accounting_Period__c;
        this.isUpdate = false;
        this.template.querySelector('lightning-record-form').submit(fields);
    }

    handleSectionToggle(event) {
        const openSections = event.detail.openSections;
    }

    handleSuccess(event) {
        this.isRecordCreated = true;
        this.isAddRecepit = true;
        this.recordId = event.detail.id;

    }
    handleReset(event) {
        event.preventDefault();
        //this.navigateToObjectHome();
    }
    @track isShowToast = false;
    @track toastType = '';
    @track title = '';
    @track message = '';
    toastManager(toastType, title, message) {
        this.toastType = "slds-notify slds-notify_toast ";
        if (toastType == 'error') {
            this.toastType += 'slds-theme_error';
            this.toastIcon = 'utility:error';
        } else if (toastType == 'success') {
            this.toastType += 'slds-theme_success';
            this.toastIcon = 'utility:success';
        } else if (toastType == 'info') {
            this.toastType += 'slds-theme_info';
            this.toastIcon = 'utility:info';
        }
        this.title = title;
        this.message = message;
        this.isShowToast = true;
        setTimeout(function () {
            this.isShowToast = false;
        }.bind(this), 10000);
    }
    handleManageClose(event) {

       /* isGUAAllocationCreated({ invoiceId: this.selectedinvoice }).then(result => {
            if (result) {
                this.manageShow = false;
            } else {
                this.toastManager('error', 'GUA Allocation is required.', '');
            }
        }).catch(error => { }) */
    }
    closeToast(event) {
        this.isShowToast = false;
        return false;
    }
    // add Receipt Click
    handleClick() {
        this.formtitle = 'Add Invoice';
        this.isModalOpen = true;
        this.isRecordCreated = true;
        this.selectedinvoice = '';
        this.isOpenGAUAll = true;
        this.isUpdate = false;
        //c/addReceiptthis.isUpdate = true;
        setTimeout(function () {
            const staffOp = this.template.querySelectorAll(
                'c-staff-operation-invoice'
            );

            if (staffOp) {
                staffOp.forEach(op => {
                    op.setIsUpdate(null);
                });
            }
        }.bind(this), 1000);
    }
    handleAddAdjustment() {
        this.formtitle = 'Add Adjustment';
        this.isModalOpenA = true;
        this.isRecordCreated = true;
        this.selectedinvoice = '';
        this.isOpenGAUAll = true;
        this.isUpdate = false;
        //c/addReceiptthis.isUpdate = true;
        setTimeout(function () {
            const staffOp = this.template.querySelectorAll(
                'c-staff-operation-adjustment'
            );

            if (staffOp) {
                staffOp.forEach(op => {
                    op.setIsUpdate(null);
                });
            }
        }.bind(this), 1000);
    }
    closeModal(event) {
        var isNext = event.detail;
        if (isNext) {
            this.isModalOpen = true;
            this.isModalOpenA = true;
        } else {
            // to close modal set isModalOpen tarck value as false
            this.isModalOpen = false;
            this.isModalOpenA = false;
            if (this.isAdjustmentOpen == true || this.isInvoice == true) {
                this.isAdjustmentOpen = false;
                this.isInvoice = false;
                this.isRecordCreated = false;
                this.recordId = false;
                if (isNext != null && isNext != true) {
                    if (this.selectedVoucher != null && this.selectedVoucher != '') {
                        window.open("/" + this.selectedVoucher, '_blank');
                        this.selectedVoucher = '';
                    }
                }

            }

            this.getInvoices();
            if (this.isOpenGAUAll && this.selectedinvoice != null && this.selectedinvoice != '' && this.selectedinvoice != 1) {
                //this.openGAUAllocaion();
            } else {
                this.isOpenGAUAll = false;
            }
        }

    }
    closeInvoiceModal() {
        this.isModalOpen = false;
        this.isModalOpenA = false;
        if (this.isAdjustmentOpen == true || this.isInvoice == true) {
            this.isAdjustmentOpen = false;
            this.isInvoice = false;
            this.isRecordCreated = false;
            this.recordId = false;
        }

    }
    @track selectedInvoiceFYear;
    handleRowAction(event) {
        const action = event.detail.action;

        const row = event.detail.row;

        switch (action.name) {
            case "editInvoice":
                this.selectedinvoice = row.Id;
                this.isRecepit = true;
                this.isModalOpen = true;
                this.isUpdate = true;
                if (row.RecordtypeName == 'Adjustments') {
                    this.formtitle = 'Edit Adjustment';
                    setTimeout(function () {
                        const staffOp = this.template.querySelectorAll(
                            'c-staff-operation-adjustment'
                        );
                        if (staffOp) {
                            staffOp.forEach(op => {
                                op.setIsUpdate(this.selectedinvoice);
                                //op.setIsUpdate(null);
                            });
                        }
                    }.bind(this), 1000);
                }
                else {
                    this.formtitle = 'Edit Invoice';
                    setTimeout(function () {
                        const staffOp = this.template.querySelectorAll(
                            'c-staff-operation-invoice'
                        );
                        if (staffOp) {
                            staffOp.forEach(op => {
                                op.setIsUpdate(this.selectedinvoice);
                                //op.setIsUpdate(null);
                            });
                        }
                    }.bind(this), 1000);
                }
                break;
            case "deleteInvoice":
                this.selectedinvoice = row.Id;
                this.deleteConfirmation();
                break;

            case "gauallocaion":
                //if (row.Residental__c == "LASER Administrative")
                {
                    this.selectedinvoice = row.Id;
                    this.openGAUAllocaion();


                }

                break;
        }
    }
    handlePayInvoices(event) {
        window.open(this.sfdcBaseURL + '/lightning/n/Pay_Invoices?c__poid=' + this.recordId);
    }
    @track recordTypeOfInvoice = '';
    editInvoice(event) {
        this.recordTypeOfInvoice = event.target.dataset.recordtype;
        this.selectedinvoice = event.target.dataset.id;
        this.isRecepit = true;
        this.isUpdate = true;

        if (this.recordTypeOfInvoice == 'Adjustments') {
            this.isModalOpenA = true;
            this.formtitle = 'Edit Adjustment';
            setTimeout(function () {
                const staffOp = this.template.querySelectorAll(
                    'c-staff-operation-adjustment'
                );
                if (staffOp) {
                    staffOp.forEach(op => {
                        op.setIsUpdate(this.selectedinvoice);

                    });
                }
            }.bind(this), 1000);
        }
        else {

            this.isModalOpen = true;
            this.formtitle = 'Edit Invoice';
            setTimeout(function () {
                const staffOp = this.template.querySelectorAll(
                    'c-staff-operation-invoice'
                );
                if (staffOp) {
                    staffOp.forEach(op => {
                        op.setIsUpdate(this.selectedinvoice);
                        //op.setIsUpdate(null);
                    });
                }
            }.bind(this), 1000);
        }


    }
    isAdjustmentOpen = false;
    isInvoice = false;
    selectedVoucher = '';
    recordIdd = '';
    openAdjustment(event) {
        this.selectedVoucher = event.target.dataset.id;
        this.selectedAp = event.target.dataset.apid;
        this.formtitle = 'Add Adjustment';
        this.selectedinvoice = '';
        this.isUpdate = false;

        //c/addReceiptthis.isUpdate = true;
        getOpportunityRecordByComp({ campId: this.selectedAp }).then(result => {
            this.recordIdd = result.Id;
            this.isAdjustmentOpen = true;
            setTimeout(function () {
                const staffOp = this.template.querySelectorAll(
                    'c-staff-operation-adjustment'
                );

                if (staffOp) {
                    staffOp.forEach(op => {

                        op.setIsUpdate(null);
                    });
                }
            }.bind(this), 1000);
        }).catch(error => { });

        //window.open( "/" + this.selectedinvoice ,'_blank');
    }
    openInvoice(event) {
        this.isInvoice = true;
        this.selectedVoucher = event.target.dataset.id;
        this.selectedAp = event.target.dataset.apid;
        this.formtitle = 'Add Invoice';
        this.selectedinvoice = '';
        this.isUpdate = false;
        //c/addReceiptthis.isUpdate = true;
        getOpportunityRecordByComp({ campId: this.selectedAp }).then(result => {
            this.recordIdd = result.Id;
            setTimeout(function () {
                const staffOp = this.template.querySelectorAll(
                    'c-staff-operation-invoice'
                );

                if (staffOp) {
                    staffOp.forEach(op => {
                        op.setIsUpdate(null);
                    });
                }
            }.bind(this), 1000);
        }).catch(error => { });


        //window.open( "/" + this.selectedinvoice ,'_blank');
    }
    deleteInvoice(event) {
        this.selectedinvoice = event.target.dataset.id;
        this.deleteConfirmation();
    }
    viewInvoice(event) {
        window.open("/" + event.target.dataset.id);

    }
    payInvoice(event) {
        window.open(this.sfdcBaseURL + '/lightning/n/Pay_Invoices?c__pid=' + event.target.dataset.id);
    }
    gauallocaion(event) {
        this.selectedinvoice = event.target.dataset.id;
        this.openGAUAllocaion();
    }
    isOpenGAUAll = false;

    openGAUAllocaion() {
        this.manageShow = true;
        this.manageURL = this.sfdcBaseURL + '/apex/npsp__ALLO_ManageAllocations?id=' + this.selectedinvoice;
        this.isOpenGAUAll = false;
    }
    @track manageShow = false;
    handleManage(event) {
        this.manageShow = true;
        this.manageURL = this.sfdcBaseURL + '/apex/npsp__ALLO_ManageAllocations?id=' + this.selectedinvoice;
        console.log(this.manageURL, ' this.manageURL????');
    }
    @track showDeleteConfirm = false;
    deleteConfirmation() {
        this.showDeleteConfirm = true;
    }
    closesDeleteConfirm(event) {
        this.showDeleteConfirm = false;
    }
    campaignIdChange(event) {
        this.CampaignId = event.detail.value;
    }
    handleSelectYear(event) {
        getOpportunityRecordByComp({ campId: this.CampaignId }).then(result => {
            this.recordId = result.Id;
            this.isRecordCreated = true;
            this.getInvoices();
        }).catch(error => { });
    }



    deleteInvoiceHandle(event) {
        deleteInvoice({ InvoiceID: this.selectedinvoice }).then((result) => {
            if (result) {
                this.getInvoices();
                this.toastManager('success', 'The Invoice has been deleted.', '');
                this.closesDeleteConfirm(null);
                this.refreshROC();
            }
        })
            .catch((error) => {
                //console.log(error);
            });
    }
}