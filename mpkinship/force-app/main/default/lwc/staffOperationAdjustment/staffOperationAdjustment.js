import { LightningElement, api, track, wire } from 'lwc';

import Stage from '@salesforce/schema/Payment__c.Invoice_Stage__c';
import InvoiceNumber from '@salesforce/schema/Payment__c.Invoice_Number__c';
import Vendor from '@salesforce/schema/Payment__c.Vendor__c';
import Description from '@salesforce/schema/Payment__c.Description__c';
import Category from '@salesforce/schema/Payment__c.Category_lookup__c';
import Payment_Method from '@salesforce/schema/Payment__c.Payment_Method__c';
import Amount from '@salesforce/schema/Payment__c.Payment_Amount__c';
import Accounting_Period from '@salesforce/schema/Payment__c.Accounting_Period__c';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import payment from '@salesforce/schema/Payment__c';
import { hideSpinner, showSpinner } from 'c/util';
import getInvoiceId from '@salesforce/apexContinuation/StaffAndOperationsController.getInvoiceId';
import insertStaffAndOperation from '@salesforce/apexContinuation/StaffAndOperationsController.insertStaffAndOperation';
import uploadFile from '@salesforce/apexContinuation/StaffAndOperationsController.uploadFile';
import getFileName from '@salesforce/apexContinuation/StaffAndOperationsController.getFileName';

import updateFipsCodeinInvoices from "@salesforce/apexContinuation/ROCController.updateFipsCodeinInvoices";
import lookUp from '@salesforce/apex/Lookup.search';
import { getRecordUi, getRecordNotifyChange } from 'lightning/uiRecordApi';
import TickerSymbol from '@salesforce/schema/Account.TickerSymbol';
const columns = [
    {
        label: "Title",
        fieldName: "titleLink",
        type: "url",
        typeAttributes: { label: { fieldName: "title" }, tooltip: "title", target: "_blank" }
    },
    {
        label: "Owner",
        fieldName: "ownerLink",
        type: "url",
        typeAttributes: { label: { fieldName: "owner" }, tooltip: "owner", target: "_blank" }
    },
];
export default class StaffOperationInvoice extends LightningElement {
    @track sfdcBaseURL = window.location.origin;
    columns = columns;
    @api recordTypeOfInvoice;
    @api recordId;
    @api objectApiName;
    @track showLoader = false;
    @api isUpdate;
    @api opportunityid;
    @api parentfiscalyearid;
    @api selectedInvoice;
    @api formtitle;
    @track selectedRecordId;
    @track fiscalyearId;
    @track accountingPeriod;
    @track isView = false;
    @track fund = '';
    recordTypeIdStr
    fields = [InvoiceNumber, Vendor, Amount, Payment_Method, Category, Stage, Description];
    @wire(getRecordUi, { recordIds: '$selectedInvoice', layoutTypes: 'Full', modes: 'View' })
    accountRecordUi({ error, data }) {
        if (data) {
            console.log(data, 'data')
        }
    }
    @wire(getObjectInfo, { objectApiName: payment })
    getObjectdata({ error, data }) {
        if (data) {

            console.log('data.defaultRecordTypeId ' + data.defaultRecordTypeId);
            console.log('data.defaultRecordTypeId ', data.recordTypeInfos);
            for (var key in data.recordTypeInfos) {
                if (data.recordTypeInfos[key].name == "Adjustments") {
                    this.recordTypeIdStr = data.recordTypeInfos[key].recordTypeId;
                }
            }
            // perform your custom logic here
        } else if (error) {
            // perform your logic related to error 
        }
    };

    catWhereStr = "  AND Inactive__c = false";
    disableCat = false;
    selectedCate = '';

    selectedCategory(event) {
        var ind = parseInt(event.target.dataset.id);
        let selectedRecords = JSON.parse(event.detail);
        //this.selectedCate = '';
        if (selectedRecords && selectedRecords.object === "Category__c") {
            if (selectedRecords.records.length > 0) {
                for (var i = 0; i < selectedRecords.records.length; i++) {
                    if (this.selectedCate == '') {
                        this.selectedCate = selectedRecords.records[i].recId;
                    }
                    else {
                        this.selectedCate = this.selectedCate + ',' + selectedRecords.records[i].recId;
                    }
                }
                for (var i = 0; i < this.itemList.length; i++) {
                    if (ind == this.itemList[i].id) {
                        this.itemList[i].disableCat = true;
                    }
                }
                if (this.inEdit) {
                    this.disableCat = true;
                }

            } else {
                for (var i = 0; i < this.itemList.length; i++) {
                    if (ind == this.itemList[i].id) {
                        this.itemList[i].disableCat = false;
                    }
                }
                this.selectedCate = '';

            }
        }
    }
    connectedCallback() {
        this.showAddDelete = true;
        console.log(this.opportunityid + ' this.selectedInvoice???');

        getInvoiceId({ InvoiceId: this.selectedInvoice }).then((result) => {
            console.log(result + ' this.getInvoiceId???');
            console.log(result.Fund__c + ' this.result.fund__c???');
            console.log(result.Category_lookup__c + ' this.result.Category_lookup__c???');

            if (result) {
                this.fund = result.Fund__c;
                this.selectedCate = result.Category_lookup__c;
            } else {
                this.fund = 'Reimbursable';
            }
            console.log(this.selectedCate + ' this.selectedCate ???');

        })
            .catch((error) => {
                this.fund = 'Reimbursable';
            });
    }

    handleSelection(event) {
        this.fiscalyearId = event.detail;
        console.log("the selected record id is" + event.detail);
    }
    showEditHandle(event) {
        this.isView = false;
    }
    allnumeric(inputtxt) {
        var numbers = /^[0-9]+$/;
        if (inputtxt.match(numbers)) {
            return true;
        }
        else {
            return false;
        }
    }


    handleSubmit(event) {
        event.preventDefault(); // stop the form from submitting
        showSpinner(this);
        const fields = event.detail.fields;
        var isVal = true;
        if (this.totalAmount != 0) {
            this.toastManager('info', 'Info', 'Total Amount must equal to Zero.');
            event.preventDefault();
            hideSpinner(this);
            isVal = false;
            return true;
        }
        this.template.querySelectorAll('lightning-input-field').forEach(element => {
            if (element.fieldName == 'Opportunity__c') {
                element.value = this.opportunityid;
            }
            if (element.fieldName == 'fund__c') {
                element.value = this.fund;
            }
            if (element.fieldName == 'Invoice_Number__c') {
                element.value = this.invoiceNumber;
            }
            if (element.fieldName == 'Description__c') {
                if (this.disablePM) {
                    if (this.description == null || this.description == '') {
                        this.toastManager('info', 'Info', 'Description is required.');
                        event.preventDefault();
                        hideSpinner(this);
                        isVal = false;
                        return true;
                    }
                    else {
                        element.value = this.description;
                    }
                }
                else {
                    element.value = this.description;
                }
            }
            if (element.fieldName == 'Category_lookup__c') {
                if (this.selectedCate == null || this.selectedCate == '') {
                    this.toastManager('info', 'Info', 'Category is required.');
                    event.preventDefault();
                    hideSpinner(this);
                    isVal = false;
                    return true;
                }
                else {
                    if (this.selectedCate.includes(',')) {
                        const myArr = this.selectedCate.split(",");
                        element.value = myArr[0];
                        var a = this.selectedCate.indexOf(",");
                        a = a + 1;
                        this.selectedCate = this.selectedCate.substr(a, this.selectedCate.length);
                    }
                    else {
                        element.value = this.selectedCate;
                    }
                }
            }

            if (element.fieldName == 'Vendor__c') {
                if (this.vendor == null || this.vendor == '') {
                    this.toastManager('info', 'Info', 'Vendor is required.');
                    event.preventDefault();
                    hideSpinner(this);
                    isVal = false;
                    return true;
                }
                else {
                    if (this.vendor[0] == '0') {
                        element.value = this.vendor;
                    }
                    else {
                        element.value = this.vendor[0];
                    }

                }
            }
            if (element.fieldName == 'Payment_Amount__c') {
                if (element.value == null || element.value == '') {
                    this.toastManager('info', 'Info', 'Payment Amount is required.');
                    event.preventDefault();
                    hideSpinner(this);
                    isVal = false;
                    return true;
                }
            }
            if (element.fieldName == 'Payment_Method__c') {
                if (this.paymentMethod == null || this.paymentMethod == '') {
                    this.toastManager('info', 'Info', 'Payment method is required.');
                    event.preventDefault();
                    hideSpinner(this);
                    isVal = false;
                    return true;
                }
                else {
                    element.value = this.paymentMethod;
                }
            }
            var JournalEntry = false;
            if (element.fieldName == 'Payment_Method__c') {
                if (element.value == 'Journal Entry' || element.value == 'Credit Card') {

                    var today = new Date();
                    JournalEntry = true;
                    //element.Payment_Date__c = today;
                    element.Invoice_Stage__c = 'Pending';
                    this.stage = 'Pending';
                }
            }

            if (JournalEntry && element.fieldName == 'Invoice_Stage__c') {
                element.value == 'Pending';
                this.stage = 'Pending';
            }
            if (element.fieldName == 'Payment_Date__c') {
                if (this.paymentDate == null || this.paymentDate == '') {
                    this.toastManager('info', 'Info', 'Adjustment Date is required.');
                    event.preventDefault();
                    hideSpinner(this);
                    isVal = false;
                    return true;
                }
                else {
                    element.value = this.paymentDate;
                }
            }
        });
        if (!this.showAddDelete) {
            if (this.totalAmount != 0) {
                this.toastManager('info', 'Info', 'Total Amount must equal to Zero.');
                event.preventDefault();
                hideSpinner(this);
                isVal = false;
                return true;
            }
            fields.Id = this.selectedInvoice;
            fields.Opportunity__c = this.opportunityid;
            fields.fund__c = this.fund;
            fields.Invoice_Number__c = this.invoiceNumber;
            fields.Description__c = this.description;
            if (this.selectedCate == null || this.selectedCate == '') {
                this.toastManager('info', 'Info', 'Category is required.');
                event.preventDefault();
                hideSpinner(this);
                isVal = false;
                return true;
            }
            else {
                fields.Category_lookup__c = this.selectedCate;
            }
            if (this.vendor == null || this.vendor == '') {
                this.toastManager('info', 'Info', 'Vendor is required.');
                event.preventDefault();
                hideSpinner(this);
                isVal = false;
                return true;
            }
            else {
                if (this.vendor[0] == '0') {
                    fields.Vendor__c = this.vendor;
                }
                else {
                    fields.Vendor__c = this.vendor[0];
                }

            }

            if (fields.Description__c == null || fields.Description__c == '') {
                this.toastManager('info', 'Info', 'Description is required.');
                event.preventDefault();
                hideSpinner(this);
                isVal = false;
                return true;
            }
            if (this.paymentDate == null || this.paymentDate == '') {
                this.toastManager('info', 'Info', 'Adjustment Date is required.');
                event.preventDefault();
                hideSpinner(this);
                isVal = false;
                return true;
            }
            else {
                fields.Payment_Date__c = this.paymentDate;

            }
            if (fields.Payment_Amount__c == null || fields.Payment_Amount__c == '') {
                this.toastManager('info', 'Info', 'Payment Amount is required.');
                event.preventDefault();
                hideSpinner(this);
                isVal = false;
                return true;
            }
            if (this.paymentMethod == null || this.paymentMethod == '') {
                this.toastManager('info', 'Info', 'Payment method is required.');
                event.preventDefault();
                hideSpinner(this);
                isVal = false;
                return true;
            }
            else {
                fields.Payment_Method__c = this.paymentMethod;
            }

            var JournalEntry = false;
            if (fields.Payment_Method__c == 'Journal Entry' || fields.Payment_Method__c == 'Credit Card') {
                var today = new Date();
                JournalEntry = true;
                fields.Invoice_Stage__c = 'Pending';
                this.stage = 'Pending';
            }
            if (isVal) {
                this.template.querySelector('lightning-record-edit-form').submit(fields);
            }
        }
        //if(this.showAddDelete)
        {
            if (isVal) {
                this.template.querySelectorAll('lightning-record-edit-form').forEach(element => {
                    element.submit();
                });
            }
        }
    }
    @track paymentDate;
    @track description;
    @track paymentAmount;
    @track invoiceNumber;
    @track vendor;
    @track paymentMethod;
    @track stage = '';
    paymentMethodChange(event) {
        const temp = event.detail.value;
        if (temp == 'Check') {
            this.stage = 'Ready to Pay';

        } else if (temp == 'Journal Entry') {
            this.stage = 'Pending';

        } else if (temp == 'Credit Card') {
            this.stage = 'Pending';
        }
        this.paymentMethod = event.detail.value;

    }
    invoiceNumberChange(event) {
        this.invoiceNumber = event.detail.value;
    }
    vendorChange(event) {
        this.vendor = event.detail.value;
    }
    paymentDateChange(event) {
        this.paymentDate = event.detail.value;
    }
    descriptionChange(event) {
        this.description = event.detail.value;
    }
    handleSuccess(event) {
        this.selectedInvoice = event.detail.id;
        uploadFile({ recordId: this.selectedInvoice, uploadIds: this.uploadIds }).then((result) => {
            console.log(result + ' ???? result?????');
        })
            .catch((error) => {
                console.log(error);
            });
        this.disableCat = false;
        this.selectedCate = '';
        setTimeout(function () {
            this.template.querySelector("c-sat-multi-select-lookup").setData();
        }.bind(this), 1000);


        updateFipsCodeinInvoices({ invoiceid: this.selectedInvoice }).then((result) => {
            console.log(result + ' ???? result?????');
        })
            .catch((error) => {
                console.log(error);
            });
        this.selectedInvoice = null;
        const inputFields = this.template.querySelectorAll(
            'lightning-input-field'
        );
        if (inputFields) {
            inputFields.forEach(field => {
                if (field.value != 'Reimbursable' && field.value != 'Non-Reimbursable' && field.value != 'Journal Entry' && field.value != 'Pending') {
                    field.value = null;
                }

            });
        }
        this.template.querySelectorAll('lightning-record-edit-form').forEach(element => {
            if (element.value != 'Reimbursable' && element.value != 'Non-Reimbursable' && element.value != 'Journal Entry' && element.value != 'Pending') {
                element.value = null;
            }
        });
        for (var i = this.itemList.length; i >= 1; i--) {
            this.itemList = this.itemList.filter(function (element) {
                return parseInt(element.id) !== parseInt(event.target.accessKey);
            });
        }
        for (var i = 0; i < this.itemList.length; i++) {
            this.itemList[i].disableCat = false;
        }
        this.fileName = [];
        hideSpinner(this);
        if (!this.isNext) {
            this.dispatchEvent(new CustomEvent('closeinvoicecancel', {
                detail: this.isNext
            }));
        }
        else {
            this.uploadIds = '';
        }

    }
    handleReset(event) {
        this.isView = true;
    }
    errorHandler(event) {
        console.log('s000000000000000', event.detail.value);
        hideSpinner(this);
        console.log('s');
        this.template.querySelector(".slds-notify_container.slds-is-relative");
    }

    @track isShowToast = false;
    @track toastType = '';
    @track title = '';
    @track message = '';
    @track toastIcon = 'utility:error';
    toastManager(toastType, title, message) {
        if(toastType.toUpperCase() == 'ERROR'){
            toastType = 'info';
        }
        this.toastType = "slds-notify slds-notify_toast ";
        if (toastType == 'error') {
            this.toastType += 'slds-theme_error';
        } else if (toastType == 'success') {
            this.toastType += 'slds-theme_success';
        } else if (toastType == 'info') {
            this.toastType += 'slds-theme_info';
            this.toastIcon = 'utility:info';
        }
        this.title = title;
        this.message = message;
        this.isShowToast = true;
        setTimeout(function () {
            this.isShowToast = false;
        }.bind(this), 10000);
    }
    closeToast(event) {
        this.isShowToast = false;
        return false;
    }
    @track isSelectedRecord = false;
    @track selectedRecordName = false;
    @track selectedCategoryStr = '';
    @track inEdit = false;
    @track showAddDelete = true;
    @track fileData = [];
    @track showFile = true;
    @track disablePM = false;
    @api
    setIsUpdate(isUpdate) {

        this.isUpdate = isUpdate;
        this.paymentMethod = 'Journal Entry';
        this.stage = 'Pending';
        this.disablePM = true;
        this.catWhereStr = " AND Inactive__c = false AND Category_Type__c = 'Administrative'";
        if (isUpdate != null && isUpdate != '') {
            this.totalAmount = 0;
            this.itemList[0].disableCat = true;
            this.showAddDelete = false;
            this.isView = false;
            getInvoiceId({ InvoiceId: this.isUpdate }).then((result) => {
                if (result) {
                    this.parentfiscalyearid = result.PO_Parent_Fiscal_Year__c;
                    this.selectedRecordName = result.Fiscal_Year_Name__c;
                    this.description = result.Description__c;
                    this.invoiceNumber = result.Invoice_Number__c;
                    this.paymentDate = result.Payment_Date__c;
                    this.vendor = result.Vendor__c;
                    this.paymentMethod = result.Payment_Method__c;
                    this.isSelectedRecord = true;
                    this.stage = result.Invoice_Stage__c;
                    this.selectedCate = result.Category_lookup__c;
                    this.selectedCategoryStr = JSON.stringify([{ 'recId': result.Category_lookup__c, 'recName': result.Category_lookup__r.Name }])

                    setTimeout(function () {
                        const staffOp = this.template.querySelectorAll(
                            'c-sat-multi-select-lookup'
                        );
                        if (staffOp) {
                            staffOp.forEach(op => {
                                op.setData();
                                this.disableCat = true;
                                this.inEdit = true;
                                //op.setIsUpdate(null);
                            });
                        }
                    }.bind(this), 1000);
                } else {
                    this.parentfiscalyearid = null;
                    this.isSelectedRecord = false;
                }
            })
                .catch((error) => {
                    this.isSelectedRecord = false;
                    this.parentfiscalyearid = null;
                    //console.log(error);
                });
            getFileName({ InvoiceId: this.isUpdate }).then((result) => {
                if (result.length > 0) {
                    this.showFile = false;
                    this.fileData = result;
                    var a = '';
                    if (result.length > 1) {
                        result.forEach(function (key) {
                            result.Title = key.ContentDocument.Title;
                            result.Owner = key.ContentDocument.Owner.Name;
                            /*if(a == ''){
                                a = key.ContentDocument.Title;
                            }
                            else{
                                a = a + ', '+key.ContentDocument.Title;
                            }*/

                        });
                        //this.fileName = a;
                    }
                    else {
                        result.Title = result[0].ContentDocument.Title;
                        result.Owner = result[0].ContentDocument.Owner.Name;
                        //this.fileName = result[0].ContentDocument.Title;
                    }

                }
            })
                .catch((error) => {
                    this.isSelectedRecord = false;
                    this.parentfiscalyearid = null;
                    //console.log(error);
                });
        } else {
            this.isSelectedRecord = false;
            this.selectedInvoice = '';
            //this.parentfiscalyearid = null;
            this.selectedRecordName = '';
            this.isView = false;
        }

    }

    invoicecancel() {
        this.isNext = undefined;
        this.dispatchEvent(new CustomEvent('closeinvoicecancel', {
            detail: this.isNext
        }));
    }
    @track isNext = true;
    saveOrNextHandle(event) {

        var flag = event.target.dataset.isnext;
        if (flag == "0") {
            this.isNext = false;
        }
        if (flag == "1") {
            this.isNext = true;
            this.selectedInvoice = null;
        }
        if (this.showAddDelete) {
            this.handleSubmit(event);
        }

    }

    get acceptedFormats() {
        return ['.pdf', '.png', '.jpg', '.jpeg', '.doc', '.xls', '.csv', '.xlsx'];
    }
    @track uploadIds = '';
    @track uploadedFiles = [];
    @track fileName = [];
    handleUploadFinished(event) {
        // Get the list of uploaded files
        //this.fileName = '';
        const uploadedFile = event.detail.files;
        this.uploadedFiles.push(event.detail.files);
        for (let i = 0; i < uploadedFile.length; i++) {
            if (this.uploadIds == '') {
                this.uploadIds = uploadedFile[i].contentVersionId;
                //this.fileName = uploadedFile[i].name;
            }
            else {
                this.uploadIds = this.uploadIds + ',' + uploadedFile[i].contentVersionId;
                //this.fileName = this.fileName + ', '+uploadedFile[i].name;
            }
            this.fileName.push(uploadedFile[i].name);
        }
        /*for(var i = 0 ; i< this.uploadedFiles.length ;i++){
            if(this.uploadedFiles.length>1){
                for(let j = 0; j < uploadedFile.length; j++){
                    if(this.fileName == ''){
                        this.fileName = this.uploadedFiles[i][j].name;
                    }
                    else{
                        this.fileName = this.fileName + ', '+this.uploadedFiles[i][j].name;
                    }
                }
                
                
            }
            else{
                this.fileName = this.uploadedFiles[i][0].name;
            }
        }*/

    }
    keyIndex = 0;
    @track itemList = [
        {
            id: 0,
            amount: 0
        }
    ];
    @track totalAmount;
    get getTotalAmountFun() {
        var total = 0;
        this.itemList.forEach(e => {
            total += e.amount;
        });
        this.totalAmount = total;
        return total;
    }
    addRow() {
        ++this.keyIndex;
        var newItem = [{ id: this.keyIndex, amount: 0 }];
        this.itemList = this.itemList.concat(newItem);
    }
    hideDeleteButton = false;
    removeRow(event) {
        if (this.itemList.length >= 2) {
            this.itemList = this.itemList.filter(function (element) {
                return parseInt(element.id) !== parseInt(event.target.accessKey);
            });
        }
    }
    @track showTotalAmount = false;
    paymentAmountChange(event) {
        this.showTotalAmount = true;
        var ind = parseInt(event.target.dataset.id);
        if (event.detail.value && event.detail.value != '') {
            for (var i = 0; i < this.itemList.length; i++) {
                if (ind == this.itemList[i].id) {
                    this.itemList[i].amount = parseFloat(event.detail.value);
                }
            }

        } else {
            for (var i = 0; i < this.itemList.length; i++) {
                if (ind == this.itemList[i].id) {
                    this.itemList[i].amount = 0;
                }
            }
        }

    }
    printROC(event) {
        /*if (this.isAddRecepit) {
            this.isAmountsEqual(function(iThis) {
                markCompleteStage({ ROCID: iThis.reportId }).then((result) => {
                        if (result) {
                            const event = new ShowToastEvent({
                                title: 'Record updated',
                                variant: 'success',
                                message: '',
                            });
                            iThis.stageLabel = 'Completed';
                            //iThis.dispatchEvent(event);
                            iThis.toastManager('success', 'The Report of Collection has been completed.', '');
                            iThis.rocURL = iThis.sfdcBaseURL + '/apex/Report_Collection?id=' + iThis.reportId;
                            console.log(iThis.rocURL + ' iThis.rocURL???');
                            iThis.openPrint();
                            iThis.ROCStage = true;
                            iThis.displayType = 'readonly';
                            iThis.isAddRecepit = false;

                        } else {

                        }
                    })
                    .catch((error) => {
                        //console.log(error);
                    });

            });
        } else {
            this.rocURL = this.sfdcBaseURL + '/apex/Report_Collection?id=' + this.reportId;
            console.log(this.rocURL + ' this.rocURL???');
            this.openPrint();

        }*/

    }
}