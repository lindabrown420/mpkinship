import { LightningElement } from 'lwc';

import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import checkDuplicate from '@salesforce/apex/caseAssignmentController.checkDuplicate'
export default class CaseAssignment extends LightningElement {
    
    handleSubmit(event){
        console.log('handleSubmit');
        event.preventDefault();       // stop the form from submitting
        const fields = event.detail.fields;
        console.log(fields.Employee__c);
        console.log(fields.Case__c);

        checkDuplicate({
            employeeId: fields.Employee__c,
            caseId: fields.Case__c,
            role: fields.Role__c
        }).then(result => {
            console.log(result);
            if(result == 0){
                this.template.querySelector('lightning-record-edit-form').submit(fields);
            }
            else{
                const evt = new ShowToastEvent({
                    title: 'Duplicate Record',
                    message: 'An active record already exist for this employee and case combination for the given role',
                    variant: 'Error'
                });
                this.dispatchEvent(evt);
            }
        })
    }

    handleSuccess(event){
        console.log('handleSuccess');
        window.open('/'+event.detail.id,'_self');
    }
}