import { track } from "lwc";
import { LightningElement, wire, api } from 'lwc';
import getVendorInvoiceByFilter from "@salesforce/apexContinuation/SWRController.getVendorInvoiceByFilter";
import getSWACaseId from "@salesforce/apexContinuation/SWRController.getSWACaseId";
import createReimbursements from "@salesforce/apexContinuation/SWRController.createReimbursements";
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import adjustAmount from '@salesforce/schema/Payment__c.Adjustment_Amount__c';
import Payment_OBJECT from '@salesforce/schema/Payment__c';
import Pay_stage from '@salesforce/schema/Payment__c.Invoice_Stage__c';

// const Reccolumns = [

//     { label: 'Vendor', fieldName: 'Vendor_Name__c' ,fixedWidth: 120},
//     { label: 'Check #', fieldName: 'Check_Number__c',fixedWidth: 88},
//     { label: 'Payment Date', fieldName: 'Payment_Date__c',fixedWidth: 126 },
//     { label: 'Category', fieldName: 'Category_Name__c' ,fixedWidth: 96,},    
//     { label: 'Case', fieldName: 'Case_Name__c' ,fixedWidth: 120},
//     { label: 'Payment Amount', fieldName: 'Payment_Amount__c',fixedWidth: 150},
//     { label: 'Prior Refund', fieldName: 'Prior_Refund1__c' ,fixedWidth: 120},
//     { label: 'Balance Remaining', fieldName: 'Balance_Remaining__c',fixedWidth: 156 },
//     { label: 'Refund Amount', fieldName: 'Refund_Amount__c',fixedWidth: 145,editable: true ,cellAttributes :{class:'slds-cell-edit slds-is-edited'} },
//     { label: 'Refund Category', fieldName: 'categoryName',fixedWidth: 145 },
//     {
//         label: "",
//         type: "button-icon",
//         typeAttributes: {
//             iconName: "utility:edit",
//             //label: 'Note',
//             name: "editVendorRefund",
//             title: "Edit Vendor Refund",
//             disabled: { fieldName: "isCategoryNull" },
//             value: { fieldName: "Id" }
//         },
//         fixedWidth: 50
//     },


// ];
//Vendor__r.Name, Check_Number__c, Payment_Date__c, Category_Name__c, Category_lookup__c, Payment_Amount__c, Refund_Amount__c, Prior_Refund1__c
const vendorInvoiceColumns = [
    { label: 'Vendor Name', fieldName: 'vendorName', editable: false, initialWidth: 140 },
    { label: 'Check #', fieldName: 'Check_Number__c', editable: false, initialWidth: 90  },
    { label: 'Payment Date', fieldName: 'Payment_Date__c', type: 'date', editable: false,initialWidth: 125, typeAttributes: {
        month: "numeric",
        day: "numeric",
        year: "numeric"
    } },
    { label: 'Category', fieldName: 'Category_Name__c', editable: false, wrapText: true,initialWidth: 125},
    { label: 'Payment Amount', fieldName: 'Payment_Amount__c', type: 'currency', editable: false,initialWidth: 125 },
    { label: 'Prior Reimbursement Amount', fieldName: 'Prior_Refund1__c', type: 'currency', editable: false,initialWidth: 130  },
    { label: 'Remaining Reimbursement Amount', fieldName: 'Balance_Remaining__c', type: 'currency', editable: false,initialWidth: 150  },
    { label: 'Reimbursement Amount', fieldName: 'Refund_Amount__c', type: 'currency', editable: true,initialWidth: 160,    cellAttributes: { 
        class: 'slds-theme_warning', 
    },},
    {
        label: 'Transaction Code', fieldName: 'Payment_Source__c', type: 'picklist', initialWidth: 450, wrapText: true,
        cellAttributes: { 
            class: 'slds-theme_warning', 
        }, 
        typeAttributes: {
            placeholder: 'Choose Transaction Code', options: [
                { label: 'Child Support Collections through DCSE', value: '5' },
                { label: 'Payments made on behalf of the child (ex: SSA, SSI, VA benefits, …) – Special Welfare Refunds', value: '6' },
            ] // list of all picklist options
            , value: { fieldName: 'Payment_Source__c' }
            , context: { fieldName: 'Id' } 
        }
    }
];
export default class AddSWRReceipt extends LightningElement {
    sbr = true;
    @track payStage = 'Ready to Pay';
    @track toastIcon = 'utility:error';
    paymentMethodValue = 'Journal Entry';
    checkReferenceNumber;
    swaValue;
    dateReceiptValue;
    recordTypeIdStr;
    @api recordId;
    @wire(getObjectInfo, { objectApiName: Payment_OBJECT })
    getObjectdata({ error, data }) {
        if (data) {
            for (var key in data.recordTypeInfos) {
                if (data.recordTypeInfos[key].name == "Special Welfare Reimbursement") {
                    this.recordTypeIdStr = data.recordTypeInfos[key].recordTypeId;
                }
            }
            // perform your custom logic here
        } else if (error) {
            // perform your logic related to error 
        }
    };
    connectedCallback() {
        var d = new Date();
        var dtString = d.toLocaleDateString();
        var splitDate =  dtString.split('/');
        var year = splitDate[2];
        var month = splitDate[0];
        var day = splitDate[1];
        this.dateReceiptValue =year+'-'+month+'-'+day;
        console.log('Current date????????',this.dateReceiptValue);
        //hrefSWA = '/lightning/r/Special_Welfare_Account__c/'+ this.Special_Welfare_Account__c +'/view';
        //console()
    }

    //Boolean tracked variable to indicate if modal is open or not default value is false as modal is closed when page is loaded 
    @track isModalOpen = true;
    openModal() {
        // to open modal set isModalOpen tarck value as true
        this.isModalOpen = true;
    }
    closeModal() {
        // to close modal set isModalOpen tarck value as false
        this.isModalOpen = false;
    }

    @track isModal2Open = false;
    openModal2() {
        // to open modal set isModalOpen tarck value as true
        this.isModalOpen = false;
        this.isModal2Open = true;
    }
    closeModal2() {
        // to close modal set isModalOpen tarck value as false
        this.isModal2Open = false;
    }
    @track hrefSWA

    @track isShowToast = false;
    // datatable 
    vendorInvoiceColumns = vendorInvoiceColumns;
    vendorInvoicesData = [];

    accountingPeriodValue;
    @track startDate;
    @track endDate;
    @track totalRefundAmount = 0;
    getSelectedInvoice() {
    }
    EndDateHandle(event) {
        this.endDate = event.target.value;
    }
    StartDateHandle(event) {
        this.startDate = event.target.value;
    }
    accountingPeriodChange(event){
        this.accountingPeriodValue = event.target.value;
    }
    toastManager(toastType, title, message) {
        this.toastType = "slds-notify slds-notify_toast ";
        if (toastType == 'error') {
            this.toastType += 'slds-theme_error';
        } else if (toastType == 'success') {
            this.toastType += 'slds-theme_success';
        } else if (toastType == 'info') {
            this.toastType += 'slds-theme_info';
            this.toastIcon = 'utility:info';
        }
        this.title = title;
        this.message = message;
        this.isShowToast = true;
        setTimeout(function() {
            this.isShowToast = false;
        }.bind(this), 10000);
    }
    closeToast(event) {
        this.isShowToast = false;
        return false;
    }
    @track itemList = [
        {
            id: 0,
            amount:0
        }
    ];
    handleEditCellChange(event){
        //this.totalRefundAmount = this.totalRefundAmount + parseFloat(event.currentTarget.draftValues[0].Refund_Amount__c);
        //this.totalRefundAmount = event.currentTarget.draftValues;
        var total = 0;
        for(var i = 0 ; i<event.currentTarget.draftValues.length ; i++ ){
            if(event.currentTarget.draftValues[i].Refund_Amount__c != ''){
                total += parseFloat(event.currentTarget.draftValues[i].Refund_Amount__c);
                //this.totalRefundAmount =  this.totalRefundAmount + parseFloat(event.currentTarget.draftValues[i].Refund_Amount__c);
            }
            
        }
        this.updateDraftValues(event.detail.draftValues[0]);
        this.totalRefundAmount = total;
        //console.log('n bshdfgshdfbjsb',event);
    }
    @track totalAmount;
        /*get getTotalAmountFun(){
            var total = 0;
            this.itemList.forEach(e => {
                total += e.amount;
            });
            this.totalAmount = total;
            return total;
        }*/
    handleTableSave(event) {
        var totalAmount = 0;
       // var updatedRecords = event.detail.draftValues;
       this.lastSavedData = JSON.parse(JSON.stringify(this.vendorInvoicesData));
       var updatedRecords = this.draftValues;
        //const rtis = this.objectInfo.data.recordTypeInfos;
        //var reimbursementRT = Object.keys(rtis).find(rti => rtis[rti].name === 'Special Welfare Reimbursement');
        
        if(updatedRecords.length > 0){
            var isBalanceGreater = false;
            var isChildBalanceGreater = false;
            var isSSA_SSIBalanceGreater = false;
            var isCurrentBalanceGreater = false;
            var refundAmountRequired = false;
            var  isRequired = true;
            var i = 0;
            updatedRecords.forEach(e => {
                e.Invoice_Stage__c = this.payStage;
                console.log('Paystage??????????????????', e.Invoice_Stage__c);
                if(e.Refund_Amount__c == undefined || e.Refund_Amount__c == ''){
                    this.toastManager('error', 'Error','Reimbursement Amount is Required.');
                    isRequired = false;
                    refundAmountRequired = true;
                    //return null;
                    
                    
                }
                if(e.Payment_Source__c == undefined || e.Payment_Source__c == '')
                {
                    this.toastManager('error', 'Error','Transaction Code is Required.');
                    isRequired = false;
                    //return null;
                    refundAmountRequired = true;
                }
                if(e.Refund_Amount__c != undefined || e.Refund_Amount__c != '' || e.Payment_Source__c != undefined || e.Payment_Source__c != '')
                {
                    //refundAmountRequired = true;
                    
                console.log("Inside 3rd IF>>>>>>>>>>>>>>");
                //updatedRecords.splice(i,1);
                }
                
                i= i+1;
                
                // else if(e.Refund_Amount__c == ''){
                //     updatedRecords.splice(i, 1);
                // }

                //i = i+1;
            });
            if(!refundAmountRequired){
                console.log("Main IF????????????????????????????????");
                //console.log("Main IF Reimursement", Refund_Amount__c);
                //Payment_Source__c=Payment_Source__c;
                //Refund_Amount__c=Refund_Amount__c

                updatedRecords.forEach(e => {
                    totalAmount = totalAmount + parseFloat(e.Refund_Amount__c);
                    this.totalRefundAmount = totalAmount;
                    if(this.remainginBalance < totalAmount){
                        isCurrentBalanceGreater = true;
                    }
                    else{
                        this.template.querySelectorAll('lightning-input-field').forEach(recordInput => {    
                            if(recordInput.value && recordInput.value != ''){
                                e[recordInput.fieldName] = recordInput.value;
                            }
                            
                        });
                        if(this.dateReceiptValue != null && this.dateReceiptValue != ''){
                            console.log('this.dateReceiptValue'+this.dateReceiptValue);
                            if(e.Date_of_Receipt__c == undefined){
                                e['Date_of_Receipt__c'] = new Date(this.dateReceiptValue);
                                
                            }
                        }
                        else{
                            this.toastManager('error', 'Error','Date Receipt is required.');
                            isRequired = false;
                        }
                        if(this.swaValue != null && this.swaValue != ''){
                            if(e.Special_Welfare_Account__c == undefined){
                                e['Special_Welfare_Account__c'] = this.swaValue;
                            }
                        }
                        else{
                            this.toastManager('error', 'Error','Payer is required.');
                            isRequired = false;
                        }
                        if(this.paymentMethodValue != null && this.paymentMethodValue != ''){
                            if(e.Payment_Method__c == undefined){
                               e['Payment_Method__c'] = this.paymentMethodValue;
                            }
                        }
                        else{
                            this.toastManager('error', 'Error','Payment Method is required.');
                            isRequired = false;
                        }
                        if(this.checkReferenceNumber != null && this.checkReferenceNumber != ''){
                            if(e.Check_Reference_Number__c == undefined){
                               e['Check_Reference_Number__c'] = this.checkReferenceNumber;
                            }
                        }
                        const currentRecord = this.vendorInvoicesData.find(viRecord => viRecord.Id === e.Id);
                        if(currentRecord.Balance_Remaining__c < e.Refund_Amount__c){
                            isBalanceGreater = true;
                        }
                        
                        if(isBalanceGreater || isCurrentBalanceGreater || isChildBalanceGreater  || isSSA_SSIBalanceGreater || !isRequired){
                            //e.Id = e.Id;
                            //e.Refund_Amount__c = e.Refund_Amount__c; 
                        }
                        else{
                           e.Vendor__c = currentRecord.Vendor__c; 
                            e.Kinship_Case__c =  currentRecord.Kinship_Case__c;e.Check_Number__c = currentRecord.Check_Number__c;
                            if(e.Refund_Amount__c == ''){
                               e.Payment_Amount__c = 0;
                            }
                            else{
                               e.Payment_Amount__c = e.Refund_Amount__c;
                            }
                            e.Payment_Date__c = currentRecord.Payment_Date__c;
                            if(e.Payment_Source__c){
                                e.Payment_Source__c = e.Payment_Source__c;
                            }
                            else{
                                e.Payment_Source__c = currentRecord.Payment_Source__c;
                            }
                            e.Category_lookup__c = currentRecord.Category_lookup__c;
                            e.Refund_Amount__c = e.Refund_Amount__c; 
                            e.Payment__c = e.Id;
                            e.Payer__c = 'Special Welfare Account';
                            e.Invoice_Stage__c = this.payStage;
                            e.Receipt_Type__c = currentRecord.Category_lookup__r.Category_Type__c
                            e.Recipient__c  = 'Case';
                            if( currentRecord.Prior_Refund1__c == undefined){
                                e.Prior_Refund1__c = 0;
                            }
                            else{
                                e.Prior_Refund1__c = currentRecord.Prior_Refund1__c;
                            }
                            e.Id = undefined;
                            e.Refund_Amount__c = 0;
                        }
                        e.Opportunity__c = this.recordId;
                        e.RecordTypeId = this.recordTypeIdStr;
                    }
                });
            }
            
            if(isCurrentBalanceGreater){
                //this.showLoader = false; 
                this.toastManager('error', 'Error','Reimbursement amount should not be greater than current balance remaining.'); 
                return null;

            }
            else if(isBalanceGreater){
                //this.showLoader = false; 
                this.toastManager('error', 'Error','Reimbursement Amount should not be greater than the remaining balance.');
                return null;

            }
            /*else if(isChildBalanceGreater){
                this.toastManager('error', 'Error','Reimbursement amount should not be greater than child balance remaining.');
                return null;

            }
            else if(isSSA_SSIBalanceGreater){
                this.toastManager('error', 'Error','Reimbursement amount should not be greater than SSA/SSI balance remaining.');
                return null;

            }*/
            // else if(refundAmountRequired){
            //     this.toastManager('error', 'Error','Reimbursement amount or Transaction Code is required.');
            //     isRequired = false;
            //     return null;

            // }
            console.log('updatedRecordsupdatedRecords????',updatedRecords)
            if(isRequired){
                createReimbursements({reimbursements: updatedRecords}).then(result => {
                    this.toastManager('success', result);
                    const selectedEvent = new CustomEvent("closereimbursementcancel", {
                        detail: false
                    });
                    this.dispatchEvent(selectedEvent);
                }).catch(error => {
                    this.toastManager('error', 'Something went wrong. We are not able to create Reimbursement.', JSON.stringify(error));
                });
            }
            
        }
        
    }

    // Datatable End
    // Filter
    dateOfReceipt;
    reportNumber;
    paymentMethod;
    balanceRemaining;
    checkReferenceNumber;
    description;
    payer;
    recipient;
    @track iveCat;
    remainginBalance = 0;
    Child_Support_Funds_Remaining = 0;
    SSA_SSI_Funds_Remaining = 0;
    @track viewPayer = false;
    @track closModal = false;
    @track recordIdd
    @track stage
    @track showFilter = false;
    @track hideFilter = false;

    closeCreateNewModal(){
        if(this.iveCat == undefined || this.iveCat == '')
        {
            this.toastManager('error', 'Error !', 'Please select Title IV-E Eligibility.');
        }
        else
        {
            this.closModal = false;
        }
        
    }
    showFilters(){
        this.showFilter=true;
        this.hideFilter = true;
    }

    hideFilters()
    {
        this.hideFilter = false;
        this.showFilter = false;
    }
    changeSWA(event) {
        if (event.detail.value[0]) {
            var value = event.detail.value[0];
            this.swaValue = event.detail.value[0];
            if(this.swaValue == null || this.swaValue == ' ')
            {
                this.viewPayer = flase; 
            }
            else
            {
                this.viewPayer = true;
            }
            this.hrefSWA = '/lightning/r/Special_Welfare_Account__c/'+ this.swaValue +'/view';
            getSWACaseId({ SWAID: value })
                .then(result => {
                    
                    this.recipient = result.Case_KNP__c;
                    this.iveCat = result.Case_KNP__r.Title_IVE_Eligibility__c;
                   
                    console.log('IVECAtegory???????',this.iveCat);
                    console.log('CaseID???????',this.recipient);
                    
                    
                    if(this.iveCat == undefined || this.iveCat == '')
                    {
                        this.closModal = true;   
                        this.toastManager('error', 'Error !', 'Title IV-E Eligibility record is undefined.');
                        
                    }
                    else{
                        this.closModal = false;
                    }
                    
                    this.remainginBalance = result.Current_Balance__c;
                    this.Child_Support_Funds_Remaining = result.Child_Support_Funds_Remaining__c;
                    this.SSA_SSI_Funds_Remaining = result.SSA_SSI_Funds_Remaining__c;
                    
                    //console.log('remainginBalance',this.remainginBalance);
                    
                    getVendorInvoiceByFilter({
                        accountingPeriod: this.accountingPeriodValue, 
                        endDate: this.endDate,
                        startDate: this.startDate,
                        recipient: this.recipient 
                    }).then(result => {
                        this.stage = result.Invoice_Stage__c;
                        console.log('Stage???????',result.Invoice_Stage__c);
                        if(result.length > 0){
                            result.forEach(e => {
                                e.vendorName = e.Vendor__r.Name;
                            });
                            this.draftValues = [];
                        }
                        else{
                            this.draftValues = null;
                            this.toastManager('error', 'No Record!', 'There is no record of paid invoices to reimburse for this case.');
                        }
                        this.vendorInvoicesData = result;
                        this.totalRefundAmount = 0;
                    }).catch(error => {
            
                    });
                })
                .catch(error => {

                });
        }

    }
    searchByFilter(){
        this.filterSubmitHandle();
    }
    filterSubmitHandle(event){
        //event.preventDefault(); 
        //const fields = event.detail.fields;
        getVendorInvoiceByFilter({
            //accountingPeriod: fields.Accounting_Period__c,
            accountingPeriod:this.accountingPeriodValue,
            endDate: this.endDate,
            startDate: this.startDate,
            recipient: this.recipient
            //recipient: fields.Kinship_Case__c
        }).then(result => {
            if(result.length > 0){
                result.forEach(e => {
                    e.vendorName = e.Vendor__r.Name;
                });
                this.draftValues = [];
            }
            else{
                this.draftValues = null;
            }
            this.vendorInvoicesData = result;
            this.totalRefundAmount = 0;
            this.lastSavedData = JSON.parse(JSON.stringify(this.vendorInvoicesData));
        }).catch(error => {

        });
    }
    value = '';

    get options() {
        return [
            { label: 'Receipt', value: 'Receipt' },
            { label: 'Special Welfare Reimbursement', value: 'Special Welfare Reimbursement' },
        ];
    }
    typeOptionChange(event) {
        this.value = event.target.value;
        //console.log(this.value);
    }

    @track formtitle = 'Add Receipt / Refund';
    NextOptions() {
        // to close modal set isModalOpen tarck value as false
        if (this.value == "Receipt") {
            //this.formtitle = 'Add Receipt';    
            this.openModal2();

        }
        if (this.value == "Special Welfare Reimbursement") {
            this.openModal2();
            // this.formtitle = 'Add Vendor Refund';
        }

    }


    draftValues = [];
    emptydraft = false;
    lastSavedData = [];
    updateDataValues(updateItem) {
        let copyData = [... this.vendorInvoicesData];
        copyData.forEach(item => {
            if (item.Id === updateItem.Id) {
                for (let field in updateItem) {
                    item[field] = updateItem[field];
                }
            }
        });

        //write changes back to original data
        this.vendorInvoicesData = [...copyData];
    }

    updateDraftValues(updateItem) {
        let draftValueChanged = false;
        let copyDraftValues = [...this.draftValues];
        //store changed value to do operations
        //on save. This will enable inline editing &
        //show standard cancel & save button
        copyDraftValues.forEach(item => {
            if (item.Id === updateItem.Id) {
                for (let field in updateItem) {
                    item[field] = updateItem[field];
                }
                draftValueChanged = true;
            }
        });

        if (draftValueChanged) {
            this.draftValues = [...copyDraftValues];
        } else {
            this.draftValues = [...copyDraftValues, updateItem];
        }
    }

    //listener handler to get the context and data
    //updates datatable
    picklistChanged(event) {
        event.stopPropagation();
        let dataRecieved = event.detail.data;
        let updatedItem = { Id: dataRecieved.context, Payment_Source__c: dataRecieved.value };
        this.updateDraftValues(updatedItem);
        this.updateDataValues(updatedItem);
    }

    handleCancel(event) {
        //remove draftValues & revert data changes
        this.vendorInvoicesData = JSON.parse(JSON.stringify(this.lastSavedData));
        this.draftValues = [];
        this.totalRefundAmount = 0;
    }
    @track iveValue
    changeHandler(event)
    {
        this.value = event.detail.value;
        this.iveVal =  this.value;
        console.log("IVE value??????????", this.value);
        if(this.value == '1' || this.value == '2' )
        {
            this.iveVal =  this.value;
        }
        else
        {
            this.toastManager('error', 'Select Value!', 'Title IVE Category is empty.');

        }
    }

    handleSuccess()
    {  
        if(this.iveVal == '1' || this.iveVal == '2')
        {
            this.toastManager('success', 'Record Updated', 'Title IVE Category is updated.');
            this.closModal = false;
        }
        else
        {
            this.toastManager('error', 'Select Value!', 'Title IVE Category is empty.');
        }
        
    }
    
    

    // paymentMethodChange(event){
    //     this.value = event.detail.value;
    //     console.log('PaymentMethod???????',this.value);
    //     if(this.value == 'Check')
    //     {
    //         this.payStage = 'Ready to Pay';
    //     }
    //     else
    //     {
    //         this.payStage = 'Pending';
    //     }
    //     console.log('payStagevalue???????',this.payStage);
    // }
    checkReferenceNumberChange(event){
        this.checkReferenceNumber = event.detail.value;
    }
    dateReceiptValueChange(event){
        this.dateReceiptValue  = event.detail.value;
        
    }
    
}