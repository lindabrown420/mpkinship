import { api, track, LightningElement } from 'lwc';

export default class HomeDesignUtil extends LightningElement {
    @api sectionData;
    @track backgroundColor;
    @track fontColor;
    @track backgroundColorChange = false;
    @track fontColorChange = false;
    connectedCallback(){
        if(this.sectionData.backgroundColor != null && this.sectionData.backgroundColor != ''){
            this.backgroundColor = 'background-color:'+this.sectionData.backgroundColor+';';
            this.backgroundColorChange = true;
        }
        if(this.sectionData.fontColor != null && this.sectionData.fontColor != ''){
            this.fontColor = 'color:'+this.sectionData.fontColor+';';
            this.fontColorChange = true;
        }
        
    }
}