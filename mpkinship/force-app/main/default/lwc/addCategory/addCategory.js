import { LightningElement, api, track, wire } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import getCOARecord from "@salesforce/apexContinuation/AddCategoryController.getCOA";

export default class AddCategory extends NavigationMixin(LightningElement) {
    @api recordtypeid;
    @track isshowcoa = false;
    @track showLoader = false;

    @track isShowToast = false;
    @track toastType = '';
    @track title = '';
    @track message = '';
    @track localityAccount = 'a1v3R0000003FApQAM';
    toastManager(toastType, title, message) {
        if(toastType.toUpperCase() == 'ERROR'){
            toastType = 'info';
        }
        this.toastType = "slds-notify slds-notify_toast ";
        if (toastType == 'error') {
            this.toastType += 'slds-theme_error';
            this.toastIcon = 'utility:error';
        } else if (toastType == 'success') {
            this.toastType += 'slds-theme_success';
            this.toastIcon = 'utility:success';
        } else if (toastType == 'info') {
            this.toastType += 'slds-theme_info';
            this.toastIcon = 'utility:info';
        }
        this.title = title;
        this.message = message;
        this.isShowToast = true;
        setTimeout(function() {
            this.isShowToast = false;
        }.bind(this), 10000);
    }
    closeToast(event) {
        this.isShowToast = false;
        return false;
    }

    get islaser(){
        if(this.recordtypeid === '0123d0000004M3VAAU') return true;
        return false;
    }

    get isledrs(){
        if(this.recordtypeid === '0123d0000004M3WAAU') return true;
        return false;
    }

    get islocal(){
        if(this.recordtypeid === '0123d0000004M3UAAU') return true;
        return false;
    }

    connectedCallback() {
    }

    handlCateSubmit(event) {
    }

    handleCateCreateSuccess(event) {
        console.log('handleCateCreateSuccess: ' + JSON.stringify(event.detail));
        this.cateId = event.detail.id;
        this.localityAccount = event.detail.fields.Locality_Account__c.value;
        console.log('handleCateCreateSuccess: ' + this.localityAccount);
        getCOARecord({ strId: this.localityAccount }).then((result) => {
            console.log('handleCateCreateSuccess:' + JSON.stringify(result));
            if (result && result.length > 0) {
                event.preventDefault();
                this.navigateToRecordPage(this.cateId);
            } else {
                this.isshowcoa = true;
            }
        })
        .catch((error) => {
            console.log('handleCateCreateSuccess:' + JSON.stringify(error));
        });
    }

    createCateNewCancel(event) {
        event.preventDefault();
        // go back to Category List
        this.navigateToObjectHome();
    }

    handleCateError(event){
        this.showLoader = false;
        this.toastManager('info', event.detail.message, '');
        console.log('handleerror' + JSON.stringify(event.detail));
    }

    handlCOASubmit(event) {
    }

    handleCOACreateSuccess(event) {
        event.preventDefault();
        // go back to Category List
        this.navigateToRecordPage(this.cateId);
    }

    createCOANewCancel(event) {
        event.preventDefault();
        // go back to Category List
        this.navigateToRecordPage(this.cateId);
    }

    handleCOAError(event){
        console.log('handleerror' + JSON.stringify(event.detail));
        this.showLoader = false;
        this.toastManager('info', event.detail.message, '');
    }

    handleReset(event) {
        event.preventDefault();
        // go back to Category List
        this.navigateToObjectHome();
    }

    navigateToObjectHome() {
        // Navigate to the Account home page
        this[NavigationMixin.Navigate]({
            type: 'standard__objectPage',
            attributes: {
                objectApiName: 'Category__c',
                actionName: 'home'
            }
        });
    }

    navigateToRecordPage(recordId) {
        console.log('navigateToRecordPage:' + recordId);
        // Navigate to the Account home page
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: recordId,
                objectApiName: 'Category__c',
                actionName: 'view'
            }
        });
    }
}