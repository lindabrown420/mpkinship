import { LightningElement, track, wire, api, } from 'lwc';
import Report_Number from '@salesforce/schema/Payment__c.Report_Number__c';
import Recipient from '@salesforce/schema/Payment__c.Recipient__c';
import Kinship_Case__c from '@salesforce/schema/Payment__c.Kinship_Case__c';
import Vendor__c from '@salesforce/schema/Payment__c.Vendor__c';
import Receipt_Type from '@salesforce/schema/Payment__c.Receipt_Type__c';
import Amount from '@salesforce/schema/Payment__c.Payment_Amount__c';
//import Pay_Method from '@salesforce/schema/Payment__c.Pay_Method__c';
import Pay_Method from '@salesforce/schema/Payment__c.Payment_Method__c';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import payment_obj from '@salesforce/schema/Payment__c';
import Category from '@salesforce/schema/Payment__c.Category_lookup__c';
import Refund_Receipt from '@salesforce/schema/Payment__c.Refunded_Receipt__c';
import Change_Category from '@salesforce/schema/Payment__c.Change_Category__c';
import Returned_Check from '@salesforce/schema/Payment__c.Returned_Check__c';
import PaySource from '@salesforce/schema/Payment__c.Payment_Source__c';
import Claim__c from '@salesforce/schema/Payment__c.Claims__c';
import getCategoryType from "@salesforce/apexContinuation/ROCController.getCategoryType";
import Claim__Name from '@salesforce/schema/Payment__c.Claims__r.Name';
import Special_Welfare_Account__c from '@salesforce/schema/Payment__c.Special_Welfare_Account__c';
import Special_Welfare_Account__Name from '@salesforce/schema/Payment__c.Special_Welfare_Account__r.Name';


import { NavigationMixin } from 'lightning/navigation';
import { hideSpinner, showSpinner } from 'c/util';
import getReceiptInfo from "@salesforce/apexContinuation/ROCController.getReceiptInfo";

export default class AddROCAdjustment extends NavigationMixin (LightningElement) {


    @api closedate;
    @api parentId;
    @api enddate;
    @api beginningreceipt;
    @api endingreceipt;
    @api recordId;
    @api selectedReceipt;
    @api reportCollectionId;
    @api fipsCode;
    @api childFy;
    titleoftoast;
    @track RNumberId;
    newRNumberNameObject='';
    @track receiptNumber;
    @track payValue;
    @track isCategory = false;
    @track caseValue;
    @track claimValue = '';
    @track claimValueId = '';
    @track swaValue;
    @track swaValueId;
    @track caseValueId;
    @track vendorValue;
    @track vendorValueId;
    @track contactValueId;
    @track contactValue;
    @track recipientValue='';
    @track payMethod='';
    @track catValue;
    @track catValueId;
    @track RecpId;
    @track disableRNumber = false;
    @track showInfo= false;
    @track fields;
    catWhereStr ='';
    catWhereStrn ='';
    isSelected = false;

    @track showOtherTranscationText = false;
    @wire(getObjectInfo, { objectApiName: payment_obj })
    getObjectdata({ error, data }) {
        if (data) {
            console.log('data.defaultRecordTypeId ' + data.defaultRecordTypeId);
            console.log('data.defaultRecordTypeId222 ', data.recordTypeInfos);
            for (var key in data.recordTypeInfos) {
                if (data.recordTypeInfos[key].name == "Adjustments") {
                    this.recordTypeIdStr = data.recordTypeInfos[key].recordTypeId;
                }
            }
            console.log('data.defaultRecordTypeId5555 ', data.recordTypeInfos[key].name);
            // perform your custom logic here
        } else if (error) {
            // perform your logic related to error 
        }
    };
    recordTypeIdStr;
    fields = [Report_Number, Amount, Category,Refund_Receipt,Returned_Check,Change_Category,PaySource,Kinship_Case__c,Vendor__c,Claim__c,Recipient,Special_Welfare_Account__c
    ];
    
    dateChange(event)
    {
        this.closedate = event.target.value;
    }
    handleLoadForm(event){
        const fields = event.detail.fields;
    }
    @track isShowToast = false;
    @track toastType = '';
    @track title = '';
    @track message = '';
    @track toastIcon = 'utility:error';
    titleoftoast;
    toastManager(toastType, title, message) {
        this.toastType = "slds-notify slds-notify_toast ";
        if (toastType == 'error') {
            this.toastType += 'slds-theme_error';
        } else if (toastType == 'success') {
            this.toastType += 'slds-theme_success';
        } else if (toastType == 'info') {
            this.toastType += 'slds-theme_info';
            this.toastIcon = 'utility:info';
        }
        this.titleoftoast = title;
        this.message = message;
        this.isShowToast = true;
        setTimeout(function() {
            this.isShowToast = false;
        }.bind(this), 10000);
    }
    errorHandler(e) {
        var errorMessage = e.detail.detail;
        console.log('errorMessage>>>>>>>' , errorMessage);
        this.template.querySelector(".slds-notify_container.slds-is-relative");
        hideSpinner(this);
    }
    paymentSourceValue='';
    handlePaymentSourceChange(event){
        this.paymentSourceValue = event.detail.value;
        console.log('this.paymentSourceValue',this.paymentSourceValue);
        if(this.paymentSourceValue == '11'){
            this.showOtherTranscationText = true;
        }
        else{
            this.showOtherTranscationText = false;
        }
    }

    selectedCategory(event) {
        this.catWhereStrn = "  AND Inactive__c = false AND Category_Type__c != 'IVE'";
        let selectedCategory = JSON.parse(event.detail);
      
        if (selectedCategory && selectedCategory.object === "Category__c") {
            if(selectedCategory.records.length > 0){
                this.SWAcategoryId= selectedCategory.records[0].recId;
                getCategoryType({ categoryId: this.SWAcategoryId}).then((result) => {
                    if (result) {
                        var catRecName = '';
                       this.receiptTypeValue = result.Category_Type__c;
                       this.categoryType = result.Category_Type__c;
                       catRecName = result.RecordType.Name;
                       if(catRecName.includes('LASER')){
                        this.isCatLaser = true;
                       
                        //this.isCatLaser = true;
                       }
                       else{
                           this.isCatLaser = false;
                       }
                       this.SWAcategoryId = result.Id;
                       //if(this.rcordname != 'Vendor Refund'){
                           if(result.Category_Type__c =='CSA' || result.Category_Type__c =='Special Welfare' || result.Category_Type__c == 'IVE'){	
                                this.showRefundSource = true;	
                                if(result.Category_Type__c =='CSA'){	
                                 this.optionsTranscationCode = [];	
                                 this.optionsTranscationCode =[{ label: 'Parental Co-Payment', value: '4' },{ label: 'Other-Credits', value: '11' }];	
                                }	
                                else if(result.Category_Type__c =='Special Welfare'){	
                                    this.optionsTranscationCode = [];	
                                    this.optionsTranscationCode = [{label: 'Child Support Collections through DCSE', value: '5' },{ label: 'Payments made on behalf of the child (ex: SSA, SSI, VA benefits, …) – Special Welfare Refunds', value: '6'  }];	
                                }	
                                else{	
                                    this.fundValue = 'Reimbursable';
                                    this.optionsTranscationCode = [];	
                                    this.optionsTranscationCode =[{ label: 'Parental Co-Payment', value: '4' },{ label: 'Other-Credits', value: '11' }];	
                                    
                                }
                    
                                this.showRefundSourc = true;
                           }
                           
                           else{
                               this.showRefundSource = false;
                               this.showRefundSourc = false;
                               this.showOtherTranscationText = false;
                           }
                           if(result.Category_Type__c =='Special Welfare'){
                               this.value = 'SpecialWelfareAccount';
                               this.showSpecialWelfareAccount = true;
                               this.showCase = false;
                               
                               
                               this.showClaim = false;
                               this.showClaimBalance = false;
                               //this.disabledCase = true;
                               this.Kinship_Case__c = null;
                               this.showRefundSourc = false;
                               this.SWAcategoryName='';
                           }
                           else{
                               this.disabledCase = false;
                           }
                           if(result.Category_Type__c =='Claims Payable'){
                            this.catWhereStrn = "  AND Inactive__c = false AND Category_Type__c = 'Claims Payable'";
                            this.value ='Claim'
                            this.choseCase=false;
                            
                            this.showCase = false;
                            
                            this.showClaim = true;
                            this.showSpecialWelfareAccount = false;
                            this.showSWRBalance = false;
                            if(!this.showRefundSourc){
                                this.showRefundSource = false;
                                this.showOtherTranscationText = false;
                            }
                        }
                       //}
                       
                   } else {
                      this.receiptTypeValue = '';
                      
       
                   }
               })
               .catch((error) => {
                   //console.log(error);
               });
                
                this.disableFiscalYear = true;
            }else{
                this.disableFiscalYear = false;
                
                this.showRefundSource = false;
                this.SWAcategoryId= '';
                this.value ='Case'
                        this.receiptTypeValue = '';
                        
                        this.showCase = true;
                        
                        this.showClaim = false;
                        this.showClaimBalance = false;
                        this.showSpecialWelfareAccount = false;
                        this.showSWRBalance = false;
                        if(!this.showRefundSourc){
                            this.showRefundSource = false;
                            this.showOtherTranscationText = false;
                        }
            }
        }
        
    }



    @track showTrans = true;
    @track paymentValue;
    @track fips;
    @track accPeriod;
    @track period;
    @track transVal;
    @track adjAmount = 0;
    selectedRecords(event)
    {
        let selectedRecords = JSON.parse(event.detail);
        if (selectedRecords && selectedRecords.object === "Payment__c") {
            if(selectedRecords.records.length > 0){
                console.log('selectedRecords.records.length',selectedRecords.records.length);
                this.RNumberId = selectedRecords.records[0].recId;
               
                getReceiptInfo({recpId: this.RNumberId}).then((result) => {
                    if(result)
                    {
                        console.log('result',result);
                        this.receiptNumber = result.Report_Number__c;
                        if(result.Invoices__r )
                        {
                            result.Invoices__r.forEach(payadj => {
                                if(payadj.Adjustment_Amount__c)
                                {
                                    this.adjAmount +=payadj.Adjustment_Amount__c;

                                }
                                
                            });

                            console.log('this.adjAmount>>>>>>>>',this.adjAmount)

                        }
                        if(result.Recipient__c)
                        {
                            this.recipient = result.Recipient__c;
                        }
                        else
                        {
                            this.recipient = '';
                        }
                        console.log('Transaction Value???',result.Payment_Source__c);
                        if(result.Payment_Source__c)
                        {
                                this.transVal = result.Payment_Source__c;   
                        }
                        else
                        {
                            this.transVal = '';
                        }
                        if(result.Kinship_Case__c)
                        {
                            this.caseValueId = result.Kinship_Case__c;
                            this.caseValue = result.Kinship_Case__r.Name;
                        }
                        else
                        {
                            this.caseValue = '';
                        }
                        console.log('result.Claims__r.Name>>>>>>', result.Claims__c);
                        if(result.Claims__c)
                        {
                            this.claimValue = result.Claims__r.Name;
                            this.claimValueId = result.Claims__c;
                        }
                        else
                        {
                            this.claimValue = ' ';

                        }
                        console.log('this.claimValueId>>>>>>', this.claimValueId);
                        if(result.Special_Welfare_Account__c) 
                        {
                            this.swaValueId = result.Special_Welfare_Account__c;
                            this.swaValue = result.Special_Welfare_Account__r.Name;
                        }
                        else
                        {
                            this.swaValue = '';
                        }
                        /*if(result.Pay_Method__c)
                        {
                            this.paymentValue = result.Pay_Method__c;
                        } */
                        if(result.Payment_Method__c)
                        {
                            this.paymentValue = result.Payment_Method__c;
                        }
                        else
                        {
                            this.paymentValue = result.Payment_Method__c;
                        }
                        if(result.FIPS_Code__c)
                        {
                            this.fips = result.FIPS_Code__c;
                        }
                        else{
                            this.fips= '';
                        }

                        if(result.Accounting_Period__c)
                        {
                            this.accPeriod = result.Accounting_Period__c;
                        }
                        else
                        {
                            this.accPeriod = '';
                        }
                        
                        if(result.Vendor__c)
                        {
                            this.vendorValue = result.Vendor__r.Name;
                            this.vendorValueId = result.Vendor__c;   
                        }
                        else
                        {
                            this.vendorValue = '';
                        }
                        if(result.Payer_Contact__c)
                        {
                            this.contactValue = result.Payer_Contact__r.Name;
                            this.contactValueId = result.Payer_Contact__c;   
                        }
                        else
                        {
                            this.contactValue = '';
                        }
                        this.vendorValueId = result.Vendor__c;
                        this.catValue = result.Category_lookup__r.Name;
                        this.catValueId = result.Category_lookup__c;
                        this.payValue = result.Payment_Amount__c;
                        console.log('result.Payment_Amount__c?????', result.Payment_Amount__c);
                        console.log('this.payValue>>>>>', this.payValue);
                        
                        if(result.Category_lookup__r.Category_Type__c =='CSA' || result.Category_lookup__r.Category_Type__c == 'IVE'){	
                            this.disableRNumber = true;
                            this.showInfo = true;
                            this.showTrans = true;
                            this.optionsTranscationCode = [];	
                            this.optionsTranscationCode =[{ label: 'Other-Debits', value: '11' },{ label: 'Vendor Refund (Reversal)', value: '13' },{ label: 'Parental Co-Payment (Reversal)', value: '12' },
                            { label: 'Child Support Collections through DCSE (Reversal)', value: '15' }, { label: 'Payments made on behalf of the child (ex: SSA, SSI, VA benefits, …) – Special Welfare Refunds (Reversal)', value: '16' },
                            ];	
                        }  
                        else{
                            this.showInfo = true;
                            this.disableRNumber = true;
                            this.showTrans = false;
                            //this.toastManager('error', 'Error', 'Categories other than CSA & IVE are not Adjustable');
                        }
                        console.log('catValueId>>>>>>>>>>>',this.catValueId);
                        if(this.catValueId != '' && this.catValue != ''){
                            let newsObject = [{ 'recId' : this.catValueId ,'recName' : this.catValue }];
                            this.SWAcategoryId = this.catValueId;
                            this.parentFisaclYearName = JSON.stringify(newsObject);
                        } 
                        
                }  
            })
            .catch((error) => {
                console.log(error);
            });
          
        }
        else{
            this.showInfo = false;
            this.disableRNumber=false;
            this.showOtherTranscationText=false;
        }
        }
    }
    @track payAmount = 0;
    handleChangePaymentAmount(event)
    {
        this.payAmount=event.detail.value;
        console.log('this.Amount',this.Amount);
        console.log('this.payAmount',this.payAmount);
        
    }

    @track changeCat;
    handleCategory(event)
    {   
        this.changeCat = event.detail.value;
        console.log(' this.changeCat>>>>>>>',this.changeCat);

    }

    @track catCheck;
    handleCategoryCheck(event)
    {
        this.catCheck = event.detail.checked;
        
        if(this.catCheck == true)
        {
            this.isCategory = true;
        }
        else{
            this.isCategory = false;
        }

    }
    
    handleSubmit(event) {
        showSpinner(this);
        event.preventDefault(); 
        const fields = event.detail.fields;
        fields.Opportunity__c = this.reportCollectionId;
        fields.Payment__c  = this.RNumberId;
        fields.Category_lookup__c = this.SWAcategoryId;
        fields.Kinship_Case__c = this.caseValueId;
        fields.Vendor__c = this.vendorValueId;
        fields.Payer_Contact__c = this.contactValueId;
        fields.Claims__c = this.claimValueId;
        fields.Special_Welfare_Account__c = this.swaValueId; 
        fields.Invoice_Stage__c = 'Pending';
        fields.Payment_Method__c = this.paymentValue;
        fields.FIPS_Code__c = this.fips;
        fields.Accounting_Period__c = this.accPeriod;
        fields.Recipient__c = this.recipient;

        if(this.adjAmount == 0)
        {
            this.adjAmount = this.payAmount;
        }
        else
        {
            this.adjAmount =  +this.adjAmount + +this.payAmount;
        }

        console.log('this.adjAmount>>>>>>>>',this.adjAmount);
        fields.Adjustment_Amount__c = this.payAmount;
        
        
        
        if(fields.Refunded_Receipt__c == true){
            fields.Payment_Method__c = 'Check';
        }

        console.log('fields.Category_lookup__c>>>>>>>>>>',fields.Category_lookup__c);

        console.log('Fields', fields);
        console.log('this.recordId', this.recordId);
        console.log('this.return check???', this.Returned_Check);
        console.log('this.refund receipt???', this.Refund_Receipt);

        //this.fields = event.detail.fields;
        if(this.recordId == null || this.recordId == ''){
            fields.RecordTypeId = this.recordTypeIdStr;
        }
        console.log('fields.RecordTypeId', fields.RecordTypeId);
        console.log('this.disableRNumber', this.disableRNumber);
        console.log('this.Amount', this.Amount);
        console.log('fields.Amount', fields.Amount);
        console.log('this.paymentSourceValue', this.paymentSourceValue);

        if(this.payValue < this.adjAmount)
        {
            this.adjAmount =  +this.adjAmount - +this.payAmount;
            this.toastManager('error', 'Error', 'Adjustment Amount exceeded than Receipt Amount. So, It cannot be adjusted');
                event.preventDefault();
                hideSpinner(this);
                return true;
        }

        if(this.showTrans){
            if (this.paymentSourceValue == null || this.paymentSourceValue == '') {
                this.toastManager('error', 'Error', 'Transaction Code is required.');
                event.preventDefault();
                hideSpinner(this);
                return true;
            }
            else{
                fields.Payment_Source__c=this.paymentSourceValue;
                console.log('PaySource Value?????', fields.PaySource);
            }
        }
        if(this.showOtherTranscationText){
            if (fields.Other_Transaction_Description__c == null || fields.Other_Transaction_Description__c == '') {
                this.toastManager('error', 'Error', 'Other Transaction Description is required.');
                event.preventDefault();
                hideSpinner(this);
                return true;
            
            }
        }


        if(this.payAmount == null || this.payAmount == '')
        {
            this.toastManager('error', 'Error', 'Payment Amount is required.');
            event.preventDefault();
            hideSpinner(this);
            return true;
        }

        if(this.payAmount)
        {
            if(this.payAmount < this.payValue)
            {
                if(this.payAmount > 0)
                {
                    this.payAmount = this.payAmount * -1;
                }
                console.log('Negative Amount>>>>', this.payAmount);
            }  
            else
            {
                this.toastManager('error', 'Error', 'Adjustment Amount should be less than Payment Amount.');
                event.preventDefault();
                hideSpinner(this);
                return true;    
            } 
        }
        fields.Payment_Amount__c = this.payAmount;

        console.log('fields.Payment_Amount__c>>>>>>>>>>',fields.Payment_Amount__c);

        if(fields.Refunded_Receipt__c == true && fields.Returned_Check__c == true)
        {
            this.toastManager('error', 'Error', 'Refund Receipt and Returned Check cannot be checked together.');
            event.preventDefault();
            hideSpinner(this);
            return true;
        }
        else if(fields.Refunded_Receipt__c == false && fields.Returned_Check__c == false && fields.Change_Category__c == false)
        {
            this.toastManager('error', 'Error', 'Please Select atleast one checkbox.');
            event.preventDefault();
            hideSpinner(this);
            return true;
        }
        this.template.querySelector('lightning-record-edit-form').submit(fields);

        //showSpinner(this);
    }
    handleSuccess(event) {
        this.selectedReceipt = event.detail.id;
        hideSpinner(this);
        this.toastManager('success', 'Success', 'Record added succesfully');
        if(!this.isNext ){
            const selectedEvent = new CustomEvent("closereceiptcancel", {
                detail:this.isNext 

            });
            this.dispatchEvent(selectedEvent);
            
        }
        //this.dispatchEvent(selectedEvent);
        const inputFields = this.template.querySelectorAll(
            'lightning-input-field'
        );
        if (inputFields) {
            inputFields.forEach(field => {
                if(field.fieldName == 'Report_Number__c'){      
                    this.receiptNumber = parseInt(this.receiptNumber)+1;
                    this.receiptNumber = this.receiptNumber.toString(); 
                    console.log('ELSE Condition receipt number>>>>>', this.receiptNumber);
                    
                }
                else if(field.value != this.closedate){
                    field.value  = null;
                }
                
                 
            });
        }
    }
    @track isNext = true;
    saveOrNextHandle(event){        
        var flag = event.target.dataset.isnext;        
        if(flag == "0"){
            this.isNext = false;
            
        }
        if(flag == "1"){
            this.isNext = true;
            this.selectedReceipt = null;
        }

        
    }
    invoicecancel() {
        this.isNext = false;
        const selectedEvent = new CustomEvent("closereceipt", {
            detail: this.isNext
        });
        this.dispatchEvent(selectedEvent);
    }
    connectedCallback() {

        this.parentId=this.recordId;
        this.catWhereStr= " AND Opportunity__c =: " + this.parentId + "AND RecordType.Name != 'Adjustments'";
       
        //this.catWhereStr = " AND Opportunity__c =: " + this.parentId;
        // this.WhereStr = " AND Opportunity__c = parentId";
    }
}