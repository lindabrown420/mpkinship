import { LightningElement, api } from 'lwc';

export default class spinner extends LightningElement {
    loaded = false;
    @api
    showSpinner() {
        this.loaded = true;
    }
    @api
    hideSpinner() {
        this.loaded = false;
    }
}