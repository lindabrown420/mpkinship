import { LightningElement,api,wire,track } from 'lwc';

import getGoalRecord from '@salesforce/apex/ServicePlanGoals.getGoalRecord';
import saveRecord from '@salesforce/apex/ServicePlanGoals.saveRecord';
import deleteRecords from '@salesforce/apex/ServicePlanGoals.deleteRecords';
import getGoalTemplates from '@salesforce/apex/ServicePlanGoals.getGoalTemplates';
import getObjectiveTemplates from '@salesforce/apex/ServicePlanGoals.getObjectiveTemplates';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
export default class ServicePlanGoalsContinue extends LightningElement {
    @api goalId;
    @api servicePlanId;
    goalName;
    selectedStatus;
    @api edit;
    @api accordianArrow;
    @track actionItemList = [];
    actionItemLength = 0;
    completedActionItem = 0;
    @api newGoal = false;
    @api showGoalTemplates = false;
    showActionTemplates = false;
    selectedGoalTemplate;
    gtList = [];
    otList = [];
    showCross = false;
    @api caseId;
    @api isDisabled = false;
    //showStatusError = false;
    //showStatusMessage = '';
    
    goalTemplates(){
        console.log('goalTemplates');
        //let gtList = [];
        getGoalTemplates({
            recId: this.servicePlanId
        }).then(result => { 
            console.log(result);
            for(let i=0;i<result.length;i++){
                const option = {
                    label: result[i].Name,
                    value: result[i].Id
                };
                this.gtList = [ ...this.gtList, option ];              
            }
            this.gtList = [ ...this.gtList, {label: '+ Add Custom Goal', value: null} ];
            console.log(this.gtList);
            return this.gtList;
        })
    }

    objectiveTemplates(){
        console.log('objectiveTemplates');
        this.otList = [];
        getObjectiveTemplates({
            recId: this.goalId,
            gtId: this.selectedGoalTemplate
        }).then(result => { 
            console.log(result);
            if(result != null){
                for(let i=0;i<result.length;i++){
                    const option = {
                        label: result[i].Name,
                        value: result[i].Id
                    };
                    this.otList = [ ...this.otList, option ];              
                }
                
            }
            this.otList = [ ...this.otList, {label: '+ Add Custom Objective', value: null} ];
            console.log(this.otList);
            //return this.gtList;
        })
    }

    get status() {
        return [
            { label: 'Not Started', value: 'Not Started' },
            { label: 'In Progress', value: 'In Progress' },
            { label: 'Completed', value: 'Completed' },
            { label: 'On Hold', value: 'On Hold' },
            { label: 'Abandoned', value: 'Abandoned' },
        ];
    }

    connectedCallback(){
        console.log('connectedCallback servicePlanGoalsContinue');
        console.log(this.goalId);
        
        this.fecthData();     
    }

    fecthData(){
        getGoalRecord({
            recId: this.goalId
        })
        .then(result => { 
            console.log(result);
            if(result != null || result != undefined)
                this.fetchDataCont(result);
            else
                this.goalTemplates();
        })
    }

    /*@wire (getGoalRecord, {
        recId: "$goalId"
    })
    fecthData(result) {
        console.log("fecthData wire method called in service Plan Goal Continue");
        if (result.data) {
            console.log('success');
            let resultData = {...result.data};
            this.fetchDataCont(resultData);
            //console.log(result.data);            
        } else {
            console.log('error');            
        }
        
    }*/

    fetchDataCont(data){
        console.log('fetchDataCont');
        this.edit = false;
        this.accordianArrow = false;
        this.newGoal = false;
        this.goalName = data.Name;
        this.goalId = data.Id;
        this.selectedStatus = data.Status__c;
        if(data.Action_Items__r != undefined){
            this.completedActionItem = 0;
            this.actionItemLength = data.Action_Items__r.length;  
            this.actionItemList = data.Action_Items__r.map(row => ({
                ...row,
                pillCSS: 'notStarted',
                showObjTemplate: false,
                selectedObjTemp: undefined,
                showCross: false,
                Action_Item_Template__c: null
            }));
            for(var i=0; i < this.actionItemList.length; i++){
                if(this.actionItemList[i].Status__c == 'Not Started'){
                    this.actionItemList[i].pillCSS = 'notStarted';                    
                }
                else if(this.actionItemList[i].Status__c == 'In Progress'){
                    this.actionItemList[i].pillCSS = 'inProgress';
                }
                else if(this.actionItemList[i].Status__c == 'Completed'){
                    this.actionItemList[i].pillCSS = 'completed';
                    this.completedActionItem += 1;
                }
                else if(this.actionItemList[i].Status__c == 'On Hold'){
                    this.actionItemList[i].pillCSS = 'onHold';
                }
                else if(this.actionItemList[i].Status__c == 'Abandoned'){
                    this.actionItemList[i].pillCSS = 'abandoned';
                }
            }
        }
        this.checkEditMethod();
    }

    get progress(){
        return ((this.completedActionItem/this.actionItemLength)*100);
    }

    handleEdit(){
        if(this.edit == true){
            this.edit = false;
            this.checkEditMethod();                      
        }
        else{
            this.edit = true;
            this.checkEditMethod();            
        }
        this.accordianArrow = true;
    }
    
    handleAccordian(){
        if(this.accordianArrow == true){
            this.accordianArrow = false;
            this.edit = false;            
        }
        else{
            this.accordianArrow = true;
        }        
    }

    handleViewDetails(event){
        let recId =  event.target.dataset.id;  
        window.open('/'+recId,'_blank');   
    }

    handleDelete(event){
        console.log('handleDelete');
        let rId =  event.target.dataset.id;  
        let objName = event.target.dataset.object;

        if(objName == 'Goal'){
            this.deleteRec(rId, objName);           
        }
        else{
            let index = event.target.dataset.index;
            this.actionItemList.splice(index, 1);
            if(rId != undefined)
                this.deleteRec(rId, objName);
        }             
    }

    deleteRec(rId, objName){
        deleteRecords({
            recId: rId,
            objectName: objName
        })
        .then(result => {            
            console.log(result);
            this.refreshParentComponent();

            const event = new ShowToastEvent({
                variant: 'success',
                title: 'Success',
                message:
                    'Record successfully deleted.',
            });
            this.dispatchEvent(event);

        }) 
    }

    refreshParentComponent(){
        const refreshParent = new CustomEvent('refresh', {
            detail: this.goalId
        });
        this.dispatchEvent(refreshParent);
    }

    cancelComponent(){
        console.log('cancelComponent');
                 
        const newButtonFalse = new CustomEvent('newbutton', {
            detail: false
        });
        this.dispatchEvent(newButtonFalse);

        if(this.goalId){
            getGoalRecord({
                recId: this.goalId
            }).then(result => {            
                console.log(result);            
                this.fetchDataCont(result);
            })
        }        
    }

    addRow(){
        //this.fakeList = [];
        console.log('addRow');
        this.objectiveTemplates();
        console.log(this.otList);
        let newItem = { 'Name': '',
                        'Status__c': 'Not Started',
                        'Due_Date__c': '', 
                        'pillCSS': 'notStarted',
                        'Goal__c': this.goalId,
                        'showObjTemplate': true
                      };
        
        let a = this.actionItemList;
        //a.push({ 'Name': 'Testing','Status__c': 'Not Started', 'Due_Date__c': '', 'Case__c': '' });
        a.push(newItem);
        this.actionItemList = a;
    }

    handleNameChange(event){
        let name = event.target.value;
        if(!event.target.dataset.object){
            let index = event.target.dataset.index;        
            this.actionItemList[index].Name = name;
            console.log(JSON.stringify(this.actionItemList));
        }
        else{
            this.goalName = name;
        }
    }

    handleDateChange(event){
        let date = event.target.value;
        let index = event.target.dataset.index;        
        this.actionItemList[index].Due_Date__c = date;
        
        console.log(JSON.stringify(this.actionItemList));

    }

    handleStatusChange(event){
        console.log('handleStatusChange');
        let status = event.target.value;

        if(!event.target.dataset.object){
            let index = event.target.dataset.index;        
            this.actionItemList[index].Status__c = status;            
            console.log(JSON.stringify(this.actionItemList));
        }
        else{
            this.selectedStatus = status; 
        }

    }

    handleGoalTemplateChange(event){
        this.showCross = true;
        console.log('handleGoalTemplateChange');
        let gt = event.target.value; 
        this.selectedGoalTemplate = gt;       
        //console.log(gt);
        if(gt == null){
            this.showGoalTemplates = false;
        }
    }

    saveMethod(){
        console.log('saveMethod');
        console.log(this.caseId);
        console.log(JSON.stringify(this.actionItemList));
        let showNameError = false;
        let showDateError = false;
        let showStatusError = false;
        let showStatusMessage = false;
        let inProgress = false;
        for(var i=0;i<this.actionItemList.length;i++){
            console.log('in for');  
            console.log(this.actionItemList[i]);
            console.log(this.actionItemList[i].Name);
            console.log(this.actionItemList[i]);
            if((this.actionItemList[i].Name == null || this.actionItemList[i].Name == '') && 
            (this.actionItemList[i].Action_Item_Template__c == null || this.actionItemList[i].Action_Item_Template__c == '')){
                showNameError = true;
                break;
            }
            if(this.actionItemList[i].Due_Date__c == null || this.actionItemList[i].Due_Date__c == ''){
                showDateError = true;
                break;
            }
            if(this.selectedStatus == null || this.selectedStatus == undefined || this.selectedStatus == ''){
                showStatusError = true;
                showStatusMessage = 'Select a Goal Status';
                break;
            }
            if(this.selectedStatus == 'Not Started'){
                if(this.actionItemList[i].Status__c != 'Not Started'){
                    showStatusError = true;
                    showStatusMessage = 'If Goal is Not Started, Objective can only be Not Started';
                    break;
                }
            }
            if(this.selectedStatus == 'In Progress'){
                if(this.actionItemList[i].Status__c == 'In Progress'){
                    inProgress = true;
                    showStatusError = false;
                }
                if(inProgress == false){
                    showStatusError = true;
                    showStatusMessage = 'If Goal is In Progress, at least one Objective should be In Progress';
                }                    
                //break;
            }
            if(this.selectedStatus == 'Completed'){
                if(this.actionItemList[i].Status__c != 'Completed' && this.actionItemList[i].Status__c != 'Abandoned'){
                    showStatusError = true;
                    showStatusMessage = 'If Goal is Completed, Objectives can only be Completed or Abandoned';
                    break;
                }
            }
            if(this.selectedStatus == 'On Hold'){
                if(this.actionItemList[i].Status__c == 'In Progress'){
                    showStatusError = true;
                    showStatusMessage = 'If Goal is On Hold, no Objective can be In Progress';
                    break;
                }
            }
            if(this.selectedStatus == 'Abandoned'){
                if(this.actionItemList[i].Status__c != 'Abandoned' && this.actionItemList[i].Status__c != 'Completed'){
                    showStatusError = true;
                    showStatusMessage = 'If Goal is Abandoned, Objectives can only be Abandoned or Completed';
                    break;
                }
            }
        }
        
        if((this.goalName == null || this.goalName == '' || this.goalName == undefined) && 
        (this.selectedGoalTemplate == null || this.selectedGoalTemplate == '' || this.selectedGoalTemplate == undefined)){
            const event = new ShowToastEvent({
                variant: 'error',
                title: 'Error',
                message:
                    'Please select a goal template or enter goal name.',
            });
            this.dispatchEvent(event);
        }
        else if(showNameError == true){
            const event = new ShowToastEvent({
                variant: 'error',
                title: 'Error',
                message:
                    'Please select an objective template or enter objective name.',
            });
            this.dispatchEvent(event);
        }
        else if(showDateError == true){
            const event = new ShowToastEvent({
                variant: 'error',
                title: 'Error',
                message:
                    'Please enter the due date.',
            });
            this.dispatchEvent(event);
        }
        else if(showStatusError == true){
            const event = new ShowToastEvent({
                variant: 'error',
                title: 'Error',
                message: showStatusMessage,
            });
            this.dispatchEvent(event);
        }
        else{
            saveRecord({
                jsonString: JSON.stringify(this.actionItemList),
                servicePlanId: this.servicePlanId,
                goalName: this.goalName,
                goalId: this.goalId,
                goalStatus: this.selectedStatus,
                goalTemplateId: this.selectedGoalTemplate
            }).then(result => {            
                console.log(result);            
                this.fetchDataCont(result);
                this.refreshParentComponent();
    
                const event = new ShowToastEvent({
                    variant: 'success',
                    title: 'Success',
                    message:
                        'Record successfully updated/created.',
                });
                this.dispatchEvent(event);
            }).catch((error) => {
                console.log('error: '+JSON.stringify(error));
                const event = new ShowToastEvent({
                    variant: 'error',
                    title: 'Error',
                    message:
                        'Some error',
                });
                this.dispatchEvent(event);
             }) 
        }        
        /*const newButtonFalse = new CustomEvent('newbutton', {
            detail: false
        });
        this.dispatchEvent(newButtonFalse);*/
        
    }

    handleCross(){
        console.log('handleCross');
        this.showGoalTemplates = true;
        this.selectedGoalTemplate = undefined;
        this.showCross = false;
    }

    handleObjTemplateChange(event){
        console.log('handleObjTemplateChange');
        console.log(event.target.options.find(opt => opt.value === event.detail.value).label);
        let index = event.target.dataset.index;
        this.actionItemList[index].showCross = true;
        let ot = event.target.value;
        this.actionItemList[index].Action_Item_Template__c = ot;
        
        if(ot == null){
            this.actionItemList[index].showObjTemplate = false;
        }
    }

    handleObjCross(event){
        console.log('handleObjCross');
        let index = event.target.dataset.index;
        this.actionItemList[index].showObjTemplate = true;
        this.actionItemList[index].Action_Item_Template__c = undefined;
        this.actionItemList[index].showCross = false;

        console.log(this.actionItemList);
    }

    checkEditMethod(){
        const editCheck = new CustomEvent('editcheck', {
            detail: this.edit
        });
        this.dispatchEvent(editCheck);
    }
}